﻿using System.Windows.Controls;

namespace TMTool.Views.TexturePanel
{
    /// <summary>
    /// Interaction logic for TexturePanelView.xaml
    /// </summary>
    public partial class TexturePanelView : UserControl
    {
        public TexturePanelView()
        {
            InitializeComponent();
        }
    }
}
