﻿using System.Windows.Controls;

namespace TMTool.Views.TexturePanel
{
    /// <summary>
    /// Interaction logic for TexturePanelControls.xaml
    /// </summary>
    public partial class TexturePanelControls : UserControl
    {
        public TexturePanelControls()
        {
            InitializeComponent();
        }
    }
}
