﻿using System.Windows;
using System.Windows.Controls;
using TMTool.ViewModel;

namespace TMTool.Views.MeshPanel
{
    /// <summary>
    /// Interaction logic for MeshManagerView.xaml
    /// </summary>
    public partial class MeshManagerView : UserControl
    {
        public MeshManagerView()
        {
            InitializeComponent();
            DataContext = new TmToolViewModelLocator().MeshManagerVm;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
