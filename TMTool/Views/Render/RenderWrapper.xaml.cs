﻿using System.Diagnostics;
using System.Windows.Controls;

namespace TMTool.Views.Render
{
    /// <summary>
    /// Interaction logic for RenderWrapper.xaml
    /// </summary>
    public partial class RenderWrapper : UserControl
    {
        private ViewModel.ViewModelLocator vml;
        private ViewModel.TmToolMainViewModel mainVm;
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public RenderWrapper()
        {
            InitializeComponent();

            timer.Interval = 1;
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            ViewModel.TmToolMainViewModel mainVm = GetMainVm();

            if (mainVm != null)
            {
                mainVm.MonoEngine.InputFocused = IsMouseOver;
            }
        }

        private ViewModel.TmToolMainViewModel GetMainVm()
        {
            if (mainVm == null)
            {
                vml = new ViewModel.ViewModelLocator();
                mainVm = vml.MainVm;
            }
            return mainVm;
        }
    }
}
