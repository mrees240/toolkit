﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Views.ArchiveTree
{
    /// <summary>
    /// Interaction logic for ArchiveTreeView.xaml
    /// </summary>
    public partial class ArchiveTreeView : UserControl
    {
        private ArchiveTreeViewModel archiveTreeVm;

        public ArchiveTreeView()
        {
            InitializeComponent();
            archiveTreeVm = new TmToolViewModelLocator().ArchiveTreeVm;
            DataContext = archiveTreeVm;
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            MainTree.Items.Clear();

           // var renderVm = new TmToolViewModelLocator().RenderVm;

            //archiveTreeVm.UpdateTree(new ObservableCollection<EntityViewModel>(renderVm.Entities.Where(en => en.Entity.Instruction.IsRootInstruction)), renderVm.Meshes);

            foreach (var entity in archiveTreeVm.Entities)
            {
                MainTree.Items.Add(entity);
            }

            /*var entities = new ViewModelLocator().RenderVm.Scene.Entities.Where(en => en.Entity.Instruction.IsRootInstruction);

            foreach (var entity in entities)
            {
                MainTree.Items.Add(entity);
            }*/
        }
    }
}
