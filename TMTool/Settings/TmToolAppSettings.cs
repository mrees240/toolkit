﻿using WpfCore.Settings;

namespace TMTool.Settings
{
    /// <summary>
    /// Stores UI and control settings for the application.
    /// </summary>
    public class TmToolAppSettings
    {
        private FontSettings fontSettings;
        private DisplaySettings displaySettings;
        private InputSettings inputSettings;

        public TmToolAppSettings(FontSettings fontSettings, DisplaySettings displaySettings, InputSettings inputSettings)
        {
            this.fontSettings = fontSettings;
            this.displaySettings = displaySettings;
            this.inputSettings = inputSettings;
        }

        public void SaveSettings()
        {
            fontSettings.WriteToFile();
            displaySettings.WriteToFile();
            inputSettings.WriteToFile();
        }

        public void LoadSettings()
        {
            fontSettings.ReadFromFile();
            displaySettings.ReadFromFile();
            inputSettings.ReadFromFile();
        }

        public FontSettings FontSettings { get => fontSettings; set => fontSettings = value; }
        public DisplaySettings DisplaySettings { get => displaySettings; set => displaySettings = value; }
        public InputSettings InputSettings { get => inputSettings; set => inputSettings = value; }
    }
}
