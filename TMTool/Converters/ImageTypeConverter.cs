﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using TMLibrary.Textures;

namespace TMTool.Converters
{
    public class ImageTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string fileType = value as string;

            if (fileType != null)
            {
                switch (fileType.ToUpper())
                {
                    case "TIM":
                        return typeof(TimFile);
                    case "BMP":
                        return typeof(Bitmap);
                }
            }
            throw new InvalidOperationException("The value must be TIM or BMP.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
