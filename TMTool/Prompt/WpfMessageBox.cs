﻿using System.Windows;

namespace TMTool.Prompt
{
    /// <summary>
    /// Will provide a prompt using WPF and proper MVVM conventions. This needs finished.
    /// </summary>
    public class WpfMessageBox
    {
        public void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error");
        }

        public void ShowMessage(string message, string caption)
        {
            MessageBox.Show(message, caption);
        }

        public bool ShowPromptMessage(string message, string caption)
        {
            var result = System.Windows.MessageBox.Show(message, caption, System.Windows.MessageBoxButton.YesNo);
            return result == MessageBoxResult.Yes;
        }
    }
}
