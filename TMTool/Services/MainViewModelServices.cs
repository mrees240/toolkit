﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using Microsoft.Win32;
using MonoEngine.Code;
using MonoEngine.Code.Render;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using TMLibrary.Lib;
using TMLibrary.Lib.Collada;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.FileTypes.Group;
using TMLibrary.Lib.FileTypes.Point;
using TMLibrary.Lib.FileTypes.Ramps;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMLibrary.Rendering;
using TMTool.Prompt;
using TMTool.Render;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;
using WpfCore.Views.FileDialogs.Collada;

namespace TMTool.Services
{
    /// <summary>
    /// Provides functionality for commands within the main view model.
    /// </summary>
    public class MainViewModelServices
    {
        private Random rng = new Random();
        private TmToolMainViewModel mainVm;
        private ColladaFileDialog colladaFileDialog;
        private ColladaExporter colladaExporter;

        private bool debugMatricesLog = false;
        private StringBuilder matricesLog;

        public MainViewModelServices(TmToolMainViewModel mainVm)
        {
            this.mainVm = mainVm;
            colladaFileDialog = new ColladaFileDialog();
            colladaExporter = new ColladaExporter();
            matricesLog = new StringBuilder();

#if (DEBUG)
            debugMatricesLog = true;
#endif
        }

        public void OpenPtsFile()
        {
            /*var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            var renderVm = new TmToolViewModelLocator().RenderVm;

            if (dialog.ShowDialog() == true)
            {
                var pointsController = new PointsFileController(renderVm.Scene.PointsFile);
                pointsController.ReadPoints(dialog.FileName);
                var sceneController = new SceneController(renderVm.Scene);
                sceneController.UpdatePointsObjects();
            }*/
        }

        public void ShowSettings()
        {
            new TmToolViewModelLocator().MainVm.ShowSettingsPanel = true;
        }

        public void HideSettings()
        {
            new TmToolViewModelLocator().MainVm.ShowSettingsPanel = false;
        }

        public void OpenGroupFile()
        {
            /*var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            var renderVm = new TmToolViewModelLocator().RenderVm;

            if (dialog.ShowDialog() == true)
            {
                var groupController = new GroupFileController(renderVm.Scene.GroupFile);
                groupController.ReadFile(dialog.FileName);
                var sceneController = new SceneController(renderVm.Scene);
                sceneController.UpdateGroupFileObjects();
            }*/
        }

        public void OpenRmpFile()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == true)
            {
                var rampsFile = new RampsFile();
                var rampsFileController = new RampsFileController(rampsFile, new System.Threading.CancellationTokenSource());
                //renderVm.Scene.TextMeshes.Clear();
                rampsFileController.ReadRamps(dialog.FileName);
                var rampMesh = rampsFile.CreateRampMesh();
                mainVm.RenderManager.RampObject.UpdateMesh(rampMesh);
                //mainVm.MonoEngine.GameScene.AddObject(mainVm.RampObject);
                //var sceneController = new SceneController(renderVm.Scene);
                //sceneController.UpdateRampBuffers();
            }
        }

        public void OpenProjectFile()
        {
#if (DEBUG)
            AttemptToOpenFiles();
#else
            try
            {
                AttemptToOpenFiles();
            }
            catch (FileFormatException ffe)
            {
                new WpfMessageBox().ShowErrorMessage(ffe.Message);
            }
            catch (FileNotFoundException e)
            {
                new WpfMessageBox().ShowErrorMessage(e.Message);
            }
            catch (Exception e)
            {
                new WpfMessageBox().ShowErrorMessage(e.Message);
            }
#endif
        }

        /// <summary>
        /// Saves any changes made to the project files.
        /// </summary>
        public void SaveProject()
        {
            var projController = new ProjectController(mainVm.Project);
            projController.AttemptToSaveProject();
        }

        /// <summary>
        /// Exports every mesh that is being rendered as a Collada file.
        /// </summary>
        /// <param name="renderVm"></param>
        /// <param name="project"></param>
        /// <param name="fileName"></param>
        public void ExportScene()
        {
            var dialog = colladaFileDialog.SaveColladaFileDialog();

            if (dialog.ShowDialog() == true)
            {
                if (mainVm.InEntityMode)
                {
                    // Create hierarchy
                    var entityIndex = mainVm.SelectedEntityIndex;
                    var rootEntity = mainVm.EntityVmListFiltered[entityIndex];

                    var projectMeshMatrix = CreateMeshMatrixHierarchy(rootEntity, 0);
                    //rootMeshMatrix.Children.Add(projectMeshMatrix);

                    colladaExporter.Export(dialog.FileName, projectMeshMatrix, true);
                }
                else if (mainVm.InMeshMode)
                {
                    var meshIndex = mainVm.SelectedMeshIndex;
                    var meshVm = mainVm.MeshVmList[meshIndex];
                    var rootMeshMatrix = new MeshMatrixPair(meshVm.Mesh.GetBaseMesh(), Matrix4x4.Identity);

                    colladaExporter.Export(dialog.FileName, rootMeshMatrix, true);
                }
            }
        }

        private MeshMatrixPair CreateMeshMatrixHierarchy(EntityViewModel currentEntity, int depth)
        {
            var gameMeshes = currentEntity.MeshList;
            var matrix = currentEntity.Entity.Matrix;
            var meshMatrixPair = new MeshMatrixPair(matrix);

            foreach (var gameMesh in gameMeshes)
            {
                //if (gameMesh.ShowInRenderer)
                {
                    //var meshChild = new MeshMatrixPair(gameMesh.GetBaseMesh(), Matrix4x4.Identity);
                    meshMatrixPair.Meshes.Add(gameMesh.GetBaseMesh());
                }
            }

            foreach (var childEntity in currentEntity.EntityChildVmList)
            {
                meshMatrixPair.Children.Add(CreateMeshMatrixHierarchy(childEntity, depth + 1));
            }

            return meshMatrixPair;
        }

        /*private void AddSceneMeshes(EntityViewModel rootEntityVm, SceneObject rootSceneObject, EntityViewModel currentVm)
        {
            if (currentVm.Entity.Instruction.IsBoundingSphere)
            {
                var boundingSphereInstruction = currentVm.Entity.Instruction as Dpc701BoundingSphere;
                var sphereSceneObject = new SceneObject(mainVm);
                sphereSceneObject.SetMeshes(new List<Mesh>() { mainVm.SphereMesh });
                sphereSceneObject.GetTransformation().SetLocalScale(boundingSphereInstruction.BoundingSphere.Radius);
                sphereSceneObject.GetTransformation().SetLocalPosition(boundingSphereInstruction.BoundingSphere.Center);
                sphereSceneObject.GetTransformation().ParentTransformation = currentVm.SceneObjects[0].GetTransformation();
                currentVm.AddSceneObject(sphereSceneObject);
                mainVm.MonoEngine.GameScene.AddObject(sphereSceneObject);
            }
            foreach (var child in currentVm.EntityChildVmList)
            {
                var sceneObject = new SceneObject(mainVm);
                sceneObject.SetMeshes(child.MeshList);
                var trans = new Transformation();
                sceneObject.SetTransformation(trans);
                child.AddSceneObject(sceneObject);
                AddSceneMeshes(rootEntityVm, rootSceneObject, child);

                child.AddMeshesToGameScene(mainVm.MonoEngine.GameScene);
            }
        }*/

        public void ReplacePreviousSelectedPolygon()
        {
            var prevSo = mainVm.SelectedPolygonSceneObject;

            if (prevSo != null)
            {
                mainVm.MonoEngine.GameScene.RemoveObject(prevSo);
            }

            if (mainVm.SelectedPolygonVm != null)
            {

                var so = CreatePolygonSceneObject(mainVm.SelectedMeshVm, mainVm.SelectedPolygonVm);
                //so.GetDrawState().ActiveShader = mainVm.MonoEngine.GameScene.Shaders.DiffuseOnlyShader;
                so.GetDrawState().IsSolidColor = true;
                so.GetDrawState().TwoSides = true;
                so.GetDrawState().Shadeless = true;
                so.GetDrawState().Color = System.Drawing.Color.Red;
                so.GetDrawState().IgnoresDepthBuffer = true;
                mainVm.SelectedPolygonSceneObject = so;
                mainVm.MonoEngine.GameScene.AddObject(so);
            }
        }

        private GenericSceneObject CreatePolygonSceneObject(MeshViewModel meshVm, PolygonViewModel polygonVm)
        {
            var so = new GenericSceneObject();

            var mesh = new Mesh(meshVm.Mesh.GetBaseMesh());
            mesh.RemoveAllPolygons();
            //mesh.AddPolygon(polygonVm.Polygon);

            var indices = polygonVm.Polygon.GetIndices();

            var p = new Polygon(indices[0], indices[1], indices[2]);
            mesh.AddPolygon(p);
            /*var poly = polygonVm.Polygon;

            if (poly.IndexAmount == 4)
            {
                var p0 = new Polygon(0, 1, 2);
                var p1 = new Polygon(2, 1, 3);
                mesh.Vertices.AddRange(verts);
                mesh.AddPolygon(p0);
                mesh.AddPolygon(p1);
            }
            else
            {
                var p0 = new Polygon(poly);
                mesh.Vertices.AddRange(verts);
                mesh.AddPolygon(p0);
            }*/
            so.SetMeshes(new List<Mesh>() { mesh });

            return so;
        }

        /*private void UpdateAllEntityMatrices(IEnumerable<EntityViewModel> entityVms, SceneObject parent, Matrix4x4 stack, int depth)
        {
            foreach (var entityChild in entityVms)
            {
                foreach (var sceneObject in entityChild.SceneObjects)
                {
                    var trans = sceneObject.GetTransformation();

                    trans.SetLocalMatrix(entityChild.Entity.Matrix);
                    if (debugMatricesLog)
                    {
                        for (int i = 0; i < depth; i++)
                        {
                            matricesLog.Append("\t");
                        }
                        matricesLog.Append($"LOCAL POS: {localMatrix.Translation.X},{localMatrix.Translation.Y},{localMatrix.Translation.Z}");
                        matricesLog.Append($" WORLD POS: {trans.WorldMatrix.Translation.X},{trans.WorldMatrix.Translation.Y},{trans.WorldMatrix.Translation.Z}");
                    }
                    UpdateAllEntityMatrices(entityChild.EntityChildVmList, sceneObject, stack, depth + 1);
                }

            }
        }*/

        /// <summary>
        /// Attempts to open a raw game file. Needs reorganized todo.
        /// </summary>
        public void AttemptToOpenFiles()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == true)
            {
                mainVm.TextureFileVms.Clear();
                var projectTypes = new ProjectFactory().LoadProjectTypes().Values.ToArray();
                char[] fileSignatureChars = new char[12];
                char[] textureFileSignatureChars = new char[12];
                ProjectDefinition project = null;

                using (var reader = new StreamReader(dialog.FileName))
                {
                    reader.Read(fileSignatureChars, 0, fileSignatureChars.Length);
                }

                // This needs reoptimized so it will use the dictionary key. todo
                foreach (var projectType in projectTypes)
                {
                    string fileSignature = new string(fileSignatureChars);
                    string testSignature = projectType.MeshFile.Definition.FileSignature;

                    if (CompareSignatures(testSignature, fileSignature))
                    {
                        project = projectType;
                        break;
                    }
                }

                var sigString = new string(fileSignatureChars);

                if (project == null)
                {
                    throw new System.Exception($"Unable to find project type with signature: { sigString }");
                }

                new ProjectController(project).LoadProject(dialog.FileName, dialog.SafeFileName);

                mainVm.Project = project;
                var texturePanelVm = new TmToolViewModelLocator().TexturePanelVm;
                //var renderVm = new TmToolViewModelLocator().RenderVm;
                var meshManagerVm = new TmToolViewModelLocator().MeshManagerVm;

                //renderVm.Project = project;
                var newTextureFileViewModels = new List<TextureFileViewModel>();

                foreach (var texFile in project.TextureArchiveFile.TextureFiles)
                {
                    var bitmapSet = new BitmapSet();
                    bitmapSet.Diffuse = texFile.BitmapFilePair;
                    newTextureFileViewModels.Add(new TextureFileViewModel(texFile, bitmapSet));
                }
                mainVm.TextureFileVms = new ObservableCollection<TextureFileViewModel>(newTextureFileViewModels);
                texturePanelVm.Textures = mainVm.TextureFileVms;
                /*renderVm.Scene.Textures = newTextureFileViewModels;
                renderVm.Scene.Project = project;
                renderVm.Textures = mainVm.TextureFileVms;*/
                mainVm.RenderManager.ResetScene();
                mainVm.MeshVmList.Clear();
                mainVm.EntityVmList.Clear();
                mainVm.EntityVmListFiltered.Clear();
                mainVm.EntitySceneObjects.Clear();
                mainVm.MeshSceneObjects.Clear();
                mainVm.SelectedEntityIndex = 0;

                foreach (var entityVm in mainVm.EntityVmList)
                {
                    entityVm.Entity.Instruction.UpdateDrawDistance();
                }

                foreach (var mesh in project.MeshFile.Meshes)
                {
                    var meshVm = new MeshViewModel(mesh, mainVm.MeshVmList.Count());
                    mainVm.MeshVmList.Add(meshVm);

                    var baseMesh = mesh.GetBaseMesh();
                    var meshSceneObject = new MeshSceneObject(baseMesh);
                    meshSceneObject.GetDrawState().Shadeless = true;
                    mainVm.MeshSceneObjects.Add(baseMesh, meshSceneObject);
                    meshVm.AddSceneObject(meshSceneObject);
                }

                var sphereInstructionList = new List<Dpc701BoundingSphere>();

                foreach (var entity in mainVm.Project.MeshFile.Entities)
                {
                    // Filter
                    //var entityInstruction = entity.Instruction as DpcSubEntity509;

                    //if (entityInstruction != null)
                    {
                        var entityVm = new EntityViewModel(entity, mainVm, mainVm.EntityVmList.Count);

                        var currentSceneObject = new SceneObject(entityVm, mainVm);
                        currentSceneObject.GetTransformation().SetLocalMatrix(entityVm.Entity.Matrix);
                        var drawState = currentSceneObject.GetDrawState();
                        var objectState = currentSceneObject.GetObjectState();
                        objectState.Name = $"Instruction at {entity.Instruction.Address}";

                        var trans = currentSceneObject.GetTransformation();

                        foreach (var mesh in entityVm.Entity.MeshList)
                        {
                            var meshId = project.MeshFile.Meshes.IndexOf(mesh);
                            var meshVm = mainVm.MeshVmList[meshId];
                            entityVm.MeshVmList.Add(meshVm);

                            var baseMesh = mesh.GetBaseMesh();

                            var childObj = new SceneObject(entityVm, mainVm);
                            childObj.SetMeshes(new List<Mesh>() { baseMesh });
                            childObj.GetDrawState().Shadeless = true;
                            childObj.GetObjectState().Name = $"Mesh #{meshId}";
                            //childObj.GetDrawState().ApplyDrawDistance = true;
                            //childObj.GetDrawState().MinDrawDistance = entityVm.Entity.Instruction.MinDrawDistance;
                            //childObj.GetDrawState().MaxDrawDistance = entityVm.Entity.Instruction.MaxDrawDistance;

                            currentSceneObject.AddChild(childObj);
                        }
                        mainVm.EntitySceneObjects.Add(entityVm, currentSceneObject);


                        mainVm.EntityVmList.Add(entityVm);
                        entityVm.AddSceneObject(currentSceneObject);
                        if (entityVm.Entity.Instruction.IsBoundingSphere)
                        {
                            sphereInstructionList.Add((Dpc701BoundingSphere)entityVm.Entity.Instruction);
                        }
                    }
                }
                // Add Entity children
                foreach (var entityVm in mainVm.EntityVmList)
                {
                    foreach (var child in entityVm.Entity.Children)
                    {
                        var childVm = mainVm.EntityVmList.Where(e => e.Entity == child).FirstOrDefault();
                        if (childVm != null)
                        {
                            entityVm.EntityChildVmList.Add(childVm);
                        }
                    }
                }
                var rootEntities = mainVm.EntityVmList.Where(e => e.Entity.Instruction.IsRootInstruction);
                var selectableEntities = new List<EntityViewModel>();//.SelectMany(e => e.EntityChildVmList);

                if (debugMatricesLog)
                {
                    matricesLog.Clear();
                }
                //UpdateAllEntityMatrices(rootEntities, null, Matrix4x4.Identity, 0);
                if (mainVm.DebugBoundingSpheres)
                {
                    mainVm.RenderManager.SetSpheres(sphereInstructionList);
                }
                if (debugMatricesLog)
                {
                    File.WriteAllText($"{EngineSettings.DebugDir}\\MatricesLog.txt", matricesLog.ToString());
                }

                foreach (var entityObjectPair in mainVm.EntitySceneObjects)
                {
                    var entity = entityObjectPair.Key;
                    var sceneObject = entityObjectPair.Value;

                    foreach (var child in entity.EntityChildVmList)
                    {
                        sceneObject.AddChild(mainVm.EntitySceneObjects[child]);
                    }
                }

                selectableEntities = mainVm.EntityVmList.SelectMany(e => e.EntityChildVmList).
                    Where(e => e.Entity.Instruction is DpcSubEntity509).SelectMany(e => e.EntityChildVmList).ToList();
                selectableEntities = selectableEntities.Where(e => e.Entity.Instruction.GetAllMeshes().Count() > 0).ToList();
                //selectableEntities = mainVm.EntityVmList.SelectMany(e => e.EntityChildVmList).ToList();
                foreach (var rootEnt in rootEntities)
                {
                    selectableEntities.Add(rootEnt);
                }
                for (int iEntity = 0; iEntity < selectableEntities.Count; iEntity++)
                {
                    var entity = selectableEntities[iEntity];

                    if (entity.Entity.Instruction.IsRootInstruction)
                    {
                        entity.Info = "Root Entity";
                    }
                    else
                    {
                        //entity.Info = $"Entity #{iEntity+1} {entity.Entity.Instruction.GetType().ToString()} {entity.Entity.Instruction.Address}";
                        entity.Info = $"Entity #{iEntity + 1}";

                    }
                }
                /*foreach (var entityVm in mainVm.EntityVmList)
                {
                    if (entityVm.Entity.Instruction is DpcTranslationMatrix)
                    {
                        foreach (var child in entityVm.EntityChildVmList)
                        {
                            if (child.Entity.Instruction is DpcSubEntity509)
                            {
                                foreach (var sceneObject in child.SceneObjects)
                                {
                                    sceneObject.GetObjectState().Transformation.WorldMatrix = Matrix4x4.CreateTranslation(sceneObject.GetObjectState().Transformation.WorldMatrix.Translation);
                                }
                            }
                        }
                    }
                }*/
                //selectableEntities = mainVm.EntityVmList.ToList();
                this.mainVm.EntityVmListFiltered = new ObservableCollection<EntityViewModel>(selectableEntities);
                mainVm.ProjectLoaded = true;
                mainVm.SelectedEntityIndex = 0;
                mainVm.SelectedMeshIndex = 0;
            }
        }


        /// <summary>
        /// Compares the expected file signature with another signature. Asterisks (*) are used as wildcards.
        /// </summary>
        /// <param name="signatureEntered"></param>
        /// <param name="expectedSignature"></param>
        /// <returns></returns>
        private bool CompareSignatures(string signatureEntered, string expectedSignature)
        {
            // This may need to be changed to a regular expression at some point.
            byte[] sigEnteredBuffer = Encoding.ASCII.GetBytes(signatureEntered);
            byte[] sigExpectedBuffer = Encoding.ASCII.GetBytes(expectedSignature);

            if (sigEnteredBuffer.Length != sigEnteredBuffer.Length)
            {
                return false;
            }
            for (int iChar = 0; iChar < signatureEntered.Length; iChar++)
            {
                if (signatureEntered.ElementAt(iChar) != '*' && expectedSignature.ElementAt(iChar) != '*')
                {
                    if (sigEnteredBuffer[iChar] != sigExpectedBuffer[iChar])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Saves the hex values within the instruction hierarchy into a text document.
        /// </summary>
        public void SaveInstructionsLogPrompt(string fileName = "log")
        {
            var dialog = new SaveFileDialog();
            dialog.FileName = fileName;
            dialog.DefaultExt = ".txt";
            dialog.Filter = "Text documents (.txt)|*.txt";

            if (mainVm.Project != null && mainVm.Project.MeshFile.Instructions.Count() > 0)
            {
                if (dialog.ShowDialog() == true)
                {
                    SaveInstructionsLog(dialog.FileName);
                }
            }
            else
            {
                new WpfMessageBox().ShowErrorMessage("A file with instructions must be loaded before you can save an instructions log.");
            }
        }

        public void SaveInstructionsLog(string fileName = "log")
        {
            using (var textWriter = new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate)))
            {
                new MeshArchiveController(mainVm.Project.MeshFile, mainVm.Project).SaveInstructionsLog(textWriter);
            }
        }
    }
}
