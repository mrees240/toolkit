﻿using System.Windows;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Services
{
    public class ArchiveTreeServices
    {
        private ArchiveTreeViewModel archiveTree;

        public ArchiveTreeServices(ArchiveTreeViewModel archiveTree)
        {
            this.archiveTree = archiveTree;
        }

        public void AddMeshToEntity(EntityViewModel entityVm, MeshViewModel meshVm)
        {
            entityVm.AddMesh(meshVm);
            MessageBox.Show("Mesh added");
        }
    }
}
