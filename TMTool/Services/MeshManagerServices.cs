﻿using ColladaLibrary.Collada;
using ColladaLibrary.Misc;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMLibrary.Lib.Collada;
using TMLibrary.Rendering;
using TMLibrary.Textures;
using TMTool.Prompt;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Services
{
    /// <summary>
    /// Provides functionality for Mesh Manager View Model commands.
    /// </summary>
    public class MeshManagerServices
    {
        private MeshManagerViewModel meshManagerVm;

        public MeshManagerServices(MeshManagerViewModel meshManagerVm)
        {
            this.meshManagerVm = meshManagerVm;
        }

        /// <summary>
        /// Promps the user with an open file dialog and allows them to select meshes they wish to import.
        /// </summary>
        public void ImportMeshesPrompt()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.DefaultExt = ".dae";
            dialog.Filter = "Collada Files (.dae)|*.dae";

            if (dialog.ShowDialog() == true)
            {
                for (int iFile = 0; iFile < dialog.FileNames.Length; iFile++)
                {
                    string fileName = dialog.FileNames[iFile];
                    string safeFileName = dialog.SafeFileNames[iFile];
#if (DEBUG)
                    AddMeshFile(fileName, safeFileName, true);
#else
                    try
                    {
                        AddMeshFile(fileName, safeFileName, true);
                    }
                    catch (Exception ex)
                    {
                        new WpfMessageBox().ShowErrorMessage(ex.Message);
                    }
#endif
                }
            }
        }

        /// <summary>
        /// Adds a mesh to the project model and updates all mesh view model collections.
        /// </summary>
        /// <param name="fileName"></param>
        public void AddMeshFile(string fileName, string safeFileName, bool defaultLighting)
        {
            var gameDefinition = meshManagerVm.Project;
            var directory = fileName.Replace(safeFileName, "");
            var importer = new ColladaImporter(false, defaultLighting, fileName);
            importer.Load();
            var colladaMesh = importer.CreateSingleMesh();
            var newTextures = importer.NewTextures;

            for (int iTex = 0; iTex < newTextures.Count; iTex++)
            {
                var bitmap = newTextures[iTex];
                var filePair = new BitmapFilePair("", bitmap.Bitmap);
                var texture = new TextureFile(meshManagerVm.Project.TextureArchiveFile, filePair);
                var texController = new TextureFileController(texture);
                texController.ReadFromBitmap(bitmap.Bitmap);
                AddTexture(texture);
            }
            var gameMesh = new GameMesh(meshManagerVm.Project.MeshFile);
            var meshController = new MeshController(gameMesh, gameDefinition);
            meshController.ReadFromColladaMesh(colladaMesh, defaultLighting);

            meshManagerVm.Project.MeshFile.Meshes.Add(gameMesh);
            AddMeshToViewModel(gameMesh);
        }

        /// <summary>
        /// Creates a texture view model for all the new textures and updates the corresponding view models.
        /// </summary>
        /// <param name="textureFile"></param>
        private void AddTexture(TextureFile textureFile)
        {
            /*var texturePanelVm = new TmToolViewModelLocator().TexturePanelVm;
            var textureFileVm = new TextureFileViewModel(textureFile);
            textureFileVm.Transparency = textureFile.Transparency;
            meshManagerVm.Scene.Textures.Add(textureFileVm);
            texturePanelVm.Textures.Add(textureFileVm);*/
        }

        /// <summary>
        /// Goes through every identity and replaces all selected mesh IDs with the new mesh ID.
        /// </summary>
        /// <param name="replacedIds"></param>
        /// <param name="replacementMesh"></param>
        public void UpdateEntityMeshes(List<GameMesh> replacedMeshes, MeshViewModel replacementMesh)
        {
            /*foreach (var entity in meshManagerVm.Scene.Entities)
            {
                for (int iMesh = 0; iMesh < entity.MeshList.Count; iMesh++)
                {
                    var mesh = entity.MeshList[iMesh];

                    if (replacedMeshes.Contains(mesh))
                    {
                        entity.MeshList[iMesh] = replacementMesh.Mesh;
                    }
                }
            }*/
        }

        /// <summary>
        /// Adds mesh view models to all the mesh view model lists in the Mesh Manager panel.
        /// </summary>
        /// <param name="meshVm"></param>
        private void AddMeshToViewModel(GameMesh mesh)
        {
            /*var meshVm = new MeshViewModel(mesh, meshManagerVm.MeshViewModels.Count(), meshManagerVm.Scene);
            var replacementMeshVm = new MeshViewModel(mesh, meshManagerVm.MeshViewModels.Count(), meshManagerVm.Scene);
            meshManagerVm.MeshViewModels.Add(meshVm);
            meshManagerVm.ReplacementMeshViewModels.Add(replacementMeshVm);
            new TmToolViewModelLocator().RenderVm.UpdateNewMesh(meshVm);*/
        }

        /// <summary>
        /// Replaces the selected meshes with the selected replacement mesh. Updates all pointers and entity mesh ID lists.
        /// </summary>
        public void ReplaceMesh()
        {
            var meshArchiveController = new MeshArchiveController(meshManagerVm.Project.MeshFile, meshManagerVm.Project);
            var replacedMeshVms = meshManagerVm.MeshViewModels.Where(m => m.CheckBoxSelected).ToList();
            var replacedMeshes = replacedMeshVms.Select(m => m.Mesh).ToList();
            var replacementMesh = meshManagerVm.ReplacementMeshViewModels.Where(m => m.CheckBoxSelected).FirstOrDefault();

            if (replacementMesh != null)
            {
                meshArchiveController.UpdateMeshPointers(replacedMeshes, replacementMesh.Mesh);
                UpdateEntityMeshes(replacedMeshes, replacementMesh);
            }

            DeselectAllMeshes();
        }

        private void DeselectAllMeshes()
        {
            Parallel.ForEach(meshManagerVm.MeshViewModels, (m) =>
            {
                m.CheckBoxSelected = false;
            });
            Parallel.ForEach(meshManagerVm.ReplacementMeshViewModels, (m) =>
            {
                m.CheckBoxSelected = false;
            });
        }
    }
}
