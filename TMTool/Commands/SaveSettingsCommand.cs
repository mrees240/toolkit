﻿using System;
using System.Windows.Input;
using WpfCore.ViewModel.Settings;
using WpfCore.Settings;
using WpfCore.ViewModel;
using TMTool.Settings;

namespace TMTool.Commands
{
    public class SaveSettingsCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        private void AttemptToSaveSettings(TmToolAppSettings settings)
        {
            settings.SaveSettings();
        }

        public void Execute(object parameter)
        {
            object[] parameters = (object[])parameter;
            DisplaySettingsViewModel displaySettingsVm = (DisplaySettingsViewModel)parameters[0];
            FontSettingsViewModel fontSettingsVm = (FontSettingsViewModel)parameters[1];
            InputSettingsViewModel inputSettingsVm = (InputSettingsViewModel)parameters[2];
            var appSettings = new TmToolAppSettings(fontSettingsVm.Model, displaySettingsVm.Model, inputSettingsVm.InputSettings);

            AttemptToSaveSettings(appSettings);
            fontSettingsVm.UpdateFontSettingsInUi();
            inputSettingsVm.UpdateInputSettingsInUi();
        }
    }
}
