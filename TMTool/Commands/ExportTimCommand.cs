﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.Windows.Input;
using TMLibrary.Textures;

namespace TMTool.Commands
{
    public class ExportTimCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
            /*var vml = new ViewModelLocator();
            return vml.Main.Project.TpcFile.AtLeastOneSelected;*/
        }

        public void Execute(object parameter)
        {
            var textureFile = parameter as TextureFile;

            var dialog = new SaveFileDialog();
            dialog.DefaultExt = ".tim";
            dialog.AddExtension = true;
            bool? opened = dialog.ShowDialog();

            if (opened == true)
            {
                string timFileName = dialog.FileName;
                var timFileController = new TextureFileController(textureFile);
                timFileController.WriteTimFile(timFileController.CreateTimFileFromBitmap(textureFile.BitmapFilePair.Bitmap, textureFile.Parent), timFileName);
            }
        }
    }
}
