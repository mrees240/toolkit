﻿using ColladaLibrary.Utility;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using TMLibrary.Lib.Definitions;
using TMLibrary.Textures;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Commands
{
    public class ExportMultipleTextureFilesCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                object[] parameters = (object[])parameter;
                var textureViewModels = parameters[0] as ObservableCollection<TextureFileViewModel>;
                if (textureViewModels != null)
                {
                    return textureViewModels.Where(t => t.Selected).Count() > 0;
                }
            }
            return false;
        }

        private int FindSelectedTimAmount(ObservableCollection<TextureFileViewModel> textureViewModels)
        {
            return textureViewModels.Where(t => t.Selected).Count();
        }

        public void Execute(object parameter)
        {
            object[] parameters = (object[])parameter;
            var textureViewModels = parameters[0] as ObservableCollection<TextureFileViewModel>;
            Type imageType = parameters[1] as Type;
            TmToolMainViewModel mainVm = parameters[2] as TmToolMainViewModel;

            var selectedTextures = textureViewModels.Where(t => t.Selected);

            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            dialog.ShowNewFolderButton = true;
            bool showingDialog = (bool)dialog.ShowDialog((App.Current.MainWindow));

            if (showingDialog)
            {
                for (int i = 0; i < selectedTextures.Count() ; i++)
                {
                    var textureViewModel = textureViewModels[textureViewModels.IndexOf(selectedTextures.ElementAt(i))];
                    var timFileController = new TextureFileController(textureViewModel.TextureFile);
                    string dir = dialog.SelectedPath + @"\" + GetFileName(textureViewModel.TextureFile, imageType, mainVm.Project);

                    if (imageType.Equals(typeof(Bitmap)))
                    {
                        TextureFileUtility.WriteBitmapFile(dir, textureViewModel.TextureFile.BitmapFilePair.Bitmap);
                    }
                    else if (imageType.Equals(typeof(TimFile)))
                    {
                        var tim = timFileController.CreateTimFileFromBitmap(textureViewModel.TextureFile.BitmapFilePair.Bitmap, textureViewModel.TextureFile.Parent);
                        timFileController.WriteTimFile(tim, dir);
                    }
                }
            }
        }

        private string GetFileName(TextureFile textureFile, Type fileType, ProjectDefinition project)
        {
            string fileNameStart = project.TextureArchiveSafeFileName.Split('.').First();
            string extension = "tim";
            if (fileType.Equals(typeof(Bitmap)))
            {
                extension = "bmp";
            }
            return string.Format("{2}-{0}.{1}", textureFile.GetId(), extension, fileNameStart);
        }
    }
}
