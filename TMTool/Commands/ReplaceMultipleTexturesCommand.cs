﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TMLibrary.Lib;
using TMLibrary.Lib.Definitions;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Commands
{
    public class ReplaceMultipleTexturesCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                object[] parameters = (object[])parameter;
                var textureViewModels = parameters[3] as ObservableCollection<TextureFileViewModel>;
                if (textureViewModels != null)
                {
                    return textureViewModels.Where(t => t.Selected).Count() > 0;
                }
            }
            return false;
        }

        public void Execute(object parameter)
        {
            object[] parameters = (object[])parameter;
            var mainVm = (TmToolMainViewModel)parameters[0];
            var texturePanelVm = (TexturePanelViewModel)parameters[1];
            //var renderVm = (RenderViewModel)parameters[2];
            var project = mainVm.Project;
            //AttemptToReplaceTextures(project, texturePanelVm, renderVm);
        }

        /*private void AttemptToReplaceTextures(ProjectDefinition project, TexturePanelViewModel texturePanelVm, RenderViewModel renderVm)
        {
            var vml = new TmToolViewModelLocator();
            var dialog = new OpenFileDialog();
            bool? opened = dialog.ShowDialog();
            var projectController = new ProjectController(project);

            if (opened == true)
            {
                string newTextureFileName = dialog.FileName;
                var newTexture = new TextureFileViewModel(projectController.LoadTextureFile(newTextureFileName));

                renderVm.UpdateTexturesAndMeshes(texturePanelVm.Textures, newTexture, false);
            }
        }*/
    }
}
