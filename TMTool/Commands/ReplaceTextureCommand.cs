﻿using Microsoft.Win32;
using System;
using System.Windows.Input;
using TMLibrary.Lib;
using TMLibrary.Textures.Exceptions;
using TMTool.ViewModel;
using System.Collections.ObjectModel;
using TMLibrary.Lib.Definitions;
using TMTool.Prompt;
using TMTool.ViewModel.Rendering;
using ColladaLibrary.Render;
using WpfCore.Utility;

namespace TMTool.Commands
{
    public class ReplaceTextureCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            object[] parameters = (object[])parameter;
            var project = parameters[0] as ProjectDefinition;
            var prevTimFile = parameters[1] as TextureFileViewModel;
            //var mainVm = parameters[2] as TmToolMainViewModel;

            try
            {
                AttemptToReplaceTexture(project, prevTimFile);
            }
            catch (InvalidBppException ex)
            {
                new WpfMessageBox().ShowErrorMessage(ex.Message);
            }
        }

        private void AttemptToReplaceTexture(ProjectDefinition project, TextureFileViewModel previousTexture)
        {
            var dialog = new OpenFileDialog();
            var projectController = new ProjectController(project);
            bool? opened = dialog.ShowDialog();

            if (opened == true)
            {
                string newTextureFileName = dialog.FileName;
                var set = new BitmapSet();
                var tex = projectController.LoadTextureFile(newTextureFileName);
                set.Diffuse.Bitmap = tex.BitmapFilePair.Bitmap;
                //var newTexture = new TextureFileViewModel(tex, set);

                //var previousTextures = new ObservableCollection<TextureFileViewModel>();
                //previousTextures.Add(previousTexture);

                //previousTexture.BitmapSource = BitmapUtility.BitmapToBitmapSource(set.Diffuse.Bitmap);
                previousTexture.UpdateTextureFile(tex);
                
                //renderViewModel.UpdateTexturesAndMeshes(previousTextures, newTexture, true);
            }
        }
    }
}
