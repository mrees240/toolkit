﻿using System;
using System.Linq;
using System.Windows.Input;
using TMLibrary.Textures;
using System.IO;
using ColladaLibrary.Misc;

namespace TMTool.Commands
{
    public class AddTextureCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        private void AttemptToAddTexture(TextureArchiveFile textureArchive)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog();
            bool? opened = dialog.ShowDialog();

            if (opened == true)
            {
                var filePair = new BitmapFilePair();
                string fileName = dialog.FileName;
                var newTimFile = new TextureFile(textureArchive, filePair);
                var tpcFileController = new TextureArchiveController(textureArchive);
                var newTimFileController = new TextureFileController(newTimFile);
                if (fileName.ToUpper().Contains(".tim".ToUpper()))
                {
                    newTimFileController.ReadFromExternalTimFile(fileName, textureArchive.TextureFiles.ToList());
                }
                else if (fileName.ToUpper().Contains(".bmp".ToUpper()))
                {
                    newTimFileController.ReadFromBitmapFile(fileName);
                }
                else
                {
                    throw new FileFormatException("Invalid file format. It must be a .tim file or a .bmp file.");
                }
                tpcFileController.AddTextureFile(newTimFile);
            }
        }

        public void Execute(object parameter)
        {
            var tpcFile = parameter as TextureArchiveFile;

            AttemptToAddTexture(tpcFile);
        }
    }
}
