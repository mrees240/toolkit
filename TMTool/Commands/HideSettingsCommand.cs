﻿using System;
using System.Windows.Input;
using WpfCore.ViewModel;
using WpfCore.ViewModel.Settings;

namespace TmTool.Commands
{
    public class HideSettingsCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            /*object[] parameters = (object[])parameter;
            var coreVm = (CoreViewModel)parameters[0];
            var displaySettingsVm = (DisplaySettingsViewModel)parameters[1];
            var fontSettingsVm = (FontSettingsViewModel)parameters[2];
            var inputSettingsVm = (InputSettingsViewModel)parameters[3];
            coreVm.ShowSettingsPanel = false;
            UpdateSettings(displaySettingsVm, inputSettingsVm);*/
        }

        private void UpdateSettings(DisplaySettingsViewModel displaySettingsVm, InputSettingsViewModel inputSettingsVm)
        {
            displaySettingsVm.Model.ReadFromFile();
            displaySettingsVm.Model.RevertColorProperties();

            inputSettingsVm.InputSettings.ReadFromFile();
            inputSettingsVm.UpdateInputSettingsInUi();
        }
    }
}
