﻿using ColladaLibrary.Utility;
using Microsoft.Win32;
using System;
using System.Windows.Input;
using TMLibrary.Textures;

namespace TMTool.Commands
{
    public class ExportBmpCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var timFile = parameter as TextureFile;

            var dialog = new SaveFileDialog();
            dialog.DefaultExt = ".bmp";
            dialog.AddExtension = true;
            bool? opened = dialog.ShowDialog();

            if (opened == true)
            {
                string bmpFileName = dialog.FileName;
                TextureFileUtility.WriteBitmapFile(bmpFileName, timFile.BitmapFilePair.Bitmap);
            }
        }
    }
}
