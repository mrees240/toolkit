﻿using Microsoft.Xna.Framework.Graphics;
using System.IO;
using TMLibrary.Textures;

namespace TMTool.Extensions
{
    public static class TextureFileExtensions
    {
        public static Texture2D GetTexture(this TextureFileController controller, GraphicsDevice device, bool flipRgb)
        {
            using (var stream = new MemoryStream())
            {
                controller.WritePng(stream, flipRgb);
                var tex = Texture2D.FromStream(device, stream);

                return tex;
            }
        }
    }
}
