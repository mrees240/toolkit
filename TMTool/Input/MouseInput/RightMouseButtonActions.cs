﻿using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMTool.ViewModel;
using WpfCore.Settings;
using WpfCore.ViewModel;

namespace TMTool.Input.MouseInputs
{
    public class RightMouseButtonActions : IMouseButtonActions
    {
        private MouseButton mouseButton;
        private InputSettings inputSettings;
        private TmToolMainViewModel mainViewModel;
        private IMonoViewportViewModelWrapper viewportVm;

        public RightMouseButtonActions(
            InputSettings inputSettings,
            MouseButton mouseButton,
            TmToolMainViewModel mainViewModel,
            IMonoViewportViewModelWrapper viewportVm)
        {
            this.viewportVm = viewportVm;
            this.inputSettings = inputSettings;
            this.mouseButton = mouseButton;
            this.mainViewModel = mainViewModel;
        }

        public void UpdateViewportVm(IMonoViewportViewModelWrapper viewportVm)
        {
            this.viewportVm = viewportVm;
        }

        public void ClickAction(MonoEngineScene scene)
        {
        }

        public void DefaultAction(MonoEngineScene scene)
        {
        }

        public void DownAction(MonoEngineScene scene)
        {
        }

        public void HoldAction(MonoEngineScene scene)
        {
        }

        public void ReleaseAction(MonoEngineScene scene)
        {
        }

        public MouseButton GetLmb()
        {
            return mouseButton;
        }

        public void DoubleClickAction(MonoEngineScene scene)
        {
            throw new NotImplementedException();
        }
    }
}
