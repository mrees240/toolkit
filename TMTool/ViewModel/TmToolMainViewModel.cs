using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MonoEngine.Code;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Render;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using MonoEngine.ViewModel;
using MonoGameCore.Render.Texture;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using TMLibrary.Lib.Definitions;
using TMTool.Commands;
using TMTool.Input;
using TMTool.Input.MouseInputs;
using TMTool.Render;
using TMTool.Services;
using TMTool.Settings;
using TMTool.ViewModel.Rendering;
using WpfCore.Settings;
using WpfCore.ViewModel;

namespace TMTool.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public class TmToolMainViewModel : ViewModelBase
    {
        private ProjectDefinition project;
        private ObservableCollection<TextureFileViewModel> textureFileVms;
        private ObservableCollection<MeshViewModel> meshVmList;
        private ObservableCollection<EntityViewModel> entityVmList;
        private ObservableCollection<EntityViewModel> entityVmListFiltered;
        private int currentSettingsPanelId;
        private bool showSettingsPanel;
        private bool projectLoaded;
        private int selectedMeshIndex;
        private int selectedEntityIndex;
        private MouseState mouseState;
        private bool showingPolygonPanel;
        private bool inEntityMode;
        private bool inMeshMode;
        private bool runAnimations;
        private bool debugBoundingSpheres;
        private RenderManagerTmTool renderManager;
        private bool showDebugMenus;

        private GenericSceneObject selectedPolygonSceneObject;
            
        private MonoEngine.Code.MonoEngineCore monoEngine;
        private List<IMonoViewportViewModelWrapper> monoViewportVms;
        private InputSettings inputSettings;


        private MainViewModelServices mainVmServices;


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public TmToolMainViewModel()
        {
            debugBoundingSpheres = false;
            mainVmServices = new MainViewModelServices(this);
            inEntityMode = true;
            textureFileVms = new ObservableCollection<TextureFileViewModel>();
            entityVmList = new ObservableCollection<EntityViewModel>();
            meshVmList = new ObservableCollection<MeshViewModel>();
            entityVmListFiltered = new ObservableCollection<EntityViewModel>();
            RaisePropertyChanged(nameof(ProjectSavable));
            ProjectLoaded = false;
            InitializeCommands();
            HidePolygonPanel();


            InitializeMonoEngine();

            monoEngine.StartEngine();
            renderManager = new RenderManagerTmTool(monoEngine.GameScene);
        }

        private void TogglePolygonPanel()
        {
            if (!showingPolygonPanel)
            {
                ShowPolygonPanel();
            }
            else
            {
                HidePolygonPanel();
            }
        }

        private void HidePolygonPanel()
        {
            showingPolygonPanel = false;
            ViewportColumnSpan = 2;
            PolygonPanelColumnSpan = 0;
        }

        private void ShowPolygonPanel()
        {
            showingPolygonPanel = true;
            ViewportColumnSpan = 1;
            PolygonPanelColumnSpan = 1;
        }

        private DefaultEditorLmbAction defaultEditorLmbAction;

        private void InitializeMonoEngine()
        {
            inputSettings = new InputSettings();
            var vml = new TmToolViewModelLocator();
            monoViewportVms = new List<IMonoViewportViewModelWrapper>();
            monoViewportVms.Add(new TmToolMonoViewportViewModel());

            var rmb = new MonoEngine.Input.MouseButton(MouseButtons.Right);

            var mouseController = new MouseController();
            this.mouseState = mouseController.MouseState;

            var rmbOrbitAction = new DefaultOrbitRmbAction(inputSettings, mouseState, rmb, monoViewportVms);
            var rmbController = new MouseButtonController(mouseState, rmbOrbitAction);
            var lmbActions = new LeftMouseButtonActions(inputSettings, mouseState, new MonoEngine.Input.MouseButton(System.Windows.Forms.MouseButtons.Left),
                monoViewportVms, this);
            //var rmbActions = new RightMouseButtonActions(inputSettings, new WpfCore.Input.MouseButton(System.Windows.Forms.MouseButtons.Right), projectVm);
            var rmbActions = new MonoEngine.Input.MouseInputs.DefaultOrbitRmbAction(inputSettings, mouseState,
                new MouseButton(System.Windows.Forms.MouseButtons.Right),
                monoViewportVms);
            var mmbActions = new MonoEngine.Input.MouseInputs.DefaultPanMmbAction(inputSettings, mouseState, new MouseButton(System.Windows.Forms.MouseButtons.Middle),
                monoViewportVms);
            mouseController.InitializeActions(lmbActions, rmbActions, mmbActions, monoViewportVms);

            //inputSettings = vml.InputSettingsVm.InputSettings;
            //var mouseController = new MouseController(mouseState, lmbActions, rmbActions, mmbActions, monoViewportVms);
            EngineSettings settings = new EngineSettings();
            SetEngineSettings(settings);

            monoEngine = new MonoEngineCore(monoViewportVms, mouseController, settings);

            defaultEditorLmbAction = new DefaultEditorLmbAction(monoEngine, mouseController.MouseState, false);
            monoEngine.GameScene.EditorLmbAction = defaultEditorLmbAction;
            InitializeKeyboardInput();
            //monoEngine.GameScene.AddObject(new GridObject(engineSettings));
        }

        private void SetEngineSettings(EngineSettings settings)
        {
            settings.ShowGrid = true;
            settings.GridLength = 1000;
            settings.GridRows = 10;
        }

        public void InitializeCommands()
        {
            OpenFileCommand = new RelayCommand(() => new MainViewModelServices(this).OpenProjectFile());
            OpenPtsFileCommand = new RelayCommand(() => new MainViewModelServices(this).OpenPtsFile());
            OpenRmpFileCommand = new RelayCommand(() => new MainViewModelServices(this).OpenRmpFile());
            SaveInstructionsCommand = new RelayCommand(() => new MainViewModelServices(this).SaveInstructionsLogPrompt());
            SaveFileCommand = new RelayCommand(() => { new MainViewModelServices(this).SaveProject(); });
            ExportSceneCommand = new RelayCommand(() => { new MainViewModelServices(this).ExportScene(); });
            OpenGroupFileCommand = new RelayCommand(() => new MainViewModelServices(this).OpenGroupFile());
            ShowSettingsCommand = new RelayCommand(() => new MainViewModelServices(this).ShowSettings());
            SwitchPolygonPanelOnOffCommand = new RelayCommand(() =>
            {
                TogglePolygonPanel();
            });
            ActivateEntityModeCommand = new RelayCommand(() =>
            {
                InEntityMode = true;
                InMeshMode = false;
                ChangeRenderMode();
            });
            ActivateMeshModeCommand = new RelayCommand(() =>
            {
                InEntityMode = false;
                InMeshMode = true;
                ChangeRenderMode();
            });
            //Test1Command = new RelayCommand(() => new MainViewModelServices(this).Test());
        }

        private void ChangeRenderMode()
        {
            SelectedMeshIndex = selectedMeshIndex;
            SelectedEntityIndex = selectedEntityIndex;
        }

        public TmToolAppSettings GetAppSettings()
        {
            var vml = new TmToolViewModelLocator();

            return new TmToolAppSettings(vml.FontSettingsVm.Model, vml.DisplaySettingsVm.Model, vml.InputSettingsVm.InputSettings);
        }

        public ProjectDefinition Project
        {
            get
            {
                return project;
            }
            set
            {
                project = value;
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(ProjectSavable));
            }
        }

        public RelayCommand ShowMapCompilerPanelCommand
        {
            get;
            private set;
        }

        public RelayCommand OldCompileMapCommand
        {
            get;
            private set;
        }

        public RelayCommand CompileCarSelCommand
        {
            get;
            private set;
        }

        public RelayCommand SaveInstructionsCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenFileCommand
        {
            get;
            private set;
        }

        public RelayCommand SwitchPolygonPanelOnOffCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenRmpFileCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenGroupFileCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenPtsFileCommand
        {
            get;
            private set;
        }

        public RelayCommand SaveFileCommand
        {
            get;
            private set;
        }

        public RelayCommand ExportSceneCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ActivateEntityModeCommand
        {
            get;
            private set;
        }

        public RelayCommand ActivateMeshModeCommand
        {
            get;
            private set;
        }

        public RelayCommand Test1Command
        {
            get;
            private set;
        }

        public ObservableCollection<TextureFileViewModel> TextureFileVms { get => textureFileVms; set => textureFileVms = value; }
        public ObservableCollection<MeshViewModel> MeshVmList { get => meshVmList; set => meshVmList = value; }
        public ObservableCollection<EntityViewModel> EntityVmList { get => entityVmList; set => entityVmList = value; }

        public bool ProjectSavable
        {
            get
            {
                return project!= null && project.Saveable;
            }
        }

        public int CurrentSettingsPanelId { get => currentSettingsPanelId; set { currentSettingsPanelId = value; RaisePropertyChanged(); } }

        public bool ShowSettingsPanel { get => showSettingsPanel; set { showSettingsPanel = value; RaisePropertyChanged(); } }


        public MonoViewportViewModel GetMonoViewportVm0 { get { return monoViewportVms[0].GetBaseViewportVm(); } }

        public MonoEngineCore MonoEngine { get => monoEngine; }
        public bool ProjectLoaded { get => projectLoaded; set { projectLoaded = value; RaisePropertyChanged(); } }

        private int viewportColumnSpan;
        private int polygonPanelColumnSpan;

        public int SelectedMeshIndex
        {
            get => selectedMeshIndex;
            set
            {
                selectedMeshIndex = value;
                if (selectedMeshIndex >= 0 && selectedMeshIndex < project.MeshFile.Meshes.Count)
                {
                    monoEngine.GameScene.RemoveAllSceneObjects();
                    RaisePropertyChanged();

                    SelectedMeshVm = meshVmList[selectedMeshIndex];
                    SelectedMeshPolygons = selectedMeshVm.PolygonViewModels;

                    if (inMeshMode)
                    {
                        if (selectedMeshIndex >= 0)
                        {
                            var currentMesh = MeshVmList[selectedMeshIndex];

                            monoEngine.GameScene.AddObject(currentMesh.SceneObjects[0]);
                        }
                    }
                }
            }
        }

        private MeshViewModel selectedMeshVm;

        private PolygonViewModel selectedPolygonVm;

        public int SelectedEntityIndex
        {
            get => selectedEntityIndex;
            set
            {
                if (EntityVmListFiltered.Count > 0)
                {
                    if (selectedEntityIndex >= 0 && selectedEntityIndex < EntityVmListFiltered.Count)
                    {
                        var prevEntity = EntityVmListFiltered[selectedEntityIndex];

                        if (prevEntity != null)
                        {
                            foreach (var sceneObject in prevEntity.SceneObjects)
                            {
                                monoEngine.GameScene.RemoveObject(sceneObject);
                            }
                        }
                    }
                    //entityVmList[selectedEntityIndex].GetDrawState().IsVisible = false;
                    selectedEntityIndex = value;
                    //entityVmList[selectedEntityIndex].GetDrawState().IsVisible = true;
                    RaisePropertyChanged();

                    if (inEntityMode)
                    {
                        if (selectedEntityIndex >= 0)
                        {
                            var currentEntity = EntityVmListFiltered[selectedEntityIndex];

                            foreach (var sceneObject in currentEntity.SceneObjects)
                            {
                                monoEngine.GameScene.AddObject(sceneObject);
                            }
                        }
                    }
                }
            }
        }

        private ObservableCollection<PolygonViewModel> selectedMeshPolygons = new ObservableCollection<PolygonViewModel>();

        private Dictionary<Mesh, MeshSceneObject> meshSceneObjects = new Dictionary<Mesh, MeshSceneObject>();
        private Dictionary<EntityViewModel, SceneObject> entitySceneObjects = new Dictionary<EntityViewModel, SceneObject>();

        public ObservableCollection<EntityViewModel> EntityVmListFiltered { get => entityVmListFiltered; set { entityVmListFiltered = value; RaisePropertyChanged(); } }

        public Dictionary<EntityViewModel, SceneObject> EntitySceneObjects { get => entitySceneObjects; }
        public Dictionary<Mesh, MeshSceneObject> MeshSceneObjects { get => meshSceneObjects; }

        public bool InEntityMode { get => inEntityMode; set { inEntityMode = value; RaisePropertyChanged(); } }
        public bool InMeshMode { get => inMeshMode; set { inMeshMode = value; RaisePropertyChanged(); } }

        public bool RunAnimations { get => runAnimations; set { runAnimations = value; RaisePropertyChanged(); } }

        public MeshViewModel SelectedMeshVm { get => selectedMeshVm; set { selectedMeshVm = value; RaisePropertyChanged(); } }

        private int selectedPolygonIndex;

        public PolygonViewModel SelectedPolygonVm { get => selectedPolygonVm;
            set
            {
                selectedPolygonVm = value;
                RaisePropertyChanged();
                mainVmServices.ReplacePreviousSelectedPolygon();
            }
        }

        private Action ToggleShowDebugMenus()
        {
            return new Action(() => { ShowDebugMenus = !showDebugMenus; });
        }

        private void InitializeKeyboardInput()
        {
            monoEngine.KeyboardController.AddKeyCommand(System.Windows.Input.Key.F12, ToggleShowDebugMenus(), KeyStateMono.Released);
        }

        public ObservableCollection<PolygonViewModel> SelectedMeshPolygons { get => selectedMeshPolygons; set { selectedMeshPolygons = value; RaisePropertyChanged(); } }

        public int ViewportColumnSpan { get => viewportColumnSpan; set { viewportColumnSpan = value; RaisePropertyChanged(); } }

        public int PolygonPanelColumnSpan { get => polygonPanelColumnSpan; set { polygonPanelColumnSpan = value; RaisePropertyChanged(); } }

        public bool ShowingPolygonPanel { get => showingPolygonPanel; set { showingPolygonPanel = value; RaisePropertyChanged(); } }

        public GenericSceneObject SelectedPolygonSceneObject { get => selectedPolygonSceneObject; set => selectedPolygonSceneObject = value; }
        public int SelectedPolygonIndex { get => selectedPolygonIndex; set { selectedPolygonIndex = value; RaisePropertyChanged(); if (selectedPolygonIndex >= 0) { SelectedPolygonVm = selectedMeshPolygons[selectedPolygonIndex]; } } }

        public RenderManagerTmTool RenderManager { get => renderManager; }
        public bool DebugBoundingSpheres { get => debugBoundingSpheres; set { debugBoundingSpheres = value; RaisePropertyChanged(); } }

        public bool ShowDebugMenus { get => showDebugMenus; set { showDebugMenus = value; RaisePropertyChanged(); } }
    }
}