﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System.Collections.ObjectModel;
using TMLibrary.Lib.Definitions;
using TMTool.ViewModel.Rendering;

namespace TMTool.ViewModel
{
    public class ArchiveTreeViewModel : ViewModelBase
    {
        private ProjectDefinition project;
        private ObservableCollection<MeshViewModel> meshes;
        private ObservableCollection<EntityViewModel> entities;
        private EntityViewModel selectedEntity;

        public ArchiveTreeViewModel()
        {
            meshes = new ObservableCollection<MeshViewModel>();
            entities = new ObservableCollection<EntityViewModel>();
            DefineCommands();
        }

        public void UpdateTree(ObservableCollection<EntityViewModel> entityVms, ObservableCollection<MeshViewModel> meshes)
        {
            Entities = entityVms;
            Meshes = meshes;
        }

        public RelayCommand<MeshViewModel> AddMeshCommand
        {
            get;
            private set;
        }

        public RelayCommand<EntityViewModel> SetSelectedEntityCommand
        {
            get;
            private set;
        }

        private void DefineCommands()
        {
            SetSelectedEntityCommand = new RelayCommand<EntityViewModel>((parameter) =>
            {
                this.selectedEntity = parameter;
            });
            AddMeshCommand = new RelayCommand<MeshViewModel>((parameter) =>
            {
                var service = new Services.ArchiveTreeServices(this);
                service.AddMeshToEntity(selectedEntity, parameter);
            });
        }

        public ProjectDefinition Project { get => project; set => project = value; }
        public ObservableCollection<MeshViewModel> Meshes { get => meshes; set { meshes = value; RaisePropertyChanged(); } }
        public ObservableCollection<EntityViewModel> Entities { get => entities; set { entities = value; RaisePropertyChanged(); } }
    }
}
