﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoEngine.ViewModel;
using WpfCore.ViewModel;

namespace TMTool.ViewModel
{
    public class TmToolMonoViewportViewModel : IMonoViewportViewModelWrapper
    {
        private MonoViewportViewModel baseViewModel;

        public TmToolMonoViewportViewModel()
        {
            baseViewModel = new MonoViewportViewModel();

            baseViewModel.BackgroundColor = System.Windows.Media.Color.FromRgb(153, 217, 234);
        }

        public MonoViewportViewModel GetBaseViewportVm()
        {
            return baseViewModel;
        }
    }
}
