﻿using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using TMLibrary.Textures;
using WpfCore.Utility;

namespace TMTool.ViewModel.Rendering
{
    /// <summary>
    /// Provides a view model for the texture files. Used in the Texture panel.
    /// </summary>
    public class TextureFileViewModel : ViewModelBase
    {
        private TextureFile textureFile;
        private bool selected;
        private BitmapSource bitmapSource;
        private ushort actualWidth;
        private ushort height;
        private bool transparency;

        public TextureFileViewModel(TextureFile textureFile, BitmapSet bitmapSet)
        {
            this.textureFile = textureFile;
            UpdateVm(textureFile);
        }

        public bool Selected { get => selected; set { selected = value; RaisePropertyChanged(); } }

        public void UpdateTextureFile(TextureFile texture)
        {
            //BitmapSource = BitmapUtility.BitmapToBitmapSource(texture.BitmapFilePair.Bitmap);
            UpdateVm(texture);
            new TextureArchiveController(textureFile.Parent).ReplaceTextureFile(texture, textureFile);
            TextureFile.BitmapFilePair.OnUpdate();
        }

        private void UpdateVm(TextureFile texture)
        {
            texture.BitmapSource = BitmapUtility.BitmapToBitmapSource(texture.BitmapFilePair.Bitmap);
            BitmapSource = texture.BitmapSource;
            ActualWidth = (ushort)texture.ActualWidth;
            Height = (ushort)texture.Height;
            TransparencyEnabled = texture.TransparencyEnabled;
        }

        public TextureFile TextureFile
        {
            get => textureFile;
        }

        public int IdNumber { get { return textureFile.GetId(); } }

        public BitmapSource BitmapSource { get => bitmapSource; set { bitmapSource = value; RaisePropertyChanged(); } }

        public ushort ActualWidth { get => actualWidth; set { actualWidth = value; RaisePropertyChanged(); } }
        public ushort Height { get => height; set { height = value; RaisePropertyChanged(); } }

        public bool TransparencyEnabled { get => textureFile.TransparencyEnabled; set { textureFile.TransparencyEnabled = value; RaisePropertyChanged(); } }
    }
}
