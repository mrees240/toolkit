﻿using GalaSoft.MvvmLight;
using MonoGameCore.Render.Mesh;
using MonoGameCore.Render.Texture;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TMLibrary.Rendering;
using TMTool.Render;

namespace TMTool.ViewModel.Rendering
{
    public class MeshViewModel : ViewModelBase
    {
        private GameMesh mesh;
        private List<GameMesh> visualMeshes;
        private List<PolygonGroup> polygonGroups;
        private int idNumber;
        private bool selected;
        private bool checkBoxSelected;
        private bool initialized;
        private bool showInRenderer;
        private uint minDrawDistance;
        private uint maxDrawDistance;

        private List<MeshSceneObject> sceneObjects;

        private ObservableCollection<PolygonViewModel> polygonViewModels;

        // Needs reorganized
        private PolygonGroup[] billboardGroups;
        private MeshBuffer[] billboardBuffers;

        public MeshViewModel(GameMesh mesh, int idNumber)
        {
            sceneObjects = new List<MeshSceneObject>();
            polygonGroups = new List<PolygonGroup>();
            polygonViewModels = new ObservableCollection<PolygonViewModel>();
            var meshController = new MeshArchiveController(mesh.ParentFile, mesh.ParentFile.Parent);
            this.idNumber = idNumber;
            this.mesh = mesh;
            visualMeshes = meshController.CreateVisualMeshes(mesh);
            showInRenderer = mesh.ShowInRenderer;
            CreatePolygonViewModels();
            Update();
        }

        private void CreatePolygonViewModels()
        {
            foreach (var polygon in mesh.OriginalPolygons)
            {
                polygonViewModels.Add(new PolygonViewModel(polygon));
            }
        }

        /*private void CreateBillboardMeshes(GameMesh mesh)
        {
            var meshController = new MeshArchiveController(mesh.ParentFile, mesh.ParentFile.Parent);

            var billboardPolygons = mesh.OriginalPolygons.Where(p => p.RenderFlag != RenderFlag.Normal);

            mesh.BillboardMeshes = new GameMesh[billboardPolygons.Count()];
            billboardGroups = new PolygonGroup[mesh.BillboardMeshes.Length];

            for (int iBillboard = 0; iBillboard < mesh.BillboardMeshes.Length; iBillboard++)
            {
                var billboardPolygon = billboardPolygons.ElementAt(iBillboard);
                mesh.BillboardMeshes[iBillboard] = meshController.CreateBillboardMesh(billboardPolygon, mesh.ParentFile);
            }
        }*/

        public void AddSceneObject(MeshSceneObject sceneObject)
        {
            sceneObjects.Add(sceneObject);
        }

        public void Update()
        {
            /*foreach (var visualMesh in visualMeshes)
            {
                var polyGroupController = new PolygonGroupController();
                int polysPerGroup = 99999;
                int groupAmount = (visualMesh.Polygons.Count() / polysPerGroup) + 1;
                for (int iGroup = 0; iGroup < groupAmount; iGroup++)
                {
                    int minPolyIndex = polysPerGroup * iGroup;
                    polygonGroups.AddRange(polyGroupController.CreatePolygonGroups(visualMesh, minPolyIndex, polysPerGroup, scene.Project));
                }

                for (int iBillboard = 0; iBillboard < billboardGroups.Count(); iBillboard++)
                {
                    var billboardMesh = mesh.BillboardMeshes[iBillboard];
                    billboardGroups[iBillboard] = new PolygonGroupController().CreatePolygonGroups(billboardMesh, 0, billboardMesh.Polygons.Count(), scene.Project).First();
                }

                foreach (var polyGroup in PolygonGroups)
                {
                    InitializeVbo(polyGroup);
                }

                foreach (var billboardGroup in billboardGroups)
                {
                    InitializeVbo(billboardGroup);
                }
            }*/
        }

        private void InitializeVbo(PolygonGroup polygonGroup)
        {
            var polygonGroupController = new PolygonGroupController();
            polygonGroupController.InitializePolygonGroup(polygonGroup, mesh);
        }

        /*public void UpdateTextureCoordinates(TextureFileViewModel previousTexture, TextureFileViewModel newTexture, RenderViewModel renderViewModel)
        {
            foreach (var polygon in mesh.Polygons)
            {
                foreach (var index in polygon.GetIndices())
                {
                    index.TexCoord.U = 0;
                    index.TexCoord.V = 0;
                }
            }
            foreach (var polygon in mesh.Polygons)
            {
                foreach (var index in polygon.GetIndices())
                {
                    index.TexCoord.U = 0;
                    index.TexCoord.V = 0;
                }
            }
        }*/

        public string DebugData
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var group in polygonGroups)
                {
                    foreach (var val in group.VertexValues)
                    {
                        sb.Append(val + " ");
                    }
                }
                return sb.ToString();
            }
        }

        public string MeshInfo
        {
            get
            {
                return $"Mesh #{IdNumber + 1}";
            }
        }

        public int IdNumber { get => idNumber; }
        public bool Selected { get => selected; set { selected = value; RaisePropertyChanged(); } }
        public ObservableCollection<GamePolygon> Polygons
        {
            get
            {
                return new ObservableCollection<GamePolygon>(mesh.OriginalPolygons);
            }
        }

        /*public PolygonViewModel SelectedPolygon { get => selectedPolygon; set {
                selectedPolygon = value;
                RaisePropertyChanged();
                scene.SelectedPolygonInitialized = false; } }*/

        public GameMesh Mesh { get => mesh; set => mesh = value; }
        public bool CheckBoxSelected { get => checkBoxSelected; set { checkBoxSelected = value; RaisePropertyChanged(); } }

        public PolygonGroup[] BillboardGroups { get => billboardGroups; }
        public MeshBuffer[] BillboardBuffers { get => billboardBuffers; set => billboardBuffers = value; }
        public bool Initialized { get => initialized; set => initialized = value; }
        public ObservableCollection<PolygonViewModel> PolygonViewModels { get => polygonViewModels; }
        public List<PolygonGroup> PolygonGroups { get => polygonGroups; set => polygonGroups = value; }
        public List<GameMesh> VisualMeshes { get => visualMeshes; }
        public uint MinDrawDistance { get => minDrawDistance; set => minDrawDistance = value; }
        public uint MaxDrawDistance { get => maxDrawDistance; set => maxDrawDistance = value; }
        public List<MeshSceneObject> SceneObjects { get => sceneObjects; }
    }
}