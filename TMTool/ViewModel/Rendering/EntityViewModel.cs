﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using GalaSoft.MvvmLight;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Numerics;
using System.Text;
using TMLibrary.Lib;
using TMLibrary.Rendering;
using TMTool.Render;

namespace TMTool.ViewModel.Rendering
{
    public class EntityViewModel : ViewModelBase
    {
        private Entity entity;
        private List<MeshViewModel> meshVmList;
        private ObservableCollection<EntityViewModel> entityChildVmList;
        private int idNumber;
        private string info;
        private List<Mesh> meshes;
        private List<SceneObject> sceneObjects;
        private TmToolMainViewModel mainVm;

        public EntityViewModel(Entity entity, TmToolMainViewModel mainVm, int idNumber)
        {
            this.idNumber = idNumber;
            this.mainVm = mainVm;
            this.entity = entity;
            this.info = "MISSING LABEL";
            meshVmList = new List<MeshViewModel>();
            entityChildVmList = new ObservableCollection<EntityViewModel>();
            sceneObjects = new List<SceneObject>();
        }

        private bool childrenInitialized = false;

        public List<GameMesh> MeshList { get => entity.MeshList; }

        // This is bound to the TM2 PC format. Needs reimplemented. todo
        public string TreeViewInfo
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(entity.Instruction.Address);
                sb.Append("->");
                sb.Append(HexHeader);

                foreach (var mesh in MeshList)
                {
                    int meshId = mesh.ParentFile.Meshes.IndexOf(mesh);
                    sb.Append($"->Mesh {meshId + 1}");
                }

                return sb.ToString();
            }
        }

        public List<EntityViewModel> AllDescentants
        {
            get
            {
                return FindAllDescendants(this);
            }
        }

        public List<EntityViewModel> FindAllDescendants(EntityViewModel entityVm)
        {
            var descendants = new List<EntityViewModel>();

            foreach (var child in entityVm.EntityChildVmList)
            {
                descendants.Add(child);
                descendants.AddRange(FindAllDescendants(child));
            }

            return descendants;
        }

        public Entity Entity { get => entity; }
        public List<MeshViewModel> MeshVmList { get => meshVmList; }
        public ObservableCollection<EntityViewModel> EntityChildVmList { get => entityChildVmList; }
        public long Address { get => entity.Instruction.Address; }
        public string HexHeader
        {
            get
            {
                return entity.Instruction.ToString();
            }
        }

        public void AddMesh(MeshViewModel meshVm)
        {
            Entity.Instruction.MeshList.Add(meshVm.Mesh);
            RaisePropertyChanged(nameof(MeshVmList));
        }

        public void AddSceneObject(SceneObject sceneObject)
        {
            sceneObjects.Add(sceneObject);
        }

        public void RemoveMeshesFromGameScene(MonoEngineScene scene)
        {
            foreach (var sceneObject in sceneObjects)
            {
                scene.RemoveObject(sceneObject);
            }
        }

        public void SetRenderVisibility(bool isVisible)
        {
            foreach (var sceneObject in sceneObjects)
            {
                sceneObject.GetDrawState().IsVisible = isVisible;
            }
        }

        public string Info
        {
            get => info;
            set { info = value; RaisePropertyChanged(); }
        }

        public List<SceneObject> SceneObjects { get => sceneObjects; }
    }
}
