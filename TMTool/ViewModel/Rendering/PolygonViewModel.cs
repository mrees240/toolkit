﻿using Microsoft.Xna.Framework;
using System;
using MonoGameCore.Render;
using TMLibrary.Rendering;
using ColladaLibrary.Rendering;
using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace TMTool.ViewModel.Rendering
{
    public class PolygonViewModel : ViewModelBase
    {
        private List<TextureCoordinate> texCoords;
        private GamePolygon polygon;

        public PolygonViewModel(GamePolygon polygon)
        {
            this.polygon = polygon;
            TexCoords = polygon.GetAllTextureCoordinates();
        }

        public GamePolygon Polygon { get => polygon; }
        public List<TextureCoordinate> TexCoords { get => texCoords; private set { texCoords = value; RaisePropertyChanged(); } }
        public long Address { get { return polygon.Address; } }
        public String HeaderTagHex { get { return polygon.PolygonType.Header.ToString("X"); } }
        public String IndicesText { get { return polygon.IndicesText; } }
        public ushort RenderFlagId { get { return polygon.RenderFlagId; } }
        public RenderFlag RenderFlag { get { return polygon.RenderFlag; } }
        public float CrossProductX { get { return polygon.CrossProduct.X; } }
        public float CrossProductY { get { return polygon.CrossProduct.Y; } }
        public float CrossProductZ { get { return polygon.CrossProduct.Z; } }
        public float CrossProductXAbs { get { return Math.Abs(polygon.CrossProduct.X); } }
        public float CrossProductYAbs { get { return Math.Abs(polygon.CrossProduct.Y); } }
        public float CrossProductZAbs { get { return Math.Abs(polygon.CrossProduct.Z); } }
        public float DotUnitX { get { return System.Numerics.Vector3.Dot(polygon.CrossProduct, System.Numerics.Vector3.UnitX); } }
        public float DotUnitY { get { return System.Numerics.Vector3.Dot(polygon.CrossProduct, System.Numerics.Vector3.UnitY); } }
        public float DotUnitZ { get { return System.Numerics.Vector3.Dot(polygon.CrossProduct, System.Numerics.Vector3.UnitZ); } }
        public float DotUnit1 { get { var unit = new System.Numerics.Vector3(1.0f, 1.0f, 1.0f);
            unit = unit.GetNormalizedVector3(); return System.Numerics.Vector3.Dot(polygon.CrossProduct, unit ); } }
        public double Area { get { return polygon.GetArea(); } }

        public string PolySort0
        {
            get
            {
                byte[] finalBytes = BitConverter.GetBytes(polygon.PolySort0);
                return finalBytes[0].ToString() + " " + finalBytes[1].ToString();
            }
        }
        public string PolySort1
        {
            get
            {
                byte[] finalBytes = BitConverter.GetBytes(polygon.PolySort1);
                return finalBytes[0].ToString() + " " + finalBytes[1].ToString();
            }
        }

        public ushort U_0 { get { return polygon.UnknownUShorts[0]; } }
        public ushort U_1 { get { return polygon.UnknownUShorts[1]; } }
        public ushort U_2 { get { return polygon.UnknownUShorts[2]; } }
        public ushort U_3 { get { return polygon.UnknownUShorts[3]; } }
        public ushort U_4 { get { return polygon.UnknownUShorts[4]; } }
    }
}
