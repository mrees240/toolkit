/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:TMTool"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using MonoEngine.ViewModel;
using WpfCore.ViewModel;
using WpfCore.ViewModel.Settings;

namespace TMTool.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class TmToolViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public TmToolViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<TmToolMainViewModel>();
            SimpleIoc.Default.Register<TexturePanelViewModel>();
            SimpleIoc.Default.Register<MonoViewportViewModel>();
            SimpleIoc.Default.Register<ArchiveTreeViewModel>();
            SimpleIoc.Default.Register<MeshManagerViewModel>();

            SimpleIoc.Default.Register<FontSettingsViewModel>();
            SimpleIoc.Default.Register<DisplaySettingsViewModel>();
            SimpleIoc.Default.Register<InputSettingsViewModel>();
        }

        public FontSettingsViewModel FontSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FontSettingsViewModel>();
            }
        }

        public DisplaySettingsViewModel DisplaySettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DisplaySettingsViewModel>();
            }
        }

        public InputSettingsViewModel InputSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<InputSettingsViewModel>();
            }
        }

        public ArchiveTreeViewModel ArchiveTreeVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ArchiveTreeViewModel>();
            }
        }

        public TmToolMainViewModel MainVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TmToolMainViewModel>();
            }
        }

        public TexturePanelViewModel TexturePanelVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TexturePanelViewModel>();
            }
        }

        public MonoViewportViewModel MonoViewportVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MonoViewportViewModel>();
            }
        }

        public MeshManagerViewModel MeshManagerVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MeshManagerViewModel>();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}