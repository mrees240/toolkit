﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using TMLibrary.Lib.Definitions;
using TMTool.Services;
using TMTool.ViewModel.Rendering;

namespace TMTool.ViewModel
{
    /// <summary>
    /// Provides a view model for the Mesh Manager panel. Includes mesh lists for the selected meshes and replacement meshes.
    /// </summary>
    public class MeshManagerViewModel : ViewModelBase
    {
        private ObservableCollection<MeshViewModel> meshViewModels;
        private ObservableCollection<MeshViewModel> replacementMeshViewModels;
        private ProjectDefinition project;
        private bool controlsEnabled;

        public MeshManagerViewModel()
        {
            meshViewModels = new ObservableCollection<MeshViewModel>();
            replacementMeshViewModels = new ObservableCollection<MeshViewModel>();
            //project = new ProjectDefinition();
            DefineCommands();
        }

        private void DefineCommands()
        {
            ReplaceMeshCommand = new RelayCommand(() =>
            {
                var service = new MeshManagerServices(this);
                service.ReplaceMesh();
            });
            ImportMeshCommand = new RelayCommand(() =>
            {
                var service = new MeshManagerServices(this);
                service.ImportMeshesPrompt();
            });
        }

        public void LoadProject(ProjectDefinition project)
        {
            this.project = project;
            ControlsEnabled = true;

            MeshViewModels = CreateMeshViewModels();
            ReplacementMeshViewModels = CreateMeshViewModels();
        }

        private ObservableCollection<MeshViewModel> CreateMeshViewModels()
        {
            ObservableCollection<MeshViewModel> meshViewModels = new ObservableCollection<MeshViewModel>();

            foreach (var mesh in project.MeshFile.Meshes)
            {
                meshViewModels.Add(new MeshViewModel(mesh, meshViewModels.Count));
            }

            return meshViewModels;
        }

        public RelayCommand ReplaceMeshCommand
        {
            get;
            private set;
        }

        public RelayCommand ImportMeshCommand
        {
            get;
            private set;
        }

        public ObservableCollection<MeshViewModel> MeshViewModels { get => meshViewModels; set { meshViewModels = value; RaisePropertyChanged(); } }
        public ObservableCollection<MeshViewModel> ReplacementMeshViewModels { get => replacementMeshViewModels; set { replacementMeshViewModels = value; RaisePropertyChanged(); } }

        public ProjectDefinition Project { get => project; }
        //public Scene Scene { get => scene; }
        public bool ControlsEnabled { get => controlsEnabled; set { controlsEnabled = value; RaisePropertyChanged(); } }
    }
}
