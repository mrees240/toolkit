﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using TMTool.Commands;
using TMTool.ViewModel.Rendering;

namespace TMTool.ViewModel
{
    public class TexturePanelViewModel : ViewModelBase
    {
        private ObservableCollection<TextureFileViewModel> textures;

        public TexturePanelViewModel()
        {
            textures = new ObservableCollection<TextureFileViewModel>();
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            SelectAllTextureFilesCommand = new RelayCommand(() =>
            {
                Parallel.ForEach(textures, (t) =>
                {
                    t.Selected = true;
                });
            });
            DeselectAllTextureFilesCommand = new RelayCommand(() =>
            {
                Parallel.ForEach(textures, (t) =>
                {
                    t.Selected = false;
                });
            });
        }

        public ObservableCollection<TextureFileViewModel> Textures { get => textures; set { textures = value; RaisePropertyChanged(); } }

        public RelayCommand DeselectAllTextureFilesCommand
        {
            get;
            private set;
        }

        public RelayCommand SelectAllTextureFilesCommand
        {
            get;
            private set;
        }

        private ICommand exportMultipleTextureFilesCommand;
        private ICommand replaceTextureCommand;
        private ICommand replaceMultipleTexturesCommand;
        public ICommand ExportMultipleTextureFilesCommand => exportMultipleTextureFilesCommand ?? (exportMultipleTextureFilesCommand = new ExportMultipleTextureFilesCommand());
        public ICommand ReplaceTextureCommand => replaceTextureCommand ?? (replaceTextureCommand = new ReplaceTextureCommand());
        public ICommand ReplaceMultipleTexturesCommand => replaceMultipleTexturesCommand ?? (replaceMultipleTexturesCommand = new ReplaceMultipleTexturesCommand());
    }
}
