﻿using System.Drawing;
using System.Numerics;
using Microsoft.Xna.Framework;

namespace TMTool.Render
{
    public class RenderUtility
    {
        public static System.Drawing.Bitmap CreateTextBitmap(string text, float size, Brush color, int width, int height)
        {
            var bmp = new System.Drawing.Bitmap(width, height);

            var rectf = new System.Drawing.RectangleF(0, 0, width, height);

            var g = System.Drawing.Graphics.FromImage(bmp);

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.DrawString(text, new Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, size, FontStyle.Regular), color, rectf);

            g.Flush();

            return bmp;
        }
    }
}
