﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TMTool.Render
{
    public class RampObject : GenericSceneObject
    {
        private Random rng;

        public RampObject()
        {
            rng = new Random();
            meshes = new List<Mesh>();
            previousMeshes = new List<Mesh>();
            drawState = new DrawState();
            drawState.TwoSides = true;
            drawState.Shadeless = true;
        }

        private System.Drawing.Color RngColor()
        {
            int r = (int)(byte.MaxValue * rng.NextDouble());
            int g = (int)(byte.MaxValue * rng.NextDouble());
            int b = (int)(byte.MaxValue * rng.NextDouble());

            return System.Drawing.Color.FromArgb(r, g, b);
        }

        public void UpdateMesh(Mesh mesh)
        {
            previousMeshes.AddRange(meshes);
            meshes.Clear();
            meshes.Add(mesh);
            foreach (var vert in mesh.Vertices)
            {
                vert.DiffuseColor = RngColor();
            }
        }

        public void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.DiffuseOnlyShader;

            if (previousMeshes.Any())
            {
                drawState.MeshBufferNeedsUpdated = true;
            }
        }
    }
}
