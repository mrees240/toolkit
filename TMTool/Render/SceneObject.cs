﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMLibrary.Rendering;
using TMTool.ViewModel;
using TMTool.ViewModel.Rendering;

namespace TMTool.Render
{
    public class SceneObject : GenericSceneObject
    {
        /*private DrawState drawState;
        private List<Mesh> previousMeshes;
        private List<Mesh> meshes;
        private List<IDrawableSceneObject> children;
        private List<Light> internalLights;
        private Matrix4x4 matrix;*/
        private bool isAnimation;
        private List<Mesh> animationMeshes;
        //private TmToolMainViewModel mainVm;
        private int animationFrame;
        private int msSinceAnimationUpdate;
        private const int animationUpdateIntervalMs = 100;
        private EntityViewModel owner;
        private TmToolMainViewModel mainVm;
        private Transformation transformation;
        private SceneObjectState objectState;

        public SceneObject(EntityViewModel owner, TmToolMainViewModel mainVm) : base()
        {
            this.mainVm = mainVm;
            this.owner = owner;
            animationMeshes = new List<Mesh>();
            isAnimation = owner.Entity.Instruction.IsAnimationContainer;
        }

        public void SetMeshes(List<GameMesh> gameMeshes)
        {
            var meshes = new List<Mesh>();

            foreach (var mesh in gameMeshes)
            {
                meshes.Add(mesh.GetBaseMesh());
            }
            SetMeshes(meshes);
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            if (objectState == null)
            {
                objectState = GetObjectState();
            }
            if (transformation == null)
            {
                transformation = objectState.Transformation;
            }
            if (GetObjectState().Name == "Mesh #147")
            {
                GetDrawState().Color = System.Drawing.Color.Red;
                GetDrawState().IsSolidColor = true;
            }
            //transformation.SetLocalPosition(owner.Entity.Instruction.MatrixWrapper.Matrix.Translation);
            matrixStack = Matrix4x4.Multiply(matrixStack, owner.Entity.Instruction.MatrixWrapper.Matrix);

            objectState.Transformation.WorldMatrix = matrixStack;
            //transformation.SetLocalMatrix();
            //objectState.Transformation.IsIndependent = true;
            //objectState.Transformation.WorldMatrix = Matrix4x4.Multiply(owner.Entity.Instruction.MatrixWrapper.Matrix, matrixStack);
            base.OnUpdate();

            //base.Update(deltaMs, matrixStack, mouseState, scene);


            if (isAnimation && mainVm.RunAnimations)
            {
                if (animationFrame >= drawableChildren.Count)
                {
                    animationFrame = 0;
                }

                for (int iChild = 0; iChild < drawableChildren.Count; iChild++)
                {
                    if (iChild == animationFrame)
                    {
                        drawableChildren[iChild].GetDrawState().IsVisible = true;
                    }
                    else
                    {
                        drawableChildren[iChild].GetDrawState().IsVisible = false;
                    }
                }
                msSinceAnimationUpdate += deltaMs;
                if (msSinceAnimationUpdate >= animationUpdateIntervalMs)
                {
                    animationFrame++;
                    msSinceAnimationUpdate -= animationUpdateIntervalMs;
                }
            }
            if (isAnimation && !mainVm.RunAnimations)
            {
                animationFrame = 0;
                msSinceAnimationUpdate = 0;

                for (int iChild = 0; iChild < drawableChildren.Count; iChild++)
                {
                    if (iChild == 0)
                    {
                        drawableChildren[iChild].GetDrawState().IsVisible = true;
                    }
                    else
                    {
                        drawableChildren[iChild].GetDrawState().IsVisible = false;
                    }
                }
            }
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.NormalShader;
            drawState.Shadeless = true;
            
            if (owner.Entity.Instruction.IsMeshLink)
            {
                drawState.ApplyDrawDistance = true;
                drawState.MinDrawDistance = owner.Entity.Instruction.MinDrawDistance;
                drawState.MaxDrawDistance = owner.Entity.Instruction.MaxDrawDistance;
            }
            if (owner.Entity.Instruction.IsMeshContainer)
            {
                if (owner.Entity.Instruction.OnlyShowLastChildInRenderer)
                {
                    if (owner.EntityChildVmList.Any())
                    {
                        var last = owner.EntityChildVmList.Last();

                        foreach (var child in owner.EntityChildVmList)
                        {
                            if (child == last)
                            {
                                owner.SetRenderVisibility(true);
                                /*foreach (var sceneObject in child.SceneObjects)
                                {
                                    sceneObject.GetDrawState().IsVisible = true;
                                }*/
                            }
                            else
                            {
                                owner.SetRenderVisibility(false);
                                /*foreach (var sceneObject in child.SceneObjects)
                                {
                                    sceneObject.GetDrawState().IsVisible = false;
                                }*/
                            }
                        }
                    }
                }
            }
        }

        public bool IsAnimation { get => isAnimation; }
    }
}
