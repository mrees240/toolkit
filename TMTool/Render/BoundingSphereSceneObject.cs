﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using TMLibrary.Lib;

namespace TMTool.Render
{
    public class BoundingSphereSceneObject : GenericSceneObject
    {
        private BoundingSphere boundingSphere;

        public BoundingSphereSceneObject(BoundingSphere boundingSphere)
        {
            this.boundingSphere = boundingSphere;
        }
    }
}
