﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TMTool.Render
{
    public class MeshSceneObject : GenericSceneObject
    {
        public MeshSceneObject(Mesh mesh)
        {
            SetMeshes(new List<Mesh>() { mesh });
        }
    }
}
