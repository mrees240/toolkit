﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using MonoEngine.Code.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMTool.ViewModel.Rendering;

namespace TMTool.Render
{
    public class RenderManagerTmTool
    {
        private static Random rng = new Random();
        private Mesh sphereMesh;
        private List<Mesh> sphereMeshList;
        // Scene objects
        private RampObject rampObject;

        private List<Dpc701BoundingSphere> sphereInstructions;
        private List<Dpc701BoundingSphere> distinctSphereInstructions;
        private List<SceneObject> sphereSceneObjects;
        private MonoEngineScene scene;

        public RenderManagerTmTool(MonoEngineScene scene)
        {
            distinctSphereInstructions = new List<Dpc701BoundingSphere>();
            this.scene = scene;
            sphereSceneObjects = new List<SceneObject>();
            LoadSphereMesh();
            InitializeSceneObjects();
            sphereInstructions = new List<Dpc701BoundingSphere>();
            scene.PreSceneUpdateOccured += Scene_PreSceneUpdateOccured;
        }

        private void Scene_PreSceneUpdateOccured(object sender, EventArgs e)
        {
            /*if (scene.ActiveMonoViewportVm != null)
            {
                sphereSceneObjects = sphereSceneObjects.OrderBy(s => Vector3.Distance(s.GetTransformation().PositionWorld,
                    scene.ActiveMonoViewportVm.Camera.Coordinates)).ToList();
            }*/
        }

        private void LoadSphereMesh()
        {
            // then create sphere scene object
            var collada = new ColladaImporter(true, true, "content/Sphere.dae");
            collada.Load();
            sphereMesh = collada.CreateSingleMesh();
            sphereMeshList = new List<Mesh>() { sphereMesh };
        }

        private void InitializeSceneObjects()
        {
            rampObject = new RampObject();
            scene.AddObject(rampObject, false);
        }

        public void ResetScene()
        {
            scene.ResetScene();
            sphereSceneObjects.Clear();
            scene.AddObject(rampObject);
        }

        public void SetSpheres(List<Dpc701BoundingSphere> sphereInstructions)
        {
            distinctSphereInstructions = new List<Dpc701BoundingSphere>();
            this.sphereInstructions = sphereInstructions.OrderBy(s => s.BoundingSphere.Radius).Reverse().ToList();

            foreach (var sphereInstruction in this.sphereInstructions)
            {
                var duplicates = this.distinctSphereInstructions.Where(s => s.BoundingSphere.Center == sphereInstruction.BoundingSphere.Center && s.BoundingSphere.Radius == sphereInstruction.Radius);

                if (!duplicates.Any())
                {
                    var red = (int)(rng.NextDouble() * 50);
                    var green = (int)(rng.NextDouble() * 50);
                    var blue = (int)(rng.NextDouble() * 50);
                    var sphereSceneObject = new BoundingSphereSceneObject(sphereInstruction.BoundingSphere);
                    //sphereSceneObject.GetDrawState().TwoSides = true;
                    //sphereSceneObject.GetDrawState().IgnoresDepthBuffer = true;
                    //sphereSceneObject.GetDrawState().PrimitiveType = Microsoft.Xna.Framework.Graphics.PrimitiveType.LineList;
                    //sphereSceneObject.GetDrawState().IgnoresDepthBuffer = true;
                    sphereSceneObject.GetDrawState().Shadeless = true;
                    sphereSceneObject.GetDrawState().IsSolidColor = true;
                    sphereSceneObject.GetDrawState().Color = System.Drawing.Color.FromArgb(255, red, green, blue);
                    //sphereSceneObject.GetDrawState().TwoSides = true;
                    sphereSceneObject.SetMeshes(sphereMeshList);
                    sphereSceneObject.GetTransformation().SetLocalScale(new Vector3(sphereInstruction.BoundingSphere.Radius * 2));
                    sphereSceneObject.GetTransformation().PositionLocal = (sphereInstruction.BoundingSphere.Center);
                    //sphereSceneObject.GetTransformation().ParentTransformation = entityVm.SceneObjects[0].GetTransformation();
                    //sphereSceneObject.GetTransformation().UpdateMatrices();
                    //entityVm.AddSceneObject(sphereSceneObject);
                    //mainVm.MonoEngine.GameScene.AddObject(sphereSceneObject);
                    //currentSceneObject.AddChild(sphereSceneObject);
                    scene.AddObject(sphereSceneObject);
                    distinctSphereInstructions.Add(sphereInstruction);
                }
            }
        }

        public List<SceneObject> SphereSceneObjects { get => sphereSceneObjects; }
        public RampObject RampObject { get => rampObject; }
    }
}
