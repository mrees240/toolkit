﻿using System.Collections.ObjectModel;
using System.Linq;
using TMLibrary.Textures;
using TMTool.ViewModel.Rendering;

namespace TMTool.Render
{
    /// <summary>
    /// Responsible for updating texture view models.
    /// </summary>
    public class TextureViewModelUtility
    {
        /*public void UpdateTexture(ObservableCollection<TextureFileViewModel> allTextures,
            ObservableCollection<TextureFileViewModel> prevTextures, TextureFileViewModel newTexture,
            bool onlyOneTexture)
        {
            int selectedMeshIndex = scene.RenderMeshes.IndexOf(scene.SelectedMesh);

            for (int iPrevTex = 0; iPrevTex < prevTextures.Count; iPrevTex++)
            {
                var prevTexture = prevTextures[iPrevTex];

                if (prevTexture.Selected || onlyOneTexture)
                {
                    prevTexture.Selected = false;
                    var textureFileController = new TextureFileController(prevTexture.TextureFile);

                    for (int iMesh = 0; iMesh < scene.RenderMeshes.Count(); iMesh++)
                    {
                        var meshVm = scene.RenderMeshes[iMesh];

                        var meshModel = meshVm.Mesh;
                        if (meshModel.Textures.Contains(prevTexture.TextureFile))
                        {
                            foreach (var polygon in meshModel.Polygons)
                            {
                                if (polygon.TextureFile == prevTexture.TextureFile)
                                {
                                    polygon.TextureFile = newTexture.TextureFile;
                                }
                            }
                        }

                        foreach (var polygon in meshVm.Polygons)
                        {
                            if (polygon.TextureFile == prevTexture.TextureFile)
                            {
                                polygon.TextureFile = newTexture.TextureFile;
                            }
                        }
                    }

                    prevTexture.UpdateTextureFile(newTexture.TextureFile, scene.Textures);
                }
            }

            if (selectedMeshIndex >= 0)
            {
                scene.SelectedMesh = scene.RenderMeshes[selectedMeshIndex];
            }
            scene.TexturesNeedUpdate = true;
            scene.MeshesNeedUpdate = true;
        }*/
    }
}
