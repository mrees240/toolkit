﻿namespace TMLibrary.Textures
{
    /// <summary>
    ///     Provides a palette and pixel coordinate buffer for an 8-bit Tim file.
    /// </summary>
    public class ColorLookUpTable
    {
        /// <summary>
        ///     The pixel buffer stores colors as two-byte values (15ARGB) within a Tim file.
        /// </summary>
        private ushort[] pixelBuffer;

        public ColorLookUpTable()
        {
            pixelBuffer = new ushort[0];
        }

        public ushort[] PixelBuffer
        {
            get => pixelBuffer;

            set => pixelBuffer = value;
        }
    }
}