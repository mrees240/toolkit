﻿namespace TMLibrary.Textures
{
    /// <summary>
    ///     The coordinate buffer determines which color appears in which pixel depending on the color's index.
    ///     Each pixel within a coordinate buffer has an index value which is used to load a color from a Color Look Up Table
    ///     (CLUT.)
    /// </summary>
    public class CoordinateBuffer
    {
        public CoordinateBuffer()
        {
            Buffer = new byte[0];
        }

        public CoordinateBuffer(int bufferLength)
        {
            Buffer = new byte[bufferLength];
        }

        public byte[] Buffer { get; set; }
    }
}