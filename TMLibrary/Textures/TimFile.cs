﻿using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using TMLibrary.Lib.Util;

namespace TMLibrary.Textures
{
    public class TimFile : IDisposable
    {
        private Bitmap bitmap;
        private BitmapSource bitmapSource;

        public TimFile(TextureArchiveFile parent, ImageBitType imageBitType)
        {
            this.ImageBitType = imageBitType;
            this.Parent = parent;
            ColorLookUpTable = new ColorLookUpTable();
            CoordinateBuffer = new CoordinateBuffer();
        }

        public void UpdateBitmaps()
        {
            if (bitmap != null)
            {
                bitmap.Dispose();
            }
            bitmapSource = ConvertToBitmapSource();
            bitmap = TextureFileUtility.BitmapFromSource(bitmapSource);
        }

        private BitmapSource ConvertToBitmapSource()
        {
            //var bitmap = new Bitmap(ActualWidth, Height, ImageBitType.PixelFormat);

            var pixelAmount = ImageBitType.BytesPerClut / 2;
            var colorList = LoadColorList();

            var winColors = new List<System.Windows.Media.Color>();

            for (int i = 0; i < byte.MaxValue; i++)
            {
                if (i < colorList.Count)
                {
                    var drawColor = colorList[i];

                    winColors.Add(System.Windows.Media.Color.FromArgb(byte.MaxValue, drawColor.R, drawColor.G, drawColor.B));
                }
            }

            var palette = new BitmapPalette(winColors);

            BitmapSource source = null;

            var windowsPixelFormat = System.Windows.Media.PixelFormats.Indexed8;

            source = BitmapSource.Create(ActualWidth, Height, 96, 96, windowsPixelFormat, palette,
                CoordinateBuffer.Buffer, ActualWidth);

            return source;
        }

        public List<Color> LoadColorList()
        {
            var colors = new List<Color>();

            foreach (var colorShort in ColorLookUpTable.PixelBuffer)
            {
                var color = TextureUtility.UShortToDrawColor(colorShort, Parent.Definition.FlipRgb);
                colors.Add(color);
            }

            return colors;
        }

        public void Dispose()
        {
            bitmap.Dispose();
        }


        /*public void UpdateBitmapSource()
        {
            Bitmap bitmap;

            if (ImageBitType.HasColorLookUpTable)
            {
                bitmap = new Bitmap(ActualWidth, Height, ImageBitType.PixelFormat);

                var pixelAmount = ImageBitType.BytesPerClut / 2;
                var paletteColors = new Color[pixelAmount];
                var textureFileController = new TextureFileController(this);

                for (var iPixel = 0; iPixel < ColorLookUpTable.PixelBuffer.Length; iPixel++)
                    paletteColors[iPixel] =
                        textureFileController.UShortToColor(ColorLookUpTable.PixelBuffer[iPixel], Definition.FlipRgb);

                var palette = new BitmapPalette(paletteColors);

                BitmapSource source = null;
                var windowsPixelFormat = PixelFormats.Indexed8;

                source = BitmapSource.Create(bitmap.Width, bitmap.Height, 96, 96, windowsPixelFormat, palette,
                    CoordinateBuffer.Buffer, bitmap.Width);

                bitmapSource = source;
            }
        }*/

        public TextureArchiveFile Parent { get; set; }

        public ushort ActualWidth { get; set; }

        public ushort Height { get; set; }

        public ushort Width { get; set; }

        public ColorLookUpTable ColorLookUpTable { get; set; }

        public CoordinateBuffer CoordinateBuffer { get; set; }

        public int ImageDataSize { get; set; }

        public short ClutCount { get; set; }

        public short PalletOrgX { get; set; }

        public short PalletOrgY { get; set; }

        public short ImageOrgX { get; set; }

        public short ImageOrgY { get; set; }

        public byte[] IdTagBuffer { get; set; }

        public bool UsesSharedClut { get; set; }

        public ImageBitType ImageBitType { get; set; }

        public int ClutSize { get; set; }

        public int BytesPerClut => ImageBitType.BytesPerClut;

        public ushort ColorsPerClut { get; set; }

        public bool Transparency { get; set; }

        public PixelFormat PixelFormat
        {
            get
            {
                return ImageBitType.PixelFormat;
            }
        }

        public Bitmap Bitmap { get { if (bitmap == null) { UpdateBitmaps(); } return bitmap; } }
        public BitmapSource BitmapSource { get { if (bitmapSource == null) { UpdateBitmaps(); } return bitmapSource; } }
    }
}
