﻿using System.Windows.Media;

namespace TMLibrary.Textures
{
    /// <summary>
    ///     Provides a template for creating different image types for textures.
    /// </summary>
    public class ImageBitType
    {
        private PixelFormat windowsPixelFormat;

        public ImageBitType(System.Drawing.Imaging.PixelFormat pixelFormat, int bytesPerClut,
            PixelFormat windowsPixelFormat,
            bool hasColorLookUpTable,
            double actualWidthToHeightRatio)
        {
            PixelFormat = pixelFormat;
            BytesPerClut = bytesPerClut;
            this.windowsPixelFormat = windowsPixelFormat;
            HasColorLookUpTable = hasColorLookUpTable;
            ActualWidthToHeightRatio = actualWidthToHeightRatio;
        }

        /// <summary>
        ///     Usesd to determine if the image type has a Color Look Up Table (CLUT.)
        ///     Images with higher bits-per-pixel do not have CLUTs.
        /// </summary>
        public bool HasColorLookUpTable { get; }

        public double ActualWidthToHeightRatio { get; }

        public System.Drawing.Imaging.PixelFormat PixelFormat { get; }

        public int BytesPerClut { get; }
    }
}