﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;
using TMLibrary.Lib.Definitions;
using Color = System.Windows.Media.Color;
using TMLibrary.Lib.Util;
using ColladaLibrary.Misc;

namespace TMLibrary.Textures
{
    public class TextureFile
    {
        private TextureArchiveFile parent;
        private BitmapFilePair bitmapPair;
        private bool transparency;
        private BitmapSource bitmapSource;

        public TextureFile(TextureArchiveFile parent, BitmapFilePair bitmapPair)
        {
            this.parent = parent;
            this.bitmapPair = bitmapPair;
        }

        public TextureFile(TextureFile prev)
        {
            TransparencyEnabled = prev.TransparencyEnabled;
            this.bitmapPair = new BitmapFilePair (prev.bitmapPair);
        }

        public BitmapSource BitmapSource
        {
            get
            {
                //UpdateBitmapSource();
                return bitmapSource;
            }
            set => bitmapSource = value;
        }

        public TextureArchiveFile Parent { get => parent; set => parent = value; }
        public BitmapFilePair BitmapFilePair { get => bitmapPair; set => bitmapPair = value; }
        public bool TransparencyEnabled { get { return bitmapPair.TransparencyEnabled; } set { bitmapPair.TransparencyEnabled = value; } }

        public int ActualWidth
        {
            get
            {
                return bitmapPair.Bitmap.Width;
            }
        }

        public int Width
        {
            get
            {
                if (PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
                {
                    return bitmapPair.Bitmap.Width / 2;
                }
                return bitmapPair.Bitmap.Width;
            }
        }

        public int Height
        {
            get
            {
                return bitmapPair.Bitmap.Height;
            }
        }

        public System.Drawing.Imaging.PixelFormat PixelFormat
        {
            get
            {
                return bitmapPair.Bitmap.PixelFormat;
            }
        }

        //public ImageBitType ImageBitType { get; set; }

        //public int ClutSize { get; set; }

        //public int BytesPerClut => ImageBitType.BytesPerClut;

        //public ushort ColorsPerClut { get; set; }

        private System.Drawing.Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();

                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        private byte[] GetPixelData(Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
            var length = bitmapData.Stride * bitmapData.Height;

            byte[] bytes = new byte[length];

            // Copy bitmap to byte[]
            Marshal.Copy(bitmapData.Scan0, bytes, 0, length);
            bitmap.UnlockBits(bitmapData);

            return bytes;
        }

        public int GetId()
        {
            return parent.TextureFiles.IndexOf(this);
        }

        /*public void UpdateBitmapSource()
        {
            //if (bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                var textureFileController = new TextureFileController(this);
                var format = textureFileController.CreateImageBitType8Bpp();
                var pixelAmount = format.BytesPerClut / 2;

                //bitmap = TextureUtility.ConvertTo8Bpp(bitmap);

                var palette = bitmap.Palette;

                var winColors = new List<System.Windows.Media.Color>();

                foreach (var drawColor in palette.Entries)
                {
                    winColors.Add(Color.FromRgb(drawColor.R, drawColor.G, drawColor.B));
                }

                var winPalette = new BitmapPalette(winColors);
                var windowsPixelFormat = PixelFormats.Indexed8;

                var getPixelBuffer = GetPixelData(bitmap);

                bitmapSource = BitmapSource.Create(bitmap.Width, bitmap.Height, 96, 96, windowsPixelFormat, winPalette,
                    getPixelBuffer, bitmap.Width);
            }
            //else
            {
                //throw new System.Exception("Unknown pixel format.");
            }
        }*/
    }
}