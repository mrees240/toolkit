﻿namespace TMLibrary.Textures
{
    public class TimHeaderFormat
    {
        private byte[] headerBytes;
        private bool transparency;
        private int bitsPerPixel;
        private bool includeColorLookupTable;

        public TimHeaderFormat(byte[] headerBytes, int bitsPerPixel, bool transparency, bool includeColorLookupTable)
        {
            this.headerBytes = headerBytes;
            this.bitsPerPixel = bitsPerPixel;
            this.transparency = transparency;
            this.includeColorLookupTable = includeColorLookupTable;
        }

        public byte[] HeaderBytes { get => headerBytes; }
        public bool Transparency { get => transparency; }
        public int BitsPerPixel { get => bitsPerPixel; }
        public bool IncludeColorLookupTable { get => includeColorLookupTable; }
    }
}
