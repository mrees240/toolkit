﻿using System.Collections.Generic;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Textures
{
    public class TextureArchiveFile
    {
        public TextureArchiveFile(TextureArchiveDefinition definition)
        {
            Definition = definition;
            TextureFiles = new List<TextureFile>();
        }

        public List<TextureFile> TextureFiles { get; }

        public TextureArchiveDefinition Definition { get; }
    }
}