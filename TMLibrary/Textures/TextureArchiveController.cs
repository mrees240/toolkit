﻿using ColladaLibrary.Misc;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using TMLibrary.Lib.Util;

namespace TMLibrary.Textures
{
    /// <summary>
    ///     Provides a controller for TPC files, including methods that access the Tim file list.
    /// </summary>
    public class TextureArchiveController
    {
        private readonly TextureArchiveFile textureArchiveFile;
        private ProgressManager progress;

        public ProgressManager Progress { get => progress; }

        public TextureArchiveController(TextureArchiveFile textureArchiveFile)
        {
            this.textureArchiveFile = textureArchiveFile;
            progress = new ProgressManager();
        }

        public void ResetTpcFile()
        {
            textureArchiveFile.TextureFiles.Clear();
        }

        public void AddTextureFile(TextureFile textureFile)
        {
            new TextureFileController(textureFile).CheckRequirements();
            textureArchiveFile.TextureFiles.Add(textureFile);
        }

        public void RemoveTimFile(TextureFile textureFile)
        {
            textureArchiveFile.TextureFiles.Remove(textureFile);
        }

        public void ReplaceTextureFile(TextureFile newFile, TextureFile prevFile)
        {
            new TextureFileController(newFile).CheckRequirements();
            newFile.TransparencyEnabled = false;
            int index = textureArchiveFile.TextureFiles.IndexOf(prevFile);
            textureArchiveFile.TextureFiles[index] = newFile;
        }

        public void WriteFile(string fileName)
        {
            WriteFile(fileName, File.Create(fileName));
        }

        public void WriteFile(string fileName, Stream stream)
        {
            progress.FileWriteProgress = 0;
            int totalTextures = textureArchiveFile.TextureFiles.Count;
            int currentTexture = 0;

            using (var writer = new BinaryWriter(stream))
            {
                WriteHeader(writer);
                foreach (var textureFile in textureArchiveFile.TextureFiles)
                {
                    var timFileController = new TextureFileController(textureFile);
                    var textureFileHeader = CreateTpcTimHeader(textureFile);
                    writer.Write(textureFileHeader[0]);
                    writer.Write(textureFileHeader[1]);

                    var timFile = timFileController.CreateTimFileFromBitmap(textureFile.BitmapFilePair.Bitmap, textureArchiveFile);
                    timFile.Transparency = textureFile.BitmapFilePair.TransparencyEnabled;

                    if (timFile.Transparency)
                    {

                    }

                    timFileController.Write(timFile, writer, true);
                    currentTexture++;
                    progress.FileWriteProgress = (currentTexture / (double) totalTextures);
                }
            }
        }

        private void ReadTextureFiles(string fileName)
        {
            using (var fileStream = File.Open(fileName, FileMode.Open))
            {
                using (var reader = new BinaryReader(fileStream))
                {
                    ReadHeader(reader);

                    if (textureArchiveFile.Definition.FirstTimAddress > 0)
                    {
                        reader.BaseStream.Position = textureArchiveFile.Definition.FirstTimAddress;
                    }
                    else
                    {
                        reader.BaseStream.Position = 12;
                        byte amount = reader.ReadByte();
                        reader.BaseStream.Position += 3;
                        reader.BaseStream.Position += amount * 4;
                    }
                    while (ReaderStillHasBytesToRead(reader))
                    {
                        var texLocation = (int)reader.BaseStream.Position;
                        var texFile = ReadFilteredTimFile(reader);

                        AddTextureFile(texFile);
                    }
                }
            }
        }

        public void ReadFile(string fileName)
        {
            if (textureArchiveFile.TextureFiles != null)
            {
                textureArchiveFile.TextureFiles.Clear();
            }
            ReadTextureFiles(fileName);
        }

        private ushort[] CreateTpcTimHeader(TextureFile timFile)
        {
            var timFileController = new TextureFileController(timFile);
            var area = timFileController.ActualArea();
            // Not sure why 544 is needed
            var header0 = (ushort)(area + 544);
            ushort header1 = 0;
            if (area >= ushort.MaxValue)
            {
                header0 = 544;
                header1 = (ushort)(area / ushort.MaxValue);
            }
            return new[] { header0, header1 };
        }

        private TextureFile ReadFilteredTimFile(BinaryReader reader)
        {
            var bmp = new Bitmap(2, 2, PixelFormat.Format8bppIndexed);
            var filePair = new BitmapFilePair("", bmp);
            var timFile = new TextureFile(textureArchiveFile, filePair);
            var timFileController = new TextureFileController(timFile);
            string ext = textureArchiveFile.Definition.FileExtension.ToLower();

            var readingFromArchiveFile = ext == "tpc" || ext == "tms";

            timFileController.ReadFromReader(reader, readingFromArchiveFile, textureArchiveFile.TextureFiles);

            return timFile;
        }

        private bool ReaderStillHasBytesToRead(BinaryReader reader)
        {
            return reader.BaseStream.Position < reader.BaseStream.Length - 4;
        }

        // Right now this only works with TPC. Needs updated to work with a texture archive definition.
        public void WriteHeader(BinaryWriter writer)
        {
            writer.Write(textureArchiveFile.Definition.FileHeader);
            WriteTimCount(writer);
        }

        // Right now this only works with TPC. Needs updated to work with a texture archive definition.
        private void ReadHeader(BinaryReader reader)
        {
            textureArchiveFile.Definition.FileHeader = reader.ReadBytes(16);
        }

        // Right now this only works with TPC. Needs updated to work with a texture archive definition.
        private void WriteTimCount(BinaryWriter writer)
        {
            writer.BaseStream.Position = 12;
            writer.Write((ushort)textureArchiveFile.TextureFiles.Count());
            writer.BaseStream.Position = 16;
        }
    }
}