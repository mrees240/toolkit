﻿using System.Drawing;

namespace TMLibrary.Textures
{
    /// <summary>
    ///     A color used by a Color Look Up Table (CLUT.)
    ///     Provides an index used by the coordinate buffer and the color itself.
    /// </summary>
    public class PaletteColor
    {
        public PaletteColor(Color color)
        {
            Color = color;
        }

        public byte Index { get; set; }

        public Color Color { get; set; }
    }
}