﻿using ColladaLibrary.Utility;
using FreeImageAPI;
using Kaliko.ImageLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TMLibrary.Lib.Util;
using TMLibrary.Textures.Exceptions;
using Color = System.Drawing.Color;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace TMLibrary.Textures
{
    /// <summary>
    ///     Provides a controller for reading and writing to Tim/Bitmap files.
    /// </summary>
    public class TextureFileController
    {
        private readonly ImageBitType[] bitTypes;
        private readonly TextureFile textureFile;

        public TextureFileController(TextureFile textureFile)
        {
            this.textureFile = textureFile;
            bitTypes = CreateBitTypes();
        }

        /// <summary>
        ///     Provides a factory for every image format needed.
        /// </summary>
        /// <returns></returns>
        private ImageBitType[] CreateBitTypes()
        {
            return new[]
            {
                CreateImageBitType8Bpp(),
                CreateImageBitType24Bpp()
            };
        }

        public ImageBitType CreateImageBitType24Bpp()
        {
            return new ImageBitType(PixelFormat.Format16bppArgb1555, 0,
                    PixelFormats.Rgb24,
                    false, 1.0);
        }
        public ImageBitType CreateImageBitType8Bpp()
        {
            return new ImageBitType(PixelFormat.Format8bppIndexed, 512,
                    PixelFormats.Indexed8,
                    true, 2.0);
        }

        /// <summary>
        ///     Converts a color to a two-byte value.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="flipRgb"></param>
        /// <param name="readingFromBmp"></param>
        /// <returns></returns>
        public ushort ColorToUShort(Color color, bool flipRgb, bool readingFromBmp)
        {
            ushort val = 0;
            var b = (byte)(color.B / 8);
            var g = (byte)(color.G / 8);
            var r = (byte)(color.R / 8);
            if (flipRgb)
            {
                b = (byte)(color.R / 8);
                r = (byte)(color.B / 8);
            }
            var rShort = (ushort)(b << 10);
            var bShort = (ushort)(g << 5);
            var gShort1 = (ushort)(r << 0);
            val = (ushort)(gShort1 | rShort | bShort);

            return val;
        }

        public bool DimensionsArePowersOfTwo()
        {
            var powersOfTwo = new int[14];

            var current = 2;
            for (var iPower = 0; iPower < powersOfTwo.Length; iPower++)
            {
                powersOfTwo[iPower] = current;
                current *= 2;
            }

            return powersOfTwo.Contains(textureFile.ActualWidth) && powersOfTwo.Contains(textureFile.Height);
        }

        public byte[] FindTimIdTagBuffer()
        {
            byte[] buffer = new byte[4];

            if (textureFile.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                if (!textureFile.TransparencyEnabled)
                {
                    buffer = textureFile.Parent.Definition.NonTransparentTimHeader.HeaderBytes;
                }
                else
                {
                    buffer = textureFile.Parent.Definition.TransparentTimHeader.HeaderBytes;
                }
            }
            else
            {
                throw new Exception($"Unrecognized pixel format found in TIM file: {textureFile.PixelFormat.ToString()}");
            }

            return buffer;
        }

        public bool MeetsMaxAreaRequirement()
        {
            var area = textureFile.ActualWidth * textureFile.Height;
            if (!textureFile.Parent.Definition.HasMaxArea || area <= textureFile.Parent.Definition.MaxArea)
            {
                return true;
            }

            return false;
        }

        public void CheckRequirements()
        {
            if (!DimensionsArePowersOfTwo() && textureFile.Parent.Definition.RequiresPowerOfTwoDimensions)
            {
                throw new PowersOfTwoException();
            }
        }

        private int FindFirstNonTransparencyIndex(List<Color> paletteList)
        {
            for (var i = 0; i < paletteList.Count(); i++)
            {
                if (!IsTransparencyColor(paletteList.ElementAt(i)))
                {
                    return i;
                }
            }
            return -1;
        }

        public void ReadFromBitmap(Bitmap bitmap)
        {
            textureFile.BitmapFilePair.Bitmap = bitmap;
        }


        public TimFile CreateTimFileFromBitmap(Bitmap bitmap, TextureArchiveFile parent)
        {
            var timFile = new TimFile(parent, CreateImageBitType8Bpp());
            timFile.ActualWidth = (ushort)bitmap.Width;
            timFile.Width = (ushort)(timFile.ActualWidth / 2);
            timFile.Height = (ushort)bitmap.Height;
            bool bitTypeFound = false;

            var ditheredBitmap = new Bitmap(bitmap);
            var bitmap8Bit = ditheredBitmap;

            if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                bitmap8Bit = TextureUtility.ConvertTo8Bit(ditheredBitmap);
            }
            else
            {
                bitmap8Bit = bitmap;
            }

            /*byte[] imageBytes = TextureUtility.ImageToByteArray(bitmap8Bit);
            byte[] imageBytes2 = TextureUtility.ImageToByteArray(ditheredBitmap);
            File.WriteAllBytes("M:\\FILE_5.bmp", imageBytes);*/

            System.Drawing.Imaging.PixelFormat pixelFormat = bitmap8Bit.PixelFormat;

            foreach (var bitType in bitTypes)
            {
                if (bitType.PixelFormat == pixelFormat)
                {
                    byte transparencyIndex = 255;
                    bitTypeFound = true;
                    timFile.ImageBitType = bitType;
                    ushort width = (ushort)(bitmap8Bit.Width);
                    ushort height = (ushort)(bitmap8Bit.Height);
                    timFile.ClutSize = bitType.BytesPerClut;
                    timFile.ColorsPerClut = (ushort)(timFile.ClutSize / sizeof(short));
                    timFile.ClutCount = 1;
                    timFile.IdTagBuffer = TimFormatIDTag();

                    //using (var winBmp = TextureUtility.ConvertTo8Bit(bitmap))
                    {
                        //stream.Write(bitmapBytes, 0, bitmapBytes.Length);

                        int originalBitmapEntriesAmount = bitmap8Bit.Palette.Entries.Count();
                        List<System.Drawing.Color> palletEntries = new List<System.Drawing.Color>();
                        List<byte> transparencyEntries = new List<byte>();
                        palletEntries.AddRange(bitmap8Bit.Palette.Entries);
                        /*for (int i = palletEntries.Count(); i < originalBitmapEntriesAmount; i++)
                        {
                            palletEntries.Add(System.Drawing.Color.Black);
                        }*/
                        int coordinateBufferArea = timFile.ActualWidth * timFile.Height;

                        byte iPixel = 0;
                        timFile.ColorLookUpTable.PixelBuffer = new ushort[256];
                        foreach (var color in palletEntries)
                        {
                            if (IsTransparencyColor(color))
                            {
                                transparencyEntries.Add(iPixel);
                            }
                            timFile.ColorLookUpTable.PixelBuffer[iPixel] = ColorToUShort(color, timFile.Parent.Definition.FlipRgb, true);
                            ++iPixel;
                        }
                        //bitmap.Dispose();
                        FillCoordinateBuffer(timFile, palletEntries, transparencyEntries, originalBitmapEntriesAmount,
                            coordinateBufferArea, timFile.Height, timFile.ActualWidth, bitmap8Bit);
                        if (originalBitmapEntriesAmount == 256 && transparencyEntries.Count() > 0)
                        {
                            if (!IsTransparencyColor(palletEntries.Last()))
                            {
                                byte firstBlackIndex = (byte)transparencyEntries.First();
                                if (firstBlackIndex >= 0)
                                {
                                    for (int iCoord = 0; iCoord < timFile.CoordinateBuffer.Buffer.Length; iCoord++)
                                    {
                                        byte index = timFile.CoordinateBuffer.Buffer[iCoord];
                                        if (index == transparencyIndex)
                                        {
                                            timFile.CoordinateBuffer.Buffer[iCoord] = firstBlackIndex;
                                        }
                                        else if (index == firstBlackIndex)
                                        {
                                            timFile.CoordinateBuffer.Buffer[iCoord] = transparencyIndex;
                                        }
                                    }
                                    ushort transparent = timFile.ColorLookUpTable.PixelBuffer[transparencyIndex];
                                    ushort nonTransparent = timFile.ColorLookUpTable.PixelBuffer[firstBlackIndex];
                                    timFile.ColorLookUpTable.PixelBuffer[transparencyIndex] = nonTransparent;
                                    timFile.ColorLookUpTable.PixelBuffer[firstBlackIndex] = transparent;
                                }
                            }
                        }
                        int entryCount = palletEntries.Count;
                        for (int iCoord = 0; iCoord < timFile.CoordinateBuffer.Buffer.Length; iCoord++)
                        {
                            var entry = timFile.CoordinateBuffer.Buffer[iCoord];

                            if (entry < entryCount)
                            {
                                if (IsTransparencyColor(palletEntries.ElementAt(entry)))
                                {
                                    timFile.CoordinateBuffer.Buffer[iCoord] = transparencyIndex;
                                }
                            }
                        }
                        timFile.ImageDataSize = timFile.CoordinateBuffer.Buffer.Length + 12;
                        break;
                    }

                }
            }
            ditheredBitmap.Dispose();
            bitmap8Bit.Dispose();
            if (!bitTypeFound)
            {
                //bitmap.Dispose();
                throw new InvalidBppException();
            }
            //bitmap.Dispose();
            return timFile;
        }

        public void ReadFromBitmapFile(string fileName)
        {
            try
            {
                fileName = fileName.Replace("%20", " ");
                if (!File.Exists(fileName))
                {
                    fileName = fileName.Replace(" ", "_");
                }
                ReadFromBitmap(new FreeImageAPI.FreeImageBitmap(fileName).ToBitmap());
            }
            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException($"Unable to find file: {fileName}\n");
            }
            catch (InvalidBppException bbpEx)
            {
                throw new Exception($"Unable to read texture file: {fileName}. Make sure it is 8 bit-per-pixel.\n");
            }
            /*catch (Exception ex)
            {
                throw new Exception($"Unable to read texture file: {fileName}\n");
            }*/

        }

        public void ReadFromExternalTimFile(string fileName, List<TextureFile> timFiles)
        {
            using (var reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                ReadFromReader(reader, false, timFiles);
            }
        }

        public void Write(TimFile timFile, BinaryWriter writer, bool writingToTpc)
        {
            WriteTimFormatIDTag(writer);
            WriteBitsPerPixelHeader(timFile, writer);
            if (timFile.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                WriteClutSizeHeader(timFile, writer);
                WritePalletOrgHeader(timFile, writer);
                WriteColorsPerClutHeader(timFile, writer);
                WriteClutCountHeader(timFile, writer);
                WriteClutData(timFile, writer, writingToTpc);
                WriteImageDataSizeHeader(timFile, writer);
                WriteImageOrgHeader(timFile, writer);
                WriteImageDimensionsHeader(timFile, writer);
                WriteImageData(timFile, writer);
            }
        }

        public void ReadFromReader(BinaryReader reader, bool readingFromTpcFile, List<TextureFile> timFiles)
        {
            /*if (textureFile.Parent.Definition.SharesClut)
            {
                textureFile.Transparency = true;
                if (readingFromTpcFile)
                {
                    SkipTpcTimHeader(reader);
                }
                ReadTimFormatIDTag(reader);
                ReadBitsPerPixelHeader(reader);
                if (textureFile.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    if (timFiles.Count() > 0)
                    {
                        var paletteTexture = timFiles.First();
                        textureFile.ColorLookUpTable = paletteTexture.ColorLookUpTable;
                        textureFile.ColorsPerClut = paletteTexture.ColorsPerClut;
                        textureFile.ClutCount = paletteTexture.ClutCount;
                        ReadClutSizeHeader(reader);
                        ReadPalletOrgHeader(reader);
                        ReadImageDimensionsHeader(reader);
                        textureFile.ImageDataSize = textureFile.ActualWidth * textureFile.Height * sizeof(ushort);
                        ReadImageData(reader, textureFile.Definition.FlipRgb);
                    }
                    else
                    {
                        ReadClutSizeHeader(reader);
                        ReadPalletOrgHeader(reader);
                        ReadColorsPerClutHeader(reader);
                        ReadClutCountHeader(reader);
                        ReadClutData(reader, readingFromTpcFile);
                        ReadImageDataSizeHeader(reader);
                        ReadImageOrgHeader(reader);
                        ReadImageDimensionsHeader(reader);
                        ReadImageData(reader, textureFile.Definition.FlipRgb);
                    }
                }
            }
            else*/
            {
                var timFile = new TimFile(textureFile.Parent, CreateImageBitType8Bpp());

                if (readingFromTpcFile)
                {
                    SkipTpcTimHeader(reader);
                    ReadTimFormatIDTag(reader);
                }
                ReadBitsPerPixelHeader(timFile, reader);

                if (textureFile.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    ReadClutSizeHeader(timFile, reader);
                    ReadPalletOrgHeader(timFile, reader);
                    ReadColorsPerClutHeader(timFile, reader);
                    ReadClutCountHeader(timFile, reader);
                    ReadClutData(timFile, reader, readingFromTpcFile);
                    ReadImageDataSizeHeader(timFile, reader);
                    ReadImageOrgHeader(timFile, reader);
                    ReadImageDimensionsHeader(timFile, reader);
                    ReadImageData(timFile, reader, timFile.Parent.Definition.FlipRgb);
                }
                else
                {
                    throw new Exception("Unrecognized pixel format.");
                }
                timFile.UpdateBitmaps();
                textureFile.TransparencyEnabled = timFile.Transparency;
                textureFile.BitmapFilePair.Bitmap = timFile.Bitmap;
                textureFile.BitmapSource = timFile.BitmapSource;
                //textureFile.Bitmap = timFile.Bitmap;//timFile.ConvertToBitmap(PixelFormat.Format8bppIndexed);
            }
            //UpdateBitmap();
        }

        public void SwapColorEntries(TimFile timFile, int index0, int index1, List<Color> paletteEntries)
        {
            var color0 = timFile.ColorLookUpTable.PixelBuffer.ElementAt(index0);
            var color1 = timFile.ColorLookUpTable.PixelBuffer.ElementAt(index1);
            var colorEntry0 = paletteEntries.ElementAt(index0);
            var colorEntry1 = paletteEntries.ElementAt(index1);

            for (var i = 0; i < timFile.CoordinateBuffer.Buffer.Length; i++)
            {
                var val = timFile.CoordinateBuffer.Buffer[i];

                if (val == index0)
                {
                    timFile.CoordinateBuffer.Buffer[i] = (byte)index0;
                }
                else if (val == index1)
                {
                    timFile.CoordinateBuffer.Buffer[i] = (byte)index1;
                }
            }

            paletteEntries[index0] = colorEntry1;
            paletteEntries[index1] = colorEntry0;
            timFile.ColorLookUpTable.PixelBuffer[index0] = color1;
            timFile.ColorLookUpTable.PixelBuffer[index1] = color0;
        }

        private void SkipTpcTimHeader(BinaryReader reader)
        {
            reader.ReadBytes(4);
        }


        private bool IsTransparencyColor(Color color)
        {
            return
                color.R == textureFile.Parent.Definition.TransparencyColor.R &&
                color.G == textureFile.Parent.Definition.TransparencyColor.G &&
                color.B == textureFile.Parent.Definition.TransparencyColor.B;
        }

        private void ReadImageData(TimFile timFile, BinaryReader reader, bool flipRgb)
        {
            timFile.CoordinateBuffer.Buffer = reader.ReadBytes(timFile.ActualWidth * timFile.Height);
        }

        private void WriteColorsPerClutHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.ColorsPerClut);
        }

        private void ReadColorsPerClutHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ColorsPerClut = reader.ReadUInt16();
        }

        private void FillCoordinateBuffer(TimFile timFile, List<Color> palletEntries, List<byte> blackEntryIndices,
            int originalBitmapEntriesAmount, int actualArea, int height, int actualWidth, Bitmap bitmap)
        {
            timFile.CoordinateBuffer = new CoordinateBuffer(actualArea);

            //byte[] bitmapBytes = TextureUtility.ImageToByteArray(bitmap);

            //var stream = new MemoryStream(bitmapBytes);

            //using (var bmp = FreeImage.)
            {

                //using (var reader = new BinaryReader(stream))
                {
                    var bitmapHeaderLength = 54;
                    var coordBufferStart = 4 * originalBitmapEntriesAmount + bitmapHeaderLength;
                    //reader.BaseStream.Position = coordBufferStart;
                    var remainingBytes = actualArea;
                    timFile.CoordinateBuffer.Buffer = new byte[remainingBytes];

                    for (var iCoordAxis = 0; iCoordAxis < timFile.CoordinateBuffer.Buffer.Length; iCoordAxis++)
                    {
                        //var nextCoord = reader.ReadByte();
                        var row = height - iCoordAxis / actualWidth - 1;
                        var col = iCoordAxis % actualWidth;
                        var index = row * actualWidth + col;
                        var color = bitmap.GetPixel(col, row);
                        int colIndex = palletEntries.IndexOf(color);
                        timFile.CoordinateBuffer.Buffer[index] = (byte)colIndex;
                        //timFile.CoordinateBuffer.Buffer[index] = nextCoord;*/
                    }
                }
            }
            //stream.Dispose();
        }

        private void WriteImageData(TimFile timFile, BinaryWriter writer)
        {
            if (timFile.ImageBitType.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                for (var iCoordinate = 0; iCoordinate < timFile.CoordinateBuffer.Buffer.Length; iCoordinate++)
                {
                    writer.Write(timFile.CoordinateBuffer.Buffer[iCoordinate]);
                }
            }
            else
            {
                throw new Exception("Pixel format not found.");
            }
        }

        private void WriteColor(Color color, BinaryWriter writer, bool flipRgb)
        {
            writer.Write(ColorToUShort(color, flipRgb, false));
        }

        private void WriteClutData(TimFile timFile, BinaryWriter writer, bool writingToTpc)
        {
            for (var iPixelBuffer = 0; iPixelBuffer < timFile.ColorLookUpTable.PixelBuffer.Length; iPixelBuffer++)
            {
                if (writingToTpc)
                {
                    writer.Write(ReverseRgbPixel(timFile.ColorLookUpTable.PixelBuffer[iPixelBuffer]));
                }
                else
                {
                    writer.Write(timFile.ColorLookUpTable.PixelBuffer[iPixelBuffer]);
                }
            }
        }

        private void ReadClutData(TimFile timFile, BinaryReader reader, bool readingFromTpc)
        {
            // Ex. 8 BPP, 256 Colors per CLUT, 512 bytes per CLUT.
            ReadClutBuffer(reader, timFile, readingFromTpc);
        }

        private void WriteImageDataSizeHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.ImageDataSize);
        }

        private void ReadImageDataSizeHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ImageDataSize = reader.ReadInt32();
        }

        private void WriteClutSizeHeader(TimFile timFile, BinaryWriter writer)
        {
            var clutSizeWrite = timFile.ClutSize + 12;
            writer.Write(clutSizeWrite);
        }

        private void ReadClutSizeHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ClutSize = reader.ReadInt32() - 12;
        }

        private void ReadImageSizeHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ImageDataSize = reader.ReadInt32() - 12;
        }

        private void WriteClutCountHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.ClutCount);
        }

        private void ReadClutCountHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ClutCount = reader.ReadInt16();
        }

        private void WriteImageOrgHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.ImageOrgX);
            writer.Write(timFile.ImageOrgY);
        }

        private void ReadImageOrgHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.ImageOrgX = reader.ReadInt16();
            timFile.ImageOrgY = reader.ReadInt16();
        }

        private void WritePalletOrgHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.PalletOrgX);
            writer.Write(timFile.PalletOrgY);
        }

        private void ReadPalletOrgHeader(TimFile timFile, BinaryReader reader)
        {
            timFile.PalletOrgX = reader.ReadInt16();
            timFile.PalletOrgY = reader.ReadInt16();
        }

        private void WriteImageDimensionsHeader(TimFile timFile, BinaryWriter writer)
        {
            writer.Write(timFile.Width);
            writer.Write(timFile.Height);
        }

        private void UpdateDimensions(ushort actualWidth, ushort height)
        {
            textureFile.BitmapFilePair.Bitmap = new Bitmap(textureFile.BitmapFilePair.Bitmap, actualWidth, height);
        }

        private void ReadImageDimensionsHeader(TimFile timFile, BinaryReader reader)
        {
            var width = reader.ReadUInt16();
            var height = reader.ReadUInt16();
            timFile.Width = width;
            timFile.Height = height;
            timFile.ActualWidth = (ushort)(timFile.ImageBitType.ActualWidthToHeightRatio * width);
        }

        public int Area()
        {
            return textureFile.Width * textureFile.Height;
        }

        public int ActualArea()
        {
            return textureFile.ActualWidth * textureFile.Height;
        }

        private byte FindPixelColumn(short coordinateBuffer)
        {
            var rightHalfByte = (byte)(coordinateBuffer & 15);
            return rightHalfByte;
        }

        private byte FindPixelRow(short coordinateBuffer)
        {
            var leftHalfByte = (byte)((coordinateBuffer & 240) >> 4);
            return leftHalfByte;
        }

        /*public byte[] TimHeader8BppTransparent()
        {
            return new byte[]
            {
                0x19, 0x00, 0x00, 0x00
            };
        }*/

        /// <summary>
        ///     Used by TIM files where one CLUT table is shared by all the images in an archive.
        ///     When this header is read, the reader will only read the coordinate buffer and not the CLUT.
        /// </summary>
        /// <returns></returns>
        public byte[] TimHeaderCoordsOnly()
        {
            return new byte[]
            {
                0x01, 0x00, 0x00, 0x00
            };
        }

        /*public byte[] TimHeader8Bpp()
        {
            return new byte[]
            {
                0x09, 0x00, 0x00, 0x00
            };
        }*/

        // Used in JetMoto
        public byte[] TimHeader8Bpp2()
        {
            return new byte[]
            {
                217, 0x00, 0x00, 0x00
            };
        }

        // Used in JetMoto
        public byte[] TimHeader8Bpp3()
        {
            return new byte[]
            {
                89, 0x00, 0x00, 0x00
            };
        }

        public byte[] TimFormatIDTag()
        {
            return new byte[]
            {
                0x10, 0x00, 0x00, 0x00
            };
        }

        private void WriteTimFormatIDTag(BinaryWriter writer)
        {
            writer.Write(TimFormatIDTag());
        }

        private void ReadTimFormatIDTag(BinaryReader reader)
        {
            var idTagBuffer = reader.ReadBytes(4);

            if (!idTagBuffer.SequenceEqual(TimFormatIDTag()))
            {
                throw new TimFileParseException(string.Format("Error reading format ID tag at {0}.",
                    reader.BaseStream.Position - 4));
            }
        }

        private void WriteBitsPerPixelHeader(TimFile timFile, BinaryWriter writer)
        {
            if (timFile.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                if (textureFile.TransparencyEnabled)
                {
                    writer.Write(timFile.Parent.Definition.TransparentTimHeader.HeaderBytes);
                }
                else
                {
                    writer.Write(timFile.Parent.Definition.NonTransparentTimHeader.HeaderBytes);
                }
            }
            else
            {
                throw new Exception("Unknown pixel format.");
            }
        }

        private void ReadBitsPerPixelHeader(TimFile timFile, BinaryReader reader)
        {
            var bitsPerPixelHeader = reader.ReadBytes(4);
            var header = BitConverter.ToUInt32(bitsPerPixelHeader, 0);
            //if (model.Definition.ContainsTimHeader(bitsPerPixelHeader))
            {

                //textureFile.ImageBitType = bitTypes[0];
                if (bitsPerPixelHeader.SequenceEqual(timFile.Parent.Definition.TransparentTimHeader.HeaderBytes))
                {
                    timFile.Transparency = true;
                }
            }
            /*else
            {
                throw new InvalidBppException();
            }*/
        }

        public void WriteTimFile(TimFile timFile, string fileName)
        {
            using (var outStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(File.OpenWrite(fileName)))
                {
                    Write(timFile, writer, false);
                }
            }
        }

        /*public void UpdateBitmap()
        {
            using (var stream = new MemoryStream())
            {
                model.Bitmap = BitmapFromSource(stream);
                //bitmap.Save(stream, ImageFormat.Bmp);
                //model.Bitmap = bitmap;
            }
        }*/

        public void WritePng(Stream stream, bool flipRgb)
        {
            var prevStream = new MemoryStream();
            BitmapEncoder enc = new BmpBitmapEncoder();
            //textureFile.UpdateBitmapSource();
            enc.Frames.Add(BitmapFrame.Create(textureFile.BitmapSource));
            enc.Save(prevStream);
            var bitmap = new Bitmap(prevStream).Clone(
                new Rectangle(0, 0, textureFile.ActualWidth, textureFile.Height),
                PixelFormat.Format8bppIndexed);
            bitmap.Save(stream, ImageFormat.Bmp);

            bitmap.Dispose();
        }

        // S	Blue	Green	Red	
        // 15	14	10	9	5	4	0	
        private ushort ReverseRgbPixel(ushort pixelBufferValue)
        {
            var r = (ushort)((pixelBufferValue & 31) << 3);
            var g = (ushort)(((pixelBufferValue >> 5) & 31) << 3);
            var b = (ushort)(((pixelBufferValue >> 10) & 31) << 3);

            var redBuffer = r;
            r = b;
            b = redBuffer;

            var flippedBufferValue = (ushort)((r << 7) | (g << 2) | (b << 12) | (b >> 3));
            return flippedBufferValue;
        }

        public void ReadClutBuffer(BinaryReader reader, TimFile timFile, bool readingFromTpc)
        {
            ReadClutPixels(GetBytesPerClut(timFile.ImageBitType.PixelFormat), timFile, reader, readingFromTpc);
        }


        public int GetBytesPerClut(PixelFormat pixelFormat)
        {
            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    return 512;
            }
            return 0;
        }

        private void ReadClutPixels(int bytesPerClut, TimFile timFile, BinaryReader reader, bool readingFromTpc)
        {
            var pixelAmount = timFile.ClutSize / sizeof(ushort);
            timFile.ColorLookUpTable.PixelBuffer = new ushort[pixelAmount];

            for (var iPixel = 0; iPixel < timFile.ColorLookUpTable.PixelBuffer.Length; iPixel++)
            {
                timFile.ColorLookUpTable.PixelBuffer[iPixel] = reader.ReadUInt16();
                if (readingFromTpc)
                {
                    timFile.ColorLookUpTable.PixelBuffer[iPixel] =
                        ReverseRgbPixel(timFile.ColorLookUpTable.PixelBuffer[iPixel]);
                }
            }
        }
    }
}