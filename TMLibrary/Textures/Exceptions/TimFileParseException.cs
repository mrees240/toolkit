﻿using System;

namespace TMLibrary.Textures.Exceptions
{
    /// <summary>
    ///     Provides exception handling when reading from a Tim file.
    /// </summary>
    public class TimFileParseException : Exception
    {
        public TimFileParseException(string message) : base(message)
        {
        }
    }
}