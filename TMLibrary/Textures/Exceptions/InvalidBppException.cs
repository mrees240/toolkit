﻿using System;

namespace TMLibrary.Textures.Exceptions
{
    /// <summary>
    ///     An exception that is used to ensure that the Tim file being read has 8 bits-per-pixel, which is required by the
    ///     Twisted Metal engine.
    /// </summary>
    public class InvalidBppException : Exception
    {
        public InvalidBppException() : base("Error opening image. Unrecognized bits-per-pixel format.")
        {
        }
    }
}