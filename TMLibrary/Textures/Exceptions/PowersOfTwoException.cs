﻿using System;

namespace TMLibrary.Textures.Exceptions
{
    public class PowersOfTwoException : Exception
    {
        public PowersOfTwoException() : base(
            "Texture dimensions must be powers of two. (8, 16, 32, 64, 128, 256, 512, 1024...)")
        {
        }
    }
}