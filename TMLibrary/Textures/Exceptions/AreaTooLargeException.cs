﻿using System;

namespace TMLibrary.Textures.Exceptions
{
    public class AreaTooLargeException : Exception
    {
        public AreaTooLargeException(int maxArea) : base(
            $"The texture's area is too big. The game engine can only use textures with an area up to {maxArea}.")
        {
        }
    }
}