﻿using System;

namespace TMLibrary.Textures.Exceptions
{
    public class MissingFileNameException : Exception
    {
        public MissingFileNameException() : base("A texture is missing its file name.")
        {
        }
    }
}