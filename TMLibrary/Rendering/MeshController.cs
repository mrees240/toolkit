﻿using ColladaLibrary.Render;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Textures;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     Provides general methods for reading meshes.
    /// </summary>
    public class MeshController
    {
        private GameMesh gameMesh;
        private BinaryReader reader;
        private ProjectDefinition project;

        public MeshController(GameMesh gameMesh, ProjectDefinition project)
        {
            this.gameMesh = gameMesh;
            this.project = project;
        }

        /// <summary>
        /// Converts a mesh from the Collada library to the game mesh format.
        /// </summary>
        /// <param name="colladaMesh"></param>
        public void ReadFromColladaMesh(ColladaLibrary.Render.Mesh colladaMesh, bool defaultLighting)
        {
            int indexAmount = colladaMesh.GetPolygonIndices().Select(i => i.Id).Max();
            var bitmaps = colladaMesh.GetDiffuseList();
            var bitmapPairs = colladaMesh.GetTextures();

            var textures = new List<TextureFile>();

            // Add textures
            for (int iTex = 0; iTex < bitmaps.Count(); iTex++)
            {
                var textureFile = new TextureFile(project.TextureArchiveFile, bitmapPairs[iTex].Diffuse);
                textures.Add(textureFile);
            }
            // Convert vertices.
            foreach (var vertex in colladaMesh.Vertices)
            {
                gameMesh.Vertices.Add(new ColladaLibrary.Render.Vertex(vertex.Vector));
            }
            // Convert polygons
            for (int iPoly = 0; iPoly < colladaMesh.PolygonAmount; iPoly++)
            {
                var polygon = colladaMesh.GetPolygon(iPoly);
                var gamePolygon = new GamePolygon(project, defaultLighting);
                int currentIndexAmount = polygon.GetIndexAmount();

                var polygonIndices = new List<PolygonIndex>();

                for (int iIndex = 0; iIndex < currentIndexAmount; iIndex++)
                {
                    //var prevTexCoord = polygon.GetPolygonIndex(iIndex).TexCoord;

                    //var prevPoly = polygon.GetPolygonIndex(iIndex);


                    var id = polygon.GetPolygonIndex(iIndex).Id;
                    var coord = polygon.GetPolygonIndex(iIndex).TexCoord;
                    var color = polygon.GetPolygonIndex(iIndex).DiffuseColor;
                    var polygonIndex = new PolygonIndex(id, color, coord);
                    polygonIndices.Add(polygonIndex);
                    //gamePolygon.GetPolygonIndex(iIndex).TexCoord = new ColladaLibrary.Rendering.TextureCoordinate(prevTexCoord.U, prevTexCoord.V);
                    //gamePolygon.GetPolygonIndex(iIndex).DiffuseColor = polygon.GetPolygonIndex(iIndex).DiffuseColor;
                }

                if (polygon.GetIndexAmount() == 3)
                {
                    gamePolygon = new GamePolygon(project, polygonIndices[0], polygonIndices[1], polygonIndices[2]);

                }
                else
                {
                    gamePolygon = new GamePolygon(project, polygonIndices[0], polygonIndices[1], polygonIndices[2], polygonIndices[3]);
                }

                // Set texture
                if (polygon.Textures.Diffuse.Bitmap != null)
                {
                    int texId = bitmaps.IndexOf(polygon.Textures.Diffuse.Bitmap);
                    var textureFile = textures[texId];

                    if (textureFile != null)
                    {
                        gamePolygon.TextureFile = textureFile;
                    }
                }

                gameMesh.OriginalPolygons.Add(gamePolygon);
            }
        }

        public void ReadMesh(BinaryReader reader, GameMesh mesh, int polygonAmount)
        {
            this.gameMesh = mesh;
            this.gameMesh.WrittenToFile = true;
            var polyController = mesh.ParentFile.Definition.PolygonController;

            reader.BaseStream.Position = mesh.MeshHeader.PolygonStartAddress;
            for (int iPolygon = 0; iPolygon < polygonAmount; iPolygon++)
            {
                var polygon = polyController.Read(reader, mesh);

                if (polygon != null)
                {
                    polygon.Parent = mesh;
                    this.gameMesh.OriginalPolygons.Add(polygon);
                }
                else
                {
                    break;
                }
            }
            // TEST
            //mesh.Polygons = mesh.Polygons.OrderBy(p => p.PolySort0).ToList();
        }

        public void ReadVertexPositions(BinaryReader reader, GameMesh mesh)
        {
            this.gameMesh = mesh;


            if (this.gameMesh.OriginalPolygons.Count() > 0)
            {
                var vertexStride = this.gameMesh.ParentFile.Definition.VertexStride;
                var indices = gameMesh.OriginalPolygons.SelectMany(p => p.GetIndices());
                int maxId = indices.Max(i => i.Id);
                reader.BaseStream.Position = mesh.MeshHeader.VertexStartAddress;

                for (int iVert = 0; iVert <= maxId; iVert++)
                {
                    this.gameMesh.VertexPositions.Add(ReadVertexPosition(reader));
                }
                /*var billboardPolys = this.gameMesh.Polygons.Where(p => p.RenderFlag != RenderFlag.Normal);
                var normalPolys = this.gameMesh.Polygons.Where(p => p.RenderFlag == RenderFlag.Normal);
                int maxIndex = 0;
                int maxBillboardIndex = 0;
                if (normalPolys.Count() > 0)
                {
                    maxIndex = normalPolys.SelectMany(p => p.GetIndices()).Select(i => i.Id).Max();
                }
                if (billboardPolys.Count() > 0)
                {
                    maxBillboardIndex = billboardPolys.Select(p => p.GetPolygonIndex(0)).Select(i => i.Id).Max();

                    if (maxBillboardIndex > maxIndex)
                    {
                        maxIndex = maxBillboardIndex;
                    }
                }
                var vertices = new List<ColladaLibrary.Render.Vertex>();
                for (int i = 0; i < maxIndex + 1; i++)
                {
                    vertices.Add(null);
                }

                foreach (var normalPoly in normalPolys)
                {
                    var indices = normalPoly.GetIndices();

                    foreach (var index in indices)
                    {
                        reader.BaseStream.Position = mesh.MeshHeader.VertexStartAddress + vertexStride * index.Id;
                        vertices[index.Id] = ReadVertex(reader);
                    }
                }
                foreach (var billboardPoly in billboardPolys)
                {
                    int vertexPositionIndex = billboardPoly.GetPolygonIndex(0).Id;
                    reader.BaseStream.Position = mesh.MeshHeader.VertexStartAddress + vertexStride * vertexPositionIndex;
                    vertices[vertexPositionIndex] = ReadVertex(reader);
                }

                this.gameMesh.Vertices.Clear();
                this.gameMesh.Vertices.AddRange(vertices);*/
            }
        }

        private Vector3 ReadVertexPosition(BinaryReader reader)
        {
            var start = reader.BaseStream.Position;
            var x = reader.ReadInt16();
            var y = reader.ReadInt16();
            var z = reader.ReadInt16();
            reader.ReadUInt16();

            var vertexPosition = new System.Numerics.Vector3(x, y, z);
            return vertexPosition;
        }
    }
}