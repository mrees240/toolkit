﻿using System.Collections.Generic;
using System.Linq;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Rendering
{
    public class PolygonGroupController
    {
        private GameMesh mesh;
        private PolygonGroup polygonGroup;

        /// <summary>
        ///     Creates the buffers required by the 3D renderer.
        /// </summary>
        public void InitializePolygonGroup(PolygonGroup polygonGroup, GameMesh mesh)
        {
            this.polygonGroup = polygonGroup;
            this.mesh = mesh;
        }

        private void AssignPolygonType(GamePolygon polygon, ProjectDefinition project)
        {
            if (polygon.IndexAmount == 4)
            {
                if (polygon.TextureFile == null)
                {
                    polygon.PolygonType = project.PolygonTypeDictionary[0x8090100];
                    polygon.RenderFlagId = 4516;
                }
                else
                {
                    polygon.PolygonType = project.PolygonTypeDictionary[0x90D0104];
                    polygon.RenderFlagId = 5772;
                }
            }
            else if (polygon.IndexAmount == 3)
            {
                if (polygon.TextureFile == null)
                {
                    polygon.PolygonType = project.PolygonTypeDictionary[0x4070100];
                    polygon.RenderFlagId = 4419;
                }
                else
                {
                    polygon.PolygonType = project.PolygonTypeDictionary[0x90E0303];
                    polygon.RenderFlagId = 14283;
                }
            }
        }

        /// <summary>
        ///     Creates all the polygon groups required for one mesh.
        /// </summary>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public PolygonGroup[] CreatePolygonGroups(GameMesh mesh, int minPolygonIndex, int polygonAmount, ProjectDefinition project)
        {
            var polygonSection = mesh.OriginalPolygons.Skip(minPolygonIndex).Take(polygonAmount);

            foreach (var poly in polygonSection)
            {
                if (poly.PolygonType == null)
                {
                    AssignPolygonType(poly, project);
                }
            }

            var valuesPerVertex = GameMesh.ValuesPerVertex;
            // + 1 is needed for non textured polygons
            var textures = mesh.Textures;
            // TEMPORARY SORTING SOLUTION
            var texturedGroupsNoDiffuse = mesh.OriginalPolygons.Where(p => p.HideDiffuse);
            var texturedGroupsWithDiffuse = mesh.OriginalPolygons.Where(p => !p.HideDiffuse);
            var texturedGroupCount = texturedGroupsNoDiffuse.Count() + texturedGroupsWithDiffuse.Count();

            var texturedGroups = new PolygonGroup[texturedGroupCount];
            var texturedBillboardGroups = new PolygonGroup[texturedGroupCount];
            var coloredGroup = new PolygonGroup();

            if (mesh.ParentFile != null && mesh.ParentFile.Parent.SkipTextures)
            {
                texturedGroups = new PolygonGroup[0];
                texturedBillboardGroups = new PolygonGroup[0];
            }

            if (mesh.OriginalPolygons.Count() > 0)
            {
                // Colored polygons
                if (mesh.ParentFile.Parent.SkipTextures)
                {
                    coloredGroup.Polygons = polygonSection.ToList();
                }
                else
                {
                    coloredGroup.Polygons = polygonSection.Where(p => p.PolygonType.ColorOnly).ToList();
                }
            }
            var coloredVertexValues = new List<float>();

            foreach (var polygon in coloredGroup.Polygons)
            {
                polygon.Parent = mesh;
                coloredVertexValues.AddRange(CreatePolygonVertices(polygon));
            }
            coloredGroup.VertexValues = coloredVertexValues;
            coloredGroup.Indices = CreateLinearIndices(coloredVertexValues.Count() / valuesPerVertex);

            for (var iGroup = 0; iGroup < texturedGroups.Length; iGroup++)
            {
                var group = new PolygonGroup();
                var billboardGroup = new PolygonGroup();
                billboardGroup.RenderFlag = RenderFlag.Billboard2;
                int noDiffuseAmount = texturedGroupsNoDiffuse.Count();

                if (mesh.Textures != null && mesh.Textures.Count() > 0)
                {
                    int textureId = 0;
                    if (iGroup < texturedGroupsNoDiffuse.Count())
                    {
                        textureId = texturedGroupsNoDiffuse.ElementAt(iGroup).TextureId;
                        group.HideDiffuse = true;
                    }
                    else
                    {
                        textureId = texturedGroupsWithDiffuse.ElementAt(iGroup - noDiffuseAmount).TextureId;
                    }

                    if (textureId >= 0)
                    {
                        var textureFile = project.TextureArchiveFile.TextureFiles.ElementAt(textureId);
                        group.TextureFile = textureFile;
                        billboardGroup.TextureFile = textureFile;
                        texturedGroups[iGroup] = group;
                        texturedBillboardGroups[iGroup] = billboardGroup;
                    }
                }


                var polygons = polygonSection
                    .Where(p => p.TextureFile == group.TextureFile && p.RenderFlag == RenderFlag.Normal).ToArray();
                var billboardPolygons = polygonSection
                    .Where(p => p.TextureFile == group.TextureFile && p.RenderFlag != RenderFlag.Normal).ToArray();

                var vertexValues = new List<float>();
                var billboardVertexValues = new List<float>();
                foreach (var polygon in polygons)
                {
                    polygon.Parent = mesh;
                    vertexValues.AddRange(CreatePolygonVertices(polygon));
                }
                foreach (var billboardPolygon in billboardPolygons)
                {
                    billboardPolygon.Parent = mesh;
                    billboardVertexValues.AddRange(CreatePolygonVertices(billboardPolygon));
                }
                group.Polygons.AddRange(polygons);
                group.VertexValues = vertexValues;
                group.Indices = CreateLinearIndices(vertexValues.Count() / valuesPerVertex);
                billboardGroup.VertexValues = billboardVertexValues;
                billboardGroup.Indices = CreateLinearIndices(billboardVertexValues.Count() / valuesPerVertex);
            }

            var groups =
                new PolygonGroup[texturedGroups.Count() + texturedBillboardGroups.Count() + 1];
            texturedGroups.CopyTo(groups, 0);
            texturedBillboardGroups.CopyTo(groups, texturedGroups.Count());
            groups[groups.Length - 1] = coloredGroup;

            return groups;
        }

        /// <summary>
        ///     Creates a list of index values in order.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private int[] CreateLinearIndices(int offset, int length)
        {
            var indices = new int[length];
            for (int i = 0; i < length; i++)
            {
                indices[i] = i + offset;
            }
            return indices;
        }

        /// <summary>
        ///     Creates a list of index values in order.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private int[] CreateLinearIndices(int length)
        {
            var indices = new int[length];
            for (int i = 0; i < length; i++)
            {
                indices[i] = i;
            }
            return indices;
        }

        /// <summary>
        ///     Gets a list of vertex counts for each polygon. Mainly used when exporting with the Collada file format.
        /// </summary>
        /// <returns></returns>
        public int[] GetVertexCountList()
        {
            var vertexCounts = new int[polygonGroup.Polygons.Count()];

            for (var iPolygon = 0; iPolygon < vertexCounts.Length; iPolygon++)
            {
                vertexCounts[iPolygon] = polygonGroup.Polygons.ElementAt(iPolygon).GetIndexAmount();
            }

            return vertexCounts;
        }

        /// <summary>
        ///     Creates an array of vertices from a polygon.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        private float[] CreatePolygonVertices(GamePolygon polygon)
        {
            int valuesPerIndex = 9;
            int indexAmount = polygon.GetIndexAmount();
            int triangleAmount = 1;
            if (indexAmount == 4)
            {
                triangleAmount = 2;
            }

            int[] localIndices = new int[] { 0, 1, 2 };
            if (triangleAmount == 2)
            {
                localIndices = new int[] { 0, 1, 2, 0, 2, 3 };
            }
            var vertices = new float[valuesPerIndex * localIndices.Length];

            //int[] quadIndices = new int[] { 0, 1, 2, 0, 2, 3 };
            //if (!polygon.PolygonType.ColorOnly && !polygon.Parent.ParentFile.Parent.SkipTextures)
            {
                //for (int iTriangle = 0; iTriangle < triangleAmount; iTriangle++)
                {
                    /*var i0 = polygon.GetPolygonIndex(0);
                    var i1 = polygon.GetPolygonIndex(1);
                    var i2 = polygon.GetPolygonIndex(2);

                    if (iTriangle == 2)
                    {
                        i0 = polygon.GetPolygonIndex(0);
                    }*/
                    var indices = polygon.GetIndices();

                    for (int iIndex = 0; iIndex < localIndices.Length; iIndex++)
                    {
                        if (indices.Min(i => i.Id) >= 0)
                        {
                            int localIndex = localIndices[iIndex];
                            var index = indices[localIndex];
                            var vertex = polygon.Parent.Vertices[index.Id];
                            vertices[iIndex * valuesPerIndex + 0] = vertex.Vector.X;
                            vertices[iIndex * valuesPerIndex + 1] = vertex.Vector.Y;
                            vertices[iIndex * valuesPerIndex + 2] = vertex.Vector.Z;
                            vertices[iIndex * valuesPerIndex + 3] = index.TexCoord.U;
                            vertices[iIndex * valuesPerIndex + 4] = index.TexCoord.V;
                            vertices[iIndex * valuesPerIndex + 5] = index.DiffuseColor.B / 255.0f;
                            vertices[iIndex * valuesPerIndex + 6] = index.DiffuseColor.G / 255.0f;
                            vertices[iIndex * valuesPerIndex + 7] = index.DiffuseColor.R / 255.0f;
                            vertices[iIndex * valuesPerIndex + 8] = index.DiffuseColor.A / 255.0f;
                        }
                    }
                    return vertices;
                }

            }
        }
    }
}