﻿using System.Collections.Generic;
using TMLibrary.Textures;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     A polygon group is a group of polygon that share the same VBO.
    ///     They are grouped by render flags and texture files in order to minimize the amount of texture binding calls.
    /// </summary>
    public class PolygonGroup
    {
        public PolygonGroup()
        {
            Polygons = new List<GamePolygon>();
            RenderFlag = RenderFlag.Normal;
            VertexValues = new List<float>();
        }

        public bool HideDiffuse { get; set; }

        public List<float> VertexValues { get; set; }

        public int[] Indices { get; set; }

        public TextureFile TextureFile { get; set; }

        public RenderFlag RenderFlag { get; set; }

        public List<GamePolygon> Polygons { get; set; }
    }
}