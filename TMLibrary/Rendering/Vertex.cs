﻿using System;
using System.Numerics;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     Defines a vertex with a texture coordinate.
    /// </summary>
    /*public class Vertex : IEquatable<Vertex>
    {
        public static readonly int ValuesPerVertex = 9;

        public Vertex()
        {
            Vector = new Vector3();
        }

        public Vertex(float x, float y, float z)
        {
            Vector = new Vector3(x, y, z);
            S = 0.0f;
            T = 0.0f;
        }

        public Vertex(Vector3 vec) : this(vec.X, vec.Y, vec.Z)
        {
        }

        public Vertex(Vector3 vec, float s, float t) : this(vec.X, vec.Y, vec.Z)
        {
            this.S = s;
            this.T = t;
        }

        public Vertex(float x, float y, float z, float s, float t) : this(x, y, z)
        {
            S = s;
            T = t;
        }

        public Vertex(Vertex clone)
        {
            Vector = clone.Vector;
            S = clone.S;
            T = clone.T;
        }

        public float S { get; set; }

        public float T { get; set; }

        public Vector3 Vector { get; set; }

        public bool Equals(Vertex other)
        {
            return Vector == other.Vector && S == other.S && T == other.T;
        }
    }*/
}
