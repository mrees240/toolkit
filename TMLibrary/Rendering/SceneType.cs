﻿namespace TMLibrary.Rendering
{
    /// <summary>
    ///     Defines which render mode the user is in.
    /// </summary>
    public enum SceneType
    {
        Mesh,
        Entity
    }
}