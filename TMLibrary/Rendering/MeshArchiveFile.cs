﻿using System.Collections.Generic;
using System.Linq;
using TMLibrary.Lib;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc.Definitions;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     Stores meshes, entities, and instructions found in a mesh archive file.
    /// </summary>
    public class MeshArchiveFile
    {
        private FileHeader fileHeader;

        public MeshArchiveFile(MeshArchiveDefinition definition, ProjectDefinition parent)
        {
            Definition = definition;
            Parent = parent;
            Meshes = new List<GameMesh>();
            Entities = new List<Entity>();
            Instructions = new List<Instruction>();
            //Pointers = new SortedSet<Pointer>();
            fileHeader = new FileHeader();
        }

        public List<GameMesh> Meshes { get; set; }

        public MeshArchiveDefinition Definition { get; }

        public ProjectDefinition Parent { get; set; }

        public List<Entity> Entities { get; set; }

        public List<Instruction> Instructions { get; set; }

        //public SortedSet<Pointer> Pointers { get; set; }
        public FileHeader FileHeader { get => fileHeader; }

        public long GetFileSize()
        {
            long size = fileHeader.Buffer.Length;

            // Root instruction pointers
            size += 4 + Instructions.Count() * 4;

            foreach (var mesh in Meshes)
            {
                size += mesh.GetBufferSize();
            }
            foreach (var instruction in Instructions)
            {
                size += instruction.GetRecursiveBufferLength();
            }

            return size;
        }

        public SortedList<long, Instruction> GetAllInstructions()
        {
            var instructions = new SortedList<long, Instruction>();

            foreach (var instruction in this.Instructions)
            {
                GetRecursiveInstructions(instruction, instructions);
            }

            return instructions;
        }

        private void GetRecursiveInstructions(Instruction instruction, SortedList<long, Instruction> instructions)
        {
            if (!instructions.ContainsKey(instruction.Address))
            {
                instructions.Add(instruction.Address, instruction);
            }
            foreach (var child in instruction.Children)
            {
                GetRecursiveInstructions(child, instructions);
            }
        }

        /// <summary>
        /// Gets all meshes that are owned by at least one instruction.
        /// </summary>
        /// <returns></returns>
        public List<GameMesh> GetSavableMeshes()
        {
            var allInstructions = GetAllInstructions();

            var meshes = new List<GameMesh>();

            foreach (var mesh in this.Meshes)
            {
                var owners = allInstructions.Where(i => i.Value.MeshList.Contains(mesh));
                if (owners.Any())
                {
                    meshes.Add(mesh);
                }
            }

            return meshes;
        }

        public List<DpcCollisionFF08> GetAllCollisionVolumes()
        {
            return GetAllInstructions().OfType<DpcCollisionFF08>().ToList();
        }
    }
}