﻿using System.IO;

namespace TMLibrary.Rendering
{
    public class MeshHeader
    {
        protected int bufferSize;
        protected GameMesh mesh;
        protected long vertexStartAddress;
        protected long polygonStartAddress;
        protected ushort polygonAmount;

        public int BufferSize { get => bufferSize; }
        public long VertexStartAddress { get => vertexStartAddress; set => vertexStartAddress = value; }
        public long PolygonStartAddress { get => polygonStartAddress; set => polygonStartAddress = value; }
        public ushort PolygonAmount { get => polygonAmount; set => polygonAmount = value; }

        public MeshHeader(GameMesh mesh)
        {
            this.mesh = mesh;
        }

        public virtual void ReadMeshHeader(BinaryReader reader)
        {

        }

        public virtual void WriteMeshHeader(BinaryWriter writer)
        {

        }
    }
}
