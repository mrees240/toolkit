﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Textures;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     Provides the main internal polygon format. All imported polygons from mesh files are converted to this format.
    /// </summary>
    public class GamePolygon : ColladaLibrary.Render.Polygon
    {
        // These fields are used as filler for storing unknown values
        private ushort[] unknownUShorts;
        private PolygonType polygonType;

        protected uint header;
        protected TextureFile textureFile;
        //protected List<Color> textureDiffuseColors;
        //protected List<int> indices;
        //private TextureCoordSet textureCoordSet;
        //protected TextureCoordinate[] texCoords;
        protected ushort renderFlagId;
        protected RenderFlag renderFlag;
        protected long address;
        protected bool hideDiffuse;
        protected ProjectDefinition project;
        protected bool defaultLighting;
        protected Polygon basePolygon;

        // Two short values that are used for determining the polygon depth sort
        // Not sure how they work yet
        private short polySort0, polySort1;

        // Used by collada importer
        private bool colladaShadless;

        public int GetPolygonAmount()
        {
            return PolygonType.IndexAmount;
        }

        /*public void UpdateSubPolygonTexCoords(int iteration)
        {
        }*/

        /*public void FixTexCoords()
        {
            TextureCoordSet.FixTexCoords();
        }*/

        /// <summary>
        /// A polygon designed for a specific game defined with a project definition.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="indexAmount"></param>
        public GamePolygon(ProjectDefinition project) : base()
        {
            //indices = new int[indexAmount];
            unknownUShorts = new ushort[0];
            this.project = project;

            //TexCoords = new TextureCoordinate[0];
            crossProduct = new Vector3();
            renderFlagId = 0;
            renderFlag = RenderFlag.Normal;
            defaultLighting = true;
        }

        public GamePolygon(ProjectDefinition project, PolygonIndex i0, PolygonIndex i1, PolygonIndex i2) : this(project)
        {
            indices[0] = i0;
            indices[1] = i1;
            indices[2] = i2;

            SetAsTriangle();
        }

        public GamePolygon(ProjectDefinition project, PolygonIndex i0, PolygonIndex i1, PolygonIndex i2, PolygonIndex i3) : this(project)
        {
            indices[0] = i0;
            indices[1] = i1;
            indices[2] = i2;
            indices[3] = i3;

            SetAsQuad();
        }

        /*public void RemoveLastIndex()
        {
            int lastIndex = indices.Length - 1;
            var newIndices = new List<PolygonIndex>(indices);
            newIndices.RemoveAt(lastIndex);
            indices = newIndices;
        }

        public void RemoveFirstIndex()
        {
            var newIndices = new List<PolygonIndex>(indices);
            newIndices.RemoveAt(0);
            indices = newIndices;
        }

        public void ReverseIndexOrder()
        {
            indices.Reverse();
        }*/

        public GamePolygon()
        {
        }

        public GamePolygon(ProjectDefinition project, bool defaultLighting) : this(project)
        {
            this.defaultLighting = defaultLighting;
        }

        public PolygonIndex[] GetTriangulatedIndices()
        {
            int indexAmount = GetIndexAmount();

            if (indexAmount == 3)
            {
                return indices;
            }
            else if (indexAmount == 4)
            {
                return new PolygonIndex[]
                {
                    indices[0],
                    indices[1],
                    indices[2],
                    indices[2],
                    indices[1],
                    indices[3],
                };
            }
            else
            {
                throw new Exception("Polygon does not have 3 or 4 indices.");
            }
        }

        /*public void SetDefaultTextureCoordinates()
        {
            textureCoordSet.SetDefaultTextureCoordinates();
        }*/

        public void CloneTo(GamePolygon polygon)
        {
            CloneTo(polygon, false);
        }

        // WILL BE REMOVED
        public void CloneTo(GamePolygon polygon, bool isExternalMesh)
        {
            polygon.polygonType = PolygonType;
            polygon.Parent = Parent;
            polygon.RenderFlag = RenderFlag;
            polygon.RenderFlagId = RenderFlagId;
            //polygon.SetDefaultDiffuseColors(indices.Count());
            polygon.PolygonType = polygonType;
            //polygon.TextureDiffuseColors = new List<Color>();
            for (int iIndex = 0; iIndex < polygon.indices.Length; iIndex++)
            {
                indices[iIndex] = polygon.indices[iIndex];
                //polygon.Indices.Add(Indices[iIndex]);
            }
            polygon.TextureFile = textureFile;
            polygon.crossProduct = crossProduct;
            polygon.Center = center;

            polygon.ColladaShadless = colladaShadless;
            polygon.polySort0 = polySort0;
            polygon.PolySort1 = PolySort1;
            polygon.UnknownUShorts = UnknownUShorts;
        }

        /*public Polygon(Polygon polygon, ProjectDefinition project) : this(project)
        {
            if (polygon.TextureFile != null)
            {
                TextureFile = polygon.TextureFile;
            }
            SetDefaultDiffuseColors(polygon.indices.Count());
            TexCoords = new List<TextureCoordinate>(polygon.TexCoords).ToArray();
            Parent = polygon.Parent;
            RenderFlag = polygon.RenderFlag;
            RenderFlagId = polygon.RenderFlagId;
            Indices = new List<int>(polygon.Indices).ToArray();
            crossProduct = polygon.crossProduct;

            for (int iTexDiffuse = 0; iTexDiffuse < indices.Length; iTexDiffuse++)
            {
                TextureDiffuseColors[iTexDiffuse] = polygon.TextureDiffuseColors[iTexDiffuse];
            }
        }*/

        public string HeaderTagHex
        {
            get
            {
                return header.ToString("X");
            }
            /*get
            {
                // May need revised
                try
                {
                    return Parent.ParentFile.Definition.PolygonDefinitions.Where(v => v.Value == Definition)
                        .FirstOrDefault().Key.ToString("X");
                }
                catch (Exception ex)
                {
                    return "-";
                }
            }*/
        }

        public int GetFinalSort0()
        {
            int sort = polySort0;
            for (int i = 0; i < 12; i++)
            {
                sort *= 2;
            }
            sort += 800;
            return sort;
        }

        public string IndicesText
        {
            get
            {
                {
                    if (indices.Count() == 3)
                    {
                        return $"{indices[0]},{indices[1]},{indices[2]}";
                    }
                    if (indices.Count() == 4)
                    {
                        return $"{indices[0]},{indices[1]},{indices[2]},{indices[3]}";
                    }
                    return "Unknown";
                }
            }
        }

        public string Info => $"{Address} - Render Flag: {renderFlagId}";

        /*protected void InitializeTextureCoordinates(int amount)
        {
            textureCoordSet.SetDefaultTextureCoordinates();
        }*/

        public TextureFile TextureFile
        {
            get => textureFile;
            set
            {
                textureFile = value;
                textureFile.BitmapFilePair = textureFile.BitmapFilePair;
            }
        }

        public int GetTextureId()
        {
            return textureFile.GetId();
        }

        public GameMesh Parent { get; set; }

        public RenderFlag RenderFlag { get; set; }

        //public int[] Indices { get => indices; set { indices = value; if (Parent.Vertices.Count() > 0 && indices.Max() >= Parent.Vertices.Count()) { throw new Exception("Index exceeded vertex count."); } } }
        //public List<int> Indices { get => indices; set { indices = value; } }
        public long Address { get => address; set => address = value; }

        public bool IsWritable()
        {
            int indexAmount = GetIndexAmount();
            return polygonType != null && indexAmount >= 3 && indexAmount <= 4;
        }

        /*public List<Color> TextureDiffuseColors
        {
            get => textureDiffuseColors;
            set
            {
                textureDiffuseColors = value;
            }
        }*/
        public ushort RenderFlagId { get => renderFlagId; set => renderFlagId = value; }

        /*public void SetDefaultDiffuseColors(int amount)
        {
            textureDiffuseColors = new Color[amount];

            for (var iTextureDiffuse = 0; iTextureDiffuse < textureDiffuseColors.Length; iTextureDiffuse++)
            {
                textureDiffuseColors[iTextureDiffuse] = Color.White;
            }
        }*/

        public void SetDiffuseColors(Color color)
        {
            foreach (var index in indices)
            {
                index.DiffuseColor = color;
            }
        }

        public void SetWhiteDiffuseColors()
        {
            SetDiffuseColors(Color.White);
        }

        public int TextureId
        {
            get
            {
                return project.TextureArchiveFile.TextureFiles.IndexOf(textureFile);
            }
        }

        public double GetArea()
        {
            return 0;
                /*
            var verts = GetVertices();
            var a = Vector3.Distance(verts[0].Vector, verts[1].Vector);
            var b = Vector3.Distance(verts[1].Vector, verts[2].Vector);
            /*var c = Vector3.Distance(verts[2].Vector, verts[0].Vector);*/

            //return a * b / 2.0;
        }

        public Vector3 CrossProduct { get => crossProduct; set => crossProduct = value; }
        public bool HideDiffuse { get => hideDiffuse; set => hideDiffuse = value; }
        public Vector3 Center { get => center; set => center = value; }
        public bool DefaultLighting { get => defaultLighting; set => defaultLighting = value; }
        public PolygonType PolygonType
        {
            get => polygonType;
            set => polygonType = value;
        }
        public ushort[] UnknownUShorts { get => unknownUShorts; set => unknownUShorts = value; }
        public short PolySort0 { get => polySort0; set => polySort0 = value; }
        public short PolySort1 { get => polySort1; set => polySort1 = value; }
        public bool ColladaShadless { get => colladaShadless; set => colladaShadless = value; }
        public bool ColladaApplyLighting { get; set; }
        public Polygon BasePolygon { get => basePolygon; set => basePolygon = value; }
    }
}