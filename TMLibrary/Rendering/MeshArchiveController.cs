﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using TMLibrary.Lib;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Extensions;
using TMLibrary.Lib.Util;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     A controller for reading from and writing to a mesh archive file.
    /// </summary>
    public class MeshArchiveController
    {
        private readonly MeshArchiveFile model;
        private ProjectDefinition project;

        public MeshArchiveController(MeshArchiveFile model, ProjectDefinition project)
        {
            this.model = model;
            this.project = project;
        }

        public void ReadFile(string fileName)
        {
            var fileBuffer = File.ReadAllBytes(fileName);
            ReadMeshArchive(fileBuffer);
        }

        public long ReadPointer(BinaryReader reader)
        {
            return Utility.ReadPointer(reader.ReadUInt32(), model.Definition);
        }

        private bool WithinPointerRange(long address, PointerDefinition definition)
        {
            return address >= definition.MinAddress && address <= definition.MaxAddress;
        }

        /// <summary>
        ///     A visual mesh represents how the mesh is supposed to look when exporting a scene to Collada or another external
        ///     file format.
        /// </summary>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public List<GameMesh> CreateVisualMeshes(GameMesh mesh)
        {
            var visualMeshes = new List<GameMesh>();
            int polygonsPerVisualMesh = 10;
            int visualMeshAmount = (mesh.OriginalPolygons.Count() / polygonsPerVisualMesh) + 1;
            var normalPolys = mesh.OriginalPolygons.Where(p => p.RenderFlag == RenderFlag.Normal);

            for (int iMesh = 0; iMesh < visualMeshAmount; iMesh++)
            {
                int minPolyIndex = iMesh * polygonsPerVisualMesh;
                var polys = normalPolys.Skip(minPolyIndex).Take(polygonsPerVisualMesh);
                var visualMesh = new GameMesh(model);
                visualMesh.ParentFile = mesh.ParentFile;
                visualMesh.Vertices.AddRange(mesh.Vertices);
                visualMesh.OriginalPolygons = new List<GamePolygon>(polys);
                //CreateBillboardMeshes(mesh);
                visualMeshes.Add(visualMesh);
            }

            return visualMeshes;
        }

        /*private void CreateBillboardMeshes(GameMesh mesh)
        {
            var meshController = new MeshArchiveController(mesh.ParentFile, project);

            var billboardPolygons = mesh.OriginalPolygons.Where(p => p.RenderFlag != RenderFlag.Normal);
            mesh.BillboardMeshes = new GameMesh[billboardPolygons.Count()];

            for (var iBillboard = 0; iBillboard < billboardPolygons.Count(); iBillboard++)
            {
                var billboardPolygon = billboardPolygons.ElementAt(iBillboard);
                mesh.BillboardMeshes[iBillboard] =
                    meshController.CreateBillboardMesh(billboardPolygon, mesh.ParentFile);
            }
        }

        /// <summary>
        ///     Creates a billboard mesh from a polygon
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        public GameMesh CreateBillboardMesh(GamePolygon originalPolygon, MeshArchiveFile meshArchiveFile)
        {
            var oldIndices = originalPolygon.GetIndices();
            var parentMesh = originalPolygon.Parent;
            var position = parentMesh.VertexPositions[oldIndices[0].Id];

            var mesh = new GameMesh(model);
            mesh.ParentFile = meshArchiveFile;

            var halfWidth = oldIndices[1].Id;
            var halfHeight = oldIndices[2].Id;

            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(-halfWidth + position.X, position.Y,
                -halfHeight + position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(halfWidth + position.X, position.Y,
                -halfHeight + position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(halfWidth + position.X, position.Y,
                halfHeight + position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(-halfWidth + position.X, position.Y,
                halfHeight + position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));

            var polygon = new GamePolygon(model.Parent);
            polygon.PolygonType = project.PolygonTypeDictionary[0xC0F0184];
            for (int iCol = 0; iCol < 4; iCol++)
            {
                var polygonIndex = oldIndices[iCol];
                polygonIndex.DiffuseColor = Color.White;
                polygonIndex.TexCoord = oldIndices[iCol].TexCoord;
            }
            polygon.Parent = mesh;
            if (originalPolygon.TextureFile != null)
            {
                polygon.TextureFile = originalPolygon.TextureFile;
            }

            mesh.OriginalPolygons.Add(polygon);

            return mesh;
        }*/

        /// <summary>
        ///     Incomplete
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="visualMesh"></param>
        /// <param name="originalMesh"></param>
        /// <returns></returns>
        private GamePolygon[] ApplyRenderTag(GamePolygon polygon, GameMesh visualMesh, GameMesh originalMesh)
        {
            return CreatePolygonBillboard2(polygon, visualMesh, originalMesh);
        }

        /// <summary>
        ///     Converts a polygon to a billboard.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="visualMesh"></param>
        /// <param name="originalMesh"></param>
        /// <returns></returns>
        private GamePolygon[] CreatePolygonBillboard2(GamePolygon polygon, GameMesh visualMesh, GameMesh originalMesh)
        {
            var startIndex = 0;
            var newPolygon1 = new PolygonIndex[4];

            if (visualMesh.OriginalPolygons.Count() > 0)
            {
                startIndex = visualMesh.OriginalPolygons.SelectMany(p => p.GetIndices()).Select(p => p.Id).Max() + 1;
            }
            else
            {
                startIndex = originalMesh.OriginalPolygons.SelectMany(p => p.GetIndices()).Select(p => p.Id).Max() + 1;
            }

            newPolygon1 = new[]
            {
                new PolygonIndex(startIndex), new PolygonIndex(startIndex + 1), new PolygonIndex(startIndex + 3), new PolygonIndex(startIndex + 2)
            };

            var vertices = new ColladaLibrary.Render.Vertex[polygon.GetIndexAmount()];
            var indices = polygon.GetIndices();
            var position = polygon.Parent.Vertices[indices[0].Id];
            var halfWidth = indices[1].Id;
            var halfHeight = indices[2].Id;
            vertices[0] = new Vertex(halfWidth + position.Vector.X, 0, -halfHeight + position.Vector.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White);
            vertices[1] = new Vertex(-halfWidth + position.Vector.X, 0, -halfHeight + position.Vector.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White);
            vertices[2] = new Vertex(-halfWidth + position.Vector.X, 0, halfHeight + position.Vector.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White);
            vertices[3] = new Vertex(halfWidth + position.Vector.X, 0, halfHeight + position.Vector.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White);

            visualMesh.Vertices.AddRange(vertices);

            var poly1 = new GamePolygon(model.Parent);
            polygon.CloneTo(poly1);
            //poly1.Indices.AddRange(newPolygon1);

            for (int iTexCoord = 0; iTexCoord < polygon.GetIndexAmount(); iTexCoord++)
            {
                poly1.GetIndices()[iTexCoord].TexCoord = polygon.GetIndices()[iTexCoord].TexCoord;
            }
            //poly1.TextureCoordSet = new TextureCoordSet(polygon.TextureCoordSet);
            poly1.Address = polygon.Address;
            poly1.RenderFlag = polygon.RenderFlag;
            poly1.RenderFlagId = polygon.RenderFlagId;
            return new[] {poly1};
        }

        private void ReadMeshArchive(byte[] fileBuffer)
        {
            using (var reader = new BinaryReader(new MemoryStream(fileBuffer)))
            {
                if (model.Definition.InstructionParser != null)
                {
                    model.Definition.InstructionParser.ParseInstructions(model, reader);
                }
            }
        }

        /// <summary>
        ///     Reads a pointer and adds it to a sorted list.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="pointers"></param>
        private void AddPointerToList(BinaryReader reader, SortedSet<Pointer> pointers)
        {
            reader.BaseStream.Position -= sizeof(uint);
            var pointerAddress = reader.BaseStream.Position;
            var pointToAddress = ReadPointer(reader);
            if (pointToAddress >= 0 && pointToAddress < reader.BaseStream.Length)
            {
                var ptr = new Pointer(pointerAddress, pointToAddress);
                var start = reader.BaseStream.Position;
                reader.BaseStream.Position = pointToAddress;
                ptr.ValueAtPointToAddress = reader.PeekUInt16();
                reader.BaseStream.Position = start;
                pointers.Add(ptr);
            }
        }

        private bool IsValidPointer(ushort value)
        {
            // Get pointer offset range
            var pointerDefinitions = model.Definition.PointerDefinitions;
            var minPointerTag = pointerDefinitions.Min(p => p.HeaderTag);
            var maxPointerTag = pointerDefinitions.Max(p => p.HeaderTag);
            return value >= minPointerTag && value <= maxPointerTag;
        }

        /// <summary>
        ///     Creates lists of pointers and mesh addresses
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public void ReadAddresses(BinaryReader reader, SortedSet<Pointer> pointers, List<long> meshAddresses)
        {
            ushort prev = 0;
            ushort next = 0;

            while (!reader.ReachedEnd())
            {
                prev = next;
                next = reader.ReadUInt16();

                if (IsValidPointer(next))
                {
                    AddPointerToList(reader, pointers);
                }

                if (next == (model.Definition.MeshHeaderTag & 0xFFFF))
                {
                    meshAddresses.Add(reader.BaseStream.Position - sizeof(ushort));
                }
            }
        }

        /// <summary>
        ///     Reads and returns a mesh from a binary reader based on its address.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="reader"></param>
        /// <param name="indexStartAddress"></param>
        /// <param name="vertexStartAddress"></param>
        /// <returns></returns>
        private GameMesh CreateMesh(GameMesh mesh, long address, BinaryReader reader)
        {
            var meshController = new MeshController(mesh, mesh.ParentFile.Parent);

            meshController.ReadMesh(reader, mesh, mesh.MeshHeader.PolygonAmount);
            meshController.ReadVertexPositions(reader, mesh);

            return mesh;
        }

        public GameMesh ParseMesh(BinaryReader reader, List<GameMesh> meshes, long address)
        {
            reader.BaseStream.Position = address;

            var mesh = new GameMesh(model);
            mesh.Address = address;
            var meshHeader = model.Parent.MeshReader.ReadMeshHeader(reader, mesh);
            mesh.MeshHeader = meshHeader;

            CreateMesh(mesh, address, reader);

            return mesh;
        }

        /// <summary>
        ///     Creates a list of meshes from a list of mesh addresses and a binary reader.
        /// </summary>
        /// <param name="meshAddresses"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        private List<GameMesh> ReadMeshes(List<long> meshAddresses, BinaryReader reader)
        {
            var meshes = new List<GameMesh>();

            foreach (var address in meshAddresses)
            {
                ParseMesh(reader, meshes, address);
            }

            return meshes;
        }

        /// <summary>
        ///     Finds all the new meshes added since the last save and calculates the total amount of additional space that will be
        ///     needed.
        /// </summary>
        /// <returns></returns>
        /*private long FindNewMeshBufferLength(IEnumerable<Mesh> newMeshes)
        {
            return newMeshes.Sum(m => m.GetBufferSize());
        }*/

        /// <summary>
        ///     Updates any changes made to previous meshes, including texture coordinate changes.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="writer"></param>
        private void UpdatePreviousMeshes(ProjectDefinition project, BinaryWriter writer)
        {
            var previousMeshes =
                model.Meshes.Where(m => m.WrittenToFile && m.MeshHeader.PolygonStartAddress > 0 && m.MeshHeader.VertexStartAddress > 0);
            foreach (var mesh in previousMeshes)
            {
                project.MeshWriter.WriteMeshToFile(writer, mesh);
            }
        }

        /// <summary>
        ///     Takes a list of new meshes and appends them to the end of a file buffer.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="writer"></param>
        /// <param name="newMeshes"></param>
        /// <param name="startingAddress"></param>
        private void WriteNewMeshes(ProjectDefinition project, BinaryWriter writer, IEnumerable<GameMesh> newMeshes,
            long startingAddress)
        {
            writer.BaseStream.Position = startingAddress;
            foreach (var mesh in newMeshes)
            {
                project.MeshWriter.WriteMeshToFile(writer, mesh);
            }
        }

        /// <summary>
        ///     The main method used when the user saves the file. Updates texture coordinates on meshes with updated textures
        ///     and appends any new meshes added to the file.
        /// </summary>
        /// <param name="project"></param>
        public void WriteFile(ProjectDefinition project)
        {
            var oldFileBuffer = File.ReadAllBytes(model.Parent.MeshArchiveFileName);
            long newMeshStart = oldFileBuffer.Length;
            var fileBuffer = new byte[0];
            var newMeshes = model.Meshes.Where(m => !m.WrittenToFile);

            long newMeshBufferSize = newMeshes.Sum(m => m.GetBufferSize());

            fileBuffer = new byte[newMeshStart + newMeshBufferSize];

            using (var writer = new BinaryWriter(new MemoryStream(fileBuffer)))
            {
                writer.Write(oldFileBuffer);
                UpdatePreviousMeshes(project, writer);
                WriteNewMeshes(project, writer, newMeshes, newMeshStart);
                UpdateInstructions(writer);
            }

            // Flush buffer to file
            File.WriteAllBytes(model.Parent.MeshArchiveFileName, fileBuffer);
        }

        private void UpdateInstructions(BinaryWriter writer)
        {
            var allInstructions = model.GetAllInstructions();

            foreach (var instruction in allInstructions)
            {
                writer.BaseStream.Position = instruction.Value.Address + instruction.Value.BufferSize;
                foreach (var mesh in instruction.Value.MeshList)
                {
                    writer.Write(Utility.CreatePointer(mesh.Address, model.Definition));
                }
                foreach (var child in instruction.Value.Children)
                {
                    writer.Write(Utility.CreatePointer(child.Address, model.Definition));
                }
            }
        }

        public void UpdateMeshPointers(List<GameMesh> replacedMeshes, GameMesh replacementMesh)
        {
            var allInstructions = model.GetAllInstructions();

            foreach (var instruction in allInstructions)
            {
                for (int iMesh = 0; iMesh < instruction.Value.MeshList.Count; iMesh++)
                {
                    if (replacedMeshes.Contains(instruction.Value.MeshList[iMesh]))
                    {
                        instruction.Value.MeshList[iMesh] = replacementMesh;
                    }
                }
            }
        }

        /// <summary>
        ///     Formats an unsigned integer as a hexadecimal string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string UIntHexText(uint value)
        {
            var valueAsBytes = BitConverter.GetBytes(value);
            var text = valueAsBytes[0].ToString("X").PadLeft(2, '0');
            text += valueAsBytes[1].ToString("X").PadLeft(2, '0');
            return text;
        }

        /// <summary>
        ///     Writes an instruction used in an instruction log.
        /// </summary>
        /// <param name="instruction"></param>
        /// <param name="writer"></param>
        /// <param name="indents"></param>
        /// <param name="parseChildren"></param>
        private void WriteInstructionText(Instruction instruction, TextWriter writer, int indents, bool parseChildren)
        {
            if (instruction.IsRootInstruction)
            {
                writer.WriteLine("ROOT INSTRUCTION");
            }
            if (indents > 1000)
            {
                writer.Write("Exceeding number of children. Log stopped here.");
                return;
            }
            for (var iIndex = 0; iIndex < indents; iIndex++)
            {
                writer.Write(" ");
            }
            try
            {
                writer.Write(instruction.GetInstructionText() + " ");
            }
            catch (NotImplementedException ex)
            {
                writer.Write(instruction.ToString().Split('.').Last());
                /*for (var iBuffer = 0; iBuffer < instruction.Buffer.Length / 2; iBuffer += 2)
                {
                    var value = BitConverter.ToUInt16(instruction.Buffer, iBuffer);
                    writer.Write(UIntHexText(value));
                    writer.Write(" ");
                }*/
            }
            writer.Write($"@ {instruction.Address}");
            foreach (var mesh in instruction.MeshList)
            {
                int meshId = project.MeshFile.Meshes.IndexOf(mesh);
                writer.Write($" -> Mesh: {meshId + 1}");
            }
            foreach (var child in instruction.Children)
            {
                writer.Write($" -> Child: @{child.Address}");
            }
            /*foreach (var ptr in instruction.ChildPointers)
            {
                writer.Write($" {ptr.PointToAddress}={UIntHexText(ptr.ValueAtPointToAddress)} ");
            }*/
            var totalChildrenCount = instruction.Children.Count() + instruction.MeshList.Count();
            if (instruction.ChildAmount != totalChildrenCount)
            {
                writer.Write($" CHILD PTR COUNT DOES NOT MATCH CHILDREN - Total Child Count {totalChildrenCount} Expected {instruction.ChildAmount}");
            }

            if (parseChildren)
            {
                if (instruction.Children.Any())
                {
                    writer.Write("\n");
                    foreach (var child in instruction.Children)
                    {
                        WriteInstructionText(child, writer, indents + 1, true);
                    }
                }
                else
                {
                    foreach(var childAddress in instruction.ChildAddresses)
                    {
                        writer.Write($" Ptr: {childAddress}");
                    }
                    /*if (instruction.MeshIdList.Any())
                    {
                    }
                    else if (instruction.Definition.Type != InstructionType.MeshContainer)
                    {
                        writer.Write(" NO CHILDREN/MESHES:");
                        foreach (var address in instruction.ChildAddresses)
                        {
                            writer.Write("CHILD ADDRESS: " + address);
                        }
                    }*/
                    writer.Write("\n");
                }
            }
            else
            {
                writer.Write("\n");
            }
        }

        /// <summary>
        ///     Saves a list of instructions within a hierarchy along with a list of meshes.
        /// </summary>
        /// <param name="writer"></param>
        public void SaveInstructionsLog(TextWriter writer)
        {
            if (model.Definition.InstructionParser != null)
            {
                var count = model.Instructions.Count();
                foreach (var root in model.Instructions.Where(i => i.IsRootInstruction))
                {
                    WriteInstructionText(root, writer, 0, true);
                }
            }
            SaveMeshListLog(writer);
            SaveEntityListLog(writer);
            WriteVertices(writer);
        }

        private void WriteVertices(TextWriter writer)
        {
            for (int iMesh = 0; iMesh < model.Meshes.Count(); iMesh++)
            {
                var mesh = model.Meshes[iMesh];
                writer.Write($"Mesh {iMesh} Vertices at {mesh.MeshHeader.VertexStartAddress}:\n");
                foreach (var vert in mesh.Vertices)
                {
                    if (vert != null)
                    {
                        writer.Write($"V X: {vert.Vector.X} Y: {vert.Vector.Y} Z: {vert.Vector.Z}\n");
                    }
                }
            }
        }

        private void WriteOrpahns(TextWriter writer)
        {
            writer.Write("Orphans\n");
            var orphans = model.Instructions.Where(i => i.ParentCount == 0 && !i.IsRootInstruction);

            foreach (var orphan in orphans)
            {
                WriteInstructionText(orphan, writer, 0, true);
            }
        }

        private void SaveEntityListLog(TextWriter writer)
        {
            writer.Write("Entity List\n");
            for (var iEntity = 0; iEntity < model.Entities.Count; iEntity++)
            {
                writer.Write($"Entity {iEntity + 1}: {model.Entities[iEntity].Instruction.Address}\n");
            }
        }

        /// <summary>
        ///     Saves a list of meshes with each address to a text writer.
        /// </summary>
        /// <param name="writer"></param>
        private void SaveMeshListLog(TextWriter writer)
        {
            writer.Write("Mesh List\n");
            for (var iMesh = 0; iMesh < model.Meshes.Count(); iMesh++)
            {
                writer.Write($"Mesh {iMesh + 1}: {model.Meshes[iMesh].Address}\n");
            }
        }
    }
}