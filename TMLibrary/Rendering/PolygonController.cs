﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Extensions;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     The main class used for parsing polygons from a binary reader.
    /// </summary>
    public abstract class PolygonController
    {
        protected ProjectDefinition project;

        public PolygonController(ProjectDefinition project)
        {
            this.project = project;
        }

        /// <summary>
        ///     Reads a polygon from a binary reader. Uses a uint header tag to find the correct polygon definition.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public abstract GamePolygon Read(BinaryReader reader, GameMesh parent);
        public abstract void Write(BinaryWriter writer, GamePolygon polygon);

        protected virtual TextureCoordinate ReadTextureCoordinate(BinaryReader reader)
        {
            var texCoord = new TextureCoordinate();
            texCoord.U = reader.ReadUInt16();
            texCoord.V = reader.ReadUInt16();

            return texCoord;
        }

        protected void WriteTextureCoordinates(BinaryWriter writer, GamePolygon polygon)
        {
            for (int iTexCoord = 0; iTexCoord < polygon.GetIndexAmount(); iTexCoord++)
            {
                var index = polygon.GetPolygonIndex(iTexCoord);
                var texCoord = index.TexCoord;
                WriteTextureCoordinate(writer, texCoord, polygon);
            }
        }

        protected void WriteBlankTextureCoordinates(BinaryWriter writer, GamePolygon polygon)
        {
            for (int iTexCoord = 0; iTexCoord < polygon.GetPolygonAmount(); iTexCoord++)
            {
                writer.Write((ushort)(0));
                writer.Write((ushort)(0));
            }
        }

        protected void WriteTextureCoordinate(BinaryWriter writer, TextureCoordinate texCoord, GamePolygon polygon)
        {
            double u = texCoord.U;
            double v = texCoord.V;

            v *= polygon.TextureFile.Height;
            u *= polygon.TextureFile.ActualWidth;

            writer.Write((ushort)(u));
            writer.Write((ushort)(v));
        }

        protected void WriteTextureId(BinaryWriter writer, GamePolygon polygon)
        {
            if (polygon.TextureFile != null && polygon.TextureId >= 0)
            {
                writer.Write((ushort)(16384 + polygon.TextureId));
            }
            else
            {
                //writer.Write((ushort)(16384 + polygon.TextureFile.IdNumber + polygon.TextureFile.Parent.TextureFiles.Count()));
                //throw new Exception("No texture ID found.");
                writer.Write((ushort)(16384));
            }
        }

        protected virtual void ReadTextureId(BinaryReader reader, GamePolygon polygon)
        {
            ushort originalTextureId = reader.ReadUInt16();

            // Still not sure how this works. Temporary solution.
            int textureId = (ushort)(originalTextureId - originalTextureId / 0x1000 * 0x1000);

            polygon.TextureFile = project.TextureArchiveFile.TextureFiles[textureId];

            RescaleTextureCoordinates(polygon);
        }

        public virtual void RescaleTextureCoordinates(GamePolygon polygon)
        {
            for (int iTexCoord = 0; iTexCoord < polygon.GetIndexAmount(); iTexCoord++)
            {
                var texCoord = polygon.GetPolygonIndex(iTexCoord).TexCoord;
                texCoord.U /= polygon.TextureFile.ActualWidth;
                texCoord.V /= polygon.TextureFile.Height;
            }
        }

        protected virtual void ReadTextureCoordinates(BinaryReader reader, GamePolygon polygon)
        {
            for (int iCoord = 0; iCoord < polygon.GetIndexAmount(); iCoord++)
            {
                polygon.GetPolygonIndex(iCoord).TexCoord = ReadTextureCoordinate(reader);
            }
        }

        protected void WriteIndices(BinaryWriter writer, GamePolygon polygon)
        {
            if (polygon.GetIndexAmount() != polygon.PolygonType.IndexAmount)
            {
                throw new Exception("Polygon index amount does not match definition.");
            }
            var indices = polygon.GetIndices();
            foreach (var index in indices)
            {
                writer.Write((ushort)index.Id);
            }
            if (!polygon.HasFourIndices())
            {
                writer.Write((ushort)0);
            }
        }

        protected void ReadIndices(BinaryReader reader, GamePolygon polygon, int indexAmount)
        {
            var indices = polygon.GetIndices();
            polygon.IndexAmount = indexAmount;

            for (int iIndex = 0; iIndex < indexAmount; iIndex++)
            {
                polygon.SetIndex(iIndex, new PolygonIndex(reader.ReadUInt16()));
            }
            if (!polygon.HasFourIndices())
            {
                reader.SkipBytes(sizeof(ushort));
            }
        }

        protected void ReadTextureDiffuseColors(BinaryReader reader, GamePolygon polygon)
        {
            for (int iColor = 0; iColor < polygon.GetIndexAmount(); iColor++)
            {
                var index = polygon.GetPolygonIndex(iColor);
                index.DiffuseColor = ReadTextureDiffuseColor(reader);
            }
        }

        protected void ReadSharedTextureDiffuseColor(BinaryReader reader, GamePolygon polygon)
        {
            var color = ReadTextureDiffuseColor(reader);
            for (int iColor = 0; iColor < polygon.GetIndexAmount(); iColor++)
            {
                var index = polygon.GetPolygonIndex(iColor);
                index.DiffuseColor = color;
            }
        }

        protected void ReadRenderFlagId(BinaryReader reader, GamePolygon polygon)
        {
            polygon.RenderFlagId = reader.ReadUInt16();

            polygon.RenderFlag = project.MeshReader.FindRenderFlag(polygon.RenderFlagId);
        }

        protected Color ReadTextureDiffuseColor(BinaryReader reader)
        {
            var b = reader.ReadByte();
            var g = reader.ReadByte();
            var r = reader.ReadByte();
            var a = reader.ReadByte();

            return Color.FromArgb(a, r, g, b);
        }

        protected void ReadCrossProduct(BinaryReader reader, GamePolygon polygon)
        {
            float x = reader.ReadInt16() / 4096.0f;
            float y = reader.ReadInt16() / 4096.0f;
            float z = reader.ReadInt16() / 4096.0f;

            polygon.CrossProduct = new Vector3(x, y, z);
        }

        protected void WriteRenderFlagId(BinaryWriter writer, GamePolygon polygon)
        {
            writer.Write(polygon.RenderFlagId);
        }

        protected void WriteSharedTextureDiffuseColor(BinaryWriter writer, GamePolygon polygon)
        {
            var color = polygon.GetPolygonIndex(0).DiffuseColor;
            writer.Write(color.B);
            writer.Write(color.G);
            writer.Write(color.R);
            writer.Write((byte)0);
        }

        protected void WriteTextureDiffuseColors(BinaryWriter writer, GamePolygon polygon)
        {
            for (int iColor = 0; iColor < polygon.GetIndexAmount(); iColor++)
            {
                var color = polygon.GetPolygonIndex(0).DiffuseColor;
                writer.Write(color.B);
                writer.Write(color.G);
                writer.Write(color.R);
                writer.Write((byte)0);
            }
        }

        protected void WriteCrossProduct(BinaryWriter writer, GamePolygon polygon)
        {
            var crossProductMax = 4096.0;
            writer.Write((short)(polygon.CrossProduct.X * crossProductMax));
            writer.Write((short)(polygon.CrossProduct.Y * crossProductMax));
            writer.Write((short)(polygon.CrossProduct.Z * crossProductMax));
        }
    }
}