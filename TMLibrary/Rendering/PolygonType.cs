﻿namespace TMLibrary.Rendering
{
    public class PolygonType
    {
        private uint header;
        private int indexAmount;
        private bool colorOnly;
        private bool perVertexColors;
        private bool displayVertexColors;
        private bool hasDiffuseColors;

        /// <summary>
        /// Used as a placeholder for storing unknown values that come after known values in a polygon.
        /// </summary>
        private int unknownUShortAmount;

        public PolygonType(uint header, int indexAmount, bool colorOnly, bool perVertexColors, bool displayVertexColors, bool hasDiffuseColors)
        {
            this.header = header;
            this.indexAmount = indexAmount;
            this.colorOnly = colorOnly;
            this.perVertexColors = perVertexColors;
            this.displayVertexColors = displayVertexColors;
            this.hasDiffuseColors = hasDiffuseColors;
        }

        public PolygonType(uint header, int indexAmount, bool colorOnly, bool perVertexColors, bool displayVertexColors, bool hasDiffuseColors, int unknownUShortAmount) : this(header, indexAmount, colorOnly, perVertexColors, displayVertexColors, hasDiffuseColors)
        {
            this.unknownUShortAmount = unknownUShortAmount;
        }

        public uint Header { get => header; }
        public int IndexAmount { get => indexAmount; }
        public bool ColorOnly { get => colorOnly; }
        public bool PerVertexColors { get => perVertexColors; }
        public bool DisplayVertexColors { get => displayVertexColors; }
        public int UnknownUShortAmount { get => unknownUShortAmount; }
        public bool HasDiffuseColors { get => hasDiffuseColors; }
    }
}
