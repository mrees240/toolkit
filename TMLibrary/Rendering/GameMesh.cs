﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.Collada.Hierarchy;
using TMLibrary.Lib.Util;
using TMLibrary.Textures;

namespace TMLibrary.Rendering
{
    /// <summary>
    ///     The internal mesh format used by the toolkit. Imported meshes are converted to this class.
    /// </summary>
    public class GameMesh
    {
        public static readonly int ValuesPerVertex = 9;
        private long address;
        private MeshHeader meshHeader;
        private bool addressInitialized;
        private List<Vector3> vertexPositions;
        public List<GamePolygon> OriginalPolygons { get; set; }
        private List<Vertex> vertices;
        public List<GamePolygon> Polygons { get; set; }

        public MeshArchiveFile ParentFile { get; set; }

        private Mesh baseMesh;

        // Collada import fields
        private bool isColladaBillboard;
        private Matrix4x4 colladaMatrix;

        private Node node;

        public GameMesh()
        {
            vertexPositions = new List<Vector3>();
            vertices = new List<Vertex>();
            colladaMatrix = Matrix4x4.Identity;
            OriginalPolygons = new List<GamePolygon>();
            Polygons = new List<GamePolygon>();
            BillboardMeshes = new GameMesh[0];
        }

        public GameMesh(MeshArchiveFile parentFile) : this()
        {
            this.ParentFile = parentFile;
        }

        public int GetPolygonAmount()
        {
            return OriginalPolygons.Sum(p => p.GetPolygonAmount());
        }

        public float GetHeight()
        {
            if (isColladaBillboard)
            {
                return OriginalPolygons.First().GetPolygonIndex(2).Id;
            }
            else
            {
                var minZ = Vertices.Min(v => v.Vector.Z);
                var maxZ = Vertices.Max(v => v.Vector.Z);

                return Math.Abs(maxZ - minZ);
            }
        }

        public void Round()
        {
            for (int i = 0; i < Vertices.Count; i++)
            {
                var v = Vertices[i].Vector;
                Vertices[i].Vector = new Vector3((float)Math.Round(v.X), (float)Math.Round(v.Y), (float)Math.Round(v.Z));
            }
        }

        public void Transform(Matrix4x4 matrix)
        {
            for (int iVert = 0; iVert < Vertices.Count(); iVert++)
            {
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, matrix);
            }
            if (!IsColladaBillboard)
            {
                foreach (var poly in OriginalPolygons)
                {
                    if (poly.RenderFlag == RenderFlag.Normal)
                    {
                        poly.CrossProduct = Utility.FindCrossProduct(poly);
                    }
                }
            }
            else
            {
                foreach (var poly in OriginalPolygons)
                {
                    if (poly.RenderFlag != RenderFlag.Normal)
                    {
                        int positionIndex = poly.GetPolygonIndex(0).Id;
                        Vertices[positionIndex].Vector = Vector3.Transform(Vertices[positionIndex].Vector, matrix);
                    }
                }
            }
        }

        public IEnumerable<Color> Colors
        {
            get { return OriginalPolygons.Where(p => p.PolygonType.ColorOnly).Select(p => p.GetPolygonIndex(0).DiffuseColor).Distinct(); }
        }

        public IEnumerable<TextureFile> Textures
        {
            get { return OriginalPolygons.Where(p => p.PolygonType != null && !p.PolygonType.ColorOnly).Select(p => p.TextureFile).Distinct(); }
        }

        public GameMesh(GameMesh mesh, bool defaultLighting)
        {
            this.ParentFile = mesh.ParentFile;
            this.isColladaBillboard = mesh.isColladaBillboard;
            OriginalPolygons = new List<GamePolygon>();
            this.meshHeader = mesh.meshHeader;

            foreach (var poly in mesh.OriginalPolygons)
            {
                if (this.ParentFile != null)
                {
                    GamePolygon clonedPolygon = new GamePolygon(this.ParentFile.Parent, defaultLighting);
                    poly.CloneTo(clonedPolygon);
                    if (!mesh.isColladaBillboard)
                    {
                        poly.CrossProduct = Utility.FindCrossProduct(poly);
                    }
                    clonedPolygon.Parent = this;
                    OriginalPolygons.Add(clonedPolygon);
                }
                else
                {
                    OriginalPolygons.Add(poly);
                }
            }
            foreach (var vert in mesh.Vertices)
            {
                Vertices.Add(new ColladaLibrary.Render.Vertex(vert.Vector.X, vert.Vector.Y, vert.Vector.Z, vert.U, vert.V, vert.Normal.X, vert.Normal.Y, vert.Normal.Z, vert.DiffuseColor));
            }
        }

        public bool WrittenToFile { get; set; }

        public long Address
        {
            get => address;
            set
            {
                address = value;
            }
        }

        public List<PolygonIndex> GetTriangulatedIndices()
        {
            var quadCount = OriginalPolygons.Count(p => p.HasFourIndices());
            var triCount = OriginalPolygons.Count(p => p.HasThreeIndices());

            var triangulatedIndices = new List<PolygonIndex>();

            int index = 0;

            foreach (var poly in OriginalPolygons)
            {
                var currPolyIndices = poly.GetTriangulatedIndices();

                for (int iIndex = 0; iIndex < currPolyIndices.Length; iIndex++)
                {
                    triangulatedIndices[iIndex + index] = currPolyIndices[iIndex];
                }
                //Array.Copy(currPolyIndices, 0, triangulatedIndices, index, currPolyIndices.Count);
                index += currPolyIndices.Length;
            }

            return triangulatedIndices;
        }

        public bool ShowInRenderer
        {
            get;
            set;
        }

        public GameMesh[] BillboardMeshes { get; set; }
        public MeshHeader MeshHeader { get => meshHeader; set => meshHeader = value; }
        public bool IsColladaBillboard { get => isColladaBillboard; set => isColladaBillboard = value; }
        public Matrix4x4 ColladaMatrix { get => colladaMatrix; set => colladaMatrix = value; }
        public Node Node { get => node; set => node = value; }
        public List<Vertex> Vertices { get => vertices; set => vertices = value; }
        public List<Vector3> VertexPositions { get => vertexPositions; set => vertexPositions = value; }

        public IEnumerable<Color> GetColors()
        {
            var colors = new List<Color>();

            foreach (var polygon in OriginalPolygons.Where(p => p.PolygonType.ColorOnly))
            {
                var solidColor = polygon.GetPolygonIndex(0).DiffuseColor;
                if (!colors.Contains(solidColor))
                {
                    colors.Add(solidColor);
                }
            }

            return colors;
        }

        public Vector3 GetCenterPoint()
        {
            var validVerts = Vertices.Where(v => v != null);
            if (validVerts.Count() == 0)
            {
                return new Vector3();
            }
            var minX = validVerts.Min(v => v.Vector.X);
            var maxX = validVerts.Max(v => v.Vector.X);
            var minY = validVerts.Min(v => v.Vector.Y);
            var maxY = validVerts.Max(v => v.Vector.Y);
            var minZ = validVerts.Min(v => v.Vector.Z);
            var maxZ = validVerts.Max(v => v.Vector.Z);

            var xLength = Math.Abs(maxX - minX);
            var yLength = Math.Abs(maxY - minY);
            var zLength = Math.Abs(maxZ - minZ);

            return new Vector3(minX + xLength / 2.0f, minY + yLength / 2.0f, minZ + zLength / 2.0f);
        }

        public float GetBoundingSphereRadius()
        {
            var verts = Vertices.Where(v => v != null);

            if (Vertices.Count() > 0)
            {
                /*float maxLength = 0;

                var minX = Vertices.Min(v => v.Vector.X);
                var minY = Vertices.Min(v => v.Vector.Y);
                var minZ = Vertices.Min(v => v.Vector.Z);
                var maxX = Vertices.Max(v => v.Vector.X);
                var maxY = Vertices.Max(v => v.Vector.Y);
                var maxZ = Vertices.Max(v => v.Vector.Z);
                //return Vertices.Max(v => v.Vector.Length());

                float xLength = (int)(Math.Abs(maxX - minX) + 1);
                float yLength = (int)(Math.Abs(maxY - minY) + 1);
                float zLength = (int)(Math.Abs(maxZ - minZ) + 1);
                if (xLength > maxLength)
                {
                    maxLength = xLength;
                }
                if (yLength > maxLength)
                {
                    maxLength = yLength;
                }
                if (zLength > maxLength)
                {
                    maxLength = zLength;*/
                var center = GetCenterPoint();
                return verts.Max(v => (v.Vector - center).Length());
            }
            return 0;
        }

        public void TranslateMesh(Vector3 translation)
        {
            var translationMatrix = Matrix4x4.CreateTranslation(translation);
            for (int iVert = 0; iVert < Vertices.Count(); iVert++)
            {
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, translationMatrix);
            }
        }

        public void RotateMesh(Quaternion rotation)
        {
            var finalMatrix = Matrix4x4.CreateFromQuaternion(rotation);
            var center = Matrix4x4.CreateTranslation(GetCenterPoint());
            var centerNeg = Matrix4x4.CreateTranslation(GetNegCenterPoint());

            for (int iVert = 0; iVert < Vertices.Count(); iVert++)
            {
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, centerNeg);
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, finalMatrix);
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, center);
            }
        }

        public void ScaleMesh(float scalar)
        {
            ScaleMesh(new Vector3(scalar));
        }

        private Vector3 GetNegCenterPoint()
        {
            var center = GetCenterPoint();
            return new Vector3(-center.X, -center.Y, -center.Z);
        }

        public void CenterMeshAtOrigin()
        {
            var centerNeg = Matrix4x4.CreateTranslation(GetNegCenterPoint());
            Transform(centerNeg);
        }

        public void ScaleMesh(Vector3 scale)
        {
            var scaleMatrix = Matrix4x4.CreateScale(scale);
            var center = Matrix4x4.CreateTranslation(GetCenterPoint());
            /*var centerNeg = Matrix.CreateTranslation(GetNegCenterPoint());
            for (int iVert = 0; iVert < Vertices.Count(); iVert++)
            {
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, centerNeg);
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, scaleMatrix);
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, center);
            }*/
            CenterMeshAtOrigin();
            Transform(scaleMatrix);
            Transform(center);
        }

        public long GetBufferSize()
        {
            int vertexSize = 8;
            long totalPolygonSize = Polygons.Count * 64;
            long totalVertexSize = Vertices.Count * vertexSize;
            long meshHeaderLength = meshHeader.BufferSize;

            return totalPolygonSize + totalVertexSize + meshHeaderLength;
        }

        public void TriangulateMesh()
        {
            var newPolys = new List<GamePolygon>();
            foreach (var poly in Polygons)
            {
                newPolys.AddRange(TriangulatePolygon(poly));
            }
            Polygons = new List<GamePolygon>(newPolys);
        }

        /// <summary>
        /// This is used to determine if the mesh should be written to certain file formats.
        /// If the mesh has a polygon with more than four indices, don't write the mesh at all.
        /// </summary>
        /// <returns></returns>
        /*public bool HasPolygonWithMoreThanFourIndices()
        {
            return Polygons.Select(p => p.IndexCount() > 4).Count() > 0;
        }*/

        public bool ContainsVertex(Vector3 vector)
        {
            if (vector.X >= Vertices.Min(v => v.Vector.X) && vector.X <= Vertices.Max(v => v.Vector.X))
            {
                if (vector.Y >= Vertices.Min(v => v.Vector.Y) && vector.Y <= Vertices.Max(v => v.Vector.Y))
                {
                    if (vector.Z >= Vertices.Min(v => v.Vector.Z) && vector.Z <= Vertices.Max(v => v.Vector.Z))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the number of triangles after triangulating the polygon.
        /// </summary>
        /// <param name="polygonId"></param>
        /// <returns></returns>
        private new List<GamePolygon> TriangulatePolygon(GamePolygon polygon)
        {
            var indices = polygon.GetIndices();
            //int polyCount = polygon.Indices.Count()
            int iTri = 1;
            var newPolys = new List<GamePolygon>();
            //int triangleCount = 1;
            if (polygon.HasFourIndices())
            {
                //triangleCount = polygon.IndexCount() - 2;
                for (iTri = 0; iTri < polygon.GetIndexAmount() - 2; iTri++)
                {
                    int startIndex = iTri;
                    var p0 = new GamePolygon(ParentFile.Parent, indices[0], indices[1], indices[2], null);
                    var p1 = new GamePolygon(ParentFile.Parent, indices[0], indices[2], indices[3], null);
                    polygon.CloneTo(p0);
                    polygon.CloneTo(p1);
                    //p0.TexCoords = currentTexCoords;
                    //p0.TextureDiffuseColors = currentDiffuse;
                    if (p0.TextureFile == null)
                    {
                        p0.PolygonType = ParentFile.Parent.PolygonTypeDictionary[0x4070100];
                        p0.RenderFlagId = 4419;
                    }
                    else
                    {
                        p0.PolygonType = ParentFile.Parent.PolygonTypeDictionary[0x90D0103];
                        p0.RenderFlagId = 5595;
                    }
                    if (p1.TextureFile == null)
                    {
                        p1.PolygonType = ParentFile.Parent.PolygonTypeDictionary[0x4070100];
                        p1.RenderFlagId = 4419;
                    }
                    else
                    {
                        p1.PolygonType = ParentFile.Parent.PolygonTypeDictionary[0x90D0103];
                        p1.RenderFlagId = 5595;
                    }
                    newPolys.Add(p0);
                    newPolys.Add(p1);
                }
            }
            else if (polygon.HasThreeIndices())
            {
                newPolys.Add(polygon);
            }
            return newPolys;
        }

        public Mesh GetBaseMesh()
        {
            if (baseMesh == null)
            {
                baseMesh = ToBaseMeshType();
            }

            return baseMesh;
        }

        private ColladaLibrary.Render.Mesh ToBaseMeshType()
        {
            var mesh = new ColladaLibrary.Render.Mesh();

            var billboardPolys = new List<Polygon>();

            mesh.VertexPositions.AddRange(vertexPositions);

            var defaultNorm = new Vector3(0.0f, 0.0f, 1.0f);

            foreach (var poly in OriginalPolygons)
            {
                bool isBillboard = (poly.RenderFlag == RenderFlag.Billboard || poly.RenderFlag == RenderFlag.Billboard2);

                if (!isBillboard)
                {
                    if (!poly.HasFourIndices())
                    {
                        var i0 = poly.GetPolygonIndex(1);
                        var i1 = poly.GetPolygonIndex(0);
                        var i2 = poly.GetPolygonIndex(2);
                        var v0 = new Vertex(vertexPositions[i0.Id], defaultNorm, i0.TexCoord.AsVector(), i0.DiffuseColor);
                        var v1 = new Vertex(vertexPositions[i1.Id], defaultNorm, i1.TexCoord.AsVector(), i1.DiffuseColor);
                        var v2 = new Vertex(vertexPositions[i2.Id], defaultNorm, i2.TexCoord.AsVector(), i2.DiffuseColor);

                        i0.Id = mesh.Vertices.Count() + 0;
                        i1.Id = mesh.Vertices.Count() + 1;
                        i2.Id = mesh.Vertices.Count() + 2;

                        mesh.Vertices.Add(v0);
                        mesh.Vertices.Add(v1);
                        mesh.Vertices.Add(v2);

                        var newPoly = new Polygon(i0, i1, i2);
                        if (poly.TextureFile != null)
                        {
                            newPoly.Textures.Diffuse = poly.TextureFile.BitmapFilePair;
                        }
                        mesh.AddPolygon(newPoly);
                    }
                    else
                    {
                        var i0 = poly.GetPolygonIndex(1);
                        var i1 = poly.GetPolygonIndex(0);
                        var i2 = poly.GetPolygonIndex(2);
                        var i3 = poly.GetPolygonIndex(3);

                        var iNew0 = new PolygonIndex(mesh.Vertices.Count() + 0, i0.DiffuseColor, i0.TexCoord);
                        var iNew1 = new PolygonIndex(mesh.Vertices.Count() + 1, i1.DiffuseColor, i1.TexCoord);
                        var iNew2 = new PolygonIndex(mesh.Vertices.Count() + 2, i2.DiffuseColor, i2.TexCoord);
                        var iNew3 = new PolygonIndex(mesh.Vertices.Count() + 3, i2.DiffuseColor, i2.TexCoord);
                        var iNew4 = new PolygonIndex(mesh.Vertices.Count() + 4, i1.DiffuseColor, i1.TexCoord);
                        var iNew5 = new PolygonIndex(mesh.Vertices.Count() + 5, i3.DiffuseColor, i3.TexCoord);


                        var v0 = new Vertex(vertexPositions[i0.Id], defaultNorm, iNew0.TexCoord.AsVector(), iNew0.DiffuseColor);
                        var v1 = new Vertex(vertexPositions[i1.Id], defaultNorm, iNew1.TexCoord.AsVector(), iNew1.DiffuseColor);
                        var v2 = new Vertex(vertexPositions[i2.Id], defaultNorm, iNew2.TexCoord.AsVector(), iNew2.DiffuseColor);

                        var v3 = new Vertex(vertexPositions[i2.Id], defaultNorm, iNew3.TexCoord.AsVector(), iNew3.DiffuseColor);
                        var v4 = new Vertex(vertexPositions[i1.Id], defaultNorm, iNew4.TexCoord.AsVector(), iNew4.DiffuseColor);
                        var v5 = new Vertex(vertexPositions[i3.Id], defaultNorm, iNew5.TexCoord.AsVector(), iNew5.DiffuseColor);

                        mesh.Vertices.Add(v0);
                        mesh.Vertices.Add(v1);
                        mesh.Vertices.Add(v2);
                        mesh.Vertices.Add(v3);
                        mesh.Vertices.Add(v4);
                        mesh.Vertices.Add(v5);

                        var newPoly = new Polygon(iNew0, iNew1, iNew2);
                        var newPoly2 = new Polygon(iNew3, iNew4, iNew5);
                        newPoly.CrossProduct = RenderUtility.FindCrossProduct(mesh, newPoly);
                        newPoly2.CrossProduct = RenderUtility.FindCrossProduct(mesh, newPoly2);

                        v0.Normal = newPoly.CrossProduct;
                        v1.Normal = newPoly.CrossProduct;
                        v2.Normal = newPoly.CrossProduct;

                        v3.Normal = newPoly2.CrossProduct;
                        v4.Normal = newPoly2.CrossProduct;
                        v5.Normal = newPoly2.CrossProduct;

                        /*foreach (var index in poly.GetIndices())
                        {
                            vertices[index.Id].DiffuseColor = index.DiffuseColor;
                        }*/

                        if (poly.TextureFile != null)
                        {
                            newPoly.Textures.Diffuse = poly.TextureFile.BitmapFilePair;
                            newPoly2.Textures.Diffuse = poly.TextureFile.BitmapFilePair;
                        }
                        mesh.AddPolygon(newPoly);
                        mesh.AddPolygon(newPoly2);
                    }

                    // Default
                    //newPoly.Bitmap = new Bitmap(2, 2);

                }
                else
                {
                    poly.IsBillboard = true;
                    //billboardPolys.Add(poly);
                    var newPoly = new Polygon(poly.GetPolygonIndex(0), poly.GetPolygonIndex(1), poly.GetPolygonIndex(2), poly.GetPolygonIndex(3));

                    for (int iIndex = 0; iIndex < poly.GetIndexAmount(); iIndex++)
                    {
                        var oldIndex = poly.GetPolygonIndex(iIndex);
                        var newIndex = new PolygonIndex(oldIndex.Id, oldIndex.DiffuseColor, oldIndex.TexCoord);
                        newPoly.SetIndex(iIndex, newIndex);
                    }
                    if (poly.HasFourIndices())
                    {
                        newPoly.SetAsQuad();
                    }

                    // Default
                    if (poly.TextureFile != null)
                    {
                        newPoly.Textures.Diffuse = poly.TextureFile.BitmapFilePair;
                    }

                    //mesh.AddPolygon(newPoly);
                    billboardPolys.Add(newPoly);
                }
            }
            foreach (var poly in billboardPolys)
            {
                // IS BILLBOARD
                var pos = mesh.VertexPositions[poly.GetPolygonIndex(0).Id];

                var vertPositions = CreateBillboardVertexPositions(pos, poly.GetIndices());
                int vertOffset = mesh.Vertices.Count;
                //mesh.Vertices.AddRange(verts);
                var norm = poly.CrossProduct;

                int i0 = vertOffset + 0;
                int i1 = vertOffset + 1;
                int i2 = vertOffset + 2;
                int i3 = vertOffset + 3;
                int i4 = vertOffset + 4;
                int i5 = vertOffset + 5;

                var t0 = poly.GetPolygonIndex(1).TexCoord;
                var t1 = poly.GetPolygonIndex(0).TexCoord;
                var t2 = poly.GetPolygonIndex(3).TexCoord;
                var t3 = poly.GetPolygonIndex(2).TexCoord;


                /*p0.SetIndex(0, new PolygonIndex(currentIndex + 0, Color.White, poly.GetPolygonIndex(1).TexCoord));
                p0.SetIndex(1, new PolygonIndex(currentIndex + 1, Color.White, poly.GetPolygonIndex(0).TexCoord));
                p0.SetIndex(2, new PolygonIndex(currentIndex + 2, Color.White, poly.GetPolygonIndex(2).TexCoord));
                p1.SetIndex(0, new PolygonIndex(currentIndex + 2, Color.White, poly.GetPolygonIndex(2).TexCoord));
                p1.SetIndex(1, new PolygonIndex(currentIndex + 1, Color.White, poly.GetPolygonIndex(0).TexCoord));
                p1.SetIndex(2, new PolygonIndex(currentIndex + 3, Color.White, poly.GetPolygonIndex(3).TexCoord));*/
                var newI0 = new PolygonIndex(i0, Color.White, t0);
                var newI1 = new PolygonIndex(i1, Color.White, t1);
                var newI2 = new PolygonIndex(i2, Color.White, t3);
                var newI3 = new PolygonIndex(i3, Color.White, t3);
                var newI4 = new PolygonIndex(i4, Color.White, t1);
                var newI5 = new PolygonIndex(i5, Color.White, t2);

                var vertex0 = new Vertex(vertPositions[0], norm, newI0.TexCoord.AsVector(), Color.White);
                var vertex1 = new Vertex(vertPositions[1], norm, newI1.TexCoord.AsVector(), Color.White);
                var vertex2 = new Vertex(vertPositions[2], norm, newI2.TexCoord.AsVector(), Color.White);
                var vertex3 = new Vertex(vertPositions[2], norm, newI3.TexCoord.AsVector(), Color.White);
                var vertex4 = new Vertex(vertPositions[1], norm, newI4.TexCoord.AsVector(), Color.White);
                var vertex5 = new Vertex(vertPositions[3], norm, newI5.TexCoord.AsVector(), Color.White);

                mesh.Vertices.Add(vertex0);
                mesh.Vertices.Add(vertex1);
                mesh.Vertices.Add(vertex2);
                mesh.Vertices.Add(vertex3);
                mesh.Vertices.Add(vertex4);
                mesh.Vertices.Add(vertex5);

                var p0 = new Polygon(newI0, newI1, newI2);
                var p1 = new Polygon(newI3, newI4, newI5);

                // Default
                if (poly.Textures.Diffuse.Bitmap != null)
                {
                    p0.Textures.Diffuse = poly.Textures.Diffuse;
                    p1.Textures.Diffuse = poly.Textures.Diffuse;
                }

                mesh.AddPolygon(p0);
                mesh.AddPolygon(p1);
            }

            return mesh;
        }

        private List<Vector3> CreateBillboardVertexPositions(System.Numerics.Vector3 position, PolygonIndex[] indices)
        {
            var verts = new List<Vector3>();

            float halfWidth = indices[1].Id;
            float halfHeight = indices[2].Id;

            var v0 = new Vector3(position.X - halfWidth, position.Y, halfHeight + position.Z);//, t0.X, t0.Y, 0.0f, 0.0f, 0.0f, indices[0].DiffuseColor); // Top Left
            var v1 = new Vector3(position.X + halfWidth, position.Y, halfHeight + position.Z);//, t1.X, t1.Y, 0.0f, 0.0f, 0.0f, indices[1].DiffuseColor); // Top Right
            var v2 = new Vector3(position.X - halfWidth, position.Y, -halfHeight + position.Z);//, t2.X, t2.Y, 0.0f, 0.0f, 0.0f, indices[2].DiffuseColor); // Bottom Left
            var v3 = new Vector3(position.X + halfWidth, position.Y, -halfHeight + position.Z);//, t3.X, t3.Y, 0.0f, 0.0f, 0.0f, indices[3].DiffuseColor); // Bottom Right

            verts.Add(v0);
            verts.Add(v1);
            verts.Add(v2);
            verts.Add(v3);

            return verts;
        }
    }
}