﻿namespace TMLibrary.Rendering
{
    public class FileHeader
    {
        private byte[] buffer;

        public FileHeader()
        {
            buffer = new byte[0];
        }

        public byte[] Buffer { get => buffer; set => buffer = value; }
    }
}
