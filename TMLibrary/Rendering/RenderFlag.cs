﻿namespace TMLibrary.Rendering
{
    public enum RenderFlag
    {
        Normal,
        Billboard,
        Billboard2
    }
}