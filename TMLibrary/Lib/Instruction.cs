﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Extensions;
using TMLibrary.Lib.Instructions.Dpc;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     An instruction is found in a mesh archive file. It's used to create a hierarchy of entities.
    /// </summary>
    public abstract class Instruction : IComparer<Instruction>, IComparable<Instruction>
    {
        protected uint nearDrawDistance;
        protected uint farDrawDistance;
        protected byte childAmount;
        protected List<long> childAddresses;
        protected long address;
        protected bool animationConfirmed;
        protected byte[] buffer;
        protected List<Instruction> children;
        protected List<Instruction> parents;
        protected bool executed;
        protected bool isRootInstruction;
        protected bool isAnimationContainer;
        protected bool isBoundingSphere;
        protected InstructionMatrix matrix;
        protected List<GameMesh> meshList;
        protected int parentCount;
        protected bool onlyShowLastChildInRenderer;
        protected bool showAnimationInRenderer;
        protected int timesExecuted;
        protected ProjectDefinition project;
        protected bool isMeshContainer;
        protected int bufferSize;
        protected bool isHighQualityAnimation;
        protected bool writtenToFile;
        protected Matrix4x4 globalMatrix;
        protected bool isMeshLink;

        public Instruction(ProjectDefinition project, long address)
        {
            childAddresses = new List<long>();
            this.project = project;
            timesExecuted = 0;
            parents = new List<Instruction>();
            children = new List<Instruction>();
            this.address = address;
            meshList = new List<GameMesh>();
            MatrixWrapper = new InstructionMatrix();
            globalMatrix = Matrix4x4.Identity;
        }

        public void WriteInstruction(BinaryWriter writer)
        {
            if (!WrittenToFile)
            {
                address = writer.BaseStream.Position;
                WriteInstructionContent(writer);
                WrittenToFile = true;
                WriteChildren(writer);
            }
        }

        public void WriteInstructionPointers(BinaryWriter writer)
        {
            writer.BaseStream.Position = address + bufferSize;
            foreach (var mesh in meshList)
            {
                writer.Write(Utility.CreatePointer(mesh.Address, project.MeshFile.Definition));
            }
            foreach (var child in children)
            {
                writer.Write(Utility.CreatePointer(child.Address, project.MeshFile.Definition));
            }
            foreach (var child in children)
            {
                child.WriteInstructionPointers(writer);
            }
        }

        protected abstract void WriteInstructionContent(BinaryWriter writer);
        public abstract string GetInstructionText();

        public int AllDescendantsCount()
        {
            return children.Count + meshList.Count;
        }

        public long Address
        {
            get => address;
            set => address = value;
        }

        public bool Executed
        {
            get => executed;
            set => executed = value;
        }

        public byte[] Buffer
        {
            get => buffer;
            set => buffer = value;
        }

        public List<Instruction> Parents => parents;
        public List<Instruction> Children => children;

        public int TimesExecuted
        {
            get => timesExecuted;
            set => timesExecuted = value;
        }

        public bool IsRootInstruction
        {
            get => isRootInstruction;
            set => isRootInstruction = value;
        }

        public int ParentCount
        {
            get => parentCount;
            set => parentCount = value;
        }

        public bool AnimationConfirmed
        {
            get => animationConfirmed;
            set => animationConfirmed = value;
        }

        public bool ShowAnimationInRenderer
        {
            get => showAnimationInRenderer;
            set => showAnimationInRenderer = value;
        }

        /// <summary>
        /// Gets the length of the instruction in memory including child pointers.
        /// </summary>
        /// <returns></returns>
        public uint GetTotalBufferLength()
        {
            uint length = (uint)(bufferSize + (children.Count + meshList.Count) * 4);

            return length;
        }

        public void ReadInstruction(BinaryReader reader)
        {
            ReadInstructionContent(reader);
        }
        protected abstract void ReadInstructionContent(BinaryReader reader);

        public InstructionMatrix MatrixWrapper
        {
            get { return matrix; }
            set { matrix = value; }
        }

        public List<long> ChildAddresses { get => childAddresses; }

        public int CompareTo(Instruction other)
        {
            return Compare(this, other);
        }

        public bool IsMeshContainer
        {
            get => isMeshContainer;
        }

        public bool IsAnimationContainer
        {
            get => isAnimationContainer;
        }

        public bool IsHighQualityAnimation
        {
            get => isHighQualityAnimation;
        }

        public int Compare(Instruction x, Instruction y)
        {
            if (x.address > y.address)
            {
                return 1;
            }
            if (x.address < y.address)
            {
                return -1;

            }
            return 0;
        }

        public List<GameMesh> GetAllMeshes()
        {
            var meshList = new List<GameMesh>();

            meshList.AddRange(this.meshList);

            //if (!isAnimationContainer)
            {
                foreach (var child in children)
                {
                    if (child != this)
                    {
                        meshList.AddRange(child.GetAllMeshes());
                    }
                }
            }

            return meshList;
        }

        protected void WritePointer(BinaryWriter writer, long address)
        {
            writer.Write(Utility.CreatePointer(address, project.MeshFile.Definition));
        }

        protected void WriteChildren(BinaryWriter writer)
        {
            long ptrStart = writer.BaseStream.Position;
            writer.SkipBytes(AllDescendantsCount() * 4);
            childAmount = (byte)(children.Count + meshList.Count);
            foreach (var child in Children)
            {
                child.WriteInstruction(writer);
            }
        }

        protected void WriteChildAmount(BinaryWriter writer)
        {
            writer.Write((byte)(Children.Count + MeshList.Count));
        }

        /// <summary>
        /// Gets the size of the instruction in bytes including children.
        /// </summary>
        /// <returns></returns>
        public uint GetRecursiveBufferLength()
        {
            uint bufferLength = 0;
            foreach (var child in children)
            {
                bufferLength += child.GetRecursiveBufferLength();
            }
            return GetTotalBufferLength() + bufferLength;
        }

        public byte ChildAmount
        {
            get
            {
                return childAmount;
            }
        }

        public void UpdateDrawDistance()
        {
            foreach (var child in children)
            {
                if (isMeshLink || MaxDrawDistance > 0)
                {
                    child.MinDrawDistance = MinDrawDistance;
                    child.MaxDrawDistance = MaxDrawDistance;
                }
                child.UpdateDrawDistance();
            }
        }

        public uint MinDrawDistance
        {
            get
            {
                return nearDrawDistance;
            }
            set
            {
                nearDrawDistance = value;
            }
        }

        public uint MaxDrawDistance
        {
            get
            {
                return farDrawDistance;
            }
            set
            {
                farDrawDistance = value;
            }
        }

        public List<GameMesh> MeshList { get => meshList; set => meshList = value; }
        public bool WrittenToFile { get => writtenToFile; set => writtenToFile = value; }
        public int BufferSize { get => bufferSize; }
        public Matrix4x4 GlobalMatrix { get => globalMatrix; set => globalMatrix = value; }
        public bool IsMeshLink { get => isMeshLink; }
        public bool IsBoundingSphere { get => isBoundingSphere; }
        public bool OnlyShowLastChildInRenderer { get => onlyShowLastChildInRenderer; }
    }
}