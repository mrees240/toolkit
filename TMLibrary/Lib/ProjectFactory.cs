﻿using MapEditor.Project;
using System.Collections.Generic;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.ProjectTypes;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     A factory that provides methods for creating different types of projects for different games.
    ///     The project types are stored in an array.
    /// </summary>
    public class ProjectFactory
    {
        public Dictionary<GameType, ProjectDefinition> LoadProjectTypes()
        {
            var projectTypes = new Dictionary<GameType, ProjectDefinition>();

            //projectTypes.Add("TM1PC", new Tm1Pc().CreateProjectType());
            projectTypes.Add(GameType.TM2PC, new Tm2PcProject().CreateProjectType());
            //projectTypes.Add("JM1PC", new JmPcProject().CreateProjectType());

            //projectTypes.Add("TM2PS1", new Tm2Ps1Project().CreateProjectType());
            //projectTypes.Add("OUTWARSPC", new OutwarsPcProject().CreateProjectType());

            return projectTypes;
        }
    }
}