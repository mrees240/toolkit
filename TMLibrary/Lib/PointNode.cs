﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.FileTypes.Point;

namespace TMLibrary.Lib
{
    public class PointNode
    {
        private Vector3 coordinate;
        private System.Drawing.Color color;
        private List<int> connectedNodeIds;
        private List<PointNode> connectedNodes;
        private long address;
        private int id;
        private ushort header;
        private byte parameter0, parameter1, parameter2, parameter3;
        private int originalChildAmount;

        public PointNode(ushort header, Vector3 coordinate, System.Drawing.Color color, long address, int id,
            byte parameter0, byte parameter1, byte parameter2, byte parameter3)
        {
            this.header = header;
            connectedNodes = new List<PointNode>();
            connectedNodeIds = new List<int>();
            this.coordinate = coordinate;
            this.color = color;
            this.address = address;
            this.id = id;
            this.parameter0 = parameter0;
            this.parameter1 = parameter1;
            this.parameter2 = parameter2;
            this.parameter3 = parameter3;
        }

        public PointNode(int id)
        {
            header = PointsFile.PointHeader1;
            this.id = id;
            this.color = System.Drawing.Color.Black;
        }

        public void AddChildNodeId(int nodeId)
        {
            if (connectedNodeIds.Count() >= 4)
            {
                throw new Exception($"Node ID: {id}. Nodes cannot have more than 4 children.");
            }
            originalChildAmount++;
            connectedNodeIds.Add(nodeId);
            connectedNodeIds = connectedNodeIds.OrderBy(n => n).ToList();
        }

        public Vector3 Coordinate { get => coordinate; }
        public System.Drawing.Color Color { get => color; }
        //public List<PointNode> ConnectedNodes { get => connectedNodes; }
        public int GetConnectedNodeAmount()
        {
            return connectedNodes.Count();
        }
        public void AddConnectedNode(PointNode node)
        {
            connectedNodes.Add(node);
            connectedNodeIds.Add(node.id);
        }
        public long Address { get => address; set => address = value; }
        public int Id { get => id; set => id = value; }
        public ushort Header { get => header; set => header = value; }
        public byte Parameter0 { get => parameter0; set => parameter0 = value; }
        public byte Parameter1 { get => parameter1; set => parameter1 = value; }
        public byte Parameter2 { get => parameter2; set => parameter2 = value; }
        public byte Parameter3 { get => parameter3; set => parameter3 = value; }
        public int OriginalChildAmount { get => originalChildAmount; }

        public int GetConnectedNodeId(int index)
        {
            return connectedNodeIds.ElementAt(index);
        }

        public bool HasAtLeaseOneConnection()
        {
            return connectedNodes.Count() >= 1;
        }

        public int MaxConnectedNodeId()
        {
            if (connectedNodeIds.Count() == 0)
            {
                return 0;
            }
            return connectedNodeIds.Max(v => v);
        }
        
        public bool HasEmptyConnectionIds()
        {
            return connectedNodeIds.Count() == 0 || connectedNodeIds.Max() == 0;
        }

        public int GetLastConnectedNodeId()
        {
            return connectedNodeIds.Last();
        }

        public int GetConnectedNodeIdAmount()
        {
            return connectedNodeIds.Count();
        }
    }
}
