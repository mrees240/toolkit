﻿using System.IO;
using TMLibrary.Rendering;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     Provides an interface for writing/updating meshes for a project definition.
    /// </summary>
    public interface IMeshWriter
    {
        void WriteMeshToFile(BinaryWriter writer, GameMesh mesh);
        void WriteNewMesh(BinaryWriter writer, GameMesh mesh);
        void WriteVertices(BinaryWriter writer, GameMesh mesh);
    }
}