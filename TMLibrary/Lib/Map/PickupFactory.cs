﻿using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMLibrary.Lib.Map.Project;

namespace TMLibrary.Map
{
    /// <summary>
    /// A class used to create pickups/items used in TM2. Assuming you are using an unmodified copy of Dish level.
    /// </summary>
    public class PickupFactory
    {
        private ProjectDefinition project;
        
        public PickupFactory(ProjectDefinition project)
        {
            this.project = project;
        }

        public DpcTranslationMatrix CreateGameItemInstruction(ObjectInstance instance, int xPos, int yPos, int zPos)
        {
            switch (instance.GameItemDefinition.ItemType)
            {
                case GameItemObjectType.Health:
                    return CreateTm2HealthPickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.Turbo:
                    return CreateTm2TurboPickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.FireMissle:
                    return CreateTm2FireMisslePickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.HomingMissle:
                    return CreateTm2HomingMisslePickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.Napalm:
                    return CreateTm2NapalmPickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.PowerMissle:
                    return CreateTm2PowerMisslePickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.Remote:
                    return CreateTm2RemotePickupInstruction(xPos, yPos, zPos);
                case GameItemObjectType.Ricochet:
                    return CreateTm2RicochetPickupInstruction(xPos, yPos, zPos);
            }
            throw new System.Exception($"{instance.GameItemDefinition.ItemType.ToString()} is not a recognized object type.");
        }

        public DpcTranslationMatrix CreateTm2TurboPickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.Turbo, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere turbo = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 52, 43, 0, 0, 0);
            turbo.MeshList.Add(project.MeshFile.Meshes[20]);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            animation.MeshList.Add(project.MeshFile.Meshes[21]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF08 coll = new DpcCollisionFF08(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 50);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 55);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 50);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 50);

            var meshContainer = new DpcMeshContainer602(project, 0);
            var meshLink = new DpcMeshLink(project, 0);
            meshLink.MeshList.Add(project.MeshFile.Meshes[22]);
            meshContainer.Children.Add(meshLink);
            translationInstruction.Children.Add(meshContainer);

            translationInstruction.Children.Add(turbo);
            turbo.Children.Add(animation);
            turbo.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2HealthPickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.Health, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere health = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 52, 111, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            animation.MeshList.Add(project.MeshFile.Meshes[166]);
            animation.MeshList.Add(project.MeshFile.Meshes[167]);

            DpcCollisionFF08 coll = new DpcCollisionFF08(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 60);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 55);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 60);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 55);
            coll.ConvexVolume.Planes[4] = new Plane(new Vector3(0, 0, -1), 128);
            coll.ConvexVolume.Planes[5] = new Plane(new Vector3(0, 0, 1), 23);

            translationInstruction.Children.Add(health);
            health.Children.Add(animation);
            health.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2FireMisslePickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.FireMissle, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere fireMissle = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 52, 43, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            fireMissle.MeshList.Add(project.MeshFile.Meshes[170]);
            animation.MeshList.Add(project.MeshFile.Meshes[171]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF08 coll = new DpcCollisionFF08(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 50);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 50);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 50);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 50);

            translationInstruction.Children.Add(fireMissle);
            fireMissle.Children.Add(animation);
            fireMissle.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2HomingMisslePickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.Homing, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere homingMissle = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 54, 29, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            homingMissle.MeshList.Add(project.MeshFile.Meshes[142]);
            animation.MeshList.Add(project.MeshFile.Meshes[143]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF06 coll = new DpcCollisionFF06(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(-1, 0, 0), 709);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, -1, 0), 80);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(1, 0, 0), -936);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, 1, 0), 80);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(-1, 0, 0), 1096);

            translationInstruction.Children.Add(homingMissle);
            homingMissle.Children.Add(animation);
            homingMissle.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2NapalmPickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.Napalm, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere napalm = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 52, 45, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            napalm.MeshList.Add(project.MeshFile.Meshes[168]);
            animation.MeshList.Add(project.MeshFile.Meshes[169]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF04 coll = new DpcCollisionFF04(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 50);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 50);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 50);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 50);

            translationInstruction.Children.Add(napalm);
            napalm.Children.Add(animation);
            napalm.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2RicochetPickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.Ricochet, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere ricochet = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 59, 43, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            ricochet.MeshList.Add(project.MeshFile.Meshes[174]);
            animation.MeshList.Add(project.MeshFile.Meshes[175]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF04 coll = new DpcCollisionFF04(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 50);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 50);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 50);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 50);

            translationInstruction.Children.Add(ricochet);
            ricochet.Children.Add(animation);
            ricochet.Children.Add(coll);

            return translationInstruction;
        }
        
        public DpcTranslationMatrix CreateTm2PowerMisslePickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.PowerMissle, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere powerMissle = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 50, 27, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            powerMissle.MeshList.Add(project.MeshFile.Meshes[146]);
            animation.MeshList.Add(project.MeshFile.Meshes[147]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF05 coll = new DpcCollisionFF05(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 62);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 62);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 62);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 62);
            coll.ConvexVolume.Planes[4] = new Plane(new Vector3(0, 0, 1), -656);

            translationInstruction.Children.Add(powerMissle);
            powerMissle.Children.Add(animation);
            powerMissle.Children.Add(coll);

            return translationInstruction;
        }

        public DpcTranslationMatrix CreateTm2RemotePickupInstruction(int xPos, int yPos, int zPos)
        {
            DpcTranslationMatrix translationInstruction = new DpcTranslationMatrix(project, 0, MatrixType.RemoteExplosive, new Vector3(xPos, yPos, zPos), 0, 0, 0);
            Dpc701BoundingSphere remote = new Dpc701BoundingSphere(project, 0, 0, 0, 0, 50, 27, 0, 0, 0);
            DpcAnimationContainer animation = new DpcAnimationContainer(project, 0);
            remote.MeshList.Add(project.MeshFile.Meshes[144]);
            animation.MeshList.Add(project.MeshFile.Meshes[145]);
            var emptyUnknown = new Dpc701BoundingSphere(project, 0);
            animation.Children.Add(emptyUnknown);

            DpcCollisionFF05 coll = new DpcCollisionFF05(project, 0);
            coll.Header1 = 65286;
            coll.ConvexVolume.Planes[0] = new Plane(new Vector3(1, 0, 0), 62);
            coll.ConvexVolume.Planes[1] = new Plane(new Vector3(0, 1, 0), 62);
            coll.ConvexVolume.Planes[2] = new Plane(new Vector3(-1, 0, 0), 62);
            coll.ConvexVolume.Planes[3] = new Plane(new Vector3(0, -1, 0), 62);
            coll.ConvexVolume.Planes[4] = new Plane(new Vector3(0, 0, 1), -656);

            translationInstruction.Children.Add(remote);
            remote.Children.Add(animation);
            remote.Children.Add(coll);

            return translationInstruction;
        }
    }
}
