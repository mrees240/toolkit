﻿using System.Numerics;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.ProjectTypes.Tm2Pc.Map
{
    /// <summary>
    /// Used by the map compiler to determine if a path point should be used as a spawn point or not.
    /// </summary>
    public class SpawnMesh
    {
        private GameMesh mesh;
        private string nodeName;

        public string NodeName { get => nodeName; }

        public SpawnMesh(GameMesh mesh, string nodeName)
        {
            this.mesh = mesh;
            this.nodeName = nodeName;
        }

        public bool ContainsVertex(Vector3 vertex)
        {
            return mesh.ContainsVertex(vertex);
        }
    }
}
