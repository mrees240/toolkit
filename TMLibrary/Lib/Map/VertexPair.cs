﻿using System.Numerics;

namespace TMLibrary.Lib.ProjectTypes.Tm2Pc.Map
{
    /// <summary>
    /// A pair of vertices used to determine siblings in PTS files.
    /// </summary>
    public class VertexPair
    {
        private Vector3 v0, v1;

        public VertexPair(Vector3 v0, Vector3 v1)
        {
            this.v0 = v0;
            this.v1 = v1;
        }

        public Vector3 V0 { get => v0; }
        public Vector3 V1 { get => v1; }
    }
}
