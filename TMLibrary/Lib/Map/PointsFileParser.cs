﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib;
using TMLibrary.Lib.FileTypes.Point;
using TMLibrary.Lib.ProjectTypes.Tm2Pc.Map;

namespace TMLibrary.Map
{
    public class PointsFileParser
    {
        private List<Vector3> aiNodes;
        private List<Vector3> allNodes;
        private List<VertexPair> pairs;
        private bool onSpawnList;
        private MapCompilerResult mapCompilerResult;
        private readonly int SiblingLimit = 4;

        public PointsFileParser(List<Vector3> aiNodes, List<VertexPair> pairs, MapCompilerResult mapCompilerResult)
        {
            this.aiNodes = aiNodes;
            this.pairs = pairs;
            this.mapCompilerResult = mapCompilerResult;
        }
        
        public void WriteToPointFile(string fileName)
        {
            var pointsFile = new PointsFile();
            var allNodes = new List<PointNode>();

            for (int iNode = 0; iNode < aiNodes.Count(); iNode++)
            {
                var vert = aiNodes[iNode];
                ushort header = PointsFile.PointHeader1;
                PointNode pointNode = new PointNode(header, vert, System.Drawing.Color.White, 0, iNode, 0, 0, 0, 0);

                var currentPairs = pairs.Where(v => v.V0 == vert || v.V1 == vert);
                if (currentPairs.Count() == 0)
                {
                    mapCompilerResult.AddMessage("One of your path points isn't connected to any others. This could cause the game to freeze. Make sure each point has at least one edge connecting to another.");
                }
                for (int i = 0; i < SiblingLimit; i++)
                {
                    if (i < currentPairs.Count())
                    {
                        var pair = currentPairs.ElementAt(i);

                        if (vert == pair.V0)
                        {
                            var otherVert = pair.V1;
                            int index = aiNodes.IndexOf(otherVert);
                            pointNode.AddChildNodeId(index);
                        }
                        else
                        {
                            var otherVert = pair.V0;
                            int index = aiNodes.IndexOf(otherVert);
                            pointNode.AddChildNodeId(index);
                        }
                    }
                    else
                    {
                        if (pointNode.GetConnectedNodeAmount() == 0)
                        {
                            pointNode.AddChildNodeId(iNode);
                        }
                        else
                        {
                            pointNode.AddChildNodeId(iNode);
                        }
                    }
                }

                allNodes.Add(pointNode);
            }

            using (var writer = new BinaryWriter(new FileStream(fileName, FileMode.Create)))
            {
                new PointsFileController(pointsFile).WritePoints(writer, allNodes);
            }
        }

        private Vector3 ParseVertex(string[] splitLine)
        {
            return new Vector3(
                (int)float.Parse(splitLine[1]),
                (int)float.Parse(splitLine[2]),
                (int)float.Parse(splitLine[3]));
        }

        private VertexPair ParseVertexPair(string[] splitLine)
        {
            int i0 = int.Parse(splitLine[1]);
            int i1 = int.Parse(splitLine[2]);
            return new VertexPair(allNodes[i0 - 1], allNodes[i1 - 1]);
        }
    }
}
