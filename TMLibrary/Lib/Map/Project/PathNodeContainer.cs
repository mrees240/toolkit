﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project.Xml;

namespace TMLibrary.Lib.Map.Project
{
    public class PathNodeContainer
    {
        private List<PathNode> pathNodes;

        public PathNodeContainer()
        {
            pathNodes = new List<PathNode>();
        }

        [XmlElement("path_node", Type = typeof(PathNodeXml))]
        public List<PathNode> PathNodes { get { return pathNodes; } set { pathNodes = value; } }
    }
}
