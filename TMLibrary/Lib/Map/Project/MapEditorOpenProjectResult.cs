﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.Map.Project
{
    public class MapEditorOpenProjectResult
    {
        private bool succeeded;
        private List<string> messages;

        public MapEditorOpenProjectResult()
        {
            messages = new List<string>();
            succeeded = true;
        }

        public void AddMessage(string message)
        {
            this.messages.Add(message);
        }

        public bool Succeeded { get => succeeded; set => succeeded = value; }
        public List<string> Messages { get => messages; }
    }
}
