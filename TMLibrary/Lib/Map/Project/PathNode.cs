﻿using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project
{
    public class PathNode : MeshObject
    {
        public PathNode() : base()
        {
        }

        public PathNode(string name) : this()
        {
            this.Name = name;
        }
        
        public bool IsSpawnPoint { get; set; }
        new public string Name { get; set; }
    }
}
