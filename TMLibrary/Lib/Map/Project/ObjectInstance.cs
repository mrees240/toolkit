﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using ColladaLibrary.Xml;
using MapEditor.Project;

namespace TMLibrary.Lib.Map.Project
{
    public class ObjectInstance
    {
        private ObjectInstance parent;
        private int idNumber;

        // Local transformations
        private Transformation transformation;

        public ObjectInstance()
        {
            ObjectType = ObjectType.DefaultObject;
            Children = new List<ObjectInstance>();
            transformation = new Transformation();
            ItemType = GameItemObjectType.Default;
            GameItemDefinition = new GameItemDefinition();
            ConnectedNodes = new List<ObjectInstance>();
        }

        public ObjectInstance(string name, int id) : this()
        {
            this.idNumber = id;
            this.Name = name;

            if (ObjectType == ObjectType.PathNode)
            {
                PathNode.Name = name;
            }
        }

        public ObjectInstance(StaticObjectDefinition staticObject, string name, int id) : this(name, id)
        {
            this.StaticObject = staticObject;
            ObjectType = ObjectType.StaticObject;
        }

        public ObjectInstance(GameItemDefinition gameItemObject, string name, int id) : this(name, id)
        {
            this.GameItemDefinition = gameItemObject;
            ObjectType = ObjectType.GameItemObject;
            ItemType = gameItemObject.ItemType;
        }

        public ObjectInstance(ObjectInstance parent, PathNode pathNode, string name, int id) : this(name, id)
        {
            this.PathNode = pathNode;
            ObjectType = ObjectType.PathNode;
        }

        public List<Mesh> GetMeshes()
        {
            switch (ObjectType)
            {
                case ObjectType.StaticObject:
                    return StaticObject.Meshes;
                case ObjectType.GameItemObject:
                    return GameItemDefinition.Meshes;
                case ObjectType.PathNode:
                    return PathNode.Meshes;
            }

            return new List<Mesh>();
        }

        [XmlElement("object_type")]
        public ObjectType ObjectType
        {
            get; set;
        }

        [XmlElement("item_type")]
        public GameItemObjectType ItemType
        {
            get;
            set;
        }

        /*public void SetLocalMatrix(Matrix4x4 localMatrix)
        {
            this.LocalMatrix = localMatrix;
        }*/

        public void AddChildren(List<ObjectInstance> children)
        {
            foreach (var child in children)
            {
                AddChild(child);
            }
        }

        public void RemoveChildren(List<ObjectInstance> children)
        {
            foreach (var child in children)
            {
                RemoveChild(child);
            }
        }

        public void AddChild(ObjectInstance child)
        {
            child.parent = this;
            Children.Add(child);
        }

        public void RemoveChild(ObjectInstance child)
        {
            child.parent = null;
            Children.Remove(child);
        }

        public string StaticObjectDefinitionName { get; set; }

        [XmlIgnore]
        public StaticObjectDefinition StaticObject
        {
            get; set;
        }

        public void UpdateGameItemDefinition(Dictionary<GameItemObjectType, GameItemDefinition> definitions, GameItemObjectType itemType)
        {
            this.GameItemDefinition = definitions[itemType];
        }

        public void UpdateMatrices(Matrix4x4 matrixStack)
        {
            var mat = Matrix4x4.Multiply(matrixStack, transformation.GetLocalMatrix());
            transformation.SetWorldMatrix(mat);

            foreach (var child in Children)
            {
                child.UpdateMatrices(mat);
            }
        }

        public void ResetConnections()
        {
            ConnectedNodes.Clear();
            ConnectedNodeNames.Clear();
        }

        public void AddConnection(ObjectInstance obj)
        {
            ConnectedNodes.Add(obj);
            ConnectedNodeNames.Add(obj.Name);
        }

        public void RemoveConnection(ObjectInstance obj)
        {
            ConnectedNodeNames.Remove(obj.Name);
            ConnectedNodes.Remove(obj);
        }

        public ObjectInstance GetConnectedObject(int index)
        {
            return ConnectedNodes[index];
        }

        [XmlIgnore]
        public string Name { get; set; }
        [XmlIgnore]
        public GameItemDefinition GameItemDefinition { get; set; }
        [XmlIgnore]
        public PathNode PathNode { get; set; }
        [XmlIgnore]
        public List<ObjectInstance> ConnectedNodes { get; set; }
        [XmlIgnore]
        public List<string> ConnectedNodeNames { get; set; }

        [XmlIgnore]
        public List<ObjectInstance> Children { get; set; }
        public Transformation Transformation { get => transformation; }
        public int IdNumber { get => idNumber; set => idNumber = value; }
    }
}
