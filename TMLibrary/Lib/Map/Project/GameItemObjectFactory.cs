﻿using ColladaLibrary.Collada;
using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.Map.Project
{
    public class GameItemObjectFactory
    {
        public Dictionary<GameItemObjectType, GameItemDefinition> CreateTm2GameItemObjects()
        {
            var tm2GameObjects = new Dictionary<GameItemObjectType, GameItemDefinition>();

            // HEALTH
            var healthBmp = new BitmapFilePair();
            healthBmp.FileName = @"Resources\Games\Tm2\health_0.bmp";
            healthBmp.Bitmap = new System.Drawing.Bitmap(healthBmp.FileName);
            var healthBillboard = new BillboardInfo();
            healthBillboard.Width = 120;
            healthBillboard.Height = 118;
            healthBillboard.ZOffset = 56;
            healthBillboard.Texture = healthBmp;

            // TURBO
            var turboBmpImg = new BitmapFilePair();
            var turboBmpText = new BitmapFilePair();
            turboBmpImg.FileName = @"Resources\Games\Tm2\turbo_img.bmp";
            turboBmpText.FileName = @"Resources\Games\Tm2\turbo_text.bmp";
            turboBmpImg.Bitmap = new System.Drawing.Bitmap(turboBmpImg.FileName);
            turboBmpText.Bitmap = new System.Drawing.Bitmap(turboBmpText.FileName);
            var turboImgBillboard = new BillboardInfo();
            var turboTextBillboard = new BillboardInfo();
            turboTextBillboard.Width = 86;
            turboTextBillboard.Height = 32;
            turboTextBillboard.Texture = turboBmpText;
            turboImgBillboard.Width = 96;
            turboImgBillboard.Height = 64;
            turboTextBillboard.YOffset = -5;
            turboImgBillboard.ZOffset = 32;
            turboImgBillboard.Texture = turboBmpImg;

            // FIRE MISSLE
            var fireMissleTextBillboard = new BillboardInfo(86.0f, 32.0f, 0.0f, -5.0f, 58.0f, @"Resources\Games\Tm2\fire_text.bmp");
            var fireMissleImgBillboard = new BillboardInfo(40.0f, 100.0f, 0.0f, 0.0f, 51.0f, @"Resources\Games\Tm2\fire_img.bmp");

            // HOMING MISSLE
            var homingMissleTextBillboard = new BillboardInfo(104.0f, 32.0f, 0.0f, -5.0f, 58.0f, @"Resources\Games\Tm2\homing_text.bmp");
            var homingMissleImgBillboard = new BillboardInfo(40.0f, 100.0f, 0.0f, 0.0f, 51.0f, @"Resources\Games\Tm2\homing_img.bmp");

            // NAPALM
            var napalmTextBillboard = new BillboardInfo(104.0f, 32.0f, 0.0f, 0.0f, 16.0f, @"Resources\Games\Tm2\napalm_text.bmp");
            var napalmImgBillboard = new BillboardInfo(82.0f, 94.0f, 0.0f, 0.0f, 79.0f, @"Resources\Games\Tm2\napalm_img.bmp");

            // POWER MISSLE
            var powerMissleTextBillboard = new BillboardInfo(86.0f, 32.0f, 0.0f, -5.0f, 58.0f, @"Resources\Games\Tm2\power_text.bmp");
            var powerMissleImgBillboard = new BillboardInfo(40.0f, 100.0f, 0.0f, 0.0f, 51.0f, @"Resources\Games\Tm2\power_img.bmp");

            // REMOTE
            var remoteTextBillboard = new BillboardInfo(96.0f, 32.0f, 0.0f, -5.0f, 16, @"Resources\Games\Tm2\remote_text.bmp");
            var remoteImgBillboard = new BillboardInfo(96.0f, 48.0f, 0.0f, 0.0f, 56, @"Resources\Games\Tm2\remote_img.bmp");

            // RICOCHET
            var ricochetTextBillboard = new BillboardInfo(104.0f, 32.0f, 0.0f, -5.0f, 35, @"Resources\Games\Tm2\ricochet_text.bmp");
            var ricochetImgBillboard = new BillboardInfo(84.0f, 98.0f, 0.0f, 0.0f, 50, @"Resources\Games\Tm2\ricochet_img.bmp");


            // TURBO
            var turboBillboards = new List<BillboardInfo>();
            turboBillboards.Add(turboTextBillboard);
            turboBillboards.Add(turboImgBillboard);

            var fireBillboards = new List<BillboardInfo>()
            {
                fireMissleTextBillboard,
                fireMissleImgBillboard
            };
            var homingBillboards = new List<BillboardInfo>()
            {
                homingMissleTextBillboard,
                homingMissleImgBillboard
            };
            var napalmBillboards = new List<BillboardInfo>()
            {
                napalmTextBillboard,
                napalmImgBillboard
            };
            var powerBillboards = new List<BillboardInfo>()
            {
                powerMissleTextBillboard,
                powerMissleImgBillboard
            };
            var remoteBillboards = new List<BillboardInfo>()
            {
                remoteTextBillboard,
                remoteImgBillboard
            };
            var ricochetBillboards = new List<BillboardInfo>()
            {
                ricochetTextBillboard,
                ricochetImgBillboard
            };

            var healthPickup = new GameItemDefinition("Health", "../Resources/Games/Tm2/health.bmp", healthBillboard, GameItemObjectType.Health);
            var turboPickup = new GameItemDefinition("Turbo", "../Resources/Games/Tm2/turbo.bmp", turboBillboards, GameItemObjectType.Turbo);
            var firePickup = new GameItemDefinition("Fire Missle", "../Resources/Games/Tm2/fire.bmp", fireBillboards, GameItemObjectType.FireMissle);
            var homingPickup = new GameItemDefinition("Homing Missle", "../Resources/Games/Tm2/homing.bmp", homingBillboards, GameItemObjectType.HomingMissle);
            var napalmPickup = new GameItemDefinition("Napalm", "../Resources/Games/Tm2/napalm.bmp", napalmBillboards, GameItemObjectType.Napalm);
            var powerPickup = new GameItemDefinition("Power Missle", "../Resources/Games/Tm2/power.bmp", powerBillboards, GameItemObjectType.PowerMissle);
            var remotePickup = new GameItemDefinition("Remote", "../Resources/Games/Tm2/remote.bmp", remoteBillboards, GameItemObjectType.Remote);
            var ricochetPickup = new GameItemDefinition("Ricochet", "../Resources/Games/Tm2/ricochet.bmp", ricochetBillboards, GameItemObjectType.Ricochet);

            healthPickup.TranslationOnly = true;
            turboPickup.TranslationOnly = true;
            firePickup.TranslationOnly = true;
            homingPickup.TranslationOnly = true;
            napalmPickup.TranslationOnly = true;
            powerPickup.TranslationOnly = true;
            remotePickup.TranslationOnly = true;
            ricochetPickup.TranslationOnly = true;

            tm2GameObjects.Add(healthPickup.ItemType, healthPickup);
            tm2GameObjects.Add(turboPickup.ItemType, turboPickup);
            tm2GameObjects.Add(firePickup.ItemType, firePickup);
            tm2GameObjects.Add(homingPickup.ItemType, homingPickup);
            tm2GameObjects.Add(napalmPickup.ItemType, napalmPickup);
            tm2GameObjects.Add(powerPickup.ItemType, powerPickup);
            tm2GameObjects.Add(remotePickup.ItemType, remotePickup);
            tm2GameObjects.Add(ricochetPickup.ItemType, ricochetPickup);

            foreach (var obj in tm2GameObjects)
            {
                bool isBillboard = obj.Value.BillboardInfoList != null;

                if (!isBillboard)
                {
                    var colladaImporter = new ColladaImporter(false, true, obj.Value.ModelUrl);
                    colladaImporter.Load();

                    foreach (var tex in colladaImporter.AllMaterials)
                    {
                        tex.Effect.Image.BitmapFile.TransparencyEnabled = true;
                    }

                    foreach (var mesh in colladaImporter.AllMeshes)
                    {
                        var transformedMesh = new Mesh(mesh);

                        obj.Value.Meshes.Add(transformedMesh);
                    }
                }
                else
                {
                    foreach (var billboard in obj.Value.BillboardInfoList)
                    {
                        billboard.Texture.TransparencyEnabled = true;
                    }
                }
            }

            return tm2GameObjects;
        }
    }
}
