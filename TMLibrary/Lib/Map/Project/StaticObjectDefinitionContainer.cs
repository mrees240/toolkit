﻿using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project.Xml;

namespace TMLibrary.Lib.Map.Project
{
    public class StaticObjectDefinitionContainer
    {
        private List<StaticObjectDefinition> staticObjectDefinitions;

        public StaticObjectDefinitionContainer()
        {
            staticObjectDefinitions = new List<StaticObjectDefinition>();
        }

        [XmlElement("static_object_definition",Type =typeof(StaticObjectDefinitionXml))]
        public List<StaticObjectDefinition> StaticObjectDefinitions { get { return staticObjectDefinitions; } set { staticObjectDefinitions = value; } }
    }
}
