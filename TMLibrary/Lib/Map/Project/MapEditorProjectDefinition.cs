﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project;

namespace MapEditor.Project
{
    public class MapEditorProjectDefinition
    {
        private MapEditorProjectType projectType;
        private string name;
        private Dictionary<GameItemObjectType, GameItemDefinition> gameItemObjects;

        public MapEditorProjectDefinition()
        {

        }

        public MapEditorProjectDefinition(string name, MapEditorProjectType projectType, Dictionary<GameItemObjectType, GameItemDefinition> gameItemObjects)
        {
            this.projectType = projectType;
            this.name = name;
            this.gameItemObjects = gameItemObjects;
        }

        [XmlIgnore]
        public Dictionary<GameItemObjectType, GameItemDefinition> GameItemObjects { get => gameItemObjects; }

        [XmlAttribute("type")]
        public MapEditorProjectType ProjectType { get => projectType; set => projectType = value; }
    }
}
