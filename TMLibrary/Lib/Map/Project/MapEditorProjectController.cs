﻿using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project.Xml;

namespace TMLibrary.Lib.Map.Project
{
    public class MapEditorProjectController
    {
        private MapEditorProject project;
        private StringBuilder sb;

        public MapEditorProjectController(MapEditorProject project)
        {
            this.project = project;
        }

        public void SaveProjectToFile(string fileName)
        {
            var serializer = new XmlSerializer(typeof(MapEditorProject));

            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(stream, project);
            }
        }

        public void RemoveObjectInstance(ObjectInstance parent, ObjectInstance objectInstance)
        {
            parent.Children.Remove(objectInstance);
        }

        public void AddObjectInstance(ObjectInstance parent, ObjectInstance objectInstance)
        {
            project.AddObjectInstance(parent, objectInstance);
        }

        public MapEditorOpenProjectResult OpenProjectFile(string fileUrl, Version appVersion, MapEditorProjectDefinition definition)
        {
            var result = new MapEditorOpenProjectResult();
            ResetProject();
            DeserializeProjectFile(fileUrl, appVersion, definition);

            var objectInstances = project.GetAllObjectInstances();

            foreach (var staticObjectDefinition in project.StaticObjectDefinitionContainer.StaticObjectDefinitions)
            {
                staticObjectDefinition.MeshLoaded = false;
                //staticObjectDefinition.UpdateMeshes();
            }

            // Update game item definitions
            foreach (var obj in objectInstances)
            {
                foreach (var child in obj.Children)
                {
                    if (child.ObjectType == ObjectType.StaticObject)
                    {
                        var staticObjectDefinition = project.StaticObjectDefinitionContainer.StaticObjectDefinitions.Where(d => d.Name == child.StaticObjectDefinitionName).FirstOrDefault();

                        if (staticObjectDefinition != null)
                        {
                            child.StaticObject = staticObjectDefinition;
                        }
                        else
                        {
                            result.Succeeded = false;
                            result.AddMessage($"Cannot find static object definition {child.StaticObjectDefinitionName} for object instance {child.Name}.");
                        }
                    }
                    if (child.ObjectType == ObjectType.GameItemObject)
                    {
                        var itemType = child.GameItemDefinition.ItemType;
                        if (itemType == GameItemObjectType.Default)
                        {
                            result.Succeeded = false;
                            result.AddMessage($"{child.Name} does not have a defined game item type.");
                        }
                        else
                        {
                            child.GameItemDefinition = project.ProjectDefinition.GameItemObjects[itemType];
                        }
                    }
                    if (child.ObjectType == ObjectType.PathNode)
                    {
                        var pathNode = project.PathNodeContainer.PathNodes.Where(p => p.Name == child.Name).FirstOrDefault();
                        if (pathNode != null)
                        {
                            child.PathNode = pathNode;
                        }
                        else
                        {
                            result.Succeeded = false;
                            result.AddMessage($"Unable to find path node {child.Name} in path node definitions.");
                        }
                    }
                }
            }

            return result;
        }

        private void DeserializeProjectFile(string fileUrl, Version appVersion, MapEditorProjectDefinition definition)
        {
            var xmlSerializer = new XmlSerializer(typeof(MapEditorProject));
            if (!File.Exists(fileUrl))
            {
                fileUrl = Directory.GetCurrentDirectory() + fileUrl;
            }

            using (var stream = new FileStream(fileUrl, FileMode.Open))
            {
                var deserializedProject = (MapEditorProject)xmlSerializer.Deserialize(stream);

                var fileVersion = new Version(deserializedProject.FileVersion);

                bool fileVersionIsTooNew = false;

                if (fileVersion.Major > appVersion.Major)
                {
                    fileVersionIsTooNew = true;
                }
                else if (fileVersion.Major == appVersion.Major)
                {
                    if (fileVersion.Minor > appVersion.Minor)
                    {
                        fileVersionIsTooNew = true;
                    }
                }

                if (fileVersionIsTooNew)
                {
                    throw new Exception(
                        $"File version is newer than your Map Editor's version. You will need to download the newest version of the Map Editor to open it.\n\n" +
                        $"File version: {fileVersion.ToString()}. Your Map Editor's version: {appVersion.ToString()}."
                        );
                }
                project.UpdateProjectDefinition(definition);
                project.PathNodeContainer.PathNodes.AddRange(deserializedProject.PathNodeContainer.PathNodes);
                project.StaticObjectDefinitionContainer.StaticObjectDefinitions.AddRange(deserializedProject.StaticObjectDefinitionContainer.StaticObjectDefinitions);
                project.AmbientLight.SetLight(deserializedProject.AmbientLight);
                project.SunLight.SetLight(deserializedProject.SunLight);
                project.RootNode.Children.Clear();
                project.RootNode.AddChildren(deserializedProject.RootNode.Children);
                project.PathConnectionContainer = deserializedProject.PathConnectionContainer;

                project.SkySettings.FrontSide.TextureUrl = deserializedProject.SkySettings.FrontSide.TextureUrl;
                project.SkySettings.BackSide.TextureUrl = deserializedProject.SkySettings.BackSide.TextureUrl;
                project.SkySettings.LeftSide.TextureUrl = deserializedProject.SkySettings.LeftSide.TextureUrl;
                project.SkySettings.RightSide.TextureUrl = deserializedProject.SkySettings.RightSide.TextureUrl;
                project.SkySettings.Top.TextureUrl = deserializedProject.SkySettings.Top.TextureUrl;
                project.SkySettings.Bottom.TextureUrl = deserializedProject.SkySettings.Bottom.TextureUrl;

                var pathNodeDictionary = project.GetAllObjectInstances().Where(i => i.ObjectType == ObjectType.PathNode).ToDictionary(p => p.Name);

                foreach (var connection in project.PathConnectionContainer.PathNodeConnections)
                {
                    connection.P0 = pathNodeDictionary[connection.Name0];
                    connection.P1 = pathNodeDictionary[connection.Name1];

                    if (connection.Direction == PathConnectionDirection.P0ToP1)
                    {
                        connection.P0.AddConnection(connection.P1);
                    }
                    else if (connection.Direction == PathConnectionDirection.P1ToP0)
                    {
                        connection.P1.AddConnection(connection.P0);
                    }
                    else
                    {
                        connection.P0.AddConnection(connection.P1);
                        connection.P1.AddConnection(connection.P0);
                    }
                }
            }
        }

        public void ResetProject()
        {
            project.ResetProject();
        }
    }
}
