﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;
using TMLibrary.Lib.ProjectTypes;
using TMLibrary.Map;

namespace MapEditor.Project
{

    public class ProjectDefinitionFactory
    {

        public Dictionary<GameType, MapEditorProjectDefinition> CreateProjectDefinitions()
        {
            var definitions = new Dictionary<GameType, MapEditorProjectDefinition>();

            var tm2PcDefinition = CreateTm2PcDefinition();
            definitions.Add(GameType.TM2PC, tm2PcDefinition);

            return definitions;
        }

        private MapEditorProjectDefinition CreateTm2PcDefinition()
        {
            var gameItemFactory = new GameItemObjectFactory();
            var tm2PcDefinition = new MapEditorProjectDefinition("Twisted Metal 2 (PC)", MapEditorProjectType.Twisted_Metal_2_Pc,
                gameItemFactory.CreateTm2GameItemObjects());

            return tm2PcDefinition;
        }
    }
}
