﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.Map.Project
{
    public enum ObjectType
    {
        DefaultObject, StaticObject, GameItemObject, PathNode
    };
}
