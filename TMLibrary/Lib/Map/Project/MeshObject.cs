﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using Kaliko.ImageLibrary.Scaling;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.Map.Project
{
    public abstract class MeshObject
    {
        protected bool meshLoaded = false;
        protected bool collisionMeshLoaded = false;
        private List<BillboardInfo> billboardInfoList;
        private List<FileSystemWatcher> modelFileWatchers;
        private List<FileSystemWatcher> collisionFileWatchers;
        private string modelUrl;
        private string collisionModelUrl;

        public MeshObject()
        {
            modelFileWatchers = new List<FileSystemWatcher>();
            collisionFileWatchers = new List<FileSystemWatcher>();
            billboardInfoList = new List<BillboardInfo>();
            Meshes = new List<Mesh>();
            CollisionMeshes = new List<Mesh>();
            modelUrl = string.Empty;
            collisionModelUrl = string.Empty;
        }

        public bool MeshSelected()
        {
            return ModelUrl != null && ModelUrl.Length > 0;
        }

        private void UpdateCollisionMeshes()
        {
            var colladaImporter = new ColladaImporter(false, true, CollisionModelUrl);

            colladaImporter.Load(true);
            CollisionMeshes.Clear();
            CollisionMeshes.Add(colladaImporter.CreateSingleMesh());
            collisionMeshLoaded = true;
        }

        private void UpdateVisualMesh()
        {
            while (!meshLoaded)
            {
                if (!File.Exists(ModelUrl))
                {
                    ResetModel();
                    break;
                }
                if (!FileUtility.IsFileLocked(ModelUrl))
                {
                    var colladaImporter = new ColladaImporter(false, true, ModelUrl);

                    colladaImporter.Load(true);
                    for (int iBitmap = 0; iBitmap < colladaImporter.NewTextures.Count; iBitmap++)
                    {
                        var newTex = colladaImporter.NewTextures[iBitmap];

                        colladaImporter.NewTextures[iBitmap] = newTex;
                    }
                    Meshes.Clear();
                    Meshes.Add(colladaImporter.CreateSingleMesh());
                    meshLoaded = true;

                    UpdateModelFileWatchers(colladaImporter);

                    OnMeshChanged();
                    /*try
                    {
                        colladaImporter.Load(true);
                        for (int iBitmap = 0; iBitmap < colladaImporter.NewTextures.Count; iBitmap++)
                        {
                            var newTex = colladaImporter.NewTextures[iBitmap];

                            colladaImporter.NewTextures[iBitmap] = newTex;
                        }
                        Meshes.Clear();
                        Meshes.Add(colladaImporter.CreateSingleMesh());
                        meshLoaded = true;

                        UpdateModelFileWatchers(colladaImporter);

                        OnMeshChanged();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(LogFileUtility.CreateErrorMessage($"An error occured when loading {ModelUrl}."));
                        LogFileUtility.AppendToLogFile($"{ex.Message}");
                        ResetModel();
                    }*/
                }
            }
        }

        public void UpdateMeshes()
        {
            if (File.Exists(CollisionModelUrl) && !collisionMeshLoaded)
            {
                UpdateCollisionMeshes();
            }
            if (File.Exists(ModelUrl) && !meshLoaded)
            {
                UpdateVisualMesh();
            }
        }

        public void UpdateMeshesAsync()
        {
            //new Task(() =>
            {
                UpdateMeshes();
            }//).Start();
        }

        [XmlIgnore]
        public string Name { get; set; }
        [XmlIgnore]
        public List<Mesh> CollisionMeshes { get; set; }
        [XmlIgnore]
        public List<Mesh> Meshes { get; set; }
        [XmlIgnore]
        public bool MeshLoaded { get => meshLoaded; set => meshLoaded = value; }

        public event System.EventHandler MeshChanged;

        public virtual void OnMeshChanged()
        {
            if (MeshChanged != null)
            {
                MeshChanged(this, EventArgs.Empty);
            }
        }

        public void SetBillboard(BillboardInfo billboard)
        {
            SetBillboard(new List<BillboardInfo> { billboard });
        }

        public void SetBillboard(List<BillboardInfo> billboards)
        {
            this.billboardInfoList.Clear();
            this.billboardInfoList.AddRange(billboards);
        }

        public void UpdateCollisionModelUrl(string collisionModelUrl)
        {
            collisionMeshLoaded = false;
            CollisionModelUrl = collisionModelUrl;
            UpdateCollisionMeshes();
            UpdateCollisionFileWatcher();
        }

        public void UpdateModelUrl(string modelUrl)
        {
            ModelUrl = modelUrl;
            MeshLoaded = false;
            UpdateMeshesAsync();
        }

        private void UpdateCollisionFileWatcher()
        {
            CreateFileWatcher(collisionModelUrl);
        }

        private void ResetModel()
        {
            ModelUrl = string.Empty;
            meshLoaded = true;
            OnMeshChanged();
        }

        private void UpdateModelFileWatchers(ColladaImporter collada)
        {
            foreach (var watch in modelFileWatchers)
            {
                watch.Dispose();
            }
            CreateFileWatcher(modelUrl);
            foreach (var tex in collada.NewTextures)
            {
                if (File.Exists(tex.FileName))
                {
                    CreateFileWatcher(tex.FileName);
                }
            }
        }

        private void CreateFileWatcher(string file)
        {
            if (File.Exists(file))
            {
                var texFileName = Path.GetFileName(file);
                var texPath = Path.GetDirectoryName(file);
                var texWatcher = new FileSystemWatcher(texPath, texFileName);
                texWatcher.NotifyFilter = NotifyFilters.Attributes |
                    NotifyFilters.CreationTime |
                    NotifyFilters.FileName |
                    NotifyFilters.LastAccess |
                    NotifyFilters.LastWrite |
                    NotifyFilters.Size |
                    NotifyFilters.Security;
                texWatcher.Changed += TexWatcher_Changed;
                texWatcher.Created += TexWatcher_Created;
                texWatcher.Deleted += TexWatcher_Deleted;
                modelFileWatchers.Add(texWatcher);
                texWatcher.EnableRaisingEvents = true;
            }
        }

        private void ModelWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            UpdateMeshesAsync();
        }

        private void ModelWatcher_Created1(object sender, FileSystemEventArgs e)
        {
            UpdateMeshesAsync();
        }

        private void TexWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            UpdateMeshesAsync();
        }

        private void TexWatcher_Created(object sender, FileSystemEventArgs e)
        {
            UpdateMeshesAsync();
        }

        private void ModelWatcher_Created(object sender, FileSystemEventArgs e)
        {
            UpdateMeshesAsync();
        }

        private void TexWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            collisionMeshLoaded = false;
            meshLoaded = false;
            UpdateMeshesAsync();
        }

        private void ModelWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            collisionMeshLoaded = false;
            meshLoaded = false;
            UpdateMeshesAsync();
        }

        [XmlIgnore]
        public string ModelUrl { get => modelUrl; set { modelUrl = value; } }
        [XmlIgnore]
        public string CollisionModelUrl { get; set; }
        [XmlElement]
        public List<BillboardInfo> BillboardInfoList { get => billboardInfoList; }
    }
}
