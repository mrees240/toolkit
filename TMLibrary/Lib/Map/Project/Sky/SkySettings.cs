﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Sky
{
    public class SkySettings
    {
        private SkySideInfo leftSide;
        private SkySideInfo rightSide;
        private SkySideInfo frontSide;
        private SkySideInfo backSide;
        private SkySideInfo top;
        private SkySideInfo bottom;

        public SkySettings()
        {
            leftSide = new SkySideInfo();
            rightSide = new SkySideInfo();
            frontSide = new SkySideInfo();
            backSide = new SkySideInfo();
            top = new SkySideInfo();
            bottom = new SkySideInfo();
        }

        public void ResetSettings()
        {
            leftSide.Reset();
            rightSide.Reset();
            frontSide.Reset();
            backSide.Reset();
            top.Reset();
            bottom.Reset();
        }

        [XmlElement("left")]
        public SkySideInfo LeftSide { get => leftSide; set => leftSide = value; }
        [XmlElement("right")]
        public SkySideInfo RightSide { get => rightSide; set => rightSide = value; }
        [XmlElement("front")]
        public SkySideInfo FrontSide { get => frontSide; set => frontSide = value; }
        [XmlElement("back")]
        public SkySideInfo BackSide { get => backSide; set => backSide = value; }
        [XmlElement("top")]
        public SkySideInfo Top { get => top; set => top = value; }
        [XmlElement("bottom")]
        public SkySideInfo Bottom { get => bottom; set => bottom = value; }
    }
}
