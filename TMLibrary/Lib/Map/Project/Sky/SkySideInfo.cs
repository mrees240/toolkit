﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Sky
{
    /// <summary>
    /// Stores info for each side of a skybox, including the texture.
    /// </summary>
    public class SkySideInfo
    {
        private string textureUrl;

        public event EventHandler TextureFileChanged;

        public virtual void OnTextureChanged()
        {
            if (TextureFileChanged != null)
            {
                TextureFileChanged(this, EventArgs.Empty);
            }
        }

        public void Reset()
        {
            textureUrl = string.Empty;
        }
        
        [XmlAttribute("texture_url")]
        public string TextureUrl { get => textureUrl; set { textureUrl = value; OnTextureChanged(); } }
    }
}
