﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.Map.Project
{
    public enum GameItemObjectType
    {
        Default, Health, Turbo, FireMissle, HomingMissle, PowerMissle, Napalm, Ricochet, Remote
    }
}
