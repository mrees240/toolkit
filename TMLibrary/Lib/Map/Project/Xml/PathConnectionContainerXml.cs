﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class PathConnectionContainerXml
    {
        private List<PathNodeConnection> pathNodeConnections;

        public PathConnectionContainerXml()
        {
            pathNodeConnections = new List<PathNodeConnection>();
        }

        [XmlElement("connection")]
        public List<PathNodeConnection> PathNodeConnections { get => pathNodeConnections; set => pathNodeConnections = value; }
    }
}
