﻿using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class StaticObjectDefinitionXml
    {
        private StaticObjectDefinition staticObjectDefinition = new StaticObjectDefinition();

        public StaticObjectDefinitionXml() { }
        public StaticObjectDefinitionXml(StaticObjectDefinition staticObjectDefinition) { this.staticObjectDefinition = staticObjectDefinition; }

        public StaticObjectDefinition ToStaticObjectDefinition()
        {
            return staticObjectDefinition;
        }

        public void FromStaticObjectDefinition(StaticObjectDefinition color)
        {
            this.staticObjectDefinition = color;
        }

        public static implicit operator StaticObjectDefinition(StaticObjectDefinitionXml staticObjectDefinitionXml)
        {
            return staticObjectDefinitionXml.ToStaticObjectDefinition();
        }

        public static implicit operator StaticObjectDefinitionXml(StaticObjectDefinition staticObjectDefinition)
        {
            return new StaticObjectDefinitionXml(staticObjectDefinition);
        }


        [XmlAttribute("name")]
        new public string Name { get { return staticObjectDefinition.Name; } set { staticObjectDefinition.Name = value; } }
        [XmlAttribute("model_url")]
        public string ModelUrl
        {
            get
            {
                return staticObjectDefinition.ModelUrl;
            }
            set
            {
                staticObjectDefinition.ModelUrl = value;
            }
        }
        [XmlAttribute("collision_model_url")]
        public string CollisionModelUrl
        {
            get
            {
                return staticObjectDefinition.CollisionModelUrl;
            }
            set
            {
                staticObjectDefinition.CollisionModelUrl = value;
            }
        }
        [XmlAttribute("has_collision_response")]
        public bool HasCollisionResponse { get { return staticObjectDefinition.HasCollisionResponse; } set { staticObjectDefinition.HasCollisionResponse = value; } }
        [XmlAttribute("default_lighting")]
        public bool DefaultLighting { get { return staticObjectDefinition.DefaultLighting; } set { staticObjectDefinition.DefaultLighting = value; } }
        [XmlAttribute("collision_thickness")]
        public float CollisionThickness { get { return staticObjectDefinition.CollisionThickness; } set { staticObjectDefinition.CollisionThickness = value; } }
        [XmlAttribute("is_drivable_surface")]
        public bool IsDrivableSurface { get { return staticObjectDefinition.IsDrivableSurface; } set { staticObjectDefinition.IsDrivableSurface = value; } }
        [XmlAttribute("is_explosive")]
        public bool IsExplosive { get { return staticObjectDefinition.IsExplosive; } set { staticObjectDefinition.IsExplosive = value; } }
        [XmlAttribute("explodes_on_impact")]
        public bool ExplodesOnImpact { get { return staticObjectDefinition.ExplodesOnImpact; } set { staticObjectDefinition.ExplodesOnImpact = value; } }
    }
}
