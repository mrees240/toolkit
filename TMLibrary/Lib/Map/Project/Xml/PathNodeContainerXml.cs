﻿using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class PathNodeContainerXml
    {
        private PathNodeContainer pathNodeContainer = new PathNodeContainer();

        public PathNodeContainerXml() { }
        public PathNodeContainerXml(PathNodeContainer pathNodeContainer) { this.pathNodeContainer = pathNodeContainer; }

        public PathNodeContainer ToPathNodeContainer()
        {
            return pathNodeContainer;
        }

        public void FromColor(PathNodeContainer pathNodeContainer)
        {
            this.pathNodeContainer = pathNodeContainer;
        }

        public static implicit operator PathNodeContainer(PathNodeContainerXml pathNodeContainerXml)
        {
            return pathNodeContainerXml.ToPathNodeContainer();
        }

        public static implicit operator PathNodeContainerXml(PathNodeContainer pathNodeContainer)
        {
            return new PathNodeContainerXml(pathNodeContainer);
        }

        [XmlElement("path_node",Type =typeof(PathNodeXml))]
        public List<PathNode> PathNodeDefinitions
        {
            get
            {
                return pathNodeContainer.PathNodes;
            }
            set
            {
                pathNodeContainer.PathNodes = value;
            }
        }
    }
}
