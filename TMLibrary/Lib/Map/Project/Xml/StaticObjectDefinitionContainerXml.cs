﻿using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class StaticObjectDefinitionContainerXml
    {
        private StaticObjectDefinitionContainer staticObjectDefinitionContainer = new StaticObjectDefinitionContainer();

        public StaticObjectDefinitionContainerXml() { }
        public StaticObjectDefinitionContainerXml(StaticObjectDefinitionContainer staticObjectDefinitionContainer) { this.staticObjectDefinitionContainer = staticObjectDefinitionContainer; }

        public StaticObjectDefinitionContainer ToStaticObjectDefinitionContainer()
        {
            return staticObjectDefinitionContainer;
        }

        public void FromColor(StaticObjectDefinitionContainer staticObjectDefinitionContainer)
        {
            this.staticObjectDefinitionContainer = staticObjectDefinitionContainer;
        }

        public static implicit operator StaticObjectDefinitionContainer(StaticObjectDefinitionContainerXml staticObjectDefinitionContainerXml)
        {
            return staticObjectDefinitionContainerXml.ToStaticObjectDefinitionContainer();
        }

        public static implicit operator StaticObjectDefinitionContainerXml(StaticObjectDefinitionContainer staticObjectDefinitionContainer)
        {
            return new StaticObjectDefinitionContainerXml(staticObjectDefinitionContainer);
        }

        [XmlElement("static_object_definition",Type =typeof(StaticObjectDefinitionXml))]
        public List<StaticObjectDefinition> StaticObjectDefinitions
        {
            get
            {
                return staticObjectDefinitionContainer.StaticObjectDefinitions;
            }
            set
            {
                staticObjectDefinitionContainer.StaticObjectDefinitions = value;
            }
        }
    }
}
