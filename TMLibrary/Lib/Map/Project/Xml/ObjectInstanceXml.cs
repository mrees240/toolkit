﻿using ColladaLibrary.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class ObjectInstanceXml
    {
        private ObjectInstance objectInstance = new ObjectInstance();

        public ObjectInstanceXml() { }
        public ObjectInstanceXml(ObjectInstance objectInstance) { this.objectInstance = objectInstance; }

        public ObjectInstance ToObjectInstance()
        {
            return objectInstance;
        }

        public void FromObjectInstance(ObjectInstance objectInstance)
        {
            this.objectInstance = objectInstance;
        }

        public static implicit operator ObjectInstance(ObjectInstanceXml objectInstanceXml)
        {
            return objectInstanceXml.ToObjectInstance();
        }

        public static implicit operator ObjectInstanceXml(ObjectInstance objectInstance)
        {
            return new ObjectInstanceXml(objectInstance);
        }

        [XmlAttribute("name")]
        public string Name { get { return objectInstance.Name; } set { objectInstance.Name = value; } }
        [XmlAttribute("definition")]
        public string StaticObjectDefinitionName
        {
            get { if (ObjectType == ObjectType.StaticObject) { return objectInstance.StaticObject.Name; } else { return null; } }
            set { objectInstance.StaticObjectDefinitionName = value; }
        }
        [XmlAttribute("type")]
        public ObjectType ObjectType { get { return objectInstance.ObjectType; } set { objectInstance.ObjectType = value; } }

        [XmlAttribute("item_type")]
        public GameItemObjectType ItemType { get { return objectInstance.ItemType; } set { objectInstance.ItemType = value; objectInstance.GameItemDefinition.ItemType = value; } }

        [XmlElement("local_position")]
        public Vector3 LocalPosition { get { return objectInstance.Transformation.PositionLocal; } set { objectInstance.Transformation.SetLocalPosition(value); } }

        [XmlElement("local_rotation_degrees")]
        public Vector3 LocalRotationDegrees { get { return objectInstance.Transformation.GetLocalRotationDegrees(); } set { objectInstance.Transformation.SetLocalRotationDegrees(value); } }

        [XmlElement("local_scale")]
        public Vector3 LocalScale { get { return objectInstance.Transformation.ScaleLocal; } set { objectInstance.Transformation.SetLocalScale(value); } }

        //[XmlElement("matrix",Type =typeof(XmlMatrix))]
        //public Matrix4x4 Matrix { get { return objectInstance.Transformation.LocalMatrix; } set { objectInstance.LocalMatrix = value; } }

        [XmlElement("child",Type =typeof(ObjectInstanceXml))]
        public List<ObjectInstance> Children { get { return objectInstance.Children; } set { objectInstance.Children = value; } }

        [XmlElement("connected_node_ids")]
        public List<string> ConnectedNodesNames { get => objectInstance.ConnectedNodeNames; set { if (value != null) { objectInstance.ConnectedNodeNames = value; } } }

        /*[XmlAttribute("is_spawn_point")]
        public bool IsSpawnPoint { get { return objectInstance.IsSpawnPoint; } set { objectInstance.IsSpawnPoint = value; } }*/
    }
}
