﻿using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project.Sky;

namespace TMLibrary.Lib.Map.Project.Xml
{
    [XmlRoot(ElementName = "project", Namespace = "http://www.collada.org/2005/11/COLLADASchema")]
    public class MapEditorProjectXml
    {
        private MapEditorProject project;

        public MapEditorProject ToProject()
        {
            return project;
        }

        public MapEditorProjectXml() { }
        public MapEditorProjectXml(MapEditorProject project) : this()
        {
            this.project = project;
        }

        public void FromProject(MapEditorProject project)
        {
            this.project = project;
        }

        public static implicit operator MapEditorProject(MapEditorProjectXml xml)
        {
            return xml.ToProject();
        }

        public static implicit operator MapEditorProjectXml(MapEditorProject project)
        {
            return new MapEditorProjectXml(project);
        }

        [XmlElement("version")]
        public string FileVersion { get { return project.FileVersion; } set { project.FileVersion = value; } }

        [XmlElement("game_definition")]
        public MapEditorProjectDefinition ProjectDefinition { get => project.ProjectDefinition; set { project.UpdateProjectDefinition(value); } }

        [XmlElement("sky_settings")]
        public SkySettings SkySettings { get => project.SkySettings; set { project.SkySettings = value; } }

        [XmlElement("static_object_definitions", Type = typeof(StaticObjectDefinitionContainerXml))]
        public StaticObjectDefinitionContainer StaticObjectDefinitions
        {
            get => project.StaticObjectDefinitionContainer;
            set
            {
                project.StaticObjectDefinitionContainer = value;
            }
        }

        [XmlElement("path_node_definitions", Type = typeof(PathNodeContainerXml))]
        public PathNodeContainer PathNodeContainer
        {
            get => project.PathNodeContainer;
            set
            {
                project.PathNodeContainer = value;
            }
        }

        [XmlElement("path_connections")]
        public PathConnectionContainerXml PathConnectionContainer { get => project.PathConnectionContainer; set => project.PathConnectionContainer = value; }

        [XmlElement("scene_root")]
        public ObjectInstance RootNode { get { return project.RootNode; } set { project.RootNode = value; } }
    }
}
