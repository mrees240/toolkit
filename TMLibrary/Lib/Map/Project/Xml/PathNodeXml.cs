﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project.Xml
{
    public class PathNodeXml
    {
        private PathNode pathNode = new PathNode();

        public PathNodeXml() { }
        public PathNodeXml(PathNode pathNode) { this.pathNode = pathNode; }

        public PathNode ToPathNode()
        {
            return pathNode;
        }

        public void FromPathNode(PathNode pathNode)
        {
            this.pathNode = pathNode;
        }

        public static implicit operator PathNode(PathNodeXml pathNodeXml)
        {
            return pathNodeXml.ToPathNode();
        }

        public static implicit operator PathNodeXml(PathNode pathNode)
        {
            return new PathNodeXml(pathNode);
        }

        [XmlAttribute("name")]
        public string Name { get { return pathNode.Name; } set { pathNode.Name = value; } }
        [XmlAttribute("is_spawn_point")]
        public bool IsSpawnPoint { get { return pathNode.IsSpawnPoint; } set { pathNode.IsSpawnPoint = value; } }
    }
}
