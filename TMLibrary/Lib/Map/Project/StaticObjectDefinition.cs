﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Scene;
using TMLibrary.Lib.Collada.Hierarchy;
using TMLibrary.Lib.Map.Project;
using TMLibrary.Lib.Util;

namespace MapEditor.Project
{
    public class StaticObjectDefinition : MeshObject
    {
        //private List<Mesh> meshes;
        //private Mesh collisionMesh;
        //private bool meshWasUpdated;
        //private List<MeshBuffer> meshBuffers;
        //private List<MeshBuffer> lineMeshBuffers;

        // may have loading percentage
        private PixelFormat pixelFormat;
        //private object meshLoadedLock;
        private FileSystemWatcher fileWatcher;

        // User settings

        public StaticObjectDefinition()
        {
            pixelFormat = PixelFormat.Format8bppIndexed;
            SetDefaultSettings();
        }

        private void SetDefaultSettings()
        {
            this.Name = string.Empty;
            HasCollisionResponse = true;
            CollisionThickness = 100.0f;
            IsDrivableSurface = true;
            MinDrawDistance = 0;
            MaxDrawDistance = 2500000;
            IsBillboard = false;
        }

        public StaticObjectDefinition(string name) : this()
        {
            this.Name = name;
        }

        public bool HasCollision()
        {
            return HasCollisionResponse && CollisionModelUrl != null && CollisionModelUrl.Length > 0;
        }

        [XmlAttribute("name")]
        new public string Name { get; set; }
        [XmlAttribute("has_collision_response")]
        public bool HasCollisionResponse { get; set; }
        [XmlAttribute("default_lighting")]
        public bool DefaultLighting { get; set; }
        [XmlAttribute("collision_thickness")]
        public float CollisionThickness { get; set; }
        [XmlAttribute("is_drivable_surface")]
        public bool IsDrivableSurface { get; set; }
        [XmlAttribute("is_explosive")]
        public bool IsExplosive { get; set; }
        [XmlAttribute("explodes_on_impact")]
        public bool ExplodesOnImpact { get; set; }
        [XmlAttribute("min_draw_distance")]
        public int MinDrawDistance { get; set; }
        [XmlAttribute("max_draw_distance")]
        public int MaxDrawDistance { get; set; }
        [XmlAttribute("is_billboard")]
        public bool IsBillboard { get; set; }
    }
}
