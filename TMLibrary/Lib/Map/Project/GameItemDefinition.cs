﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.Map.Project
{
    /// <summary>
    /// Represents an item/pickup that belongs to a specific game.
    /// </summary>
    public class GameItemDefinition : MeshObject
    {
        private string iconUrl;
        private bool translationOnly;

        public GameItemDefinition() : base()
        {
        }

        public GameItemDefinition(string name, string iconUrl, string modelUrl, GameItemObjectType itemType) : this()
        {
            this.Name = name;
            this.iconUrl = iconUrl;
            this.ModelUrl = modelUrl;
            this.ItemType = itemType;
        }


        public GameItemDefinition(string name, string iconUrl, List<BillboardInfo> billboards, GameItemObjectType itemType) : this()
        {
            this.Name = name;
            this.iconUrl = iconUrl;
            this.ItemType = itemType;
            this.SetBillboard(billboards);
        }

        public GameItemDefinition(string name, string iconUrl, BillboardInfo billboard, GameItemObjectType itemType)
            : this(name, iconUrl, new List<BillboardInfo>() { billboard }, itemType)
        {
        }

        [XmlAttribute("item_type")]
        public GameItemObjectType ItemType { get; set; }
        [XmlIgnore]
        public string ImageIconUrl { get => iconUrl; }
        [XmlIgnore]
        public bool TranslationOnly { get => translationOnly; set => translationOnly = value; }
    }
}
