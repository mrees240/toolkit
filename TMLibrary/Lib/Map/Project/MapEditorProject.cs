﻿using ColladaLibrary.Render.Lighting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMLibrary.Lib.Map.Project;
using System.Reflection;
using TMLibrary.Lib.Map.Project.Xml;
using TMLibrary.Lib.Map.Project.Sky;

namespace MapEditor.Project
{
    [XmlRoot(ElementName = "project", Namespace = "http://www.collada.org/2005/11/COLLADASchema")]
    public class MapEditorProject
    {
        private MapEditorProjectDefinition projectDefinition;
        private Dictionary<GameItemObjectType, GameItemDefinition> gameItems;
        private StaticObjectDefinitionContainer staticObjectDefinitionContainer;
        private PathNodeContainer pathNodeContainer;
        private PathConnectionContainerXml pathConnectionContainer;
        private List<PathNodeConnection> pathNodeConnections;
        private SkySettings skySettings;

        public MapEditorProject()
        {
            pathConnectionContainer = new PathConnectionContainerXml();
            this.pathNodeConnections = pathConnectionContainer.PathNodeConnections;
            pathNodeContainer = new PathNodeContainer();
            staticObjectDefinitionContainer = new StaticObjectDefinitionContainer();
            gameItems = new Dictionary<GameItemObjectType, GameItemDefinition>();
            RootNode = new ObjectInstance("Root Node", 0);
            skySettings = new SkySettings();

            SunLight = new Light(LightType.Directional, System.Drawing.Color.White, 0.0f);
            AmbientLight = new Light(LightType.Ambient, System.Drawing.Color.White, 0.0f);
            SetDefaultLighting();
        }

        public MapEditorProject(MapEditorProjectDefinition projectDefinition) : this()
        {
            this.projectDefinition = projectDefinition;
        }

        private void SetDefaultLighting()
        {
            AmbientLight.Color = System.Drawing.Color.FromArgb(200, 200, 200);
            AmbientLight.Intensity = 0.5f;
            SunLight.Color = System.Drawing.Color.White;
            SunLight.Intensity = 0.7f;
            SunLight.YRotDegrees = 135.0f;
            SunLight.ZRotDegrees = 45.0f;
        }

        public void ResetProject()
        {
            StaticObjectDefinitionContainer.StaticObjectDefinitions.Clear();
            PathNodeContainer.PathNodes.Clear();
            RootNode.Children.Clear();

            SetDefaultLighting();
            skySettings.ResetSettings();
        }

        public void RemoveObjectInstance(ObjectInstance parent, ObjectInstance objectInstance)
        {
            if (parent != null)
            {
                parent.RemoveChild(objectInstance);
            }
            if (objectInstance.ObjectType == ObjectType.PathNode)
            {
                RemovePathNode(objectInstance.PathNode);
            }
        }

        public void AddObjectInstance(ObjectInstance parent, ObjectInstance objectInstance)
        {
            if (parent != null)
            {
                parent.AddChild(objectInstance);
            }

            if (objectInstance.ObjectType == ObjectType.PathNode)
            {
                pathNodeContainer.PathNodes.Add(objectInstance.PathNode);
            }
        }

        public void RemovePathNode(PathNode pathNode)
        {
            RemovePathNodeLinks(pathNode);
            pathNodeContainer.PathNodes.Remove(pathNode);
        }

        private void UnlinkPathNode(PathNode p0, PathNode p1)
        {
            for (int i = 0; i < pathNodeConnections.Count; i++)
            {
                var connection = pathNodeConnections[i];

                if (connection.P0.PathNode == p0 || connection.P1.PathNode == p1)
                {
                    if (connection.P1.PathNode == p1 || connection.P0.PathNode == p1)
                    {
                        pathNodeConnections.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Goes through each path node connection list and removes all links to the specified path node.
        /// </summary>
        /// <param name="pathNode"></param>
        private void RemovePathNodeLinks(PathNode pathNode)
        {
            var pathNodesToRemove = new Stack<PathNodeConnection>();

            foreach (var currentPathConnection in pathNodeConnections)
            {
                if (currentPathConnection.P0.PathNode == pathNode || currentPathConnection.P1.PathNode == pathNode)
                {
                    pathNodesToRemove.Push(currentPathConnection);
                }
            }
            while (pathNodesToRemove.Any())
            {
                pathNodeConnections.Remove(pathNodesToRemove.Pop());
            }
            /*var pathNodes = GetAllPathNodeInstances();

            for (int iPathNode = 0; iPathNode < pathNodes.Count; iPathNode++)
            {
                var currentPathNode = pathNodes[iPathNode];

                if (currentPathNode != pathNode)
                {
                    for (int iConnection = 0; iConnection < currentPathNode.ConnectedNodes.Length; iConnection++)
                    {
                        var connectedNode = currentPathNode.ConnectedNodes[iConnection];
                        if (connectedNode == pathNode)
                        {
                            currentPathNode.ConnectedNodes[iConnection] = null;
                        }
                    }
                }
            }*/
        }

        public void AddStaticObjectDefinition(StaticObjectDefinition staticObject)
        {
            StaticObjectDefinitionContainer.StaticObjectDefinitions.Add(staticObject);
        }

        public void AddGameItemDefinition(GameItemDefinition gameItem)
        {
            gameItems.Add(gameItem.ItemType, gameItem);
        }

        public List<Light> GetAllLights()
        {
            var lights = new List<Light>();

            lights.Add(SunLight);
            lights.Add(AmbientLight);

            return lights;
        }

        public List<ObjectInstance> GetAllObjectInstances()
        {
            var objectInstances = new List<ObjectInstance>();

            AddObjectInstances(RootNode, objectInstances);

            return objectInstances;
        }

        private void AddObjectInstances(ObjectInstance currentInstance, List<ObjectInstance> objectInstances)
        {
            objectInstances.Add(currentInstance);

            foreach (var child in currentInstance.Children)
            {
                AddObjectInstances(child, objectInstances);
            }
        }

        public List<ObjectInstance> GetAllGameItemInstances()
        {
            return GetAllObjectInstances().Where(i => i.ObjectType == ObjectType.GameItemObject).ToList();
        }

        public List<ObjectInstance> GetAllStaticObjectInstances()
        {
            return GetAllObjectInstances().Where(i => i.ObjectType == ObjectType.StaticObject).ToList();
        }

        public List<ObjectInstance> GetAllPathNodeInstances()
        {
            return GetAllObjectInstances().Where(i => i.ObjectType == ObjectType.PathNode).ToList();
        }

        public void RemoveStaticObjectDefinition(StaticObjectDefinition staticObject)
        {
            StaticObjectDefinitionContainer.StaticObjectDefinitions.Remove(staticObject);
            RemoveInstancesOfStaticObject(staticObject);
        }

        public void RemoveInstancesOfStaticObject(StaticObjectDefinition staticObject)
        {
            var objectInstances = GetAllObjectInstances();
            var instancesToRemove = objectInstances.Where(i => i.ObjectType == ObjectType.StaticObject && i.StaticObject == staticObject);

            foreach (var objectInstance in objectInstances)
            {
                foreach (var instanceToRemove in instancesToRemove)
                {
                    if (objectInstance.Children.Contains(instanceToRemove))
                    {
                        objectInstance.RemoveChild(instanceToRemove);
                    }
                }
            }
        }

        /*public void AddObjectInstance(ObjectInstance instance)
        {
            switch (instance.ObjectType)
            {
                case ObjectType.StaticObject:
                    staticObjectsNode.Children.Add(instance);
                    break;
                case ObjectType.GameItemObject:
                    pickupItemsNode.Children.Add(instance);
                    break;
                default:
                    throw new Exception($"Object Instance type {instance.ObjectType} needs implemented here.");
            }
        }*/

        public int GetNextIdNumber(ObjectType objectType)
        {
            switch (objectType)
            {
                case ObjectType.PathNode:
                    var pathNodes = GetAllPathNodeInstances();
                    if (!pathNodes.Any())
                    {
                        return 0;
                    }
                    return pathNodes.Max(p => p.IdNumber) + 1;
                case ObjectType.GameItemObject:
                    var gameItems = GetAllGameItemInstances();
                    if (!gameItems.Any())
                    {
                        return 0;
                    }
                    return gameItems.Max(i => i.IdNumber) + 1;
                case ObjectType.StaticObject:
                    var staticObjects = GetAllStaticObjectInstances();
                    if (!staticObjects.Any())
                    {
                        return 0;
                    }
                    return staticObjects.Max(i => i.IdNumber) + 1;
            }
            return -1;
        }

        private List<ObjectInstance> GetInstancesOfStaticObject(StaticObjectDefinition staticObject)
        {
            var objectInstances = GetAllObjectInstances();
            var instances = objectInstances.Where(s => s.StaticObject == staticObject && s.StaticObject == staticObject).ToList();
            
            return instances;
        }

        public int GetAmountOfStaticObjectInstances(StaticObjectDefinition staticObject)
        {
            return GetInstancesOfStaticObject(staticObject).Count();
        }

        // XmlElements
        [XmlElement("map_editor_version")]
        public string FileVersion { get; set; }
        [XmlElement("game_definition")]
        public MapEditorProjectDefinition ProjectDefinition { get => projectDefinition; set { UpdateProjectDefinition(value); } }
        [XmlElement("sky_settings")]
        public SkySettings SkySettings { get => skySettings; set => skySettings = value; }
        [XmlElement("static_object_definitions", Type = typeof(StaticObjectDefinitionContainerXml))]
        public StaticObjectDefinitionContainer StaticObjectDefinitionContainer { get { return staticObjectDefinitionContainer; } set { this.staticObjectDefinitionContainer = value; } }
        [XmlElement("path_node_definitions", Type=typeof(PathNodeContainerXml))]
        public PathNodeContainer PathNodeContainer { get => pathNodeContainer; set => pathNodeContainer = value; }

        [XmlElement("sun_light")]
        public Light SunLight { get; set; }
        [XmlElement("ambient_light")]
        public Light AmbientLight { get; set; }
        [XmlElement("root_scene", Type =typeof(ObjectInstanceXml))]
        public ObjectInstance RootNode { get; set; }

        public void UpdateProjectDefinition(MapEditorProjectDefinition projectDefinition)
        {
            this.projectDefinition = projectDefinition;
        }

        public List<PathNodeConnection> GetPathNodeConnections()
        {
            return pathNodeConnections;
        }

        // XmlIgnore
        [XmlIgnore]
        public Dictionary<GameItemObjectType, GameItemDefinition> GameItems { get => gameItems; }
        [XmlElement("path_connections")]
        public PathConnectionContainerXml PathConnectionContainer { get => pathConnectionContainer; set => pathConnectionContainer = value; }
        [XmlIgnore]
        public List<PathNodeConnection> PathNodeConnections { get => PathConnectionContainer.PathNodeConnections; }
    }
}