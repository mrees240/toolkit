﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Map.Project
{
    public enum PathConnectionDirection
    {
        TwoWay, P0ToP1, P1ToP0
    }

    public class PathNodeConnection
    {
        private PathConnectionDirection direction;
        private ObjectInstance p0;
        private ObjectInstance p1;

        private string name0;
        private string name1;

        public PathNodeConnection()
        {

        }

        public PathNodeConnection(ObjectInstance p0, ObjectInstance p1, PathConnectionDirection direction)
        {
            this.direction = direction;
            this.p0 = p0;
            this.p1 = p1;
            this.name0 = p0.PathNode.Name;
            this.name1 = p1.PathNode.Name;
        }

        [XmlIgnore]
        public ObjectInstance P0 { get => p0; set => p0 = value; }
        [XmlIgnore]
        public ObjectInstance P1 { get => p1; set => p1 = value; }
        [XmlAttribute("p0_name")]
        public string Name0 { get => name0; set => name0 = value; }
        [XmlAttribute("p1_name")]
        public string Name1 { get => name1; set => name1 = value; }
        [XmlAttribute("direction")]
        public PathConnectionDirection Direction { get => direction; set => direction = value; }
    }
}
