﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using MapEditor.Project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Numerics;
using TMLibrary.Lib.Collada.Hierarchy;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMLibrary.Lib.ProjectTypes.Tm2Pc.Map;
using TMLibrary.Lib.Util;
using TMLibrary.Map;
using TMLibrary.Rendering;
using TMLibrary.Rendering.DpcTm2;
using System.Linq;
using TMLibrary.Textures;
using System.Drawing;
using TMLibrary.Lib.FileTypes.Ramps;
using TMLibrary.Lib.Map.Project;
using TMLibrary.Lib.ProjectTypes;
using ColladaLibrary.Misc;
using TMLibrary.Rendering.DpcTm2.Polygons;
using Kaliko.ImageLibrary;
using ColladaLibrary.Utility;
using TMLibrary.Lib.FileTypes.Point;
using System.Linq;
using System.Text;
using System.Threading;

namespace TMLibrary.Lib.Map
{
    public class MapCompiler
    {
        private MapCompilerProgress progressManager;
        private readonly int connectionLimit = 4;
        private List<GameMesh> newMeshes = new List<GameMesh>();
        private long collisionPtrAddress = 0;
        private long collisionStartAddress;
        private ProjectDefinition project;
        private BinaryWriter writer;
        private string colladaFileUrl;
        private string safeFileName;
        private string directory;
        //private ColladaImporter colladaImporter;
        private SortedList<long, Instruction> allInstructions;
        private MapCompilerResult result;
        private RampsFile rampsFile;
        private PointsFile pointsFile;

        private string dishUrl;
        private string dishUrlTpc;
        private string dishPts;
        private string dishRmp;
        private string dishGrp;

        // A list of nodes with independent/global matrices.
        private List<ObjectInstance> globalNodes;

        // Used for PTS file
        private List<Vector3> aiNodes;
        private List<VertexPair> nodePairs;

        private List<Mesh> transformedMeshes;
        private List<SpawnMesh> spawnMeshes;

        private List<Mesh> rawInstructionListMeshesOnly = new List<Mesh>();
        private List<DpcMatrix> rawInstructionListMeshes = new List<DpcMatrix>();
        private List<DpcTranslationMatrix> rawInstructionListItems = new List<DpcTranslationMatrix>();
        private List<DpcCollisionFF08> rawInstructionListCollision = new List<DpcCollisionFF08>();
        private List<float> rawInstructionListCollisionRadius = new List<float>();
        private CancellationTokenSource cancelToken;

        private PickupFactory pickupFactory;

        private Dictionary<Bitmap, BitmapSet> lowQualityTextures;

        private Dpc701BoundingSphere rootInstruction;

        public MapCompilerResult Result { get => result; }
        public BackgroundWorker Worker { get => worker; }
        public bool IsIdle { get => isIdle; }
        public CancellationTokenSource CancelToken { get => cancelToken; }

        private bool isIdle;
        private BackgroundWorker worker;

        private MapEditorProject mapEditorProject;

        private PointsFileController pointsFileController;
        RampsFileController rampsFileController;
        private Tm2PcPolygonController polygonController;

        private ProjectController projectController;

        public MapCompiler()
        {
            progressManager = new MapCompilerProgress();
            cancelToken = new CancellationTokenSource();
            pointsFile = new PointsFile();
            rampsFile = new RampsFile();
            rampsFileController = new RampsFileController(rampsFile, cancelToken);
            pointsFileController = new PointsFileController(pointsFile);
            isIdle = true;
            globalNodes = new List<ObjectInstance>();
            transformedMeshes = new List<Mesh>();
            spawnMeshes = new List<SpawnMesh>();
            result = new MapCompilerResult();

            rampsFileController.Progress.FileWriteProgressChangedEvent += Progress_FileWriteProgressChangedEvent;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
        }

        private void Progress_FileWriteProgressChangedEvent(object sender, EventArgs e)
        {
            progressManager.RampFileWriteProgress = ((ProgressManager)sender).FileWriteProgress;
            UpdateProgress();
        }

        private void Cancel()
        {
            result.Cancelled = true;
        }

        private void WritePointer(long address)
        {
            writer.Write(Utility.CreatePointer(address, project.MeshFile.Definition));
        }

        private void CreateBitmaps(List<Bitmap> bitmaps, ObjectInstance node)
        {
            if (node.StaticObject != null)
            {
                var meshes = node.StaticObject.Meshes;

                foreach (var mesh in meshes)
                {
                    var meshBitmaps = mesh.GetDiffuseList();

                    foreach (var meshBitmap in meshBitmaps)
                    {
                        bitmaps.Add(meshBitmap);
                    }
                }

                if (node.StaticObject.IsBillboard)
                {
                    foreach (var billboard in node.StaticObject.BillboardInfoList)
                    {
                        bitmaps.Add(billboard.Texture.Bitmap);
                    }
                }
            }

            foreach (var child in node.Children)
            {
                CreateBitmaps(bitmaps, child);
            }
        }

        private bool BoundingSphereContains(BoundingSphere outerSphere, BoundingSphere possibleInnerSphere)
        {
            return ((outerSphere.Center - possibleInnerSphere.Center).Length() < outerSphere.Radius);
        }

        private bool CellContainsPt(Vector3 pt, Vector3 cellOrigin, uint cellLength)
        {
            float halfXLength = cellLength / 2.0f;
            float halfYLength = cellLength / 2.0f;
            float halfZLength = cellLength / 2.0f;
            bool containsX = pt.X >= cellOrigin.X - halfXLength && pt.X < cellOrigin.X + halfXLength;
            bool containsY = pt.Y >= cellOrigin.Y - halfYLength && pt.Y < cellOrigin.Y + halfYLength;
            bool containsZ = pt.Z >= cellOrigin.Z - halfZLength && pt.Z < cellOrigin.Z + halfZLength;

            bool cellContainsPt = containsX && containsY && containsZ;

            return cellContainsPt;
        }

        private Dpc701BoundingSphere GroupBoundingSpheres(List<Dpc701BoundingSphere> childSpheres, uint cellLength)
        {
            var rootBoundingSphere = new Dpc701BoundingSphere(project, 0);
            rootBoundingSphere.SetMaxRadius();
            uint radius = cellLength / 2;

            float minX = childSpheres.Min(s => s.BoundingSphere.Center.X);
            float maxX = childSpheres.Max(s => s.BoundingSphere.Center.X);
            float minY = childSpheres.Min(s => s.BoundingSphere.Center.Y);
            float maxY = childSpheres.Max(s => s.BoundingSphere.Center.Y);
            float minZ = childSpheres.Min(s => s.BoundingSphere.Center.Z);
            float maxZ = childSpheres.Max(s => s.BoundingSphere.Center.Z);
            float xLength = Math.Abs(maxX - minX) * 10;
            float yLength = Math.Abs(maxY - minY) * 10;
            float zLength = Math.Abs(maxZ - minZ) * 10;

            int cellAmountX = (int)((xLength / cellLength) + 1) * 2;
            int cellAmountY = (int)((yLength / cellLength) + 1) * 2;
            int cellAmountZ = (int)((zLength / cellLength) + 1) * 2;

            var center = new Vector3(minX + xLength / 2.0f, minY + yLength / 2.0f, minZ + zLength / 2.0f);

            var cellDimensions = new Vector3(xLength, yLength, zLength);

            var currentRoot = rootBoundingSphere;

            for (int x = 0; x < cellAmountX; x++)
            {
                for (int y = 0; y < cellAmountY; y++)
                {
                    for (int z = 0; z < cellAmountZ; z++)
                    {
                        var xCenter = (float)(x * cellLength + radius) - center.X;
                        var yCenter = (float)(y * cellLength + radius) - center.Y;
                        var zCenter = (float)(z * cellLength + radius) - center.Z;
                        var cellBoundingSphere = new Dpc701BoundingSphere(project, 0);
                        cellBoundingSphere.SetRadius(radius);
                        cellBoundingSphere.SetOrigin(new Vector3(xCenter, yCenter, zCenter));

                        var children = childSpheres.Where(s => CellContainsPt(s.BoundingSphere.Center, cellBoundingSphere.BoundingSphere.Center, cellLength));

                        if (children.Count() > 0)
                        {
                            cellBoundingSphere.Children.AddRange(children);
                            currentRoot.Children.Add(cellBoundingSphere);

                            if (currentRoot.Children.Count() > 250)
                            {
                                var root2 = new Dpc701BoundingSphere(project, 0);
                                root2.SetMaxRadius();
                                currentRoot.Children.Add(root2);
                                currentRoot = root2;
                            }
                        }
                    }
                }
            }

            return rootBoundingSphere;
        }

        private bool CheckIsCancelled()
        {
            if (cancelToken.IsCancellationRequested)
            {
                Cancel();
                return true;
            }
            return false;
        }

        private void CreateCollisionInstructions(ObjectInstance node)
        {
            if (node.ObjectType == ObjectType.StaticObject)
            {
                if (node.StaticObject.HasCollision())
                {
                    if (!File.Exists(node.StaticObject.CollisionModelUrl))
                    {
                        result.AddMessage($"Cannot find collision model file: {node.StaticObject.CollisionModelUrl}");
                        return;
                    }
                }
            }


            var colMeshes = node.StaticObject.CollisionMeshes;

            foreach (var meshOriginal in colMeshes)
            {
                if (CheckIsCancelled())
                {
                    return;
                }
                //var meshClone = new Mesh(mesh);
                //meshClone.Transform(node.Transformation.WorldMatrix);
                var mesh = new Mesh(meshOriginal);
                mesh.Transform(node.Transformation.WorldMatrix);

                var polys = mesh.GetAllPolygons();

                float thickness = 50.0f;

                DpcCollisionFF08 currentCol = null;

                foreach (var poly in polys)
                {
                    if (CheckIsCancelled())
                    {
                        return;
                    }
                    //var subSphere = new Dpc701BoundingSphere(project, 0);
                    //subSphere.SetMaxRadius();
                    var faceVerts0 = poly.GetVertices(mesh).Select(v => v.Vector).ToArray();
                    faceVerts0 = new Vector3[] { faceVerts0[0], faceVerts0[2], faceVerts0[1] };
                    poly.CrossProduct = RenderUtility.FindCrossProduct(faceVerts0);
                    var collision = new DpcCollisionFF08(project, 0);
                    currentCol = collision;
                    var radius = faceVerts0.Max(v => v.Length());

                    var p2Verts = poly.GetVertices(mesh).Select(v => v.Vector).ToArray();
                    var p2VertsExtrudedEnd = new List<Vector3>();
                    foreach (var vert in p2Verts)
                    {
                        p2VertsExtrudedEnd.Add(vert - (poly.CrossProduct * thickness));
                    }
                    var faceVerts1 = new Vector3[] { p2VertsExtrudedEnd[0], p2VertsExtrudedEnd[2], p2VertsExtrudedEnd[1] };
                    var faceVerts2 = new Vector3[] { faceVerts0[0], faceVerts0[0] - (poly.CrossProduct * thickness), faceVerts0[1], faceVerts0[1] - (poly.CrossProduct * thickness) };
                    var faceVerts3 = new Vector3[] { faceVerts0[1], faceVerts0[1] - (poly.CrossProduct * thickness), faceVerts0[2], faceVerts0[2] - (poly.CrossProduct * thickness) };
                    var faceVerts4 = new Vector3[] { faceVerts0[2], faceVerts0[2] - (poly.CrossProduct * thickness), faceVerts0[0], faceVerts0[0] - (poly.CrossProduct * thickness) };
                    var center0 = RenderUtility.FindCenter(faceVerts0);
                    var center1 = RenderUtility.FindCenter(faceVerts1);
                    var center2 = RenderUtility.FindCenter(faceVerts2);
                    var center3 = RenderUtility.FindCenter(faceVerts3);
                    var center4 = RenderUtility.FindCenter(faceVerts4);
                    var cp0 = RenderUtility.FindCrossProduct(faceVerts0);
                    var cp1 = -RenderUtility.FindCrossProduct(faceVerts1);
                    var cp2 = RenderUtility.FindCrossProduct(faceVerts2);
                    var cp3 = RenderUtility.FindCrossProduct(faceVerts3);
                    var cp4 = RenderUtility.FindCrossProduct(faceVerts4);
                    var extrudedCenter = center0 + ((center1 - center0) / 2.0f);

                    var plane0 = RenderUtility.PtDirectionPlaneToNormalForm(-faceVerts0[0], -cp0);
                    var plane1 = RenderUtility.PtDirectionPlaneToNormalForm(-faceVerts1[0], -cp1);
                    var plane2 = RenderUtility.PtDirectionPlaneToNormalForm(-faceVerts2[0], -cp2);
                    var plane3 = RenderUtility.PtDirectionPlaneToNormalForm(-faceVerts3[0], -cp3);
                    var plane4 = RenderUtility.PtDirectionPlaneToNormalForm(-faceVerts4[0], -cp4);

                    collision.ConvexVolume.Planes[0] = plane0;
                    collision.ConvexVolume.Planes[1] = plane1;
                    collision.ConvexVolume.Planes[2] = plane2;
                    collision.ConvexVolume.Planes[3] = plane3;
                    collision.ConvexVolume.Planes[4] = plane4;
                    collision.ConvexVolume.Planes[5] = plane4;

                    rawInstructionListCollision.Add(collision);
                    rawInstructionListCollisionRadius.Add(radius);
                }
            }
        }

        private void UpdateProgress()
        {
            worker.ReportProgress((int)progressManager.GetCurrentProgress());
        }

        public void CompileMap(string outputDirectory, MapEditorProject mapEditorProject)
        {
            progressManager.Reset();
            this.mapEditorProject = mapEditorProject;
            var projectTypes = new ProjectFactory().LoadProjectTypes();
            this.project = projectTypes[GameType.TM2PC];
            projectController = new ProjectController(this.project);
            pickupFactory = new PickupFactory(project);
            lowQualityTextures = new Dictionary<Bitmap, BitmapSet>();
            polygonController = new Tm2PcPolygonController(project);

            ValidateStaticObjects();

            result.Reset();
            worker.ReportProgress(0);
            var dishBytes = new byte[0];
            var dishBytesTpc = new byte[0];
            var dishBytesGrp = new byte[0];

            dishUrl = outputDirectory + @"\leveldb\DISH.DPC";
            dishUrlTpc = outputDirectory + @"\leveldb\DISH.TPC";
            dishPts = outputDirectory + @"\terrain\dish.pts";
            dishRmp = outputDirectory + @"\terrain\dish.rmp";
            dishGrp = outputDirectory + @"\terrain\dish.grp";

            bool validInput = false;

            string dir = Directory.GetCurrentDirectory();
            bool resourceFilesFound = true;
            string vanillaDishDpcDir = dir + "\\MapResources\\DISH.DPC";
            string vanillaDishTpcDir = dir + "\\MapResources\\DISH.TPC";
            if (!File.Exists(vanillaDishDpcDir))
            {
                resourceFilesFound = false;
                result.AddMessage("You must place an unmodified copy of DISH.DPC into the MapResources folder.");
            }
            if (!File.Exists(vanillaDishTpcDir))
            {
                resourceFilesFound = false;
                result.AddMessage("You must place an unmodified copy of DISH.TPC into the MapResources folder.");
            }
            if (!resourceFilesFound)
            {
                return;
            }

            try
            {
                dishBytes = File.ReadAllBytes(vanillaDishDpcDir);
                dishBytesTpc = File.ReadAllBytes(vanillaDishTpcDir);
                validInput = true;
            }
            catch (FileNotFoundException fnfe)
            {
            }
            try
            {
                dishBytesGrp = File.ReadAllBytes(dir + "\\MapResources\\Dish.grp");
                validInput = true;
            }
            catch (FileNotFoundException fnfe)
            {
                resourceFilesFound = false;
                result.AddMessage("The MapResources folder is missing Dish.grp.");
            }
            projectController.LoadProject(vanillaDishDpcDir, "DISH.DPC");

            if (outputDirectory == null || outputDirectory.Length == 0)
            {
                result.AddMessage("You must select an output directory.");
            }

            if (validInput)
            {
                var pathNodes = mapEditorProject.GetAllPathNodeInstances();
                bool validFiles = true;

                if (!pathNodes.Any())
                {
                    result.AddMessage("You must place at least one path node.");
                    validFiles = false;
                }

                if (validFiles)
                {
                    // Reset dish
                    using (writer = new BinaryWriter(new FileStream(dishUrl, FileMode.Create)))
                    {
                        writer.Write(dishBytes);
                    }
                    using (writer = new BinaryWriter(new FileStream(dishUrlTpc, FileMode.Create)))
                    {
                        writer.Write(dishBytesTpc);
                    }
                    using (writer = new BinaryWriter(new FileStream(dishGrp, FileMode.Create)))
                    {
                        writer.Write(dishBytesGrp);
                    }

                    project = new ProjectFactory().LoadProjectTypes()[GameType.TM2PC];


                    var projController = new ProjectController(project);
                    //var texArchiveController = new TextureArchiveController(project.TextureArchiveFile);
                    //texArchiveController.FileWriteProgressChangedEvent += TexArchiveController_FileWriteProgressChangedEvent;
                    projController.LoadProject(dishUrl, "DISH.DPC");
                    allInstructions = project.MeshFile.GetAllInstructions();

                    rootInstruction = new Dpc701BoundingSphere(project, 0, 1, 0, 0, 0, 0, 0, 0, 0);
                    rootInstruction.SetMaxRadius();
                    result.AddMessage("Creating level hierarchy...");
                    CreateInstructions(mapEditorProject.RootNode);
                    if (CheckIsCancelled())
                    {
                        return;
                    }
                    CreateInstructionHierarchy();

                    progressManager.TotalMeshes = newMeshes.Count;


                    // Create textures
                    result.AddMessage("Creating textures...");
                    var uniqueBitmaps = new List<Bitmap>();
                    CreateBitmaps(uniqueBitmaps, mapEditorProject.RootNode);
                    uniqueBitmaps = uniqueBitmaps.Distinct().ToList();
                    progressManager.TotalTextures = uniqueBitmaps.Count;

                    UpdateProgress();

                    foreach (var tex in project.TextureArchiveFile.TextureFiles)
                    {
                        if (CheckIsCancelled())
                        {
                            return;
                        }
                        if (uniqueBitmaps.Contains(tex.BitmapFilePair.Bitmap))
                        {
                            progressManager.CurrentTexture++;
                            tex.TransparencyEnabled = true;
                            UpdateProgress();
                        }
                    }

                    UpdateProgress();

                    if (CheckIsCancelled())
                    {
                        return;
                    }

                    long newInstructionLength = rootInstruction.GetRecursiveBufferLength();

                    result.AddMessage("Creating meshes...");
                    foreach (var newMesh in newMeshes)
                    {
                        if (CheckIsCancelled())
                        {
                            return;
                        }
                        newInstructionLength += newMesh.GetBufferSize();
                    }
                    UpdateProgress();

                    dishBytes = File.ReadAllBytes(dishUrl);
                    long dishLength = dishBytes.Length;

                    UpdateProgress();

                    byte[] newFileBuffer = new byte[(dishBytes.Length + newInstructionLength)];

                    result.AddMessage("Writing meshes to DPC file...");
                    using (writer = new BinaryWriter(new MemoryStream(newFileBuffer)))
                    {
                        writer.Write(dishBytes);

                        foreach (var mesh in newMeshes)
                        {
                            if (CheckIsCancelled())
                            {
                                return;
                            }
                            project.MeshWriter.WriteNewMesh(writer, mesh);

                            progressManager.CurrentMesh++;
                            UpdateProgress();
                        }

                        long startAddress = writer.BaseStream.Position;
                        rootInstruction.WriteInstruction(writer);
                        rootInstruction.WriteInstructionPointers(writer);

                        writer.BaseStream.Position = 36;
                        WritePointer(startAddress);
                        UpdateProgress();

                    }
                    File.WriteAllBytes(dishUrl, newFileBuffer);
                    var texController = new TextureArchiveController(project.TextureArchiveFile);
                    texController.Progress.FileWriteProgressChangedEvent += Progress_FileWriteProgressChangedEvent1;
                    result.AddMessage("Writing textures to TPC file...");
                    texController.WriteFile(project.TextureArchiveFileName);

                    result.AddMessage("Writing ramps file...");
                    CreateRampsFile();
                    result.AddMessage("Writing points file...");
                    CreatePointsFile();

                    if (!result.WarningsExist)
                    {
                        result.AddMessage($"Map compilation succeeded.");
                    }
                    else
                    {
                        result.AddMessage($"Map compilation succeeded but with warnings.");

                        foreach (var warning in result.Warnings)
                        {
                            result.AddMessage(warning);
                        }
                    }
                    if (CheckIsCancelled())
                    {
                        return;
                    }
                    worker.ReportProgress(100);
                }
            }
        }

        private void Progress_FileWriteProgressChangedEvent1(object sender, EventArgs e)
        {
            progressManager.TextureFileWriteProgress = ((ProgressManager)sender).FileWriteProgress;
            UpdateProgress();
        }

        private void CreatePointsFile()
        {
            var pathNodes = mapEditorProject.GetAllPathNodeInstances();
            pointsFile.Points.Clear();

            for (int pathNodeCount = 0; pathNodeCount < pathNodes.Count; pathNodeCount++)
            {
                var pathNode = pathNodes[pathNodeCount];
                //pathNode.ResetConnections();
                int id = pathNodeCount;

                var pos = pathNode.Transformation.WorldMatrix.Translation;

                var pointNode = new PointNode(PointsFile.PointHeader1, pos, Color.Aqua, 0, 0, 0, 0, 0, 0);

                pointsFile.Points.Add(pointNode);
            }

            LinkPaths();

            pointsFileController.WritePointsFile(dishPts);
        }

        private void LinkPaths()
        {
            for (int iPt = 0; iPt < pointsFile.Points.Count(); iPt++)
            {
                var pt0 = pointsFile.Points[iPt];
                var path0 = mapEditorProject.PathNodeContainer.PathNodes[iPt];

                var connections = mapEditorProject.PathConnectionContainer.PathNodeConnections;

                for (int iConnection = 0; iConnection < connectionLimit; iConnection++)
                {
                    if (iConnection < connections.Count())
                    {
                        var connection = connections.ElementAt(iConnection);
                        int otherIndex = mapEditorProject.PathNodeContainer.PathNodes.IndexOf(connection.P1.PathNode);
                        //pt0.ConnectedNodes[iConnection] = pointsFile.Points.ElementAt(otherIndex);
                        pt0.AddConnectedNode(pointsFile.Points[otherIndex]);
                    }
                    else
                    {
                        var connection = connections.ElementAt(0);
                        int otherIndex = mapEditorProject.PathNodeContainer.PathNodes.IndexOf(connection.P1.PathNode);
                        pt0.AddConnectedNode(pointsFile.Points[otherIndex]);
                    }
                }
            }
        }

        private void CreateRampsFile()
        {
            rampsFileController.WriteRampsFile(dishRmp);
        }

        private void SubdivideTextureFix(Mesh mesh)
        {
            var polys = mesh.GetAllPolygons();

            foreach (var poly in polys)
            {
                while (polygonController.NeedsSubdivisionTextureFix(poly))
                {
                    SubdivideTextureFix(mesh, poly);
                }
            }
            var polysFinal = mesh.GetFinalPolygons();
            mesh.RemoveAllPolygons();
            mesh.AddPolygonRange(polysFinal);
        }

        private void SubdivideTextureFix(Mesh mesh, Polygon polygon)
        {
            if (polygonController.NeedsSubdivisionTextureFix(polygon))
            {
                polygon.Subdivide(mesh);

                foreach (var subPoly in polygon.SubPolygons)
                {
                    SubdivideTextureFix(mesh, subPoly);
                }
            }
        }

        private Mesh CreateLowQualityMesh(Mesh mesh)
        {
            var lowQualityMesh = new Mesh(mesh);
            var allPolys = lowQualityMesh.GetAllPolygons();

            foreach (var poly in allPolys)
            {
                if (poly.Textures.Diffuse.Bitmap != null)
                {
                    poly.Textures = lowQualityTextures[poly.Textures.Diffuse.Bitmap];
                }
            }

            return lowQualityMesh;
        }

        private DpcTranslationMatrix CreateGameItemInstruction(ObjectInstance node)
        {
            var translation = node.Transformation.PositionLocal;

            var item = pickupFactory.CreateGameItemInstruction(node, (int)translation.X, (int)translation.Y, (int)translation.Z);

            return item;
        }

        private void CreateGameItemInstructions(ObjectInstance node)
        {
            var translationInstruction = CreateGameItemInstruction(node);

            rawInstructionListItems.Add(translationInstruction);
        }

        private void CreateMeshInstructions(ObjectInstance node)
        {
            /*var translationMatrix = new DpcTranslationMatrix(project, 0, MatrixType.Default, node.Transformation.WorldMatrix, 0, 0, 0);
            var rotationMatrix = new DpcMatrix(project, 0);
            Matrix4x4 rot = node.Transformation.LocalMatrix;
            rot.Translation = new Vector3();
            rotationMatrix.MatrixWrapper.Matrix = rot;*/
            var worldMatrix = node.Transformation.WorldMatrix;

            var lights = mapEditorProject.GetAllLights();
            
            foreach (var mesh in node.StaticObject.Meshes)
            {
                var worldMatrixInstruction = new DpcMatrix(project, 0);
                worldMatrixInstruction.MatrixWrapper.Matrix = worldMatrix;
                rampsFile.Ramps.AddRange(rampsFileController.CreateRampsFromMesh(mesh, node.Transformation.WorldMatrix));
                

                var finalPolygons = mesh.GetFinalPolygons();

                for (int iPoly = 0; iPoly < finalPolygons.Count; iPoly++)
                {
                    var polygon = finalPolygons[iPoly];
                    var cp = Utility.FindCrossProduct(
                        mesh.Vertices[polygon.GetPolygonIndex(0).Id].Vector,
                        mesh.Vertices[polygon.GetPolygonIndex(1).Id].Vector,
                        mesh.Vertices[polygon.GetPolygonIndex(2).Id].Vector
                        );

                    polygon.CrossProduct = cp;

                    foreach (var index in polygon.GetIndices())
                    {
                        index.DiffuseColor = Color.Black;

                        var finalLightColor = new Vector3();
                        foreach (var light in lights)
                        {
                            var intensity = 1.0f;
                            if (light.LightType == ColladaLibrary.Render.Lighting.LightType.Directional)
                            {
                                intensity = light.Intensity * Vector3.Dot(cp, light.GetDirection());
                            }
                            if (float.IsNaN(intensity))
                            {
                                intensity = 1.0f;
                            }
                            if (intensity > 1.0f)
                            {
                                intensity = 1.0f;
                            }
                            if (intensity < 0.0f)
                            {
                                intensity = -intensity;
                            }
                            var lightColor = new Vector3(light.Color.R * intensity, light.Color.G * intensity, light.Color.B * intensity);
                            finalLightColor += new Vector3(lightColor.X + index.DiffuseColor.R, lightColor.Y + index.DiffuseColor.G, lightColor.Z + index.DiffuseColor.B);

                            if (finalLightColor.X > 255.0f)
                            {
                                finalLightColor.X = byte.MaxValue;
                            }
                            if (finalLightColor.Y > 255.0f)
                            {
                                finalLightColor.Y = byte.MaxValue;
                            }
                            if (finalLightColor.Z > 255.0f)
                            {
                                finalLightColor.Z = byte.MaxValue;
                            }

                        }
                        index.DiffuseColor = Color.FromArgb((int)finalLightColor.X, (int)finalLightColor.Y, (int)finalLightColor.Z);
                    }
                }

                var gameMesh = Utility.ConvertMeshToGameMeshTm2Pc(project, mesh);
                var meshContainer = new DpcMeshContainer602(project, 0);
                var dpcMeshLink = new DpcMeshLink(project, gameMesh.Address);
                dpcMeshLink.MeshList.Add(gameMesh);
                dpcMeshLink.MinDrawDistance = (uint)node.StaticObject.MinDrawDistance;
                dpcMeshLink.MaxDrawDistance = (uint)node.StaticObject.MaxDrawDistance;
                meshContainer.Children.Add(dpcMeshLink);
                newMeshes.Add(gameMesh);
                //rotationMatrix.Children.Add(meshContainer);
                worldMatrixInstruction.GlobalMatrix = node.Transformation.WorldMatrix;
                worldMatrixInstruction.Children.Add(meshContainer);
                rawInstructionListMeshesOnly.Add(mesh);
                rawInstructionListMeshes.Add(worldMatrixInstruction);

                //subTranslationMatrix.Children.Add(subBoundingSphere);
                //currentBoundingSphere.Children.Add(subBoundingSphere);

                if (node.StaticObject.HasCollisionResponse)
                {
                    CreateCollisionInstructions(node);
                }
            }
        }

        private Matrix4x4 InvertScale(Matrix4x4 matrix)
        {
            var invertScaleMatrix = Matrix4x4.CreateScale(new Vector3(1.0f / matrix.M11, 1.0f / matrix.M22, 1.0f / matrix.M33));
            matrix = Matrix4x4.Multiply(matrix, invertScaleMatrix);

            return matrix;
        }

        private void CreateInstructions(ObjectInstance parentNode)
        {
            var translation = parentNode.Transformation.GetLocalMatrix().Translation;

            var staticObjectChildren = parentNode.Children.Where(o => o.ObjectType == ObjectType.StaticObject);
            var gameItems = parentNode.Children.Where(o => o.ObjectType == ObjectType.GameItemObject);

            // Create mesh nodes
            foreach (var childNode in staticObjectChildren)
            {
                CreateMeshInstructions(childNode);
            }

            foreach (var itemNode in gameItems)
            {
                CreateGameItemInstructions(itemNode);
            }

            foreach (var child in parentNode.Children)
            {
                CreateInstructions(child);
            }
        }

        private void ValidateStaticObjects()
        {
            var staticObjectDefinitions = mapEditorProject.StaticObjectDefinitionContainer.StaticObjectDefinitions;
            var sb = new StringBuilder();

            bool valid = true;

            foreach (var staticObject in staticObjectDefinitions)
            {
                if (!ValidateStaticObjectDefinition(sb, staticObject))
                {
                    valid = false;
                }
            }
        }

        private bool ValidateStaticObjectDefinition(StringBuilder sb, StaticObjectDefinition definition)
        {
            bool valid = true;

            if (!definition.IsBillboard && definition.ModelUrl == null)
            {
                valid = false;
                result.AddWarning($"Static Object {definition.Name} does not have a mesh.");
            }
            if (definition.CollisionModelUrl == null)
            {
                valid = false;
                result.AddWarning($"Static Object {definition.Name} does not have a collision mesh.");
            }

            return valid;
        }

        private Dpc701BoundingSphere WrapTwoBoundingSpheres(Dpc701BoundingSphere s0, Dpc701BoundingSphere s1)
        {
            var wrapper = new Dpc701BoundingSphere(project, 0);

            var diameter = Vector3.Distance(s0.BoundingSphere.Center, s1.BoundingSphere.Center) + s0.Radius + s1.Radius;
            var origin = (s0.BoundingSphere.Center + s1.BoundingSphere.Center) / 2.0f;
            wrapper.SetRadius((uint)(diameter / 2.0f));
            wrapper.Children.Add(s0);
            wrapper.Children.Add(s1);

            return wrapper;
        }

        private List<Dpc701BoundingSphere> looseBoundingSpheres = new List<Dpc701BoundingSphere>();

        private List<Dpc701BoundingSphere> GroupBoundingSpheres(List<Dpc701BoundingSphere> originalSpheres)
        {
            var originalSpheresSmallToBig = originalSpheres.OrderBy(s => s.Radius).ToList();
            var remainingOriginalSpheresSmallToBig = new List<Dpc701BoundingSphere>(originalSpheresSmallToBig);
            var completedSpheres = new List<Dpc701BoundingSphere>();

            while (remainingOriginalSpheresSmallToBig.Any())
            //for (int iSpheres = 0; iSpheres < originalSpheresSmallToBig.Count; iSpheres++)
            {
                var sphere = remainingOriginalSpheresSmallToBig.First();
                remainingOriginalSpheresSmallToBig.Remove(sphere);
                var closestSphere = remainingOriginalSpheresSmallToBig.OrderBy(s => Vector3.Distance(sphere.BoundingSphere.Center, s.BoundingSphere.Center)).FirstOrDefault();
                if (closestSphere != null)
                {
                    remainingOriginalSpheresSmallToBig.Remove(closestSphere);
                    var wrapper = WrapTwoBoundingSpheres(sphere, closestSphere);
                    completedSpheres.Add(wrapper);
                }
                else
                {
                    completedSpheres.Add(sphere);
                }
            }

            if (completedSpheres.Count() <= 10)
            {
                return completedSpheres;
            }
            else
            {
                return GroupBoundingSpheres(completedSpheres);
            }
        }

        private void CreateInstructionHierarchy()
        {
            var currentRoot = rootInstruction;

            for (int i = 0; i < rawInstructionListMeshesOnly.Count; i++)
            {
                var matrixMeshContainer = rawInstructionListMeshes[i];
                var mesh = rawInstructionListMeshesOnly[i];
                var matrixSphere = new Dpc701BoundingSphere(project, 0);
                matrixSphere.SetRadius((uint)mesh.GetBoundingSphereDiameter());
                matrixSphere.SetOrigin(matrixMeshContainer.MatrixWrapper.Matrix.Translation);
                matrixSphere.Children.Add(matrixMeshContainer);
                looseBoundingSpheres.Add(matrixSphere);
                //currentRoot.Children.Add(matrixSphere);
            }
            for (int i = 0; i < rawInstructionListCollision.Count; i++)
            {
                var collisionInstruction = rawInstructionListCollision[i];
                var matrixSphere = new Dpc701BoundingSphere(project, 0);
                //matrixSphere.SetMaxRadius();
                matrixSphere.SetRadius((uint)rawInstructionListCollisionRadius[i]);
                //matrixSphere.SetOrigin(collisionInstruction.GlobalMatrix.Translation);
                matrixSphere.Children.Add(collisionInstruction);
                looseBoundingSpheres.Add(matrixSphere);
            }
            for (int i = 0; i < rawInstructionListItems.Count; i++)
            {
                var itemInstruction = rawInstructionListItems[i];
                var matrixSphere = new Dpc701BoundingSphere(project, 0);
                matrixSphere.SetMaxRadius();
                matrixSphere.Children.Add(itemInstruction);
                looseBoundingSpheres.Add(matrixSphere);
            }
            rootInstruction.Children.AddRange(GroupBoundingSpheres(looseBoundingSpheres));
        }
    }
}