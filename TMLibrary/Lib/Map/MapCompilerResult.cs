﻿using System;
using System.Collections.Generic;

namespace TMLibrary.Map
{
    /// <summary>
    /// Returns a message and if the map conversion was a success.
    /// </summary>
    public class MapCompilerResult
    {
        private object messageLock;
        private bool warningsExist;
        private bool cancelled;
        private bool success;
        private string message;
        private List<string> warnings;

        public event EventHandler MessageChangedEvent;

        public virtual void OnMessageChanged()
        {
            if (MessageChangedEvent != null)
            {
                MessageChangedEvent(this, EventArgs.Empty);
            }
        }

        public MapCompilerResult()
        {
            warnings = new List<string>();
            messageLock = new object();
            message = string.Empty;
        }

        public void Reset()
        {
            warnings.Clear();
            success = false;
            message = string.Empty;
            OnMessageChanged();
        }

        public MapCompilerResult(bool success) : this()
        {
            this.success = success;
        }

        public MapCompilerResult(bool success, string message) : this()
        {
            this.success = success;
            lock (messageLock)
            {
                this.message = message;
            }
        }

        public void AddMessage(string message)
        {
            lock (messageLock)
            {
                this.message += message + "\n";
                OnMessageChanged();
            }
        }

        public bool Success { get => success; }
        public string Message
        {
            get
            {
                lock (messageLock)
                {
                    return message;
                }
            }
        }

        public void AddWarning(string message)
        {
            warnings.Add($"Warning: {message}");
        }

        public List<string> Warnings { get => warnings; }
        public bool WarningsExist { get { return warnings.Count > 0; } }

        public bool Cancelled { get => cancelled; set => cancelled = value; }
    }
}
