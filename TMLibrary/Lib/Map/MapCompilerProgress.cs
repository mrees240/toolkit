﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.Map
{
    public class MapCompilerProgress
    {
        private int totalMeshes;
        private int currentMesh;
        private int totalTextures;
        private int currentTexture;

        private double pointFileWriteProgress;
        private double rampFileWriteProgress;
        private double textureFileWriteProgress;

        public void Reset()
        {
            totalMeshes = 0;
            currentMesh = 0;
            textureFileWriteProgress = 0;
            rampFileWriteProgress = 0;
            totalTextures = 0;
            currentTexture = 0;
            pointFileWriteProgress = 0;
        }

        public double GetCurrentProgress()
        {
            // These values must add up to 1
            double meshCountTotalPercentage = 0.1;
            double textureFileWriteTotalPercentage = 0.6;
            double textureCountTotalPercentage = 0.1;
            double rampFileWriteTotalPercentage = 0.1;
            double pointFileWriteTotalPercentage = 0.1;

            double progress = (
                ((
                ((currentMesh / (double)totalMeshes) * meshCountTotalPercentage) +
                (textureFileWriteProgress * textureFileWriteTotalPercentage) +
                (currentTexture / (double)totalTextures * textureCountTotalPercentage) +
                (rampFileWriteProgress * rampFileWriteTotalPercentage) +
                (pointFileWriteProgress * pointFileWriteTotalPercentage))
                * 100.0
                ));

            return progress;
        }

        public int TotalMeshes { get => totalMeshes; set => totalMeshes = value; }
        public int CurrentMesh { get => currentMesh; set => currentMesh = value; }
        public double TextureFileWriteProgress { get => textureFileWriteProgress; set => textureFileWriteProgress = value; }
        public int TotalTextures { get => totalTextures; set => totalTextures = value; }
        public int CurrentTexture { get => currentTexture; set => currentTexture = value; }
        public double RampFileWriteProgress { get => rampFileWriteProgress; set => rampFileWriteProgress = value; }
        public double PointFileWriteProgress { get => pointFileWriteProgress; set => pointFileWriteProgress = value; }
    }
}
