﻿using System;
using System.Collections.Generic;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     Defines a pointer found in a mesh archive file. Pointers in mesh archive files point to different addresses in the
    ///     file.
    /// </summary>
    public class Pointer : IComparer<Pointer>, IComparable<Pointer>
    {
        // If the pointer points to a mesh, its ID is stored.
        private int meshId;
        private long address;
        private long pointToAddress;
        private PointerType pointerType;
        private uint valueAtPointToAddress;

        public Pointer(long address, long pointToAddress)
        {
            this.address = address;
            this.pointToAddress = pointToAddress;
        }

        public long Address
        {
            get => address;
        }
        public long PointToAddress
        {
            get => pointToAddress;
        }
        public PointerType PointerType
        {
            get => pointerType;
            set => pointerType = value;
        }
        public int MeshId
        {
            get => meshId;
            set => meshId = value;
        }

        public uint ValueAtPointToAddress
        {
            get { return valueAtPointToAddress; }
            set { valueAtPointToAddress = value; }
        }

        public int CompareTo(Pointer other)
        {
            return Compare(this, other);
        }

        public int Compare(Pointer x, Pointer y)
        {
            if (x.Address > y.Address)
            {
                return 1;
            }
            if (x.Address < y.Address)
            {
                return -1;
            }
            return 0;
        }
    }
}