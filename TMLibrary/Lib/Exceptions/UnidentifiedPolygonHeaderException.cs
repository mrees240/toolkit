﻿using System;
using System.Text;
using TMLibrary.Lib.Util;

namespace TMLibrary.Lib.Exceptions
{
    /// <summary>
    ///     An exception that is thrown when a binary reader attempts to read a polygon with an unrecognized header.
    /// </summary>
    public class UnidentifiedPolygonHeaderException : Exception
    {
        public UnidentifiedPolygonHeaderException(ushort[] headerBuffer)
        {
        }

        private string CreateErrorMessage(ushort[] headerBuffer)
        {
            var builder = new StringBuilder();

            builder.Append(string.Format("Unrecognized polygon header: {0}", ArrayUtility.ArrayToString(headerBuffer)));

            return builder.ToString();
        }
    }
}