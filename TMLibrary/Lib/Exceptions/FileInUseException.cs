﻿using System;

namespace TMLibrary.Lib.Exceptions
{
    /// <summary>
    ///     An exception that is thrown when a file is in use. Provides a message that the user can understand.
    /// </summary>
    public class FileInUseException : Exception
    {
        public FileInUseException(string fileName) : base(
            $"Unable to save {fileName}. It's being used by another process.")
        {
        }
    }
}