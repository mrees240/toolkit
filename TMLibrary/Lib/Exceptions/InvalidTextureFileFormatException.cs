﻿using System;

namespace TMLibrary.Lib.Exceptions
{
    /// <summary>
    ///     An exception that is thrown when the user tries to load a texture file with an unrecognized file format.
    /// </summary>
    public class InvalidTextureFileFormatException : Exception
    {
        public InvalidTextureFileFormatException(string extension) :
            base($"Unable to load texture with {extension} extension.")
        {
        }
    }
}