﻿using System;

namespace TMLibrary.Lib.Exceptions
{
    /// <summary>
    ///     An exception that is thrown when the user tries to open a file that the toolkit doesn't have defined.
    /// </summary>
    public class InvalidFileTypeException : Exception
    {
        public InvalidFileTypeException(string fileSignature, string correctFileSignature) :
            base($"The file signature was invalid. Expected {correctFileSignature} but read {fileSignature}")
        {
        }
    }
}