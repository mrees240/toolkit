﻿using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     Provides an interface for creating a project definition type.
    /// </summary>
    public interface IProjectTypeCreator
    {
        ProjectDefinition CreateProjectType();
    }
}