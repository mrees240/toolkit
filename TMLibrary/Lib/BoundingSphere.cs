﻿using System.Numerics;

namespace TMLibrary.Lib
{
    public class BoundingSphere
    {
        private float radius;
        private Vector3 center;

        public BoundingSphere(Vector3 center, float radius)
        {
            this.center = center;
            this.Radius = radius;
        }

        public Vector3 Center { get => center; set => center = value; }

        public float Radius
        {
            get { return radius; }
            set { radius = value; }
        }
    }
}
