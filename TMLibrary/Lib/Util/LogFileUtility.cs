﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TMLibrary.Lib.Util
{
    /// <summary>
    ///     Provides a controller for easily creating and writing to log files.
    /// </summary>
    public class LogFileUtility
    {
        public static readonly string FileName = "log.txt";
        public static readonly string LogDirectory = @"logs\";

        public static string CreateErrorMessage(string message)
        {
            return $"{message} Check {LogDirectory}{FileName} for more info.";
        }

        private static string GetDate()
        {
            return $"{DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}";
        }

        public static void AppendToLogFile(string message)
        {
            string date = GetDate();
            if (!Directory.Exists(LogDirectory))
            {
                Directory.CreateDirectory(LogDirectory);
            }
            File.AppendAllText(@"logs\" + FileName, $"{date} - {message}\n");
        }

        public static void AppendToLogFile(string fileName, string message)
        {
            string date = GetDate();
            if (!Directory.Exists(LogDirectory))
            {
                Directory.CreateDirectory(LogDirectory);
            }
            File.AppendAllText(@"logs\" + fileName, $"{date} - {message}\n");
        }

        public static void AppendToLogFile(List<string> messages)
        {
            AppendToLogFile(FileName, messages);
        }

        public static void AppendToLogFile(string fileName, List<string> messages)
        {
            string date = GetDate();
            if (!Directory.Exists(LogDirectory))
            {
                Directory.CreateDirectory(LogDirectory);
            }
            foreach (var message in messages)
            {
                File.AppendAllText(@"logs\" + fileName, $"{date} - {message}\n");
            }
        }
    }
}