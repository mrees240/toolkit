﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TMLibrary.Lib.Util
{
    /// <summary>
    ///     Provides methods for converting arrays to different formats. May need optimized to improve efficiency.
    /// </summary>
    public class ArrayUtility
    {
        public static bool ArraysAreDuplicates(ushort[] array1, ushort[] array2, int elementsToCheck)
        {
            if (elementsToCheck > array1.Length || elementsToCheck > array2.Length)
                elementsToCheck = Math.Min(array1.Length, array2.Length);

            var subArray1 = new ushort[elementsToCheck];
            var subArray2 = new ushort[elementsToCheck];

            Array.Copy(array1, subArray1, elementsToCheck);
            Array.Copy(array2, subArray2, elementsToCheck);

            return ArraysAreDuplicates(subArray1, subArray2);
        }

        public static byte[] StringToArray(string hexString)
        {
            var hexData = hexString.Split(' ');
            var array = new byte[hexData.Length];
            for (var i = 0; i < array.Length; i++)
                array[i] = byte.Parse(hexData[i], NumberStyles.HexNumber);
            return array;
        }

        public static byte[] ToByteArray(short[] array)
        {
            var length = array.Length * 2;
            var byteArray = new byte[length];
            for (var iElement = 0; iElement < array.Length; iElement++)
            {
                var bytes = BitConverter.GetBytes(array[iElement]);
                byteArray[iElement * 2] = bytes[0];
                byteArray[iElement * 2 + 1] = bytes[1];
            }
            return byteArray;
        }

        public static byte[] ToByteArray(ushort[] array)
        {
            var length = array.Length * 2;
            var byteArray = new byte[length];
            for (var iElement = 0; iElement < array.Length; iElement++)
            {
                var bytes = BitConverter.GetBytes(array[iElement]);
                byteArray[iElement * 2] = bytes[0];
                byteArray[iElement * 2 + 1] = bytes[1];
            }
            return byteArray;
        }

        public static bool ArraysAreDuplicates(short[] array1, short[] array2, int elementsToCheck)
        {
            if (elementsToCheck > array1.Length || elementsToCheck > array2.Length)
                elementsToCheck = Math.Min(array1.Length, array2.Length);

            var subArray1 = new short[elementsToCheck];
            var subArray2 = new short[elementsToCheck];

            Array.Copy(array1, subArray1, elementsToCheck);
            Array.Copy(array2, subArray2, elementsToCheck);

            return ArraysAreDuplicates(subArray1, subArray2);
        }

        public static double[] ToDoubleArray(string[] strings)
        {
            var fixedStrings = strings.Where(s => s.Length > 0);
            var doubleArray = new double[fixedStrings.Count()];
            for (var iIndex = 0; iIndex < doubleArray.Length; iIndex++)
                doubleArray[iIndex] = double.Parse(strings[iIndex]);
            return doubleArray;
        }

        public static float[] ToFloatArray(string[] strings)
        {
            var fixedStrings = strings.Where(s => s.Length > 0);
            var floatArray = new float[fixedStrings.Count()];
            for (var iIndex = 0; iIndex < floatArray.Length; iIndex++)
                floatArray[iIndex] = float.Parse(strings[iIndex]);
            return floatArray;
        }

        public static int[] ToIntArray(string[] strings)
        {
            var fixedStrings = strings.Where(s => s.Length > 0);
            var intArray = new int[fixedStrings.Count()];
            for (var iIndex = 0; iIndex < intArray.Length; iIndex++)
                intArray[iIndex] = int.Parse(fixedStrings.ElementAt(iIndex));
            return intArray;
        }

        public static uint[] ToUIntArray(string[] strings)
        {
            var uIntArray = new uint[strings.Length];
            for (var iIndex = 0; iIndex < uIntArray.Length; iIndex++)
                uIntArray[iIndex] = uint.Parse(strings[iIndex]);
            return uIntArray;
        }

        public static short[] ToShortArray(byte[] array)
        {
            var length = array.Length / 2;
            var shortArray = new short[length];
            for (var iElement = 0; iElement < shortArray.Length; iElement++)
                shortArray[iElement] = BitConverter.ToInt16(array, iElement * 2);
            return shortArray;
        }

        public static short[] ToShortArray(ushort[] array)
        {
            var length = array.Length;
            var shortArray = new short[length];
            for (var iElement = 0; iElement < shortArray.Length; iElement++)
                try
                {
                    shortArray[iElement] = Convert.ToInt16(array[iElement]);
                }
                catch (OverflowException e)
                {
                    shortArray[iElement] = Convert.ToInt16(array[iElement] - 32768);
                }
            return shortArray;
        }

        public static ushort[] ToUShortArray(byte[] array)
        {
            var length = array.Length / 2;
            var uShortArray = new ushort[length];
            for (var iElement = 0; iElement < uShortArray.Length; iElement++)
                uShortArray[iElement] = BitConverter.ToUInt16(array, iElement * 2);
            return uShortArray;
        }

        public static bool ArraysAreDuplicates(ushort[] array1, ushort[] array2)
        {
            if (array1.Length != array2.Length)
                return false;

            for (var index = 0; index < array1.Length; index++)
                if (array1[index] != array2[index])
                    return false;

            return true;
        }

        public static bool ArraysAreDuplicates(short[] array1, short[] array2)
        {
            if (array1.Length != array2.Length)
                return false;

            for (var index = 0; index < array1.Length; index++)
                if (array1[index] != array2[index])
                    return false;

            return true;
        }

        public static string ArrayToString(ushort[] array)
        {
            var byteBuffer = new byte[array.Length * sizeof(ushort)];
            var builder = new StringBuilder();
            foreach (int iElement in array)
                builder.Append(iElement + " ");
            return builder.ToString();
        }

        public static string ArrayToString(short[] array)
        {
            var byteBuffer = new byte[array.Length * sizeof(short)];
            var builder = new StringBuilder();
            foreach (int iElement in array)
                builder.Append(iElement + " ");
            return builder.ToString();
        }

        public static string ArrayToString(byte[] array)
        {
            return Regex.Replace(BitConverter.ToString(array), @"-", " ");
        }
    }
}