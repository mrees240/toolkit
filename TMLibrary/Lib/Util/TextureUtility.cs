﻿using ColladaLibrary.Utility;
using Kaliko.ImageLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace TMLibrary.Lib.Util
{
    public class TextureUtility
    {
        public static byte[] ImageToByteArray(Bitmap bitmap)
        {
            var stream0 = new MemoryStream();
            byte[] bytes = new byte[0];

            bitmap.Save(stream0, ImageFormat.Bmp);
            bytes = stream0.GetBuffer();
            /*try
            {
                bitmap.Save(stream0, ImageFormat.Bmp);
                bytes = stream0.GetBuffer();
            }
            catch (Exception ex)
            {
                var stream1 = new MemoryStream();
                var img2 = new Bitmap(bitmap);
                if (bitmap.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    img2 = TextureUtility.ConvertTo8Bit(img2);
                }
                img2.Save(stream1, ImageFormat.Bmp);
                bytes = stream1.GetBuffer();
                img2.Dispose();
                stream1.Dispose();
            }*/

            stream0.Dispose();
            return bytes;
            /*ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bitmap, typeof(byte[]));*/
        }

        public static Bitmap ConvertTo8Bit(Bitmap bitmap)
        {
            /*ImageDitherUtility.ApplyDitherToImage(image);

            byte[] imageBytes;


            using (var stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Gif);
                imageBytes = stream.GetBuffer();
                File.WriteAllBytes("M:\\file10.bmp", imageBytes);
            }
            using (var stream = new MemoryStream(imageBytes))
            {
                return new Bitmap(stream);
            }*/


            var ditheredBitmapSave = new Bitmap(bitmap);
            ImageDitherUtility.ApplyDitherToImage(ditheredBitmapSave);
            var ditheredBitmapSave2 = ImageConversionUtility.CopyToBpp(ditheredBitmapSave, 8);
            /*ditheredBitmapSave.Save("M:\\file9.bmp", ImageFormat.Bmp);
            ditheredBitmapSave.Save("M:\\file10.gif", ImageFormat.Gif);
            ditheredBitmapSave.Dispose();
            ditheredBitmapSave = new Bitmap("M:\\file10.gif");

            ditheredBitmapSave.Save("M:\\file11.bmp", ImageFormat.Bmp);*/

            /*using (var stream = new MemoryStream())
            {
                ditheredBitmapSave.Save(stream, ImageFormat.Bmp);
                ditheredBitmapSave.Dispose();
                ditheredBitmapSave = new Bitmap(stream);
            }*/
            //ditheredBitmapSave.Save("M:\\file10.bmp", ImageFormat.Bmp);
            //var ditheredBitmap = new Bitmap("M:\\file10.bmp");
            /*KalikoImage bitmap8Bit;
            KalikoImage img = new KalikoImage(ditheredBitmap);

            using (var stream = new MemoryStream())
            {
                img.SaveBmp(stream, ImageFormat.Gif);
                File.WriteAllBytes("M:\\file10.bmp", stream.GetBuffer());
                bitmap8Bit = new KalikoImage("M:\\file10.bmp");
            }*/
            ditheredBitmapSave.Dispose();

            return ditheredBitmapSave2;

            /*Bitmap bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color temp = image.GetPixel(i, j);
                    bitmap.SetPixel(i, j, temp);
                }
            }
            using (var stream = new MemoryStream())
            {
                bitmap.Save(@"C:\bit24.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
            }*/
        }

        public static Color UShortToDrawColor(ushort pixelBufferValue, bool flipRgb)
        {
            var winColor = UShortToColor(pixelBufferValue, flipRgb);
            return Color.FromArgb(byte.MaxValue, winColor.R, winColor.G, winColor.B);
        }


        /// <summary>
        ///     Gets a color from a 2-byte value within a TIM image.
        /// </summary>
        /// <param name="pixelBufferValue"></param>
        /// <returns>Returns the color found in the pixel.</returns>
        public static System.Windows.Media.Color UShortToColor(ushort pixelBufferValue, bool flipRgb)
        {
            var color = new System.Windows.Media.Color();
            var r = (byte)((pixelBufferValue & 31) << 3);
            var g = (byte)(((pixelBufferValue >> 5) & 31) << 3);
            var b = (byte)(((pixelBufferValue >> 10) & 31) << 3);
            byte a = 255;
            if (!flipRgb)
            {
                color = System.Windows.Media.Color.FromArgb(a, r, g, b);
            }
            else
            {
                color = System.Windows.Media.Color.FromArgb(a, b, g, r);
            }
            return color;
        }
    }
}
