﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using ColladaLibrary.Extensions;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Rendering;
using TMLibrary.Lib.Definitions;
using TMLibrary.Rendering;
using TMLibrary.Rendering.DpcTm2;
using TMLibrary.Textures;

namespace TMLibrary.Lib.Util
{
    /// <summary>
    ///     Provides a general utility class for reading from game files.
    /// </summary>
    public class Utility
    {
        /// <summary>
        ///     Converts a uint pointer found in a mesh archive file and returns its point to address.
        /// </summary>
        /// <param name="pointer"></param>
        /// <returns></returns>
        public static long ReadPointer(uint pointer, MeshArchiveDefinition definition)
        {
            var value = (ushort) (pointer & 0xFFFF);
            var pointerTag = (ushort) (pointer >> 16);
            var pointerDefinition =
                definition.PointerDefinitions.Where(p => p.HeaderTag == pointerTag).FirstOrDefault();
            if (pointerDefinition != null)
            {
                return value + pointerDefinition.PointerOperation;
            }
            return -1;
        }

        /// <summary>
        /// Converts a color to a vector object.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Vector4 ColorToVector(System.Drawing.Color color)
        {
            return new Vector4(color.B / 255.0f, color.G / 255.0f, color.R / 255.0f, 1.0f);
        }

        /// <summary>
        ///     Creates a pointer for a file that points to another address within the file.
        /// </summary>
        /// <param name="originalAddress"></param>
        /// <param name="definition"></param>
        /// <returns></returns>
        public static uint CreatePointer(long originalAddress, MeshArchiveDefinition definition)
        {
            var matchingPointer = definition.PointerDefinitions
                .Where(p => originalAddress >= p.MinAddress && originalAddress <= p.MaxAddress).FirstOrDefault();
            var offset = (ushort) (originalAddress - matchingPointer.PointerOperation);
            var offsetBytes = BitConverter.GetBytes(offset);
            var pointerTagBytes = BitConverter.GetBytes(matchingPointer.HeaderTag);
            byte[] pointerBytes = {offsetBytes[0], offsetBytes[1], pointerTagBytes[0], pointerTagBytes[1]};
            var pointer = BitConverter.ToUInt32(pointerBytes, 0);

            return pointer;
        }

        /// <summary>
        ///     Reads a matrix and converts it to an XNA matrix;
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Matrix4x4 ParseXnaMatrix(BinaryReader reader)
        {
            float norm = 4096.0f;
            
            // SCALE = 0,
            float[] vals = new float[12];
            for (int i = 0; i < vals.Length; i++)
            {
                bool isTranslation = false;
                if (i >= 9)
                {
                    isTranslation = true;
                }
                int originalValue = reader.ReadInt32();

                if (isTranslation)
                {
                    vals[i] = originalValue;
                }
                else
                {
                    vals[i] = originalValue / norm;
                }
            }

            return new Matrix4x4(
                vals[0], vals[1], vals[2], 0.0f,
                vals[3], vals[4], vals[5], 0.0f,
                vals[6], vals[7], vals[8], 0.0f,
                vals[9], vals[10], vals[11], 1.0f
                );
        }

        public static Matrix4x4 ParseMatrixDmd(BinaryReader reader)
        {
            var norm = 4096.0;

            float[] m = new float[16];
            // SCALE = 0,
            m[0] = (float)(reader.ReadInt32() / norm);
            m[1] = (float)(reader.ReadInt32() / norm);
            m[2] = (float)(reader.ReadInt32() / norm);
            m[4] = (float)(reader.ReadInt32() / norm);
            m[5] = (float)(reader.ReadInt32() / norm);
            m[6] = (float)(reader.ReadInt32() / norm);
            m[8] = (float)(reader.ReadInt32() / norm);
            m[9] = (float)(reader.ReadInt32() / norm);
            m[10] = (float)(reader.ReadInt32() / norm);
            reader.BaseStream.Position += 2;
            m[12] = reader.ReadInt32();
            m[13] = reader.ReadInt32();
            m[14] = reader.ReadInt32();

            return new Matrix4x4(
                m[0], m[1], m[2], m[3],
                m[4], m[5], m[6], m[7],
                m[8], m[9], m[10], m[11],
                m[12], m[13], m[14], m[15]);
        }

        /// <summary>
        ///     Finds the cross product of a polygon's face.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        public static Vector3 FindCrossProduct(GamePolygon polygon)
        {
            if (!polygon.Parent.IsColladaBillboard)
            {
                var indices = polygon.GetIndices();

                if (indices.Length == 3 || indices.Length == 4)
                {
                    if (polygon.Parent.Vertices.Count() > indices.Select(p => p.Id).Max())
                    {
                        var v1 = polygon.Parent.Vertices[indices[0].Id];
                        var v2 = polygon.Parent.Vertices[indices[1].Id];
                        var v3 = polygon.Parent.Vertices[indices[2].Id];
                        var diff1 = Vector3.Subtract(v3.Vector, v1.Vector);
                        var diff2 = Vector3.Subtract(v2.Vector, v1.Vector);
                        var crossProduct = Vector3.Cross(diff1, diff2);
                        crossProduct = crossProduct.Normalize();
                        return crossProduct;
                    }
                }
            }
            return new Vector3();
        }

        public static Vector3 FindCrossProduct(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            //if (!polygon.Parent.IsColladaBillboard)
            {
                //var indices = polygon.GetIndices();

                //if (indices.Length == 3 || indices.Length == 4)
                {
                    //if (polygon.Parent.Vertices.Count() > indices.Select(p => p.Id).Max())
                    {
                        var diff1 = Vector3.Subtract(v3, v1);
                        var diff2 = Vector3.Subtract(v2, v1);
                        var crossProduct = Vector3.Cross(diff1, diff2);
                        crossProduct = crossProduct.Normalize();
                        return crossProduct;
                    }
                }
            }
            return new Vector3();
        }

        public static Vector3 FindCrossProductNonNormal(GamePolygon polygon)
        {
            var indices = polygon.GetIndices();
            var v1 = polygon.Parent.Vertices[indices[0].Id];
            var v2 = polygon.Parent.Vertices[indices[1].Id];
            var v3 = polygon.Parent.Vertices[indices[2].Id];
            var diff1 = Vector3.Subtract(v3.Vector, v1.Vector);
            var diff2 = Vector3.Subtract(v2.Vector, v1.Vector);
            var crossProduct = Vector3.Cross(diff1, diff2);
            //return crossProduct;
            return diff2;
        }

        public static Vector3 FindCenter(GameMesh mesh)
        {
            var minX = mesh.Vertices.Min(v => v.Vector.X);
            var minY = mesh.Vertices.Min(v => v.Vector.Y);
            var minZ = mesh.Vertices.Min(v => v.Vector.Z);
            var maxX = mesh.Vertices.Max(v => v.Vector.X);
            var maxY = mesh.Vertices.Max(v => v.Vector.Y);
            var maxZ = mesh.Vertices.Max(v => v.Vector.Z);

            var midX = (minX + maxX) / 2.0f;
            var midY = (minY + maxY) / 2.0f;
            var midZ = (minZ + maxZ) / 2.0f;

            return new Vector3(midX, midY, midZ);
        }

        // May not be correct
        public static Vector3 FindCenter(GamePolygon polygon)
        {
            var indices = polygon.GetIndices();
            int indexCount = indices.Length;
            if (indexCount == 3 || indexCount == 4)
            {
                if (indices.Select(p => p.Id).Max() < polygon.Parent.Vertices.Count())
                {
                    var v1 = polygon.Parent.Vertices[indices[0].Id];
                    var v2 = polygon.Parent.Vertices[indices[1].Id];
                    var v3 = polygon.Parent.Vertices[indices[2].Id];

                    var x = (v1.Vector.X + v2.Vector.X + v3.Vector.X) / 3.0f;
                    var y = (v1.Vector.Y + v2.Vector.Y + v3.Vector.Y) / 3.0f;
                    var z = (v1.Vector.Z + v2.Vector.Z + v3.Vector.Z) / 3.0f;

                    return new Vector3(x, y, z);
                }
            }
            return new Vector3();
        }

        private static void GetRecursivePolygons(Polygon poly, List<Polygon> polygons)
        {
            if (poly.SubPolygons.Count == 0)
            {
                polygons.Add(poly);
            }
            else
            {
                foreach (var subPoly in poly.SubPolygons)
                {
                    GetRecursivePolygons(subPoly, polygons);
                }
            }
        }

        // NEEDS FIXING?
        public static GameMesh ConvertMeshToGameMeshTm2Pc(ProjectDefinition project, Mesh mesh)
        {
            var baseMesh = new Mesh(mesh);
            var gameMesh = new GameMesh(project.MeshFile);

            gameMesh.Vertices.AddRange(baseMesh.Vertices);

            var finalPolys = baseMesh.GetFinalPolygons();

            for (int iPoly = 0; iPoly < finalPolys.Count; iPoly++)
            {
                var polygon = finalPolys[iPoly];

                var triangulatedPolys = polygon.GetTriangulatedPolygons();

                foreach (var tri in triangulatedPolys)
                {
                    var gamePoly = CreateGamePolygon(tri, gameMesh, project);
                    //gamePoly.PolySort0 = 16392;
                    gameMesh.Polygons.Add(gamePoly);
                }
            }
            gameMesh.MeshHeader = new MeshHeaderTm2(gameMesh);

            return gameMesh;
        }

        private static Random rng = new Random();

        private static GamePolygon CreateGamePolygon(Polygon polygon, GameMesh gameMesh, ProjectDefinition project)
        {
            int[] order = new int[] { 0, 2, 1 };
            var indices = polygon.GetIndices();
            var gamePoly = new GamePolygon(project, indices[order[0]], indices[order[1]], indices[order[2]]);
            var gamePolyIndices = gamePoly.GetIndices();
            gamePolyIndices[0].DiffuseColor = indices[order[0]].DiffuseColor;
            gamePolyIndices[1].DiffuseColor = indices[order[1]].DiffuseColor;
            gamePolyIndices[2].DiffuseColor = indices[order[2]].DiffuseColor;
            if (gamePolyIndices.Length == 4)
            {
                gamePolyIndices[3].DiffuseColor = indices[order[3]].DiffuseColor;
            }

            gamePoly.Parent = gameMesh;
            gamePoly.CrossProduct = Utility.FindCrossProduct(gamePoly);
            gamePoly.RenderFlag = RenderFlag.Normal;
            gamePoly.RenderFlagId = 14283;

            if (polygon.Textures.Diffuse.Bitmap!= null)
            {
                var match = project.TextureArchiveFile.TextureFiles.Where(t => t.BitmapFilePair.Bitmap == polygon.Textures.Diffuse.Bitmap).FirstOrDefault();

                if (match == null)
                {
                    var tex = new TextureFile(gameMesh.ParentFile.Parent.TextureArchiveFile, polygon.Textures.Diffuse);
                    project.TextureArchiveFile.TextureFiles.Add(tex);
                    gamePoly.TextureFile = tex;
                }
                else
                {

                    gamePoly.TextureFile = match;
                }

                var texCoords = polygon.GetIndices().Select(i => i.TexCoord).ToList();
                var texCoordSet = new TextureCoordSet(texCoords);

                // Not sure why it needs to be mirrored twice.
                texCoordSet.MirrorVertical();
                texCoordSet.MirrorVertical();


                gamePoly.PolygonType = project.PolygonTypeDictionary[0x90E0303];
            }
            else
            {
                gamePoly.PolygonType = project.PolygonTypeDictionary[0x4080100];
            }

            return gamePoly;
        }
    }
}