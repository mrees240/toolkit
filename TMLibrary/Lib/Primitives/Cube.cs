﻿using System.Numerics;

namespace TMLibrary.Lib.Primitives
{
    public class Cube
    {
        private long address;
        private Vector3 position;
        private float width, length, height;

        public Cube()
        {
            position = new Vector3();
        }

        public Cube(Vector3 position, float width, float length, float height)
        {
            this.position = position;
            this.width = width;
            this.length = length;
            this.height = height;
        }

        public Vector3 Position { get => position; set => position = value; }
        public float Width { get => width; set => width = value; }
        public float Length { get => length; set => length = value; }
        public float Height { get => height; set => height = value; }
        public long Address { get => address; set => address = value; }
    }
}
