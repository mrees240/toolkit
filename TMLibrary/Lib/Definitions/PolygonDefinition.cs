﻿namespace TMLibrary.Lib.Definitions
{
    /// <summary>
    ///     Defines a polygon that can be rendered in 3D space. Each game has different polygon definitions.
    ///     Headers are defined as a uint key in a dictionary.
    /// </summary>
    /*public class PolygonDefinition
    {
        public PolygonDefinition(int length, int indexAmount, int texCoordIndex, int textureIdIndex, int colorIndex,
            bool isSolidColor)
        {
            IgnoreDiffuseValues = true;
            Length = length;
            IndexAmount = indexAmount;
            TexCoordIndex = texCoordIndex;
            TextureIdIndex = textureIdIndex;
            ColorIndex = colorIndex;
            IsSolidColor = isSolidColor;
        }

        public int Length { get; set; }

        public int IndexAmount { get; set; }

        public bool IsSolidColor { get; set; }

        public int TexCoordIndex { get; }

        public int ColorIndex { get; }

        public int TextureIdIndex { get; }

        public bool HasMultipleTextureDiffuseValues { get; set; }

        public bool IgnoreDiffuseValues { get; set; }
    }*/
}