﻿using System.Drawing;
using TMLibrary.Textures;
using System.Linq;
using System.Drawing.Imaging;

namespace TMLibrary.Lib.Definitions
{
    /// <summary>
    ///     Defines a texture archive for different texture archive formats such as TMS or TPC.
    /// </summary>
    public class TextureArchiveDefinition
    {
        /// <summary>
        ///     If all the texture files share the same color lookup table. The first texture's CLUT is used for every other
        ///     texture.
        /// </summary>
        private bool sharesClut;

        public PixelFormat DefaultPixelFormat { get; set; }

        public string FileSignature { get; set; }

        public string FileExtension { get; set; }

        public bool RequiresPowerOfTwoDimensions { get; set; }

        public int MaxArea { get; set; }

        public bool HasMaxArea { get; set; }

        public byte[] FileHeader { get; set; }

        public bool SkipOnlyFirstTpcHeader { get; set; }

        public bool SharesClut
        {
            get => sharesClut;
            set => sharesClut = value;
        }

        public Color TransparencyColor { get; set; }

        public bool FlipRgb { get; set; }

        public long FirstTimAddress { get; set; }

        public bool AlwaysSkipExtraTimHeader { get; set; }

        public TimHeaderFormat[] TimHeaderFormats { get; set; }

        public TimHeaderFormat[] TimHeaderFormats4Bpp { get; set; }
        public TimHeaderFormat[] TimHeaderFormats8Bpp { get; set; }

        public TimHeaderFormat NonTransparentTimHeader { get; set; }
        public TimHeaderFormat TransparentTimHeader { get; set; }

        public TimHeaderFormat FindHeaderFormat(byte[] header)
        {
            foreach (var timHeader in TimHeaderFormats)
            {
                if (header.SequenceEqual(timHeader.HeaderBytes))
                {
                    return timHeader;
                }
            }
            return null;
        }

        public bool ContainsTimHeader(byte[] header)
        {
            foreach (var timHeader in TimHeaderFormats)
            {
                if (header.SequenceEqual(timHeader.HeaderBytes))
                {
                    return true;
                }
            }
            return false;
        }
    }
}