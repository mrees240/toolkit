﻿namespace TMLibrary.Lib.Definitions
{
    /// <summary>
    ///     Defines a pointer with a tag, the way the pointer is read, and the minimum and maximum address range for that
    ///     pointer type.
    /// </summary>
    public class PointerDefinition
    {
        /// <param name="headerTag">The tag that comes after the value, which determines which pointer definition it belongs to.</param>
        /// <param name="pointerOperation">
        ///     The operation that is performed when reading a pointer. Used to determine the address
        ///     pointed to.
        /// </param>
        /// <param name="minAddress">The minimum address of the pointer definition's address range.</param>
        /// <param name="maxAddress">The maximum address of the pointer definition's address range.</param>
        public PointerDefinition(ushort headerTag, long pointerOperation, long minAddress, long maxAddress)
        {
            HeaderTag = headerTag;
            PointerOperation = pointerOperation;
            MinAddress = minAddress;
            MaxAddress = maxAddress;
        }

        public ushort HeaderTag { get; }

        public long PointerOperation { get; }

        public long MinAddress { get; }

        public long MaxAddress { get; }
    }
}