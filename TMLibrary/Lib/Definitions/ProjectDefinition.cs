﻿using TMLibrary.Lib.Instructions;
using TMLibrary.Rendering;
using TMLibrary.Textures;
using System.Collections.Generic;

namespace TMLibrary.Lib.Definitions
{
    /// <summary>
    ///     Used to define a project type for an individual game.
    /// </summary>
    public class ProjectDefinition
    {
        public ProjectDefinition()
        {
        }

        public bool ProjectLoaded => MeshFile != null && TextureArchiveFile != null;

        public IFileWriter FileWriter { get; set; }

        public MeshArchiveFile MeshFile { get; set; }

        public TextureArchiveFile TextureArchiveFile { get; set; }

        public bool SkipTextures { get; set; }

        public string MeshArchiveFileName { get; set; }

        public string TextureArchiveFileName { get; set; }

        public string MeshArchiveSafeFileName { get; set; }

        public string TextureArchiveSafeFileName { get; set; }

        public IMeshWriter MeshWriter { get; set; }

        public IMeshReader MeshReader { get; set; }

        public Dictionary<uint, PolygonType> PolygonTypeDictionary { get; set; }

        public bool Saveable { get; set; }
    }
}