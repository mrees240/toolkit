﻿using System.Drawing;

namespace TMLibrary.Lib.Definitions
{
    public class PointNodeDefinition
    {
        private ushort header;
        private Color color;
        private int length;

        public PointNodeDefinition(ushort header, Color color, int length)
        {
            this.header = header;
            this.color = color;
            this.length = length;
        }

        public ushort Header { get => header; }
        public Color Color { get => color; }
        public int Length { get => length; }
    }
}
