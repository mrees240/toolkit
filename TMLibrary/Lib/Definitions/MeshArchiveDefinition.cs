﻿using TMLibrary.Lib.Instructions;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.Definitions
{
    /// <summary>
    ///     Defines a mesh archive for different mesh archive formats such as DPC.
    /// </summary>
    public class MeshArchiveDefinition
    {
        /// <summary>
        ///     The filesignature is read to identify the correct archive definition. Asterisks (*) are used as wildcards.
        /// </summary>
        private string fileSignature;

        public string FileSignature
        {
            get => fileSignature;
            set => fileSignature = value;
        }

        public PolygonController PolygonController { get; set; }

        public string FileExtension { get; set; }

        public uint MeshHeaderTag { get; set; }

        public int VertexStride { get; set; }

        public PointerDefinition[] PointerDefinitions { get; set; }

        public IInstructionParser InstructionParser { get; set; }

        /*public uint CustomPolygonTexturedHeader { get; set; }

        public uint CustomPolygonColorHeader { get; set; }

        public uint CustomQuadTexturedHeader { get; set; }

        public uint CustomQuadColorHeader { get; set; }*/
    }
}