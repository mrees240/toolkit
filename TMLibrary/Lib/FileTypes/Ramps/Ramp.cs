﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace TMLibrary.Lib.FileTypes.Ramps
{
    public class Ramp
    {
        private uint header;
        private RampType rampType;
        private Vector3[] vertices;
        private Vector3 basePoint;
        private Vector3 cornerPoint;
        private Vector3 tipPoint;
        private Vector3 anchorPoint;
        private double zRadian;
        private double width, depth;
        private int ptAmount;

        private float[] heights;

        public bool IsTestSlope;

        public double ZRadian { get => zRadian; }
        public Vector3 BasePoint { get => basePoint; }
        public Vector3 CornerPoint { get => cornerPoint; }
        public Vector3 TipPoint { get => tipPoint; }
        public Vector3 AnchorPoint { get => anchorPoint; }
        public RampType RampType { get => rampType; set => rampType = value; }

        public Ramp()
        {
            vertices = new Vector3[3];
            rampType = RampType.Default;
            ptAmount = 3;
        }

        public Ramp(Vector3 v0, Vector3 v1, Vector3 v2) : this()
        {
            vertices = new Vector3[3];
            vertices[0] = v0;
            vertices[1] = v1;
            vertices[2] = v2;
        }

        // (header, width, depth, xPos, yPos, cos, sin, height0, height1, height2, height3, pointAmount);
        public Ramp(uint header, double width, double depth, double xPos, double yPos, double cos, double sin,
            double height0, double height1, double height2, double height3, int pointAmount)
        {
            heights = new float[]
            {
                (float)height0, (float)height1, (float)height2, (float)height3
            };
            this.ptAmount = pointAmount;
            this.header = header;
            this.width = width;
            this.depth = depth;
            basePoint = new Vector3((float)(xPos), (float)(yPos), 0);
            cornerPoint = new Vector3((float)(xPos), (float)(yPos + depth), 0);
            tipPoint = new Vector3((float)(xPos + width), (float)(yPos + depth), 0);
            anchorPoint = FindAnchorPoint();

            zRadian = Math.Atan2(sin, cos);

            while (zRadian < 0)
            {
                zRadian += Math.PI * 2.0;
            }
        }

        public int VertexCount
        {
            get
            {
                return vertices.Count();
            }
        }

        public uint Header { get => header; set => header = value; }
        public int PtAmount { get => ptAmount; set => ptAmount = value; }
        public float[] Heights { get => heights; set => heights = value; }
        public Vector3[] Vertices { get => vertices; }

        /*public Slope(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3) : this()
        {
            vertices = new Vector3[4];
            vertices[0] = v0;
            vertices[1] = v1;
            vertices[2] = v2;
            vertices[3] = v3;
        }*/

        public void UpdateAnchorPoint()
        {
            anchorPoint = FindAnchorPoint();
        }

        /// <summary>
        /// The third height of a triangle slope acts as an anchor for the surface. It could be used to create a triangle with a curved surface.
        /// </summary>
        /// <returns></returns>
        private Vector3 FindAnchorPoint()
        {
            var side = Vector3.Subtract(tipPoint, cornerPoint);
            var anchorSide = Vector3.Add(side, basePoint);

            return anchorSide;
        }

        private void Transform(Matrix4x4 matrix)
        {
            for (int iVert = 0; iVert < vertices.Count(); iVert++)
            {
                vertices[iVert] = Vector3.Transform(vertices[iVert], matrix);
            }
            cornerPoint = Vector3.Transform(cornerPoint, matrix);
            basePoint = Vector3.Transform(basePoint, matrix);
            tipPoint = Vector3.Transform(tipPoint, matrix);
            UpdateAnchorPoint();
        }

        private bool FloatCompare(float val0, float val1, float threshold)
        {
            return Math.Abs(val0 - val1) < threshold;
        }

        private Ramp[] SliceXAxisYAxisAligned()
        {
            var maxX = vertices.OrderBy(v => v.X).Last();
            var maxY = vertices.OrderBy(v => v.Y).Last();
            var minY = vertices.OrderBy(v => v.Y).First();
            var minX = vertices.OrderBy(v => v.X).First();

            float threshold = 0.2f;
            var matchingMinYVerts = vertices.Where(v => FloatCompare(v.Y, minY.Y, threshold));
            var matchingMaxYVerts = vertices.Where(v => FloatCompare(v.Y, maxY.Y, threshold));
            var matchingMinXVerts = vertices.Where(v => FloatCompare(v.X, minX.X, threshold));
            var matchingMaxXVerts = vertices.Where(v => FloatCompare(v.X, maxX.X, threshold));

            bool cornerYGtTipY = cornerPoint.Y > tipPoint.Y;
            bool cornerXGtTipX = cornerPoint.X > tipPoint.X;

            var vertsSortedByX = vertices.OrderBy(v => v.X).ToArray();
            var vertsSortedByY = vertices.OrderBy(v => v.Y).ToArray();

            zRadian = 0.0;

            bool cornerIsMinOrMaxY = cornerPoint == maxY || cornerPoint == minY;

            if (matchingMinYVerts.Count() == 2)
            {
                if (matchingMinXVerts.Count() == 1)
                {
                    rampType = RampType.M;
                    zRadian = Math.PI / 2.0;
                    basePoint = vertsSortedByY.Last();
                    tipPoint = vertsSortedByX.First();
                    cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                    depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                    width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                }
                else if (matchingMinXVerts.Count() == 2)
                {
                    rampType = RampType.N;
                    basePoint = vertsSortedByX.Last();
                    tipPoint = vertsSortedByY.Last();
                    cornerPoint = vertices.Where(v => v != tipPoint && v != basePoint).First();
                    zRadian = 0;
                    depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                    width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                    var side = Vector3.Subtract(tipPoint, cornerPoint);
                    var testPt = Vector3.Add(side, basePoint);
                }
            }
            else if (matchingMaxYVerts.Count() == 2)
            {
                if (matchingMinXVerts.Count() == 1)
                {
                    rampType = RampType.P;
                    zRadian = Math.PI;
                    tipPoint = vertsSortedByY.First();
                    basePoint = vertsSortedByX.First();
                    cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                    depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                    width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                }
                else if (matchingMinXVerts.Count() == 2)
                {
                    rampType = RampType.O;
                    tipPoint = vertsSortedByX.Last();
                    basePoint = vertsSortedByY.First();
                    cornerPoint = vertices.Where(v => v != tipPoint && v != basePoint).First();
                    zRadian = Math.PI * 3.0 / 2.0;
                    depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                    width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                }
            }
            UpdateAnchorPoint();
            return new Ramp[] { this };
        }

        public Ramp[] Slice()
        {
            if (vertices.Count() != 3 && vertices.Count() != 4)
            {
                return new Ramp[0];
            }
            float len0 = Length0();
            float len1 = Length1();
            float len2 = Length2();

            Vector3 hypoFirst = new Vector3();
            Vector3 hypoLast = new Vector3();

            double slope0 = 0.0;
            var vertsSortedByX = vertices.OrderBy(v => v.X);
            var vertsSortedByY = vertices.OrderBy(v => v.Y);
            bool xAxisAligned = vertices[0].X == vertices[1].X || vertices[1].X == vertices[2].X || vertices[0].X == vertices[2].X;
            bool yAxisAligned = vertices[0].Y == vertices[1].Y || vertices[1].Y == vertices[2].Y || vertices[0].Y == vertices[2].Y;

            if (len0 > len1 && len0 > len2)
            {
                tipPoint = vertices[2];
                hypoFirst = vertices[1];
                hypoLast = vertices[0];

            }
            else if (len1 > len0 && len1 > len2)
            {
                tipPoint = vertices[0];
                hypoFirst = vertices[1];
                hypoLast = vertices[2];
            }
            else if (len2 > len0 && len2 > len1)
            {
                tipPoint = vertices[1];
                hypoFirst = vertices[0];
                hypoLast = vertices[2];
            }
            else
            {
                // Lengths same size
                tipPoint = vertices[2];
                hypoFirst = vertices[0];
                hypoLast = vertices[1];
            }

            if (hypoLast.X - hypoFirst.X != 0)
            {
                slope0 = (hypoLast.Y - hypoFirst.Y) / (hypoLast.X - hypoFirst.X);
            }
            double slopePerp = 0.0;
            if (slope0 != 0)
            {
                slopePerp = -1.0 / slope0;
                var tipPt2 = new Vector3(tipPoint.X - 1.0f, (float)(tipPoint.Y - slopePerp), 0);
                tipPt2.Z = (float)FindHeight(tipPt2.X, tipPt2.Y);
                cornerPoint = FindIntersection(hypoFirst, hypoLast, tipPoint, tipPt2);
            }
            else
            {
                bool verticalHypotenuse = hypoFirst.X == hypoLast.X;
                bool horizontalHypotenuse = hypoFirst.Y == hypoLast.Y;
                var cornerPt = vertices.Where(v => v != hypoFirst && v != hypoLast).FirstOrDefault();
                if (verticalHypotenuse)
                {
                    cornerPoint = new Vector3(hypoFirst.X, tipPoint.Y, (float)FindHeight(hypoFirst.X, tipPoint.Y));
                }
                else if (horizontalHypotenuse)
                {
                    //corner = new Vector3(hypoFirst.X + (float)width / 2.0f, hypoFirst.Y);
                    cornerPoint = new Vector3(vertsSortedByX.ElementAt(1).X, hypoFirst.Y, (float)FindHeight(vertsSortedByX.ElementAt(1).X, hypoFirst.Y));
                }
                if (double.IsNaN(cornerPoint.Z))
                {
                    cornerPoint.Z = hypoFirst.Z;
                }
            }

            if (xAxisAligned && yAxisAligned)
            {
                var slicedTriangles = SliceXAxisYAxisAligned();

                return slicedTriangles;
            }
            else
            {
                var t0 = new Ramp();
                var t1 = new Ramp();
                t0 = new Ramp(hypoFirst, cornerPoint, tipPoint);
                t0.tipPoint = tipPoint;
                t0.cornerPoint = cornerPoint;
                t0.basePoint = hypoFirst;
                t0.zRadian = FindAngle(new Vector3(t0.basePoint.X + 100, t0.basePoint.Y, 0), t0.basePoint, cornerPoint);
                t1 = new Ramp(tipPoint, cornerPoint, hypoLast);
                t1.tipPoint = hypoLast;
                t1.cornerPoint = cornerPoint;
                t1.basePoint = tipPoint;

                List<Ramp> slopes = new List<Ramp>();

                t0.ApplyClassOperations();
                t1.ApplyClassOperations();

                if (t0.width > 0 && t0.depth > 0)
                {
                    slopes.Add(t0);
                }
                if (t1.width > 0 && t1.depth > 0)
                {
                    slopes.Add(t1);
                }

                t0.UpdateAnchorPoint();
                t1.UpdateAnchorPoint();

                return slopes.ToArray();
            }
        }

        public double GetHeight()
        {
            return Math.Abs(vertices.Max(v => v.Z) - vertices.Min(v => v.Z));
        }

        private Vector3 SwapYZ(Vector3 vec)
        {
            return new Vector3(vec.X, vec.Z, vec.Y);
        }

        public double FindHeight(float x, float y)
        {
            var p1 = SwapYZ(vertices[0]);
            var p2 = SwapYZ(vertices[1]);
            var p3 = SwapYZ(vertices[2]);
            var pos = new Vector2(x, y);

            float det = (p2.Z - p3.Z) * (p1.X - p3.X) + (p3.X - p2.X) * (p1.Z - p3.Z);
            float L1 = ((p2.Z - p3.Z) * (pos.X - p3.X) + (p3.X - p2.X) * (pos.Y - p3.Z)) / det;
            float L2 = ((p3.Z - p1.Z) * (pos.X - p3.X) + (p1.X - p3.X) * (pos.Y - p3.Z)) / det;
            float L3 = 1.0f - L1 - L2;
            return L1 * p1.Y + L2 * p2.Y + L3 * p3.Y;
        }

        public bool IsRightTriangle()
        {
            double angle = FindAngle(basePoint, cornerPoint, tipPoint);
            return Math.Abs(angle - 0.78) < 0.01;
        }

        public Vector2 Vec2(Vector3 vector)
        {
            return new Vector2(vector.X, vector.Y);
        }

        public void ApplyClassOperations()
        {
            bool cornerYGtTipY = cornerPoint.Y > tipPoint.Y;
            bool cornerXGtTipX = cornerPoint.X > tipPoint.X;

            var vertsSortedByX = vertices.OrderBy(v => v.X).ToArray();
            var vertsSortedByY = vertices.OrderBy(v => v.Y).ToArray();
            var maxX = vertices.OrderBy(v => v.X).Last();
            var maxY = vertices.OrderBy(v => v.Y).Last();
            var minY = vertices.OrderBy(v => v.Y).First();
            var minX = vertices.OrderBy(v => v.X).First();

            zRadian = 0.0;

            bool cornerIsMinOrMaxY = cornerPoint == maxY || cornerPoint == minY;

            bool xAxisAligned = vertices[0].X == vertices[1].X || vertices[1].X == vertices[2].X || vertices[0].X == vertices[2].X;
            bool yAxisAligned = vertices[0].Y == vertices[1].Y || vertices[1].Y == vertices[2].Y || vertices[0].Y == vertices[2].Y;

            bool axisAlignedTriangle = xAxisAligned || yAxisAligned;


            if (xAxisAligned && yAxisAligned)
            {
                var newTri = SliceXAxisYAxisAligned().First();
                this.rampType = newTri.RampType;
                this.basePoint = newTri.basePoint;
                this.cornerPoint = newTri.cornerPoint;
                this.tipPoint = newTri.tipPoint;
                this.zRadian = newTri.zRadian;
                this.width = newTri.GetWidth();
                this.depth = newTri.GetDepth();
            }
            else
            {
                if (cornerIsMinOrMaxY) // ADEH
                {
                    if (cornerYGtTipY) // AD
                    {
                        if (cornerXGtTipX) // A
                        {
                            rampType = RampType.A;
                            basePoint = vertsSortedByX.ElementAt(0);
                            tipPoint = vertsSortedByX.ElementAt(2);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(basePoint, cornerPoint, new Vector3(cornerPoint.X - 100, cornerPoint.Y, 0)) + Math.PI;
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else // D
                        {
                            rampType = RampType.D;
                            basePoint = vertsSortedByX.ElementAt(0);
                            tipPoint = vertsSortedByX.ElementAt(2);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(basePoint, cornerPoint, new Vector3(cornerPoint.X - 100, cornerPoint.Y, 0)) + Math.PI;
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                    }
                    else // EH
                    {
                        if (cornerXGtTipX) // H
                        {
                            rampType = RampType.H;
                            basePoint = vertsSortedByX.Last();
                            tipPoint = vertsSortedByX.First();
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(new Vector3(cornerPoint.X + 100, cornerPoint.Y, 0), cornerPoint, basePoint);
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else if (cornerPoint.X < tipPoint.X) // E
                        {
                            rampType = RampType.E;
                            basePoint = vertsSortedByX.ElementAt(2);
                            tipPoint = vertsSortedByX.ElementAt(0);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(new Vector3(basePoint.X - 100, basePoint.Y, 0), basePoint, cornerPoint);
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else // S
                        {
                            rampType = RampType.S;
                        }
                    }
                }
                else // BCGF
                {
                    if (cornerYGtTipY) // BC
                    {
                        if (cornerXGtTipX) // B
                        {
                            rampType = RampType.B;
                            basePoint = vertsSortedByY.ElementAt(2);
                            tipPoint = vertsSortedByY.ElementAt(0);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = Math.PI - FindAngle(basePoint, cornerPoint, new Vector3(cornerPoint.X - 100, cornerPoint.Y, 0));
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else if (cornerPoint.X < tipPoint.X) // C
                        {
                            rampType = RampType.C;
                            tipPoint = vertsSortedByY.ElementAt(2);
                            basePoint = vertsSortedByY.ElementAt(0);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(basePoint, cornerPoint, new Vector3(cornerPoint.X - 100, cornerPoint.Y, 0)) + Math.PI;
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else
                        {
                            rampType = RampType.R;
                        }
                    }
                    else // GF
                    {
                        if (cornerXGtTipX) // G
                        {
                            rampType = RampType.G;
                            basePoint = vertsSortedByY.ElementAt(2);
                            tipPoint = vertsSortedByY.ElementAt(0);
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(basePoint, cornerPoint, new Vector3(cornerPoint.X + 100, cornerPoint.Y, 0));
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else if (cornerPoint.X < tipPoint.X) // F
                        {
                            rampType = RampType.F;
                            basePoint = vertsSortedByY.First();
                            tipPoint = vertsSortedByY.Last();
                            cornerPoint = vertices.Where(v => v != basePoint && v != tipPoint).First();
                            zRadian = FindAngle(cornerPoint, basePoint, new Vector3(basePoint.X + 100, basePoint.Y, 0)) + Math.PI;
                            depth = Vector2.Distance(new Vector2(basePoint.X, basePoint.Y), new Vector2(cornerPoint.X, cornerPoint.Y));
                            width = Vector2.Distance(new Vector2(cornerPoint.X, cornerPoint.Y), new Vector2(tipPoint.X, tipPoint.Y));
                        }
                        else
                        {
                            rampType = RampType.Q;
                        }
                    }
                }
            }
            UpdateAnchorPoint();
        }

        public double GetDepth()
        {
            return depth;
        }

        public double GetWidth()
        {
            return width;
        }

        public double RotationDegrees()
        {
            return 180.0 / Math.PI * zRadian;
        }

        public double GetSin()
        {
            return Math.Sin(zRadian);
        }

        public double GetCos()
        {
            return Math.Cos(zRadian);
        }

        public double GetXPos()
        {
            return basePoint.X;
        }

        public double GetYPos()
        {
            return basePoint.Y;
        }

        public double FindSlope(Vector3 v0, Vector3 v1)
        {
            double run = v1.X - v0.X;
            double rise = v1.Y - v0.Y;

            double slope = 0.0;

            if (rise != 0)
            {
                slope = rise / run;
            }

            return slope;
        }

        public void Scale(float scale)
        {
            vertices[0].X *= scale;
            vertices[1].X *= scale;
            vertices[2].X *= scale;
            vertices[0].Y *= scale;
            vertices[1].Y *= scale;
            vertices[2].Y *= scale;
        }

        public double FindYIntercept(Vector3 v0, Vector3 v1)
        {
            double slope0 = FindSlope(v0, v1);

            // y = mx + b
            double yInt = v0.Y - slope0 * v0.X;

            return yInt;
        }

        public Vector3 FindIntersection(Vector3 v00, Vector3 v01, Vector3 v10, Vector3 v11)
        {
            double slope0 = FindSlope(v00, v01);
            double slope1 = FindSlope(v10, v11);
            double yInt0 = FindYIntercept(v00, v01);
            double yInt1 = FindYIntercept(v10, v11);

            double slope = slope1 - slope0;

            double x = (yInt0 - yInt1);
            if (slope != 0)
            {
                x /= slope;
            }
            double y = slope0 * x + yInt0;

            return new Vector3((float)x, (float)y, (float)FindHeight((float)x, (float)y));
        }

        public double FindAngle(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            var a = Math.Pow(v1.X - v0.X, 2) + Math.Pow(v1.Y - v0.Y, 2);
            var b = Math.Pow(v1.X - v2.X, 2) + Math.Pow(v1.Y - v2.Y, 2);
            var c = Math.Pow(v2.X - v0.X, 2) + Math.Pow(v2.Y - v0.Y, 2);

            return Math.Acos((a + b - c) / Math.Sqrt(4 * a * b));
        }

        public float Length0()
        {
            return Vector2.Distance(new Vector2(vertices[0].X, vertices[0].Y), new Vector2(vertices[1].X, vertices[1].Y));
        }

        public float Length1()
        {
            return Vector2.Distance(new Vector2(vertices[1].X, vertices[1].Y), new Vector2(vertices[2].X, vertices[2].Y));
        }

        public float Length2()
        {
            return Vector2.Distance(new Vector2(vertices[0].X, vertices[0].Y), new Vector2(vertices[2].X, vertices[2].Y));
        }
    }
}