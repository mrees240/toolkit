﻿using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace TMLibrary.Lib.FileTypes.Ramps
{
    public class RampsFile
    {
        private List<Ramp> ramps;

        public RampsFile()
        {
            ramps = new List<Ramp>();
        }

        public Mesh CreateRampMesh()
        {
            var rampMesh = new Mesh();

            foreach (var ramp in ramps)
            {
                var vertices = new Vector3[]
                {
                    new Vector3(0, 0, ramp.Heights[0]),
                    new Vector3(0, -1, ramp.Heights[1]),
                    new Vector3(1, -1, ramp.Heights[2]),
                    new Vector3(1, 0, ramp.Heights[3]),
                };

                vertices[0].Z = ramp.Heights[0];
                vertices[1].Z = ramp.Heights[1];
                vertices[3].Z = ramp.Heights[2];
                vertices[2].Z = ramp.Heights[3];

                var v0 = new Vertex(vertices[0]);
                var v1 = new Vertex(vertices[1]);
                var v2 = new Vertex(vertices[2]);
                var v3 = new Vertex(vertices[3]);

                var index0 = new PolygonIndex(rampMesh.Vertices.Count + 0, System.Drawing.Color.Red);
                var index1 = new PolygonIndex(rampMesh.Vertices.Count + 1, System.Drawing.Color.Green);
                var index2 = new PolygonIndex(rampMesh.Vertices.Count + 2, System.Drawing.Color.Blue);
                var index3 = new PolygonIndex(rampMesh.Vertices.Count + 3, System.Drawing.Color.Purple);

                var poly = new Polygon(index0, index1, index3);
                var poly2 = new Polygon(index3, index1, index2);
                var polyTri = new Polygon(index1, index2, index0);
                v0.DiffuseColor = index0.DiffuseColor;
                v1.DiffuseColor = index1.DiffuseColor;
                v2.DiffuseColor = index2.DiffuseColor;
                v3.DiffuseColor = index3.DiffuseColor;

                var translation = new Vector3((float)ramp.GetXPos(), (float)ramp.GetYPos(), 0);
                var scale = new Vector3((float)ramp.GetDepth(), (float)ramp.GetWidth(), 1.0f);
                var translationMatrix = Matrix4x4.CreateTranslation(translation);
                var scaleMatrix = Matrix4x4.CreateScale(scale);

                double sin = ramp.GetSin();
                double cos = ramp.GetCos();

                double rad = Math.Atan2(sin, cos);

                var currentRampVertices = new Vertex[]
                {
                        v0, v1, v2, v3
                };

                var rotMatrix = Matrix4x4.CreateRotationZ((float)rad);
                var matrix = Matrix4x4.Multiply(scaleMatrix, rotMatrix);
                matrix = Matrix4x4.Multiply(matrix, translationMatrix);

                for (int iVert = 0; iVert < currentRampVertices.Length; iVert++)
                {
                    currentRampVertices[iVert].Vector = Vector3.Transform(currentRampVertices[iVert].Vector, matrix);
                }

                rampMesh.Vertices.Add(v0);
                rampMesh.Vertices.Add(v1);
                rampMesh.Vertices.Add(v2);
                rampMesh.Vertices.Add(v3);
                if (ramp.PtAmount == 4)
                {
                    rampMesh.AddPolygon(poly);
                    rampMesh.AddPolygon(poly2);
                }
                else if (ramp.PtAmount == 3)
                {
                    rampMesh.AddPolygon(polyTri);
                }
            }

            return rampMesh;
        }



        public List<Ramp> Ramps { get => ramps; }
    }
}
