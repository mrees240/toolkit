﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Threading;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.FileTypes.Ramps
{
    public class RampsFileController
    {
        private RampsFile rampsFile;
        private CancellationTokenSource cancelToken;
        private ProgressManager progress;

        public ProgressManager Progress { get => progress; }

        public RampsFileController(RampsFile rampsFile, CancellationTokenSource cancelToken)
        {
            progress = new ProgressManager();
            this.cancelToken = cancelToken;
            this.rampsFile = rampsFile;
        }

        public void ReadRamps(string fileUrl)
        {
            byte[] rampBuffer = File.ReadAllBytes(fileUrl);

            using (var reader = new BinaryReader(new MemoryStream(rampBuffer)))
            {
                ReadRamps(reader);
            }
        }

        public void WriteRampsFile(string fileUrl)
        {
            using (var writer = new BinaryWriter(new FileStream(fileUrl, FileMode.Create)))
            {
                WriteRamps(writer);
            }
        }

        public void ReadRamps(BinaryReader reader)
        {
            rampsFile.Ramps.Clear();

            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    break;
                }
                rampsFile.Ramps.Add(ReadRamp(reader));
            }
        }

        public void WriteRamps(BinaryWriter writer)
        {
            progress.FileWriteProgress = 0;

            int limit = rampsFile.Ramps.Count;
            if (rampsFile.Ramps.Count == 0)
            {
                var emptyRamp = new Ramp();
                WriteRamp(emptyRamp, writer);
            }
            else
            {
                for (int iRamp = 0; iRamp < limit; iRamp++)
                {
                    if (cancelToken.IsCancellationRequested)
                    {
                        break;
                    }
                    var ramp = rampsFile.Ramps[iRamp];

                    WriteRamp(ramp, writer);
                    progress.FileWriteProgress = ((double)iRamp / (double)limit);
                }
            }


            /*int defaultFileSizeBytes = 28000;
            if (writer.BaseStream.Position < defaultFileSizeBytes)
            {
                writer.BaseStream.Position = defaultFileSizeBytes - 1;
                writer.Write((byte)0);
            }*/
        }

        private void WriteRamp(Ramp ramp, BinaryWriter writer)
        {
            if (ramp.VertexCount != 3)
            {
                throw new Exception($"The polygon has {ramp.VertexCount} vertices but it needs to have three. Try triangulating the mesh.");
            }

            double zRad = ramp.ZRadian;

            short cos = (short)(Math.Cos(zRad) * 4096.0);
            short sin = (short)(Math.Sin(zRad) * 4096.0);
            double width = ramp.GetWidth();
            double depth = ramp.GetDepth();
            int xPos = (int)(ramp.TipPoint.X);
            int yPos = (int)(ramp.TipPoint.Y);

            float zOffset = 0;

            writer.Write(0xFFFF0101);
            writer.Write((ushort)width);
            writer.Write((ushort)depth);
            writer.Write(cos);
            writer.Write(sin);
            writer.Write((short)(Math.Ceiling(ramp.TipPoint.Z + zOffset)));
            writer.Write((short)((Math.Ceiling(ramp.CornerPoint.Z + zOffset))));
            writer.Write((short)((Math.Ceiling(ramp.AnchorPoint.Z + zOffset))));
            writer.Write((short)((Math.Ceiling(ramp.BasePoint.Z + zOffset))));
            writer.Write(xPos);
            writer.Write(yPos);
        }

        public List<Ramp> CreateRampsFromMesh(Mesh meshOriginal, Matrix4x4 worldMatrix)
        {
            var mesh = new Mesh(meshOriginal);
            mesh.Transform(worldMatrix);

            var curRamps = new List<Ramp>();
            var finalRamps = new List<Ramp>();
            var polys = mesh.GetAllPolygons();

            foreach (var poly in polys)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    break;
                }
                //if (poly.HasThreeIndices())
                {
                    var vert0 = mesh.Vertices[poly.GetPolygonIndex(0).Id];
                    var vert1 = mesh.Vertices[poly.GetPolygonIndex(1).Id];
                    var vert2 = mesh.Vertices[poly.GetPolygonIndex(2).Id];

                    var cp = Utility.FindCrossProduct(vert0.Vector, vert1.Vector, vert2.Vector);

                    var flatV0 = vert0.Vector;
                    var flatV1 = vert1.Vector;
                    var flatV2 = vert2.Vector;
                    flatV0.Z = 0.0f;
                    flatV1.Z = 0.0f;
                    flatV2.Z = 0.0f;

                    float d0 = Vector3.Distance(flatV0, flatV1);
                    float d1 = Vector3.Distance(flatV0, flatV2);
                    float d2 = Vector3.Distance(flatV1, flatV2);

                    bool valid = true;
                    float minDist = 25.0f;
                    float scalar = 1.1f;
                    if (d0 < minDist || d1 < minDist || d2 < minDist)
                    {
                        valid = false;
                    }

                    if (valid)
                    {
                        var triangle2 = new Ramp(
                            new Vector3(vert0.Vector.X * scalar, vert0.Vector.Y * scalar, vert0.Vector.Z),
                            new Vector3(vert1.Vector.X * scalar, vert1.Vector.Y * scalar, vert1.Vector.Z),
                            new Vector3(vert2.Vector.X * scalar, vert2.Vector.Y * scalar, vert2.Vector.Z));

                        curRamps.Add(triangle2);
                    }
                }
            }

            //this.rampsFile.Ramps.Clear();
            //var slicedTriangles = new List<Ramp>();

            foreach (var triangle in curRamps)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    break;
                }
                var dist0 = Vector3.Distance(triangle.Vertices[0], triangle.Vertices[1]);
                var height0 = Math.Abs(triangle.Vertices[0].Z - triangle.Vertices[1].Z);
                var dist1 = Vector3.Distance(triangle.Vertices[0], triangle.Vertices[2]);
                var height1 = Math.Abs(triangle.Vertices[0].Z - triangle.Vertices[2].Z);
                var dist2 = Vector3.Distance(triangle.Vertices[1], triangle.Vertices[2]);
                var height2 = Math.Abs(triangle.Vertices[1].Z - triangle.Vertices[2].Z);

                double maxRatio = 0.8;

                /*if (dist0 == 0 || height0 / dist0 > maxRatio)
                {
                    return;
                }
                if (dist1 == 0 || height1 / dist1 > maxRatio)
                {
                    return;
                }
                if (dist2 == 0 || height2 / dist2 > maxRatio)
                {
                    return;
                }*/

                //if (validRamp)
                {
                    var currSlicedTriangles = triangle.Slice();

                    foreach (var slice in currSlicedTriangles)
                    {
                        //if (slice.GetDepth() > 25 && slice.GetWidth() > 25)
                        {
                            // ratio
                            //rampsFile.Ramps.Add(slice);
                            finalRamps.Add(slice);
                        }
                    }
                }
            }

            //WriteRampsFile(fileName);
            return finalRamps;
        }

        /*private void WriteRamp(Ramp ramp, BinaryWriter writer)
        {
            double zRad = ramp.ZRadian;

            short cos = (short)(Math.Cos(zRad) * 4096.0);
            short sin = (short)(Math.Sin(zRad) * 4096.0);
            double width = ramp.GetWidth();
            double depth = ramp.GetDepth();
            int xPos = (int)(ramp.TipPoint.X);
            int yPos = (int)(ramp.TipPoint.Y);

            writer.Write(0xFFFF0101);
            writer.Write((ushort)width);
            writer.Write((ushort)depth);
            writer.Write(cos);
            writer.Write(sin);
            writer.Write((short)(Math.Ceiling(ramp.TipPoint.Z)));
            writer.Write((short)((Math.Ceiling(ramp.CornerPoint.Z))));
            writer.Write((short)((Math.Ceiling(ramp.AnchorPoint.Z))));
            writer.Write((short)((Math.Ceiling(ramp.BasePoint.Z))));
            writer.Write(xPos);
            writer.Write(yPos);
        }*/

        private Ramp ReadRamp(BinaryReader reader)
        {
            uint header = reader.ReadUInt32();
            ushort width = reader.ReadUInt16();
            ushort depth = reader.ReadUInt16();
            double cos = reader.ReadInt16() / 4096.0;
            double sin = reader.ReadInt16() / 4096.0;
            short height0 = reader.ReadInt16();
            short height1 = reader.ReadInt16();
            short height2 = reader.ReadInt16();
            short height3 = reader.ReadInt16();
            int xPos = reader.ReadInt32();
            int yPos = reader.ReadInt32();

            int pointAmount = 4;
            //if (header == 0xFFFF0101 || header == 0xFFFF0001)
            if ((header & 0xFF) == 1)
            {
                pointAmount = 3;
            }

            var ramp = new Ramp(header, width, depth, xPos, yPos, cos, sin, height0, height1, height2, height3, pointAmount);
            return ramp;
        }
    }
}