﻿using System.Collections.Generic;

namespace TMLibrary.Lib.FileTypes.Point
{
    public class PointsFile
    {
        public static readonly ushort PointHeader0 = 0x1802;
        public static readonly ushort PointHeader1 = 0x604;
        public static readonly int ConnectionLimit = 4;
        private List<PointNode> points;

        public PointsFile()
        {
            points = new List<PointNode>();
        }

        public List<PointNode> Points { get => points; }
    }
}
