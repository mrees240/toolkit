﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TMLibrary.Lib.FileTypes.Point
{
    public class PointsFileController
    {
        private PointsFile pointsFile;
        private List<PointNode> finalPoints = new List<PointNode>();

        public PointsFileController(PointsFile pointsFile)
        {
            this.pointsFile = pointsFile;
        }

        public void ReadPoints(string fileUrl)
        {
            finalPoints.Clear();
            byte[] fileBuffer = File.ReadAllBytes(fileUrl);

            using (var reader = new BinaryReader(new MemoryStream(fileBuffer)))
            {
                ReadPoints(reader);
            }
        }

        public void WritePointsFile(string fileUrl)
        {
            int minPtsPerFile = 3360 / 28;
            finalPoints.Clear();

            finalPoints.AddRange(pointsFile.Points);

            while (finalPoints.Count() < minPtsPerFile)
            {
                foreach (var pt in pointsFile.Points)
                {
                    finalPoints.Add(pt);

                    if (finalPoints.Count() >= minPtsPerFile)
                    {
                        break;
                    }
                }
            }

            using (var writer = new BinaryWriter(new FileStream(fileUrl, FileMode.Create)))
            {
                WritePoints(writer, finalPoints);
            }
        }

        public void LogPointsFile()
        {
            var sb = new StringBuilder();

            foreach (var pt in pointsFile.Points)
            {
                sb.AppendLine($"ID: {pt.Id}");

                int connectionAmount = pt.GetConnectedNodeIdAmount();

                for (int iConnection = 0; iConnection < connectionAmount; iConnection++)
                {
                    var connectionId = pt.GetConnectedNodeId(iConnection);
                    sb.AppendLine($"Connection ID: {connectionId}");
                }
                sb.AppendLine();
            }
            File.WriteAllText("PointsLog.txt", sb.ToString());
        }

        public void WritePoints(BinaryWriter writer, List<PointNode> aiPoints)
        {
            int defaultFileSize = 3360;
            bool isFirstPoint = true;

            while (writer.BaseStream.Position < defaultFileSize)
            {
                foreach (var pt in aiPoints)
                {
                    WritePoint(writer, pt, aiPoints, isFirstPoint);
                    isFirstPoint = false;
                }
            }
        }

        private void WritePoint(BinaryWriter writer, PointNode spawnPoint, List<PointNode> allPoints, bool isFirstPoint)
        {
            int zOffset = 0;
            if (!isFirstPoint)
            {
                writer.Write((ushort)spawnPoint.Header);
            }
            writer.Write((int)spawnPoint.Coordinate.X);
            writer.Write((int)spawnPoint.Coordinate.Y);
            writer.Write((short)(spawnPoint.Coordinate.Z + zOffset));
            ushort paddingId = 0;

            if (spawnPoint.HasAtLeaseOneConnection())
            {
                paddingId = (ushort)(spawnPoint.GetConnectedNodeId(0));
            }
            for (int i = 0; i < PointsFile.ConnectionLimit; i++)
            {
                if (i < spawnPoint.GetConnectedNodeIdAmount())
                {
                    ushort id = (ushort)(spawnPoint.GetConnectedNodeId(i));
                    if (id < allPoints.Count())
                    {
                        writer.Write(id);
                    }
                    else
                    {
                        writer.Write(paddingId);
                    }
                }
                else
                {
                    writer.Write(paddingId);
                }
            }
            writer.Write(spawnPoint.Parameter0);
            writer.Write(spawnPoint.Parameter1);
            writer.Write(spawnPoint.Parameter2);
            writer.Write(spawnPoint.Parameter3);
        }

        public void ReadPoints(BinaryReader reader)
        {
            pointsFile.Points.Clear();
            reader.BaseStream.Position = 0;

            while (reader.BaseStream.Position <= reader.BaseStream.Length - 24)
            {
                var color = System.Drawing.Color.Red;
                long address = reader.BaseStream.Position;
                ushort next = 0;
                if (reader.BaseStream.Position > 0)
                {
                    next = reader.ReadUInt16();
                }

                if (next == 0 && reader.BaseStream.Position > 0)
                {
                    break;
                }

                int xPos = reader.ReadInt32();
                int yPos = reader.ReadInt32();
                int zPos = reader.ReadInt16();

                short[] nodeIds = new short[PointsFile.ConnectionLimit];
                for (int i = 0; i < nodeIds.Length; i++)
                {
                    short id = reader.ReadInt16();
                    nodeIds[i] = id;
                }
                byte parameter0 = reader.ReadByte();
                byte parameter1 = reader.ReadByte();
                byte parameter2 = reader.ReadByte();
                byte parameter3 = reader.ReadByte();
                var pt = new PointNode(next, new System.Numerics.Vector3(xPos, yPos, zPos), color, address, pointsFile.Points.Count,
                    parameter0, parameter1, parameter2, parameter3);

                foreach (var id in nodeIds)
                {
                    pt.AddChildNodeId(id);
                }

                pointsFile.Points.Add(pt);
            }
            foreach (var node in pointsFile.Points)
            {
                for (int iNode = 0; iNode < node.GetConnectedNodeIdAmount(); iNode++)
                {
                    var connectedNodeId = node.GetConnectedNodeId(iNode);
                    if (connectedNodeId < pointsFile.Points.Count())
                    {
                        if (connectedNodeId >= 0)
                        {
                            node.AddConnectedNode(pointsFile.Points[connectedNodeId]);
                        }
                    }
                }
            }
        }
    }
}
