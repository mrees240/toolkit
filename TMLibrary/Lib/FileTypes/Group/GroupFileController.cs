﻿using System.IO;
using System.Numerics;
using TMLibrary.Lib.Primitives;

namespace TMLibrary.Lib.FileTypes.Group
{
    public class GroupFileController
    {
        private GroupFile groupFile;

        public GroupFileController(GroupFile groupFile)
        {
            this.groupFile = groupFile;
        }

        public void ReadFile(string fileUrl)
        {
            byte[] fileBuffer = File.ReadAllBytes(fileUrl);

            using (var reader = new BinaryReader(new MemoryStream(fileBuffer)))
            {
                ReadCoordinates(reader);
            }
        }

        private void ReadCoordinates(BinaryReader reader)
        {
            reader.BaseStream.Position = 12;
            for (int iCoord = 0; iCoord < 20; iCoord++)
            {
                groupFile.UnknownCoordinateAddresses.Add(reader.BaseStream.Position);
                groupFile.UnknownCoordinates.Add(ReadCoordinate(reader));
            }
            reader.BaseStream.Position = 492;//432;
            for (int iCube = 0; iCube < 10; iCube++)
            {
                groupFile.UnknownCubes.Add(ReadCube(reader));
            }
        }

        private Cube ReadCube(BinaryReader reader)
        {
            var cube = new Cube();
            cube.Address = reader.BaseStream.Position;
            cube.Width = reader.ReadInt32();
            cube.Length = reader.ReadInt32();
            //cube.Height = reader.ReadInt32();
            cube.Position = new Vector3(reader.ReadInt32(), reader.ReadInt32(), 0);
            //cube.Height = reader.ReadInt32();

            return cube;
        }

        private Vector3 ReadCoordinate(BinaryReader reader)
        {
            return new Vector3(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
        }
    }
}
