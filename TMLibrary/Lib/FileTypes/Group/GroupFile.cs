﻿using System.Collections.Generic;
using System.Numerics;
using TMLibrary.Lib.Primitives;

namespace TMLibrary.Lib.FileTypes.Group
{
    public class GroupFile
    {
        private List<Vector3> unknownCoordinates;
        private List<Cube> unknownCubes;
        private List<long> unknownCoordinateAddresses;

        public GroupFile()
        {
            unknownCubes = new List<Cube>();
            unknownCoordinates = new List<Vector3>();
            unknownCoordinateAddresses = new List<long>();
        }

        public List<Vector3> UnknownCoordinates { get => unknownCoordinates; }
        public List<long> UnknownCoordinateAddresses { get => unknownCoordinateAddresses; }
        public List<Cube> UnknownCubes { get => unknownCubes; set => unknownCubes = value; }
    }
}
