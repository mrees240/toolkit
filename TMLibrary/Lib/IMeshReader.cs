﻿using System.IO;
using TMLibrary.Rendering;

namespace TMLibrary.Lib
{
    public interface IMeshReader
    {
        MeshHeader ReadMeshHeader(BinaryReader reader, GameMesh mesh);
        RenderFlag FindRenderFlag(ushort renderFlagId);
    }
}