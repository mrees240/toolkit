﻿using System.IO;

namespace TMLibrary.Lib.Extensions
{
    /// <summary>
    ///     Provides useful extensions for Binary Readers. Including peeking and skipping.
    /// </summary>
    public static class BinaryReaderExtensions
    {
        public static ushort PeekUInt16(this BinaryReader binaryReader)
        {
            var value = binaryReader.ReadUInt16();
            binaryReader.BaseStream.Position -= sizeof(ushort);
            return value;
        }

        public static uint PeekUInt32(this BinaryReader binaryReader)
        {
            var value = binaryReader.ReadUInt32();
            binaryReader.BaseStream.Position -= sizeof(uint);
            return value;
        }

        public static void SkipBytes(this BinaryReader binaryReader, int amount)
        {
            binaryReader.BaseStream.Position += amount;
        }

        public static bool ReachedEnd(this BinaryReader binaryReader)
        {
            return binaryReader.BaseStream.Position >= binaryReader.BaseStream.Length - 1;
        }
    }
}