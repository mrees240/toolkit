﻿using System.IO;

namespace TMLibrary.Lib.Extensions
{
    public static class BinaryWriterExtensions
    {

        public static void SkipBytes(this BinaryWriter binaryWriter, int amount)
        {
            binaryWriter.BaseStream.Position += amount;
        }
    }
}
