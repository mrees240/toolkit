﻿namespace TMLibrary.Lib
{
    /// <summary>
    ///     Describes what the pointer points to.
    /// </summary>
    public enum PointerType
    {
        None,
        MeshPointer,
        InstructionPointer
    }
}