﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.MeshReaders
{
    /// <summary>
    /// Reads a mesh header from a DPC file for TM1.
    /// </summary>
    public class Tm1PcMeshReader : IMeshReader
    {
        public GamePolygon FindDefintion(uint header, ProjectDefinition project)
        {
            throw new NotImplementedException();
        }

        public RenderFlag FindRenderFlag(ushort renderFlagId)
        {
            throw new NotImplementedException();
        }

        public MeshHeader ReadMeshHeader(BinaryReader reader, GameMesh mesh)
        {
            throw new NotImplementedException();
        }
    }
}