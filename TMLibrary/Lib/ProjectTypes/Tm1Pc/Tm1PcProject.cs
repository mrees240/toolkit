﻿using System.Drawing;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc;

namespace TMLibrary.Lib.ProjectTypes
{
    /// <summary>
    ///     Creates the project type definition for TM1 on PC.
    /// </summary>
    public class Tm1PcProject : IProjectTypeCreator
    {
        public ProjectDefinition CreateProjectType()
        {
            /*var project = new ProjectDefinition();
            project.MeshReader = new Tm1PcMeshReader();
            project.MeshFile = new MeshArchiveFile(CreateMeshArchiveDefinition(), project);
            project.TextureArchiveFile = new TextureArchiveFile(CreateTextureArchiveDefinition());

            return project;*/
            return null;
        }

        private TextureArchiveDefinition CreateTextureArchiveDefinition()
        {
            return new TextureArchiveDefinition
            {
                DefaultPixelFormat = System.Drawing.Imaging.PixelFormat.Format4bppIndexed,
                FileSignature = "TPCMC",
                FileExtension = "TPC",
                HasMaxArea = false,
                TransparencyColor = Color.FromArgb(255, 248, 248, 248),
                SharesClut = true,
                FirstTimAddress = 16,
                FlipRgb = false,
                RequiresPowerOfTwoDimensions = false
            };
        }

        /*private Dictionary<uint, InstructionDefinition> CreateInstructionDefinitions()
        {
            var definitions = new Dictionary<uint, InstructionDefinition>();

            AddInstructionDefinition(InstructionType.Collision, 0xFF08, 112, 0, 1, definitions);
            AddInstructionDefinition(InstructionType.AnimationFrameContainer, 0x0903, 32, 6, 0, definitions);
            AddInstructionDefinition(InstructionType.Matrix, 0x0B05, 56, 54, 0, definitions);
            AddInstructionDefinition(InstructionType.SubEntity, 0x509, 8, 6, 0, definitions);
            AddInstructionDefinition(InstructionType.TranslationMatrix, 0x0B04, 20, 18, 0, definitions);
            AddInstructionDefinition(InstructionType.BackgroundEntity, 0x030C, 8, 6, 0, definitions);
            AddInstructionDefinition(InstructionType.MeshContainer, 0x602, 20, 16, 0, definitions);
            AddInstructionDefinition(InstructionType.MeshContainer, 0x702, 20, 16, 0, definitions);
            AddInstructionDefinition(InstructionType.MeshContainer, 0x802, 20, 16, 0, definitions);
            AddInstructionDefinition(InstructionType.MeshContainer, 0x902, 20, 16, 0, definitions);
            AddInstructionDefinition(InstructionType.MeshContainer, 0xA02, 20, 16, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0x060B, 12, 10, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0xFF07, 72, 0, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0x6, 100, 0, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0x5, 40, 0, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0x701, 24, 20, 0, definitions);
            AddInstructionDefinition(InstructionType.Unknown, 0x50b, 8, 0, 3, definitions);

            return definitions;
        }

        private void AddInstructionDefinition(InstructionType type, ushort header, int instructionLength, int pointerAmountIndex, int defaultChildAmount, Dictionary<uint, InstructionDefinition> dictionary)
        {
            dictionary.Add(header, new InstructionDefinition(type, header, instructionLength, pointerAmountIndex, defaultChildAmount));
        }*/

        private PointerDefinition[] CreatePointerDefinitions()
        {
            var pointerDefinitions = new PointerDefinition[35];
            ushort startTag = 0x8001;

            for (ushort iPointer = 0; iPointer < pointerDefinitions.Length; iPointer++)
            {
                var currentTag = (ushort) (startTag + iPointer);

                if (currentTag >= 0x8002)
                {
                    var offset = currentTag - 0x8002;
                    long pointerOperation = 30536 + 65536 * offset;
                    long minAddress = 0;
                    long maxAddress = 0;
                    pointerDefinitions[iPointer] =
                        new PointerDefinition(currentTag, pointerOperation, minAddress, maxAddress);
                }
                else
                {
                    pointerDefinitions[iPointer] = new PointerDefinition(currentTag, -35000, 0, 25536);
                }
            }

            return pointerDefinitions;
        }

        private MeshArchiveDefinition CreateMeshArchiveDefinition()
        {
            var meshArchiveDefinition = new MeshArchiveDefinition
            {
                // Asterisks are wildcards
                FileSignature = "DCPMC******1",
                FileExtension = "DPC",
                MeshHeaderTag = 0xFF00,
                VertexStride = 8
            };

            meshArchiveDefinition.InstructionParser = new DpcInstructionParser("TM1PC");
            meshArchiveDefinition.PointerDefinitions = CreatePointerDefinitions();

            return meshArchiveDefinition;
        }
    }
}