﻿using ColladaLibrary.Rendering;
using System.IO;
using System.Linq;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Rendering.DpcTm2
{
    public class Tm2Ps1PolygonController : PolygonController
    {
        public Tm2Ps1PolygonController(ProjectDefinition project) : base(project)
        {
        }

        public override GamePolygon Read(BinaryReader reader, GameMesh parent)
        {
            var addr = reader.BaseStream.Position;
            uint header = reader.ReadUInt32();
            var type = project.PolygonTypeDictionary[header];
            GamePolygon polygon = new GamePolygon(project);
            polygon.Address = addr;
            polygon.PolygonType = type;
            polygon.Parent = parent;
            polygon.TextureFile = project.TextureArchiveFile.TextureFiles.First();

            /*switch (header)
            {
                case 0x4070100:
                    polygon.BufferSize = 28;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    break;
                case 0x4080100:
                    polygon.BufferSize = 32;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x5070100:
                    polygon.BufferSize = 28;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    break;
                case 0x5080100:
                    polygon.BufferSize = 32;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x6090100:
                    polygon.BufferSize = 36;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x6090300:
                    polygon.BufferSize = 36;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    break;
                case 0x60A0300:
                    polygon.BufferSize = 40;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x60B0300:
                    polygon.BufferSize = 44;
                    polygon.ColorOnly = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    break;
                case 0x70B0103:
                    polygon.BufferSize = 44;
                    polygon.HideDiffuse = true;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    break;
                case 0x70C0103:
                    polygon.BufferSize = 48;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x70C0183:
                    polygon.BufferSize = 48;
                    polygon.SetDefaultIndices(3);
                    polygon.HideDiffuse = true;
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x70D0183:
                    polygon.BufferSize = 52;
                    polygon.SetDefaultIndices(3);
                    polygon.HideDiffuse = true;
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    polygon.UnknownUInt3 = reader.ReadUInt32();
                    break;
                case 0x8090100:
                    polygon.ColorOnly = true;
                    polygon.BufferSize = 36;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x90C0104:
                    polygon.BufferSize = 48;
                    polygon.SetDefaultIndices(4);
                    polygon.HideDiffuse = true;
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    break;
                case 0x90D0103:
                    polygon.BufferSize = 52;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x90D0104:
                    polygon.BufferSize = 52;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x90D0184:
                    polygon.BufferSize = 52;
                    polygon.SetDefaultIndices(4);
                    polygon.HideDiffuse = true;
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x90D0303:
                    polygon.BufferSize = 52;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    break;
                case 0x90E0183:
                    polygon.BufferSize = 56;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    polygon.UnknownUInt3 = reader.ReadUInt32();
                    break;
                case 0x90E0184:
                    polygon.BufferSize = 56;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x90E0303:
                    polygon.BufferSize = 56;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    //ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x90E0383:
                    polygon.BufferSize = 56;
                    polygon.SetDefaultIndices(3);
                    polygon.HideDiffuse = true;
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    break;
                case 0x90F0303:
                    polygon.BufferSize = 60;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x90F0383:
                    polygon.BufferSize = 60;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0x9100383:
                    polygon.BufferSize = 64;
                    polygon.SetDefaultIndices(3);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadTextureDiffuseColors(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    polygon.UnknownUInt3 = reader.ReadUInt32();
                    break;
                case 0xC0E0104:
                    polygon.BufferSize = 56;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    break;
                case 0xC0F0184:
                    polygon.BufferSize = 60;
                    polygon.SetDefaultIndices(4);
                    ReadIndices(reader, polygon);
                    ReadCrossProduct(reader, polygon);
                    ReadRenderFlagId(reader, polygon);
                    polygon.Attribute0 = reader.ReadUInt32();
                    ReadSharedTextureDiffuseColor(reader, polygon);
                    ReadTextureCoordinates(reader, polygon);
                    ReadTextureId(reader, polygon);
                    polygon.UnknownUShort0 = reader.ReadUInt16();
                    polygon.UnknownUInt1 = reader.ReadUInt32();
                    polygon.UnknownUInt2 = reader.ReadUInt32();
                    polygon.UnknownUInt3 = reader.ReadUInt32();
                    break;*/
            /*case 0x00080904:
                polygon.BufferSize = 36;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x03080904:
                polygon.BufferSize = 36;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                polygon.UnknownUInt0 = reader.ReadUInt32();
                ReadSharedTextureDiffuseColor(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                ReadTextureId(reader, polygon);
                break;
            case 0x0C0D0404:
                polygon.BufferSize = 52;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                polygon.UnknownUInt0 = reader.ReadUInt32();
                //ReadTextureDiffuseColors(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                //ReadTextureId(reader, polygon);
                break;
            case 0x09090104:
                polygon.BufferSize = 36;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                polygon.UnknownUInt0 = reader.ReadUInt32();
                ReadSharedTextureDiffuseColor(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                ReadTextureId(reader, polygon);
                break;
            case 0x0C0E0404:
                polygon.BufferSize = 56;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                polygon.UnknownUInt0 = reader.ReadUInt32();
                //ReadSharedTextureDiffuseColor(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                ReadTextureId(reader, polygon);
                break;
            case 0x090A0104:
                polygon.BufferSize = 40;
                polygon.SetDefaultIndices(4);
                polygon.SetWhiteDiffuseColors(4);
                ReadIndices(reader, polygon);
                polygon.CrossProduct = new Microsoft.Xna.Framework.Vector3(0.0f, 0.0f, 1.0f);
                //ReadCrossProduct(reader, polygon);
                //ReadRenderFlagId(reader, polygon);
                reader.BaseStream.Center += 4;
                //ReadSharedTextureDiffuseColor(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                if (reader.BaseStream.Center >= polygon.BufferSize + polygon.Address)
                {

                }
                break;
            case 0x0C0C0404:
                polygon.BufferSize = 48;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x0C0B0104:
                polygon.BufferSize = 44;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                //ReadCrossProduct(reader, polygon);
                //ReadRenderFlagId(reader, polygon);
                ReadTextureCoordinates(reader, polygon);
                break;
            case 0x090A0184:
                polygon.BufferSize = 40;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090B0383:
                polygon.BufferSize = 44;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090A0303:
                polygon.BufferSize = 40;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x08090400:
                polygon.BufferSize = 36;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x07090103:
                polygon.BufferSize = 36;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090A0103:
                polygon.BufferSize = 40;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090B0303:
                polygon.BufferSize = 44;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090C0303:
                polygon.BufferSize = 48;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090B0184:
                polygon.BufferSize = 44;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x07080183:
                polygon.BufferSize = 32;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x07080103:
                polygon.BufferSize = 32;
                polygon.SetDefaultIndices(3);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x08080400:
                polygon.BufferSize = 32;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x0C0C0184:
                polygon.BufferSize = 48;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x090B0183:
                polygon.BufferSize = 44;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x04060100:
                polygon.BufferSize = 48;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            case 0x06070100:
                polygon.BufferSize = 52;
                polygon.SetDefaultIndices(4);
                ReadIndices(reader, polygon);
                ReadCrossProduct(reader, polygon);
                ReadRenderFlagId(reader, polygon);
                break;
            default:
                return null;
                //throw new Exception($"Found {header.ToString("X")} at {polygon.Address}");
                break;
        }
        // RESORT indices
        if (polygon.Indices.Count() == 4)
        {
            polygon.Indices = new int[]
            {
                polygon.Indices[0],
                polygon.Indices[2],
                polygon.Indices[3],
                polygon.Indices[1],
            };
        }
        if (polygon.TexCoords.Count() == 4)
        {
            polygon.TexCoords = new TextureCoordinate[]
            {
                polygon.TexCoords[0],
                polygon.TexCoords[2],
                polygon.TexCoords[3],
                polygon.TexCoords[1],
            };
        }
        RescaleTextureCoordinates(polygon);

        reader.BaseStream.Center = polygon.Address + polygon.BufferSize;*/
            return polygon;
        }

        protected override void ReadTextureCoordinates(BinaryReader reader, GamePolygon polygon)
        {
            /*for (int iCoord = 0; iCoord < polygon.TexCoords.Length; iCoord++)
            {
                polygon.TexCoords[iCoord] = ReadTextureCoordinate(reader);
            }*/
        }

        protected override TextureCoordinate ReadTextureCoordinate(BinaryReader reader)
        {
            var texCoord = new TextureCoordinate();
            texCoord.V = reader.ReadByte();
            texCoord.U = reader.ReadByte();

            return texCoord;
        }

        protected override void ReadTextureId(BinaryReader reader, GamePolygon polygon)
        {
            ushort originalTextureId = reader.ReadUInt16();

            // Still not 100% sure how this works
            int textureId = (ushort)(originalTextureId - originalTextureId / 0x1000 * 0x1000);

            polygon.TextureFile = project.TextureArchiveFile.TextureFiles[0];

            RescaleTextureCoordinates(polygon);
        }

        public override void RescaleTextureCoordinates(GamePolygon polygon)
        {
            /*foreach (var texCoord in polygon.TexCoords)
            {
                texCoord.U /= polygon.TextureFile.ActualWidth;
                texCoord.V /= polygon.TextureFile.Height;
            }*/
        }

        public override void Write(BinaryWriter writer, GamePolygon polygon)
        {
            polygon.Address = writer.BaseStream.Position;
            writer.Write(polygon.PolygonType.Header);
            //writer.BaseStream.Center = polygon.Address + polygon.BufferSize;
        }
    }
}
