﻿using System.Drawing;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dmd;
using TMLibrary.Lib.MeshReaders;
using TMLibrary.Lib.MeshWriters;
using TMLibrary.Rendering;
using TMLibrary.Rendering.DpcTm2;
using TMLibrary.Textures;

namespace TMLibrary.Lib.ProjectTypes
{
    /// <summary>
    ///     Defines the project type definition for TM2 on PS1. Is incomplete.
    /// </summary>
    public class Tm2Ps1Project : IProjectTypeCreator
    {
        public ProjectDefinition CreateProjectType()
        {
            var project = new ProjectDefinition();
            var nonTransparentTimHeader = new TimHeaderFormat(new byte[] { 0x09, 0x00, 0x00, 0x00 }, 8, false, true);
            var nonTransparentTimHeader2 = new TimHeaderFormat(new byte[] { 0x08, 0x00, 0x00, 0x00 }, 4, false, true);
            var transparentTimHeader = new TimHeaderFormat(new byte[] { 0x19, 0x00, 0x00, 0x00 }, 8, true, true);

            var meshArchiveDefinition = new MeshArchiveDefinition
            {
                FileSignature = "DXSPC******2",
                FileExtension = "DMD",
                MeshHeaderTag = 0x0000FF00,
                VertexStride = 8,
                PolygonController = new Tm2Ps1PolygonController(project)
            };

            var textureArchiveDefinition = new TextureArchiveDefinition
            {
                DefaultPixelFormat = System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                FileSignature = "TXSPC******2",
                FileExtension = "TMS",
                RequiresPowerOfTwoDimensions = true,
                TransparencyColor = Color.Black,
                FirstTimAddress = 0,
                TransparentTimHeader = transparentTimHeader,
                NonTransparentTimHeader = nonTransparentTimHeader,
                TimHeaderFormats = new TimHeaderFormat[]
                {
                    nonTransparentTimHeader2,
                    nonTransparentTimHeader,
                    transparentTimHeader
                },
                TimHeaderFormats8Bpp = new TimHeaderFormat[]
                {
                    nonTransparentTimHeader,
                    transparentTimHeader,
                },
                TimHeaderFormats4Bpp = new TimHeaderFormat[]
                {
                    nonTransparentTimHeader2,
                }
            };

            var pointerDefinitions = new PointerDefinition[15];
            ushort startTag = 0x8001;

            for (ushort iPointer = 0; iPointer < pointerDefinitions.Length; iPointer++)
            {
                var currentTag = (ushort) (startTag + iPointer);

                if (currentTag > startTag)
                {
                    var offset = (ushort) (currentTag - 0x8002);
                    long pointerOperation = 25536 + offset * ushort.MaxValue;
                    long minAddress = 25537 + offset * ushort.MaxValue;
                    var maxAddress = minAddress + ushort.MaxValue - 1;
                    pointerDefinitions[iPointer] =
                        new PointerDefinition(currentTag, pointerOperation, minAddress, maxAddress);
                }
                else
                {
                    pointerDefinitions[iPointer] = new PointerDefinition(currentTag, -40000, 0, 25536);
                }
            }
            meshArchiveDefinition.PointerDefinitions = pointerDefinitions;
            meshArchiveDefinition.InstructionParser = new DmdInstructionParser("TM2PS1");
            project.MeshReader = new Tm2Ps1MeshReader();
            project.MeshWriter = new Tm2PcMeshWriter(project);
            project.MeshFile = new MeshArchiveFile(meshArchiveDefinition, project);
            project.TextureArchiveFile = new TextureArchiveFile(textureArchiveDefinition);
            //project.SkipTextures = true;

            return project;
        }
    }
}