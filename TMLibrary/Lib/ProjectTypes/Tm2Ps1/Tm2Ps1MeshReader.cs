﻿using System.IO;
using TMLibrary.Rendering;
using TMLibrary.Rendering.DpcTm2;

namespace TMLibrary.Lib.MeshReaders
{
    /// <summary>
    /// Reads a mesh header from a DMD file for TM2.
    /// </summary>
    public class Tm2Ps1MeshReader : IMeshReader
    {
        public RenderFlag FindRenderFlag(ushort renderFlagId)
        {
            switch (renderFlagId)
            {
                case 5760:
                case 5769:
                case 5761:
                case 0x1781:
                case 5768:
                case 5856:
                    return RenderFlag.Billboard2;
            }
            return RenderFlag.Normal;
        }

        public MeshHeader ReadMeshHeader(BinaryReader reader, GameMesh mesh)
        {
            var meshHeader = new MeshHeaderTm2(mesh);
            meshHeader.ReadMeshHeader(reader);
            return meshHeader;
        }
    }
}