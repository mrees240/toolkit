﻿using System.Collections.Generic;
using System.Drawing;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc;
using TMLibrary.Lib.MeshReaders;
using TMLibrary.Lib.MeshWriters;
using TMLibrary.Rendering;
using TMLibrary.Rendering.DpcTm2.Polygons;
using TMLibrary.Textures;

namespace TMLibrary.Lib.ProjectTypes
{
    /// <summary>
    /// Defines the project type definition for TM2 on PC.
    /// </summary>
    public class OutwarsPcProject : IProjectTypeCreator
    {
        public ProjectDefinition CreateProjectType()
        {
            var project = new ProjectDefinition();
            project.MeshFile = new MeshArchiveFile(CreateMeshArchiveDefinition(project), project);
            project.TextureArchiveFile = new TextureArchiveFile(CreateTextureArchiveDefinition());
            project.MeshWriter = new Tm2PcMeshWriter(project);
            project.MeshReader = new Tm2PcMeshReader();
            project.FileWriter = new Tm2FileWriter();
            project.Saveable = false;
            project.PolygonTypeDictionary = CreatePolygonTypeDictionary();
            //project.SkipTextures = true;

            return project;
        }

        private Dictionary<uint, PolygonType> CreatePolygonTypeDictionary()
        {
            var types = new Dictionary<uint, PolygonType>();

            AddPolygonToDictionary(new PolygonType(0xC0E0104, 4, false, false, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0x90C0104, 4, false, false, true, true, 1), types);
            AddPolygonToDictionary(new PolygonType(0x90D0104, 4, false, false, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x90D0184, 4, false, false, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x90E0184, 4, false, false, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0xC0F0184, 4, false, false, true, true, 7), types);
            AddPolygonToDictionary(new PolygonType(0x90F0303, 3, false, true, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0x90F0383, 3, false, true, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0x90E0303, 3, false, true, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x90E0383, 3, false, true, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x9100383, 3, false, true, true, true, 7), types);
            AddPolygonToDictionary(new PolygonType(0x70C0183, 3, false, false, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x90D0103, 3, false, false, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0x90E0183, 3, false, false, true, true, 7), types);
            AddPolygonToDictionary(new PolygonType(0x70C0103, 3, false, false, true, true, 3), types);
            AddPolygonToDictionary(new PolygonType(0x70B0103, 3, false, false, true, true, 1), types);
            AddPolygonToDictionary(new PolygonType(0x70D0183, 3, false, false, true, true, 5), types);
            AddPolygonToDictionary(new PolygonType(0x90D0303, 3, false, true, true, true, 1), types);
            AddPolygonToDictionary(new PolygonType(0x64060B03, 3, true, false, true, true, 4), types);
            AddPolygonToDictionary(new PolygonType(0x60A0300, 3, true, false, true, true, 6), types);
            AddPolygonToDictionary(new PolygonType(0x60B0300, 3, true, true, true, true, 4), types);
            AddPolygonToDictionary(new PolygonType(0x6090300, 3, true, false, true, true, 4), types);
            AddPolygonToDictionary(new PolygonType(0x4070100, 3, true, false, true, true), types);
            AddPolygonToDictionary(new PolygonType(0x4080100, 3, true, false, true, true, 2), types);
            AddPolygonToDictionary(new PolygonType(0x5080100, 4, true, false, true, true, 2), types);
            AddPolygonToDictionary(new PolygonType(0x6090100, 4, true, false, true, true, 4), types);
            AddPolygonToDictionary(new PolygonType(0x5070100, 4, true, false, true, true), types);
            AddPolygonToDictionary(new PolygonType(0x8090100, 4, true, false, true, true, 4), types);

            return types;
        }

        private void AddPolygonToDictionary(PolygonType polygonType, Dictionary<uint, PolygonType> polygonTypeDictionary)
        {
            polygonTypeDictionary.Add(polygonType.Header, polygonType);
        }

        private TextureArchiveDefinition CreateTextureArchiveDefinition()
        {
            var nonTransparentTimHeader = new TimHeaderFormat(new byte[] { 0x09, 0x00, 0x00, 0x00 }, 8, false, true);
            var transparentTimHeader = new TimHeaderFormat(new byte[] { 0x19, 0x00, 0x00, 0x00 }, 8, true, true);

            return new TextureArchiveDefinition
            {
                DefaultPixelFormat = System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                FileSignature = "TCPMC...**Y4",
                FileExtension = "TPC",
                RequiresPowerOfTwoDimensions = false,
                HasMaxArea = false,
                TransparencyColor = Color.Black,
                FlipRgb = true,
                FirstTimAddress = 16,
                TransparentTimHeader = transparentTimHeader,
                NonTransparentTimHeader = nonTransparentTimHeader,
                SkipOnlyFirstTpcHeader = false,
                TimHeaderFormats = new TimHeaderFormat[]
                {
                    new TimHeaderFormat(new byte[] { 1, 0x00, 0x00, 0x00 }, 8, false, true),
                    nonTransparentTimHeader,
                    transparentTimHeader
                }
            };
        }

        private MeshArchiveDefinition CreateMeshArchiveDefinition(ProjectDefinition project)
        {
            var meshArchiveDefinition = new MeshArchiveDefinition
            {
                FileSignature = "DCPMC******5",
                FileExtension = "DPC",
                MeshHeaderTag = 0xFF00,
                VertexStride = 8,
                PolygonController = new Tm2PcPolygonController(project)
            };

            meshArchiveDefinition.PointerDefinitions = CreatePointerDefinitions();
            meshArchiveDefinition.InstructionParser = new DpcInstructionParser("TM2PC");

            return meshArchiveDefinition;
        }

        private PointerDefinition[] CreatePointerDefinitions()
        {
            var pointerDefinitions = new PointerDefinition[500];
            ushort pointerStartTag = 0x8001;

            for (ushort iPointer = 0; iPointer < pointerDefinitions.Length; iPointer++)
            {
                var currentTag = (ushort) (pointerStartTag + iPointer);

                if (currentTag > pointerStartTag)
                {
                    var offset = (ushort) (currentTag - 0x8002);
                    long pointerOperation = 25535 + offset * ushort.MaxValue + (currentTag - pointerStartTag);
                    long minAddress = pointerDefinitions[iPointer - 1].MaxAddress + 1;
                    var maxAddress = minAddress + ushort.MaxValue;
                    pointerDefinitions[iPointer] =
                        new PointerDefinition(currentTag, pointerOperation, minAddress, maxAddress);
                    maxAddress += iPointer;
                }
                else
                {
                    pointerDefinitions[iPointer] = new PointerDefinition(currentTag, -40000, 0, 25534);
                }
            }

            return pointerDefinitions;
        }
    }
}