﻿using ColladaLibrary.Render;
using System;
using System.IO;
using System.Linq;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Rendering.DpcTm2.Polygons
{
    public class Tm2PcPolygonController : PolygonController
    {
        public Tm2PcPolygonController(ProjectDefinition project) : base(project)
        {
        }

        public override GamePolygon Read(BinaryReader reader, GameMesh parent)
        {
            var addr = reader.BaseStream.Position;
            uint header = reader.ReadUInt32();

            if (!project.PolygonTypeDictionary.ContainsKey(header))
            {
                //return null;
                throw new Exception($"Unrecognized polygon found at {addr}. Header = {header.ToString("x")}");
            }

            var type = project.PolygonTypeDictionary[header];

            GamePolygon polygon = new GamePolygon(project);
            polygon.PolygonType = type;
            polygon.Parent = parent;
            polygon.UnknownUShorts = new ushort[polygon.PolygonType.UnknownUShortAmount];

            ReadIndices(reader, polygon, type.IndexAmount);
            ReadCrossProduct(reader, polygon);
            ReadRenderFlagId(reader, polygon);
            polygon.PolySort0 = reader.ReadInt16();
            polygon.PolySort1 = reader.ReadInt16();
            if (polygon.PolygonType.HasDiffuseColors && !polygon.PolygonType.PerVertexColors)
            {
                ReadSharedTextureDiffuseColor(reader, polygon);
            }
            else if (polygon.PolygonType.HasDiffuseColors && polygon.PolygonType.PerVertexColors)
            {
                ReadTextureDiffuseColors(reader, polygon);
            }
            if (!polygon.PolygonType.ColorOnly)
            {
                ReadTextureCoordinates(reader, polygon);
                ReadTextureId(reader, polygon);
            }
            for (int iUnknown = 0; iUnknown < polygon.UnknownUShorts.Length; iUnknown++)
            {
                polygon.UnknownUShorts[iUnknown] = reader.ReadUInt16();
            }
            return polygon;
        }

        public bool NeedsSubdivisionTextureFix(Polygon polygon)
        {
            if (!polygon.SubPolygons.Any())
            {
                if (!polygon.Textures.Diffuse.HasBitmap)
                {
                    return false;
                }

                var tex = polygon.Textures.Diffuse.Bitmap;

                var texCoords = polygon.GetAllTextureCoordinates();
                var maxU = texCoords.Max(t => t.U) * tex.Width;
                var maxV = texCoords.Max(t => t.V) * tex.Height;
                var minU = texCoords.Min(t => t.U) * tex.Width;
                var minV = texCoords.Min(t => t.V) * tex.Height;



                if (Math.Abs(maxV - minV) > 512)// || Math.Abs(maxU - minU) > 512)
                {
                    return true;
                }

                return false;
            }
            else
            {
                foreach (var subPoly in polygon.SubPolygons)
                {
                     if (NeedsSubdivisionTextureFix(subPoly))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static Random rng = new Random();

        public override void Write(BinaryWriter writer, GamePolygon polygon)
        {
            polygon.Address = writer.BaseStream.Position;
            writer.Write(polygon.PolygonType.Header); // 0
            WriteIndices(writer, polygon); // 4
            WriteCrossProduct(writer, polygon); // 12
            WriteRenderFlagId(writer, polygon); // 18

            writer.Write(polygon.PolySort0); // 20
            writer.Write(polygon.PolySort1); // 22

            if (polygon.PolygonType.HasDiffuseColors)
            {
                if (!polygon.PolygonType.PerVertexColors)
                {
                    WriteSharedTextureDiffuseColor(writer, polygon);
                }
                else
                {
                    WriteTextureDiffuseColors(writer, polygon);
                }
            }
            if (!polygon.PolygonType.ColorOnly)
            {
                WriteTextureCoordinates(writer, polygon);
                WriteTextureId(writer, polygon);
            }
            for (int unknown = 0; unknown < polygon.PolygonType.UnknownUShortAmount; unknown++)
            {
                if (unknown < polygon.UnknownUShorts.Count())
                {
                    writer.Write(polygon.UnknownUShorts[unknown]);
                }
                else
                {
                    writer.Write((ushort)0);
                }
            }
        }
    }
}
