﻿using System.IO;
using TMLibrary.Lib.Util;

namespace TMLibrary.Rendering.DpcTm2
{
    public class MeshHeaderTm2 : MeshHeader
    {
        private ushort unknown0;
        private long unknownPointer0;
        private byte[] buffer;

        public MeshHeaderTm2(GameMesh mesh) : base(mesh)
        {
            polygonAmount = (ushort)mesh.GetPolygonAmount();

            bufferSize = 44;
            buffer = new byte[26];

        }

        public override void ReadMeshHeader(BinaryReader reader)
        {
            ushort header = reader.ReadUInt16();
            unknown0 = reader.ReadUInt16();
            this.vertexStartAddress = Utility.ReadPointer(reader.ReadUInt32(), mesh.ParentFile.Definition);
            unknownPointer0 = Utility.ReadPointer(reader.ReadUInt32(), mesh.ParentFile.Definition);
            this.polygonStartAddress = Utility.ReadPointer(reader.ReadUInt32(), mesh.ParentFile.Definition);
            polygonAmount = reader.ReadUInt16();

            buffer = reader.ReadBytes(26);
        }

        public override void WriteMeshHeader(BinaryWriter writer)
        {
            /*var polys = mesh.GetAllPolygons().Where(p => p.IsWritable());

            polygonAmount = (ushort)(polys.Count());*/
            writer.Write((ushort)0xFF00);
            writer.Write(unknown0);
            writer.Write(Utility.CreatePointer(vertexStartAddress, mesh.ParentFile.Definition));
            writer.Write(Utility.CreatePointer(unknownPointer0, mesh.ParentFile.Definition));
            writer.Write(Utility.CreatePointer(polygonStartAddress, mesh.ParentFile.Definition));
            writer.Write(polygonAmount);
            writer.BaseStream.Position += buffer.Length;
        }
    }
}
