﻿using System.IO;
using System.Linq;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;
using TMLibrary.Textures;

namespace TMLibrary.Lib.Instructions.Dpc
{
    public class Tm2FileWriter : IFileWriter
    {
        private ProjectDefinition project;

        public Tm2FileWriter()
        {
        }

        public void WriteFiles(string fileName, ProjectDefinition project)
        {
            this.project = project;
            string dpcFileName = fileName;

            byte[] dpcBuffer = new byte[project.MeshFile.GetFileSize()];

            using (var writer = new BinaryWriter(new MemoryStream(dpcBuffer)))
            {
                WriteDpcFile(writer);
            }
            using (var writer = new BinaryWriter(new FileStream(dpcFileName, FileMode.Create)))
            {
                writer.Write(dpcBuffer);
            }
            var textureArchiveController = new TextureArchiveController(project.TextureArchiveFile);
            textureArchiveController.WriteFile(project.TextureArchiveFileName);
        }

        private void ResetInstructionsForNewWrite()
        {
            foreach (var instruction in project.MeshFile.Instructions)
            {
                ResetInstructionWrittenToFile(instruction);
            }
        }

        private void ResetInstructionWrittenToFile(Instruction instruction)
        {
            instruction.WrittenToFile = false;

            foreach (var child in instruction.Children)
            {
                ResetInstructionWrittenToFile(child);
            }
        }

        private void WriteDpcFile(BinaryWriter writer)
        {
            var savableMeshes = project.MeshFile.Meshes;
            var meshController = new MeshArchiveController(project.MeshFile, project);
            ResetInstructionsForNewWrite();

            writer.Write(project.MeshFile.FileHeader.Buffer);
            long rootInstructionStart = writer.BaseStream.Position;
            // Allocate space for root instructions
            writer.Write(new byte[project.MeshFile.Instructions.Count() * sizeof(uint) + 4]);

            // FINISH THIS

            /*foreach (var mesh in savableMeshes)
            {
                // NEEDS CHANGED
                mesh.Subdivide(5000.0f);
                project.MeshWriter.WriteNewMesh(writer, mesh);
            }*/
            foreach (var instruction in project.MeshFile.Instructions)
            {
                instruction.WriteInstruction(writer);
            }
            foreach (var instruction in project.MeshFile.Instructions)
            {
                instruction.WriteInstructionPointers(writer);
            }
            writer.BaseStream.Position = rootInstructionStart;
            writer.Write((byte)project.MeshFile.Instructions.Count());
            writer.Write((byte)0);
            writer.Write((byte)0);
            writer.Write((byte)0);

            // Write root instruction pointers
            foreach (var instruction in project.MeshFile.Instructions)
            {
                writer.Write(Utility.CreatePointer(instruction.Address, project.MeshFile.Definition));
            }
        }
}
}
