﻿using System.Numerics;

namespace TMLibrary.Lib.Instructions.Dpc
{
    public class InstructionMatrix
    {
        private Matrix4x4 matrix;
        private MatrixType matrixType;

        public InstructionMatrix()
        {
            Matrix = Matrix4x4.Identity;
            MatrixType = MatrixType.Default;
        }

        public InstructionMatrix(Matrix4x4 matrix)
        {
            this.Matrix = matrix;
            MatrixType = MatrixType.Default;
        }

        public InstructionMatrix(Matrix4x4 matrix, MatrixType matrixType)
        {
            this.Matrix = matrix;
            this.MatrixType = matrixType;
        }

        public Matrix4x4 Matrix
        {
            get { return matrix; }
            set { matrix = value; }
        }

        public MatrixType MatrixType
        {
            get { return matrixType; }
            set { matrixType = value; }
        }
    }
}
