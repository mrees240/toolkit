﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using TMLibrary.Lib.Extensions;
using TMLibrary.Lib.Instructions.Dpc.Definitions;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.Instructions.Dpc
{
    /// <summary>
    ///     A parser designed to read instructions from a DPC file.
    /// </summary>
    public class DpcInstructionParser : IInstructionParser
    {
        private List<Instruction> allInstructionList;
        private readonly string gameKey;
        private MeshArchiveFile meshFile;

        public DpcInstructionParser(string gameKey)
        {
            allInstructionList = new List<Instruction>();
            this.gameKey = gameKey;
        }

        public void ParseInstructions(MeshArchiveFile meshFile, BinaryReader reader)
        {
            this.meshFile = meshFile;
            ReadFileHeader(reader);
            ReadRootInstructionsTm2pc(reader);
            CreateEntities();
        }

        private void ReadFileHeader(BinaryReader reader)
        {
            if (gameKey == "TM2PC")
            {
                meshFile.FileHeader.Buffer = reader.ReadBytes(32);
            }
        }

        private Instruction ReadInstruction(BinaryReader reader, Matrix4x4 stack)
        {
            long address = reader.BaseStream.Position;
            Instruction instruction = null;

            instruction = allInstructionList.Where(i => i.Address == address).FirstOrDefault();
            if (instruction != null)
            {
                return instruction;
            }

            var header = reader.ReadUInt16();

            bool isMeshContainer = false;

            switch (header)
            {
                case 0x5:
                    instruction = new Dpc5(meshFile.Parent, address);
                    break;
                case 0x6:
                    instruction = new Dpc6(meshFile.Parent, address);
                    break;
                case 0x701:
                    instruction = new Dpc701BoundingSphere(meshFile.Parent, address);
                    break;
                case 0x50B:
                    instruction = new Dpc50B(meshFile.Parent, address);
                    break;
                case 0x60B:
                    instruction = new Dpc60B(meshFile.Parent, address);
                    break;
                case 0x903:
                    instruction = new DpcAnimationContainer(meshFile.Parent, address);
                    break;
                case 0x30C:
                    instruction = new DpcBackgroundEntity(meshFile.Parent, address);
                    break;
                case 0xFF08:
                    instruction = new DpcCollisionFF08(meshFile.Parent, address);
                    break;
                case 0xFF07:
                    instruction = new DpcFF07(meshFile.Parent, address);
                    break;
                case 0xB05:
                    instruction = new DpcMatrix(meshFile.Parent, address);
                    break;
                case 0x509:
                    instruction = new DpcSubEntity509(meshFile.Parent, address);
                    break;
                case 0xB04:
                    instruction = new DpcTranslationMatrix(meshFile.Parent, address);
                    break;
                case 0x602:
                    instruction = new DpcMeshContainer602(meshFile.Parent, address);
                    isMeshContainer = true;
                    break;
                case 0x702:
                    instruction = new DpcMeshContainer702(meshFile.Parent, address);
                    isMeshContainer = true;
                    break;
                case 0x802:
                    instruction = new DpcMeshContainer802(meshFile.Parent, address);
                    isMeshContainer = true;
                    break;
                case 0x902:
                    instruction = new DpcMeshContainer902(meshFile.Parent, address);
                    isMeshContainer = true;
                    break;
                case 0xA02:
                    instruction = new DpcMeshContainerA02(meshFile.Parent, address);
                    isMeshContainer = true;
                    break;
                default:
                    instruction = new DpcMeshLink(meshFile.Parent, address);
                    break;
            }
            /*if (isMeshContainer)
            {
                instruction.ReadInstruction(reader);
                ReadMeshContainerChildren(instruction.ChildAmount, reader, instruction);
            }
            else*/
            {
                instruction.Address = address;
                instruction.ReadInstruction(reader);
                if (instruction is DpcMatrix)
                {
                    stack = ((DpcMatrix)instruction).MatrixWrapper.Matrix;
                }
                if (instruction is DpcTranslationMatrix)
                {
                    stack = ((DpcTranslationMatrix)instruction).MatrixWrapper.Matrix;
                }
                // Read child pointers
                for (int iChild = 0; iChild < instruction.ChildAmount; iChild++)
                {
                    instruction.ChildAddresses.Add(Utility.ReadPointer(reader.ReadUInt32(), meshFile.Definition));
                }
                //long returnAddress = reader.BaseStream.Center;
                foreach (var childAddress in instruction.ChildAddresses)
                {
                    if (childAddress > 0)
                    {
                        reader.BaseStream.Position = childAddress;
                        if (reader.BaseStream.Position >= reader.BaseStream.Length)
                        {
                            continue;
                        }
                        uint headerCheck = reader.PeekUInt16();
                        if (headerCheck != meshFile.Definition.MeshHeaderTag)
                        {
                            var child = ReadInstruction(reader, stack);
                            child.Parents.Add(instruction);
                            instruction.Children.Add(child);
                        }
                        else
                        {
                            var mesh = meshFile.Meshes.Where(m => m.Address == childAddress).FirstOrDefault();
                            if (mesh == null)
                            {
                                var meshArchiveController = new MeshArchiveController(meshFile, meshFile.Parent);
                                mesh = meshArchiveController.ParseMesh(reader, meshFile.Meshes, childAddress);
                                meshFile.Meshes.Add(mesh);
                            }
                            instruction.MeshList.Add(mesh);
                        }
                    }
                }
            }
            instruction.GlobalMatrix = stack;
            return instruction;
        }

        private void InitializeRootInstructionTm1pc(BinaryReader reader)
        {
            var baseInstructionAmountIndex = 20;
            reader.BaseStream.Position = baseInstructionAmountIndex;
            int baseInstructionAmount = reader.ReadByte();
            reader.SkipBytes(3);

            for (var iInstruction = 0; iInstruction < baseInstructionAmount; iInstruction++)
            {
                var rootPtrAddress = Utility.ReadPointer(reader.ReadUInt32(), meshFile.Definition);
                var rootInstruction =
                    meshFile.Instructions.Where(i => i.Address == rootPtrAddress).FirstOrDefault();
                if (rootInstruction != null)
                {
                    rootInstruction.IsRootInstruction = true;
                }
            }
        }

        private void ReadRootInstructionsTm2pc(BinaryReader reader)
        {
            int baseInstructionAmount = reader.ReadByte();
            reader.SkipBytes(3);

            for (var iInstruction = 0; iInstruction < baseInstructionAmount; iInstruction++)
            {
                long rootPtr = Utility.ReadPointer(reader.ReadUInt32(), meshFile.Definition);
                long returnAddress = reader.BaseStream.Position;
                reader.BaseStream.Position = rootPtr;
                var rootInstruction = ReadInstruction(reader, Matrix4x4.Identity);
                rootInstruction.IsRootInstruction = true;
                meshFile.Instructions.Add(rootInstruction);
                reader.BaseStream.Position = returnAddress;
            }
        }

        /// <summary>
        ///     Uses instructions found to create entities.
        /// </summary>
        private void CreateEntities()
        {
            foreach (var instruction in meshFile.Instructions)
            {
                CreateEntities(instruction);
            }
        }

        private Entity CreateEntities(Instruction instruction)
        {
            var entity = new Entity(instruction);
            meshFile.Entities.Add(entity);

            foreach (var child in instruction.Children)
            {
                entity.Children.Add(CreateEntities(child));
            }
            return entity;
        }
    }
}