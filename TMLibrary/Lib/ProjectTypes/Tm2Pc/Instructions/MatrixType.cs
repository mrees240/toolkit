﻿namespace TMLibrary.Lib.Instructions.Dpc
{
    /// <summary>
    /// Defines the type of object that the matrix will encapsulate. Used for meshes and item pickups.
    /// </summary>
    public enum MatrixType
    {
        Default = 0x0,
        Health = 0x01A2,
        FireMissle = 0x0191,
        Napalm = 0x019B,
        Lightning = 0x01A3,
        Ricochet = 0x019D,
        Homing = 0x0193,
        RemoteExplosive = 0x0196,
        PowerMissle = 0x0194,
        Turbo = 0x01AE
    }
}
