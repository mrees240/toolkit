﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class Dpc60B : Instruction
    {
        public Dpc60B(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 12;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            buffer = reader.ReadBytes(10);
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x60B);
            writer.Write(buffer);
        }
    }
}
