﻿using System.Collections.Generic;
using System.IO;
using System.Numerics;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class Dpc701BoundingSphere : Instruction, IComparer<Dpc701BoundingSphere>
    {
        private ushort parameter0;
        /// <summary>
        /// The bounding sphere's origin is the object's local position.
        /// </summary>
        private BoundingSphere boundingSphere;
        private byte parameter5, parameter6, parameter7;

        public BoundingSphere BoundingSphere { get => boundingSphere; set => boundingSphere = value; }

        public Dpc701BoundingSphere(ProjectDefinition project, long address) : base(project, address)
        {
            this.isBoundingSphere = true;
            bufferSize = 24;
            this.boundingSphere = new BoundingSphere(new Vector3(), 0);
        }

        public Dpc701BoundingSphere(ProjectDefinition project, long address, ushort parameter0, int xPos, int yPos, int zPos, int radius,
            byte parameter5, byte parameter6, byte parameter7) : this(project, address)
        {
            this.parameter0 = parameter0;
            this.boundingSphere = new BoundingSphere(new Vector3(xPos, yPos, zPos), radius);
            this.parameter5 = parameter5;
            this.parameter6 = parameter6;
            this.parameter7 = parameter7;
        }

        public override string GetInstructionText()
        {
            return $"0x701 Bounding Sphere Parameter0: {parameter0} Sphere X:{boundingSphere.Center.X} Y:{boundingSphere.Center.Y} Z:{boundingSphere.Center.Z} Radius:{boundingSphere.Radius}";
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            parameter0 = reader.ReadUInt16();
            int xPos = reader.ReadInt32();
            int yPos = reader.ReadInt32();
            int zPos = reader.ReadInt32();
            int radius = (int)(reader.ReadInt32() / 2.0f);
            boundingSphere = new BoundingSphere(new Vector3(xPos, yPos, zPos), radius);
            childAmount = reader.ReadByte();
            parameter5 = reader.ReadByte();
            parameter6 = reader.ReadByte();
            parameter7 = reader.ReadByte();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x701);
            writer.Write(parameter0);
            writer.Write((int)(boundingSphere.Center.X));
            writer.Write((int)(boundingSphere.Center.Y));
            writer.Write((int)(boundingSphere.Center.Z));
            writer.Write((int)(boundingSphere.Radius));
            WriteChildAmount(writer);
            writer.Write(parameter5);
            writer.Write(parameter6);
            writer.Write(parameter7);
        }

        public void SetMaxRadius()
        {
            boundingSphere.Radius = 9999999;
        }

        public void SetRadius(uint radius)
        {
            boundingSphere.Radius = radius;
        }

        public void ResetOrigin()
        {
            boundingSphere.Center = new Vector3();
        }

        public int Radius
        {
            get
            {
                return (int)boundingSphere.Radius;
            }
        }

        public void SetOrigin(Vector3 origin)
        {
            boundingSphere.Center = origin;
        }

        public void TranslateOrigin(Vector3 translation)
        {
            boundingSphere.Center += translation;
        }

        public bool ContainsSphere(Dpc701BoundingSphere otherSphere)
        {
            var distance = Vector3.Distance(boundingSphere.Center, otherSphere.boundingSphere.Center);

            return distance < boundingSphere.Radius;
        }

        int IComparer<Dpc701BoundingSphere>.Compare(Dpc701BoundingSphere x, Dpc701BoundingSphere y)
        {
            if (x.boundingSphere.Radius > y.boundingSphere.Radius)
            {
                return 1;
            }
            else if (x.boundingSphere.Radius < y.boundingSphere.Radius)
            {
                return -1;
            }
            return 0;
        }
    }
}
