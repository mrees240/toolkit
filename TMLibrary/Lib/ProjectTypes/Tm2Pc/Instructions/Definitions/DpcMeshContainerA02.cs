﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcMeshContainerA02 : Instruction
    {
        private ushort parameter0;
        private int parameter1, parameter2, parameter3, parameter4;
        private byte parameter5, parameter6, parameter7;

        public DpcMeshContainerA02(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 20;
            isMeshContainer = true;
        }

        public DpcMeshContainerA02(ProjectDefinition project, long address, ushort parameter0, int parameter1, int parameter2, int parameter3, int parameter4) : this(project, address)
        {
            this.parameter0 = parameter0;
            this.parameter1 = parameter1;
            this.parameter2 = parameter2;
            this.parameter3 = parameter3;
            this.parameter4 = parameter4;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            parameter0 = reader.ReadUInt16();
            parameter1 = reader.ReadInt32();
            parameter2 = reader.ReadInt32();
            parameter3 = reader.ReadInt32();
            childAmount = reader.ReadByte();
            parameter5 = reader.ReadByte();
            parameter6 = reader.ReadByte();
            parameter7 = reader.ReadByte();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0xA02);
            writer.Write(parameter0);
            writer.Write(parameter1);
            writer.Write(parameter2);
            writer.Write(parameter3);
            WriteChildAmount(writer);
            writer.Write(parameter5);
            writer.Write(parameter6);
            writer.Write(parameter7);
        }
    }
}
