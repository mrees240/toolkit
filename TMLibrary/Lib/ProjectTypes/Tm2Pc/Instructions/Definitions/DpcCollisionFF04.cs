﻿using System.IO;
using System.Numerics;
using TMLibrary.Lib.Collision;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcCollisionFF04 : Instruction
    {
        private uint header0;
        private uint header1;
        private uint unknown0;
        private uint unknown1;
        private ConvexVolume convexVolume;

        public DpcCollisionFF04(ProjectDefinition project, long address) : base(project, address)
        {
            convexVolume = new ConvexVolume();
            bufferSize = 112;
            Header1 = 0xFF04;
        }

        public uint Header1 { get => header1; set => header1 = value; }
        public ConvexVolume ConvexVolume { get => convexVolume; }

        public override string GetInstructionText()
        {
            string text = $"Collision Volume {header1.ToString("X")} {unknown0} {unknown1} ";

            foreach (var plane in convexVolume.Planes)
            {
                text += $"\n\t{plane.D} {plane.Normal.X} {plane.Normal.Y} {plane.Normal.Z}";
            }

            return text;
            //throw new NotImplementedException();
        }

        private Vector3 ReadNormal(BinaryReader reader)
        {
            float maxNormal = 4096.0f;
            float x = reader.ReadInt32() / maxNormal;
            float y = reader.ReadInt32() / maxNormal;
            float z = reader.ReadInt32() / maxNormal;

            return new Vector3(x, y, z);
        }

        private void WriteNormal(Plane plane, BinaryWriter writer)
        {
            var normal = plane.Normal;

            float maxNormal = 4096.0f;
            float x = normal.X * maxNormal;
            float y = normal.Y * maxNormal;
            float z = normal.Z * maxNormal;

            writer.Write((int)x);
            writer.Write((int)y);
            writer.Write((int)z);
        }

        private void WriteNormal(Vector3 normal, BinaryWriter writer)
        {
            float maxNormal = 4096.0f;
            float x = (float)normal.X * maxNormal;
            float y = (float)normal.Y * maxNormal;
            float z = (float)normal.Z * maxNormal;

            writer.Write((int)x);
            writer.Write((int)y);
            writer.Write((int)z);
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            reader.BaseStream.Position += 2;
            header1 = reader.ReadUInt32();
            unknown0 = reader.ReadUInt32();
            unknown1 = reader.ReadUInt32();

            ReadConvexVolume(reader);
        }

        private void ReadConvexVolume(BinaryReader reader)
        {
            var v0 = ReadNormal(reader);
            var v1 = ReadNormal(reader);
            var v2 = ReadNormal(reader);
            int d0 = reader.ReadInt32();
            int d1 = reader.ReadInt32();
            int d2 = reader.ReadInt32();
            var v3 = ReadNormal(reader);
            var v4 = ReadNormal(reader);
            var v5 = ReadNormal(reader);
            int d3 = reader.ReadInt32();
            int d4 = reader.ReadInt32();
            int d5 = reader.ReadInt32();

            convexVolume.Planes[0].Normal = v0;
            convexVolume.Planes[0].D = d0;
            convexVolume.Planes[1].Normal = v1;
            convexVolume.Planes[1].D = d1;
            convexVolume.Planes[2].Normal = v2;
            convexVolume.Planes[2].D = d2;
            convexVolume.Planes[3].Normal = v3;
            convexVolume.Planes[3].D = d3;
            convexVolume.Planes[4].Normal = v4;
            convexVolume.Planes[4].D = d4;
            convexVolume.Planes[5].Normal = v5;
            convexVolume.Planes[5].D = d5;
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write(0xFF08);
            writer.Write(header1);
            writer.Write(unknown0);
            writer.Write(unknown1);

            WriteConvexVolume(writer);
        }

        private void WriteConvexVolume(BinaryWriter writer)
        {
            WriteNormal(convexVolume.Planes[0], writer);
            WriteNormal(convexVolume.Planes[1], writer);
            WriteNormal(convexVolume.Planes[2], writer);
            WritePlaneDistance(convexVolume.Planes[0], writer);
            WritePlaneDistance(convexVolume.Planes[1], writer);
            WritePlaneDistance(convexVolume.Planes[2], writer);
            WriteNormal(convexVolume.Planes[3], writer);
            WriteNormal(convexVolume.Planes[4], writer);
            WriteNormal(convexVolume.Planes[5], writer);
            WritePlaneDistance(convexVolume.Planes[3], writer);
            WritePlaneDistance(convexVolume.Planes[4], writer);
            WritePlaneDistance(convexVolume.Planes[5], writer);
        }

        private void WritePlaneDistance(Plane plane, BinaryWriter writer)
        {
            writer.Write((int)plane.D);
        }
    }
}
