﻿using System;
using System.IO;
using System.Numerics;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Util;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcMatrix : Instruction
    {
        private ushort parameter0;
        private byte parameter1, parameter2, parameter3;

        public DpcMatrix(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 56;
        }

        public DpcMatrix(ProjectDefinition project, long address, Matrix4x4 matrix) : this(project, address)
        {
            this.matrix.Matrix = matrix;
        }

        public override string GetInstructionText()
        {
            return $"Dpc Matrix X: {matrix.Matrix.Translation.X} Y: {matrix.Matrix.Translation.Y} Z: {matrix.Matrix.Translation.Z}";
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            parameter0 = reader.ReadUInt16();
            matrix.Matrix = Utility.ParseXnaMatrix(reader);
            parameter1 = reader.ReadByte();
            parameter2 = reader.ReadByte();
            childAmount = reader.ReadByte();
            parameter3 = reader.ReadByte();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0xB05);
            writer.Write(parameter0);
            writer.Write((int)(MatrixWrapper.Matrix.M11 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M12 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M13 * 4096.0));

            writer.Write((int)(MatrixWrapper.Matrix.M21 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M22 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M23 * 4096.0));

            writer.Write((int)(MatrixWrapper.Matrix.M31 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M32 * 4096.0));
            writer.Write((int)(MatrixWrapper.Matrix.M33 * 4096.0));

            writer.Write((int)MatrixWrapper.Matrix.Translation.X);
            writer.Write((int)MatrixWrapper.Matrix.Translation.Y);
            writer.Write((int)MatrixWrapper.Matrix.Translation.Z);

            writer.Write(parameter1);
            writer.Write(parameter2);
            WriteChildAmount(writer);
            writer.Write(parameter3);
        }
    }
}
