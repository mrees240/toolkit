﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcFF07 : Instruction
    {
        public DpcFF07(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 72;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            buffer = reader.ReadBytes(70);
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0xFF07);
            writer.Write(buffer);
        }
    }
}
