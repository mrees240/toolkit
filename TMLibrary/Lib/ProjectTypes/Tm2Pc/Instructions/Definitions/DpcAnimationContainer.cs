﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcAnimationContainer : Instruction
    {
        public DpcAnimationContainer(ProjectDefinition project, long address) : base(project, address)
        {
            isAnimationContainer = true;
            bufferSize = 32;
            buffer = new byte[30];
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            buffer = reader.ReadBytes(30);
            childAmount = buffer[9];
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            long start = writer.BaseStream.Position;
            writer.Write((ushort)0x903);
            writer.Write(buffer);
            long end = writer.BaseStream.Position;
            writer.BaseStream.Position = start + 11;
            writer.Write((byte)GetAllMeshes().Count);
            writer.BaseStream.Position = end;
        }
    }
}
