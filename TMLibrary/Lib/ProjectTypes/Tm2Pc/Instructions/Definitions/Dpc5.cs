﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class Dpc5 : Instruction
    {

        public Dpc5(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 40;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            buffer = reader.ReadBytes(38);
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x5);
            writer.Write(buffer);
        }
    }
}
