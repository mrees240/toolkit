﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.ProjectTypes.Tm2Pc.Instructions.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcSubEntity509 : Instruction
    {
        private EntityType entityType;
        private ushort parameter1;
        private byte parameter2;
        private uint parameter3, parameter4;

        public DpcSubEntity509(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 16;
        }

        public DpcSubEntity509(ProjectDefinition project, long address, EntityType entityType, ushort parameter1, byte parameter2, uint parameter3, uint parameter4) : this(project, address)
        {
            this.entityType = entityType;
            this.parameter1 = parameter1;
            this.parameter2 = parameter2;
            this.parameter3 = parameter3;
            this.parameter4 = parameter4;
        }

        public ushort Parameter1 { get => parameter1; set => parameter1 = value; }
        public EntityType EntityType { get => entityType; set => entityType = value; }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            entityType = (EntityType)reader.ReadUInt16();
            parameter1 = reader.ReadUInt16();
            childAmount = reader.ReadByte();
            parameter2 = reader.ReadByte();
            parameter3 = reader.ReadUInt32();
            parameter4 = reader.ReadUInt32();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x509);
            writer.Write((ushort)entityType);
            writer.Write(parameter1);
            WriteChildAmount(writer);
            writer.Write(parameter2);
            writer.Write(parameter3);
            writer.Write(parameter4);
        }
    }
}
