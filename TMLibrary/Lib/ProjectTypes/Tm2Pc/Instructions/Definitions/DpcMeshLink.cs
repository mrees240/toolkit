﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    /// <summary>
    /// An instruction that includes a near and far draw distance limit for a mesh or instruction.
    /// </summary>
    public class DpcMeshLink : Instruction
    {
        public DpcMeshLink(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 8;
            childAmount = 1;
            isMeshLink = true;
        }

        public DpcMeshLink(ProjectDefinition project, long address, uint farDrawDistance, uint nearDrawDistance) : this(project, address)
        {
            this.farDrawDistance = farDrawDistance;
            this.nearDrawDistance = nearDrawDistance;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            reader.BaseStream.Position -= sizeof(ushort);
            farDrawDistance = reader.ReadUInt32();
            nearDrawDistance = reader.ReadUInt32();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write(farDrawDistance);
            writer.Write(nearDrawDistance);
        }
    }
}
