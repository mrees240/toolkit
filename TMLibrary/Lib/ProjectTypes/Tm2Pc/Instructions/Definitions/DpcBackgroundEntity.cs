﻿using System;
using System.IO;
using System.Numerics;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcBackgroundEntity : Instruction
    {
        private ushort unknown0;
        private byte unknown1;
        private byte unknown2;
        private byte unknown3;

        public DpcBackgroundEntity(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 8;
            MatrixWrapper.Matrix = Matrix4x4.CreateScale(4.0f);
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            unknown0 = reader.ReadUInt16();
            unknown1 = reader.ReadByte();
            unknown2 = reader.ReadByte();
            childAmount = reader.ReadByte();
            unknown3 = reader.ReadByte();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x30C);
            writer.Write(unknown0);
            writer.Write(unknown1);
            writer.Write(unknown2);
            writer.Write(childAmount);
            writer.Write(unknown3);
        }
    }
}
