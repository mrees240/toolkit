﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class Dpc6 : Instruction
    {
        public Dpc6(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 100;
        }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            buffer = reader.ReadBytes(98);
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x6);
            writer.Write(buffer);
        }
    }
}
