﻿using System.IO;
using System.Numerics;
using MathNet.Numerics.LinearAlgebra.Complex;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class DpcTranslationMatrix : Instruction
    {
        private byte parameter0, parameter1, parameter2;

        public DpcTranslationMatrix(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 20;
        }

        public DpcTranslationMatrix(ProjectDefinition project, long address, MatrixType matrixType, int xPos, int yPos, int zPos,
            byte parameter0, byte parameter1, byte parameter2) : this(project, address)
        {
            var translation = Matrix4x4.CreateTranslation(xPos, yPos, zPos);
            this.matrix.Matrix = translation;
            this.matrix.MatrixType = matrixType;
            this.parameter0 = parameter0;
            this.parameter1 = parameter1;
            this.parameter2 = parameter2;
        }

        public DpcTranslationMatrix(ProjectDefinition project, long address, MatrixType type,
            Vector3 translation, byte parameter0, byte parameter1, byte parameter2) : this(project, 0, type, (int)translation.X, (int)translation.Y, (int)translation.Z,
                parameter0, parameter1, parameter2)
        {

        }

        public override string GetInstructionText()
        {
            return $"Dpc Translation Matrix X: {matrix.Matrix.Translation.X} Y: {matrix.Matrix.Translation.Y} Z: {matrix.Matrix.Translation.Z}";
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            MatrixWrapper.MatrixType = (MatrixType)reader.ReadUInt16();
            MatrixWrapper.Matrix = Matrix4x4.CreateTranslation(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
            parameter0 = reader.ReadByte();
            parameter1 = reader.ReadByte();
            childAmount = reader.ReadByte();
            parameter2 = reader.ReadByte();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            var translation = matrix.Matrix.Translation;
            writer.Write((ushort)0xB04);
            writer.Write((ushort)MatrixWrapper.MatrixType);
            writer.Write((int)translation.X);
            writer.Write((int)translation.Y);
            writer.Write((int)translation.Z);
            writer.Write(parameter0);
            writer.Write(parameter1);
            WriteChildAmount(writer);
            writer.Write(parameter2);
        }
    }
}
