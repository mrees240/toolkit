﻿using System;
using System.IO;
using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions.Dpc.Definitions
{
    public class Dpc50B : Instruction
    {
        private ushort unknown0;
        private uint unknown1;

        public Dpc50B(ProjectDefinition project, long address) : base(project, address)
        {
            bufferSize = 8;
            childAmount = 3;
        }

        public ushort Unknown0 { get => unknown0; set => unknown0 = value; }
        public uint Unknown1 { get => unknown1; set => unknown1 = value; }

        public override string GetInstructionText()
        {
            throw new NotImplementedException();
        }

        protected override void ReadInstructionContent(BinaryReader reader)
        {
            unknown0 = reader.ReadUInt16();
            unknown1 = reader.ReadUInt32();
        }

        protected override void WriteInstructionContent(BinaryWriter writer)
        {
            writer.Write((ushort)0x50B);
            writer.Write(unknown0);
            writer.Write(unknown1);
        }
    }
}
