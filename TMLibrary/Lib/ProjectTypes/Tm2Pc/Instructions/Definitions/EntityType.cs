﻿namespace TMLibrary.Lib.ProjectTypes.Tm2Pc.Instructions.Definitions
{
    public enum EntityType
    {
        Default = 0x0, ExplodeOnImpact = 0x208, Destructable = 0x01F6
    }
}
