﻿using ColladaLibrary.Render;
using System;
using System.IO;
using System.Linq;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Util;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.MeshWriters
{
    /// <summary>
    ///     A mesh writer for TM2 on PC. Allows you to add custom meshes to a DPC file.
    /// </summary>
    public class Tm2PcMeshWriter : IMeshWriter
    {
        private readonly ProjectDefinition projectDefinition;
        private readonly Tm2PcMeshTemplates meshTemplates;

        public readonly byte[] HeaderTemplate =
            {
            0x00, 0xFF, 0x00, 0x00, 0xC0, 0xB5, 0x01, 0x80,
            0xF0, 0xF6, 0x01, 0x80, 0xE0, 0x17, 0x00, 0x80,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00
        };

        public Tm2PcMeshWriter(ProjectDefinition projectDefinition)
        {
            meshTemplates = new Tm2PcMeshTemplates();
            this.projectDefinition = projectDefinition;
        }

        public void WriteMeshToFile(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            /*if (mesh.Polygons.Count > 0)
            {
                var normalPolys = new List<Polygon>();
                var subPolys = new List<Polygon>();
                for (int i = 0; i < mesh.Polygons.Count; i++)
                {
                    var poly = mesh.Polygons[i];

                    if (poly.SubPolygons.Count == 0)
                    {
                        normalPolys.Add(poly);
                    }
                    else
                    {
                        subPolys.AddRange(poly.SubPolygons);
                    }
                }
                mesh.Polygons = new List<Polygon>(normalPolys);
                mesh.Polygons.AddRange(subPolys);

                if (!mesh.IsColladaBillboard && mesh.Node != null && !mesh.Node.IsCollision)
                {
                    mesh.Subdivide(9000.0f);
                }
            }*/

            if (mesh.WrittenToFile)
            {
                var firstPolygonAddress = mesh.MeshHeader.PolygonStartAddress;
                writer.BaseStream.Position = firstPolygonAddress;

                foreach (var polygon in mesh.OriginalPolygons)
                {
                    WritePolygon(writer, polygon);
                }
                writer.BaseStream.Position = mesh.MeshHeader.VertexStartAddress;
                WriteVertices(writer, mesh);
            }
            else
            {
                WriteNewMesh(writer, mesh);
            }
        }

        private void WritePolygonCount(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            writer.Write((ushort) mesh.GetPolygonAmount());
        }

        /*public void WriteMeshHeaderTag(BinaryWriter writer)
        {
            writer.Write(projectDefinition.MeshFile.Definition.MeshHeaderTag);
        }*/

        private void WritePolygon(BinaryWriter writer, GamePolygon polygon)
        {
            var polygonController = projectDefinition.MeshFile.Definition.PolygonController;
            polygonController.Write(writer, polygon);
        }

        public void WriteNewMesh(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            /*var pointerLength = 4;
            mesh.Address = writer.BaseStream.Center + pointerLength;
            var ptr = new Pointer(writer.BaseStream.Center, mesh.Address);
            writer.Write(Utility.CreatePointer(ptr.PointToAddress, mesh.ParentFile.Definition));

            writer.BaseStream.Center += mesh.MeshHeader.BufferSize;*/
            //mesh.MeshHeader.WriteMeshHeader(writer);
            //writer.BaseStream.Center = mesh.MeshHeader.VertexStartAddress;

            mesh.MeshHeader.VertexStartAddress = writer.BaseStream.Position;
            WriteVertices(writer, mesh);

            //mesh.MeshHeader.PolygonStartAddress = writer.BaseStream.Center;
            //writer.BaseStream.Center = mesh.MeshHeader.PolygonStartAddress;
            mesh.MeshHeader.PolygonStartAddress = writer.BaseStream.Position;
            WriteNewMeshPolygons(writer, mesh);

            //var endAddress = writer.BaseStream.Center;
            //writer.BaseStream.Center = mesh.Address;
            mesh.Address = writer.BaseStream.Position;
            writer.Write(HeaderTemplate);
            writer.BaseStream.Position = mesh.Address;
            mesh.MeshHeader.WriteMeshHeader(writer);

            mesh.WrittenToFile = true;
        }
        private void WriteNewMeshPolygons(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            var allPolys = mesh.Polygons;
            mesh.MeshHeader.PolygonAmount = (ushort)allPolys.Count();

            foreach (var polygon in allPolys)
            {
                WritePolygon(writer, polygon);
            }
        }

        private void WritePrevPolygons(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            var allPolys = mesh.OriginalPolygons.Where(p => p.IsWritable());
            mesh.MeshHeader.PolygonAmount = (ushort)allPolys.Count();

            foreach (var polygon in allPolys)
            {
                WritePolygon(writer, polygon);
            }
        }

        /// <summary>
        ///     Writes the ID number of the polygon's texture.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="polygon"></param>
        private void WriteTextureId(BinaryWriter writer, GamePolygon polygon, int idNumber)
        {
            if (polygon.TextureFile != null)
            {
                writer.Write((ushort) (16384 + idNumber));
            }
            else
            {
                throw new Exception("Missing texture file.");
            }
        }

        /// <summary>
        ///     Writes the index values used to identify the correct vertices needed by the polygon.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="writer"></param>
        private void WriteIndexValues(GamePolygon polygon, BinaryWriter writer)
        {
            var indices = polygon.GetIndices();

            for (var iPoly = 0; iPoly < indices.Length; iPoly++)
            {
                var index = (ushort) indices[iPoly].Id;
                writer.Write(index);
            }
            if (polygon.HasThreeIndices())
            {
                writer.Write((short) 0);
            }
        }

        /// <summary>
        ///     Writes the cross product of a polygon's face within the range of -4096 to 4096.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="polygon"></param>
        private void WriteCrossProduct(BinaryWriter writer, GamePolygon polygon)
        {
            var normal = Utility.FindCrossProduct(polygon);
            var crossProductMax = 4096.0;
            writer.Write((short) (normal.X * crossProductMax));
            writer.Write((short) (normal.Y * crossProductMax));
            writer.Write((short) (normal.Z * crossProductMax));
        }

        /*private void WriteNewMeshTextureCoordinates(BinaryWriter writer, Polygon polygon)
        {
            foreach (var texCoord in polygon.TexCoords)
            {
                var s = texCoord.S;
                var t = texCoord.T;

                s *= polygon.TextureFile.ActualWidth;
                t *= polygon.TextureFile.Height;

                writer.Write((short) s);
                writer.Write((short) t);
            }
        }*/

        private void WriteVertex(Vertex vertex, BinaryWriter writer)
        {
            writer.Write((short) vertex.Vector.X);
            writer.Write((short) vertex.Vector.Y);
            writer.Write((short) vertex.Vector.Z);
            writer.Write((ushort) 0);
        }

        public void WriteVertices(BinaryWriter writer, Rendering.GameMesh mesh)
        {
            int vertexStride = mesh.ParentFile.Definition.VertexStride;

            foreach (var vertexEntry in mesh.Vertices)
            {
                if (vertexEntry != null)
                {
                    WriteVertex(vertexEntry, writer);
                }
                else
                {
                    writer.BaseStream.Position += vertexStride;
                }
            }
        }
    }
}