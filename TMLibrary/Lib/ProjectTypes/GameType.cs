﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLibrary.Lib.ProjectTypes
{
    public enum GameType
    {
        TM2PC, TM1PC, JM1PC
    }
}
