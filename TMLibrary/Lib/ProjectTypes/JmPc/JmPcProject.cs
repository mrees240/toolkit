﻿using System.Drawing;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Instructions.Dpc;
using TMLibrary.Textures;

namespace TMLibrary.Lib.ProjectTypes
{
    /// <summary>
    ///     Defines the project type definition for JM on PC.
    /// </summary>
    public class JmPcProject : IProjectTypeCreator
    {
        public ProjectDefinition CreateProjectType()
        {
            /*var project = new ProjectDefinition();
            project.MeshFile = new MeshArchiveFile(CreateMeshArchiveDefinition(), project);
            project.TextureArchiveFile = new TextureArchiveFile(CreateTextureArchiveDefinition());
            project.MeshWriter = new Tm2PcMeshWriter(project);
            project.MeshReader = new Tm2PcMeshReader();
            project.FileWriter = new Tm2FileWriter();
            project.Saveable = true;

            return project;*/
            return null;
        }

        /*private void SetTextureDiffuse(Dictionary<uint, PolygonDefinition> polygonDefinitions)
        {
            polygonDefinitions[0x90D0303].IgnoreDiffuseValues = false;
            polygonDefinitions[0x90D0303].HasMultipleTextureDiffuseValues = true;
        }

        private Dictionary<uint, PolygonDefinition> CreatePolygonDefinitions()
        {
            var polygonDefinitions = new Dictionary<uint, PolygonDefinition>();
            polygonDefinitions.Add(0x04080100, new PolygonDefinition(32, 3, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x06090100, new PolygonDefinition(36, 3, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x06090300, new PolygonDefinition(36, 3, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x060A0300, new PolygonDefinition(40, 3, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x060B0300, new PolygonDefinition(44, 3, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x05080100, new PolygonDefinition(32, 4, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x08090100, new PolygonDefinition(36, 4, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x04070100, new PolygonDefinition(28, 4, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x05070100, new PolygonDefinition(28, 4, 0, 0, 24, true)); // done
            polygonDefinitions.Add(0x070B0103, new PolygonDefinition(44, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x070C0103, new PolygonDefinition(48, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x070C0183, new PolygonDefinition(48, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x070D0183, new PolygonDefinition(52, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090D0103, new PolygonDefinition(52, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090D0303, new PolygonDefinition(52, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090E0183, new PolygonDefinition(56, 3, 28, 28 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090E0303, new PolygonDefinition(56, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090E0383, new PolygonDefinition(56, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090F0303, new PolygonDefinition(60, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090F0383, new PolygonDefinition(60, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x09100383, new PolygonDefinition(64, 3, 36, 36 + 12, 0, false)); // done
            polygonDefinitions.Add(0x090D0104, new PolygonDefinition(52, 4, 28, 28 + 16, 0, false)); // done
            polygonDefinitions.Add(0x090D0184, new PolygonDefinition(52, 4, 28, 28 + 16, 0, false)); // done
            polygonDefinitions.Add(0x090E0184, new PolygonDefinition(56, 4, 28, 28 + 16, 0, false)); // done
            polygonDefinitions.Add(0x0C0E0104, new PolygonDefinition(56, 4, 28, 28 + 16, 0, false)); // done
            polygonDefinitions.Add(0x0C0F0184, new PolygonDefinition(60, 4, 28, 28 + 16, 0, false)); // done
            polygonDefinitions.Add(0x090C0104, new PolygonDefinition(48, 4, 28, 28 + 16, 0, false)); // done

            return polygonDefinitions;
        }*/

        private TextureArchiveDefinition CreateTextureArchiveDefinition()
        {
            var nonTransparentTimHeader = new TimHeaderFormat(new byte[] { 217, 0x00, 0x00, 0x00 }, 8, false, true);
            var transparentTimHeader = new TimHeaderFormat(new byte[] { 89, 0x00, 0x00, 0x00 }, 8, true, true);

            return new TextureArchiveDefinition
            {
                DefaultPixelFormat = System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                FileSignature = "TCPMC*****U4",
                FileExtension = "TPC",
                RequiresPowerOfTwoDimensions = true,
                HasMaxArea = false,
                TransparencyColor = Color.Black,
                FlipRgb = true,
                FirstTimAddress = 16,
                TransparentTimHeader = transparentTimHeader,
                NonTransparentTimHeader = nonTransparentTimHeader,
                TimHeaderFormats = new TimHeaderFormat[]
                {
                    new TimHeaderFormat(new byte[] { 25, 0x00, 0x00, 0x00 }, 8, false, true),
                    new TimHeaderFormat(new byte[] { 9, 0x00, 0x00, 0x00 }, 8, false, true),
                    nonTransparentTimHeader,
                    transparentTimHeader
                }
            };
        }

        private MeshArchiveDefinition CreateMeshArchiveDefinition()
        {
            var meshArchiveDefinition = new MeshArchiveDefinition
            {
                FileSignature = "DCPMC******4",
                FileExtension = "DPC",
                //MeshHeaderLength = 44,
                MeshHeaderTag = 0xFF00,
                VertexStride = 8
            };

            //meshArchiveDefinition.PolygonDefinitions = CreatePolygonDefinitions();
            meshArchiveDefinition.PointerDefinitions = CreatePointerDefinitions();
            /*meshArchiveDefinition.CustomPolygonTexturedHeader = 0x090F0303;
            meshArchiveDefinition.CustomPolygonColorHeader = 0x06090100;
            meshArchiveDefinition.CustomQuadColorHeader = 0x08090100;
            meshArchiveDefinition.CustomQuadTexturedHeader = 0x090D0184;*/
            meshArchiveDefinition.InstructionParser = new DpcInstructionParser("TM2PC");
            //SetTextureDiffuse(meshArchiveDefinition.PolygonDefinitions);

            return meshArchiveDefinition;
        }

        private PointerDefinition[] CreatePointerDefinitions()
        {
            var pointerDefinitions = new PointerDefinition[500];
            ushort pointerStartTag = 0x8001;

            for (ushort iPointer = 0; iPointer < pointerDefinitions.Length; iPointer++)
            {
                var currentTag = (ushort)(pointerStartTag + iPointer);

                if (currentTag > pointerStartTag)
                {
                    var offset = (ushort)(currentTag - 0x8002);
                    long pointerOperation = 25535 + offset * ushort.MaxValue + (currentTag - pointerStartTag);
                    long minAddress = pointerDefinitions[iPointer - 1].MaxAddress + 1;
                    var maxAddress = minAddress + ushort.MaxValue;
                    pointerDefinitions[iPointer] =
                        new PointerDefinition(currentTag, pointerOperation, minAddress, maxAddress);
                    maxAddress += iPointer;
                }
                else
                {
                    pointerDefinitions[iPointer] = new PointerDefinition(currentTag, -40000, 0, 25534);
                }
            }

            return pointerDefinitions;
        }
    }
}