﻿using System.Numerics;

namespace TMLibrary.Lib.Collision
{
    public class ConvexVolume
    {
        private Plane[] planes;

        public ConvexVolume()
        {
            InitializePlanes();
        }

        private void InitializePlanes()
        {
            planes = new Plane[6];
            for (int iPlane = 0; iPlane < planes.Length; iPlane++)
            {
                planes[iPlane] = new Plane();
            }
        }

        public Plane[] Planes { get => planes; set => planes = value; }
    }
}
