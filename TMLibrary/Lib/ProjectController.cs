﻿using ColladaLibrary.Misc;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using TMLibrary.Lib.Definitions;
using TMLibrary.Lib.Exceptions;
using TMLibrary.Rendering;
using TMLibrary.Textures;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     Provides a controller for loading and parsing a game file using a project definition.
    /// </summary>
    public class ProjectController
    {
        private readonly ProjectDefinition project;

        public ProjectController(ProjectDefinition project)
        {
            this.project = project;
        }

        public void AddMesh(GameMesh mesh, PixelFormat textureFormat)
        {
            project.MeshFile.Meshes.Add(mesh);
        }

        public void LoadProject(string fileName, string safeFileName)
        {
            var meshFileExtension = project.MeshFile.Definition.FileExtension;
            var textureFileExtension = project.TextureArchiveFile.Definition.FileExtension;
            var textureFileName = RemoveFileExtension(fileName);
            textureFileName += textureFileExtension;
            project.MeshArchiveFileName = fileName;
            project.MeshArchiveSafeFileName = safeFileName;
            project.TextureArchiveFileName = textureFileName;
            project.TextureArchiveSafeFileName = safeFileName.Replace(
                project.TextureArchiveFile.Definition.FileExtension,
                project.MeshFile.Definition.FileExtension);
            var textureArchiveController = new TextureArchiveController(project.TextureArchiveFile);
            var meshArchiveController = new MeshArchiveController(project.MeshFile, project);
            if (!project.SkipTextures)
            {
                textureArchiveController.ReadFile(textureFileName);
            }
            meshArchiveController.ReadFile(fileName);
        }

        private string RemoveFileExtension(string fileName)
        {
            return fileName.Remove(fileName.Length - 3, 3);
        }

        /// <summary>
        /// Loads a texture file from an external file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public TextureFile LoadTextureFile(string fileName)
        {
            var filePair = new BitmapFilePair("", new Bitmap(2, 2));
            var textureFile = new TextureFile(project.TextureArchiveFile, filePair);
            var extension = fileName.Split('.').LastOrDefault();

            if (extension != null)
            {
                extension = extension.ToUpper();

                if (extension == "BMP")
                {
                    new TextureFileController(textureFile).ReadFromBitmapFile(fileName);
                }
                else if (extension == "TIM")
                {
                    new TextureFileController(textureFile).ReadFromExternalTimFile(fileName,
                        project.TextureArchiveFile.TextureFiles);
                }
                else
                {
                    throw new InvalidTextureFileFormatException(extension);
                }
                return textureFile;
            }
            throw new InvalidTextureFileFormatException("unknown");
        }

        public void ReplaceTexture(string newFileName, TextureFile prevFile)
        {
            var newFile = LoadTextureFile(newFileName);
            new TextureArchiveController(project.TextureArchiveFile).ReplaceTextureFile(newFile, prevFile);
        }

        /// <summary>
        /// Saves the mesh archive file and texture archive file.
        /// </summary>
        public void AttemptToSaveProject()
        {
            if (!project.SkipTextures)
            {
                var textureArchiveController = new TextureArchiveController(project.TextureArchiveFile);
                textureArchiveController.WriteFile(project.TextureArchiveFileName);
            }
            var meshArchiveController = new MeshArchiveController(project.MeshFile, project);
            meshArchiveController.WriteFile(project);
        }
    }
}