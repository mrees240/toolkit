﻿using TMLibrary.Lib.Definitions;

namespace TMLibrary.Lib.Instructions
{
    /// <summary>
    /// Used to compile project files.
    /// </summary>
    public interface IFileWriter
    {
        void WriteFiles(string fileName, ProjectDefinition project);
    }
}
