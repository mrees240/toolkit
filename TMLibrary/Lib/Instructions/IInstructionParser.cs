﻿using System.IO;
using TMLibrary.Rendering;

namespace TMLibrary.Lib.Instructions
{
    /// <summary>
    ///     A basic interface for parsing instructions for different file formats used by the engine.
    ///     Instructions are stored in mesh archive files.
    /// </summary>
    public interface IInstructionParser
    {
        void ParseInstructions(MeshArchiveFile meshFile, BinaryReader reader);
    }
}