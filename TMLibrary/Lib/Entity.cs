﻿using System.Collections.Generic;
using System.Numerics;
using TMLibrary.Rendering;

namespace TMLibrary.Lib
{
    /// <summary>
    ///     An entity is used to categorize elements of a mesh archive file into a hierarchy.
    /// </summary>
    public class Entity
    {
        public Entity()
        {
            MeshList = new List<GameMesh>();
            Children = new List<Entity>();
        }

        public Entity(Instruction instruction) : this()
        {
            Instruction = instruction;
            MeshList = instruction.MeshList;
        }

        public List<Entity> Children { get; set; }

        public Instruction Instruction { get; }

        public List<GameMesh> MeshList { get; }

        public bool IsMeshLink { get; set; }

        public Matrix4x4 Matrix
        {
            get
            {
                if (Instruction != null)
                {
                    return Instruction.MatrixWrapper.Matrix;
                }
                return Matrix4x4.Identity;
            }
            set => Instruction.MatrixWrapper.Matrix = value;
        }
    }
}