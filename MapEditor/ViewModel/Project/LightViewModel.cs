﻿using ColladaLibrary.Render.Lighting;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.ViewModel.Project
{
    public class LightViewModel : ViewModelBase
    {
        private Light light;

        public LightViewModel(Light light)
        {
            this.light = light;
            light.UpdateDirection();
        }


        public float XRotDegrees
        {
            get
            {
                return light.XRotDegrees;
            }
            set
            {
                light.XRotDegrees = value;
                light.UpdateDirection();
                RaisePropertyChanged();
            }
        }

        public float YRotDegrees
        {
            get
            {
                return light.YRotDegrees;
            }
            set
            {
                light.YRotDegrees = value;
                light.UpdateDirection();
                RaisePropertyChanged();
            }
        }

        public float ZRotDegrees
        {
            get
            {
                return light.ZRotDegrees;
            }
            set
            {
                light.ZRotDegrees = value;
                light.UpdateDirection();
                RaisePropertyChanged();
            }
        }

        public float Intensity
        {
            get
            {
                return light.Intensity;
            }
            set
            {
                light.Intensity = value;
                RaisePropertyChanged();
            }
        }

        public byte Red
        {
            get
            {
                return light.Color.R;
            }
            set
            {
                var r = value;
                var g = light.Color.G;
                var b = light.Color.B;
                light.Color = System.Drawing.Color.FromArgb(byte.MaxValue, r, g, b);
                RaisePropertyChanged();
            }
        }

        public byte Green
        {
            get
            {
                return light.Color.G;
            }
            set
            {
                var r = light.Color.R;
                var g = value;
                var b = light.Color.B;
                light.Color = System.Drawing.Color.FromArgb(byte.MaxValue, r, g, b);
                RaisePropertyChanged();
            }
        }

        public byte Blue
        {
            get
            {
                return light.Color.B;
            }
            set
            {
                var r = light.Color.R;
                var g = light.Color.G;
                var b = value;
                light.Color = System.Drawing.Color.FromArgb(byte.MaxValue, r, g, b);
                RaisePropertyChanged();
            }
        }

        public Light Light { get => light; }
    }
}
