﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using GalaSoft.MvvmLight;
using MapEditor.Render;
using MapEditor.Settings;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using MonoEngine.ViewModel;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;
using WpfCore.ViewModel;

namespace MapEditor.ViewModel.Project
{
    public class ObjectInstanceViewModel : ViewModelBase
    {
        private ObjectInstance objectInstance;
        private ObservableCollection<ObjectInstanceViewModel> children;
        private MapEditorProjectViewModel projectVm;
        private GenericSceneObject sceneObject;
        private ObjectInstanceSceneObject childObject;
        private bool isSelected;
        private MouseControllerBase mouseController;
        private DefaultEditorLmbAction editorLmbAction;
        private TransformationViewModel transformationVm;
        private bool translationOnly;
        private MapEditorMeshContainer meshContainer;

        public ObjectInstanceViewModel(ObjectInstance objectInstance, MapEditorMeshContainer meshContainer, MapEditorProjectViewModel projectVm,
            MouseControllerBase mouseController, DefaultEditorLmbAction editorLmbAction, MiscSettings miscSettings) :
            this(objectInstance, projectVm, meshContainer, mouseController, editorLmbAction, 4, miscSettings)
        {
        }

        public ObjectInstanceViewModel(ObjectInstance objectInstance, MapEditorProjectViewModel projectVm,
            MapEditorMeshContainer meshContainer,
            MouseControllerBase mouseController, DefaultEditorLmbAction editorLmbAction, int connectedNodeAmount,
            MiscSettings miscSettings)
        {
            this.meshContainer = meshContainer;
            this.editorLmbAction = editorLmbAction;
            this.mouseController = mouseController;
            this.projectVm = projectVm;
            this.objectInstance = objectInstance;
            children = new ObservableCollection<ObjectInstanceViewModel>();

            childObject = new ObjectInstanceSceneObject(this, meshContainer, miscSettings);
            childObject.GetDrawState().IsVisible = true;
            childObject.GetObjectState().IsGizmoTranslatable = true;

            sceneObject = new GenericSceneObject(this);
            sceneObject.GetObjectState().IsPickable = true;
            sceneObject.GetDrawState().IsVisible = true;
            sceneObject.GetObjectState().IsGizmoTranslatable = true;
            sceneObject.SetTransformation(objectInstance.Transformation);
            sceneObject.AddChild(childObject);
            transformationVm = new TransformationViewModel(objectInstance.Transformation);
            objectInstance.Transformation.TransformationChanged += Transformation_TransformationChanged1;

            foreach (var child in objectInstance.Children)
            {
                children.Add(new ObjectInstanceViewModel(child, meshContainer, projectVm, mouseController, editorLmbAction, miscSettings));
            }

            if (objectInstance.ObjectType == ObjectType.StaticObject)
            {
                objectInstance.StaticObject.MeshChanged += ObjectInstance_StaticObjectMeshChanged;
                AddBillboard(objectInstance.StaticObject.BillboardInfoList);
                sceneObject.GetObjectState().Name = objectInstance.StaticObject.Name;
            }
            if (objectInstance.ObjectType == ObjectType.GameItemObject)
            {
                AddBillboard(objectInstance.GameItemDefinition.BillboardInfoList);
                TranslationOnly = objectInstance.GameItemDefinition.TranslationOnly;
            }

            foreach (var mesh in sceneObject.GetMeshes())
            {
                foreach (var tex in mesh.GetTextures())
                {
                    tex.Diffuse.TransparencyEnabled = true;
                }
            }
            foreach (var mesh in sceneObject.GetBillboards())
            {
                mesh.Key.Texture.TransparencyEnabled = true;
            }

            UpdateSceneObjectMeshes();
            sceneObject.UpdateOccured += SceneObject_UpdateOccured;
        }

        private void AddBillboard(List<BillboardInfo> billboardInfoList)
        {
            sceneObject.AddBillboard(billboardInfoList);
            sceneObject.GetObjectState().PickableRoot = childObject;

            foreach (var billboard in sceneObject.GetBillboards())
            {
                billboard.Value.GetObjectState().PickableRoot = childObject;
                billboard.Value.GetBillboardChild().GetObjectState().PickableRoot = childObject;
            }
        }

        private void ObjectInstanceViewModel_PickActivated(object sender, EventArgs e)
        {
            childObject.GetObjectState().OnPickActivated();
            projectVm.SelectedObjectInstanceVm = this;
        }

        private void Transformation_TransformationChanged1(object sender, EventArgs e)
        {
            transformationVm.UpdateTransformationInViewModel();
        }

        private void SceneObject_UpdateOccured(object sender, EventArgs e)
        {
            UpdateVisibility();
            transformationVm.UpdateTransformationInViewModel();
        }

        private void UpdateVisibility()
        {
            var billboards = sceneObject.GetBillboards();

            if (objectInstance.ObjectType == ObjectType.StaticObject)
            {
                foreach (var billboard in billboards)
                {
                    billboard.Value.GetDrawState().IsVisible = objectInstance.StaticObject.IsBillboard;
                }
                childObject.GetDrawState().IsVisible = !objectInstance.StaticObject.IsBillboard;
            }
        }

        public void UpdateSceneBillboards()
        {
            sceneObject.RemoveBillboards();
            if (objectInstance.ObjectType == ObjectType.StaticObject)
            {
                AddBillboard(objectInstance.StaticObject.BillboardInfoList);
            }
            if (objectInstance.ObjectType == ObjectType.GameItemObject)
            {
                AddBillboard(objectInstance.GameItemDefinition.BillboardInfoList);
            }
        }

        private void StaticObject_BillboardsChanged(object sender, EventArgs e)
        {
        }

        private void ObjectInstance_StaticObjectMeshChanged(object sender, EventArgs e)
        {
            UpdateSceneObjectMeshes();
        }

        private void UpdateSceneObjectMeshes()
        {
            if (objectInstance.ObjectType == ObjectType.StaticObject)
            {
                childObject.SetMeshes(objectInstance.StaticObject.Meshes);
            }
            else if (objectInstance.ObjectType == ObjectType.GameItemObject)
            {
                childObject.SetMeshes(objectInstance.GameItemDefinition.Meshes);
            }
            else if (objectInstance.ObjectType == ObjectType.PathNode)
            {
                childObject.SetMeshes(new List<Mesh>() { meshContainer.PathNodeMesh });
            }
        }

        public void RemoveAllChildren()
        {
            while (children.Any())
            {
                RemoveChild(children.Last());
            }
        }

        public void RemoveChild(ObjectInstanceViewModel objectInstanceVm)
        {
            children.Remove(objectInstanceVm);
            ObjectInstance.RemoveChild(ObjectInstance);
        }

        public void AddChild(ObjectInstanceViewModel objectInstanceVm)
        {
            children.Add(objectInstanceVm);
            ObjectInstance.AddChild(objectInstanceVm.ObjectInstance);
        }

        public int ChildrenAmount
        {
            get
            {
                return children.Count;
            }
        }

        public ObjectInstanceViewModel GetChildVm(int id)
        {
            return children[id];
        }

        public ObjectInstance ObjectInstance
        {
            get { return objectInstance; }
        }

        public string Name
        {
            get { return objectInstance.Name; }
        }

        public GenericSceneObject SceneObject { get => sceneObject; }
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                RaisePropertyChanged();
            }
        }

        public bool RotationEnabled
        {
            get
            {
                return !TranslationOnly;
            }
        }

        public TransformationViewModel TransformationVm { get => transformationVm; }
        public bool TranslationOnly { get => translationOnly; set { translationOnly = value; RaisePropertyChanged(); } }

        public ObjectInstanceSceneObject ChildObject { get => childObject; }

        public bool MaxConnectionsReached()
        {
            int emptyConnectionSlots = ObjectInstance.ConnectedNodes.Where(c => c == null).Count();
            return emptyConnectionSlots == 0;
        }
        public string GetObjectName()
        {
            return objectInstance.Name;
        }

        public void SetDefaultTransformationVm()
        {
            transformationVm = new TransformationViewModel(new Transformation());
        }
    }
}
