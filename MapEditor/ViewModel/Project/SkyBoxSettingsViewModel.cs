﻿using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using MapEditor.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.ViewModel.Settings;

namespace MapEditor.ViewModel.Project
{
    public class SkyBoxSettingsViewModel : ViewModelBase
    {
        private FontSettingsViewModel fontSettingsVm;
        private SkyObject sceneObject;
        private SkyBoxSideViewModel front;
        private SkyBoxSideViewModel back;
        private SkyBoxSideViewModel left;
        private SkyBoxSideViewModel right;
        private SkyBoxSideViewModel top;
        private SkyBoxSideViewModel bottom;
        private int displayNameWidth;
        private int displayIconWidth;
        private int texturePathWidth;

        public SkyBoxSettingsViewModel(FontSettingsViewModel fontSettingsVm, MapEditorProjectViewModel projectVm)
        {
            Initialize(fontSettingsVm, projectVm);
            UpdateFontSizeDisplay();
            SetDefaultSettings();
        }

        private void Initialize(FontSettingsViewModel fontSettingsVm, MapEditorProjectViewModel projectVm)
        {
            this.fontSettingsVm = fontSettingsVm;
            this.fontSettingsVm.FontSizeChanged += FontSettingsVm_FontSizeChanged;
            front = new SkyBoxSideViewModel("Front", projectVm.Project.SkySettings.FrontSide);
            back = new SkyBoxSideViewModel("Back", projectVm.Project.SkySettings.BackSide);
            left = new SkyBoxSideViewModel("Left", projectVm.Project.SkySettings.LeftSide);
            right = new SkyBoxSideViewModel("Right", projectVm.Project.SkySettings.RightSide);
            top = new SkyBoxSideViewModel("Top", projectVm.Project.SkySettings.Top);
            bottom = new SkyBoxSideViewModel("Bottom", projectVm.Project.SkySettings.Bottom);
        }

        private void FontSettingsVm_FontSizeChanged(object sender, EventArgs e)
        {
            UpdateFontSizeDisplay();
        }

        public void SetDefaultSettings()
        {
            front.TextureFileUrl = @"Resources/Sky/SkyFront.bmp";
            back.TextureFileUrl = @"Resources/Sky/SkyBack.bmp";
            left.TextureFileUrl = @"Resources/Sky/SkyLeft.bmp";
            right.TextureFileUrl = @"Resources/Sky/SkyRight.bmp";
            top.TextureFileUrl = @"Resources/Sky/SkyTop.bmp";
            bottom.TextureFileUrl = @"Resources/Sky/SkyBottom.bmp";
        }

        private void UpdateFontSizeDisplay()
        {
            DisplayNameWidth = (int)(this.fontSettingsVm.FontSize * 3.5);
            DisplayIconWidth = (int)(this.fontSettingsVm.FontSize * 2);
            TexturePathWidth = (int)(this.fontSettingsVm.FontSize * 20.0);
        }

        public void UpdateSkyBox()
        {
            front.Update();
            back.Update();
            left.Update();
            right.Update();
            top.Update();
            bottom.Update();
        }

        public SkyBoxSideViewModel Front { get => front; }
        public SkyBoxSideViewModel Back { get => back; }
        public SkyBoxSideViewModel Left { get => left; }
        public SkyBoxSideViewModel Right { get => right; }
        public SkyBoxSideViewModel Top { get => top; }
        public SkyBoxSideViewModel Bottom { get => bottom; }

        public int DisplayNameWidth { get => displayNameWidth; private set { displayNameWidth = value; RaisePropertyChanged(); } }
        public int DisplayIconWidth { get => displayIconWidth; set { displayIconWidth = value; RaisePropertyChanged(); } }
        public int TexturePathWidth { get => texturePathWidth; set { texturePathWidth = value; RaisePropertyChanged(); } }
    }
}
