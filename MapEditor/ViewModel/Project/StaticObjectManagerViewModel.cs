﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MapEditor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.ViewModel.Project
{
    public class StaticObjectManagerViewModel : ViewModelBase
    {
        private MapEditorProjectViewModel projectVm;
        private int selectedStaticObjectIndex;
        private StaticObjectDefinitionViewModel selectedStaticObject;

        public StaticObjectManagerViewModel()
        {
            projectVm = new MapEditorViewModelLocator().MapEditorProjectVm;
            OpenMeshFileCommand = new RelayCommand(() => new StaticObjectManagerViewModelServices(this).BrowseMeshFileUrlDialog());
            OpenCollisionMeshFileCommand = new RelayCommand(() => new StaticObjectManagerViewModelServices(this).BrowseCollisionMeshFileUrlDialog());
            AddNewStaticObjectCommand = new RelayCommand(() => new StaticObjectManagerViewModelServices(this).AddNewStaticObject());
            DeleteStaticObjectCommand = new RelayCommand(() => new StaticObjectManagerViewModelServices(this).PromptUserForDeleteConfirmation());
            //AddBillboardCommand = new RelayCommand(() => { selectedStaticObject.AddNewBillboard(new ColladaLibrary.Render.BillboardInfo()); });
            SelectedStaticObjectIndex = -1;
        }

        public int SelectedStaticObjectIndex
        {
            get => selectedStaticObjectIndex;
            set
            {
                selectedStaticObjectIndex = value;

                if (selectedStaticObjectIndex >= 0 && selectedStaticObjectIndex < ProjectVm.StaticObjectVms.Count())
                {
                    SelectedStaticObject = projectVm.StaticObjectVms.ElementAt(selectedStaticObjectIndex);
                }
                else
                {
                    SelectedStaticObject = null;
                }
                RaisePropertyChanged();
            }
        }

        public RelayCommand DeleteStaticObjectCommand
        {
            get;
            private set;
        }

        public RelayCommand AddNewStaticObjectCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenMeshFileCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenCollisionMeshFileCommand
        {
            get;
            private set;
        }

        public RelayCommand AddBillboardCommand
        {
            get;
            private set;
        }

        public StaticObjectDefinitionViewModel SelectedStaticObject { get => selectedStaticObject; set { selectedStaticObject = value; RaisePropertyChanged(); } }
        public MapEditorProjectViewModel ProjectVm { get => projectVm; }
    }
}
