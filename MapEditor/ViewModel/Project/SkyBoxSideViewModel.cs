﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using TMLibrary.Lib.Map.Project.Sky;

namespace MapEditor.ViewModel.Project
{
    public class SkyBoxSideViewModel : CustomTextureViewModel
    {
        private SkySideInfo skySideInfo;
        private string displayName;
        private string displayTextureUrl;
        private Mesh mesh;

        public SkyBoxSideViewModel(string displayName, SkySideInfo skySideInfo)
        {
            this.displayName = displayName;
            this.skySideInfo = skySideInfo;

            TextureFileChanged += SkyBoxSideViewModel_TextureFileChanged;
        }

        public string DisplayName { get => displayName; }
        //public SkySideInfo SkySideInfo { get => skySideInfo; }
        public string DisplayTextureUrl
        {
            get
            {
                return displayTextureUrl;
            }
            set
            {
                displayTextureUrl = value;
                RaisePropertyChanged();
            }
        }

        public Mesh Mesh { get => mesh; set => mesh = value; }

        private void SkyBoxSideViewModel_TextureFileChanged(object sender, EventArgs e)
        {
            skySideInfo.TextureUrl = TextureFileUrl;
            DisplayTextureUrl = TextureFileUrl;

            if (mesh != null)
            {
                var originalTexture = mesh.GetTextures()[0];
                mesh.ReplaceDiffuseTexture(originalTexture.Diffuse.Bitmap, Texture.Bitmap);
            }
        }

        public void Update()
        {
            if (File.Exists(skySideInfo.TextureUrl))
            {
                TextureFileUrl = skySideInfo.TextureUrl;
            }
        }
    }
}
