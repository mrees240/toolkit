﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;

namespace MapEditor.ViewModel.Project
{
    public class PathNodeViewModel : ViewModelBase
    {
        private string name;
        private string imageIconUrl;

        public PathNodeViewModel()
        {
            name = "Path Node";
            imageIconUrl = @"/Resources/Icons/PathNodeIcon.bmp";
        }

        public string ImageIconUrl { get { return imageIconUrl; } }
        public string Name { get { return name; } }
    }
}
