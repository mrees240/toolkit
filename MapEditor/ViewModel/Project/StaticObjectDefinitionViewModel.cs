﻿using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using MapEditor.Project;
using MonoEngine.Code.Render;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.ViewModel.Project
{
    public class StaticObjectDefinitionViewModel : ViewModelBase
    {
        private string modelUrlTrimmed;
        private string collisionModelUrlTrimmed;
        private StaticObjectDefinition staticObjectDefinition;
        private ObservableCollection<BillboardInfoViewModel> billboardInfoVmList;

        public StaticObjectDefinitionViewModel(StaticObjectDefinition staticObject)
        {
            this.staticObjectDefinition = staticObject;
            billboardInfoVmList = new ObservableCollection<BillboardInfoViewModel>();
            //billboardInfoVm = new BillboardInfoViewModel(staticObject.BillboardInfoList);
        }

        public void UpdateBillboardInfoViewModels()
        {
            billboardInfoVmList.Clear();

            foreach (var billboard in staticObjectDefinition.BillboardInfoList)
            {
                billboardInfoVmList.Add(new BillboardInfoViewModel(billboard));
            }
        }

        public string ModelUrlTrimmed
        {
            get
            {
                return modelUrlTrimmed;
            }
            set
            {
                modelUrlTrimmed = value;
                RaisePropertyChanged();
            }
        }
        public string CollisionModelUrlTrimmed
        {
            get
            {
                return collisionModelUrlTrimmed;
            }
            set
            {
                collisionModelUrlTrimmed = value;
                RaisePropertyChanged();
            }
        }

        private string TrimUrl(string originalUrl)
        {
            int amountToTake = 40;
            if (originalUrl == null)
            {
                return string.Empty;
            }
            else if (originalUrl.Length < amountToTake)
            {
                return originalUrl;
            }
            else
            {
                int skipCount = originalUrl.Length - amountToTake;
                return $"...{new string(originalUrl.Skip(skipCount).Take(amountToTake).ToArray())}";
            }
        }

        public string ModelUrl
        {
            get
            {
                return staticObjectDefinition.ModelUrl;
            }
            set
            {
                staticObjectDefinition.UpdateModelUrl(value);
                RaisePropertyChanged();
                ModelUrlTrimmed = TrimUrl(value);
            }
        }

        public string CollisionModelUrl
        {
            get
            {
                return staticObjectDefinition.CollisionModelUrl;
            }
            set
            {
                staticObjectDefinition.UpdateCollisionModelUrl(value);
                RaisePropertyChanged();
                CollisionModelUrlTrimmed = TrimUrl(value);
            }
        }

        public string Name
        {
            get
            {
                return staticObjectDefinition.Name;
            }
            set
            {
                staticObjectDefinition.Name = value;
                RaisePropertyChanged();
            }
        }

        public bool HasCollisionResponse
        {
            get
            {
                return staticObjectDefinition.HasCollisionResponse;
            }
            set
            {
                staticObjectDefinition.HasCollisionResponse = value;
                RaisePropertyChanged();
            }
        }

        public bool IsExplosive
        {
            get
            {
                return staticObjectDefinition.IsExplosive;
            }
            set
            {
                staticObjectDefinition.IsExplosive = value;
                RaisePropertyChanged();
            }
        }

        public bool ExplodesOnImpact
        {
            get
            {
                return staticObjectDefinition.ExplodesOnImpact;
            }
            set
            {
                staticObjectDefinition.ExplodesOnImpact = value;
                RaisePropertyChanged();
            }
        }

        public bool IsBillboard
        {
            get
            {
                return staticObjectDefinition.IsBillboard;
            }
            set
            {
                staticObjectDefinition.IsBillboard = value;
                RaisePropertyChanged();
            }
        }

        public bool DefaultLighting
        {
            get
            {
                return staticObjectDefinition.DefaultLighting;
            }
            set
            {
                staticObjectDefinition.DefaultLighting = value;
                RaisePropertyChanged();
            }
        }

        public bool IsDrivableSurface
        {
            get
            {
                return staticObjectDefinition.IsDrivableSurface;
            }
            set
            {
                staticObjectDefinition.IsDrivableSurface = value;
                RaisePropertyChanged();
            }
        }

        public int MinDrawDistance
        {
            get
            {
                return staticObjectDefinition.MinDrawDistance;
            }
            set
            {
                staticObjectDefinition.MinDrawDistance = value;
                RaisePropertyChanged();
            }
        }

        public int MaxDrawDistance
        {
            get
            {
                return staticObjectDefinition.MaxDrawDistance;
            }
            set
            {
                staticObjectDefinition.MaxDrawDistance = value;
                RaisePropertyChanged();
            }
        }

        public StaticObjectDefinition StaticObject { get => staticObjectDefinition; }
        public ObservableCollection<BillboardInfoViewModel> BillboardInfoVmList { get => billboardInfoVmList; }
    }
}
