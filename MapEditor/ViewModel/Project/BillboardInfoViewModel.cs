﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using WpfCore.Utility;

namespace MapEditor.ViewModel.Project
{
    public class BillboardInfoViewModel : CustomTextureViewModel
    {
        private BillboardInfo billboardInfo;
        private string textureFileUrl;
        private BitmapFilePair texture;
        private BitmapSource textureSource;

        public BillboardInfoViewModel(BillboardInfo billboardInfo)
        {
            this.billboardInfo = billboardInfo;
            this.texture = billboardInfo.Texture;
        }

        public float Width { get => billboardInfo.Width; set { if (value > 0) { billboardInfo.Width = value; } else { billboardInfo.Width = 1; } RaisePropertyChanged(); } }
        public float Height { get => billboardInfo.Height; set { if (value > 0) { billboardInfo.Height = value; } else { billboardInfo.Height = 1; } RaisePropertyChanged(); } }
        public float XOffset { get => billboardInfo.XOffset; set { billboardInfo.XOffset = value; RaisePropertyChanged(); } }
        public float YOffset { get => billboardInfo.YOffset; set { billboardInfo.YOffset = value; RaisePropertyChanged(); } }
        public float ZOffset { get => billboardInfo.ZOffset; set { billboardInfo.ZOffset = value; RaisePropertyChanged(); } }
    }
}
