﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using WpfCore.Utility;

namespace MapEditor.ViewModel.Project
{
    public class CustomTextureViewModel : ViewModelBase
    {
        protected string textureFileUrl;
        protected BitmapFilePair texture;
        protected BitmapSource textureSource;

        public event EventHandler TextureFileChanged;

        public virtual void OnTextureChanged()
        {
            if (TextureFileChanged != null)
            {
                TextureFileChanged(this, EventArgs.Empty);
            }
        }

        public CustomTextureViewModel()
        {
            BrowseTextureFileCommand = new RelayCommand(() =>
            {
                BrowseTextureFileUrlDialog();
            });
            texture = new BitmapFilePair();
        }

        public string TextureFileUrl
        {
            get { return textureFileUrl; }
            set
            {
                if (File.Exists(value))
                {
                    Bitmap newBitmap = null;

                    try
                    {
                        newBitmap = new System.Drawing.Bitmap(value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error loading {value}.\n\n{ex.Message}");
                    }
                    textureFileUrl = value;
                    texture.FileName = textureFileUrl;
                    if (texture.Bitmap != null)
                    {
                        texture.Bitmap.Dispose();
                    }
                    texture.Bitmap = newBitmap;
                    TextureSource = BitmapUtility.BitmapToBitmapSource(texture.Bitmap);
                    RaisePropertyChanged();
                    OnTextureChanged();
                }
                else
                {
                    throw new Exception($"File {value} does not exist.");
                }
            }
        }

        public RelayCommand BrowseTextureFileCommand { get; set; }


        private void BrowseTextureFileUrlDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.DefaultExt = ".bmp";
            dialog.Filter = "Bitmap Image (.bmp)|*.bmp";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                TextureFileUrl = dialog.FileName;
            }
        }
        private void UpdateBitmapSet()
        {
            if (!File.Exists(textureFileUrl))
            {
                System.Windows.MessageBox.Show($"{textureFileUrl} does not exist.");
            }
            else
            {
                Bitmap newBitmap = null;

                try
                {
                    newBitmap = new System.Drawing.Bitmap(textureFileUrl);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error loading {textureFileUrl}.\n\n{ex.Message}");
                }
                if (texture.Bitmap != null)
                {
                    texture.Bitmap.Dispose();
                }
                if (newBitmap != null)
                {
                    texture.FileName = textureFileUrl;
                    texture.Bitmap = newBitmap;
                    TextureSource = BitmapUtility.BitmapToBitmapSource(texture.Bitmap);
                }
            }
        }

        public BitmapSource TextureSource { get => textureSource; private set { textureSource = value; RaisePropertyChanged(); } }

        public BitmapFilePair Texture { get => texture; }
    }
}
