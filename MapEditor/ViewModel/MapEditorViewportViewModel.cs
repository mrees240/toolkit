﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using MapEditor.Project;
using MapEditor.ViewModel.Project;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Input;
using MonoGameCore.Render;
using WpfCore.ViewModel;

namespace MapEditor.ViewModel
{
    public class MapEditorViewportViewModel : ViewModelBase, IMonoViewportViewModelWrapper
    {
        private MapEditorProjectViewModel projectVm;
        private MonoViewportViewModel monoViewportVmBase;

        public MapEditorViewportViewModel(MapEditorProjectViewModel projectVm, MonoViewportViewModel monoViewportVmBase)
        {
            this.monoViewportVmBase = monoViewportVmBase;
            this.projectVm = projectVm;

            monoViewportVmBase.Camera.CameraType = CameraType.Fly;
            monoViewportVmBase.BackgroundColor = System.Windows.Media.Color.FromRgb(153, 217, 234);
        }


        /*public void CheckCurrentPickableObject(MouseButton mouseButton, MouseState mouseState)
        {
            float closestDistanceToOrigin = -1.0f;
            IPickable3DObject pickedItem = null;

            if (mouseButton.MouseButtonState.MouseClicked)
            {
                //if (!inputState.LeftMouseButton.ButtonReleased)
                {
                    var objectInstances = projectVm.ObjectInstanceVms;
                    var gameItemInstances = objectInstances.Where(i => i.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.GameItemObject);

                    foreach (var gameItem in gameItemInstances)
                    {
                        var tris = gameItem.SceneObject.GetPickableTriangles();
                        bool picked = false;
                        float itemClosestDistance = -1;

                        foreach (var tri in tris)
                        {
                            var result = monoViewportVmBase.MouseRay.GetRay().RayTriangleIntersection(tri);

                            if (result.Intersects)
                            {
                                picked = true;
                            }

                            if (itemClosestDistance < 0 || result.Distance < itemClosestDistance)
                            {
                                itemClosestDistance = result.Distance;
                            }
                        }

                        if (picked)
                        {
                            if (closestDistanceToOrigin < 0 || itemClosestDistance < closestDistanceToOrigin)
                            {
                                pickedItem = (IPickable3DObject)gameItem;
                                pickedObjectInstance = gameItem;
                                closestDistanceToOrigin = itemClosestDistance;
                                projectVm.SelectedObjectInstanceVm = gameItem;
                            }
                        }
                    }
                }
            }
            if (pickedObjectInstance == null)
            {
                pickedObjectInstance = projectVm.SelectedObjectInstanceVm;
            }
            if (pickedObjectInstance != null)
            {
                //var translationGizmo = pickedObjectInstance.GetTranslationGizmo();

                /*if (mouseButton.MouseButtonState.MouseClicked)
                {
                    translationGizmoController.UpdateClick(monoViewportVmBase.MouseRay.GetRay(), mouseButton, mouseState, 0);
                }
                else if (mouseButton.MouseButtonState.MouseHold)
                {
                    translationGizmoController.UpdateHold(monoViewportVmBase.MouseRay.GetRay(), mouseButton, mouseState, 0);
                }
                translationGizmoController.UpdateDefault(monoViewportVmBase.MouseRay.GetRay(), monoViewportVmBase.Camera, mouseButton, mouseState, 0);*/
            /*}
        }*/

        public MonoViewportViewModel MonoViewportVmBase { get => monoViewportVmBase; }

        public MonoViewportViewModel GetBaseViewportVm()
        {
            return MonoViewportVmBase;
        }
        //public InputMouseState InputState { get => inputState; set => inputState = value; }
    }
}
