/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:MapEditor"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using MapEditor.ViewModel.Project;
using MapEditor.ViewModel.Settings;
using WpfCore.ViewModel;
using WpfCore.ViewModel.Settings;

namespace MapEditor.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class MapEditorViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public MapEditorViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<MapEditorProjectViewModel>();
            SimpleIoc.Default.Register<StaticObjectManagerViewModel>();
            SimpleIoc.Default.Register<MapCompilerViewModel>();

            SimpleIoc.Default.Register<FontSettingsViewModel>();
            SimpleIoc.Default.Register<DisplaySettingsViewModel>();
            SimpleIoc.Default.Register<InputSettingsViewModel>();
            SimpleIoc.Default.Register<UserInterfaceSettingsViewModel>();
            SimpleIoc.Default.Register<DebugSettingsViewModel>();
            SimpleIoc.Default.Register<KeyBindSettingsViewModel>();
            //SimpleIoc.Default.Register<SettingsViewModel>();
        }

        /*public SettingsViewModel SettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SettingsViewModel>();
            }
        }*/

        public KeyBindSettingsViewModel KeyBindSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<KeyBindSettingsViewModel>();
            }
        }

        public DebugSettingsViewModel DebugSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DebugSettingsViewModel>();
            }
        }

        public UserInterfaceSettingsViewModel UserInterfaceSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<UserInterfaceSettingsViewModel>();
            }
        }

        public FontSettingsViewModel FontSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FontSettingsViewModel>();
            }
        }

        public DisplaySettingsViewModel DisplaySettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DisplaySettingsViewModel>();
            }
        }

        public InputSettingsViewModel InputSettingsVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<InputSettingsViewModel>();
            }
        }

        public MapCompilerViewModel MapCompilerVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MapCompilerViewModel>();
            }
        }

        public MainViewModel MainVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public MapEditorProjectViewModel MapEditorProjectVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MapEditorProjectViewModel>();
            }
        }

        public StaticObjectManagerViewModel StaticObjectManagerVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StaticObjectManagerViewModel>();
            }
        }

        /*public MonoViewportViewModel MonoViewportVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MonoViewportViewModel>();
            }
        }*/

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}