﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MapEditor.Services;
using MapEditor.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TMLibrary.Lib.Map;
using WpfCore.Settings;

namespace MapEditor.ViewModel
{
    public class MapCompilerViewModel : ViewModelBase
    {
        private MapCompilerSettings settings;
        private string compilationLog;
        private CancellationTokenSource cancelMapCompileToken;
        private MapCompiler mapCompiler;
        private MapCompilerService mapCompilerService;
        private int progress;

        public MapCompilerViewModel()
        {
            cancelMapCompileToken = new CancellationTokenSource();
            mapCompiler = new MapCompiler();
            mapCompiler.Worker.ProgressChanged += Worker_ProgressChanged;
            mapCompilerService = new MapCompilerService(this, cancelMapCompileToken, mapCompiler);
            InitializeCommands();
            settings = new MapCompilerSettings();
            settings.ReadFromFile();
        }

        private void Worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Progress = e.ProgressPercentage;
        }

        private void InitializeCommands()
        {
            CompileMapCommand = new RelayCommand(() =>
            {
                new Task(() =>
                {
                    mapCompilerService.CompileMap();
                }).Start();
            });
            BrowseOutputPathCommand = new RelayCommand(() => mapCompilerService.BrowseTm2PathDialog() );
            CancelCompilationCommand = new RelayCommand(() => CancelCompilation());
        }

        public RelayCommand CancelCompilationCommand
        {
            get;
            private set;
        }

        public void CancelCompilation()
        {
            cancelMapCompileToken.Cancel();
        }

        public RelayCommand CompileMapCommand
        {
            get;
            private set;
        }

        public RelayCommand BrowseOutputPathCommand
        {
            get;
            private set;
        }

        public void SaveSettings()
        {
            settings.WriteToFile();
        }

        public void Reset()
        {
            Progress = 0;
            mapCompiler.Worker.ReportProgress(0);
            CompilationLog = string.Empty;
        }

        public string OutputDirectory { get => settings.OutputDirectory; set { settings.OutputDirectory = value; RaisePropertyChanged(); } }

        public string CompilationLog { get => compilationLog; set { compilationLog = value; RaisePropertyChanged(); } }

        public int Progress { get => progress; set { progress = value; RaisePropertyChanged(); } }
    }
}
