using ColladaLibrary.Collada;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MapEditor.Input.MouseInputs;
using MapEditor.Project;
using MapEditor.Render;
using MapEditor.Services;
using MapEditor.ViewModel.Game;
using MapEditor.ViewModel.Project;
using MapEditor.Views;
using MapEditor.Views.Project;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using TMLibrary.Lib.Map.Project;
using TMLibrary.Lib.ProjectTypes;
using WpfCore.Settings;
using WpfCore.ViewModel;
using WpfCore.ViewModel.Settings;
using System.Linq;
using MapEditor.Input;
using MonoEngine.Code;
using MonoEngine.Input.MouseInputs;
using MonoEngine.Code.CommonFeatures;
using MonoEngine;
using MonoEngine.ViewModel;
using MonoEngine.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Views.Debug;
using MapEditor.Views.Settings;
using MapEditor.ViewModel.Settings;
using MapEditor.Views.FileDialogs;
using ColladaLibrary.Utility;
using System.Numerics;
using TMLibrary.Lib.FileTypes.Point;
using MonoEngine.Code.Render;
using ColladaLibrary.Render;
using TMLibrary.Lib.Util;
using System.Diagnostics;
using MonoEngine.Views.InputSettings;
using System.IO;

namespace MapEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private string mainWindowTitle;
        private MapEditorViewModelLocator vml;
        private MonoEngine.ViewModel.MonoEngineMainViewModel baseViewModel;
        private ViewportMode viewportMode;
        private MapEditorProjectViewModel projectVm;
        private int width;
        private int height;
        private bool atLeastOneStaticObjectExists;
        private MouseController mouseController;
        private System.Windows.Forms.Timer timer;
        private MouseState mouseState;
        private int currentSettingsPanelId;
        private DefaultEditorLmbAction editorLmbAction;
        private ObservableCollection<GameItemObjectViewModel> gameItemObjectVms;
        private MapEditorMeshContainer meshContainer;
        private PathNodeViewModel pathNodeVm;
        private bool showViewports;
        private List<IMonoViewportViewModelWrapper> mapEditorViewportViewModels;
        private List<MonoViewportViewModel> baseViewportViewModels;
        private Version appVersion;
        private MapEditorProjectDialog projectDialog;
        private Dictionary<GameType, MapEditorProjectDefinition> projectDefinitions;

        // Settings
        private MapEditorWindowSettingsViewModel windowSettingsCollectionViewModel;
        private InputSettings inputSettings;
        private MiscSettingsViewModel miscSettingsVm;
        private KeyBindSettingsViewModel keyBindSettingsVm;

        // View settings
        private bool showingProjectHierarchy;
        private bool showingGrid;

        // Windows
        private List<Window> allWindows;
        private KeyBindConfirmationWindow keyBindConfirmationWindow;

        private LinkManagerObject linkManager;
        private SkyObject skyObject;

        private Stack<IUndoRedoAction> undoRedoActions;

        private Action ToggleShowDebugMenu()
        {
            return new Action(() => { vml.DebugSettingsVm.ToggleShowDebugMenu(); });
        }

        public void UpdateKeyBindings()
        {
            baseViewModel.EngineCore.KeyboardController.Reset();
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingLinkObjects.KeyBinding, LinkKeyHeldDownAction(), KeyStateMono.Hold);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingLinkObjects.KeyBinding, LinkKeyUpAction(), KeyStateMono.Up);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingMoveSelectedObjectMouseRay.KeyBinding, MoveSelectedObjectInstanceMouseRayAction(), KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingSnapToGround.KeyBinding, SnapSelectedObjectToGround(), KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingToggleDebugMenu.KeyBinding, ToggleShowDebugMenu(), KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingToggleObject.KeyBinding, ToggleKeyHeldDownAction(), KeyStateMono.Hold);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingToggleObject.KeyBinding, ToggleKeyUpAction(), KeyStateMono.Up);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingDeleteObject.KeyBinding, DeleteObjectInstanceAction(), KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(keyBindSettingsVm.BindingPlacePathNode.KeyBinding, AddPathNodeMouseRayAction(), KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2Health.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[0]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2Turbo.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[1]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2FireMissle.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[2]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2HomingMissle.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[3]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2Napalm.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[4]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2PowerMissle.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[5]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2Remote.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[6]); }),
                KeyStateMono.Released);
            baseViewModel.EngineCore.KeyboardController.AddKeyCommand(
                keyBindSettingsVm.BindingTm2Ricochet.KeyBinding,
                new Action(() => { projectVm.AddGameItemInstanceMouseRay(gameItemObjectVms[7]); }),
                KeyStateMono.Released);
        }

        public void InitializeKeyboardInput()
        {
            keyBindSettingsVm = vml.KeyBindSettingsVm;
            keyBindSettingsVm.Initialize(this);
            if (!File.Exists(keyBindSettingsVm.Path))
            {
                keyBindSettingsVm.SaveSettings();
            }
            keyBindSettingsVm.LoadSettings();
            UpdateKeyBindings();
        }

        private Action SnapSelectedObjectToGround()
        {
            return new Action(() =>
            {
                if (projectVm.SelectedObjectInstanceVm != null)
                {
                    var staticObjects = projectVm.ObjectInstanceVms.
                        Where(i => i.ObjectInstance.ObjectType == ObjectType.StaticObject).
                        Select(s => (IDrawableSceneObject)s.SceneObject).ToList();
                    var groundResult = LmbAction.FindGroundPoint(
                        projectVm.SelectedObjectInstanceVm.ObjectInstance.Transformation.WorldMatrix.Translation,
                        EngineCore.GameScene,
                        projectVm.SelectedObjectInstanceVm.SceneObject,
                        staticObjects);

                    if (groundResult.Intersects)
                    {
                        projectVm.SelectedObjectInstanceVm.TransformationVm.ZPositionWorld = groundResult.IntersectionCoordinate.Z;
                    }
                }
            });
        }

        private Action DeleteObjectInstanceAction()
        {
            return new Action(() =>
            {
                if (projectVm.SelectedObjectInstanceVm != null)
                {
                    projectVm.DeleteObjectInstance(projectVm.SelectedObjectInstanceVm);
                }
            });
        }

        private Action ToggleKeyHeldDownAction()
        {
            return new Action(() =>
            {
                editorLmbAction.ToggleKeyDown = true;
            });
        }

        private Action ToggleKeyUpAction()
        {
            return new Action(() =>
            {
                editorLmbAction.ToggleKeyDown = false;
            });
        }

        private Action LinkKeyHeldDownAction()
        {
            return new Action(() =>
            {
                editorLmbAction.LinkKeyDown = true;
            });
        }

        private Action MoveSelectedObjectInstanceMouseRayAction()
        {
            return new Action(() =>
            {
                projectVm.MoveSelectedObjectMouseRay();
            });
        }

        private Action AddPathNodeMouseRayAction()
        {
            return new Action(() =>
            {
                projectVm.AddPathNodeInstanceMouseRay();
            });
        }

        private Action LinkKeyUpAction()
        {
            return new Action(() =>
            {
                editorLmbAction.LinkKeyDown = false;
            });
        }

        private void InitializeWindows()
        {
            allWindows = new List<Window>();
            windowSettingsCollectionViewModel = new MapEditorWindowSettingsViewModel();
            windowSettingsCollectionViewModel.LoadWindowSettings();

            miscSettingsVm = new MiscSettingsViewModel();
            appVersion = typeof(App).Assembly.GetName().Version;
            keyBindConfirmationWindow = new KeyBindConfirmationWindow();

            projectDialog = new MapEditorProjectDialog();
            UpdateWindowTitle();
        }

        public void UpdateWindowTitle()
        {
            if (projectVm == null)
            {
                MainWindowTitle = $"Twisted Map Editor v{appVersion.ToString()} - New Project";
            }
            else
            {
                MainWindowTitle = $"Twisted Map Editor v{appVersion.ToString()} - {projectVm.ProjectName}";
            }
            if (baseViewModel != null)
            {
                if (vml.DebugSettingsVm.ShowDebugMenu)
                {
                    MainWindowTitle += $", FPS: {GetMonoViewportVm0.FramesPerSecond}";
                    if (EngineCore.GameScene.MouseHoverManager.DirectHoverObjectNoDepth != null)
                    {
                        MainWindowTitle += $", Mouse Hover: {EngineCore.GameScene.MouseHoverManager.DirectHoverObjectNoDepth.GetObjectState().Name}";
                    }
                    else if (EngineCore.GameScene.MouseHoverManager.DirectHoverObject != null)
                    {
                        MainWindowTitle += $", Mouse Hover: {EngineCore.GameScene.MouseHoverManager.DirectHoverObject.GetObjectState().Name}";
                    }
                    else
                    {
                        MainWindowTitle += $", Mouse Hover: NONE";
                    }
                    if (EngineCore.GameScene.ActiveMonoViewportVm != null)
                    {
                        var mouseRay = EngineCore.GameScene.ActiveMonoViewportVm.MouseRay.GetRay();
                        MainWindowTitle += $", Mouse Ray: {mouseRay.GetInfoString()}";
                    }
                    else
                    {
                        MainWindowTitle += $", Mouse Ray: NONE";
                    }
                }
            }
        }

        public void CloseAllWindows()
        {
            SaveWindowSettings();
            SaveMiscSettings();

            foreach (var window in allWindows)
            {
                if (window.IsVisible)
                {
                    window.Close();
                }
            }
            System.Windows.Application.Current.Shutdown();
        }

        private void SaveMiscSettings()
        {
            miscSettingsVm.SaveMiscSettings();
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            vml = new MapEditorViewModelLocator();
#if debug
            InitializeMainVm();
#else
            try
            {
                InitializeMainVm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogFileUtility.AppendToLogFile(ex.StackTrace);
            }
#endif
        }

        private void InitializeMainVm()
        {
            undoRedoActions = new Stack<IUndoRedoAction>();
            projectDefinitions = new ProjectDefinitionFactory().CreateProjectDefinitions();
            InitializeWindows();
            InitializeMouse();
            InitializeBaseViewModel();
            InitializeTimer();
            InitializeLists();
            InitializeMouseActions();
            InitializeViewports();
            InitializeCommands();
            InitializeEngine();
            InitializeKeyboardInput();
            InitializeLinkManager();
            InitializeSky();
            StartNewProject();
        }

        private void InitializeSky()
        {
            skyObject = new SkyObject(projectVm.SkyBoxSettingsVm);
        }

        private void InitializeLinkManager()
        {
            linkManager = new LinkManagerObject(projectVm.Project.PathConnectionContainer.PathNodeConnections, projectVm, meshContainer, miscSettingsVm.MiscSettings);
        }

        private void InitializeBaseViewModel()
        {
            baseViewModel = new MonoEngineMainViewModel(mouseController);
        }

        private void InitializeMouse()
        {
            mouseController = new MouseController();
            this.mouseState = mouseController.MouseState;
        }

        private void InitializeTimer()
        {
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1;
            timer.Start();
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            UpdateWindowTitle();
        }

        private void OpenProjectFilePrompt()
        {
#if DEBUG
            var openDialog = projectDialog.OpenProjectFileDialog();

            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var fileName = openDialog.FileName;

                OpenProjectFile(fileName);
            }
#else
            try
            {
                var openDialog = projectDialog.OpenProjectFileDialog();

                if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var fileName = openDialog.FileName;

                    OpenProjectFile(fileName);
                }
            }
            catch (Exception ex)
            {
                var msg = LogFileUtility.CreateErrorMessage(ex.Message);
                MessageBox.Show(msg);
                StartNewProject();
                UpdateWindowTitle();
            }
#endif
        }

        private void OpenProjectFile(string fileUrl)
        {
            StartNewProject();
            projectVm.OpenProject(fileUrl, appVersion, projectDefinitions[GameType.TM2PC]);
            UpdateWindowTitle();
        }

        private void UpdateProjectScene()
        {
            var objectInstanceVm = projectVm.ObjectInstanceVms;

            foreach (var instanceVm in objectInstanceVm)
            {
                EngineCore.GameScene.AddObject(instanceVm.SceneObject, true);
            }
        }

        private void InitializeLists()
        {
            inputSettings = new InputSettings();
            baseViewportViewModels = new List<MonoViewportViewModel>();
            mapEditorViewportViewModels = new List<IMonoViewportViewModelWrapper>();
            pathNodeVm = new PathNodeViewModel();
            meshContainer = new MapEditorMeshContainer();
            gameItemObjectVms = new ObservableCollection<GameItemObjectViewModel>();
            projectVm = new MapEditorViewModelLocator().MapEditorProjectVm;
        }

        private void InitializeEngine()
        {
            EngineCore.EngineSettings.ShowGrid = true;
            ShowingGrid = EngineCore.EngineSettings.ShowGrid;
            baseViewModel.StartEngine();
            vml.DebugSettingsVm.Settings = EngineCore.EngineSettings;
            EngineCore.GameScene.EditorLmbAction = editorLmbAction;
        }

        private void InitializeMouseActions()
        {
            editorLmbAction = new DefaultEditorLmbAction(EngineCore, mouseState, false);
            var rmbActions = new DefaultOrbitRmbAction(inputSettings, mouseState,
                new MonoEngine.Input.MouseButton(System.Windows.Forms.MouseButtons.Right),
                mapEditorViewportViewModels);
            var mmbActions = new DefaultPanMmbAction(inputSettings, mouseState, new MonoEngine.Input.MouseButton(System.Windows.Forms.MouseButtons.Middle),
                mapEditorViewportViewModels);
            mouseController.InitializeActions(editorLmbAction, rmbActions, mmbActions, mapEditorViewportViewModels);

            projectVm.EditorLmbAction = editorLmbAction;
        }

        private void InitializeViewports()
        {
            for (int iVp = 0; iVp < 6; iVp++)
            {
                baseViewportViewModels.Add(new MonoViewportViewModel());
                mapEditorViewportViewModels.Add(new MapEditorViewportViewModel(projectVm, baseViewportViewModels[iVp]));
                baseViewModel.AddViewportViewModel(mapEditorViewportViewModels[iVp]);
            }

            UpdateViewportMode(ViewportMode.One_Perspective);
            ShowViewports = true;
        }

        public void ResetScene()
        {
            EngineCore.GameScene.ResetScene();
            linkManager.Reset();
            EngineCore.GameScene.AddObject(linkManager);
        }

        public void StartNewProject()
        {
            undoRedoActions.Clear();
            GameItemObjectVms.Clear();
            ResetScene();
            ResetProject();
            InitializeTm2PcProject();
            InitializeProjectScene();
            UpdateProjectAssemblyVersion();
        }

        private void ResetStaticObjectManagerViewModel()
        {
            vml.StaticObjectManagerVm.SelectedStaticObject = null;
            vml.StaticObjectManagerVm.SelectedStaticObjectIndex = -1;
        }

        private void InitializeProjectScene()
        {
            EngineCore.GameScene.AddLight(projectVm.SunLightVm.Light);
            EngineCore.GameScene.AddLight(projectVm.AmbientLightVm.Light);
            EngineCore.GameScene.AddObject(skyObject);
        }

        private void UpdateProjectAssemblyVersion()
        {
            projectVm.Project.FileVersion = typeof(App).Assembly.GetName().Version.ToString();
        }

        private void DebugMouseStateWindow()
        {
            var mouseStateVm = new MouseStateViewModel(mouseState);
            var mouseStateWindow = new MouseStateWindow();

            mouseStateWindow.DataContext = mouseStateVm;

            mouseStateWindow.Show();
        }

        public void UpdateViewportMode(ViewportMode mode)
        {
            this.viewportMode = mode;
            RaisePropertyChanged(nameof(ShowingOneViewport));
            RaisePropertyChanged(nameof(ShowingFourViewports));
            UpdateViewportVisibility();
        }

        private void ResetProject()
        {
            ResetStaticObjectManagerViewModel();
            projectVm.StartNewProject(mouseController);
            ResetScene();
            ResetSelectedObjectInstances();
            projectVm.StaticObjectVms.Clear();
            UpdateAtLeastOneStaticObjectExists();
            UpdateWindowTitle();
        }

        private void ResetSelectedObjectInstances()
        {
            if (projectVm.SelectedObjectInstanceVm != null)
            {
                projectVm.SelectedObjectInstanceVm.SetDefaultTransformationVm();
                projectVm.SelectedObjectInstanceVm = null;
            }
            projectVm.SelectedStaticObjectIndex = -1;
            projectVm.SelectedObjectInstanceVm = null;
        }

        public void InitializeTm2PcProject()
        {
            var gameItemFactory = new GameItemObjectFactory();
            var projectDefinition = projectDefinitions[GameType.TM2PC];
            var gameItems = projectDefinition.GameItemObjects;
            projectVm.Project.UpdateProjectDefinition(projectDefinition);
            GameItemObjectVms.Clear();

            foreach (var gameItem in gameItems)
            {
                GameItemObjectVms.Add(new GameItemObjectViewModel(gameItem.Value));
            }
        }

        private void InitializeCommands()
        {
            SetDefaultInputSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().InputSettingsVm.ApplyDefaultSettings(); });
            SaveInputSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().InputSettingsVm.SaveSettings(); });
            SetDefaultUserInterfaceSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().UserInterfaceSettingsVm.ApplyDefaultSettings(); });
            SaveUserInterfaceSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().UserInterfaceSettingsVm.SaveSettings(); });
            SetDefaultFontSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().FontSettingsVm.ApplyDefaultSettings(); });
            SaveFontSettingsCommand = new RelayCommand(() => { new MapEditorViewModelLocator().FontSettingsVm.SaveSettings(); });
            ShowUiSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenSettingsWindow(0); });
            ShowFontSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenSettingsWindow(1); });
            ShowInputSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenSettingsWindow(2); });
            ShowKeyBindSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenSettingsWindow(3); });
            CloseApplicationCommand = new RelayCommand(() => new MainViewModelServices(this).CloseApplication());
            OpenStaticObjectManagerCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenStaticObjectManager(); });
            ToggleShowProjectHierarchyCommand = new RelayCommand(() => { new MainViewModelServices(this).ToggleShowProjectHierarchy(); });
            ToggleShowGridCommand = new RelayCommand(() => { new MainViewModelServices(this).ToggleShowGrid(); });
            ShowLightSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenLightSettingsWindow(); });
            ShowSkyBoxSettingsCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenSkyBoxSettingsWindow(); });
            CloseWindowCommand = new RelayCommand<Window>((win) => { win.Close(); if (allWindows.Contains(win)) { allWindows.Remove(win); } });
            ActivateOneViewportCommand = new RelayCommand(() => { UpdateViewportMode(ViewportMode.One_Perspective); });
            ActivateFourViewportsCommand = new RelayCommand(() => { UpdateViewportMode(ViewportMode.Four_Perspective); });
            OpenCompileMapWindowCommand = new RelayCommand(() => { new MainViewModelServices(this).OpenNewCompileMapWindow(); });
            OpenProjectCommand = new RelayCommand(() => { OpenProjectFilePrompt(); });
            NewProjectCommand = new RelayCommand(() => { StartNewProject(); });
            ToggleDebugMenuCommand = new RelayCommand(() => { vml.DebugSettingsVm.ShowDebugMenu = !vml.DebugSettingsVm.ShowDebugMenu; });
            ToggleDebugShowPickableMeshesCommand = new RelayCommand(() => { vml.DebugSettingsVm.ShowPickableMeshes = !vml.DebugSettingsVm.ShowPickableMeshes; });
            ToggleDebugShowBillboardsCommand = new RelayCommand(() => { vml.DebugSettingsVm.ShowBillboards = !vml.DebugSettingsVm.ShowBillboards; });
            ToggleDebugUpdateMouseRaysCommand = new RelayCommand(() => { vml.DebugSettingsVm.DebugUpdateMouseRays = !vml.DebugSettingsVm.DebugUpdateMouseRays; });
            ToggleDebugShowBroadPhaseCollisionCommand = new RelayCommand(() => { vml.DebugSettingsVm.DebugShowBroadPhaseCollision = !vml.DebugSettingsVm.DebugShowBroadPhaseCollision; });
            ToggleDebugHighlightMouseHoveredObjectsCommand = new RelayCommand(() => { vml.DebugSettingsVm.DebugHighlightMouseHoveredObjects = !vml.DebugSettingsVm.DebugHighlightMouseHoveredObjects; });
            ToggleShowPathNodesCommand = new RelayCommand(() => { miscSettingsVm.ShowPathNodes = !miscSettingsVm.ShowPathNodes; });
            TestPickableObjectCommand = new RelayCommand(() => { new MainViewModelServices(this).StartPickableObjectTest(); });

            AddStaticObjectInstanceToSceneCommand = new RelayCommand(
                () =>
                {
                    vml.MapEditorProjectVm.AddStaticObjectInstance();
                });
            AddItemPickupInstanceCommand = new RelayCommand<GameItemObjectViewModel>((obj) =>
            {
                vml.MapEditorProjectVm.AddGameItemInstance(obj);
            });
            AddPathNodeCommand = new RelayCommand(() =>
            {
                vml.MapEditorProjectVm.AddPathNodeInstance();
            });
        }

        public RelayCommand NewProjectCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleShowGridCommand
        {
            get;
            private set;
        }

        public RelayCommand AddPathNodeCommand
        {
            get;
            private set;
        }

        public RelayCommand<GameItemObjectViewModel> AddItemPickupInstanceCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenCompileMapWindowCommand
        {
            get;
            private set;
        }

        public RelayCommand TestStaticObjectRemovalCommand
        {
            get;
            private set;
        }

        public RelayCommand TestStaticObjectCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenNewViewportCommand
        {
            get;
            private set;
        }

        public RelayCommand CloseApplicationCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenStaticObjectManagerCommand
        {
            get;
            private set;
        }

        public RelayCommand AddStaticObjectInstanceToSceneCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowFontSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowUiSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowInputSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowKeyBindSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand SaveFontSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand SetDefaultFontSettingsCommand
        {
            get;
            private set;
        }


        public RelayCommand SaveUserInterfaceSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand SetDefaultUserInterfaceSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand SaveInputSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ActivateOneViewportCommand
        {
            get;
            private set;
        }

        public RelayCommand ActivateFourViewportsCommand
        {
            get;
            private set;
        }

        public RelayCommand SetDefaultInputSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleShowProjectHierarchyCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowLightSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand ShowSkyBoxSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenProjectCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugMenuCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugShowBillboardsCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugUpdateMouseRaysCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugShowBroadPhaseCollisionCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugHighlightMouseHoveredObjectsCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleDebugShowPickableMeshesCommand
        {
            get;
            private set;
        }

        public RelayCommand HideLightSettingsCommand
        {
            get;
            private set;
        }

        public RelayCommand TestPickableObjectCommand
        {
            get;
            private set;
        }

        public RelayCommand ToggleShowPathNodesCommand
        {
            get;
            private set;
        }

        public RelayCommand<Window> CloseWindowCommand
        {
            get;
            private set;
        }

        public bool ShowingOneViewport
        {
            get
            {
                return viewportMode == ViewportMode.One_Perspective;
            }
        }

        public bool ShowingFourViewports
        {
            get
            {
                return viewportMode == ViewportMode.Four_One_Perspective_Three_Ortho || viewportMode == ViewportMode.Four_Perspective;
            }
        }

        public void UpdateAtLeastOneStaticObjectExists()
        {
            AtLeastOneStaticObjectExists = projectVm.SelectedStaticObjectIndex >= 0;
        }

        public List<IMonoViewportViewModelWrapper> ViewportViewModels { get => baseViewModel.ViewportViewModels; }

        public int Width { get => width; set { width = value; RaisePropertyChanged(); } }
        public int Height { get => height; set { height = value; RaisePropertyChanged(); } }

        public bool AtLeastOneStaticObjectExists { get => atLeastOneStaticObjectExists; set { atLeastOneStaticObjectExists = value; RaisePropertyChanged(); } }

        public int CurrentSettingsPanelId { get => currentSettingsPanelId; set { currentSettingsPanelId = value; RaisePropertyChanged(); } }

        public bool ShowingProjectHierarchy
        {
            get { return showingProjectHierarchy; }
            set { showingProjectHierarchy = value; RaisePropertyChanged(); }
        }

        public ViewportMode Mode
        {
            get { return viewportMode; }
            set { viewportMode = value; }
        }


        public MonoViewportViewModel GetMonoViewportVm4
        {
            get
            {
                return baseViewModel.ViewportViewModels[4].GetBaseViewportVm();
            }
        }


        public MonoViewportViewModel GetMonoViewportVm5
        {
            get
            {
                return baseViewModel.ViewportViewModels[5].GetBaseViewportVm();
            }
        }

        public MonoViewportViewModel GetMonoViewportVm0
        {
            get
            {
                return baseViewModel.ViewportViewModels[0].GetBaseViewportVm();
            }
        }

        public MonoViewportViewModel GetMonoViewportVm1
        {
            get
            {
                return baseViewModel.ViewportViewModels[1].GetBaseViewportVm();
            }
        }

        public MonoViewportViewModel GetMonoViewportVm2
        {
            get
            {
                return baseViewModel.ViewportViewModels[2].GetBaseViewportVm();
            }
        }

        public MonoViewportViewModel GetMonoViewportVm3
        {
            get
            {
                return baseViewModel.ViewportViewModels[3].GetBaseViewportVm();
            }
        }

        private void UpdateViewportVisibility()
        {
            if (viewportMode == ViewportMode.One_Perspective)
            {
                GetMonoViewportVm0.Visible = false;
                GetMonoViewportVm1.Visible = false;
                GetMonoViewportVm2.Visible = false;
                GetMonoViewportVm3.Visible = false;
                GetMonoViewportVm4.Visible = true;
            }
            else if (viewportMode == ViewportMode.Four_Perspective)
            {
                GetMonoViewportVm0.Visible = true;
                GetMonoViewportVm1.Visible = true;
                GetMonoViewportVm2.Visible = true;
                GetMonoViewportVm3.Visible = true;
                GetMonoViewportVm4.Visible = false;
            }
        }

        public ObservableCollection<GameItemObjectViewModel> GameItemObjectVms { get => gameItemObjectVms; set { gameItemObjectVms = value; RaisePropertyChanged(); } }

        public MonoEngineCore EngineCore { get => baseViewModel.EngineCore; }
        public List<MonoViewportViewModel> BaseViewportViewModels { get => baseViewModel.BaseViewportViewModels; }
        public MonoEngineMainViewModel BaseViewModel { get => baseViewModel; }
        public DefaultEditorLmbAction LmbAction { get => editorLmbAction; set => editorLmbAction = value; }
        public MapEditorMeshContainer MeshContainer { get => meshContainer; }
        public PathNodeViewModel PathNodeVm { get => pathNodeVm; }
        public bool ShowViewports { get => showViewports;
            set
            {
                showViewports = value;
                foreach (var monoViewport in ViewportViewModels)
                {
                    monoViewport.GetBaseViewportVm().Visible = value;
                }
                RaisePropertyChanged();
                UpdateViewportVisibility();
            }
        }

        public bool ShowingGrid { get => showingGrid; set { showingGrid = value; RaisePropertyChanged(); EngineCore.EngineSettings.ShowGrid = value; } }

        public List<Window> AllWindows { get => allWindows; }
        public MapEditorWindowSettingsViewModel WindowSettingsCollectionViewModel { get => windowSettingsCollectionViewModel; }
        public MiscSettingsViewModel MiscSettingsVm { get => miscSettingsVm; }
        public MouseController MouseController { get => mouseController; }
        public LinkManagerObject LinkManager { get => linkManager; }
        public string MainWindowTitle { get => mainWindowTitle; set { mainWindowTitle = value; RaisePropertyChanged(); } }

        public KeyBindConfirmationWindow KeyBindConfirmationWindow { get => keyBindConfirmationWindow; }

        public void SaveWindowSettings()
        {
            windowSettingsCollectionViewModel.SaveWindowSettings();
        }
    }
}