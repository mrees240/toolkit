﻿using ColladaLibrary.Utility;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MapEditor.Project;
using MapEditor.Render;
using MapEditor.ViewModel.Game;
using MapEditor.ViewModel.Project;
using MapEditor.Views.FileDialogs;
using Microsoft.Win32;
using MonoEngine.Code;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TMLibrary.Lib.Map.Project;
using TMLibrary.Lib.Util;

namespace MapEditor.ViewModel
{
    public class MapEditorProjectViewModel : ViewModelBase
    {
        private string projectName;
        private MapEditorViewModelLocator vml;
        private MapEditorProject project;
        private MapEditorProjectController projectController;
        private ObservableCollection<StaticObjectDefinitionViewModel> staticObjectVms;
        private LightViewModel sunLightVm;
        private LightViewModel ambientLightVm;
        private bool canAddStaticObjectToScene;
        private MouseControllerBase mouseController;
        private MapEditorProjectDialog projectDialog;
        private ObservableCollection<ObjectInstanceViewModel> objectInstanceVms;
        private ObjectInstanceViewModel rootNodeVm;
        private DefaultEditorLmbAction editorLmbAction;
        private SkyBoxSettingsViewModel skyBoxSettingsVm;

        private RayCollisionFilter pathNodeFilter = new RayCollisionFilter();
        private RayCollisionFilter itemRayCastPlacementFilter = new RayCollisionFilter();

        private int selectedStaticObjectIndex;
        private int selectedStaticObjectDefinitionIndex;
        private MapEditorMeshContainer meshContainer;

        public MapEditorProjectViewModel()
        {
            vml = new MapEditorViewModelLocator();
            objectInstanceVms = new ObservableCollection<ObjectInstanceViewModel>();
            projectDialog = new MapEditorProjectDialog();
            project = new MapEditorProject();
            projectController = new MapEditorProjectController(project);
            staticObjectVms = new ObservableCollection<StaticObjectDefinitionViewModel>();
            sunLightVm = new LightViewModel(project.SunLight);
            ambientLightVm = new LightViewModel(project.AmbientLight);
            rootNodeVm = new ObjectInstanceViewModel(project.RootNode, meshContainer, this, mouseController, editorLmbAction, new MapEditor.Settings.MiscSettings());
            pathNodeFilter.ExcludeVisibleInEditorOnly = true;
            itemRayCastPlacementFilter.ExcludeIgnoreItemRayCastPlacement = true;
            itemRayCastPlacementFilter.ExcludeVisibleInEditorOnly = true;
            skyBoxSettingsVm = new SkyBoxSettingsViewModel(vml.FontSettingsVm, this);

            InitializeCommands();
        }

        private void EditorLmbAction_SelectionChanged(object sender, EventArgs e)
        {
            var prev = selectedObjectInstanceVm;
            ObjectInstanceSceneObject selectedObject = editorLmbAction.PickedObject as ObjectInstanceSceneObject;

            if (selectedObject == null)
            {
                SelectedObjectInstanceVm = null;
            }
            else
            {
                for (int iObjVm = 0; iObjVm < objectInstanceVms.Count; iObjVm++)
                {
                    var objVm = objectInstanceVms[iObjVm];

                    if (selectedObject.ObjectInstanceVm == objVm)
                    {
                        SelectedObjectInstanceVm = selectedObject.ObjectInstanceVm;
                        break;
                    }
                }
            }
        }

        public void InitializeCommands()
        {
            SaveProjectCommand = new RelayCommand(() =>
            {
                try
                {
                    var dialog = projectDialog.SaveProjectFileDialog();

                    bool? opened = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                    if (projectController == null)
                    {
                        projectController = new MapEditorProjectController(project);
                    }

                    if (opened == true)
                    {
                        projectController.SaveProjectToFile(dialog.FileName);
                        ProjectName = dialog.FileName;
                        vml.MainVm.UpdateWindowTitle();
                    }
                }
                catch (Exception ex)
                {
                    string promptMessage = LogFileUtility.CreateErrorMessage("An error occured while saving the level.");
                    LogFileUtility.AppendToLogFile(ex.StackTrace);
                    MessageBox.Show(promptMessage);
                }
            });
        }

        public void AddObjectInstance(ObjectInstanceViewModel parent, ObjectInstanceViewModel objectInstanceVm)
        {
            ObjectInstance parentInstance = null;

            if (parent == null)
            {
                parent = rootNodeVm;
            }
            parentInstance = parent.ObjectInstance;

            projectController.AddObjectInstance(parentInstance, objectInstanceVm.ObjectInstance);
            ObjectInstanceVms.Add(objectInstanceVm);
        }

        public void AddStaticObjectDefinition(StaticObjectDefinition staticObject)
        {
            var staticObjectVm = new StaticObjectDefinitionViewModel(staticObject);
            project.AddStaticObjectDefinition(staticObject);
            StaticObjectVms.Add(staticObjectVm);
            SelectedStaticObjectIndex = staticObjectVms.Count() - 1;
            UpdateAtLeastOneStaticObjectExists();
        }

        public void RemoveStaticObject(StaticObjectDefinitionViewModel staticObjectVm)
        {
            RemoveInstancesOfStaticObject(staticObjectVm);
            StaticObjectVms.Remove(staticObjectVm);

            // Remove in model
            project.RemoveStaticObjectDefinition(staticObjectVm.StaticObject);
            
            UpdateAtLeastOneStaticObjectExists();
        }

        private void RemoveInstancesOfStaticObject(StaticObjectDefinitionViewModel staticObjectVm)
        {
            var instancesToRemove = objectInstanceVms.Where(o => o.ObjectInstance.StaticObject == staticObjectVm.StaticObject).ToList();
            
            while (instancesToRemove.Any())
            {
                var last = instancesToRemove.Last();
                DeleteObjectInstance(last);
                objectInstanceVms.Remove(last);
                instancesToRemove.Remove(last);
            }
        }

        private void AddObjectInstanceViewModels(ObjectInstanceViewModel objectInstanceVm, List<ObjectInstanceViewModel> objectInstanceVms)
        {
            objectInstanceVms.Add(objectInstanceVm);

            for (int iChild = 0; iChild < objectInstanceVm.ChildrenAmount; iChild++)
            {
                var child = objectInstanceVm.GetChildVm(iChild);
                AddObjectInstanceViewModels(child, objectInstanceVms);
            }
        }

        public ObservableCollection<StaticObjectDefinitionViewModel> StaticObjectVms { get => staticObjectVms; }

        public void OpenProject(string fileUrl, Version appVersion, MapEditorProjectDefinition definition)
        {
#if DEBUG
            var result = projectController.OpenProjectFile(fileUrl, appVersion, definition);
            if (result.Succeeded)
            {
                CreateAllViewmodels();
                InitializeSceneVms();
                UpdateAllMeshes();
                skyBoxSettingsVm.UpdateSkyBox();
                ProjectName = FilePathUtility.GetSafeFileName(fileUrl);
            }
            else
            {
                string promptMessage = LogFileUtility.CreateErrorMessage("An error occured while loading the level.");
                LogFileUtility.AppendToLogFile(result.Messages);
                MessageBox.Show(promptMessage);
            }
#else
            try
            {
                projectController.OpenProjectFile(fileUrl, appVersion, definition);
                CreateAllViewmodels();
                InitializeSceneVms();
                UpdateAllMeshes();
                ProjectName = FilePathUtility.GetSafeFileName(fileUrl);
            }
            catch (Exception ex)
            {
                string promptMessage = LogFileUtility.CreateErrorMessage("An error occured while loading the level.");
                LogFileUtility.AppendToLogFile(ex.Message);
                MessageBox.Show(promptMessage);
            }
#endif
        }

        private void InitializeSceneVms()
        {
            if (project.StaticObjectDefinitionContainer.StaticObjectDefinitions.Any())
            {
                SelectedStaticObjectDefinitionIndex = 0;
            }
            else
            {
                SelectedStaticObjectDefinitionIndex = -1;
            }
            if (StaticObjectVms.Any())
            {
                vml.StaticObjectManagerVm.SelectedStaticObjectIndex = 0;
                vml.MainVm.AtLeastOneStaticObjectExists = true;
                vml.MapEditorProjectVm.SelectedStaticObjectIndex = 0;
            }
            else
            {
                vml.MapEditorProjectVm.SelectedStaticObjectIndex = -1;
                vml.StaticObjectManagerVm.SelectedStaticObjectIndex = -1;
                vml.MainVm.AtLeastOneStaticObjectExists = false;
            }
        }

        private void UpdateAllMeshes()
        {
            foreach (var staticObject in staticObjectVms)
            {
                staticObject.StaticObject.UpdateMeshes();
            }
        }

        private void CreateAllViewmodels()
        {
            if (meshContainer == null)
            {
                meshContainer = vml.MainVm.MeshContainer;
            }
            ObjectInstanceVms.Clear();
            StaticObjectVms.Clear();
            var objectInstances = project.GetAllObjectInstances();

            mouseController = vml.MainVm.MouseController;

            rootNodeVm = new ObjectInstanceViewModel(project.RootNode, meshContainer, this, mouseController, editorLmbAction, vml.MainVm.MiscSettingsVm.MiscSettings);

            foreach (var staticObjectDefinition in project.StaticObjectDefinitionContainer.StaticObjectDefinitions)
            {
                var staticObjectVm = new StaticObjectDefinitionViewModel(staticObjectDefinition);
                StaticObjectVms.Add(staticObjectVm);
            }

            foreach (var objectInstance in objectInstances)
            {
                foreach (var child in objectInstance.Children)
                {
                    var objectInstanceVm = new ObjectInstanceViewModel(child, meshContainer, this, mouseController, editorLmbAction, vml.MainVm.MiscSettingsVm.MiscSettings);
                    ObjectInstanceVms.Add(objectInstanceVm);
                    vml.MainVm.EngineCore.GameScene.AddObject(objectInstanceVm.SceneObject, true);
                    if (objectInstanceVm.ObjectInstance.ObjectType == ObjectType.PathNode)
                    {
                        InitializePathNodeSceneObject(objectInstanceVm.ObjectInstance.PathNode.Name, objectInstanceVm.SceneObject);
                    }
                }
            }
            foreach (var connection in project.PathNodeConnections)
            {
                vml.MainVm.LinkManager.AddConnection(connection);
            }
        }

        public void AddPathConnectionToProject(PathNodeConnection connection)
        {
            vml.MainVm.LinkManager.AddConnection(connection);
            project.PathNodeConnections.Add(connection);
        }

        public void AddStaticObjectInstance()
        {
            int id = project.GetNextIdNumber(ObjectType.StaticObject);
            var objectDefinitionVm = StaticObjectVms[SelectedStaticObjectDefinitionIndex];
            var objectInstance = new ObjectInstance(objectDefinitionVm.StaticObject, CreateNewInstanceName(ObjectType.StaticObject, id), id);
            var objectInstanceVm = new ObjectInstanceViewModel(objectInstance, meshContainer, this, mouseController, editorLmbAction, vml.MainVm.MiscSettingsVm.MiscSettings);
            AddObjectInstance(RootNodeVm, objectInstanceVm);
            SelectedObjectInstanceVm = objectInstanceVm;
            vml.MainVm.EngineCore.GameScene.AddObject(objectInstanceVm.SceneObject, true);
            editorLmbAction.PickedObject = objectInstanceVm.SceneObject;
        }

        public void AddGameItemInstance(GameItemObjectViewModel obj)
        {
            AddGameItemInstance(obj, new Vector3());
        }

        public void AddGameItemInstance(GameItemObjectViewModel obj, Vector3 position)
        {
            int id = project.GetNextIdNumber(ObjectType.GameItemObject);
            var objectInstance = new ObjectInstance(obj.GameItemObject, CreateNewInstanceName(ObjectType.GameItemObject, id), id);
            objectInstance.Transformation.SetLocalPosition(position);
            var objectInstanceVm = new ObjectInstanceViewModel(objectInstance, meshContainer, this, mouseController, editorLmbAction, vml.MainVm.MiscSettingsVm.MiscSettings);
            AddObjectInstance(RootNodeVm, objectInstanceVm);
            SelectedObjectInstanceVm = objectInstanceVm;
            vml.MainVm.EngineCore.GameScene.AddObject(objectInstanceVm.SceneObject, true);
            editorLmbAction.PickedObject = objectInstanceVm.SceneObject;
        }

        public void UpdateAtLeastOneStaticObjectExists()
        {
            vml.MainVm.UpdateAtLeastOneStaticObjectExists();
        }

        public void AddPathNodeInstance()
        {
            AddPathNodeInstance(new Vector3(), true);
        }

        public void MoveSelectedObjectMouseRay()
        {
            if (selectedObjectInstanceVm != null)
            {
                var mouseRayCollisionResult = editorLmbAction.FindMouseRayIntersectionResult(pathNodeFilter);

                if (mouseRayCollisionResult.Intersects)
                {
                    selectedObjectInstanceVm.ObjectInstance.Transformation.SetLocalPosition(mouseRayCollisionResult.IntersectionCoordinate);
                }
            }
        }

        public void AddPathNodeInstanceMouseRay()
        {
            var mouseRayCollisionResult = editorLmbAction.FindMouseRayIntersectionResult(pathNodeFilter);

            if (mouseRayCollisionResult != null && mouseRayCollisionResult.Intersects)
            {
                AddPathNodeInstance(mouseRayCollisionResult.IntersectionCoordinate, true);
            }
        }

        public void AddGameItemInstanceMouseRay(GameItemObjectViewModel gameItem)
        {
            var mouseRayCollisionResult = editorLmbAction.FindMouseRayIntersectionResult(itemRayCastPlacementFilter);

            if (mouseRayCollisionResult != null && mouseRayCollisionResult.Intersects)
            {
                AddGameItemInstance(gameItem, mouseRayCollisionResult.IntersectionCoordinate);
            }
        }

        public void AddPathNodeInstance(Vector3 position, bool autoSelect)
        {
            int id = project.GetNextIdNumber(ObjectType.PathNode);
            LoadMeshContainer();
            string instanceName = CreateNewInstanceName(ObjectType.PathNode, id);
            var pathNode = new PathNode(instanceName);
            var objectInstance = new ObjectInstance(RootNodeVm.ObjectInstance, pathNode, instanceName, id);
            objectInstance.Transformation.PositionLocal = position;
            var objectInstanceVm = new ObjectInstanceViewModel(objectInstance, meshContainer, this, mouseController, editorLmbAction, vml.MainVm.MiscSettingsVm.MiscSettings);
            AddObjectInstance(RootNodeVm, objectInstanceVm);
            if (autoSelect)
            {
                editorLmbAction.PickedObject = objectInstanceVm.SceneObject;
                objectInstanceVm.IsSelected = true;
                SelectedObjectInstanceVm = objectInstanceVm;
            }
            vml.MainVm.LinkManager.CreateSceneObjectForPoint(objectInstanceVm);
            var child = objectInstanceVm.SceneObject.GetDrawableChildren()[0] as GenericSceneObject;
            child.Owner = objectInstanceVm;
            InitializePathNodeSceneObject(pathNode.Name, child);
            vml.MainVm.EngineCore.GameScene.AddObject(objectInstanceVm.SceneObject, true);
        }

        private void InitializePathNodeSceneObject(string name, GenericSceneObject pathNodeObject)
        {
            pathNodeObject.LinkChanged += PathNodeObject_LinkChanged;
            pathNodeObject.GetObjectState().Name = name;
        }

        private void PathNodeObject_LinkChanged(object sender, EventArgs e)
        {
            var p0 = sender as GenericSceneObject;
            var p1 = p0.LinkObject as GenericSceneObject;

            if (p1 != null)
            {
                if (p0 != p1)
                {
                    var vm0 = p0.Owner as ObjectInstanceViewModel;
                    var vm1 = p1.Owner as ObjectInstanceViewModel;

                    if (vm0 != null && vm1 != null && vm0 != vm1)
                    {
                        if (vm0.ObjectInstance.ObjectType == ObjectType.PathNode && vm1.ObjectInstance.ObjectType == ObjectType.PathNode)
                        {
                            var connections = project.PathConnectionContainer.PathNodeConnections.Where(c => c.P0 == vm0.ObjectInstance);

                            var connection = new PathNodeConnection(vm0.ObjectInstance, vm1.ObjectInstance, PathConnectionDirection.TwoWay);
                            AddPathConnectionToProject(connection);
                        }
                    }
                }
            }
        }

        public void DeleteObjectInstance(ObjectInstanceViewModel selectedObject)
        {
            if (selectedObject.ObjectInstance.ObjectType == ObjectType.PathNode)
            {
                //var connections = project.PathConnectionContainer.PathNodeConnections.Where(c => c.P0 == selectedObject.ObjectInstance || c.P1 == selectedObject.ObjectInstance).ToList();

                var allConnections = project.GetPathNodeConnections();
                var connections = allConnections.Where(c => c.P0 == selectedObject.ObjectInstance || c.P1 == selectedObject.ObjectInstance).ToList();

                while (connections.Any())
                {
                    var connection = connections.First();
                    //connections.Remove(connection);
                    vml.MainVm.LinkManager.RemoveConnection(connection);
                    connections.Remove(connection);
                }

            }
            objectInstanceVms.Remove(selectedObjectInstanceVm);

            var sceneObj = selectedObject.SceneObject.GetRootObject();
            sceneObj.GetDrawState().IsVisible = false;
            vml.MainVm.EngineCore.GameScene.RemoveObject(sceneObj);
            project.RemoveObjectInstance(rootNodeVm.ObjectInstance, selectedObject.ObjectInstance);
            vml.MainVm.LmbAction.ResetSelectedObject();
            SelectedObjectInstanceVm = null;
        }

        private string CreateNewInstanceName(ObjectType objectType, int id)
        {
            switch (objectType)
            {
                case ObjectType.StaticObject:
                    return $"Static Object {id}";
                case ObjectType.GameItemObject:
                    return $"Game Item {id}";
                case ObjectType.PathNode:
                    return $"Path Node {id}";
            }

            return "NO_NAME";
        }

        private void LoadMeshContainer()
        {
            if (meshContainer == null)
            {
                meshContainer = vml.MainVm.MeshContainer;
            }
        }

        public MapEditorProject Project { get => project; }

        private ObjectInstanceViewModel selectedObjectInstanceVm;
        public ObjectInstanceViewModel SelectedObjectInstanceVm
        {
            get
            {
                return selectedObjectInstanceVm;
            }
            set
            {
                if (selectedObjectInstanceVm != null && selectedObjectInstanceVm != value)
                {
                    selectedObjectInstanceVm.IsSelected = false;
                    selectedObjectInstanceVm.ChildObject.GetObjectState().IsSelected = false;
                }
                selectedObjectInstanceVm = value;
                if (selectedObjectInstanceVm != null)
                {
                    editorLmbAction.PickedObjectTransformationVm = selectedObjectInstanceVm.TransformationVm;
                    selectedObjectInstanceVm.IsSelected = true;
                    selectedObjectInstanceVm.ChildObject.GetObjectState().IsSelected = true;
                }
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(HasSelectedObjectInstance));
            }
        }

        public bool HasSelectedObjectInstance { get { return selectedObjectInstanceVm != null; }  }

        public void StartNewProject(MouseControllerBase mouseController)
        {
            this.mouseController = mouseController;
            ObjectInstanceVms.Clear();
            project.ResetProject();
            skyBoxSettingsVm.SetDefaultSettings();
            SetDefaultProjectName();
        }

        public int SelectedStaticObjectDefinitionIndex
        {
            get
            {
                return selectedStaticObjectDefinitionIndex;
            }
            set
            {
                selectedStaticObjectDefinitionIndex = value;
                RaisePropertyChanged();
            }
        }

        public int SelectedStaticObjectIndex
        {
            get { return selectedStaticObjectIndex; }
            set
            {
                selectedStaticObjectIndex = value;
                RaisePropertyChanged();
                if (selectedStaticObjectIndex >= 0 && selectedStaticObjectIndex < objectInstanceVms.Count())
                {
                    SelectedObjectInstanceVm = objectInstanceVms[selectedStaticObjectIndex];
                }
                else
                {
                    SelectedObjectInstanceVm = null;
                }
            }
        }

        public RelayCommand SaveProjectCommand
        {
            get;
            private set;
        }

        public void SetDefaultProjectName()
        {
            ProjectName = "New Project";
        }

        public ObjectInstanceViewModel GetObjectInstanceViewModel(ObjectInstance instance)
        {
            return objectInstanceVms.Where(o => o.ObjectInstance == instance).FirstOrDefault();
        }

        public LightViewModel SunLightVm { get => sunLightVm; }
        public LightViewModel AmbientLightVm { get => ambientLightVm; }
        public ObjectInstanceViewModel RootNodeVm { get => rootNodeVm; }
        public ObservableCollection<ObjectInstanceViewModel> ObjectInstanceVms { get => objectInstanceVms; }
        public DefaultEditorLmbAction EditorLmbAction
        {
            get => editorLmbAction;
            set
            {
                editorLmbAction = value;
                editorLmbAction.SelectionChanged += EditorLmbAction_SelectionChanged;
            }
        }

        public string ProjectName { get => projectName; set { projectName = value; RaisePropertyChanged(); } }

        public SkyBoxSettingsViewModel SkyBoxSettingsVm { get => skyBoxSettingsVm; }
    }
}
