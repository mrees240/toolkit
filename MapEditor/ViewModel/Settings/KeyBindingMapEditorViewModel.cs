﻿using GalaSoft.MvvmLight.Command;
using MapEditor.Services;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MapEditor.ViewModel.Settings
{
    public class KeyBindingMapEditorViewModel : KeyBindingViewModel
    {
        private MainViewModel mainVm;

        public KeyBindingMapEditorViewModel(KeyBinding keyBinding, string actionDescription, MainViewModel mainVm) :
            base(keyBinding, actionDescription, new MapEditorViewModelLocator().FontSettingsVm.Model, 13.0)
        {
            this.mainVm = mainVm;
            keyBinding.KeyBindingChanged += KeyBinding_KeyBindingChanged;
        }

        private void KeyBinding_KeyBindingChanged(object sender, EventArgs e)
        {
            mainVm.UpdateKeyBindings();
        }

        public new RelayCommand RemapCommand
        {
            get
            {
                if (remapCommand == null)
                {
                    remapCommand = new GalaSoft.MvvmLight.Command.RelayCommand(() =>
                    {
                        new MainViewModelServices(mainVm).OpenKeyBindConfirmationWindow(this);
                    });
                }
                return remapCommand;
            }
        }
    }
}
