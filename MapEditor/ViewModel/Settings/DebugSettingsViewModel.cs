﻿using GalaSoft.MvvmLight;
using MonoEngine.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.ViewModel.Settings
{
    public class DebugSettingsViewModel : ViewModelBase
    {
        private MonoEngine.Code.EngineSettings settings;
        private bool showDebugMenu;

        public DebugSettingsViewModel()
        {

        }

        public bool ShowPickableMeshes { get => settings.DebugShowPickableMeshes; set { settings.DebugShowPickableMeshes = value; RaisePropertyChanged(); } }

        public bool ShowDebugMenu { get => showDebugMenu; set { showDebugMenu = value; RaisePropertyChanged(); } }

        public void ToggleShowDebugMenu()
        {
            ShowDebugMenu = !showDebugMenu;
        }

        public bool DebugUpdateMouseRays { get => settings.DebugUpdateMouseRays; set { settings.DebugUpdateMouseRays = value; RaisePropertyChanged(); } }
        public EngineSettings Settings { get => settings; set => settings = value; }
        public bool DebugShowBroadPhaseCollision { get { return settings.DebugShowBroadPhaseCollision; } set { settings.DebugShowBroadPhaseCollision = value; RaisePropertyChanged(); } }
        public bool ShowBillboards { get => settings.DebugShowBillboards; set { settings.DebugShowBillboards = value; RaisePropertyChanged(); } }
        public bool DebugHighlightMouseHoveredObjects { get => settings.DebugHighlightMouseHoveredObjects; set { settings.DebugHighlightMouseHoveredObjects = value; RaisePropertyChanged(); } }
    }
}
