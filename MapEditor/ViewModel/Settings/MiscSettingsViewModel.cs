﻿using GalaSoft.MvvmLight;
using MapEditor.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.ViewModel.Settings
{
    public class MiscSettingsViewModel : ViewModelBase
    {
        private MiscSettings miscSettings;

        public MiscSettingsViewModel()
        {
            this.miscSettings = new MiscSettings();
        }

        public void SaveMiscSettings()
        {
            miscSettings.WriteToFile();
        }

        public bool ShowPathNodes { get { return miscSettings.ShowPathNodes; } set { miscSettings.ShowPathNodes = value; RaisePropertyChanged(); } }
        public double SelectedObjectInstancePanelWidth { get => miscSettings.SelectedObjectInstancePanelWidth; set { miscSettings.SelectedObjectInstancePanelWidth = value; RaisePropertyChanged(); } }

        public MiscSettings MiscSettings { get => miscSettings; }
    }
}
