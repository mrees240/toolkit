﻿using GalaSoft.MvvmLight.Command;
using MapEditor.Services;
using MapEditor.Settings;
using MonoEngine.Input;
using MonoEngine.Services;
using MonoEngine.ViewModel;
using MonoEngine.Views.InputSettings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MapEditor.ViewModel.Settings
{
    public class KeyBindSettingsViewModel
    {
        private KeyBindSettings keyBindSettings;
        private KeyBindingMapEditorViewModel bindingPlacePathNode;
        private KeyBindingMapEditorViewModel bindingSnapToGround;
        private KeyBindingMapEditorViewModel bindingLinkObjects;
        private KeyBindingMapEditorViewModel bindingMoveSelectedObjectMouseRay;
        private KeyBindingMapEditorViewModel bindingToggleDebugMenu;
        private KeyBindingMapEditorViewModel bindingToggleObject;
        private KeyBindingMapEditorViewModel bindingDeleteObject;
        // TM2
        private KeyBindingMapEditorViewModel bindingTm2Health;
        private KeyBindingMapEditorViewModel bindingTm2Turbo;
        private KeyBindingMapEditorViewModel bindingTm2FireMissle;
        private KeyBindingMapEditorViewModel bindingTm2HomingMissle;
        private KeyBindingMapEditorViewModel bindingTm2Napalm;
        private KeyBindingMapEditorViewModel bindingTm2PowerMissle;
        private KeyBindingMapEditorViewModel bindingTm2Remote;
        private KeyBindingMapEditorViewModel bindingTm2Ricochet;

        private KeyBindingMapEditorViewModel[] keyBinds;
        public event EventHandler RemappingModeChanged;

        private bool remappingMode;
        private MainViewModel mainVm;

        public void Initialize(MainViewModel mainVm)
        {
            this.mainVm = mainVm;
            this.keyBindSettings = new KeyBindSettings();
            bindingPlacePathNode = new KeyBindingMapEditorViewModel(keyBindSettings.BindingPlacePathNode, "Place Path Node", mainVm);
            bindingSnapToGround = new KeyBindingMapEditorViewModel(keyBindSettings.BindingSnapToGround, "Snap to Ground", mainVm);
            bindingLinkObjects = new KeyBindingMapEditorViewModel(keyBindSettings.BindingLinkObjects, "Link Objects", mainVm);
            bindingMoveSelectedObjectMouseRay = new KeyBindingMapEditorViewModel(keyBindSettings.BindingMoveSelectedObjectInstanceMouseRay, "Move Selection", mainVm);
            bindingToggleObject = new KeyBindingMapEditorViewModel(keyBindSettings.BindingToggleObject, "Toggle Object", mainVm);
            bindingDeleteObject = new KeyBindingMapEditorViewModel(keyBindSettings.BindingDeleteObject, "Delete Object", mainVm);

            // TM2
            bindingTm2Health = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2Health, "Health", mainVm);
            bindingTm2Turbo = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2Turbo, "Turbo", mainVm);
            bindingTm2FireMissle = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2FireMissle, "Fire Missle", mainVm);
            bindingTm2HomingMissle = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2HomingMissle, "Homing Missle", mainVm);
            bindingTm2Napalm = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2Napalm, "Napalm", mainVm);
            bindingTm2PowerMissle = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2PowerMissle, "Power Missle", mainVm);
            bindingTm2Remote = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2Remote, "Remote", mainVm);
            bindingTm2Ricochet = new KeyBindingMapEditorViewModel(keyBindSettings.BindingTm2Ricochet, "Ricochet", mainVm);

            bindingToggleDebugMenu = new KeyBindingMapEditorViewModel(keyBindSettings.BindingToggleDebugMenu, "Show/Hide Debug Menu", mainVm);

            keyBinds = new KeyBindingMapEditorViewModel[]
            {
                bindingPlacePathNode,
                bindingSnapToGround,
                bindingLinkObjects,
                bindingMoveSelectedObjectMouseRay,
                bindingToggleObject,
                bindingDeleteObject,
                bindingTm2Health,
                bindingTm2Turbo,
                bindingTm2FireMissle,
                bindingTm2HomingMissle,
                bindingTm2Napalm,
                bindingTm2PowerMissle,
                bindingTm2Remote,
                bindingTm2Ricochet,
                bindingToggleDebugMenu,
            };
            foreach (var binding in keyBinds)
            {
                binding.KeyBinding.KeyBindingChanged += KeyBinding_KeyBindingChanged;
            }
            InitializeCommands();
            keyBindSettings.SetDefaultSettings();
        }

        private void KeyBinding_KeyBindingChanged(object sender, EventArgs e)
        {
            SaveSettings();
        }

        public KeyBindSettingsViewModel()
        {
        }

        public virtual void OnRemappingModeChanged()
        {
            if (RemappingModeChanged != null)
            {
                RemappingModeChanged(this, EventArgs.Empty);
            }
        }

        private void EndAllRemappingCommands()
        {
            foreach (var binding in keyBinds)
            {
                binding.EndRemappingMode();
            }
            RemappingMode = false;
        }

        public void InitializeCommands()
        {
            BindPlacePathNodeCommand = new RelayCommand(() =>
            {
                bindingPlacePathNode.StartRemappingMode();
            });
            BindingWindowClosedCommand = new RelayCommand(() =>
            {
                EndAllRemappingCommands();
            });
        }

        public void SaveSettings()
        {
            keyBindSettings.WriteToFile();
        }

        public void LoadSettings()
        {
            keyBindSettings.ReadFromFile();
        }

        public RelayCommand BindingWindowClosedCommand
        {
            get;
            private set;
        }

        public RelayCommand BindPlacePathNodeCommand
        {
            get;
            private set;
        }

        public string Path
        {
            get
            {
                return keyBindSettings.Path;
            }
        }

        public KeyBindingViewModel BindingPlacePathNode { get => bindingPlacePathNode; }
        public KeyBindingViewModel BindingSnapToGround { get => bindingSnapToGround; }
        public KeyBindingViewModel BindingLinkObjects { get => bindingLinkObjects; }
        public bool RemappingMode { get => remappingMode; private set { remappingMode = value; OnRemappingModeChanged(); } }

        public KeyBindingViewModel BindingMoveSelectedObjectMouseRay { get => bindingMoveSelectedObjectMouseRay; }
        public KeyBindingViewModel BindingToggleObject { get => bindingToggleObject; }
        public KeyBindingViewModel BindingDeleteObject { get => bindingDeleteObject; }
        public KeyBindingViewModel BindingTm2Health { get => bindingTm2Health; }
        public KeyBindingViewModel BindingTm2Turbo { get => bindingTm2Turbo; }
        public KeyBindingViewModel BindingTm2FireMissle { get => bindingTm2FireMissle; }
        public KeyBindingViewModel BindingTm2HomingMissle { get => bindingTm2HomingMissle; }
        public KeyBindingViewModel BindingTm2Napalm { get => bindingTm2Napalm; }
        public KeyBindingViewModel BindingTm2PowerMissle { get => bindingTm2PowerMissle; }
        public KeyBindingViewModel BindingTm2Remote { get => bindingTm2Remote; }
        public KeyBindingViewModel BindingTm2Ricochet { get => bindingTm2Ricochet; }
        public KeyBindingViewModel BindingToggleDebugMenu { get => bindingToggleDebugMenu; }
        public KeyBindingViewModel[] KeyBinds { get => keyBinds; }
    }
}
