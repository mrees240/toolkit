﻿using GalaSoft.MvvmLight;
using MapEditor.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;

namespace MapEditor.ViewModel.Settings
{
    public class UserInterfaceSettingsViewModel : ViewModelBase
    {
        private UserInterfaceSettings userInterfaceSettings;

        public UserInterfaceSettingsViewModel()
        {
            userInterfaceSettings = new UserInterfaceSettings();
            userInterfaceSettings.SetDefaultSettings();
            userInterfaceSettings.ReadFromFile();
        }

        public void SaveSettings()
        {
            userInterfaceSettings.WriteToFile();
        }

        public void ApplyDefaultSettings()
        {
            userInterfaceSettings.SetDefaultSettings();
            userInterfaceSettings.WriteToFile();
            UpdateUserIntefaceSettingsInUi();
        }

        private void UpdateUserIntefaceSettingsInUi()
        {
            RaisePropertyChanged(nameof(IconSize));
        }

        public double IconSize
        {
            get
            {
                return userInterfaceSettings.IconSize;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                userInterfaceSettings.IconSize = value;
                RaisePropertyChanged();
            }
        }
    }
}
