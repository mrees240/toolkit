﻿using GalaSoft.MvvmLight;
using MapEditor.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.ViewModel.Settings;

namespace MapEditor.ViewModel
{
    public class MapEditorWindowSettingsViewModel : ViewModelBase
    {
        private MapEditorWindowSettingsCollection windowSettingsCollection;
        private GenericWindowSettingsViewModel mapCompilerWindowSettingsVm;
        private GenericWindowSettingsViewModel settingsWindowSettingsVm;
        private GenericWindowSettingsViewModel lightSettingsWindowSettingsVm;
        private GenericWindowSettingsViewModel staticObjectManagerSettingsVm;

        public MapEditorWindowSettingsViewModel()
        {
            windowSettingsCollection = new MapEditorWindowSettingsCollection();
            windowSettingsCollection.SetDefaultSettings();
            windowSettingsCollection.ReadFromFile();
            mapCompilerWindowSettingsVm = new GenericWindowSettingsViewModel(windowSettingsCollection.MapCompilerWindowSettings);
            settingsWindowSettingsVm = new GenericWindowSettingsViewModel(windowSettingsCollection.SettingsWindowSettings);
            lightSettingsWindowSettingsVm = new GenericWindowSettingsViewModel(windowSettingsCollection.LightSettingsWindowSettings);
            staticObjectManagerSettingsVm = new GenericWindowSettingsViewModel(windowSettingsCollection.StaticObjectManagerWindowSettings);
        }

        public void LoadWindowSettings()
        {
            windowSettingsCollection.ReadFromFile();
        }

        public void SaveWindowSettings()
        {
            windowSettingsCollection.WriteToFile();
            windowSettingsCollection.ReadFromFile();
        }

        public GenericWindowSettingsViewModel MapCompilerWindowSettingsVm { get => mapCompilerWindowSettingsVm; }
        public GenericWindowSettingsViewModel SettingsWindowSettingsVm { get => settingsWindowSettingsVm; }
        public GenericWindowSettingsViewModel LightSettingsWindowSettingsVm { get => lightSettingsWindowSettingsVm; }
        public GenericWindowSettingsViewModel StaticObjectManagerSettingsVm { get => staticObjectManagerSettingsVm; }
    }
}
