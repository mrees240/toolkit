﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using TMLibrary.Lib.Map.Project;
using WpfCore.Utility;

namespace MapEditor.ViewModel.Game
{
    public class GameItemObjectViewModel : ViewModelBase
    {
        private GameItemDefinition gameItemObject;

        public GameItemObjectViewModel(GameItemDefinition gameItemObject)
        {
            this.gameItemObject = gameItemObject;
        }

        public string ImageIconUrl { get { return gameItemObject.ImageIconUrl; } }
        public string Name { get { return gameItemObject.Name; } }

        public GameItemDefinition GameItemObject { get => gameItemObject; }
    }
}
