﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;

namespace MapEditor.Settings
{
    public class UserInterfaceSettings : SettingsBase
    {
        private double iconSize;

        public UserInterfaceSettings()
        {
            SetDefaultSettings();
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(@"settings\user-interface-settings.bin", FileMode.Open)))
                {
                    iconSize = reader.ReadDouble();
                }
            }
            catch
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"settings\user-interface-settings.bin", FileMode.Create)))
            {
                writer.Write(iconSize);
            }
        }

        public void SetDefaultSettings()
        {
            iconSize = 28.0f;
        }

        public double IconSize { get => iconSize; set => iconSize = value; }
    }
}
