﻿using System;
using System.IO;
using WpfCore.Settings;

namespace MapEditor.Settings
{
    public class MapCompilerSettings : SettingsBase
    {
        private string safeLevelFilePath;
        private string safePointsFilePath;
        private string levelFilePath;
        private string pointsFilePath;
        private string outputDirectory;
        private bool defaultLighting;

        public string PointsFilePath { get => pointsFilePath; set { pointsFilePath = value; OnPropertyChanged(); } }
        public string LevelFilePath { get => levelFilePath; set { levelFilePath = value; OnPropertyChanged(); } }
        public string OutputDirectory { get => outputDirectory; set { outputDirectory = value; OnPropertyChanged(); } }
        public string SafeLevelFilePath { get => safeLevelFilePath; set => safeLevelFilePath = value; }
        public string SafePointsFilePath { get => safePointsFilePath; set => safePointsFilePath = value; }
        public bool DefaultLighting { get => defaultLighting; set { defaultLighting = value; OnPropertyChanged(); } }

        public MapCompilerSettings()
        {
            SetDefaultSettings();
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(@"settings\map-settings.bin", FileMode.Open)))
                {
                    levelFilePath = reader.ReadString();
                    safeLevelFilePath = reader.ReadString();
                    pointsFilePath = reader.ReadString();
                    safePointsFilePath = reader.ReadString();
                    outputDirectory = reader.ReadString();
                    DefaultLighting = reader.ReadBoolean();
                }
            }
            catch
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"settings\map-settings.bin", FileMode.Create)))
            {
                writer.Write(levelFilePath);
                writer.Write(safeLevelFilePath);
                writer.Write(pointsFilePath);
                writer.Write(safePointsFilePath);
                writer.Write(outputDirectory);
                writer.Write(defaultLighting);
            }
        }

        public void SetDefaultSettings()
        {
            LevelFilePath = string.Empty;
            PointsFilePath = string.Empty;
            OutputDirectory = string.Empty;
            SafeLevelFilePath = string.Empty;
            SafePointsFilePath = string.Empty;
            DefaultLighting = false;
        }
    }
}
