﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;

namespace MapEditor.Settings
{
    public class MapEditorWindowSettingsCollection
    {
        private GenericWindowSettings mapCompilerWindowSettings;
        private GenericWindowSettings settingsWindowSettings;
        private GenericWindowSettings staticObjectManagerWindowSettings;
        private GenericWindowSettings lightSettingsWindowSettings;
        private const string filePath = @"settings\window-settings.bin";

        public GenericWindowSettings MapCompilerWindowSettings { get => mapCompilerWindowSettings; }
        public GenericWindowSettings SettingsWindowSettings { get => settingsWindowSettings; }
        public GenericWindowSettings StaticObjectManagerWindowSettings { get => staticObjectManagerWindowSettings; }
        public GenericWindowSettings LightSettingsWindowSettings { get => lightSettingsWindowSettings; }

        public MapEditorWindowSettingsCollection()
        {
            mapCompilerWindowSettings = new GenericWindowSettings("Map Compiler");
            settingsWindowSettings = new GenericWindowSettings("Settings");
            staticObjectManagerWindowSettings = new GenericWindowSettings("Manage Static Objects");
            lightSettingsWindowSettings = new GenericWindowSettings("Light Settings");
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            SetDefaultSettings();
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
                {
                    mapCompilerWindowSettings.ReadFromFile(reader);
                    settingsWindowSettings.ReadFromFile(reader);
                    staticObjectManagerWindowSettings.ReadFromFile(reader);
                    lightSettingsWindowSettings.ReadFromFile(reader);
                }
            }
            catch
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void SetDefaultSettings()
        {

        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
            {
                mapCompilerWindowSettings.WriteToFile(writer);
                settingsWindowSettings.WriteToFile(writer);
                staticObjectManagerWindowSettings.WriteToFile(writer);
                lightSettingsWindowSettings.WriteToFile(writer);
            }
        }
    }
}
