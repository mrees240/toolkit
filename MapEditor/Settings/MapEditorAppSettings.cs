﻿using WpfCore.Settings;

namespace MapEditor.Settings
{
    /// <summary>
    /// Stores UI and control settings for the application.
    /// </summary>
    public class MapEditorAppSettings
    {
        private FontSettings fontSettings;
        private DisplaySettings displaySettings;
        private InputSettings inputSettings;
        private MapCompilerSettings mapCompilerSettings;
        private KeyBindSettings keyBindSettings;

        public MapEditorAppSettings(FontSettings fontSettings, DisplaySettings displaySettings, InputSettings inputSettings, MapCompilerSettings mapCompilerSettings,
            KeyBindSettings keyBindSettings)
        {
            this.fontSettings = fontSettings;
            this.displaySettings = displaySettings;
            this.inputSettings = inputSettings;
            this.mapCompilerSettings = mapCompilerSettings;
            this.keyBindSettings = keyBindSettings;
        }

        public void SaveSettings()
        {
            fontSettings.WriteToFile();
            displaySettings.WriteToFile();
            inputSettings.WriteToFile();
            mapCompilerSettings.WriteToFile();
        }

        public void LoadSettings()
        {
            fontSettings.ReadFromFile();
            displaySettings.ReadFromFile();
            inputSettings.ReadFromFile();
            mapCompilerSettings.ReadFromFile();
        }

        public FontSettings FontSettings { get => fontSettings; set => fontSettings = value; }
        public DisplaySettings DisplaySettings { get => displaySettings; set => displaySettings = value; }
        public InputSettings InputSettings { get => inputSettings; set => inputSettings = value; }
        public MapCompilerSettings MapCompilerSettings { get => mapCompilerSettings; set => mapCompilerSettings = value; }
    }
}
