﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MonoEngine.Input;

namespace MapEditor.Settings
{
    public class KeyBindSettings : SettingsBase
    {
        public readonly string Path = @"settings\key-bind-settings.bin";
        private MonoEngine.Input.KeyBinding[] keyBindings;
        private KeyConverter keyConverter;
        // Bindings
        private MonoEngine.Input.KeyBinding bindingPlacePathNode;
        private MonoEngine.Input.KeyBinding bindingSnapToGround;
        private MonoEngine.Input.KeyBinding bindingLinkObjects;
        private MonoEngine.Input.KeyBinding bindingMoveSelectedObjectInstanceMouseRay;
        private MonoEngine.Input.KeyBinding bindingToggleDebugMenu;
        private MonoEngine.Input.KeyBinding bindingToggleObject;
        private MonoEngine.Input.KeyBinding bindingDeleteObject;
        // TM2 Game Items
        private MonoEngine.Input.KeyBinding bindingTm2Health;
        private MonoEngine.Input.KeyBinding bindingTm2Turbo;
        private MonoEngine.Input.KeyBinding bindingTm2FireMissle;
        private MonoEngine.Input.KeyBinding bindingTm2HomingMissle;
        private MonoEngine.Input.KeyBinding bindingTm2Napalm;
        private MonoEngine.Input.KeyBinding bindingTm2PowerMissle;
        private MonoEngine.Input.KeyBinding bindingTm2Remote;
        private MonoEngine.Input.KeyBinding bindingTm2Ricochet;


        public KeyBindSettings()
        {
            keyConverter = new KeyConverter();
            bindingPlacePathNode = new MonoEngine.Input.KeyBinding(Key.A);
            bindingSnapToGround = new MonoEngine.Input.KeyBinding(Key.F);
            bindingLinkObjects = new MonoEngine.Input.KeyBinding(Key.LeftCtrl);
            bindingMoveSelectedObjectInstanceMouseRay = new MonoEngine.Input.KeyBinding(Key.S);
            bindingToggleDebugMenu = new MonoEngine.Input.KeyBinding(Key.F12);
            bindingToggleObject = new MonoEngine.Input.KeyBinding(Key.Q);
            bindingDeleteObject = new MonoEngine.Input.KeyBinding(Key.Delete);
            // TM2
            bindingTm2Health = new MonoEngine.Input.KeyBinding(Key.D1);
            bindingTm2Turbo = new MonoEngine.Input.KeyBinding(Key.D2);
            bindingTm2FireMissle = new MonoEngine.Input.KeyBinding(Key.D3);
            bindingTm2HomingMissle = new MonoEngine.Input.KeyBinding(Key.D4);
            bindingTm2Napalm = new MonoEngine.Input.KeyBinding(Key.D5);
            bindingTm2PowerMissle = new MonoEngine.Input.KeyBinding(Key.D6);
            bindingTm2Remote = new MonoEngine.Input.KeyBinding(Key.D7);
            bindingTm2Ricochet = new MonoEngine.Input.KeyBinding(Key.D8);

            keyBindings = new MonoEngine.Input.KeyBinding[]
            {
                bindingPlacePathNode,
                bindingSnapToGround,
                bindingLinkObjects,
                bindingMoveSelectedObjectInstanceMouseRay,
                bindingToggleDebugMenu,
                bindingToggleObject,
                bindingDeleteObject,
                bindingTm2Health,
                bindingTm2Turbo,
                bindingTm2FireMissle,
                bindingTm2HomingMissle,
                bindingTm2Napalm,
                bindingTm2PowerMissle,
                bindingTm2Remote,
                bindingTm2Ricochet,
            };
        }

        public void SetDefaultSettings()
        {
            foreach (var keyBinding in keyBindings)
            {
                keyBinding.SetDefaultSettings();
            }
        }

        public void ReadFromFile()
        {
            byte[] keyBindingBytes = File.ReadAllBytes(Path);
            using (var reader = new BinaryReader(new MemoryStream(keyBindingBytes)))
            {
                foreach (var keyBinding in keyBindings)
                {
                    keyBinding.ReadFromFile(reader);
                }
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");

            using (BinaryWriter writer = new BinaryWriter(File.Open(Path, FileMode.Create)))
            {
                foreach (var keyBinding in keyBindings)
                {
                    keyBinding.WriteToFile(writer);
                }
            }
        }

        public MonoEngine.Input.KeyBinding BindingPlacePathNode { get => bindingPlacePathNode; }
        public MonoEngine.Input.KeyBinding BindingSnapToGround { get => bindingSnapToGround; }
        public MonoEngine.Input.KeyBinding BindingLinkObjects { get => bindingLinkObjects; }
        public MonoEngine.Input.KeyBinding BindingMoveSelectedObjectInstanceMouseRay { get => bindingMoveSelectedObjectInstanceMouseRay; }
        public MonoEngine.Input.KeyBinding BindingToggleDebugMenu { get => bindingToggleDebugMenu; }
        public MonoEngine.Input.KeyBinding BindingToggleObject { get => bindingToggleObject; }
        public MonoEngine.Input.KeyBinding BindingDeleteObject { get => bindingDeleteObject; }
        public MonoEngine.Input.KeyBinding BindingTm2Health { get => bindingTm2Health; }
        public MonoEngine.Input.KeyBinding BindingTm2Turbo { get => bindingTm2Turbo; }
        public MonoEngine.Input.KeyBinding BindingTm2FireMissle { get => bindingTm2FireMissle; }
        public MonoEngine.Input.KeyBinding BindingTm2HomingMissle { get => bindingTm2HomingMissle; }
        public MonoEngine.Input.KeyBinding BindingTm2Napalm { get => bindingTm2Napalm; }
        public MonoEngine.Input.KeyBinding BindingTm2PowerMissle { get => bindingTm2PowerMissle; }
        public MonoEngine.Input.KeyBinding BindingTm2Remote { get => bindingTm2Remote; }
        public MonoEngine.Input.KeyBinding BindingTm2Ricochet { get => bindingTm2Ricochet; }
    }
}
