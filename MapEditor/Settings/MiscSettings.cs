﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WpfCore.Settings;

namespace MapEditor.Settings
{
    public class MiscSettings : SettingsBase
    {
        private bool showPathNodes;
        private double viewportFourLeftColumnWidth;
        private double viewportFourTopRowHeight;
        private double selectedObjectInstancePanelWidth;
        private const string filePath = @"settings\misc-settings.bin";

        public MiscSettings()
        {
            SetDefaultSettings();
            ReadFromFile();
        }

        private void SetDefaultSettings()
        {
            selectedObjectInstancePanelWidth = 200.0;
            showPathNodes = true;
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
                {
                    viewportFourLeftColumnWidth = reader.ReadDouble();
                    viewportFourTopRowHeight = reader.ReadDouble();
                    selectedObjectInstancePanelWidth = reader.ReadDouble();
                    showPathNodes = reader.ReadBoolean();
                }
            }
            catch
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
            {
                writer.Write(viewportFourLeftColumnWidth);
                writer.Write(viewportFourTopRowHeight);
                writer.Write(selectedObjectInstancePanelWidth);
                writer.Write(showPathNodes);
            }
        }

        public double ViewportFourLeftColumnWidth { get => viewportFourLeftColumnWidth; set => viewportFourLeftColumnWidth = value; }
        public double ViewportFourTopRowHeight { get => viewportFourTopRowHeight; set => viewportFourTopRowHeight = value; }
        public double SelectedObjectInstancePanelWidth { get => selectedObjectInstancePanelWidth; set => selectedObjectInstancePanelWidth = value; }
        public bool ShowPathNodes { get => showPathNodes; set => showPathNodes = value; }
    }
}
