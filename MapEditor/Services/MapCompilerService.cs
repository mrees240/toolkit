﻿using MapEditor.Project;
using MapEditor.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TMLibrary.Lib.Map;

namespace MapEditor.Services
{
    public class MapCompilerService
    {
        private MapCompilerViewModel mapCompilerVm;
        private MapCompiler mapCompiler;
        private CancellationTokenSource cancelMapCompileToken;
        private MapEditorProjectViewModel mapEditorProjectVm;

        public MapCompilerService(MapCompilerViewModel mapCompilerVm, CancellationTokenSource cancelMapCompileToken,
            MapCompiler mapCompiler)
        {
            this.mapCompiler = mapCompiler;
            this.mapCompilerVm = mapCompilerVm;
            mapEditorProjectVm = new MapEditorViewModelLocator().MapEditorProjectVm;
            mapCompiler.Result.MessageChangedEvent += Result_MessageChangedEvent;
            this.cancelMapCompileToken = cancelMapCompileToken;
        }

        public void CompileMap()
        {
            string outputDirectory = $"{mapCompilerVm.OutputDirectory}";
            mapCompilerVm.CompilationLog = string.Empty;
            mapCompiler.Result.Reset();

#if DEBUG
            mapCompiler.CompileMap(outputDirectory, mapEditorProjectVm.Project);
            mapCompilerVm.CompilationLog = mapCompiler.Result.Message;
#else
            try
            {
                mapCompiler.CompileMap(outputDirectory);
            }
            catch (Exception ex)
            {
                string logFileName = "logs\\error_log.txt";
                string completeMessage = $"{DateTime.Now.ToLongDateString()}\n{ex.Message}\n\n{mapCompiler.Result.Message}\n(For developers)\nStack Trace:\n{ex.StackTrace}";
                File.WriteAllText(logFileName, completeMessage);
                MessageBox.Show($"An error occued during compilation. Look in {logFileName} for more details.");
            }
            mapCompilerVm.CompilationLog = mapCompiler.Result.Message;
#endif
        }

        private void Result_MessageChangedEvent(object sender, EventArgs e)
        {
            mapCompilerVm.CompilationLog = mapCompiler.Result.Message;
        }

        public void BrowseTm2PathDialog()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                mapCompilerVm.OutputDirectory = dialog.SelectedPath;
                mapCompilerVm.SaveSettings();
            }
        }
    }
}
