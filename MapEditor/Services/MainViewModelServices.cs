﻿using ColladaLibrary.Utility;
using MapEditor.Project;
using MapEditor.ViewModel;
using MapEditor.ViewModel.Game;
using MapEditor.ViewModel.Project;
using MapEditor.Views;
using MapEditor.Views.Project;
using MapEditor.Views.Settings;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Services;
using MonoEngine.Tests;
using MonoEngine.Views.InputSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using TMLibrary.Lib.Map.Project;
using WpfCore.Views.Settings.InputSettingsControls;

namespace MapEditor.Services
{
    public class MainViewModelServices
    {
        private MainViewModel mainVm;
        private WindowManagerService windowService;
        private MapEditorViewModelLocator vml;

        public MainViewModel MainVm { get => mainVm; }

        public MainViewModelServices(MainViewModel mainVm)
        {
            this.mainVm = mainVm;
            windowService = new WindowManagerService();
            vml = new MapEditorViewModelLocator();
        }

        public void SaveFontSettings(MainViewModel mainVm)
        {
            var fontSettingsVm = new MapEditorViewModelLocator().FontSettingsVm;
            fontSettingsVm.SaveSettings();
        }

        public void SaveUserInterfaceSettings(MainViewModel mainVm)
        {
            var userInterfaceSettingsVm = new MapEditorViewModelLocator().UserInterfaceSettingsVm;
            userInterfaceSettingsVm.SaveSettings();
        }

        public void OpenSettingsWindow(int settingsPanelId)
        {
            var win = new SettingsMapEditorWindow();
            mainVm.CurrentSettingsPanelId = settingsPanelId;
            win.DataContext = mainVm.WindowSettingsCollectionViewModel.SettingsWindowSettingsVm;
            OpenWindow(win);
        }

        public void OpenSkyBoxSettingsWindow()
        {
            var win = new SkyBoxSettingsWindow();
            win.DataContext = vml.MapEditorProjectVm.SkyBoxSettingsVm;
            OpenWindow(win);
        }

        public void OpenLightSettingsWindow()
        {
            var win = new LightSettingsWindow();
            win.DataContext = mainVm.WindowSettingsCollectionViewModel.LightSettingsWindowSettingsVm;
            OpenWindow(win);
        }

        public void OpenKeyBindConfirmationWindow(MonoEngine.ViewModel.KeyBindingViewModel keyBindingVm)
        {
            var win = new KeyBindConfirmationWindow();
            win.DataContext = keyBindingVm;
            OpenWindow(win);
        }

        private void OpenWindow(Window window)
        {
            windowService.OpenWindow(window, mainVm.AllWindows);
        }

        public void OpenStaticObjectManager()
        {
            var win = new StaticObjectManagerWindow();
            win.DataContext = mainVm.WindowSettingsCollectionViewModel.StaticObjectManagerSettingsVm;
            windowService.OpenWindow(win, mainVm.AllWindows);
        }

        public void ToggleShowProjectHierarchy()
        {
            mainVm.ShowingProjectHierarchy = !mainVm.ShowingProjectHierarchy;
        }

        public void ToggleShowGrid()
        {
            mainVm.ShowingGrid = !mainVm.ShowingGrid;
            mainVm.EngineCore.EngineSettings.ShowGrid = mainVm.ShowingGrid;
        }

        public void StartPickableObjectTest()
        {
            var pickObjectTest = new PickableObjectTest();
            pickObjectTest.StartTest(vml.MainVm.EngineCore.GameScene);
        }

        public void OpenNewCompileMapWindow()
        {
            var win = new CompileMapWindow();
            var winSettingsVm = mainVm.WindowSettingsCollectionViewModel.MapCompilerWindowSettingsVm;
            win.DataContext = winSettingsVm;
            vml.MapCompilerVm.Reset();
            windowService.OpenWindow(win, mainVm.AllWindows);
        }

        /*public void OpenNewViewportWindow()
        {
            int id = mainVm.ViewportViewModels.Count;
            var monoViewportVm = new MonoViewportViewModel(id);
            var viewportWindow = new ViewportWindow();
            viewportWindow.DataContext = monoViewportVm;
            mainVm.ViewportWindows.Add(viewportWindow.Viewport);
            mainVm.ViewportViewModels.Add(monoViewportVm);
            mainVm.Game.Viewports.Add(new Microsoft.Xna.Framework.Graphics.Viewport(0, 0, 2, 2));

            viewportWindow.Show();
        }*/

        public void CloseApplication()
        {
            mainVm.CloseAllWindows();
        }
    }
}
