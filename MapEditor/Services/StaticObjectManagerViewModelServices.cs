﻿using MapEditor.Project;
using MapEditor.ViewModel;
using MapEditor.ViewModel.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using WpfCore.Views.FileDialogs.Collada;

namespace MapEditor.Services
{
    public class StaticObjectManagerViewModelServices
    {
        private MapEditorViewModelLocator vml;
        private StaticObjectManagerViewModel staticObjectManagerVm;

        public StaticObjectManagerViewModelServices(StaticObjectManagerViewModel staticObjectManagerVm)
        {
            this.staticObjectManagerVm = staticObjectManagerVm;
            vml = new MapEditorViewModelLocator();
        }

        public void BrowseCollisionMeshFileUrlDialog()
        {
            var colladaDialog = new ColladaFileDialog();

            var dialog = colladaDialog.OpenColladaFileDialog(false);

            if (dialog.ShowDialog() != null)
            {
                staticObjectManagerVm.SelectedStaticObject.CollisionModelUrl = dialog.FileName;
            }
        }

        public void BrowseMeshFileUrlDialog()
        {
            var colladaDialog = new ColladaFileDialog();

            var dialog = colladaDialog.OpenColladaFileDialog(false);

            if (dialog.ShowDialog() != null)
            {
                staticObjectManagerVm.SelectedStaticObject.ModelUrl = dialog.FileName;
            }
        }

        public void PromptUserForDeleteConfirmation()
        {
            var staticObjectVm = staticObjectManagerVm.SelectedStaticObject;
            var instances = staticObjectManagerVm.ProjectVm.ObjectInstanceVms.Where(i => i.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.StaticObject
            && i.ObjectInstance.StaticObject == staticObjectVm.StaticObject);
            int instanceAmount = instances.Count();
            string message = $"Are you sure you want to delete {staticObjectVm.Name}?";

            if (instanceAmount > 0)
            {
                message += $"\n\nThere are {instanceAmount} instances of this object in your project.";
            }

            var dialogResult = System.Windows.MessageBox.Show(message, "Confirm Deletion", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            if (dialogResult == MessageBoxResult.Yes)
            {
                var projectVm = vml.MapEditorProjectVm;
                projectVm.RemoveStaticObject(staticObjectVm);
                projectVm.SelectedStaticObjectIndex = projectVm.StaticObjectVms.Count - 1;
                staticObjectManagerVm.SelectedStaticObjectIndex = projectVm.SelectedStaticObjectIndex;
                projectVm.UpdateAtLeastOneStaticObjectExists();
            }
        }

        public void AddNewStaticObject()
        {
            var projectVm = new MapEditorViewModelLocator().MapEditorProjectVm;
            var staticObject = new StaticObjectDefinition("New Static Object");
            projectVm.AddStaticObjectDefinition(staticObject);
            staticObjectManagerVm.SelectedStaticObjectIndex = projectVm.StaticObjectVms.Count() - 1;

            projectVm.UpdateAtLeastOneStaticObjectExists();
        }
    }
}
