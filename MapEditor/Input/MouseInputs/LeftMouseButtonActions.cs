﻿using MapEditor.ViewModel;
using MapEditor.ViewModel.Project;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;
using WpfCore.ViewModel;

namespace MapEditor.Input.MouseInputs
{
    public class LeftMouseButtonActions : IMouseButtonActions
    {
        private MapEditorProjectViewModel mapEditorProjectVm;
        private MouseButton mouseButton;
        private InputSettings inputSettings;
        private MouseState mouseState;
        private List<IMonoViewportViewModelWrapper> viewportWrapperVms;

        public LeftMouseButtonActions(
            InputSettings inputSettings,
            MouseState mouseState,
            MouseButton mouseButton,
            List<IMonoViewportViewModelWrapper> viewportWrapperVms,
            MapEditorProjectViewModel mapEditorProjectVm)
        {
            this.mouseState = mouseState;
            this.viewportWrapperVms = viewportWrapperVms;
            this.inputSettings = inputSettings;
            this.mouseButton = mouseButton;
            this.mapEditorProjectVm = mapEditorProjectVm;
        }

        public void ClickAction(MonoEngineScene scene)
        {
            CheckPickedObject();
        }

        public void DefaultAction(MonoEngineScene scene)
        {
            CheckPickedObject();
        }

        private void CheckPickedObject()
        {
            if (mouseState.ActiveViewportId >= 0 && mouseState.ActiveViewportId < viewportWrapperVms.Count)
            {
                var viewportVm = viewportWrapperVms[mouseState.ActiveViewportId] as MapEditorViewportViewModel;
                //viewportVm.CheckCurrentPickableObject(mouseButton, mouseState);
            }
        }

        public void DownAction(MonoEngineScene scene)
        {
        }

        public void HoldAction(MonoEngineScene scene)
        {
        }

        public void ReleaseAction(MonoEngineScene scene)
        {
        }

        public MouseButton GetLmb()
        {
            return mouseButton;
        }

        public void DoubleClickAction(MonoEngineScene scene)
        {
        }
    }
}
