﻿using MapEditor.ViewModel;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;

namespace MapEditor.Input.MouseInputs
{
    public class RightMouseButtonActions : IMouseButtonActions
    {
        private MapEditorViewportViewModel viewportVm;
        private MapEditorProjectViewModel mapEditorProjectVm;
        private MouseButton mouseButton;
        private InputSettings inputSettings;

        public RightMouseButtonActions(
            InputSettings inputSettings,
            MouseButton mouseButton,
            MapEditorProjectViewModel mapEditorProjectVm)
        {
            this.inputSettings = inputSettings;
            this.mouseButton = mouseButton;
            this.mapEditorProjectVm = mapEditorProjectVm;
        }

        public void UpdateViewportVm(MapEditorViewportViewModel viewportVm)
        {
            this.viewportVm = viewportVm;
        }

        public void ClickAction(MonoEngineScene scene)
        {
        }

        public void DefaultAction(MonoEngineScene scene)
        {
        }

        public void DownAction(MonoEngineScene scene)
        {
        }

        public void HoldAction(MonoEngineScene scene)
        {
        }

        public void ReleaseAction(MonoEngineScene scene)
        {
        }

        public MouseButton GetLmb()
        {
            return mouseButton;
        }

        public void DoubleClickAction(MonoEngineScene scene)
        {
        }
    }
}
