﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapEditor.ViewModel;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using WpfCore.ViewModel;

namespace MapEditor.Input
{
    public class MouseController : MouseControllerBase
    {
        private List<IMonoViewportViewModelWrapper> viewportWrapperViewModels;

        public MouseController() : base()
        {
            viewportWrapperViewModels = new List<IMonoViewportViewModelWrapper>();
        }

        public void InitializeActions(IMouseButtonActions leftMouseButtonActions,
            IMouseButtonActions rightMouseButtonActions, IMouseButtonActions middleMouseButtonActions,
            List<IMonoViewportViewModelWrapper> viewportWrapperViewModels)
        {
            this.viewportWrapperViewModels = viewportWrapperViewModels;
            this.lmbController = new MouseButtonController(mouseState, leftMouseButtonActions);
            this.rmbController = new MouseButtonController(mouseState, rightMouseButtonActions);
            this.mmbController = new MouseButtonController(mouseState, middleMouseButtonActions);
        }

        public MouseController(MouseState mouseState, IMouseButtonActions leftMouseButtonActions,
            IMouseButtonActions rightMouseButtonActions, IMouseButtonActions middleMouseButtonActions,
            List<IMonoViewportViewModelWrapper> viewportWrapperViewModels) :
            base(leftMouseButtonActions, rightMouseButtonActions, middleMouseButtonActions)
        {
            this.viewportWrapperViewModels = viewportWrapperViewModels;
        }

        protected override void InnerUpdateInput(int delta,
            MonoEngineScene scene,
            List<IMonoViewportViewModelWrapper> viewportVmWrappers, Rectangle windowBoundary, Point mousePosition)
        {
            if (mouseState.ActiveViewportId >= 0)
            {
            }
        }
    }
}
