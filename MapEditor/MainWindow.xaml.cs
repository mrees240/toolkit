﻿using MapEditor.Services;
using MapEditor.ViewModel;
using MapEditor.Views;
using System.Windows;
using System.Windows.Input;
using WpfCore.ViewModel;

namespace MapEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private CompileMapWindow compileMapWindow;
        private ViewModel.MainViewModel mainVm;
        private MapEditorViewModelLocator vml;

        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public MainWindow()
        {
            InitializeComponent();

            timer.Tick += Timer_Tick;
            timer.Interval = 1;

            timer.Start();

            /*var mainVm = new MapEditorViewModelLocator().MainVm;
            var projectVm = new MapEditorViewModelLocator().MapEditorProjectVm;
            /*mainVm.ViewportWindows.Add(VP0);
            mainVm.ViewportWindows.Add(VP1);
            mainVm.ViewportWindows.Add(VP2);
            mainVm.ViewportWindows.Add(VP3);*/

            /*var monoViewportVm = new MonoViewportViewModel();
            VP0.DataContext = monoViewportVm;

            mainVm.ViewportViewModels.Add(new MapEditorViewportViewModel(projectVm, monoViewportVm));*/
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            var mainVm = GetMainVm();

            if (mainVm != null)
            {
                mainVm.EngineCore.InputFocused = ViewportContainer.IsMouseOver;
            }
        }

        private ViewModel.MainViewModel GetMainVm()
        {
            if (mainVm != null)
            {
                return mainVm;
            }
            vml = new MapEditorViewModelLocator();
            mainVm = vml.MainVm;
            //mainVm.InitializeMainVm();

            return mainVm;
        }

        private void CloseWindow_Exec(object sender, ExecutedRoutedEventArgs e)
        {
            //compileMapWindow.Close();
            mainVm = new MapEditorViewModelLocator().MainVm;
            new MainViewModelServices(mainVm).CloseApplication();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var mainVm = GetMainVm();

            mainVm.Width = (int)ActualWidth;
            mainVm.Height = (int)ActualHeight;
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
        }

        private void Window_LostFocus(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Initialized(object sender, System.EventArgs e)
        {
            GetMainVm().InitializeKeyboardInput();
        }
    }
}
