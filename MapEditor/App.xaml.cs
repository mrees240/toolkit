﻿using System;
using System.Windows;

namespace MapEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            /*var ex = (Exception)(e.ExceptionObject);
            var mainVm = new ViewModel.MapEditorViewModelLocator().MainVm;

            mainVm.StopGame();

            mainVm.RenderErrorMessage = ex.Message;
            mainVm.RenderErrorOccured = true;*/
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            /*var mainVm = new ViewModel.MapEditorViewModelLocator().MainVm;

            e.Handled = true;
            mainVm.StopGame();
            mainVm.RenderErrorMessage = e.Exception.Message;
            mainVm.RenderErrorOccured = true;*/
        }

    }
}
