﻿using ColladaLibrary.Render;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;

namespace MapEditor.Render
{
    public class PathLinkConnectionObject : GenericSceneObject
    {
        private PathNodeConnection connection;
        private bool initialized;
        private bool enabled;

        public bool Enabled { get => enabled; }

        public PathLinkConnectionObject(PathNodeConnection connection, Mesh mesh)
        {
            this.connection = connection;
            SetMeshes(mesh);
            objectState.Transformation.IsIndependent = true;

            connection.P0.Transformation.TransformationChanged += Transformation_TransformationChanged;
            connection.P1.Transformation.TransformationChanged += Transformation_TransformationChanged;
            enabled = true;
        }

        private new void Transformation_TransformationChanged(object sender, EventArgs e)
        {
            base.Transformation_TransformationChanged(sender, e);
            initialized = false;
        }

        public void Disable()
        {
            enabled = false;
        }

        private void UpdateTransformation()
        {
            var p0 = connection.P0.Transformation.PositionLocal;
            var p1 = connection.P1.Transformation.PositionLocal;

            var length = System.Numerics.Vector3.Distance(p0, p1);

            objectState.Transformation.SetLocalScale(new System.Numerics.Vector3(1.0f, 1.0f, length));
            MonoGameCore.Utility.RenderUtility.ObjectLookAtPoint(objectState.Transformation, p1, p0);
            objectState.Transformation.SetLocalPosition(p0);
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);

            drawState.IsSolidColor = true;
            drawState.Color = System.Drawing.Color.Aqua;

            if (!initialized)
            {
                UpdateTransformation();
                initialized = true;
            }
            drawState.IsVisible = enabled;
        }
    }
}
