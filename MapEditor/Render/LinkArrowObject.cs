﻿using ColladaLibrary.Render;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;

namespace MapEditor.Render
{
    public class LinkArrowObject : GenericSceneObject
    {
        private PathNodeConnection connection;
        private Transformation nodeTransformation0;
        private Transformation nodeTransformation1;

        private GenericSceneObject arrowObjectPt0ToPt1;
        private GenericSceneObject arrowObjectPt1ToPt0;
        private GenericSceneObject arrowObjectTwoWay;
        private static int linkArrowCount;
        private List<PathNodeConnection> connections;

        public LinkArrowObject(PathNodeConnection connection, Transformation nodeTransformation0, Transformation nodeTransformation1,
            Mesh arrowMesh, Mesh arrowMeshMirrored, Mesh arrowMeshTwoWay, List<PathNodeConnection> connections)
        {
            this.connections = connections;
            objectState.Transformation.IsIndependent = true;
            arrowObjectPt0ToPt1 = new GenericSceneObject();
            arrowObjectPt1ToPt0 = new GenericSceneObject();
            arrowObjectTwoWay = new GenericSceneObject();
            arrowObjectPt0ToPt1.SetMeshes(new List<Mesh>() { arrowMesh });
            arrowObjectPt1ToPt0.SetMeshes(new List<Mesh>() { arrowMeshMirrored });
            arrowObjectTwoWay.SetMeshes(new List<Mesh>() { arrowMeshTwoWay });
            drawableChildren.Add(arrowObjectPt0ToPt1);
            drawableChildren.Add(arrowObjectPt1ToPt0);
            drawableChildren.Add(arrowObjectTwoWay);
            this.connection = connection;
            this.nodeTransformation0 = nodeTransformation0;
            this.nodeTransformation1 = nodeTransformation1;

            GetObjectState().Name = $"Link Arrow";
            arrowObjectPt0ToPt1.GetObjectState().Name = $"Arrow {linkArrowCount}";
            arrowObjectPt1ToPt0.GetObjectState().Name = $"Arrow {linkArrowCount}";
            arrowObjectTwoWay.GetObjectState().Name = $"Arrow {linkArrowCount}";

            linkArrowCount++;

            //arrowObjectPt0ToPt1.GetObjectState().AddPickableMesh(arrowMesh);
            //arrowObjectPt1ToPt0.GetObjectState().AddPickableMesh(arrowMeshMirrored);
            //arrowObjectTwoWay.GetObjectState().AddPickableMesh(arrowMeshTwoWay);
            arrowObjectPt0ToPt1.GetObjectState().ToggleActivated += ObjectState_ToggleActivated;
            arrowObjectPt1ToPt0.GetObjectState().ToggleActivated += ObjectState_ToggleActivated;
            arrowObjectTwoWay.GetObjectState().ToggleActivated += ObjectState_ToggleActivated;
            arrowObjectPt0ToPt1.GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectPt1ToPt0.GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectTwoWay.GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectPt0ToPt1.GetObjectState().IsPickable = true;
            arrowObjectPt1ToPt0.GetObjectState().IsPickable = true;
            arrowObjectTwoWay.GetObjectState().IsPickable = true;

            GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectPt0ToPt1.GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectPt1ToPt0.GetObjectState().OnlyVisibleInEditor = true;
            arrowObjectTwoWay.GetObjectState().OnlyVisibleInEditor = true;

            UpdateLinkArrow();
            UpdateColor();
        }

        private void ObjectState_ToggleActivated(object sender, EventArgs e)
        {
            if (connection.Direction == PathConnectionDirection.P0ToP1)
            {
                connection.Direction = PathConnectionDirection.P1ToP0;
            }
            else if (connection.Direction == PathConnectionDirection.P1ToP0)
            {
                connection.Direction = PathConnectionDirection.TwoWay;
            }
            else if (connection.Direction == PathConnectionDirection.TwoWay)
            {
                connection.Direction = PathConnectionDirection.P0ToP1;
            }
        }

        private void UpdateLinkArrow()
        {
            var v0 = nodeTransformation0.PositionLocal;
            var v1 = nodeTransformation1.PositionLocal;
            var middle = (v0 + v1) / 2.0f;

            SetVisibleArrowType();
            SetArrowPosition(middle);
        }
        
        public override void PreDraw(MonoViewportViewModel monoViewportViewModel)
        {
            UpdateLinkArrow();
        }

        private void SetVisibleArrowType()
        {
            switch (connection.Direction)
            {
                case PathConnectionDirection.P0ToP1:
                    arrowObjectPt0ToPt1.GetDrawState().IsVisible = true;
                    arrowObjectPt1ToPt0.GetDrawState().IsVisible = false;
                    arrowObjectTwoWay.GetDrawState().IsVisible = false;
                    break;
                case PathConnectionDirection.P1ToP0:
                    arrowObjectPt0ToPt1.GetDrawState().IsVisible = false;
                    arrowObjectPt1ToPt0.GetDrawState().IsVisible = true;
                    arrowObjectTwoWay.GetDrawState().IsVisible = false;
                    break;
                case PathConnectionDirection.TwoWay:
                    arrowObjectPt0ToPt1.GetDrawState().IsVisible = false;
                    arrowObjectPt1ToPt0.GetDrawState().IsVisible = false;
                    arrowObjectTwoWay.GetDrawState().IsVisible = true;
                    break;
            }
        }

        private void UpdateColor()
        {
            foreach (var child in drawableChildren)
            {
                var objState = child.GetObjectState();
                var drawState = child.GetDrawState();
                objState.OnlyVisibleInEditor = true;
                //drawState.TwoSides = true;
                drawState.IsSolidColor = true;
                drawState.Color = System.Drawing.Color.FromArgb(66, 109, 111);
                drawState.Shadeless = true;
            }
        }

        private void SetArrowPosition(Vector3 position)
        {
            foreach (var child in drawableChildren)
            {
                var drawState = child.GetDrawState();
                var transformation = child.GetTransformation();
                RenderUtility.ObjectLookAtPoint(transformation, position, connection.P0.Transformation.WorldMatrix.Translation);
                transformation.SetLocalScale(new Vector3(1.0f, 1.0f, 300));
                transformation.SetLocalPosition(position);
                transformation.SetWorldMatrix(transformation.GetLocalMatrix());
            }
        }
    }
}
