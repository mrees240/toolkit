﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.Render
{
    public enum ViewportMode
    {
        One_Perspective, Four_One_Perspective_Three_Ortho, Four_Perspective
    }
}
