﻿using ColladaLibrary.Render;
using MapEditor.ViewModel.Project;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.Render
{
    public class SkyObject : GenericSceneObject
    {
        private Mesh skyMesh;
        private Mesh defaultMesh;
        private bool usingDefaultMesh;

        private Mesh bottomMesh;
        private Mesh topMesh;
        private Mesh leftMesh;
        private Mesh rightMesh;
        private Mesh frontMesh;
        private Mesh backMesh;

        private GenericSceneObject skyObjectChild;
        private GenericSceneObject defaultSkyObject;


        private SkyBoxSettingsViewModel skyBoxVm;
        private GraphicsDevice graphicsDevice;
        private bool texturesNeedUpdated;

        private Mesh sideMesh;

        public SkyObject(SkyBoxSettingsViewModel skyBoxVm)
        {
            this.skyBoxVm = skyBoxVm;
            usingDefaultMesh = true;
            //this.defaultMesh = new Mesh(defaultSkyMesh);
            //this.defaultMesh.ScaleAtCenter(1000000.0f);
            //var textures = this.defaultMesh.GetTextures();
            //defaultMesh.ReplaceDiffuseTexture(textures[1].Diffuse.Bitmap, skyBoxVm.Front.Texture.Bitmap);

            skyObjectChild = new GenericSceneObject();
            defaultSkyObject = new GenericSceneObject();
            defaultSkyObject.GetDrawState().NeverMoves = true;
            defaultSkyObject.GetDrawState().Shadeless = true;
            defaultSkyObject.GetDrawState().DrawFirst = true;
            defaultSkyObject.GetDrawState().ChangesFarDistance = true;
            defaultSkyObject.GetDrawState().FarClipDrawDistance = 9999999999999.0f;
            defaultSkyObject.GetDrawState().TwoSides = true;
            defaultSkyObject.GetObjectState().OnlyVisibleInEditor = true;

            //skyBoxVm.Front.TextureFileChanged += Front_TextureFileChanged;

            drawableChildren.Add(skyObjectChild);
            drawableChildren.Add(defaultSkyObject);

            CreateMeshes();
            defaultSkyObject.SetMeshes(new List<Mesh>()
            {
                frontMesh,
                backMesh,
                leftMesh,
                rightMesh,
                topMesh,
                bottomMesh
            });
        }

        private void CreateMeshes()
        {
            float scale = 999999999.0f;
            frontMesh = CreateSideMesh(skyBoxVm.Front.Texture.Bitmap);
            backMesh = CreateSideMesh(skyBoxVm.Back.Texture.Bitmap);
            leftMesh = CreateSideMesh(skyBoxVm.Left.Texture.Bitmap);
            rightMesh = CreateSideMesh(skyBoxVm.Right.Texture.Bitmap);
            topMesh = CreateSideMesh(skyBoxVm.Top.Texture.Bitmap);
            bottomMesh = CreateSideMesh(skyBoxVm.Bottom.Texture.Bitmap);

            frontMesh.ScaleAtCenter(scale);
            backMesh.ScaleAtCenter(scale);
            leftMesh.ScaleAtCenter(scale);
            rightMesh.ScaleAtCenter(scale);
            topMesh.ScaleAtCenter(scale);
            bottomMesh.ScaleAtCenter(scale);
            backMesh.Transform(Matrix4x4.CreateRotationZ((float)Math.PI));
            leftMesh.Transform(Matrix4x4.CreateRotationZ((float)(Math.PI/2.0)));
            rightMesh.Transform(Matrix4x4.CreateRotationZ((float)(Math.PI / -2.0)));
            topMesh.Transform(Matrix4x4.CreateRotationX((float)(Math.PI / 2.0)));
            bottomMesh.Transform(Matrix4x4.CreateRotationX((float)(Math.PI / -2.0)));

            skyBoxVm.Front.Mesh = frontMesh;
            skyBoxVm.Back.Mesh = backMesh;
            skyBoxVm.Left.Mesh = leftMesh;
            skyBoxVm.Right.Mesh = rightMesh;
            skyBoxVm.Top.Mesh = topMesh;
            skyBoxVm.Bottom.Mesh = bottomMesh;
        }

        private Mesh CreateSideMesh(Bitmap texture)
        {
            var mesh = new Mesh();

            var v0 = new Vector3(-0.5f, 0.5f, -0.5f);
            var v1 = new Vector3(0.5f, 0.5f, -0.5f);
            var v2 = new Vector3(-0.5f, 0.5f, 0.5f);
            var v3 = new Vector3(0.5f, 0.5f, 0.5f);

            var i0 = new PolygonIndex(0, System.Drawing.Color.White, 0.0f, 1.0f);
            var i1 = new PolygonIndex(1, System.Drawing.Color.White, 1.0f, 1.0f);
            var i2 = new PolygonIndex(2, System.Drawing.Color.White, 0.0f, 0.0f);
            var i3 = new PolygonIndex(3, System.Drawing.Color.White, 1.0f, 0.0f);

            mesh.Vertices.Add(new Vertex(v0, new Vector3(0.0f, -1.0f, 0.0f), i0.TexCoord.AsVector(), System.Drawing.Color.White));
            mesh.Vertices.Add(new Vertex(v1, new Vector3(0.0f, -1.0f, 0.0f), i1.TexCoord.AsVector(), System.Drawing.Color.White));
            mesh.Vertices.Add(new Vertex(v2, new Vector3(0.0f, -1.0f, 0.0f), i2.TexCoord.AsVector(), System.Drawing.Color.White));
            mesh.Vertices.Add(new Vertex(v3, new Vector3(0.0f, -1.0f, 0.0f), i3.TexCoord.AsVector(), System.Drawing.Color.White));

            var p0 = new Polygon(i0, i1, i2);
            var p1 = new Polygon(i2, i1, i3);
            p0.Textures.Diffuse.Bitmap = texture;
            p1.Textures.Diffuse.Bitmap = texture;

            mesh.AddPolygon(p0);
            mesh.AddPolygon(p1);

            return mesh;
        }

        private void Front_TextureFileChanged(object sender, EventArgs e)
        {
            /*var textures = this.defaultMesh.GetTextures();
            textures[1].Diffuse.Bitmap = skyBoxVm.Front.Texture.Bitmap;
            //mesh.ReplaceDiffuseTexture(textures[0].Diffuse.Bitmap, skyBoxVm.Front.Texture.Bitmap);
            //defaultMesh.ReplaceDiffuseTexture(textures[1].Diffuse.Bitmap, new System.Drawing.Bitmap(2, 2));
            //mesh.ReplaceDiffuseTexture(textures[2].Diffuse.Bitmap, skyBoxVm.Front.Texture.Bitmap);
            //defaultSkyObject.SetMeshes(defaultMesh);
            texturesNeedUpdated = true;*/
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            if (graphicsDevice == null)
            {
                graphicsDevice = scene.GraphicsDevice;
            }
            base.Update(deltaMs, matrixStack, mouseState, scene);
            if (usingDefaultMesh)
            {
                //defaultSkyObject.GetDrawState().IsVisible = true;
                //skyObjectChild.GetDrawState().IsVisible = false;
            }
            //

            if (scene.ActiveMonoViewportVm != null)
            {
                var cam = scene.ActiveMonoViewportVm.Camera;
                //GetObjectState().Transformation.SetLocalMatrix(Matrix4x4.Identity);
                //GetObjectState().Transformation.SetLocalPosition(cam.Coordinates);
            }
            if (texturesNeedUpdated)
            {
                drawState.MeshBufferNeedsUpdated = true;
                texturesNeedUpdated = false;
            }
        }
    }
}
