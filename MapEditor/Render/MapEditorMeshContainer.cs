﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TMLibrary.Lib.Collada;
using TMLibrary.Lib.Util;

namespace MapEditor.Render
{
    public class MapEditorMeshContainer
    {
        private const string pathNodeNameUrl = "Resources/Paths/PathNode.dae";
        private Mesh pathNodeMesh;
        private Mesh defaultSkyMesh;
        private bool meshesLoaded;

        public MapEditorMeshContainer()
        {
            ValidateMeshes();
            LoadMeshFiles();
        }

        private void ValidateMeshes()
        {
            string[] meshList = new string[]
            {
                pathNodeNameUrl
            };
            foreach (var meshUrl in meshList)
            {
                if (!File.Exists(meshUrl))
                {
                    MessageBox.Show($"{meshUrl} is missing.");
                    Application.Current.Shutdown();
                }
            }
        }

        public void LoadMeshFiles()
        {
            var colladaImporterPathNode = new ColladaLibrary.Collada.ColladaImporter(false, true, pathNodeNameUrl);
            colladaImporterPathNode.Load();
            pathNodeMesh = colladaImporterPathNode.CreateSingleMesh();
            pathNodeMesh.Transform(Matrix4x4.CreateScale(2.0f));
            meshesLoaded = true;
        }

        public Mesh PathNodeMesh { get => pathNodeMesh; }
        public bool MeshesLoaded { get => meshesLoaded; }
        public Mesh DefaultSkyMesh { get => defaultSkyMesh; }
    }
}
