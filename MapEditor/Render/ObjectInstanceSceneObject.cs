﻿using MapEditor.Settings;
using MapEditor.ViewModel.Project;
using MapEditor.ViewModel.Settings;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor.Render
{
    public class ObjectInstanceSceneObject : GenericSceneObject
    {
        private MapEditorMeshContainer meshContainer;
        private ObjectInstanceViewModel objectInstanceVm;
        private MiscSettings miscSettings;

        public ObjectInstanceSceneObject(ObjectInstanceViewModel objectInstanceVm, MapEditorMeshContainer meshContainer, MiscSettings miscSettings)
        {
            this.miscSettings = miscSettings;
            this.meshContainer = meshContainer;
            this.objectInstanceVm = objectInstanceVm;
            GetObjectState().IsPickable = true;
            GetObjectState().IsGizmoTranslatable = true;

            if (objectInstanceVm.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.PathNode)
            {
                GetObjectState().IsSphere = true;
                GetObjectState().OnlyVisibleInEditor = true;
                GetObjectState().TranslationOnly = true;
                SetMeshes(meshContainer.PathNodeMesh);
            }

            objectState.PickActivated += ObjectState_PickActivated;
            LinkChanged += ObjectInstanceSceneObject_LinkChanged;
        }

        private void ObjectInstanceSceneObject_LinkChanged(object sender, EventArgs e)
        {
        }

        private void ObjectState_PickActivated(object sender, EventArgs e)
        {
            objectInstanceVm.IsSelected = true;
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, matrixStack, mouseState, scene);
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);

            if (objectInstanceVm.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.PathNode)
            {
                SetPathNodeDrawState();
            }

            if (GetObjectState().TransformationGizmo != null)
            {
                GetObjectState().TransformationGizmo.GetDrawState().IsVisible = GetObjectState().IsSelected;
            }
        }

        private void SetPathNodeDrawState()
        {
            drawState.IsVisible = miscSettings.ShowPathNodes;
            drawState.IsSolidColor = true;

            if (GetObjectState().IsLinkPicked)
            {
                drawState.Shadeless = true;
                drawState.Color = System.Drawing.Color.Gray;
            }
            else
            {
                drawState.Shadeless = false;
                drawState.Color = System.Drawing.Color.CadetBlue;
            }
        }

        public override bool LinkWith(IDrawableSceneObject otherObject)
        {
            if (objectInstanceVm.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.PathNode)
            {
                var otherObjSceneObject = otherObject as ObjectInstanceSceneObject;

                if (otherObjSceneObject != null)
                {
                    if (otherObjSceneObject.ObjectInstanceVm.ObjectInstance.ObjectType == TMLibrary.Lib.Map.Project.ObjectType.PathNode)
                    {
                        return base.LinkWith(otherObject);
                    }
                }
            }
            return false;
        }

        public ObjectInstanceViewModel ObjectInstanceVm { get => objectInstanceVm; }
    }
}
