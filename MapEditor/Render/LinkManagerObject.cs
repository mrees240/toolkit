﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using MapEditor.Project;
using MapEditor.Settings;
using MapEditor.ViewModel;
using MapEditor.ViewModel.Project;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Render.VertexDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TMLibrary.Lib.Map.Project;

namespace MapEditor.Render
{
    public class LinkManagerObject : GenericSceneObject
    {
        private Dictionary<ObjectInstanceViewModel, ObjectInstanceSceneObject> pathNodePoints;
        private Dictionary<PathNodeConnection, PathLinkConnectionObject> connectionObjects;

        private MonoEngineScene scene;
        private List<PathNodeConnection> connections;
        private Mesh cylinderMesh;
        private Mesh arrowMesh;
        private Mesh arrowMeshMirrored;
        private Mesh arrowMeshTwoWay;
        private MapEditorMeshContainer meshContainer;
        private Mesh pathNodeMesh;
        private DrawState instanceDrawState;

        private MiscSettings miscSettings;
        private Transformation currentLinkTransformation;

        private MapEditorProjectViewModel project;

        public List<PathNodeConnection> Connections { get => connections; }

        private Dictionary<PathNodeConnection, LinkArrowObject> linkArrows;

        private Mesh pathPointMesh;

        public LinkManagerObject(List<PathNodeConnection> connections, MapEditorProjectViewModel project, MapEditorMeshContainer meshContainer,
            MiscSettings miscSettings)
        {
            pathNodePoints = new Dictionary<ObjectInstanceViewModel, ObjectInstanceSceneObject>();
            connectionObjects = new Dictionary<PathNodeConnection, PathLinkConnectionObject>();
            pathPointMesh = meshContainer.PathNodeMesh;
            instanceDrawState = new DrawState();
            this.linkArrows = new Dictionary<PathNodeConnection, LinkArrowObject>();
            this.miscSettings = miscSettings;
            this.project = project;
            this.meshContainer = meshContainer;
            currentLinkTransformation = new Transformation();
            this.cylinderMesh = ShapeMeshUtility.CreateCylinder(new Vector3(), new Vector3(0.0f, 0.0f, -1.0f), 50.0f, 50.0f, 5, null);
            this.arrowMesh = ShapeMeshUtility.CreateCylinder(new Vector3(), new Vector3(0.0f, 0.0f, -1.0f), 150.0f, 1.0f, 5, null);
            this.arrowMeshMirrored = ShapeMeshUtility.CreateCylinder(new Vector3(), new Vector3(0.0f, 0.0f, 1.0f), 150.0f, 1.0f, 5, null);
            this.arrowMeshTwoWay = ColladaLibrary.Utility.RenderUtility.MergeMeshes(new List<Mesh>() { arrowMesh, arrowMeshMirrored });
            this.arrowMesh.Name = "Arrow Mesh";
            this.arrowMeshMirrored.Name = "Arrow Mesh Mirrored";
            this.arrowMeshTwoWay.Name = "Arrow Mesh Two Way";
            this.connections = connections;
            this.pathNodeMesh = meshContainer.PathNodeMesh;
            this.objectState.Name = "Link Manager Object";
            objectState.OnlyVisibleInEditor = true;
            internalLights = new List<Light>();
            internalLights.Add(new Light(LightType.Directional, System.Drawing.Color.White, 1.0f, 0.0f, 45.0f, 45.0f));
            internalLights.Add(new Light(LightType.Ambient, System.Drawing.Color.White, 0.5f));

            drawState.Shadeless = true;
            drawState.UseInternalLightList = true;
            drawState.IsSolidColor = true;
            //drawState.Color = System.Drawing.Color.Red;
            //drawState.TwoSides = true;
        }

        public void AddConnection(PathNodeConnection connection)
        {
            var linkArrowObj = new LinkArrowObject(connection, connection.P0.Transformation, connection.P1.Transformation, arrowMesh, arrowMeshMirrored,
                arrowMeshTwoWay, connections);
            var connectionObj = new PathLinkConnectionObject(connection, cylinderMesh);

            linkArrows.Add(connection, linkArrowObj);
            connections.Add(connection);
            connectionObjects.Add(connection, connectionObj);

            drawableChildren.Add(linkArrowObj);
            drawableChildren.Add(connectionObj);

            CreateSceneObjectForPoint(project.GetObjectInstanceViewModel(connection.P0));
            CreateSceneObjectForPoint(project.GetObjectInstanceViewModel(connection.P1));
        }

        public void CreateSceneObjectForPoint(ObjectInstanceViewModel instance)
        {
            if (!pathNodePoints.ContainsKey(instance))
            {
                var sceneObject = new ObjectInstanceSceneObject(instance, meshContainer, miscSettings);
                pathNodePoints.Add(instance, sceneObject);
                sceneObject.GetObjectState().Transformation = instance.TransformationVm.Transformation;
                //drawableChildren.Add(sceneObject);
            }
        }

        public void RemoveConnection(PathNodeConnection connection)
        {
            //drawableChildren.Remove(connectionObjects[connection]);
            var connectionObj = connectionObjects[connection];
            var connectionLinkObj = linkArrows[connection];
            //connectionObj.Disable();
            //connectionObjects.Remove(connection);
            //connectionObj.GetDrawState().IsVisible = false;

            RemoveChild(connectionObj);
            RemoveChild(connectionLinkObj);
            connections.Remove(connection);
            //connectionObj.Disable();

            //connections.Remove(connection);
            //drawableChildren.Remove(linkArrows[connection]);

            // Temp fix
            //RefreshSceneObjects();
            foreach (var currentConnection in connections)
            {
                //AddConnection(currentConnection);
            }
        }

        private void RefreshSceneObjects()
        {
            pathNodePoints.Clear();
            foreach (var child in drawableChildren)
            {
                RemoveChild(child);
            }
            linkArrows.Clear();
            connectionObjects.Clear();
        }

        public void Reset()
        {
            pathNodePoints.Clear();
            linkArrows.Clear();
            connections.Clear();
            drawableChildren.Clear();
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, matrixStack, mouseState, scene);
            drawState.IsVisible = miscSettings.ShowPathNodes;
            if (this.scene == null)
            {
                this.scene = scene;
            }
        }
    }
}
