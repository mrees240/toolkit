﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapEditor.Views.FileDialogs
{

    public class MapEditorProjectDialog
    {
        private const string defaultExtension = ".xml";
        private const string filter = "XML Files (.xml)|*.xml";

        public OpenFileDialog OpenProjectFileDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.DefaultExt = defaultExtension;
            dialog.Filter = filter;
            return dialog;
        }

        public SaveFileDialog SaveProjectFileDialog()
        {
            var dialog = new SaveFileDialog();
            dialog.DefaultExt = defaultExtension;
            dialog.Filter = filter;
            return dialog;
        }
    }
}
