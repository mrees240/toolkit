﻿using System.Windows.Controls;

namespace MapEditor.Views.Settings
{
    /// <summary>
    /// Interaction logic for SettingsWrapper.xaml
    /// </summary>
    public partial class SettingsWrapperMapEditor : UserControl
    {
        public SettingsWrapperMapEditor()
        {
            InitializeComponent();
        }
    }
}
