﻿using MapEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MapEditor.Views
{
    /// <summary>
    /// Interaction logic for CompileMapWindow.xaml
    /// </summary>
    public partial class CompileMapWindow : Window
    {
        public CompileMapWindow()
        {
            InitializeComponent();
        }

        private void CommandBindingClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = (MapCompilerViewModel)CompilationView.DataContext;
            vm.CancelCompilation();
        }
    }
}
