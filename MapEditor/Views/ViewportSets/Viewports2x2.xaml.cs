﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MapEditor.Views.ViewportSets
{
    /// <summary>
    /// Interaction logic for Viewports2x2.xaml
    /// </summary>
    public partial class Viewports2x2 : UserControl
    {
        public Viewports2x2()
        {
            InitializeComponent();
        }
    }
}
