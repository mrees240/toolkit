﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace MapEditor.Views.Project
{
    /// <summary>
    /// Interaction logic for SelectedObjectInstanceControlsView.xaml
    /// </summary>
    public partial class SelectedObjectInstanceControlsView : UserControl
    {
        public SelectedObjectInstanceControlsView()
        {
            InitializeComponent();
            ControlsViewGrid.Focusable = true;
        }

        private void SingleUpDownKeyUpHandler(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                case Key.Escape:
                    ControlsViewGrid.Focus();
                    break;
            }
        }
    }
}
