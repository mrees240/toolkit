using EngineExamples.Input;
using EngineExamples.Render;
using GalaSoft.MvvmLight;
using MonoEngine.Code;
using MonoEngine.Code.Render;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using WpfCore.ViewModel;

namespace EngineExamples.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private MonoEngineCore engine;
        private List<IMonoViewportViewModelWrapper> viewportVms;
        private DataDisplayObject exampleObject;

        public MainViewModel()
        {
            viewportVms = new List<IMonoViewportViewModelWrapper>();
            var viewportVm = new ExamplesViewportViewModel();
            viewportVms.Add(viewportVm);
            var mouseController = new ExamplesMouseInput();
            engine = new MonoEngineCore(viewportVms, mouseController);
            mouseController.Initialize(engine);
            //engine.EngineSettings.ShowGrid = true;
            engine.EngineSettings.GridLength = 10000.0f;
            engine.EngineSettings.GridRows = 40;
            engine.StartEngine();
            LoadMesh();
        }

        private void LoadMesh()
        {
            string meshFileUrl = "Resources/Sky.dae";
            if (!File.Exists(meshFileUrl))
            {
                MessageBox.Show($"{meshFileUrl} is missing.");
            }
            else
            {
                var meshLoader = new ColladaLibrary.Collada.ColladaImporter(false, true, meshFileUrl);
                meshLoader.Load();
                var mesh = meshLoader.CreateSingleMesh();
                exampleObject = new DataDisplayObject();
                exampleObject.GetDrawState().Shadeless = true;
                exampleObject.SetMeshes(new List<ColladaLibrary.Render.Mesh>() { mesh });
                engine.GameScene.AddObject(exampleObject);
            }
        }

        public MonoViewportViewModel GetMonoViewportVm
        {
            get
            {
                return viewportVms[0].GetBaseViewportVm();
            }
        }
    }
}