﻿using MonoEngine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.ViewModel;

namespace EngineExamples.ViewModel
{
    public class ExamplesViewportViewModel : IMonoViewportViewModelWrapper
    {
        private MonoViewportViewModel baseViewModel;

        public ExamplesViewportViewModel()
        {
            baseViewModel = new MonoViewportViewModel();

            baseViewModel.BackgroundColor = System.Windows.Media.Color.FromRgb(153, 217, 234);
        }

        public MonoViewportViewModel GetBaseViewportVm()
        {
            return baseViewModel;
        }
    }
}
