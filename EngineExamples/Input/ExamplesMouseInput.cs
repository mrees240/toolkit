﻿using MonoEngine.Code;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WpfCore.Settings;
using WpfCore.ViewModel;

namespace EngineExamples.Input
{
    public class ExamplesMouseInput : MouseControllerBase
    {
        private MonoEngineCore engine;
        private MouseState mouseState;
        private InputSettings inputSettings;

        public ExamplesMouseInput()
        {
            inputSettings = new InputSettings();
        }

        public void Initialize(MonoEngineCore engine)
        {
            var rmb = new MouseButton(MouseButtons.Right);
            this.engine = engine;
            mouseState = new MouseState();
            lmbController = new MouseButtonController(mouseState, new DefaultEditorLmbAction(engine, mouseState, false));
            rmbController = new MouseButtonController(mouseState, new DefaultOrbitRmbAction(inputSettings, mouseState, rmb, engine.MonoViewportVmWrappers));
            //rmbController = new MouseButtonController(mouseState, new DefaultEditorRmbAction(engine, mouseState, false));
        }

        protected override void InnerUpdateInput(int delta, MonoEngineScene scene, List<IMonoViewportViewModelWrapper> viewportVmWrappers, System.Drawing.Rectangle windowBoundary, System.Drawing.Point mousePosition)
        {
        }
    }
}
