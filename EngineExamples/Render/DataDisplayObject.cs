﻿using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EngineExamples.Render
{
    /// <summary>
    /// 
    /// </summary>
    public class DataDisplayObject : GenericSceneObject
    {
        private bool textInitialized;

        public DataDisplayObject()
        {
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, matrixStack, mouseState, scene);

            if (meshes.Any())
            {
                if (!textInitialized)
                {
                    foreach (var mesh in meshes)
                    {
                        foreach (var poly in mesh.GetAllPolygons())
                        {
                            var verts = poly.GetVertices(mesh);
                            var center = poly.FindCenter(mesh);

                            foreach (var vert in verts)
                            {
                                var vertObj = new GenericSceneObject();
                                vertObj.GetDrawState().IsText = true;
                                var centerDir = center - vert.Vector;
                                var finalPos = vert.Vector + centerDir * 0.5f;
                                vertObj.GetDrawState().Text = $"POS: ({vert.Vector.X}, {vert.Vector.Y}, {vert.Vector.Z}\nTEX COORDINATES: ({vert.U}, {vert.V})";
                                vertObj.GetObjectState().Transformation.PositionLocal = finalPos;
                                AddChild(vertObj);
                            }
                        }
                    }
                    textInitialized = true;
                }
            }
        }
    }
}
