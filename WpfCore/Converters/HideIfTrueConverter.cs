﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WpfCore.Converters
{
    public class HideIfTrueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(Visibility))
            {
                throw new InvalidOperationException("The target must be visibility.");
            }
            bool valueBool = bool.Parse(value.ToString());
            return valueBool ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
