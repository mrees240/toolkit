﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using WpfCore.Settings;

namespace WpfCore.Converters
{
    /// <summary>
    /// Converts a color file to a brush.
    /// </summary>
    public class ColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Color == false)
            {
                throw new InvalidOperationException("The value must be a color.");
            }

            return new SolidColorBrush((Color)value);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
