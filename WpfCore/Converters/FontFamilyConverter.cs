﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WpfCore.Converters
{
    /// <summary>
    /// Converts a FontFamily object from the System Drawing namespace to a Font Family from the System Windows Media namespace.
    /// </summary>
    public class FontFamilyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var drawFont = value as System.Drawing.FontFamily;

            if (drawFont != null)
            {
                System.Windows.Media.FontFamily windowsFont = new System.Windows.Media.FontFamily(drawFont.Name);
                return windowsFont;
            }
            throw new InvalidOperationException("The parameter must be a Font Family.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
