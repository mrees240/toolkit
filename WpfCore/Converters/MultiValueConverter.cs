﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfCore.Converters
{
    /// <summary>
    /// A general purpose converter for converting an array of objects.
    /// </summary>
    public class MultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
