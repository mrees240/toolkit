﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using WpfCore.Settings;

namespace WpfCore.Converters
{
    /// <summary>
    /// Converts a color file to a brush.
    /// </summary>
    public class ColorFileBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorFile = value as ColorFile;

            if (colorFile != null)
            {
                return colorFile.Brush;
            }
            throw new InvalidOperationException("The parameter must be a Color File.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
