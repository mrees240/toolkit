﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfCore.ViewModel.Input
{
    public class InputStateViewModel : ViewModelBase
    {
        /*private InputMouseState inputState;
        private InputMouseStateViewModel leftMouseButtonVm;
        private InputMouseStateViewModel rightMouseButtonVm;
        private InputMouseStateViewModel middleMouseButtonVm;

        public InputStateViewModel(InputMouseState inputState)
        {
            this.inputState = inputState;
            leftMouseButtonVm = new InputMouseStateViewModel(inputState.LeftMouseButton);
            rightMouseButtonVm = new InputMouseStateViewModel(inputState.RightMouseButton);
            middleMouseButtonVm = new InputMouseStateViewModel(inputState.MiddleMouseButton);
        }

        public InputMouseStateViewModel LeftMouseButtonVm { get => leftMouseButtonVm; }
        public InputMouseStateViewModel RightMouseButtonVm { get => rightMouseButtonVm; }
        public InputMouseStateViewModel MiddleMouseButtonVm { get => middleMouseButtonVm; }
        public bool IsOverViewport { get => inputState.IsOverViewport; }
        public bool MouseGrabbed { get => inputState.MouseGrabbed; }
        public string MousePosition { get { return $"{inputState.MousePosition.X},{inputState.MousePosition.Y}"; } }
        public string MousePositionBeforeGrab { get { return $"{inputState.MousePositionBeforeGrab.X},{inputState.MousePositionBeforeGrab.Y}"; } }
        public string MouseMovementDelta { get { return $"{inputState.MouseMovementDelta.X},{inputState.MouseMovementDelta.Y}"; } }
        public string GrabbingButton
        {
            get
            {
                if (inputState.GrabbingButton == null)
                {
                    return "None";
                }
                return inputState.GrabbingButton.ToString();
            }
        }

        public void Update()
        {
            leftMouseButtonVm.Update();
            rightMouseButtonVm.Update();
            middleMouseButtonVm.Update();

            RaisePropertyChanged(nameof(IsOverViewport));
            RaisePropertyChanged(nameof(MouseGrabbed));
            RaisePropertyChanged(nameof(GrabbingButton));
            RaisePropertyChanged(nameof(MousePosition));
            RaisePropertyChanged(nameof(MousePositionBeforeGrab));
            RaisePropertyChanged(nameof(MouseMovementDelta));
        }

    */
    }
}
