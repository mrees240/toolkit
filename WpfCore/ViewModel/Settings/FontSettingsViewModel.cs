﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using WpfCore.Settings;

namespace WpfCore.ViewModel.Settings
{
    public class FontSettingsViewModel : ViewModelBase
    {
        private FontSettings model;

        public event EventHandler FontSizeChanged;

        public FontSettingsViewModel()
        {
            model = new FontSettings();
            model.SetDefaultSettings();
            model.ReadFromFile();
        }

        public ObservableCollection<FontFamily> SystemFonts
        {
            get
            {
                return new ObservableCollection<FontFamily>(model.SystemFonts);
            }
        }

        public FontSettings Model { get => model; }
        public FontFamily SelectedFont
        {
            get => model.SelectedFont;
            set { model.SelectedFont = value; }
        }

        public virtual void OnFontSizeChanged()
        {
            if (FontSizeChanged != null)
            {
                FontSizeChanged(this, EventArgs.Empty);
            }
        }

        public double FontSize
        {
            get => model.FontSize;
            set {
                if (value < 5)
                {
                    value = 5;
                }
                model.FontSize = value;
                OnFontSizeChanged();
            }
        }

        public void SaveSettings()
        {
            model.WriteToFile();
            UpdateFontSettingsInUi();
        }

        public void ApplyDefaultSettings()
        {
            model.SetDefaultSettings();
            SaveSettings();
            UpdateFontSettingsInUi();
        }

        public void UpdateFontSettingsInUi()
        {
            RaisePropertyChanged(nameof(FontSize));
            RaisePropertyChanged(nameof(SelectedFont));
        }
    }
}
