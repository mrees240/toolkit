﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;

namespace WpfCore.ViewModel.Settings
{
    public class GenericWindowSettingsViewModel : ViewModelBase
    {
        private GenericWindowSettings windowSettings;

        public GenericWindowSettingsViewModel(GenericWindowSettings windowSettings)
        {
            this.windowSettings = windowSettings;
        }

        public double XPosition { get => windowSettings.XPosition; set { windowSettings.XPosition = value; RaisePropertyChanged();  } }
        public double YPosition { get => windowSettings.YPosition; set { windowSettings.YPosition = value; RaisePropertyChanged();  } }
        public double Width { get => windowSettings.Width; set { windowSettings.Width = value; RaisePropertyChanged();  }  }
        public double Height { get => windowSettings.Height; set { windowSettings.Height = value; RaisePropertyChanged();  } }
        public WindowState WindowState { get => windowSettings.WindowState; set { windowSettings.WindowState = value; RaisePropertyChanged(); } }
        public string WindowName { get { return windowSettings.WindowName; } }
    }
}
