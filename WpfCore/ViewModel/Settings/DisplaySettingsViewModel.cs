﻿using GalaSoft.MvvmLight;
using WpfCore.Settings;

namespace WpfCore.ViewModel.Settings
{
    public class DisplaySettingsViewModel : ViewModelBase
    {
        private DisplaySettings displaySettings;

        public DisplaySettingsViewModel()
        {
            displaySettings = new DisplaySettings();
            displaySettings.SetDefaultSettings();
            displaySettings.ReadFromFile();
        }

        public DisplaySettings Model
        {
            get
            {
                return displaySettings;
            }
        }

        public void UpdateRenderBackground()
        {
            //var renderVm = new ViewModelLocator().RenderVm;
            /*renderVm.BackgroundRed = model.RenderBackground.Red;
            renderVm.BackgroundGreen = model.RenderBackground.Green;
            renderVm.BackgroundBlue = model.RenderBackground.Blue;*/
        }

        public double TextureSize { get => displaySettings.TextureSize; set { displaySettings.TextureSize = value; RaisePropertyChanged(); } }

        public ColorFile TextureItemBackground { get => displaySettings.TextureItemBackground; set { displaySettings.TextureItemBackground = value; RaisePropertyChanged(); } }
        public ColorFile TexturePanelBackground { get => displaySettings.TexturePanelBackground; set { displaySettings.TexturePanelBackground = value; RaisePropertyChanged(); } }
        public ColorFile RenderBackground
        {
            get => displaySettings.RenderBackground;
            set
            {
                displaySettings.RenderBackground = value;
                UpdateRenderBackground();
                RaisePropertyChanged();
            }
        }
    }
}
