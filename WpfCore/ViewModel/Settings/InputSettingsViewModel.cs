﻿using GalaSoft.MvvmLight;
using WpfCore.Settings;

namespace WpfCore.ViewModel.Settings
{
    public class InputSettingsViewModel : ViewModelBase
    {
        private InputSettings inputSettings;

        public InputSettingsViewModel()
        {
            inputSettings = new InputSettings();
            inputSettings.SetDefaultSettings();
            inputSettings.ReadFromFile();
        }

        public void UpdateInputSettingsInUi()
        {
            RaisePropertyChanged(nameof(MouseSensitivity));
            RaisePropertyChanged(nameof(CameraPanSensitivity));
            RaisePropertyChanged(nameof(ZoomSpeedMultiplier));
        }

        public double CameraPanSensitivity
        {
            get
            {
                return inputSettings.CameraPanSensitivity;
            }
            set
            {
                inputSettings.CameraPanSensitivity = value;
                RaisePropertyChanged();
            }
        }

        public double ZoomSpeedMultiplier
        {
            get
            {
                return inputSettings.ZoomSpeedMultiplier;
            }
            set
            {
                inputSettings.ZoomSpeedMultiplier = value;
                RaisePropertyChanged();
            }
        }

        public double MouseSensitivity
        {
            get
            {
                return inputSettings.MouseSensitivity;
            }
            set
            {
                inputSettings.MouseSensitivity = value;
                RaisePropertyChanged();
            }
        }

        public void SaveSettings()
        {
            inputSettings.WriteToFile();
            UpdateInputSettingsInUi();
        }

        public void ApplyDefaultSettings()
        {
            inputSettings.SetDefaultSettings();
            SaveSettings();
            UpdateInputSettingsInUi();
        }

        public InputSettings InputSettings { get => inputSettings; set => inputSettings = value; }
    }
}
