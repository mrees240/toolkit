﻿using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfCore.Utility
{
    /// <summary>
    /// Handles input for Monogame. Monogame input library can't be used since the render context isn't in the default Monogame window.
    /// These are used for WPF window compatibility.
    /// </summary>
    public class InputUtility
    {
        [DllImport("User32.dll", SetLastError = true)]
        private static extern void ClipCursor(ref System.Drawing.Rectangle rect);

        [DllImport("User32.dll", SetLastError = true)]
        private static extern void ClipCursor(IntPtr rect);

        [DllImport("User32.dll", SetLastError = true)]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("User32.dll", SetLastError = true)]
        private static extern void SetCursor(System.Windows.Input.Cursor cursor);

        public static void SetCursorPosition(int x, int y)
        {
            SetCursorPos(x, y);
        }

        private static MousePoint cursorPos = new MousePoint();

        public static MousePoint GetCursorPosition()
        {
            //GetCursorPos(out cursorPos);
            var pos = System.Windows.Forms.Cursor.Position;
            cursorPos.SetCoordinates(pos.X, pos.Y);

            return cursorPos;
        }

        public static void HideCursor()
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.None;
        }

        public static void ShowCursor()
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }
    }
}
