﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for FontSettingsOptions.xaml
    /// </summary>
    public partial class FontSettingsOptions : UserControl
    {
        public FontSettingsOptions()
        {
            InitializeComponent();
        }
    }
}
