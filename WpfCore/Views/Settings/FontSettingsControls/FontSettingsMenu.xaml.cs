﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.InputSettingsControls
{
    /// <summary>
    /// Interaction logic for FontSettingsMenu.xaml
    /// </summary>
    public partial class FontSettingsMenu : UserControl
    {
        public FontSettingsMenu()
        {
            InitializeComponent();
        }
    }
}
