﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for InputSettingsOptions.xaml
    /// </summary>
    public partial class InputSettingsOptions : UserControl
    {
        public InputSettingsOptions()
        {
            InitializeComponent();
        }
    }
}
