﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.InputSettingsControls
{
    /// <summary>
    /// Interaction logic for InputSettingsMenu.xaml
    /// </summary>
    public partial class InputSettingsMenu : UserControl
    {
        public InputSettingsMenu()
        {
            InitializeComponent();
        }
    }
}
