﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for TextureItemBGControl.xaml
    /// </summary>
    public partial class TextureItemBGControl : UserControl
    {
        public TextureItemBGControl()
        {
            InitializeComponent();
        }
    }
}
