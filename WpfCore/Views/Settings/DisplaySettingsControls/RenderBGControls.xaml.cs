﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for RenderBGControls.xaml
    /// </summary>
    public partial class RenderBGControls : UserControl
    {
        public RenderBGControls()
        {
            InitializeComponent();
        }
    }
}
