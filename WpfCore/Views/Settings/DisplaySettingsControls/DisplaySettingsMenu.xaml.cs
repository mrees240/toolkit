﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for DisplaySettingsMenu.xaml
    /// </summary>
    public partial class DisplaySettingsMenu : UserControl
    {
        public DisplaySettingsMenu()
        {
            InitializeComponent();
        }
    }
}
