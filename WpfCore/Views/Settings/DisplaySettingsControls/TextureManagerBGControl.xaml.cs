﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings.Controls
{
    /// <summary>
    /// Interaction logic for TextureManagerBGControl.xaml
    /// </summary>
    public partial class TextureManagerBGControl : UserControl
    {
        public TextureManagerBGControl()
        {
            InitializeComponent();
        }
    }
}
