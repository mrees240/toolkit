﻿using System.Windows.Controls;

namespace WpfCore.Views.Settings
{
    /// <summary>
    /// Interaction logic for DisplaySettingsView.xaml
    /// </summary>
    public partial class DisplaySettingsView : UserControl
    {
        public DisplaySettingsView()
        {
            InitializeComponent();
        }
    }
}
