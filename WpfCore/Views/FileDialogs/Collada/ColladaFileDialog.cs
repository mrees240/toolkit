﻿using Microsoft.Win32;

namespace WpfCore.Views.FileDialogs.Collada
{
    public class ColladaFileDialog
    {
        private OpenFileDialog openDialog;
        private SaveFileDialog saveDialog;

        public ColladaFileDialog()
        {
            openDialog = new OpenFileDialog();
            saveDialog = new SaveFileDialog();
            string ext = ".dae";
            string filter = "Collada Files (.dae)|*.dae";
            openDialog.DefaultExt = ext;
            saveDialog.DefaultExt = ext;
            saveDialog.Filter = filter;
            openDialog.Filter = filter;
        }

        public OpenFileDialog OpenColladaFileDialog(bool multiSelect)
        {
            openDialog.Multiselect = multiSelect;
            return openDialog;
        }

        public SaveFileDialog SaveColladaFileDialog()
        {
            return saveDialog;
        }
    }
}
