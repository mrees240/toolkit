﻿using System;
using System.Drawing;
using System.IO;

namespace WpfCore.Settings
{
    /// <summary>
    /// Stores display settings related to the UI such as background colors.
    /// </summary>
    public class DisplaySettings : SettingsBase
    {
        private ColorFile textureItemBackground;
        private ColorFile texturePanelBackground;
        private ColorFile meshColor;
        private ColorFile renderBackground;
        private double textureSize;

        public DisplaySettings()
        {
            texturePanelBackground = new ColorFile();
            textureItemBackground = new ColorFile();
            renderBackground = new ColorFile();
            meshColor = new ColorFile();
        }

        public void SetDefaultSettings()
        {
            TexturePanelBackground.InitializeColor(47, 92, 149);
            TextureItemBackground.InitializeColor(232, 232, 232);
            RenderBackground.InitializeColor(153, 217, 234);
            TextureSize = 128.0;
            RevertColorProperties();
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(@"settings\display-settings.bin", FileMode.Open)))
                {
                    ReadColorFromFile(reader, TexturePanelBackground);
                    ReadColorFromFile(reader, TextureItemBackground);
                    ReadColorFromFile(reader, RenderBackground);
                }
            }
            catch (Exception noFileEx)
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void RevertColorProperties()
        {
            TextureItemBackground.RevertToPrevious();
            TexturePanelBackground.RevertToPrevious();
            RenderBackground.RevertToPrevious();
        }

        public void ReadColorFromFile(BinaryReader binaryReader, ColorFile color)
        {
            byte red = binaryReader.ReadByte();
            byte green = binaryReader.ReadByte();
            byte blue = binaryReader.ReadByte();
            color.ChangeColor(red, green, blue);
            color.ChangePreviousColor(red, green, blue);
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"settings\display-settings.bin", FileMode.Create)))
            {
                WriteColor(writer, TexturePanelBackground.Color);
                WriteColor(writer, TextureItemBackground.Color);
                WriteColor(writer, RenderBackground.Color);
            }
        }

        private Color ReadColor(BinaryReader reader)
        {
            byte r = reader.ReadByte();
            byte g = reader.ReadByte();
            byte b = reader.ReadByte();
            return Color.FromArgb(255, r, g, b);
        }

        private void WriteColor(BinaryWriter writer, Color color)
        {
            writer.Write(color.R);
            writer.Write(color.G);
            writer.Write(color.B);
        }

        public ColorFile TextureItemBackground { get { return textureItemBackground; } set { textureItemBackground = value; } }
        public ColorFile TexturePanelBackground { get { return texturePanelBackground; } set { texturePanelBackground = value; } }
        public ColorFile MeshColor { get { return meshColor; } }
        public ColorFile RenderBackground { get => renderBackground; set => renderBackground = value; }

        public double TextureSize { get => textureSize; set { textureSize = value; } }
    }
}
