﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfCore.Settings
{
    /// <summary>
    /// A base class for settings that includes a property changed event to update the UI.
    /// </summary>
    public abstract class SettingsBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
