﻿using System.IO;
using System.Windows.Media;

namespace WpfCore.Settings
{
    /// <summary>
    /// Stores color settings used by the UI.
    /// </summary>
    public class ColorFile : SettingsBase
    {
        private SolidColorBrush brush, previousBrush;

        public ColorFile()
        {
            brush = new SolidColorBrush(new Color());
            previousBrush = new SolidColorBrush(new Color());
        }

        public ColorFile(byte red, byte green, byte blue) : this()
        {
            ChangeColor(red, green, blue);
        }

        public System.Drawing.Color Color
        {
            get
            {
                System.Drawing.Color color = System.Drawing.Color.FromArgb(255, brush.Color.R, brush.Color.G, brush.Color.B);
                return color;
            }
        }

        public void InitializeColor(byte red, byte green, byte blue)
        {
            ChangeColor(red, green, blue);
            ChangePreviousColor(red, green, blue);
        }

        public void ChangeColor(byte red, byte green, byte blue)
        {
            Color color = new Color();
            color.A = 255;
            color.R = red;
            color.G = green;
            color.B = blue;
            brush.Color = color;
        }

        public void ChangePreviousColor(byte red, byte green, byte blue)
        {
            Color color = new Color();
            color.A = 255;
            color.R = red;
            color.G = green;
            color.B = blue;
            previousBrush.Color = color;
        }

        public void WriteToFile(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(brush.Color.R);
            binaryWriter.Write(brush.Color.G);
            binaryWriter.Write(brush.Color.B);
            ChangePreviousColor(brush.Color.R, brush.Color.G, brush.Color.B);
        }

        public void RevertToPrevious()
        {
            ChangeColor(previousBrush.Color.R, previousBrush.Color.G, previousBrush.Color.B);
            Red = previousBrush.Color.R;
            Green = previousBrush.Color.G;
            Blue = previousBrush.Color.B;
        }

        public SolidColorBrush Brush
        {
            get
            {
                return brush;
            }

            set
            {
                brush = value;
            }
        }

        public byte Red
        {
            get { return brush.Color.R; }
            set
            {
                ChangeColor(value, brush.Color.G, brush.Color.B);
                OnPropertyChanged();
            }
        }

        public byte Green
        {
            get { return brush.Color.G; }
            set
            {
                ChangeColor(brush.Color.R, value, brush.Color.B);
                OnPropertyChanged();
            }
        }

        public byte Blue
        {
            get { return brush.Color.B; }
            set
            {
                ChangeColor(brush.Color.R, brush.Color.G, value);
                OnPropertyChanged();
            }
        }

        public byte PreviousRed
        {
            get
            {
                return previousBrush.Color.R;
            }

            set
            {
                ChangePreviousColor(value, previousBrush.Color.G, previousBrush.Color.B);
                OnPropertyChanged();
            }
        }

        public byte PreviousGreen
        {
            get
            {
                return previousBrush.Color.G;
            }

            set
            {
                ChangePreviousColor(previousBrush.Color.R, value, previousBrush.Color.B);
                OnPropertyChanged();
            }
        }

        public byte PreviousBlue
        {
            get
            {
                return previousBrush.Color.B;
            }

            set
            {
                ChangePreviousColor(previousBrush.Color.R, previousBrush.Color.G, value);
                OnPropertyChanged();
            }
        }
    }
}
