﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;

namespace WpfCore.Settings
{
    /// <summary>
    /// Stores font settings used by the UI, such as font size and font family.
    /// </summary>
    public class FontSettings : SettingsBase
    {
        private double fontSize;
        private FontFamily selectedFont;
        private FontFamily[] systemFonts;

        public event EventHandler FontSizeChanged;

        public virtual void OnFontSizeChanged()
        {
            if (FontSizeChanged != null)
            {
                FontSizeChanged(this, EventArgs.Empty);
            }
        }

        public FontSettings()
        {
            LoadSystemFonts();
            SetDefaultSettings();
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(@"settings\font-settings.bin", FileMode.Open)))
                {
                    SelectedFont = new FontFamily(reader.ReadString());
                    FontSize = reader.ReadDouble();
                    SelectedFont = SelectedFont;
                    FontSize = FontSize;
                }
            }
            catch (Exception noFileEx)
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"settings\font-settings.bin", FileMode.Create)))
            {
                writer.Write(SelectedFont.Name);
                writer.Write(FontSize);
            }
        }

        public void SetDefaultSettings()
        {
            FontSize = 20.0;
            AttemptToLoadFontFamily("Segoe UI");
        }

        private void AttemptToLoadFontFamily(string fontSourceName)
        {
            SelectedFont = systemFonts.Where(f => f.Name == fontSourceName).FirstOrDefault();
            if (SelectedFont == null)
            {
                SelectedFont = System.Drawing.SystemFonts.DefaultFont.FontFamily;
            }
        }

        public double FontSize {
            get {
                return fontSize;
            }

            set {
                fontSize = value;
                OnPropertyChanged();
                OnFontSizeChanged();
            }
        }

        private void LoadSystemFonts()
        {
            SystemFonts = new InstalledFontCollection().Families;
        }

        public FontFamily[] SystemFonts {
            get {
                return systemFonts;
            }

            set {
                systemFonts = value;
                OnPropertyChanged();
            }
        }

        public FontFamily SelectedFont {
            get {
                return selectedFont;
            }

            set {
                selectedFont = value;
                OnPropertyChanged();
            }
        }
    }
}
