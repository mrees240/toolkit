﻿using System;
using System.IO;

namespace WpfCore.Settings
{
    public class InputSettings : SettingsBase
    {
        private double mouseSensitivity;
        private double cameraPanSensitivity;
        private double zoomSpeedMultiplier;
        private char forward, backward, left, right;

        public InputSettings()
        {
            SetDefaultSettings();
        }

        public void ReadFromFile()
        {
            Directory.CreateDirectory(@"settings");
            try
            {
                using (BinaryReader reader = new BinaryReader(File.Open(@"settings\input-settings.bin", FileMode.Open)))
                {
                    mouseSensitivity = reader.ReadDouble();
                    CameraPanSensitivity = reader.ReadDouble();
                    ZoomSpeedMultiplier = reader.ReadDouble();
                }
            }
            catch (Exception noFileEx)
            {
                WriteToFile();
                ReadFromFile();
            }
        }

        public void WriteToFile()
        {
            Directory.CreateDirectory(@"settings");
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"settings\input-settings.bin", FileMode.Create)))
            {
                writer.Write(mouseSensitivity);
                writer.Write(CameraPanSensitivity);
                writer.Write(ZoomSpeedMultiplier);
            }
        }

        public void SetDefaultSettings()
        {
            mouseSensitivity = 1.0;
            CameraPanSensitivity = 1.0;
            ZoomSpeedMultiplier = 1.0;
            forward = 'w';
            backward = 's';
            left = 'a';
            right = 'd';
        }

        public double MouseSensitivity { get => mouseSensitivity; set => mouseSensitivity = value; }
        public char Forward { get => forward; set => forward = value; }
        public char Backward { get => backward; set => backward = value; }
        public char Left { get => left; set => left = value; }
        public char Right { get => right; set => right = value; }

        public double CameraPanSensitivity
        {
            get { return cameraPanSensitivity; }
            set { cameraPanSensitivity = value; }
        }

        public double ZoomSpeedMultiplier
        {
            get { return zoomSpeedMultiplier; }
            set { zoomSpeedMultiplier = value; }
        }
    }
}
