﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfCore.Settings
{
    public class GenericWindowSettings : SettingsBase
    {
        private double xPosition;
        private double yPosition;
        private double width;
        private double height;
        private WindowState windowState;
        private string windowName;

        public GenericWindowSettings(string windowName)
        {
            this.windowName = windowName;
            windowState = WindowState.Normal;
            width = 500;
            height = 400;
        }

        public void WriteToFile(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(windowName);
            binaryWriter.Write(xPosition);
            binaryWriter.Write(yPosition);
            binaryWriter.Write(width);
            binaryWriter.Write(height);
            binaryWriter.Write((int)windowState);
        }

        public void ReadFromFile(BinaryReader reader)
        {
            reader.ReadString();
            xPosition = reader.ReadDouble();
            yPosition = reader.ReadDouble();
            width = reader.ReadDouble();
            height = reader.ReadDouble();
            windowState = (WindowState)reader.ReadInt32();
        }

        public double XPosition { get => xPosition; set => xPosition = value; }
        public double YPosition { get => yPosition; set => yPosition = value; }
        public double Width { get => width; set => width = value; }
        public double Height { get => height; set => height = value; }
        public WindowState WindowState { get => windowState; set => windowState = value; }
        public string WindowName { get => windowName; }
    }
}
