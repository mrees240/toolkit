# Twisted Toolkit #

### A general purpose modding tool ###

The Twisted Toolkit is an open-source project built in C#.
The UI was made with [Windows Presentation Foundation](https://docs.microsoft.com/en-us/dotnet/framework/wpf/) and 3D rendering was made with [MonoGame](http://www.monogame.net/).

You must agree to the project license located in the LICENSE.md file before any access or modification to the source code.

## What are the core modules of the toolkit? ##

* Collada Library
* Twisted Library
* WPF Core
* MonoEngine
* Twisted Tool
* Map Editor

The Collada library is a small library used for reading/writing Collada (DAE) files.

The Twisted Library is a library capable of reading and writing mesh and texture files used by diffent game engines. It uses the Model View Controller design pattern to help keep separation of concerns. The Twisted Library doesn't have anything related to the UI.

The WPF Core contains WPF related controls used by multiple applications.

The MonoEngine core contains reusable code used for rendering 3D shapes using the MonoGame framework.

The Twisted Tool module is the toolkit itself. It includes user-controls such as the texture manager panel.

The Map

## Building Requirements ##

* [Visual Studio 2017](https://www.visualstudio.com/downloads/)
* [.Net Framework 4.5.2](https://www.microsoft.com/en-us/download/details.aspx?id=42643&desc=dotnet452)
* [DirectX SDK](https://www.microsoft.com/en-us/download/details.aspx?id=6812)
* [MonoGame Pipeline](http://www.monogame.net/downloads/)

Before you can build the toolkit, you will need the MonoGame pipeline to compile the shaders used by MonoGame. Start the MonoGame Pipeline Tool and open the TMTool/Content/Content.mgcb file. Build the content file by using the Build menu option.