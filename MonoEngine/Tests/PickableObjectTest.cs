﻿using ColladaLibrary.Render;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Tests
{
    /// <summary>
    /// Provides methods for creating a basic mesh that can be picked by the mouse.
    /// Used for testing purposes
    /// </summary>
    public class PickableObjectTest
    {
        private Mesh mesh;
        private PickTestSceneObject sceneObject;

        public PickableObjectTest()
        {
            Initialize();
        }

        public void StartTest(MonoEngine.Code.Scenes.MonoEngineScene scene)
        {
            scene.AddObject(sceneObject);
        }

        private void InitializeMesh()
        {
            var poly = new Polygon();
            var v0 = new Vertex(new System.Numerics.Vector3());
            var v1 = new Vertex(new System.Numerics.Vector3(1000.0f, 0.0f, 0.0f));
            var v2 = new Vertex(new System.Numerics.Vector3(0.0f, 1000.0f, 0.0f));
            var i0 = new PolygonIndex(0);
            var i1 = new PolygonIndex(1);
            var i2 = new PolygonIndex(2);
            mesh = new Mesh();
            poly.SetIndex(0, i0);
            poly.SetIndex(1, i1);
            poly.SetIndex(2, i2);
            mesh.AddPolygon(poly);
            mesh.Vertices.Add(v0);
            mesh.Vertices.Add(v1);
            mesh.Vertices.Add(v2);
        }

        private void InitializeSceneObject()
        {
            sceneObject = new PickTestSceneObject();
            sceneObject.SetMeshes(new List<Mesh>() { mesh });
        }

        private void Initialize()
        {
            InitializeMesh();
            InitializeSceneObject();
        }
    }
}
