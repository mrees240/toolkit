﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public class TransformationGizmo : GenericSceneObject
    {
        private TranslationGizmo translationGizmo;
        private IDrawableSceneObject target;

        private Matrix4x4 scaleMat;
        private Matrix4x4 transMat;
        private float scale = 0.1f;

        private bool drawStateInitialized;

        public TransformationGizmo(IDrawableSceneObject target, TransformationGizmoManager gizmoManager, MonoEngineScene scene)
        {
            scaleMat = Matrix4x4.Identity;
            transMat = Matrix4x4.Identity;
            this.target = target;
            translationGizmo = new TranslationGizmo(this, target, gizmoManager, scene);
            objectState.Name = "Transformation Gizmo";
            drawableChildren.Add(translationGizmo);
            objectState.TranslationOnly = true;
            objectState.OnlyVisibleInEditor = true;
            objectState.IsPickable = true;
            drawState.IsVisible = false;
        }

        public TranslationGizmo TranslationGizmo { get => translationGizmo; }
        public IDrawableSceneObject Target { get => target; }

        public override void PreDraw(MonoViewportViewModel monoViewportViewModel)
        {
            base.PreDraw(monoViewportViewModel);
            if (!drawStateInitialized)
            {
                drawState.IsVisible = false;
                drawStateInitialized = true;
            }
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, Matrix4x4.CreateTranslation(matrixStack.Translation), mouseState, scene);
        }

        public void Translate(Vector3 newPosition, TransformationViewModel transformationVm)
        {
            transformationVm.XPositionWorld = newPosition.X;
            transformationVm.YPositionWorld = newPosition.Y;
            transformationVm.ZPositionWorld = newPosition.Z;
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);

            drawState.ActiveShader = scene.Shaders.NormalShader;
        }
    }
}
