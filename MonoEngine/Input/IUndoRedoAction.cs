﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    /// <summary>
    /// Is used in a stack to keep track of the users actions.
    /// </summary>
    public interface IUndoRedoAction
    {
        string GetActionText();
        void Action();
        void UndoAction();
    }
}
