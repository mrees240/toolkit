﻿using Microsoft.Xna.Framework.Graphics;
using MonoEngine.ViewModel;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public class MouseState
    {
        private int activeViewportId;
        private bool grabbed;
        private bool dragging;
        private MouseButton grabOwner;
        private MouseButton dragOwner;
        private MousePoint previousMousePosition;
        private MousePoint mousePositionDpiScaled;
        private MousePoint nextMousePosition;
        private MousePoint mouseMovementDelta;
        private MousePoint positionBeforeGrab;
        private MonoViewportViewModel activeViewport;
        private MousePoint activeViewportRelativeMousePosition;
        private System.Windows.Point defaultMousePosition;
        private MousePoint windowPoint;

        public bool Grabbed { get => grabbed; }
        public bool Dragging { get => dragging; }
        public MouseButton GrabOwner { get => grabOwner; }
        public MouseButton DragOwner { get => dragOwner; }

        public event System.EventHandler MouseStateChanged;

        public MouseState()
        {
            activeViewportId = -1;
            defaultMousePosition = new System.Windows.Point();
            mousePositionDpiScaled = new MousePoint();
            mouseMovementDelta = new MousePoint();
            positionBeforeGrab = new MousePoint();
            previousMousePosition = new MousePoint();
            nextMousePosition = new MousePoint();
            windowPoint = new MousePoint();
            activeViewportRelativeMousePosition = new MousePoint();
        }

        public virtual void OnMouseStateChanged()
        {
            if (MouseStateChanged != null)
            {
                MouseStateChanged(this, EventArgs.Empty);
            }
        }

        public void Drag(MouseButton dragOwner)
        {
            dragging = true;
            this.dragOwner = dragOwner;
            OnMouseStateChanged();
        }

        public void ReleaseDrag()
        {
            dragging = false;
            this.dragOwner = null;
            OnMouseStateChanged();
        }

        public void Grab(MouseButton grabOwner)
        {
            //windowPoint
            var width = App.Current.MainWindow.Width;
            var height = App.Current.MainWindow.Height;

            //if (InsideBoundary(MousePosition.X, MousePosition.Y, windowPt.X, windowPt.Y, windowPt.X +  width, windowPt.Y + height))
            if (App.Current.MainWindow.IsMouseOver)
            {
                grabbed = true;
                this.grabOwner = grabOwner;
            }
            OnMouseStateChanged();
        }

        public void ReleaseGrab()
        {
            grabbed = false;
            this.grabOwner = null;
            OnMouseStateChanged();
        }

        public MousePoint GetActiveViewportRelativeMousePosition()
        {
            if (activeViewport != null)
            {
                var relX = previousMousePosition.X - activeViewport.XPos;
                var relY = previousMousePosition.Y - activeViewport.YPos;

                activeViewportRelativeMousePosition.X = relX;
                activeViewportRelativeMousePosition.Y = relY;
            }
            else
            {
                activeViewportRelativeMousePosition.X = 0;
                activeViewportRelativeMousePosition.Y = 0;
            }
            return activeViewportRelativeMousePosition;
        }

        public MousePoint MousePosition { get => previousMousePosition; set { previousMousePosition = value; OnMouseStateChanged(); } }
        public MonoViewportViewModel ActiveViewport { get => activeViewport; set { activeViewport = value; OnMouseStateChanged(); } }
        public MousePoint PositionBeforeGrab { get => positionBeforeGrab; set { positionBeforeGrab = value; OnMouseStateChanged(); } }
        public MousePoint MouseMovementDelta { get => mouseMovementDelta; set { mouseMovementDelta = value; OnMouseStateChanged(); } }
        public int ActiveViewportId { get => activeViewportId; set { activeViewportId = value; OnMouseStateChanged(); } }

        public MousePoint MousePositionDpiScaled { get => mousePositionDpiScaled; }
    }
}
