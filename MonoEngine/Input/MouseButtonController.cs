﻿using MonoEngine.Code.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonoEngine.Input
{
    public class MouseButtonController
    {
        private bool clickHandled;
        private IMouseButtonActions mouseButtonActions;
        private MouseState mouseState;

        public MouseButtonController(MouseState mouseState, IMouseButtonActions mouseButtonActions)
        {
            this.mouseState = mouseState;
            this.mouseButtonActions = mouseButtonActions;
        }

        public void UpdateInput(int delta, MonoEngineScene scene)
        {
            var buttonDown = MouseButtonDown();

            if (mouseState.ActiveViewport != null)
            {
                if (mouseState.Dragging)
                {
                    if (!buttonDown)
                    {
                        mouseState.ReleaseDrag();
                    }
                }
                else
                {
                    if (buttonDown)
                    {
                        if (!clickHandled)
                        {
                            ClickAction(scene);
                        }
                        else
                        {
                            HoldAction(scene);
                        }
                        DownAction(scene);
                    }
                    else
                    {
                        if (clickHandled)
                        {
                            ReleasedAction(scene);
                        }
                    }
                }
            }
            DefaultAction(scene);

        }

        private void DefaultAction(MonoEngineScene scene)
        {
            mouseButtonActions.DefaultAction(scene);
        }

        private void DownAction(MonoEngineScene scene)
        {
            mouseButtonActions.GetLmb().MouseButtonState.MouseDown = true;
            mouseButtonActions.DownAction(scene);
        }

        private void ClickAction(MonoEngineScene scene)
        {
            mouseButtonActions.GetLmb().MouseButtonState.MouseClicked = true;
            mouseButtonActions.ClickAction(scene);
            clickHandled = true;
        }

        private void HoldAction(MonoEngineScene scene)
        {
            mouseButtonActions.GetLmb().MouseButtonState.MouseClicked = false;
            mouseButtonActions.HoldAction(scene);
        }

        private void ReleasedAction(MonoEngineScene scene)
        {
            mouseButtonActions.GetLmb().MouseButtonState.MouseDown = false;
            mouseButtonActions.GetLmb().MouseButtonState.MouseClicked = false;
            mouseButtonActions.ReleaseAction(scene);
            mouseState.ReleaseDrag();
            clickHandled = false;
        }

        private bool MouseButtonDown()
        {
            var mouseType = mouseButtonActions.GetLmb().MouseButtonType;
            return (System.Windows.Forms.Control.MouseButtons & mouseType) == mouseButtonActions.GetLmb().MouseButtonType;
        }

        public IMouseButtonActions MouseButtonActions { get => mouseButtonActions; }
    }
}
