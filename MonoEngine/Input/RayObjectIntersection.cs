﻿using ColladaLibrary.Misc;
using MonoEngine.Code.Render;
using MonoEngine.Input.MouseInputs;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonoEngine.Code;
using MonoEngine.Code.Render.Debug;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoGameCore.Render;
using System.Numerics;
using System.Windows.Forms;
using System.Windows.Input;

namespace MonoEngine.Input
{
    public class RayObjectIntersection
    {
        private IDrawableSceneObject sceneObject;
        private IntersectionResult result;

        public RayObjectIntersection()
        {
            result = new IntersectionResult();
        }

        public RayObjectIntersection(IDrawableSceneObject sceneObject, IntersectionResult result)
        {
            this.sceneObject = sceneObject;
            this.result = result;
        }

        public void SetResult(IntersectionResult result, IDrawableSceneObject sceneObject)
        {
            this.result = result;
            this.sceneObject = sceneObject;
        }

        public bool HasSelectedObject()
        {
            return sceneObject != null;
        }

        public void Reset()
        {
            sceneObject = null;
            result.Reset();
        }

        public IDrawableSceneObject SceneObject { get => sceneObject; }
        public IntersectionResult Result { get => result; }
    }

}
