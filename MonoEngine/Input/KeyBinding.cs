﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonoEngine.Input
{
    public class KeyBinding
    {
        private System.Windows.Input.Key defaultKey;
        private System.Windows.Input.Key currentKey;
        private KeyConverter converter;

        public event EventHandler KeyBindingChanged;

        public KeyBinding(System.Windows.Input.Key defaultKey)
        {
            converter = new KeyConverter();
            this.defaultKey = defaultKey;
            SetDefaultSettings();
        }

        public virtual void OnKeyBindingChanged()
        {
            if (KeyBindingChanged != null)
            {
                KeyBindingChanged(this, EventArgs.Empty);
            }
        }

        public Key DefaultKey { get => defaultKey; }
        public Key CurrentKey { get => currentKey; set { currentKey = value; OnKeyBindingChanged(); } }

        public KeyConverter Converter { get => converter; }

        public void WriteToFile(BinaryWriter writer)
        {
            string keyText = converter.ConvertToString(currentKey);
            writer.Write(keyText);
        }

        public void ReadFromFile(BinaryReader reader)
        {
            string keyText = reader.ReadString();
            CurrentKey = (System.Windows.Input.Key)converter.ConvertFromString(keyText);
        }

        public void SetDefaultSettings()
        {
            this.currentKey = defaultKey;
        }
    }
}
