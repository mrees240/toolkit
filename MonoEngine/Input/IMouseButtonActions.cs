﻿using MonoEngine.Code.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public interface IMouseButtonActions
    {
        void DefaultAction(MonoEngineScene scene);
        void ClickAction(MonoEngineScene scene);
        void HoldAction(MonoEngineScene scene);
        void DownAction(MonoEngineScene scene);
        void ReleaseAction(MonoEngineScene scene);
        void DoubleClickAction(MonoEngineScene scene);

        MouseButton GetLmb();
    }
}
