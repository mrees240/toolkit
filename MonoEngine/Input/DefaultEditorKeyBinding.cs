﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonoEngine.Input
{
    public class DefaultEditorKeyBinding
    {
        private Key linkKey;
        private Key oneWayLinkKey;
        private Key deleteKey;

        public DefaultEditorKeyBinding()
        {
            SetDefaultSettings();
        }

        private void SetDefaultSettings()
        {
            linkKey = Key.LeftShift;
            oneWayLinkKey = Key.LeftCtrl;
            deleteKey = Key.LeftAlt;
        }

        public Key LinkKey { get => linkKey; set => linkKey = value; }
        public Key OneWayLinkKey { get => oneWayLinkKey; set => oneWayLinkKey = value; }
        public Key DeleteKey { get => deleteKey; set => deleteKey = value; }
    }
}
