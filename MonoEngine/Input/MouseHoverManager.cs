﻿using MonoEngine.Code;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoGameCore.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonoEngine.Input
{
    /// <summary>
    /// Checks if the mouse is hovering every single object in the scene.
    /// You can then use hovered objects to filter out objects for mouse picking.
    /// </summary>
    public class MouseHoverManager
    {
        private MonoEngineScene scene;

        private IntersectionResult directHoverResult;
        private IntersectionResult directHoverResultNoDepth;
        private GenericSceneObject directHoverObject;
        private GenericSceneObject directHoverObjectNoDepth;
        private Vector3 pickCoordinate;

        public MouseHoverManager(MonoEngineScene scene)
        {
            this.scene = scene;
        }

        public GenericSceneObject DirectHoverObject { get => directHoverObject; }
        public GenericSceneObject DirectHoverObjectNoDepth { get => directHoverObjectNoDepth; }
        public Vector3 PickCoordinate { get => pickCoordinate; }
        public IntersectionResult DirectHoverResult { get => directHoverResult; }
        public IntersectionResult DirectHoverResultNoDepth { get => directHoverResultNoDepth; }

        private void UpdateMouseHoverObject(IDrawableSceneObject sceneObject, MonoGameCore.Input.Ray mouseRay)
        {
            var genericSceneObject = sceneObject as GenericSceneObject;

            var objState = sceneObject.GetObjectState();
            var drawState = sceneObject.GetDrawState();
            objState.IsMouseHovered = false;
            objState.IsDirectlyMouseHovered = false;

            if (drawState.IsVisible)
            {
                if (objState.IsPickable)
                {
                    var intersection = new IntersectionResult();
                    objState.MouseRayIntersectsObject(mouseRay, intersection);

                    SetMouseHovered(sceneObject, intersection.Intersects);

                    if (intersection.Intersects)
                    {
                        if (sceneObject.GetDrawState().IgnoresDepthBuffer)
                        {
                            if (directHoverResultNoDepth == null || intersection.Distance < directHoverResultNoDepth.Distance)
                            {
                                directHoverResultNoDepth = intersection;
                                directHoverObjectNoDepth = genericSceneObject;
                                pickCoordinate = intersection.IntersectionCoordinate;
                            }
                        }
                        else
                        {
                            if (directHoverResult == null || intersection.Distance < directHoverResult.Distance)
                            {
                                directHoverResult = intersection;
                                directHoverObject = genericSceneObject;
                                pickCoordinate = intersection.IntersectionCoordinate;
                            }
                        }
                    }
                    else
                    {
                        pickCoordinate = new Vector3();
                    }
                }
                foreach (var child in sceneObject.GetDrawableChildren())
                {
                    UpdateMouseHoverObject(child, mouseRay);
                }
            }
        }

        public void UpdateMouseHoverObjects()
        {
            var activeViewport = scene.ActiveMonoViewportVm;
            directHoverResult = null;
            directHoverResultNoDepth = null;
            directHoverObject = null;
            directHoverObjectNoDepth = null;

            if (activeViewport != null)
            {
                if (activeViewport.IsMouseOver)
                {
                    var mouseRay = activeViewport.MouseRay.GetRay();

                    foreach (var sceneObject in scene.SceneObjects)
                    {
                        UpdateMouseHoverObject(sceneObject, mouseRay);
                    }
                }
            }
            if (directHoverResultNoDepth != null && directHoverResultNoDepth.Intersects)
            {
                if (directHoverObjectNoDepth != null)
                {
                    directHoverObjectNoDepth.GetObjectState().IsDirectlyMouseHovered = true;
                }
            }
            else if (directHoverResult != null && directHoverResult.Intersects)
            {
                if (directHoverObject != null)
                {
                    directHoverObject.GetObjectState().IsDirectlyMouseHovered = true;
                }
            }
        }

        private void SetMouseHovered(IDrawableSceneObject sceneObject, bool isHovered)
        {
            sceneObject.GetObjectState().IsMouseHovered = isHovered;

            foreach (var child in sceneObject.GetDrawableChildren())
            {
                SetMouseHovered(child, isHovered);
            }
        }
    }
}
