﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public class MouseButtonState
    {
        private bool mouseClicked;
        private bool mouseHold;
        private bool mouseDown;
        private bool mouseDoubleClicked;

        public MouseButtonState()
        {

        }

        public bool MouseClicked { get => mouseClicked; set => mouseClicked = value; }
        public bool MouseHold { get => mouseHold; set => mouseHold = value; }
        public bool MouseDown { get => mouseDown; set => mouseDown = value; }
        public bool MouseDoubleClicked { get => mouseDoubleClicked; set => mouseDoubleClicked = value; }
    }
}
