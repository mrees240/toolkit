﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public interface IDeletableSceneObject
    {
        string GetObjectName();
        void Delete();
        void Undelete();
    }
}
