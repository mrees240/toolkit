﻿using MonoEngine.Code.Scenes;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Settings;
using WpfCore.ViewModel;

namespace MonoEngine.Input.MouseInputs
{
    public class DefaultOrbitRmbAction : IMouseButtonActions
    {
        private MouseButton mouseButton;
        private InputSettings inputSettings;
        private MouseState mouseState;
        private List<IMonoViewportViewModelWrapper> viewportVms;

        public DefaultOrbitRmbAction(
            InputSettings inputSettings,
            MouseState mouseState,
            MouseButton mouseButton,
            List<IMonoViewportViewModelWrapper> viewportVms)
        {
            this.mouseState = mouseState;
            this.viewportVms = viewportVms;
            this.inputSettings = inputSettings;
            this.mouseButton = mouseButton;
        }

        /*public void UpdateViewportVm(MonoViewportViewModel viewportVm, MouseState mouseState)
        {
            this.viewportVm = viewportVm;
            this.mouseState = mouseState;
        }*/

        public void ClickAction(MonoEngineScene scene)
        {
            if (!mouseState.Dragging)
            {
                mouseState.Grab(mouseButton);
            }
        }

        public void DefaultAction(MonoEngineScene scene)
        {
        }

        public void DownAction(MonoEngineScene scene)
        {
        }

        public void HoldAction(MonoEngineScene scene)
        {
            if (scene.ActiveMonoViewportVm != null && scene.ActiveMonoViewportVm.IsMouseOver)
            {
                if (mouseState.Dragging)
                {
                    mouseState.ReleaseDrag();
                }
                if (mouseState.ActiveViewportId >= 0)
                {
                    var viewportVm = viewportVms[mouseState.ActiveViewportId].GetBaseViewportVm();

                    viewportVm.CameraController.RotateLeftRight((float)mouseState.MouseMovementDelta.X, 1.0f);
                    viewportVm.CameraController.RotateUpDown((float)mouseState.MouseMovementDelta.Y, 1.0f);
                }
            }
        }

        public void ReleaseAction(MonoEngineScene scene)
        {
            if (!mouseState.Dragging)
            {
                mouseState.ReleaseGrab();
            }
        }

        public MouseButton GetLmb()
        {
            return mouseButton;
        }

        public void DoubleClickAction(MonoEngineScene scene)
        {
        }
    }
}
