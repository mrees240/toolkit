﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using MathNet.Spatial.Euclidean;
using MonoEngine.Code;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Debug;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoEngine.Utility;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using ColladaLibrary.Utility;
using MonoEngine.ViewModel;

namespace MonoEngine.Input.MouseInputs
{
    public class DefaultEditorLmbAction : IMouseButtonActions
    {
        private MonoEngineCore engine;
        private TransformationGizmoManager gizmoManager;
        private DefaultEditorKeyBinding keys;
        private KeyboardCommandController keyboardController;
        private MousePoint originalPickScreenCoord;
        private MousePoint worldMatrixScreenCoord;
        private MouseState mouseState;
        private MouseButton lmb;
        private CameraMouseRay mouseRay;
        private GenericSceneObject linkObjectOne;
        private GenericSceneObject linkObjectTwo;
        private IDrawableSceneObject selectedObject;
        private TranslationHandle translationHandle;

        private RayCollisionFilter groundCollisionFilter;
        private RayCollisionFilter defaultFilter;

        public event EventHandler SelectionChanged;

        private IntersectionResult groundIntersectionResult;
        private IntersectionResult mouseIntersectionResult;
        private IntersectionResult originalIntersectionResult;
        private Vector3 originalPickPosition;
        private Matrix4x4 originalWorldMatrix;
        private Matrix4x4 originalWorldMatrixHandle;
        private Vector3 pickOffset;

        private bool toggleKeyDown;
        private bool linkKeyDown;
        private bool isDragging;

        public bool ToggleKeyDown { get => toggleKeyDown; set => toggleKeyDown = value; }
        public bool LinkKeyDown { get => linkKeyDown; set => linkKeyDown = value; }
        public IDrawableSceneObject PickedObject
        {
            get => pickedObject;
            set
            {
                IDrawableSceneObject prevPicked = pickedObject;
                pickedObject = value;
                if (pickedObject != null)
                {
                    if (pickedObject.GetObjectState().PickableRoot != null)
                    {
                        pickedObject = pickedObject.GetObjectState().PickableRoot;
                    }
                    pickedObject.GetObjectState().IsSelected = true;
                    if (prevPicked != null)
                    {
                        prevPicked.GetObjectState().IsSelected = false;
                    }
                }
                OnSelectionChanged();
            }
        }

        public bool IsDragging { get => isDragging; }
        public TransformationViewModel PickedObjectTransformationVm { get => pickedObjectTransformationVm; set => pickedObjectTransformationVm = value; }

        private List<IDrawableSceneObject> mouseHoveredObjects;
        private IDrawableSceneObject mouseDirectlyHoveredObject;
        private IntersectionResult mouseDirectlyHoveredResult;

        private List<IDrawableSceneObject> mouseHoveredDrawFirstObjects;
        private IDrawableSceneObject mouseDirectlyHoveredDrawNoDepth;
        private IntersectionResult mouseDirectlyHoveredDrawFirstResult;
        private IDrawableSceneObject pickedObject;
        private IntersectionResult pickedObjectIntersectionResult;
        private MonoEngineScene scene;

        private TranslationHandle pickedTranslationHandle;

        private TransformationViewModel pickedObjectTransformationVm;

        private Vector3 thOriginalTargetPos;
        private Vector3 thOriginalPlaneIntersectionPos;
        private MathNet.Spatial.Euclidean.Plane thOriginalPlane;

        private Vector3 thCurrentTargetPos;
        private Vector3 thCurrentPlaneIntersectionPos;


        public virtual void OnSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        public DefaultEditorLmbAction(MonoEngineCore engine, MouseState mouseState, bool alwaysPickable)
        {
            this.engine = engine;
            this.scene = engine.GameScene;
            this.originalPickScreenCoord = new MousePoint();
            this.worldMatrixScreenCoord = new MousePoint();
            this.mouseState = mouseState;
            this.gizmoManager = engine.GameScene.TransformationGizmoManager;
            this.keys = new DefaultEditorKeyBinding();
            this.lmb = new MouseButton(MouseButtons.Left);
            this.keyboardController = engine.KeyboardController;
            mouseHoveredObjects = new List<IDrawableSceneObject>();
            mouseHoveredDrawFirstObjects = new List<IDrawableSceneObject>();
            mouseDirectlyHoveredResult = new IntersectionResult();
            mouseDirectlyHoveredDrawFirstResult = new IntersectionResult();
            mouseIntersectionResult = new IntersectionResult();
            groundIntersectionResult = new IntersectionResult();
            defaultFilter = new RayCollisionFilter();
            groundCollisionFilter = new RayCollisionFilter();
            groundCollisionFilter.ExcludeVisibleInEditorOnly = true;
        }

        public void ResetSelectedObject()
        {
            selectedObject = null;
            OnSelectionChanged();
        }

        // May need fixed
        public IntersectionResult FindMouseRayIntersectionResult(RayCollisionFilter filter)
        {
            var ray = mouseRay.GetRay();
            mouseIntersectionResult.Reset();

            var testObject = scene.MouseHoverManager.DirectHoverObject;

            if (testObject != null)
            {
                if (filter.ApprovedByFilter(testObject))
                {
                    var currentResult = new IntersectionResult();

                    testObject.GetObjectState().MouseRayIntersectsObject(ray, currentResult);

                    if (currentResult.Intersects && (!mouseIntersectionResult.Intersects || currentResult.Distance < mouseIntersectionResult.Distance))
                    {
                        mouseIntersectionResult.SetResult(currentResult.Distance, currentResult.IntersectionCoordinate);
                    }
                }
            }


            return mouseIntersectionResult;
        }

        public void ClickAction(MonoEngineScene scene)
        {
            mouseDirectlyHoveredDrawNoDepth = scene.MouseHoverManager.DirectHoverObjectNoDepth;
            mouseDirectlyHoveredObject = scene.MouseHoverManager.DirectHoverObject;

            if (linkKeyDown)
            {
                LinkClickAction(scene);
            }
            else if (toggleKeyDown)
            {
                ToggleClickAction(scene);
            }
            else
            {
                DefaultClickAction(scene);
            }
        }

        private void ToggleClickAction(MonoEngineScene scene)
        {
            if (mouseDirectlyHoveredObject != null)
            {
                mouseDirectlyHoveredObject.GetObjectState().OnToggleActivated();
            }
        }

        private void LinkClickAction(MonoEngineScene scene)
        {
            if (linkObjectOne == null)
            {
                if (mouseDirectlyHoveredObject != null)
                {
                    linkObjectOne = mouseDirectlyHoveredObject as GenericSceneObject;
                    linkObjectOne.GetObjectState().IsLinkPicked = true;
                }
            }
            else
            {
                if (mouseDirectlyHoveredObject != null)
                {
                    linkObjectTwo = mouseDirectlyHoveredObject as GenericSceneObject;
                    if (linkObjectOne != null)
                    {
                        linkObjectOne.GetObjectState().IsLinkPicked = false;
                    }
                    if (linkObjectTwo != null)
                    {
                        linkObjectTwo.GetObjectState().IsLinkPicked = true;
                    }

                    linkObjectOne.LinkWith(linkObjectTwo);
                    linkObjectOne = linkObjectTwo;
                    linkObjectTwo = null;
                }
            }
        }

        private void DefaultClickAction(MonoEngineScene scene)
        {
            var prevPicked = pickedObject;
            IDrawableSceneObject currentPicked = null;
            IntersectionResult currentPickIntersectionResult = new IntersectionResult();
            mouseDirectlyHoveredDrawNoDepth = scene.MouseHoverManager.DirectHoverObjectNoDepth;
            mouseDirectlyHoveredObject = scene.MouseHoverManager.DirectHoverObject;

            if (mouseDirectlyHoveredDrawNoDepth != null)
            {
                currentPicked = mouseDirectlyHoveredDrawNoDepth;
                currentPickIntersectionResult = mouseDirectlyHoveredDrawFirstResult;
            }
            else if (mouseDirectlyHoveredObject != null)
            {
                currentPicked = mouseDirectlyHoveredObject;
                currentPickIntersectionResult = mouseDirectlyHoveredResult;
            }
            else
            {
                if (PickedObject != null)
                {
                    pickedObject.GetObjectState().IsSelected = false;
                }
                PickedObject = null;
                if (pickedTranslationHandle != null)
                {
                    pickedTranslationHandle.GetObjectState().IsSelected = false;
                    pickedTranslationHandle = null;
                }
            }
            if (currentPicked != null)
            {
                if (mouseDirectlyHoveredDrawNoDepth != null)
                {
                    //mouseDirectlyHoveredDrawNoDepth.GetObjectState().OnPickActivated();
                    var translationHandle = currentPicked as TranslationHandle;

                    if (translationHandle != null)
                    {
                        var transformationParent = translationHandle.GetTranslationGizmo().GetParentTransformationGizmo();
                        pickedTranslationHandle = translationHandle;
                        var target = transformationParent.Target;
                        target.GetObjectState().IsSelected = true;

                        thOriginalPlane = pickedTranslationHandle.GetPlane(scene.ActiveMonoViewportVm.Camera);

                        var thOriginalPlaneIntersectionPosPt = mouseRay.GetRay().RayPlaneIntersection(thOriginalPlane);
                        if (thOriginalPlaneIntersectionPosPt.HasValue)
                        {
                            thOriginalPlaneIntersectionPos = ColladaLibrary.Utility.RenderUtility.ToVector3(thOriginalPlaneIntersectionPosPt.Value);
                            thOriginalTargetPos = target.GetObjectState().Transformation.WorldMatrix.Translation;
                        }
                        pickedTranslationHandle = translationHandle;
                        pickedTranslationHandle.GetObjectState().OnPickActivated();
                    }
                }
                else if (mouseDirectlyHoveredObject != null)
                {
                    bool selectionChanged = true;
                    if (selectionChanged)
                    {
                        if (pickedTranslationHandle != null)
                        {
                            pickedTranslationHandle.GetObjectState().IsSelected = false;
                            pickedTranslationHandle = null;
                        }
                        if (prevPicked != null)
                        {
                            prevPicked.GetObjectState().IsSelected = false;
                        }
                    }
                    PickedObject = currentPicked;
                    if (pickedObject != null)
                    {
                        pickedObject.GetObjectState().IsSelected = true;
                    }
                }
            }
        }

        private IDrawableSceneObject GetNearestObject()
        {
            return null;
        }

        private void SetMouseHovered(IDrawableSceneObject sceneObj, Ray mouseRay)
        {
        }

        public void DefaultAction(MonoEngineScene scene)
        {
            mouseHoveredObjects.Clear();
            mouseDirectlyHoveredResult.Reset();
            mouseDirectlyHoveredObject = null;

            mouseHoveredDrawFirstObjects.Clear();
            mouseDirectlyHoveredDrawFirstResult.Reset();
            mouseDirectlyHoveredObject = null;

            if (scene.ActiveMonoViewportVm != null)
            {
                mouseRay = scene.ActiveMonoViewportVm.MouseRay;
                mouseRay.UpdateMouseStartEnd();
            }

            if (mouseDirectlyHoveredObject != null && mouseDirectlyHoveredDrawNoDepth == null)
            {
                mouseDirectlyHoveredObject.GetObjectState().IsDirectlyMouseHovered = true;
            }
            if (mouseDirectlyHoveredDrawNoDepth != null)
            {
                mouseDirectlyHoveredDrawNoDepth.GetObjectState().IsDirectlyMouseHovered = true;
            }
            if (!linkKeyDown)
            {
                if (linkObjectOne != null)
                {
                    linkObjectOne.GetObjectState().IsLinkPicked = false;
                }
                if (linkObjectTwo != null)
                {
                    linkObjectTwo.GetObjectState().IsLinkPicked = false;
                }
                linkObjectOne = null;
                linkObjectTwo = null;
            }
        }

        private void UpdateMouseRayPicks(MonoEngineScene scene)
        {
        }

        private void UpdatePickMeshes(List<IDrawableSceneObject> objs)
        {
        }

        private void UpdateRayObjectIntersections(List<IDrawableSceneObject> sceneObjects)
        {
        }

        private void ResetMouseHoverObjects(List<IDrawableSceneObject> sceneObjects)
        {
        }

        public IntersectionResult FindGroundPoint(Vector3 position, MonoEngineScene scene, IDrawableSceneObject sceneObject, List<IDrawableSceneObject> possibleObjects)
        {
            groundIntersectionResult.Reset();

            var groundRay = new Ray(position, position-new Vector3(0.0f, 0.0f, 999999999));

            // Find all intersections with ground filter applied
            // Use intersection ray with shortest distance

            foreach (var obj in possibleObjects)
            {
                FindGroundPoint(obj, groundRay, groundIntersectionResult);
            }


            return groundIntersectionResult;
        }

        private void FindGroundPoint(IDrawableSceneObject sceneObject, Ray ray, IntersectionResult result)
        {
            if (groundCollisionFilter.ApprovedByFilter(sceneObject))
            {
                var currentResult = new IntersectionResult();

                sceneObject.GetObjectState().MouseRayIntersectsObject(ray, currentResult);
                if (currentResult.Intersects && (!result.Intersects || currentResult.Distance < result.Distance))
                {
                    result.SetResult(currentResult.Distance, currentResult.IntersectionCoordinate);
                }
            }

            foreach (var child in sceneObject.GetDrawableChildren())
            {
                FindGroundPoint(child, ray, result);
            }
        }

        public void DownAction(MonoEngineScene scene)
        {

        }

        public MouseButton GetLmb()
        {
            return lmb;
        }

        public void HoldAction(MonoEngineScene scene)
        {
            if (pickedTranslationHandle != null)
            {
                isDragging = true;
                var plane = thOriginalPlane;
                Ray ray = mouseRay.GetRay();
                var rayVec = ray.GetRayVector3();

                var thCurrentPlaneIntersectionPosPt = ray.RayPlaneIntersection(plane);
                if (thCurrentPlaneIntersectionPosPt.HasValue)
                {
                    thCurrentPlaneIntersectionPos = ColladaLibrary.Utility.RenderUtility.ToVector3(thCurrentPlaneIntersectionPosPt.Value);
                    thCurrentTargetPos = pickedTranslationHandle.GetObjectState().Transformation.WorldMatrix.Translation;

                    var planeIntersectionVec = new Vector3(
                        (float)thCurrentPlaneIntersectionPosPt.Value.X,
                        (float)thCurrentPlaneIntersectionPosPt.Value.Y,
                        (float)thCurrentPlaneIntersectionPosPt.Value.Z);

                    var pos = thCurrentPlaneIntersectionPos - (thOriginalPlaneIntersectionPos - thOriginalTargetPos);

                    if (pickedTranslationHandle.AxisDirection == AxisDirection.Positive_X)
                    {
                        pos = new Vector3(pos.X, thOriginalTargetPos.Y, thOriginalTargetPos.Z);
                    }
                    if (pickedTranslationHandle.AxisDirection == AxisDirection.Positive_Y)
                    {
                        pos = new Vector3(thOriginalTargetPos.X, pos.Y, thOriginalTargetPos.Z);
                    }
                    if (pickedTranslationHandle.AxisDirection == AxisDirection.Positive_Z)
                    {
                        pos = new Vector3(thOriginalTargetPos.X, thOriginalTargetPos.Y, pos.Z);
                    }
                    pickedTranslationHandle.Drag(pos, pickedObjectTransformationVm);
                    pickedTranslationHandle.GetObjectState().IsDragging = true;
                }
            }
            else
            {
                isDragging = false;
            }
        }

        private void ResetLinkObjects()
        {
        }

        public void ReleaseAction(MonoEngineScene scene)
        {
            if (pickedTranslationHandle != null)
            {
                pickedTranslationHandle.GetObjectState().IsDragging = false;
            }
            isDragging = false;
        }

        public void DoubleClickAction(MonoEngineScene scene)
        {
        }
    }
}
