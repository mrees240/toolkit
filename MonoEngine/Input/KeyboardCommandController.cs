﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MonoEngine.Code;

namespace MonoEngine.Input
{
    public enum KeyStateMono
    {
        Pressed, Released, Hold, Up
    }

    public class KeyboardCommandController
    {
        private Dictionary<KeyBinding, Action> keyHoldBindings;
        private Dictionary<KeyBinding, Action> keyReleasedBindings;
        private Dictionary<KeyBinding, Action> keyPressedBindings;
        private Dictionary<KeyBinding, Action> keyUpBindings;
        private List<KeyBinding> pressedKeys;
        private List<KeyBinding> releasedKeys;
        private List<KeyBinding> heldKeys;
        private List<KeyBinding> upKeys;
        private MonoEngineCore monoEngine;

        private Action sceneLogAction;

        public KeyboardCommandController(MonoEngineCore monoEngine)
        {
            this.monoEngine = monoEngine;
            pressedKeys = new List<KeyBinding>();
            releasedKeys = new List<KeyBinding>();
            heldKeys = new List<KeyBinding>();
            upKeys = new List<KeyBinding>();
            keyPressedBindings = new Dictionary<KeyBinding, Action>();
            keyHoldBindings = new Dictionary<KeyBinding, Action>();
            keyReleasedBindings = new Dictionary<KeyBinding, Action>();
            keyUpBindings = new Dictionary<KeyBinding, Action>();

#if DEBUG
            sceneLogAction = new Action(() =>
            {
                monoEngine.GameScene.SaveSceneLog();
                System.Windows.MessageBox.Show("Scene log was saved");
            });
            AddKeyCommand(new KeyBinding(Key.F1), sceneLogAction, KeyStateMono.Released);
#endif
        }

        public void AddKeyCommand(KeyBinding key, Action command, KeyStateMono keyState)
        {
            switch (keyState)
            {
                case KeyStateMono.Hold:
                    keyHoldBindings.Add(key, command);
                    break;
                case KeyStateMono.Pressed:
                    keyPressedBindings.Add(key, command);
                    break;
                case KeyStateMono.Released:
                    keyReleasedBindings.Add(key, command);
                    break;
                case KeyStateMono.Up:
                    keyUpBindings.Add(key, command);
                    break;
            }
        }

        public void Reset()
        {
            keyHoldBindings.Clear();
            keyReleasedBindings.Clear();
            keyPressedBindings.Clear();
            keyUpBindings.Clear();
        }

        private void CheckInput(Dictionary<KeyBinding, Action> keyBindings)
        {
            foreach (var keyBinding in keyBindings)
            {
                var key = keyBinding.Key;

                var keyDown = Keyboard.IsKeyDown(key.CurrentKey);
                var keyUp = Keyboard.IsKeyUp(key.CurrentKey);

                if (keyDown)
                {
                    if (!pressedKeys.Contains(key) && !heldKeys.Contains(key))
                    {
                        pressedKeys.Add(key);
                    }
                    else
                    {
                        if (pressedKeys.Contains(key))
                        {
                            pressedKeys.Remove(key);
                        }
                        if (!heldKeys.Contains(key))
                        {
                            heldKeys.Add(key);
                        }
                    }
                    if (upKeys.Contains(key))
                    {
                        upKeys.Remove(key);
                    }
                }
                else
                {
                    if (releasedKeys.Contains(key))
                    {
                        releasedKeys.Remove(key);
                    }
                    bool keyReleased = (pressedKeys.Contains(key) || heldKeys.Contains(key)) && !releasedKeys.Contains(key);
                    if (keyReleased)
                    {
                        releasedKeys.Add(key);
                        if (pressedKeys.Contains(key))
                        {
                            pressedKeys.Remove(key);
                        }
                        if (heldKeys.Contains(key))
                        {
                            heldKeys.Remove(key);
                        }
                    }
                    else
                    {
                        if (!upKeys.Contains(key))
                        {
                            upKeys.Add(key);
                        }
                    }
                }
            }
        }

        public void CheckKeyboardInput()
        {
            CheckInput(keyPressedBindings);
            CheckInput(keyReleasedBindings);
            CheckInput(keyHoldBindings);
            CheckInput(keyUpBindings);

            foreach (var keyBinding in keyReleasedBindings)
            {
                var key = keyBinding.Key;

                if (releasedKeys.Contains(key))
                {
                    keyBinding.Value.Invoke();
                }
            }
            foreach (var keyBinding in keyPressedBindings)
            {
                var key = keyBinding.Key;

                if (pressedKeys.Contains(key))
                {
                    keyBinding.Value.Invoke();
                }
            }
            foreach (var keyBinding in keyHoldBindings)
            {
                var key = keyBinding.Key;

                if (heldKeys.Contains(key))
                {
                    keyBinding.Value.Invoke();
                }
            }
            foreach (var keyBinding in keyUpBindings)
            {
                var key = keyBinding.Key;

                if (upKeys.Contains(key))
                {
                    keyBinding.Value.Invoke();
                }
            }
        }
    }
}
