﻿using ColladaLibrary.Collada;
using ColladaLibrary.Render;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    /// <summary>
    /// Stores info needed for transformation gizmos, including meshes.
    /// </summary>
    public class TransformationGizmoManager
    {
        //private Dictionary<IDrawableSceneObject, TransformationGizmo> transformationGizmos;
        private Mesh translationMeshHead;
        private Mesh xAxisLineMesh;
        private Mesh yAxisLineMesh;
        private Mesh zAxisLineMesh;
        private MonoEngineScene scene;

        public TransformationGizmoManager(MonoEngineScene scene)
        {
            float min = -9999999.0f;
            float max = 9999999.0f;
            this.scene = scene;
            var colladaImporter = new ColladaImporter(false, false, "Resources/Gizmos/GizmoTranslation.dae");
            colladaImporter.Load();
            translationMeshHead = colladaImporter.CreateSingleMesh();
            xAxisLineMesh = MeshUtility.CreateLineMesh(System.Drawing.Color.Red, new System.Numerics.Vector3(min, 0.0f, 0.0f), new System.Numerics.Vector3(max, 0.0f, 0.0f));
            yAxisLineMesh = MeshUtility.CreateLineMesh(System.Drawing.Color.Green, new System.Numerics.Vector3(0.0f, min, 0.0f), new System.Numerics.Vector3(0.0f, max, 0.0f));
            zAxisLineMesh = MeshUtility.CreateLineMesh(System.Drawing.Color.Blue, new System.Numerics.Vector3(0.0f, 0.0f, min), new System.Numerics.Vector3(0.0f, 0.0f, max));
            //transformationGizmos = new Dictionary<IDrawableSceneObject, TransformationGizmo>();
        }

        public Mesh TranslationMeshHead { get => translationMeshHead; }
        public Mesh XAxisLineMesh { get => xAxisLineMesh; }
        public Mesh YAxisLineMesh { get => yAxisLineMesh; }
        public Mesh ZAxisLineMesh { get => zAxisLineMesh; }

        /*public TranslationGizmo TranslationGizmo { get => translationGizmo; }

        public void SetTarget(IDrawableSceneObject sceneObject)
        {
            this.target = sceneObject;
        }*/
    }
}
