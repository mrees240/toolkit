﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonoEngine.Input
{
    public class MouseButton
    {
        private MouseButtonState mouseButtonState;
        private MouseButtons mouseButtonType;

        public MouseButton(MouseButtons mouseButtonType)
        {
            mouseButtonState = new MouseButtonState();
            this.mouseButtonType = mouseButtonType;
        }

        public MouseButtonState MouseButtonState { get => mouseButtonState; }
        public MouseButtons MouseButtonType { get => mouseButtonType; }
    }
}
