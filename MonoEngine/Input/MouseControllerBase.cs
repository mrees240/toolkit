﻿using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Scenes;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using WpfCore.Utility;
using WpfCore.ViewModel;

namespace MonoEngine.Input
{
    /// <summary>
    /// Handles all mouse input for monogame viewports.
    /// </summary>
    public abstract class MouseControllerBase
    {
        protected double dpiX;
        protected double dpiY;
        protected bool grabHandled;
        protected MouseState mouseState;

        protected MouseButtonController lmbController;
        protected MouseButtonController rmbController;
        protected MouseButtonController mmbController;

        protected MousePoint currentViewportMouseRelativePos;
        protected MousePoint finalViewportMousePosition;
        protected MousePoint currentMousePosition;
        protected MousePoint currentMousePositionDpiScaled;
        protected double cursorHeight;

        public MouseControllerBase()
        {
            this.mouseState = new MouseState();
            dpiX = 1;
            dpiY = 1;

            cursorHeight = System.Windows.SystemParameters.CursorHeight;

            currentViewportMouseRelativePos = new MousePoint();
            finalViewportMousePosition = new MousePoint();
            currentMousePosition = new MousePoint();
            currentMousePositionDpiScaled = new MousePoint();
        }

        public MouseControllerBase(IMouseButtonActions leftMouseButtonActions,
            IMouseButtonActions rightMouseButtonActions, IMouseButtonActions middleMouseButtonActions) : this()
        {

            lmbController = new MouseButtonController(mouseState, leftMouseButtonActions);
            rmbController = new MouseButtonController(mouseState, rightMouseButtonActions);
            mmbController = new MouseButtonController(mouseState, middleMouseButtonActions);
        }

        public void UpdateInput(int deltaMs,
            MonoEngineScene scene,
            List<IMonoViewportViewModelWrapper> viewportVmWrappers, Rectangle windowBoundary, double dpiX, double dpiY)
        {
            this.dpiX = dpiX;
            this.dpiY = dpiY;
            UpdateInputWrapper(deltaMs, scene, viewportVmWrappers, windowBoundary);
        }

        protected abstract void InnerUpdateInput(int delta,
            MonoEngineScene scene,
            List<IMonoViewportViewModelWrapper> viewportVmWrappers, Rectangle windowBoundary, Point mousePosition);

        /// <summary>
        /// The start of all mouse input code.
        /// </summary>
        protected void UpdateInputWrapper(int deltaMs,
            MonoEngineScene scene,
            List<IMonoViewportViewModelWrapper> viewportVmWrappers,
            Rectangle windowBoundary)
        {
            // Input is always 1 tick behind in order to detect if the mouse is about to leave the viewport.
            currentMousePosition = GetCurrentMousePosition();

            currentMousePositionDpiScaled.X = currentMousePosition.X / dpiX;
            currentMousePositionDpiScaled.Y = currentMousePosition.Y / dpiY;


            var windowCenterX = windowBoundary.X + (windowBoundary.Width / 2.0f);
            var windowCenterY = windowBoundary.Y + (windowBoundary.Height / 2.0f);

            if (mouseState.Grabbed)
            {
                if (!grabHandled)
                {
                    GrabCursor();
                    InputUtility.SetCursorPosition((int)windowCenterX, (int)windowCenterY);
                    mouseState.MouseMovementDelta.SetCoordinates(0, 0);
                }
                else
                {
                    mouseState.MouseMovementDelta.SetCoordinates((currentMousePosition.X - windowCenterX), (currentMousePosition.Y - windowCenterY));
                    InputUtility.SetCursorPosition((int)windowCenterX, (int)windowCenterY);
                }
            }
            else
            {
                if (grabHandled)
                {
                    ReleaseGrabbedCursor();
                }
            }

            if (!mouseState.Grabbed)
            {
                int viewportIndex = 0;
                bool viewportFound = false;

                var deltaX = (currentMousePosition.X - mouseState.MousePosition.X);
                var deltaY = (currentMousePosition.Y - mouseState.MousePosition.Y);
                mouseState.MouseMovementDelta.SetCoordinates(deltaX, deltaY);
                mouseState.MousePosition.SetCoordinates((currentMousePosition.X), (currentMousePosition.Y));
                mouseState.MousePositionDpiScaled.SetCoordinates((currentMousePosition.X / dpiX), (currentMousePosition.Y / dpiY));

                foreach (var monoViewportVmWrapper in viewportVmWrappers)
                {
                    var viewportVm = monoViewportVmWrapper.GetBaseViewportVm();

                    //var mousePos = new Point(mouseState.MousePosition.X - (int)viewportXTest, mouseState.MousePosition.Y);
                    /*var currentViewportMousePos = currentMousePosition;
                    currentViewportMouseRelativePos = viewportVm.GetViewportRelativeMousePosition(currentMousePosition);
                    currentViewportMouseRelativePos.SetCoordinates((currentViewportMouseRelativePos.X / dpiX),
                        (currentViewportMouseRelativePos.Y / dpiY));*/
                    //finalViewportMousePosition.SetCoordinates(viewport.X + currentViewportMouseRelativePos.X, viewport.Y + currentViewportMouseRelativePos.Y);

                    //viewportVm.UpdateCameraMouseRay(deltaMs, currentMousePositionDpiScaled, windowBoundary);

                    if (viewportVm.Visible && viewportVm.MouseRay.Viewport.Bounds.Contains((int)(currentMousePositionDpiScaled.X), (int)(currentMousePositionDpiScaled.Y)))
                    {
                        mouseState.ActiveViewport = viewportVm;
                        mouseState.ActiveViewportId = viewportIndex;
                        viewportFound = true;
                        break;
                    }
                    viewportIndex++;
                }

                if (!viewportFound)
                {
                    mouseState.ActiveViewport = null;
                    mouseState.ActiveViewportId = -1;
                }
                mouseState.ActiveViewportId = viewportIndex;
            }

            if (mouseState.ActiveViewport != null)
            {
                if (lmbController != null)
                {
                    if (lmbController.MouseButtonActions != null)
                    {
                        lmbController.UpdateInput(deltaMs, scene);
                    }
                }
                if (rmbController != null)
                {
                    if (rmbController.MouseButtonActions != null)
                    {
                        rmbController.UpdateInput(deltaMs, scene);
                    }
                }
                if (mmbController != null)
                {
                    if (mmbController.MouseButtonActions != null)
                    {
                        mmbController.UpdateInput(deltaMs, scene);
                    }
                }
            }
        }

        public MousePoint GetCurrentMousePosition()
        {
            /*var originalPosition = InputUtility.GetCursorPosition();
            var position = new Point((int)(originalPosition.X / dpiX), (int)(originalPosition.Y / dpiY));

            return position;*/

            var pos = InputUtility.GetCursorPosition();
            pos.SetCoordinates(pos.X, pos.Y);

            return pos;
        }

        protected void GrabCursor()
        {
            if (App.Current.MainWindow.IsMouseOver)
            {
                mouseState.PositionBeforeGrab.SetCoordinates(currentMousePosition.X, currentMousePosition.Y);
                grabHandled = true;
                InputUtility.HideCursor();
            }
        }

        protected void ReleaseGrabbedCursor()
        {
            InputUtility.SetCursorPosition((int)mouseState.PositionBeforeGrab.X, (int)mouseState.PositionBeforeGrab.Y);
            //mouseState.PositionBeforeGrab.SetCoordinates(0, 0);
            grabHandled = false;
            InputUtility.ShowCursor();
        }

        public MouseState MouseState
        {
            get
            {
                return mouseState;
            }
        }

        public MouseButtonController LmbController { get => lmbController; }
        public MouseButtonController RmbController { get => rmbController; }
        public MouseButtonController MmbController { get => mmbController; }
    }
}
