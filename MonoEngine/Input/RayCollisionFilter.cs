﻿using MonoEngine.Code.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Input
{
    public class RayCollisionFilter
    {
        private bool excludeVisibleInEditorOnly;
        private bool excludeIgnoreItemRayCastPlacement;

        public RayCollisionFilter()
        {

        }

        public bool ApprovedByFilter(IDrawableSceneObject sceneObject)
        {
            var state = sceneObject.GetObjectState();

            if (excludeVisibleInEditorOnly)
            {
                if (state.OnlyVisibleInEditor)
                {
                    return false;
                }
            }
            if (excludeIgnoreItemRayCastPlacement)
            {
                if (state.IgnoredByItemPlacementRayCast)
                {
                    return false;
                }
            }

            return true;
        }

        public bool ExcludeVisibleInEditorOnly { get => excludeVisibleInEditorOnly; set => excludeVisibleInEditorOnly = value; }
        public bool ExcludeIgnoreItemRayCastPlacement { get => excludeIgnoreItemRayCastPlacement; set => excludeIgnoreItemRayCastPlacement = value; }
    }
}
