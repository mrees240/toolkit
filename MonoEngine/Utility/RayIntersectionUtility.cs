﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using MathNet.Spatial.Euclidean;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Input;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using MonoGameCore.Render;

namespace MonoEngine.Utility
{
    public class RayIntersectionUtility
    {
        public static void CheckRayObjectIntersection(List<IDrawableSceneObject> sceneObjects, Ray ray, RayCollisionFilter filter,
            RayObjectIntersection intersection,
            bool checkChildren)
        {
            var intersections = new List<RayObjectIntersection>();

            foreach (var obj in sceneObjects)
            {
                CheckRayObjectIntersection(obj, ray, filter, intersections, checkChildren);
            }

            if (intersections.Any())
            {
                var nearest = intersections.OrderBy(r => r.Result.Distance).First();

                intersection.Result.SetResult(true, nearest.Result.Distance, nearest.Result.IntersectionCoordinate);
                intersection.SetResult(intersection.Result, nearest.SceneObject);
            }
        }

        public static void CheckRayObjectIntersections(List<IDrawableSceneObject> sceneObjects, Ray ray, RayCollisionFilter filter,
            List<RayObjectIntersection> intersections,
            bool checkChildren)
        {
            foreach (var obj in sceneObjects)
            {
                CheckRayObjectIntersection(obj, ray, filter, intersections, checkChildren);
            }

            /*if (intersections.Any())
            {
                var nearest = intersections.OrderBy(r => r.Result.Distance).First();

                intersection.Result.SetResult(true, nearest.Result.Distance, nearest.Result.IntersectionCoordinate);
                intersection.SetResult(intersection.Result, nearest.SceneObject);
            }*/
        }

        public static void CheckRayObjectIntersection(IDrawableSceneObject sceneObject, Ray ray, RayCollisionFilter filter, List<RayObjectIntersection> intersections,
            bool checkChildren)
        {
            var state = sceneObject.GetObjectState();

            if (filter.ApprovedByFilter(sceneObject) && sceneObject.GetDrawState().IsVisible)
            {
                if (ray != null)
                {
                    IntersectionResult broadPhaseResult = new IntersectionResult();

                    var mat = Matrix4x4.Identity;

                    ray.Intersects(sceneObject.GetObjectState().BoundingSphere, mat, broadPhaseResult);

                    if (broadPhaseResult.Intersects)
                    {
                        if (sceneObject.GetObjectState().IsSphere)
                        {
                            var newRecord = new RayObjectIntersection(sceneObject, broadPhaseResult);
                            intersections.Add(newRecord);
                        }
                        else
                        {
                            var meshes = sceneObject.GetMeshes();
                            IntersectionResult narrowPhaseResult = new IntersectionResult();

                            var matrix = sceneObject.GetObjectState().Transformation.WorldMatrix;

                            for (int iMesh = 0; iMesh < meshes.Count; iMesh++)
                            {
                                var pickableMesh = meshes[iMesh];
                                var polys = pickableMesh.GetAllPolygons();

                                ray.RayTriangleListIntersection(pickableMesh, matrix, narrowPhaseResult);

                                if (narrowPhaseResult.Intersects)
                                {
                                    var prevRecord = intersections.Where(i => i.SceneObject == sceneObject).FirstOrDefault();
                                    var newRecord = new RayObjectIntersection(sceneObject, narrowPhaseResult);

                                    if (!(sceneObject is TranslationHandle))
                                    {
                                        if (prevRecord == null)
                                        {
                                            intersections.Add(newRecord);
                                        }
                                        else
                                        {
                                            if (narrowPhaseResult.Distance < prevRecord.Result.Distance)
                                            {
                                                intersections.Remove(prevRecord);
                                                intersections.Add(newRecord);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var translationHandle = sceneObject as TranslationHandle;
                                        var translationGizmo = translationHandle.GetTranslationGizmo();

                                        if (translationGizmo.GetParentTransformationGizmo().GetDrawState().IsVisible)
                                        {
                                            if (prevRecord == null)
                                            {
                                                intersections.Add(newRecord);
                                            }
                                            else
                                            {
                                                if (narrowPhaseResult.Distance < prevRecord.Result.Distance)
                                                {
                                                    intersections.Remove(prevRecord);
                                                    intersections.Add(newRecord);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (checkChildren)
            {
                var children = sceneObject.GetDrawableChildren();

                foreach (var child in children)
                {
                    CheckRayObjectIntersection(child, ray, filter, intersections, checkChildren);
                }
            }
            /*if (!intersection.Result.Intersects)
            {
                var drawChildren = sceneObject.GetDrawableChildren();

                foreach (var drawChild in drawChildren)
                {
                    CheckRayObjectIntersection(drawChild, ray, filter, intersection);
                }
            }*/
        }
    }
}
