﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MonoEngine.Services
{
    public class WindowManagerService
    {
        public void OpenWindow<T>(T window, List<Window> windowList) where T : Window
        {
            var previousWindows = windowList.Where(w => w.GetType() == window.GetType());
            bool alreadyExists = previousWindows.Any();

            if (alreadyExists)
            {
                var prevWindow = previousWindows.First();

                if (!prevWindow.IsVisible)
                {
                    windowList.Remove(prevWindow);
                    windowList.Add(window);
                    window.Show();
                }
            }
            else
            {
                windowList.Add(window);
                window.Show();
            }
        }
    }
}
