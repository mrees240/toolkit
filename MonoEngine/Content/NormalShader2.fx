float4x4 xProjectionMatrix;
float4x4 xViewMatrix;
float4x4 xWorldMatrix;
float4 xLocalPosition;

Texture xDiffuseTexture;
Texture xNormalTexture;
//float3 xClippingPlaneDirections[6];
//float3 xClippingPlanePositions[6];
//float xActiveClippingPlanes[6]; // Enabled = 1, Disabled = 0
bool xTransparency;
float3 xTransparencyColor;

const int lightLimit = 6;
float4 xLightDirections[6]; // w = is directional, 0 = directional, 1 = not directional
float4 xLightColors[6]; // w = intensity
float4 xSolidColor;
bool xUseLocalLights;
bool xIsSolidColor;
bool xIsShadeless;
bool xIsBillboard;
bool xIsLine;
bool xTextureTransparencyEnabled;
bool xNeverMoves;
//float xIsDirectionalLightList[6]; // 1 = is directional
bool xHasDiffuseTexture;

float4 xLineStartPoint;
float4 xLineEndPoint;

/*float Shininess = 200;
float4 SpecularColor = float4(1, 1, 1, 1);
float SpecularIntensity = 1;
float3 ViewVector = float3(1, 0, 0);*/

sampler TextureSampler = sampler_state { texture = <xDiffuseTexture>; magfilter = POINT; minfilter = POINT; mipfilter = POINT; AddressU = WRAP; AddressV = WRAP; };
sampler NormalTextureSampler = sampler_state { texture = <xNormalTexture>; magfilter = POINT; minfilter = POINT; mipfilter = POINT; AddressU = WRAP; AddressV = WRAP; };

struct VertexToPixel
{
	float4 Position     : POSITION;
	float4 Normal     : NORMAL0;
	float2 TexCoords    : TEXCOORD0;
	float4 Position3D    : TEXCOORD1;
	float4 DiffuseColor      : COLOR0;
};


struct PixelToFrame
{
	float4 Color        : COLOR0;
};


VertexToPixel SimplestVertexShader(float4 inPos : POSITION0, float4 inNorm : NORMAL0,
	float2 inTexCoords : TEXCOORD0, float4 inColor: COLOR0 )
{
	VertexToPixel Output = (VertexToPixel)0;
	float4 finalPos = inPos;
	if (xNeverMoves) {
		xViewMatrix[3][0] = 0.0;
		xViewMatrix[3][1] = 0.0;
		xViewMatrix[3][2] = 0.0;
	}

	float4x4 worldViewMatrix = mul(xViewMatrix, xProjectionMatrix);
	
	

	float3 normWorld2 = inNorm;

	if (!xUseLocalLights)
	{
		float3x3 worldMatNoTranslation = xWorldMatrix;
		normWorld2 = mul(inNorm, worldMatNoTranslation);
	}

	if (xIsLine)
	{
		if (inPos.y < 1.0)
		{
			finalPos = xLineStartPoint;
		}
		else
		{
			finalPos = xLineEndPoint;
		}
		finalPos.w = 1.0;
	}
	Output.DiffuseColor = float4(1.0, 1.0, 1.0, 1.0);

	if (!xIsShadeless)
	{
		Output.DiffuseColor = float4(0.0, 0.0, 0.0, 1.0);
		for (int iLight = 0; iLight < lightLimit; iLight++)
		{
			if (xLightColors[iLight].w > 0.0)
			{
				float4 lightDir = float4(xLightDirections[iLight].x, xLightDirections[iLight].y, xLightDirections[iLight].z, 1.0);
				lightDir = normalize(lightDir);
				bool isDirectional = xLightDirections[iLight].w != 0.0;
				float4 lightColor = float4(xLightColors[iLight].x, xLightColors[iLight].y, xLightColors[iLight].z, 1.0);
				float lightMag = xLightColors[iLight].w;

				float mag = lightMag;

				if (isDirectional)
				{
					mag = max(dot(normWorld2, lightDir), 0.0) * lightMag;
				}
				Output.DiffuseColor = saturate(Output.DiffuseColor + lightColor * mag);
			}
		}
		Output.DiffuseColor = saturate(Output.DiffuseColor);
	}
	else
	{
		Output.DiffuseColor = inColor;
	}
	if (xIsSolidColor)
	{
		Output.DiffuseColor = saturate(Output.DiffuseColor * xSolidColor);
	}
	else
	{
		//Output.DiffuseColor = inColor;
	}

	float4 pos = mul(finalPos, worldViewMatrix);
	float4 pos3d = mul(finalPos, xWorldMatrix);
	

	Output.TexCoords = inTexCoords;
	/*if (!xIsBillboard) {
		Output.Position = pos;
	}
	else {
		Output.Position = pos;
	}*/
	Output.Position = pos;
	Output.Position3D = pos3d;

	return Output;
}

float4 PtDirectionPlaneToNormalForm(float3 pt, float3 n)
{
	float nx = n.x;
	float nx0 = nx * -pt.x;
	float ny = n.y;
	float ny0 = ny * -pt.y;
	float nz = n.z;
	float nz0 = nz * -pt.z;

	float d = -nx0 - ny0 - nz0;

	return float4(nx, ny, nz, d);
}

PixelToFrame OurFirstPixelShader(VertexToPixel PSIn)
{
	PixelToFrame Output = (PixelToFrame)0;

	float4 texColor = tex2D(TextureSampler, PSIn.TexCoords);
	
	if (!xTextureTransparencyEnabled)
	{
		texColor.w = 1.0;
	}

	if (xTransparency)
	{
		if (texColor.r == xTransparencyColor.r && texColor.g == xTransparencyColor.g && texColor.b == xTransparencyColor.b)
		{
			discard;
		}
	}
	if (!xIsSolidColor)
	{
		Output.Color = saturate(PSIn.DiffuseColor * texColor);
	}
	else
	{
		Output.Color = PSIn.DiffuseColor;
	}

	return Output;
}

technique Simplest
{
	pass Pass0
	{
		VertexShader = compile vs_4_0_level_9_3 SimplestVertexShader();
		PixelShader = compile ps_4_0_level_9_3 OurFirstPixelShader();
	}
}