﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render
{
    /// <summary>
    /// When a user attemps to link two objects that implement the ILinkableObject interface, it will first check to see if it's allowed
    /// and will return an instance of the link attempt result class.
    /// </summary>
    public class LinkAttemptResult
    {
        private bool success;
        private string message;
        private bool notifyUser;

        public LinkAttemptResult(bool success, string message, bool notifyUser)
        {
            this.success = success;
            this.message = message;
            this.notifyUser = notifyUser;
        }

        public bool Success { get => success; }
        public string Message { get => message; }
        public bool NotifyUser { get => notifyUser; }
    }
}
