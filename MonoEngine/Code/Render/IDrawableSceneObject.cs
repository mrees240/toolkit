﻿using ColladaLibrary.MathHelper;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WpfCore.ViewModel;

namespace MonoEngine.Code.Render
{
    public interface IDrawableSceneObject
    {
        List<ColladaLibrary.Render.Mesh> GetMeshes();
        void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene);
        void UpdateDrawState(MonoEngineScene scene);
        void PreDraw(MonoViewportViewModel monoViewportViewModel);
        void SetParent(IDrawableSceneObject parent);
        IDrawableSceneObject GetParent();
        List<IDrawableSceneObject> GetDrawableChildren();
        Dictionary<BillboardInfo, BillboardSceneObject> GetBillboards();
        DrawState GetDrawState();
        List<Light> GetInternalLights();
        List<ColladaLibrary.Render.Mesh> GetPreviousMeshes();
        void ClearPreviousMeshes();
        bool LinkWith(IDrawableSceneObject otherObject);
        bool HasCustomDrawMethod();
        SceneObjectState GetObjectState();
        IDrawableSceneObject GetRootObject();
        void CustomDraw(GraphicsDevice graphicsDevice, ShaderContainer shaders, Camera camera,
            Microsoft.Xna.Framework.GraphicsDeviceManager graphicsDeviceManager, MouseState mouseState);
        void UpdateBillboards(System.Numerics.Vector3 cameraPosition);


        Transformation GetTransformation();


    }
}
