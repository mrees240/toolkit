﻿using MonoEngine.Code.CommonFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render
{
    public interface ILinkableObject
    {
        bool MaxConnectionsReached();
        LinkAttemptResult CanLinkWith(ILinkableObject other);
        void LinkWith(ILinkableObject other, LinkObject drawableLink);
        void UnlinkFrom(ILinkableObject other);
    }
}
