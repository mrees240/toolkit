﻿using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render
{
    public class BillboardSceneObject : GenericSceneObject
    {
        private BillboardInfo billboardInfo;
        private GenericSceneObject billboardChild;

        public BillboardSceneObject(BillboardInfo billboardInfo)
        {
            this.billboardInfo = billboardInfo;
            billboardChild = new GenericSceneObject();

            var mesh = ColladaLibrary.Utility.RenderUtility.CreateBillboardMesh(billboardInfo.Texture);
            billboardChild.SetMeshes(new List<ColladaLibrary.Render.Mesh>() { mesh });

            var drawState = billboardChild.GetDrawState();
            drawState.IsBillboard = true;
            drawState.TwoSides = true;
            drawState.Shadeless = true;
            drawState.BillboardInfo = billboardInfo;

            GetObjectState().IsPickable = true;
            GetObjectState().Name = "Billboard";
            GetObjectState().IgnoredByItemPlacementRayCast = true;
            billboardChild.GetObjectState().Name = "Billboard Child";
            billboardChild.GetObjectState().IsPickable = true;
            billboardChild.GetObjectState().Transformation.IsIndependent = true;
            //billboardChild.GetObjectState().IgnoreBroadPhaseCollision = true;
            billboardChild.GetObjectState().IsBillboard = true;

            AddChild(billboardChild);
        }

        public GenericSceneObject GetBillboardChild()
        {
            return billboardChild;
        }

        public List<ColladaLibrary.Render.Mesh> GetAllBillboardMeshes()
        {
            return billboardChild.GetMeshes();
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, matrixStack, mouseState, scene);
        }

        public void Update(Vector3 cameraPosition)
        {
            var transformation = billboardChild.GetObjectState().Transformation;
            var scale = new Vector3(billboardInfo.Width, 1.0f, billboardInfo.Height);
            var pos = GetObjectState().Transformation.WorldMatrix.Translation + billboardInfo.GetPositionVector();
            var scaleMat = Matrix4x4.CreateScale(scale);
            var posMat = Matrix4x4.CreateTranslation(pos);
            var rotMat = MonoGameCore.Utility.RenderUtility.GetBillboardLookAtPointRotationMatrix(pos, billboardInfo, cameraPosition);
            transformation.SetLocalScale(scale);
            transformation.SetLocalPosition(pos);
            MonoGameCore.Utility.RenderUtility.BillboardLookAtPoint(
                billboardChild.GetObjectState().Transformation,
                objectState.Transformation.WorldMatrix.Translation, billboardInfo, cameraPosition);
        }
    }
}
