﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoEngine.Code.Scenes;
using MonoEngine.Code.Settings;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Utility;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using WpfCore.Utility;
using WpfCore.ViewModel;

namespace MonoEngine.Code.Render
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class BaseEngineGame : Game
    {
        protected List<IMonoViewportViewModelWrapper> monoViewportWrapperViewModels;
        protected GraphicsDeviceManager graphicsDeviceManager;
        protected ShaderContainer shaders;
        protected MonoEngineScene gameScene;
        protected Stopwatch deltaWatch;
        protected EngineSettings engineSettings;
        private MonoEngine.Input.MouseState mouseState;
        //private RenderTarget2D renderTarget;
        private RenderTargetBinding[] renderTargets;
        private MonoEngineCore engineCore;
        private Microsoft.Xna.Framework.Color backgroundColor;

        public BaseEngineGame(
            MonoEngineCore engineCore,
            List<IMonoViewportViewModelWrapper> monoViewportWrapperViewModels,
            MonoEngineScene gameScene,
            EngineSettings engineSettings,
            MonoEngine.Input.MouseState mouseState
            )
        {
            backgroundColor = new Microsoft.Xna.Framework.Color();
            this.engineCore = engineCore;
            renderTargets = new RenderTargetBinding[1];
            this.engineSettings = engineSettings;
            this.mouseState = mouseState;
            deltaWatch = new Stopwatch();
            this.gameScene = gameScene;
            this.monoViewportWrapperViewModels = monoViewportWrapperViewModels;
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            graphicsDeviceManager.GraphicsProfile = GraphicsProfile.Reach;
            Content.RootDirectory = "Content";

            graphicsDeviceManager.PreparingDeviceSettings += GraphicsDeviceManager_PreparingDeviceSettings;
            graphicsDeviceManager.DeviceCreated += GraphicsDeviceManager_DeviceCreated;
        }

        private void GraphicsDeviceManager_DeviceCreated(object sender, System.EventArgs e)
        {
            /*BlendState bs = new BlendState();
            BlendState oldbs = graphicsDeviceManager.GraphicsDevice.BlendState;

            bs.ColorBlendFunction = BlendFunction.Add;
            bs.AlphaBlendFunction = BlendFunction.Add

            bs.ColorSourceBlend = Blend.SourceAlpha;
            bs.ColorDestinationBlend = Blend.InverseSourceAlpha;

            bs.AlphaSourceBlend = Blend.Zero;
            bs.AlphaDestinationBlend = Blend.no*/

            graphicsDeviceManager.GraphicsDevice.BlendState = BlendState.NonPremultiplied;
            //graphicsDeviceManager.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            //graphicsDeviceManager.GraphicsDevice.PresentationParameters.DeviceWindowHandle = monoViewportWrapperViewModels[0].GetBaseViewportVm().WindowHandle;
        }

        private void GraphicsDevice_Disposing(object sender, System.EventArgs e)
        {
        }

        private void GraphicsDeviceManager_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            //e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = monoViewportWrapperViewModels[0].GetBaseViewportVm().WindowHandle;
            e.GraphicsDeviceInformation.PresentationParameters.IsFullScreen = false;
            //e.GraphicsDeviceInformation.PresentationParameters.BackBufferWidth = 400;
            //e.GraphicsDeviceInformation.PresentationParameters.BackBufferHeight = 400;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            InitializeDepthBuffer();

            base.Initialize();

            deltaWatch.Start();
        }

        protected void InitializeDepthBuffer()
        {
            var depthState = new DepthStencilState();
            depthState.DepthBufferEnable = true;
            depthState.DepthBufferWriteEnable = true;
            GraphicsDevice.DepthStencilState = depthState;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            LoadFont();
            LoadShaders();
        }

        private void LoadFont()
        {
            gameScene.SpriteBatch = new SpriteBatch(GraphicsDevice);
            gameScene.SpriteFont1 = Content.Load<SpriteFont>("Bin/windows/Font1");
            gameScene.HudDebugFont = Content.Load<SpriteFont>("Bin/windows/Font1");
            //gameScene.HudDebugFont = Content.Load<SpriteFont>("Bin/windows/DebugHudFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        private void LoadShaders()
        {
            shaders = new ShaderContainer();
            shaders.NormalShader = new Shader(Content, "Bin/windows/NormalShader", ShaderType.Normal);
            /*shaders.NormalShaderInstanced = new Shader(Content, "Bin/windows/NormalShaderInstanced", ShaderType.NormalInstanced);
            shaders.DiffuseOnlyShader = new Shader(Content, "Bin/windows/DiffuseOnlyShader", ShaderType.DiffuseColorOnly);
            shaders.NormalShaderBumpMap = new Shader(Content, "Bin/windows/NormalShaderBumpMap", ShaderType.NormalWithBumpMap);
            shaders.DepthShader = new Shader(Content, "Bin/windows/DepthShader", ShaderType.Depth);*/

            gameScene.Shaders = shaders;
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            long msSincePrevUpdate = deltaWatch.ElapsedMilliseconds;
            gameScene.UpdateScene(gameTime, this.mouseState, (int)msSincePrevUpdate);
            deltaWatch.Restart();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            gameScene.UpdateGraphicsDevice(graphicsDeviceManager.GraphicsDevice);

            for (int iViewport = 0; iViewport < monoViewportWrapperViewModels.Count; iViewport++)
            {
                var viewportVm = monoViewportWrapperViewModels[iViewport].GetBaseViewportVm();

                if (viewportVm.Visible && viewportVm.SwapChain != null)
                {
                    if (viewportVm.ViewportWidth > 1 && viewportVm.ViewportHeight > 1)
                    {
                        renderTargets[0] = viewportVm.SwapChain;
                        graphicsDeviceManager.GraphicsDevice.SetRenderTargets(renderTargets);
                        DrawSceneToTexture(gameTime, iViewport);
                        graphicsDeviceManager.GraphicsDevice.SetRenderTarget(null);
                        viewportVm.SwapChain.Present();
                    }
                }
            }
        }

        public void StopGame()
        {
            gameScene.StopGame();
        }

        /// <summary>
        /// Draws the entire scene in the given render target.
        /// </summary>
        private void DrawSceneToTexture(GameTime gameTime, int viewportId)
        {
            var viewportVm = monoViewportWrapperViewModels[viewportId].GetBaseViewportVm();

            backgroundColor.R = viewportVm.BackgroundColor.R;
            backgroundColor.G = viewportVm.BackgroundColor.G;
            backgroundColor.B = viewportVm.BackgroundColor.B;
            backgroundColor.A = byte.MaxValue;

            graphicsDeviceManager.GraphicsDevice.Clear(backgroundColor);

            gameScene.DrawScene(shaders, viewportVm, graphicsDeviceManager, mouseState);

            foreach (var mouseViewport in monoViewportWrapperViewModels)
            {
                gameScene.UpdateMouseRays(shaders, viewportVm, mouseViewport.GetBaseViewportVm(), graphicsDeviceManager, mouseState);

                if (engineSettings.DebugShowPickableMeshes)
                {
                    gameScene.DrawMouseRays(shaders, viewportVm, mouseViewport.GetBaseViewportVm(), graphicsDeviceManager, mouseState);
                }
            }
        }

        public ShaderContainer Shaders { get => shaders; }
        public bool GameRunning { get => gameScene.GameRunning; }
    }
}
