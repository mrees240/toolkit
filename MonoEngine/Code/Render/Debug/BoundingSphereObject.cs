﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoEngine.Code.Scenes;

namespace MonoEngine.Code.Render.Debug
{
    /// <summary>
    /// Used for rendering bounding spheres.
    /// </summary>
    public class BoundingSphereObject : GenericSceneObject
    {
        public BoundingSphereObject(ResourcesLoader resources)
        {
            AddMesh(resources.SphereMesh);
            objectState.IsBoundingObject = true;
            objectState.Transformation.IsIndependent = true;
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);
            drawState.IgnoresDepthBuffer = true;
            drawState.IsSolidColor = true;
            drawState.Shadeless = true;
            drawState.Color = System.Drawing.Color.FromArgb(32, System.Drawing.Color.White);
        }
    }
}
