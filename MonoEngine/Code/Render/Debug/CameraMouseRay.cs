﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Utility;
using WpfCore.ViewModel;

namespace MonoEngine.Code.Render.Debug
{
    public class CameraMouseRay : GenericSceneObject
    {
        private Mesh lineMesh;
        private MonoGameCore.Input.Ray ray;
        private Viewport viewport;
        private System.Numerics.Vector3 diff;

        public Viewport Viewport { get => viewport; set => viewport = value; }
        public MousePoint MousePoint { get => mousePoint; }
        private EngineSettings settings;


        private Camera camera;

        private MousePoint mousePoint;

        public CameraMouseRay(Camera camera, Viewport viewport, EngineSettings settings) : base()
        {
            this.settings = settings;
            this.viewport = viewport;
            mousePoint = new MousePoint();
            this.camera = camera;
            internalLights = new List<Light>();
            drawableChildren = new List<IDrawableSceneObject>();
            drawState = new DrawState();
            ray = new MonoGameCore.Input.Ray();
            diff = new System.Numerics.Vector3();
            objectState.Name = "Mouse Ray";

            drawState.PrimitiveType = PrimitiveType.LineList;
            drawState.Shadeless = true;
            drawState.Color = System.Drawing.Color.White;
            drawState.IgnoresDepthBuffer = false;
            drawState.IsSolidColor = true;
            drawState.IsLine = true;
            drawState.IsVisible = true;

            meshes.Add(MeshUtility.CreateLineMesh(System.Drawing.Color.White, new System.Numerics.Vector3(0.0f, 0.0f, 0.0f), new System.Numerics.Vector3(0.0f, 1.0f, 0.0f)));
        }

        public void UpdateMouseStartEnd()
        {
            ray.StartPoint = new System.Numerics.Vector3(drawState.LineStart.X, drawState.LineStart.Y, drawState.LineStart.Z);
            ray.EndPoint = new System.Numerics.Vector3(drawState.LineEnd.X, drawState.LineEnd.Y, drawState.LineEnd.Z);
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.NormalShader;

            if (settings.DebugUpdateMouseRays)
            {
                drawState.LineStart = camera.NearPoint.ToXnaVector3();
                drawState.LineEnd = camera.FarPoint.ToXnaVector3();
                var dir = Microsoft.Xna.Framework.Vector3.Normalize(drawState.LineEnd - drawState.LineStart);
                drawState.LineEnd = drawState.LineStart + dir * 99999999999.0f;

                UpdateMouseStartEnd();
            }
        }

        public MonoGameCore.Input.Ray GetRay()
        {
            return ray;
        }


        public void SetLocalMatrix(Matrix4x4 matrix)
        {
        }

        public Microsoft.Xna.Framework.Vector3 Project(System.Numerics.Vector3 position)
        {
            return viewport.Project(position.ToXnaVector3(), MonoGameCore.Render.RenderUtility.ToXnaMatrix(camera.ProjectionMatrix),
                MonoGameCore.Render.RenderUtility.ToXnaMatrix(camera.ViewMatrix), Microsoft.Xna.Framework.Matrix.Identity);
        }
    }
}
