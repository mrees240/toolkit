﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;

namespace MonoEngine.Code.Render.Debug
{
    /// <summary>
    /// A scene object that changes color if the mouse is hovering over it.
    /// Used for testing purposes.
    /// </summary>
    public class PickTestSceneObject : GenericSceneObject
    {
        private float sinX;

        public PickTestSceneObject()
        {
            objectState.Name = "Pick Test Object";
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, matrixStack, mouseState, scene);

            sinX += 0.001f * deltaMs;

            float z = (float)Math.Sin(sinX) * 1000.0f;

            objectState.Transformation.SetLocalPosition(new Vector3(0.0f, 0.0f, z));
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);
            drawState.IsSolidColor = true;
            drawState.Shadeless = true;
            objectState.IsPickable = true;

            if (objectState.IsDirectlyMouseHovered)
            {
                drawState.Color = System.Drawing.Color.White;
            }
            else if (objectState.IsMouseHovered)
            {
                drawState.Color = System.Drawing.Color.Gray;
            }
            else
            {
                drawState.Color = System.Drawing.Color.Black;
            }
        }
    }
}
