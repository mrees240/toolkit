﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using MathNet.Spatial.Euclidean;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Collision;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WpfCore.ViewModel;

namespace MonoEngine.Code.Render.Input.Gizmos
{
    public class TranslationGizmo : GenericSceneObject
    {
        private bool isMouseHovered;
        private string meshUrl;
        //private List<MeshBuffer> meshBuffers;
        private bool meshLoaded;
        private System.Numerics.Vector3 currentAxisGrabOffset;
        private System.Numerics.Vector3 startAxisGrabPlanePosition;
        private System.Numerics.Vector3 currentAxisGrabPlanePosition;
        private System.Numerics.Vector3 startWorldPosition;

        private System.Numerics.Vector3 originalPickCoordinate;
        private System.Numerics.Vector3 pickOffset;

        private Camera camera;
        private float gizmoRelativeToScreenScale;

        private bool hasTranslated;

        public static int tempIdCount;

        private TranslationHandle xAxis;
        private TranslationHandle yAxis;
        private TranslationHandle zAxis;

        private TranslationHandle pickedAxis;

        private TranslationHandle[] gizmoAxes;

        private DrawState drawState;
        private bool isPicked;
        private IDrawableSceneObject target;

        private bool dragInitialized;
        private bool draggingAxis;
        private bool offsetInitialized;

        private Mesh gizmoHeadMesh;

        private MouseButton mouseButton;


        private TransformationGizmoManager gizmoManager;

        private MonoEngineScene scene;

        public TranslationGizmo(IDrawableSceneObject parent, IDrawableSceneObject target, TransformationGizmoManager gizmoManager, MonoEngineScene scene) : base()
        {
            this.scene = scene;
            this.gizmoManager = gizmoManager;
            this.parent = parent;
            this.target = target;
            originalPickCoordinate = new System.Numerics.Vector3();
            pickOffset = new System.Numerics.Vector3();
            drawState = new DrawState();
            startWorldPosition = new System.Numerics.Vector3();
            startAxisGrabPlanePosition = new System.Numerics.Vector3();
            currentAxisGrabPlanePosition = new System.Numerics.Vector3();
            currentAxisGrabOffset = new System.Numerics.Vector3();
            gizmoRelativeToScreenScale = 0.003f;
            gizmoAxes = new TranslationHandle[0];
            objectState.Name = "Translation Gizmo";
            objectState.TranslationOnly = true;
            objectState.OnlyVisibleInEditor = true;
            objectState.IsPickable = true;

            LoadMesh(gizmoManager.TranslationMeshHead);
        }

        public void LoadMesh(Mesh translationGizmoMeshHead)
        {
            this.gizmoHeadMesh = new Mesh(translationGizmoMeshHead);

            xAxis = new TranslationHandle(this, AxisDirection.Positive_X, gizmoManager, scene);
            yAxis = new TranslationHandle(this, AxisDirection.Positive_Y, gizmoManager, scene);
            zAxis = new TranslationHandle(this, AxisDirection.Positive_Z, gizmoManager, scene);
            gizmoAxes = new TranslationHandle[]
            {
                xAxis, yAxis, zAxis
            };
            drawableChildren.AddRange(new List<IDrawableSceneObject>()
            {
                xAxis, yAxis, zAxis
            });

            meshLoaded = true;
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);

            var shaders = scene.Shaders;
            drawState.ActiveShader = shaders.NormalShader;
            drawState.IsSolidColor = true;

            drawState.IsVisible = true;
        }

        public TransformationGizmo GetParentTransformationGizmo()
        {
            return (TransformationGizmo)parent;
        }

        public TranslationHandle[] GetGizmoAxes()
        {
            return gizmoAxes;
        }

        public string MeshUrl { get => meshUrl; }
        public bool MeshLoaded { get => meshLoaded; }

        public bool HasTranslated { get => hasTranslated; set => hasTranslated = value; }
        public float GizmoRelativeToScreenScale { get => gizmoRelativeToScreenScale; set => gizmoRelativeToScreenScale = value; }
        public System.Numerics.Vector3 CurrentAxisGrabOffset { get => currentAxisGrabOffset; set => currentAxisGrabOffset = value; }
        public System.Numerics.Vector3 StartAxisGrabPlanePosition { get => startAxisGrabPlanePosition; set => startAxisGrabPlanePosition = value; }
        public System.Numerics.Vector3 CurrentAxisGrabPlanePosition { get => currentAxisGrabPlanePosition; set => currentAxisGrabPlanePosition = value; }
        public System.Numerics.Vector3 StartWorldPosition { get => startWorldPosition; set => startWorldPosition = value; }
        public bool DraggingAxis { get => draggingAxis; }
        public System.Numerics.Vector3 OriginalPickCoordinate { get => originalPickCoordinate; set => originalPickCoordinate = value; }
        public System.Numerics.Vector3 PickOffset { get => pickOffset; set => pickOffset = value; }
        public IDrawableSceneObject Target { get => target; }
    }
}
