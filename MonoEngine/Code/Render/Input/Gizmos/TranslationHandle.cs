﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using MathNet.Spatial.Euclidean;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render.Input.Gizmos
{
    public class TranslationHandle : GenericSceneObject
    {
        private Mesh gizmoAxisMesh;
        private float axisHeadOffset;
        private AxisDirection axisDirection;

        public AxisDirection AxisDirection { get => axisDirection; set => axisDirection = value; }
        public float AxisHeadOffset { get => axisHeadOffset; }
        private TransformationGizmoManager gizmoManager;

        private LineObject lineObject;

        private MonoEngineScene scene;

        public TranslationHandle(TranslationGizmo parent, AxisDirection axisDirection, TransformationGizmoManager gizmoManager, MonoEngineScene scene)
        {
            this.scene = scene;
            this.gizmoManager = gizmoManager;
            this.axisDirection = axisDirection;
            drawState = new DrawState();
            this.parent = parent;
            this.gizmoAxisMesh = new Mesh(gizmoManager.TranslationMeshHead);
            meshes.Add(gizmoAxisMesh);
            float tempScale = 10.0f;
            this.gizmoAxisMesh.Transform(Matrix4x4.CreateScale(tempScale));
            axisHeadOffset = 2000.0f;
            objectState.OnlyVisibleInEditor = true;
            objectState.IsPickable = true;

            SetTexture();
            OrientAxisMesh();


            objectState.Name = $"Translation Handle {this.axisDirection}";

            SetupLineSceneObject();
        }

        public IDrawableSceneObject GetTarget()
        {
            return ((TranslationGizmo)parent).Target;
        }

        private void SetupLineSceneObject()
        {
            switch (axisDirection)
            {
                case AxisDirection.Positive_X:
                    lineObject = new LineObject(axisDirection, gizmoManager.XAxisLineMesh, this, scene);
                    break;
                case AxisDirection.Positive_Y:
                    lineObject = new LineObject(axisDirection, gizmoManager.YAxisLineMesh, this, scene);
                    break;
                case AxisDirection.Positive_Z:
                    lineObject = new LineObject(axisDirection, gizmoManager.ZAxisLineMesh, this, scene);
                    break;
            }

            drawableChildren.Add(lineObject);
        }

        public TranslationGizmo GetTranslationGizmo()
        {
            return (TranslationGizmo)parent;
        }

        public MathNet.Spatial.Euclidean.Plane GetPlane(Camera camera)
        {
            var worldTranslation = GetTransformation().WorldMatrix.Translation;
            return GetPlane(camera, worldTranslation);
        }

        public MathNet.Spatial.Euclidean.Plane GetPlane(Camera camera, System.Numerics.Vector3 position)
        {
            var unitVector = UnitVector3D.Create(1.0, 0.0, 0.0);
            var pos = new Point3D(position.X, position.Y, position.Z);

            switch (AxisDirection)
            {
                case AxisDirection.Positive_X:
                    unitVector = UnitVector3D.Create(0.0, 1.0f, 0.0);
                    break;
                case AxisDirection.Positive_Y:
                    unitVector = UnitVector3D.Create(1.0, 0.0f, 0.0);
                    break;
                case AxisDirection.Positive_Z:
                    unitVector = UnitVector3D.Create(camera.CameraDirection.X, camera.CameraDirection.Y, 0.0);
                    break;
            }


            MathNet.Spatial.Euclidean.Plane plane = new MathNet.Spatial.Euclidean.Plane(pos, unitVector);
            return plane;
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, Matrix4x4.CreateTranslation(matrixStack.Translation), mouseState, scene);

            if (objectState.IsDragging)
            {
                ShowAxisLine();
            }
            else
            {
                HideAxisLine();
            }
        }

        public void Drag(System.Numerics.Vector3 newPosition, TransformationViewModel transformationVm)
        {
            var parentTransformationGizmo = ((TranslationGizmo)parent).GetParentTransformationGizmo();
            parentTransformationGizmo.Translate(newPosition, transformationVm);
        }

        private void OrientAxisMesh()
        {
            gizmoAxisMesh.Transform(Matrix4x4.CreateScale(0.05f));

            var rotMatYNeg90 = Matrix4x4.CreateRotationY((float)(-Math.PI / 2.0));
            var rotMatYNeg180 = Matrix4x4.CreateRotationY((float)(-Math.PI));
            var rotMatZRot90 = Matrix4x4.CreateRotationZ((float)(Math.PI / 2.0));

            switch (axisDirection)
            {
                case AxisDirection.Positive_X:
                    gizmoAxisMesh.Transform(rotMatYNeg90);
                    gizmoAxisMesh.Transform(Matrix4x4.CreateTranslation(new System.Numerics.Vector3(axisHeadOffset, 0.0f, 0.0f)));
                    break;
                case AxisDirection.Positive_Y:
                    gizmoAxisMesh.Transform(rotMatYNeg90);
                    gizmoAxisMesh.Transform(rotMatZRot90);
                    gizmoAxisMesh.Transform(Matrix4x4.CreateTranslation(new System.Numerics.Vector3(0.0f, axisHeadOffset, 0.0f)));
                    break;
                case AxisDirection.Positive_Z:
                    gizmoAxisMesh.Transform(rotMatYNeg180);
                    gizmoAxisMesh.Transform(Matrix4x4.CreateTranslation(new System.Numerics.Vector3(0.0f, 0.0f, axisHeadOffset)));
                    break;
            }
            //objectState.AddPickableMesh(gizmoAxisMesh);
            SetMeshes(gizmoAxisMesh);
        }

        private void SetTexture()
        {
            switch (axisDirection)
            {
                case AxisDirection.Positive_X:
                    drawState.Color = System.Drawing.Color.Red;
                    break;
                case AxisDirection.Positive_Y:
                    drawState.Color = System.Drawing.Color.Green;
                    break;
                case AxisDirection.Positive_Z:
                    drawState.Color = System.Drawing.Color.Blue;
                    break;
            }
            internalLights.Add(new Light(LightType.Directional, drawState.Color, 0.9f, 0.0f, 135.0f, 135.0f));
            internalLights.Add(new Light(LightType.Ambient, System.Drawing.Color.DarkGray, 0.3f, 0.0f, 0.0f, 0.0f));
        }

        public bool IsVisible()
        {
            return true;
        }

        public override void UpdateDrawState(MonoEngineScene scene)
        {
            base.UpdateDrawState(scene);
            drawState.ActiveShader = scene.Shaders.NormalShader;

            drawState.IgnoresDepthBuffer = true;
            drawState.IsVisible = true;
            drawState.UseInternalLightList = true;
            drawState.UseSizeRelativeToScreen = true;
            drawState.ScreenRelativeSize = 0.0005f;
            drawState.IsSolidColor = true;
        }

        public void ShowAxisLine()
        {
            lineObject.GetDrawState().IsVisible = true;
        }

        public void HideAxisLine()
        {
            lineObject.GetDrawState().IsVisible = false;
        }
    }
}
