﻿using ColladaLibrary.Render;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoGameCore.Collision;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render.Input
{
    /*public class MonoGameAnchorPoint : IGizmoTranslatable, IPickable3DObject
    {
        private static Mesh mesh;
        private MeshBuffer meshBuffer;
        private MeshBufferController meshBufferController;
        private AnchorPoint anchorPoint;
        private string meshUrl;
        private bool selected;
        private TranslationGizmo translationGizmo;

        public bool Selected { get => selected; set => selected = value; }
        public TranslationGizmo TranslationGizmo { get => translationGizmo; }

        public MonoGameAnchorPoint(AnchorPoint anchorPoint, string meshUrl)
        {
            this.anchorPoint = anchorPoint;
            this.meshUrl = meshUrl;
            translationGizmo = new TranslationGizmo("Resources/Gizmos/GizmoTranslation.dae", this);
        }

        public List<BoundingSphere> GetBoundingSpheres()
        {
            throw new NotImplementedException();
        }

        public List<Triangle> GetPickableTriangles()
        {
            return mesh.GetBasicTriangles(Matrix4x4.CreateTranslation(anchorPoint.Point));
        }

        public System.Numerics.Vector3 GetPosition()
        {
            return anchorPoint.Point;
        }

        public void SetPosition(System.Numerics.Vector3 position)
        {
            anchorPoint.Point = position;
        }

        public void Translate(System.Numerics.Vector3 translation)
        {
            anchorPoint.Point += translation;
        }

        System.Numerics.Vector3 IGizmoTranslatable.GetPosition()
        {
            return anchorPoint.Point;
        }

        public void Draw(GraphicsDevice graphicsDevice, Camera camera, ShaderContainer shaders)
        {
            var meshBufferController = new MeshBufferController(graphicsDevice);
            if (mesh == null)
            {
                var importer = new ColladaLibrary.Collada.Collada.ColladaImporter(false, false, meshUrl);
                importer.Load();
                mesh = importer.CreateSingleMesh(false);
            }
            if (meshBuffer == null)
            {
                meshBuffer = meshBufferController.CreateBufferFromMesh(mesh, shaders.DiffuseOnlyShader);
            }
            if (!selected)
            {
                meshBufferController.Draw(meshBuffer, camera, Matrix4x4.CreateTranslation(anchorPoint.Point), shaders.DiffuseOnlyShader);
            }
            else
            {
                meshBufferController.Draw(meshBuffer, camera, Matrix4x4.CreateTranslation(anchorPoint.Point), shaders.HighlightSelectionShader);
            }
        }

        /*public void DrawGizmos(GraphicsDevice graphicsDevice, Camera camera, ShaderContainer shaders)
        {
            if (selected)
            {
                translationGizmo.Draw(camera, graphicsDevice, shaders);
            }
        }*/

        /*public void Update(Ray mouseRay, InputMouseButtonState inputState)
        {
            translationGizmo.Update(mouseRay, inputState);
        }*/

        /*public IntersectionResult TestIsPicked(MonoGameCore.Input.Ray mouseRay)
        {
            var tris = mesh.GetBasicTriangles(Matrix4x4.CreateTranslation(anchorPoint.Point));
            bool picked = false;
            float closestDistance = -1.0f;

            foreach (var tri in tris)
            {
                var result = mouseRay.RayTriangleIntersection(tri);
                
                if (result.Intersects)
                {
                    picked = true;
                    if (closestDistance < 0 || result.Distance < closestDistance)
                    {
                        closestDistance = result.Distance;
                    }
                }
            }
            return new IntersectionResult(picked, closestDistance);
        }

        public void ActivatePicked()
        {
        }

        public void ActivateUnpicked()
        {
        }
    }*/
}
