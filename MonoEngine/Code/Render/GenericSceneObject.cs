﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ColladaLibrary.MathHelper;
using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Render.Debug;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using MonoEngine.ViewModel;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;

namespace MonoEngine.Code.Render
{
    public class GenericSceneObject : IDrawableSceneObject
    {
        protected List<Mesh> previousMeshes;
        protected List<Mesh> meshes;
        protected DrawState drawState;
        //protected Matrix4x4 localMatrix;
        //protected Matrix4x4 worldMatrix;
        protected List<IDrawableSceneObject> drawableChildren;
        protected List<Light> internalLights;
        protected IDrawableSceneObject parent;
        protected SceneObjectState objectState;
        protected Dictionary<BillboardInfo, BillboardSceneObject> billboards;
        protected object owner;
        protected bool hasCustomDraw;
        protected Camera currentCamera;

        protected List<IDrawableSceneObject> sceneObjectsToRemove;


        public event EventHandler UpdateOccured;

        public event EventHandler LinkChanged;
        public IDrawableSceneObject linkObject;
        public IDrawableSceneObject LinkObject { get => linkObject; }
        public object Owner { get => owner; set => owner = value; }

        public virtual void OnLinkChange()
        {
            if (LinkChanged != null)
            {
                LinkChanged(this, EventArgs.Empty);
            }
        }

        public virtual void OnUpdate()
        {
            if (UpdateOccured != null)
            {
                UpdateOccured(this, EventArgs.Empty);
            }
        }

        private void Initialize()
        {
            sceneObjectsToRemove = new List<IDrawableSceneObject>();
            billboards = new Dictionary<BillboardInfo, BillboardSceneObject>();
            objectState = new SceneObjectState(this);
            this.drawState = new DrawState();
            previousMeshes = new List<Mesh>();
            meshes = new List<Mesh>();
            drawableChildren = new List<IDrawableSceneObject>();
            internalLights = new List<Light>();
            objectState.Transformation.TransformationChanged += Transformation_TransformationChanged;
        }

        public GenericSceneObject() : this(null)
        {
            Initialize();
        }

        public void RemoveChild(IDrawableSceneObject childToRemove)
        {
            drawableChildren.Remove(childToRemove);
            //sceneObjectsToRemove.Add(childToRemove);
        }

        public GenericSceneObject(object owner)
        {
            this.owner = owner;
            Initialize();
        }

        protected void Transformation_TransformationChanged(object sender, EventArgs e)
        {
            //objectState.UpdatePickableMeshes();
            //RequestPickMeshUpdate();
            //UpdatePickableMeshes();
            //UpdatePickableMeshes();
            objectState.RequestCollisionUpdate();
        }

        public void InitializeTransformationGizmo(MonoEngineScene scene)
        {
            objectState.InitializeTransformationGizmo(this, scene.TransformationGizmoManager, scene);
            if (objectState.IsGizmoTranslatable)
            {
                drawableChildren.Add(objectState.TransformationGizmo);
            }
        }

        public List<IDrawableSceneObject> GetDrawableChildren()
        {
            return drawableChildren;
        }

        public PrimitiveType GetPrimitiveType()
        {
            return PrimitiveType.TriangleList;
        }

        public void AddChild(IDrawableSceneObject sceneObjectChild)
        {
            drawableChildren.Add(sceneObjectChild);
            sceneObjectChild.SetParent(this);
        }

        public void AddBillboards(List<BillboardInfo> billboards)
        {
            foreach (var billboard in billboards)
            {
                AddBillboard(billboard);
            }
        }

        public void RemoveBillboards()
        {
            while (billboards.Any())
            {
                var current = billboards.First();
                RemoveBillboard(current.Key);
            }
        }

        public void AddBillboard(BillboardInfo billboardInfo)
        {
            var billboardSceneObject = new BillboardSceneObject(billboardInfo);
            billboardSceneObject.owner = owner;
            billboards.Add(billboardInfo, billboardSceneObject);
            AddChild(billboardSceneObject);
        }

        public void AddBillboard(List<BillboardInfo> billboardInfoList)
        {
            foreach (var billboard in billboardInfoList)
            {
                AddBillboard(billboard);
            }
        }

        public void RemoveBillboard(BillboardInfo billboardInfo)
        {
            var sceneObject = billboards[billboardInfo];
            drawableChildren.Remove(sceneObject);
            sceneObjectsToRemove.Add(sceneObject);
            billboards.Remove(billboardInfo);
        }

        public void ClearPreviousMeshes()
        {
            previousMeshes.Clear();
        }

        public virtual void CustomDraw(GraphicsDevice graphicsDevice, ShaderContainer shaders, Camera camera,
            Microsoft.Xna.Framework.GraphicsDeviceManager graphicsDeviceManager, MouseState mouseState)
        {
        }

        public DrawState GetDrawState()
        {
            return drawState;
        }

        public List<Light> GetInternalLights()
        {
            return internalLights;
        }

        public List<Mesh> GetMeshes()
        {
            return meshes;
        }

        public void SetMeshes(Mesh mesh)
        {
            previousMeshes.AddRange(meshes);
            meshes.Clear();
            AddMesh(mesh);
        }

        public void AddMesh(Mesh mesh)
        {
            meshes.Add(mesh);
            drawState.MeshBufferNeedsUpdated = true;
            objectState.RequestBoundingBoxUpdate();
            objectState.RequestCollisionUpdate();
        }

        public void SetMeshes(List<Mesh> meshes)
        {
            previousMeshes.AddRange(this.meshes);
            this.meshes.Clear();

            foreach (var mesh in meshes)
            {
                AddMesh(mesh);
            };
        }

        public List<Mesh> GetPreviousMeshes()
        {
            return previousMeshes;
        }

        /*public Matrix4x4 GetLocalMatrix()
        {
            return localMatrix;
        }*/

        public bool HasCustomDrawMethod()
        {
            return hasCustomDraw;
        }

        public virtual void PreDraw(MonoViewportViewModel monoViewportViewModel)
        {
        }

        public virtual void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            if (objectState.IsGizmoTranslatable && objectState.TransformationGizmo == null)
            {
                objectState.InitializeTransformationGizmo(this, scene.TransformationGizmoManager, scene);
                AddChild(objectState.TransformationGizmo);
            }

            var mat = Matrix4x4.Multiply(matrixStack, objectState.Transformation.GetLocalMatrix());
            objectState.Transformation.SetWorldMatrix(mat);

            objectState.UpdateBoundingSphere();

            scene.RemoveObjects(sceneObjectsToRemove);
            var drawState = GetDrawState();

            OnUpdate();
        }
        
        public virtual void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.NormalShader;
        }

        public IDrawableSceneObject GetParent()
        {
            return parent;
        }

        public void SetParent(IDrawableSceneObject parent)
        {
            this.parent = parent;
        }

        public void SetTransformation(Transformation transformation)
        {
            this.objectState.Transformation = transformation;
        }

        public Transformation GetTransformation()
        {
            return objectState.Transformation;
        }

        public SceneObjectState GetObjectState()
        {
            return objectState;
        }

        public Dictionary<BillboardInfo, BillboardSceneObject> GetBillboards()
        {
            return billboards;
        }

        public IDrawableSceneObject GetRootObject()
        {
            if (parent == null)
            {
                return this;
            }
            return parent.GetParent();
        }

        public virtual bool LinkWith(IDrawableSceneObject otherObject)
        {
            if (this != otherObject)
            {
                this.linkObject = otherObject;
                OnLinkChange();
                return true;
            }
            return false;
        }

        public void UpdateBillboards(Vector3 cameraPosition)
        {
            foreach (var billboard in billboards)
            {
                billboard.Value.Update(cameraPosition);
            }
        }
    }
}
