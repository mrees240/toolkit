﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Collision;
using MonoGameCore.Input;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.Render
{
    public class SceneObjectState
    {
        private bool boundingBoxNeedsUpdated;
        private bool collisionNeedsUpdated;
        private bool isMouseHovered;
        private bool isDirectlyMouseHovered;
        private bool isPickable;
        private bool isPickableMesh;
        private bool isGizmoTranslatable;
        private bool isDragging;
        private bool isSelected;
        private bool isSphere;
        private string name;
        private TransformationGizmo transformationGizmo;
        private Transformation transformation;
        private bool ignoreBroadPhaseCollision;
        private bool onlyVisibleInEditor;
        private bool ignoredByItemPlacementRayCast;
        private bool translationOnly;
        private bool isLinkPicked;
        private bool isBoundingObject;
        private bool isBillboard;
        private IntersectionResult mouseIntersectionResult;
        private BoundingSphere boundingSphere;
        private IntersectionResult mouseIntersectionResultBroadPhase;
        private IntersectionResult mouseIntersectionResultNarrowPhase;
        private IDrawableSceneObject owner;
        private BoundingBoxMinMax boundingBox;
        private bool boundingBoxInitialized;
        private IDrawableSceneObject pickableRoot;

        public event EventHandler ToggleActivated;
        public event EventHandler PickActivated;

        public SceneObjectState(IDrawableSceneObject owner)
        {
            this.transformation = new Transformation();
            name = "Default Name";
            mouseIntersectionResult = new IntersectionResult();
            boundingSphere = new BoundingSphere();
            boundingBox = new BoundingBoxMinMax();
            mouseIntersectionResultBroadPhase = new IntersectionResult();
            mouseIntersectionResultNarrowPhase = new IntersectionResult();
            transformation.TransformationChanged += Transformation_TransformationChanged;
            this.owner = owner;
        }

        private void Transformation_TransformationChanged(object sender, EventArgs e)
        {
            RequestBoundingBoxUpdate();
        }

        public virtual void OnPickActivated()
        {
            if (PickActivated != null)
            {
                PickActivated(this, EventArgs.Empty);
            }
        }

        public virtual void OnToggleActivated()
        {
            if (ToggleActivated != null)
            {
                ToggleActivated(this, EventArgs.Empty);
            }
        }

        public void InitializeTransformationGizmo(IDrawableSceneObject target, TransformationGizmoManager gizmoManager, MonoEngineScene scene)
        {
            if (transformationGizmo == null)
            {
                this.transformationGizmo = new TransformationGizmo(target, gizmoManager, scene);
            }
        }

        public bool MouseIntersectsMeshBroadPhase(Ray ray)
        {
            mouseIntersectionResultBroadPhase.Reset();

            ray.Intersects(boundingSphere, Matrix4x4.Identity, mouseIntersectionResultBroadPhase);

            return mouseIntersectionResultBroadPhase.Intersects;
        }

        public IntersectionResult MouseRayIntersectsMeshNarrowPhase(Ray ray, Mesh mesh, Matrix4x4 matrix)
        {
            var result = new IntersectionResult();

            ray.RayTriangleListIntersection(mesh, matrix, result);

            return result;
        }

        public void MouseRayIntersectsObject(Ray ray, IntersectionResult result)
        {
            result.Reset();
            mouseIntersectionResultBroadPhase.Reset();
            mouseIntersectionResultNarrowPhase.Reset();
            mouseIntersectionResult.Reset();

            if (ignoreBroadPhaseCollision || MouseIntersectsMeshBroadPhase(ray))
            {
                var meshes = owner.GetMeshes();
                var mat = transformation.WorldMatrix;

                if (transformation.IsIndependent)
                {
                    mat = transformation.GetLocalMatrix();
                }

                for (int i = 0; i < meshes.Count; i++)
                {
                    var mesh = meshes[i];

                    var currentResult = MouseRayIntersectsMeshNarrowPhase(ray, mesh, mat);

                    if ((currentResult.Intersects && (currentResult.Distance < mouseIntersectionResult.Distance || !mouseIntersectionResult.Intersects)))
                    {
                        mouseIntersectionResult.SetResult(currentResult.Distance, currentResult.IntersectionCoordinate);
                        result.SetResult(currentResult.Distance, currentResult.IntersectionCoordinate);
                    }
                }
            }
        }

        public void RequestBoundingBoxUpdate()
        {
            if (!isBoundingObject)
            {
                boundingBoxNeedsUpdated = true;
            }
        }

        private void UpdateBoundingBox()
        {
            var meshes = owner.GetMeshes();
            if (isSphere)
            {
                var mesh = meshes.First();
                var vert = mesh.Vertices[0];
                var radius = vert.Vector.Length();

                boundingBox.MinX = -radius;
                boundingBox.MinY = -radius;
                boundingBox.MinZ = -radius;
                boundingBox.MaxX = radius;
                boundingBox.MaxY = radius;
                boundingBox.MaxZ = radius;
            }
            else
            {
                var verts = meshes.SelectMany(m => m.GetVertices());

                if (verts.Count() > 0)
                {
                    var diameter = verts.Max(v => v.Vector.Length());

                    var minX = verts.Min(v => v.Vector.X);
                    var minY = verts.Min(v => v.Vector.Y);
                    var minZ = verts.Min(v => v.Vector.Z);
                    var maxX = verts.Max(v => v.Vector.X);
                    var maxY = verts.Max(v => v.Vector.Y);
                    var maxZ = verts.Max(v => v.Vector.Z);

                    boundingBox.MaxX = maxX;
                    boundingBox.MaxY = maxY;
                    boundingBox.MaxZ = maxZ;
                    boundingBox.MinX = minX;
                    boundingBox.MinY = minY;
                    boundingBox.MinZ = minZ;
                }
            }

            boundingBoxNeedsUpdated = false;
        }

        public void UpdateBoundingSphere()
        {
            {
                if (!boundingBoxInitialized)
                {
                    UpdateBoundingBox();
                    boundingBoxInitialized = true;
                }

                boundingSphere.Position = boundingBox.GetCenter() + transformation.WorldMatrix.Translation;

                if (isSphere)
                {
                    boundingSphere.Diameter = boundingBox.GetLongestAxis();
                }
                else
                {
                    if (!owner.GetDrawState().IsBillboard)
                    {
                        boundingSphere.Diameter = boundingBox.GetDiagonalDistance();
                    }
                    else
                    {
                        float width = owner.GetDrawState().BillboardInfo.Width;
                        float height = owner.GetDrawState().BillboardInfo.Height;
                        float len = width;
                        if (height > len)
                        {
                            len = height;
                        }
                        boundingSphere.Diameter = len * 2;
                        boundingSphere.Position = transformation.PositionLocal;
                    }
                }
            }
        }

        public bool IsPickable { get => isPickable; set => isPickable = value; }
        public bool IsGizmoTranslatable
        {
            get => isGizmoTranslatable;
            set
            {
                isGizmoTranslatable = value;
            }
        }

        public void RequestCollisionUpdate()
        {
            if (!isBoundingObject)
            {
                collisionNeedsUpdated = true;
            }
        }

        public bool IsMouseHovered { get => isMouseHovered; set => isMouseHovered = value; }
        public TransformationGizmo TransformationGizmo { get => transformationGizmo; }
        public string Name { get => name; set => name = value; }
        public Transformation Transformation { get => transformation; set => transformation = value; }
        public bool OnlyVisibleInEditor { get => onlyVisibleInEditor; set => onlyVisibleInEditor = value; }
        public bool TranslationOnly { get => translationOnly; set => translationOnly = value; }
        public bool IgnoredByItemPlacementRayCast { get => ignoredByItemPlacementRayCast; set => ignoredByItemPlacementRayCast = value; }
        public bool IsDirectlyMouseHovered { get => isDirectlyMouseHovered; set => isDirectlyMouseHovered = value; }
        public bool IsLinkPicked { get => isLinkPicked; set => isLinkPicked = value; }
        public bool IsPickableMesh { get => isPickableMesh; set => isPickableMesh = value; }
        public BoundingSphere BoundingSphere { get => boundingSphere; }
        public IntersectionResult MouseIntersectionResultBroadPhase { get => mouseIntersectionResultBroadPhase; }
        public IntersectionResult MouseIntersectionResultNarrowPhase { get => mouseIntersectionResultNarrowPhase; }
        public bool IsBoundingObject { get => isBoundingObject; set => isBoundingObject = value; }
        public bool CollisionNeedsUpdated { get => collisionNeedsUpdated; set => collisionNeedsUpdated = value; }
        public bool IsSphere { get => isSphere; set => isSphere = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public bool IsDragging { get => isDragging; set => isDragging = value; }
        public IDrawableSceneObject PickableRoot { get => pickableRoot; set => pickableRoot = value; }
        public bool IgnoreBroadPhaseCollision { get => ignoreBroadPhaseCollision; set => ignoreBroadPhaseCollision = value; }
        public bool IsBillboard { get => isBillboard; set => isBillboard = value; }
    }
}
