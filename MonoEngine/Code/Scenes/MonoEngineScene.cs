﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Rendering;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Render;
using MonoEngine.Code.Render.Debug;
using MonoEngine.Code.Render.Input;
using MonoEngine.Code.Render.Input.Gizmos;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using MonoEngine.ViewModel;
using MonoGameCore.Collision;
using MonoGameCore.Render;
using MonoGameCore.Render.Buffer;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Render.Texture;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfCore.ViewModel;

namespace MonoEngine.Code.Scenes
{
    public class MonoEngineScene
    {
        protected List<Light> sceneLights;
        protected BufferManager bufferManager;
        protected List<IDrawableSceneObject> sceneObjects;
        protected int deltaMs;
        protected MonoViewportViewModel activeMonoViewportVm;
        protected MonoViewportViewModel currentMonoViewportVm;
        protected List<IMonoViewportViewModelWrapper> monoViewportViewModels;
        protected bool gameRunning;
        protected ShaderContainer shaders;
        protected GraphicsDevice graphicsDevice;
        protected EngineSettings settings;
        protected DefaultEditorLmbAction editorLmbAction;
        protected MouseHoverManager mouseHoverManager;

        protected Stack<IDrawableSceneObject> sceneObjectsToBeAdded;
        protected Stack<IDrawableSceneObject> sceneObjectsToBeRemoved;
        protected ResourcesLoader resources;

        protected IDrawableSceneObject rootSceneObject;

        // Debug
        protected BoundingSphereObject boundingSphereObject;


        // Engine Feature Objects
        protected GridObject grid;
        private Mesh lineMesh;

        // Testing font
        private SpriteBatch spriteBatch;
        private SpriteFont spriteFont1;
        private SpriteFont hudDebugFont;

        // Transformation gizmo
        protected TransformationGizmoManager transformationGizmoManager;

        public List<IDrawableSceneObject> SceneObjects { get => sceneObjects; }
        public MonoViewportViewModel ActiveMonoViewportVm { get => activeMonoViewportVm; }
        public bool GameRunning { get => gameRunning; set => gameRunning = value; }
        public ShaderContainer Shaders { get => shaders; set => shaders = value; }
        public GraphicsDevice GraphicsDevice { get => graphicsDevice; }
        public SpriteBatch SpriteBatch { get => spriteBatch; set => spriteBatch = value; }
        public SpriteFont SpriteFont1 { get => spriteFont1; set => spriteFont1 = value; }
        public SpriteFont HudDebugFont { get => hudDebugFont; set => hudDebugFont = value; }
        public TransformationGizmoManager TransformationGizmoManager { get => transformationGizmoManager; }
        public Mesh LineMesh { get => lineMesh; }
        public MonoViewportViewModel CurrentMonoViewportVm { get => currentMonoViewportVm; }
        public KeyboardCommandController KeyboardController { get => keyboardController; }
        public DefaultEditorLmbAction EditorLmbAction { get => editorLmbAction; set => editorLmbAction = value; }
        public bool DebugDrawPickableMeshes { get => DebugDrawPickableMeshes; }
        public MouseHoverManager MouseHoverManager { get => mouseHoverManager; }

        private RasterizerState rasterizeStateCull;
        private RasterizerState rasterizeStateNoCull;
        private KeyboardCommandController keyboardController;

        // Pickable object debug
        /*protected DrawState hoveredPickMeshDrawState;
        protected DrawState directlyHoveredPickMeshDrawState;
        protected DrawState pickMeshDrawState;
        protected DrawState selectedMeshDrawState;*/

        public event System.EventHandler PreSceneUpdateOccured;

        public MonoEngineScene(
            List<IMonoViewportViewModelWrapper> monoViewportViewModels,
            ResourcesLoader resources,
            EngineSettings settings,
            KeyboardCommandController keyboardController)
        {
#if DEBUG
            Directory.CreateDirectory(MonoEngine.Code.EngineSettings.DebugDir);
#endif
            this.resources = resources;
            rootSceneObject = new GenericSceneObject();
            this.keyboardController = keyboardController;
            LoadMeshes();
            transformationGizmoManager = new TransformationGizmoManager(this);
            InitializeRasterStates();
            this.settings = settings;
            grid = new GridObject(settings);
            sceneObjectsToBeAdded = new Stack<IDrawableSceneObject>();
            sceneObjectsToBeRemoved = new Stack<IDrawableSceneObject>();
            this.monoViewportViewModels = monoViewportViewModels;
            sceneObjects = rootSceneObject.GetDrawableChildren();
            sceneLights = new List<Light>();
            gameRunning = true;
            mouseHoverManager = new MouseHoverManager(this);
            boundingSphereObject = new BoundingSphereObject(resources);
        }

        private void LoadMeshes()
        {
        }

        private void InitializeRasterStates()
        {
            rasterizeStateNoCull = new RasterizerState();
            rasterizeStateCull = new RasterizerState();
            rasterizeStateNoCull.CullMode = CullMode.None;
            rasterizeStateCull.CullMode = CullMode.CullClockwiseFace;
        }

        protected void DisableCulling()
        {
            graphicsDevice.RasterizerState = rasterizeStateNoCull;
        }

        protected void EnableCulling()
        {
            graphicsDevice.RasterizerState = rasterizeStateCull;
        }

        public void RemoveAllSceneObjects()
        {
            foreach (var sceneObject in SceneObjects)
            {
                /*if (sceneObject == grid)
                {
                }
                else
                {
                    RemoveObject(sceneObject);
                }*/
                //RemoveObject(sceneObject);
            }
        }

        private void AddMeshBufferToScene(Mesh mesh, DrawState drawState)
        {
            bufferManager.AddMesh(mesh);
        }

        public void DrawSceneObject(IDrawableSceneObject sceneObj,
            ShaderContainer shaders, MonoViewportViewModel viewportVm,
            GraphicsDeviceManager graphicsDeviceManager,
            MouseState mouseState,
            Matrix4x4 matrix,
            bool ignoringDepthBuffer,
            bool drawingFirst)
        {
            {
                sceneObj.UpdateDrawState(this);
                var objectState = sceneObj.GetObjectState();
                var meshes = sceneObj.GetMeshes();
                var drawState = sceneObj.GetDrawState();

                if (settings.DebugHighlightMouseHoveredObjects)
                {
                    if (objectState.IsDirectlyMouseHovered)
                    {
                        drawState.IsSolidColor = true;
                        drawState.Color = System.Drawing.Color.Red;
                    }
                    else if (objectState.IsMouseHovered)
                    {
                        drawState.IsSolidColor = true;
                        drawState.Color = System.Drawing.Color.Orange;
                    }
                    else
                    {
                        drawState.IsSolidColor = false;
                        drawState.Color = System.Drawing.Color.White;
                    }
                }

                if (drawState.IsText)
                {
                    if (activeMonoViewportVm != null)
                    {
                        System.Numerics.Vector3 pos = sceneObj.GetObjectState().Transformation.PositionLocal;
                    }
                }

                if (drawState.TwoSides)
                {
                    DisableCulling();
                }
                else
                {
                    EnableCulling();
                }
                var trans = sceneObj.GetObjectState().Transformation;

                if (trans.IsIndependent)
                {
                    matrix = trans.GetLocalMatrix();
                }
                else
                {
                    if (!objectState.TranslationOnly)
                    {
                        matrix = Matrix4x4.Multiply(matrix, trans.GetLocalMatrix());
                    }
                    else
                    {
                        matrix = Matrix4x4.Multiply(Matrix4x4.CreateTranslation(matrix.Translation), Matrix4x4.CreateTranslation(trans.GetLocalMatrix().Translation));
                    }
                }

                sceneObj.PreDraw(viewportVm);

                if (viewportVm != null)
                {
                    //if (!settings.DebugShowPickableMeshes)
                    {
                        if (sceneObj.GetDrawState().IsVisible)
                        {
                            if (!sceneObj.HasCustomDrawMethod())
                            {
                                if (drawState.DrawFirst == drawingFirst)
                                {
                                    if (drawState.IgnoresDepthBuffer == ignoringDepthBuffer)
                                    {
                                        if (settings.DebugShowBillboards || !drawState.IsBillboard)
                                        {
                                            sceneObj.UpdateBillboards(viewportVm.Camera.Coordinates);

                                            if (sceneObj.GetDrawState().UseInternalLightList)
                                            {
                                                bufferManager.Draw(sceneObj.GetMeshes(), graphicsDevice, viewportVm.Camera,
                                                    matrix, sceneObj.GetDrawState(), sceneObj.GetInternalLights());
                                            }
                                            else
                                            {
                                                bufferManager.Draw(sceneObj.GetMeshes(), graphicsDevice, viewportVm.Camera,
                                                    matrix, sceneObj.GetDrawState(), sceneLights);
                                            }
                                        }
                                    }
                                }

                                foreach (var child in sceneObj.GetDrawableChildren())
                                {
                                    DrawSceneObject(child, shaders, viewportVm, graphicsDeviceManager, mouseState, matrix, ignoringDepthBuffer, drawingFirst);
                                }
                            }
                            else
                            {
                                sceneObj.CustomDraw(graphicsDevice, shaders, viewportVm.Camera, graphicsDeviceManager, mouseState);
                            }

                            // Draw bounding sphere
                            if (settings.DebugShowBroadPhaseCollision)
                            {
                                if (!sceneObj.GetObjectState().IsBoundingObject)
                                {
                                    boundingSphereObject.GetTransformation().SetLocalScale(sceneObj.GetObjectState().BoundingSphere.Diameter/2.0f);
                                    boundingSphereObject.GetTransformation().SetLocalPosition(sceneObj.GetObjectState().BoundingSphere.Position);

                                    DrawSceneObject(boundingSphereObject, shaders, viewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, ignoringDepthBuffer, drawingFirst);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ConfirmSceneObjectsToScene(IDrawableSceneObject sceneObject)
        {
            sceneObjects.Add(sceneObject);
        }

        private void DrawEngineFeatures()
        {
            grid.GetDrawState().IsVisible = settings.ShowGrid;
        }
        

        public void DrawScene(ShaderContainer shaders, MonoViewportViewModel monoViewportVm,
            GraphicsDeviceManager graphicsDeviceManager, MouseState mouseState)
        {
            if (gameRunning && graphicsDevice != null)
            {
                foreach (var sceneObject in sceneObjectsToBeAdded)
                {
                    ConfirmSceneObjectsToScene(sceneObject);
                }
                sceneObjectsToBeAdded.Clear();

                DrawEngineFeatures();

                this.currentMonoViewportVm = monoViewportVm;

                monoViewportVm.Camera.InitializeMatrix(graphicsDeviceManager.GraphicsDevice, monoViewportVm.ViewportWidth, monoViewportVm.ViewportHeight,
                    mouseState.MousePositionDpiScaled, monoViewportVm.MouseRay.Viewport, monoViewportVm.Dpi);

                foreach (var sceneObj in sceneObjects)
                {
                    DrawSceneObject(sceneObj, shaders, monoViewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, false, true);
                    DrawSceneObject(sceneObj, shaders, monoViewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, false, false);
                    DrawSceneObject(sceneObj, shaders, monoViewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, false, false);
                }
                graphicsDevice.Clear(ClearOptions.DepthBuffer, Microsoft.Xna.Framework.Color.Black, 1.0f, 1);
                foreach (var sceneObj in sceneObjects)
                {
                    DrawSceneObject(sceneObj, shaders, monoViewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, true, false);
                }
            }
        }

        public void DrawTextAtPoint(MonoViewportViewModel viewportVm, System.Numerics.Vector2 screenPoint, string text, Microsoft.Xna.Framework.Color color,
            SpriteFont font)
        {
            var projectionMatrix = viewportVm.Camera.ProjectionMatrix;
            var viewMatrix = viewportVm.Camera.ViewMatrix;

            spriteBatch.Begin();
            spriteBatch.DrawString(font, text, new Microsoft.Xna.Framework.Vector2(screenPoint.X, screenPoint.Y), color);
            spriteBatch.End();
        }

        public void DrawTextAtPoint(MonoViewportViewModel viewportVm, System.Numerics.Vector3 worldPosition, string text, Microsoft.Xna.Framework.Color color,
            SpriteFont font)
        {
            var projectionMatrix = viewportVm.Camera.ProjectionMatrix;
            var viewMatrix = viewportVm.Camera.ViewMatrix;

            var screenPoint = viewportVm.Viewport.
                Project(worldPosition.ToXnaVector3(),
                    MonoGameCore.Render.RenderUtility.ToXnaMatrix(projectionMatrix),
                    MonoGameCore.Render.RenderUtility.ToXnaMatrix(viewMatrix),
                    Microsoft.Xna.Framework.Matrix.Identity);

            spriteBatch.Begin();
            spriteBatch.DrawString(font, text, new Microsoft.Xna.Framework.Vector2(screenPoint.X, screenPoint.Y), color);
            spriteBatch.End();
        }

        public void UpdateMouseRays(ShaderContainer shaders, MonoViewportViewModel monoViewportVm, MonoViewportViewModel mouseRayViewport,
            GraphicsDeviceManager graphicsDeviceManager, MouseState mouseState)
        {
            mouseRayViewport.MouseRay.UpdateDrawState(this);
            monoViewportVm.MouseRay.UpdateDrawState(this);
        }

        /// <summary>
        /// Draws the ray casts by the user's mouse in the world-space. Primarily used during debugging.
        /// </summary>
        public void DrawMouseRays(ShaderContainer shaders, MonoViewportViewModel monoViewportVm, MonoViewportViewModel mouseRayViewport,
            GraphicsDeviceManager graphicsDeviceManager, MouseState mouseState)
        {
            DrawSceneObject(mouseRayViewport.MouseRay, shaders, monoViewportVm, graphicsDeviceManager, mouseState, Matrix4x4.Identity, false, false);
        }

        public void AddLight(Light light)
        {
            sceneLights.Add(light);
        }

        public void RemoveLight(Light light)
        {
            if (sceneLights.Contains(light))
            {
                sceneLights.Remove(light);
            }
        }

        public void AddObject(IDrawableSceneObject sceneObject)
        {
            AddObject(sceneObject, false);
        }

        public void AddObject(IDrawableSceneObject sceneObject, bool addTransformationGizmo)
        {
            var objState = sceneObject.GetObjectState();
            sceneObjectsToBeAdded.Push(sceneObject);
        }

        public void RemoveObject(IDrawableSceneObject sceneObject)
        {
            var children = sceneObject.GetDrawableChildren();

            foreach (var child in children)
            {
                RemoveObject(child);
            }
            sceneObjectsToBeRemoved.Push(sceneObject);
        }

        public void RemoveObjects(List<IDrawableSceneObject> sceneObjects)
        {
            while (sceneObjects.Count > 0)
            {
                RemoveObject(sceneObjects.First());
            }
        }

        public void UpdateActiveViewModel(MonoViewportViewModel monoViewportVm)
        {
            this.activeMonoViewportVm = monoViewportVm;
        }

        public void ResetScene()
        {
            DisposeMeshBuffers();
            sceneObjects.Clear();
            sceneObjectsToBeAdded.Clear();
            sceneObjectsToBeRemoved.Clear();
            sceneLights.Clear();

            // Engine feature objects are added back in.
            AddObject(grid, false);

            // Add debug objects
            AddObject(boundingSphereObject);
        }

        private void DisposeMeshBuffers()
        {
        }

        public virtual void OnPreSceneUpdate()
        {
            if (PreSceneUpdateOccured != null)
            {
                PreSceneUpdateOccured(this, EventArgs.Empty);
            }
        }

        public void UpdateScene(GameTime gameTime, MouseState mouseState, int deltaMs)
        {
            this.deltaMs = deltaMs;

            OnPreSceneUpdate();

            CleanScene();

            foreach (var sceneObject in sceneObjects)
            {
                Update(sceneObject, gameTime, mouseState, Matrix4x4.Identity, deltaMs);
            }

            mouseHoverManager.UpdateMouseHoverObjects();
        }

        private void Update(IDrawableSceneObject sceneObject, GameTime gameTime, MouseState mouseState, Matrix4x4 matrixStack, int deltaMs)
        {
            var localMatrix = sceneObject.GetObjectState().Transformation.GetLocalMatrix();
            var mat = Matrix4x4.Multiply(matrixStack, localMatrix);

            if (sceneObject.GetObjectState().Transformation.IsIndependent)
            {
                mat = localMatrix;
            }

            sceneObject.Update(deltaMs, matrixStack, mouseState, this);

            var children = sceneObject.GetDrawableChildren();
            var billboards = sceneObject.GetBillboards();

            foreach (var child in children)
            {
                Update(child, gameTime, mouseState, mat, deltaMs);
            }
        }

        private StringBuilder sceneLog;

        public void SaveSceneLog()
        {
            /*sceneLog.Clear();

            foreach (var obj in sceneObjects)
            {
                SaveSceneLog(obj, 0);
            }
            File.WriteAllText($"{EngineSettings.DebugDir}\\SceneLog.txt", sceneLog.ToString());*/
        }

        private void SaveSceneLog(IDrawableSceneObject sceneObject, int depth)
        {
            /*for (int i = 0; i < depth; i++)
            {
                sceneLog.Append("\t");
            }
            sceneLog.AppendLine($"{sceneObject.GetObjectState().Name}: World Translation {sceneObject.GetTransformation().WorldMatrix.Translation} Local Translation {sceneObject.GetTransformation().GetLocalMatrix().Translation} Rotation: {sceneObject.GetObjectState().Transformation.RotationRadiansLocal.X},{sceneObject.GetObjectState().Transformation.RotationRadiansLocal.Y},{sceneObject.GetObjectState().Transformation.RotationRadiansLocal.Z} Min Draw Distance: {sceneObject.GetDrawState().MinDrawDistance} Max Draw Distance: {sceneObject.GetDrawState().MaxDrawDistance}");

            foreach (var child in sceneObject.GetDrawableChildren())
            {
                SaveSceneLog(child, depth + 1);
            }*/
        }

        private void AppendTransformationToLog(string name, int depth, Transformation transformation)
        {
            /*for (int i = 0; i < depth; i++)
            {
                sceneLog.Append("\t");
            }
            sceneLog.AppendLine($"{name}: World Translation {transformation.WorldMatrix.Translation} Local Translation {transformation.GetLocalMatrix().Translation} Rotation: {transformation.RotationRadiansLocal.X},{transformation.RotationRadiansLocal.Y},{transformation.RotationRadiansLocal.Z}");
        */}

        private void CleanObject(IDrawableSceneObject sceneObject)
        {
            foreach (var child in sceneObject.GetDrawableChildren())
            {
                CleanObject(child);
            }
            if (sceneObjects.Contains(sceneObject))
            {
                sceneObjects.Remove(sceneObject);
            }
        }

        private void CleanScene()
        {
            /*while (sceneObjectsToBeRemoved.Any())
            {
                var sceneObject = sceneObjectsToBeRemoved.Pop();
                //SceneObjects.Remove(sceneObject);
                CleanObject(sceneObject);
            }
            while (sceneObjectsToBeAdded.Any())
            {
                var sceneObject = sceneObjectsToBeAdded.Pop();
                SceneObjects.Add(sceneObject);
            }*/
        }

        public void StopGame()
        {
            //DisposeMeshBuffers();
            gameRunning = false;
        }

        public void UpdateGraphicsDevice(GraphicsDevice graphicsDevice)
        {
            // Not sure if this is necessary.
            if (this.graphicsDevice != graphicsDevice)
            {
                this.graphicsDevice = graphicsDevice;
                bufferManager = new BufferManager(graphicsDevice, shaders);
            }
        }
    }
}
