﻿using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code
{
    /// <summary>
    /// Loads any meshes, textures, etc found in the Resources directory.
    /// </summary>
    public class ResourcesLoader
    {
        private Mesh sphereMesh;

        public ResourcesLoader()
        {
            var colladaImporter = new ColladaLibrary.Collada.ColladaImporter(true, true, "Resources/Meshes/sphere.dae");
            colladaImporter.Load();
            sphereMesh = colladaImporter.CreateSingleMesh();
        }

        public Mesh SphereMesh { get => sphereMesh; }
    }
}
