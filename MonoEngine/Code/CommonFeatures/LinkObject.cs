﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using FreeImageAPI;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.CommonFeatures
{
    public class LinkObject : GenericSceneObject
    {
        private ILinkableObject owner;
        private Mesh mesh;
        private Vector3 p0;
        private Vector3 p1;
        private IDrawableSceneObject parent;

        public LinkObject(ILinkableObject owner) : base()
        {
            internalLights.Add(CreateDefaultLight());
            this.owner = owner;
            drawState = new DrawState();
            p0 = new Vector3();
            p1 = new Vector3();
        }

        private Light CreateDefaultLight()
        {
            var light = new Light(LightType.Directional, Color.White, 1.0f);

            return light;
        }

        public void ClearPreviousMeshes()
        {
        }

        public List<IDrawableSceneObject> GetDrawableChildren()
        {
            return new List<IDrawableSceneObject>();
        }

        public DrawState GetDrawState()
        {
            return drawState;
        }

        public List<Light> GetInternalLights()
        {
            return internalLights;
        }

        public List<Mesh> GetMeshes()
        {
            if (mesh == null)
            {
                mesh = ShapeMeshUtility.CreateCylinder(new Vector3(), new Vector3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f, 8, null);
            }
            return new List<Mesh>() { mesh };
        }

        public List<Mesh> GetPreviousMeshes()
        {
            return new List<Mesh>();
        }

        public void PreDraw(MonoViewportViewModel monoViewportViewModel)
        {
        }

        public void UpdatePoints(Vector3 p0, Vector3 p1)
        {
            this.p0 = p0;
            this.p1 = p1;
        }

        public void Update(int deltaMs, MouseState mouseState, MonoEngineScene scene)
        {
            float scale = 100.0f;
            var length = Vector3.Distance(p0, p1);
            var direction = Vector3.Normalize(p0 - p1);
            float zDistance = p0.Z - p1.Z;
            var zRot = (float)Math.Atan2(direction.Y, direction.X) - (float)(Math.PI / 2.0f);
            var xRot = (float)Math.Asin(zDistance / length);

            GetTransformation().SetLocalScale(new Vector3(scale, length, scale));
            GetTransformation().SetLocalRadianRotationX(xRot);
            GetTransformation().SetLocalRadianRotationZ(zRot);
            GetTransformation().SetLocalPosition(p1);
        }

        public void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.NormalShader;
            drawState.UseInternalLightList = true;
            drawState.IsSolidColor = true;
            drawState.Color = System.Drawing.Color.CadetBlue;
            drawState.TwoSides = true;
        }

        public bool HasCustomDrawMethod()
        {
            return false;
        }

        public void CustomDraw(GraphicsDevice graphicsDevice, ShaderContainer shaders, Camera camera)
        {
        }

        public IDrawableSceneObject GetParent()
        {
            return parent;
        }

        public void SetParent(IDrawableSceneObject parent)
        {
            this.parent = parent;
        }
    }
}
