﻿using ColladaLibrary.Render;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.CommonFeatures
{
    public class LineObject : GenericSceneObject
    {
        private Mesh lineMesh;

        public LineObject(AxisDirection direction, Mesh lineMesh, IDrawableSceneObject parent, MonoEngineScene scene) : base()
        {
            this.lineMesh = lineMesh;
            var drawState = this.GetDrawState();
            drawState.PrimitiveType = Microsoft.Xna.Framework.Graphics.PrimitiveType.LineList;
            drawState.IsSolidColor = true;
            drawState.Color = System.Drawing.Color.White;
            drawState.Shadeless = true;
            drawState.ActiveShader = scene.Shaders.NormalShader;
            drawState.IsVisible = false;
            drawState.IgnoresDepthBuffer = true;
            objectState.Name = "Line Object";
            this.parent = parent;

            switch (direction)
            {
                case AxisDirection.Positive_X:
                    drawState.Color = System.Drawing.Color.Red;
                    break;
                case AxisDirection.Positive_Y:
                    drawState.Color = System.Drawing.Color.Green;
                    break;
                case AxisDirection.Positive_Z:
                    drawState.Color = System.Drawing.Color.Blue;
                    break;
            }

            SetMeshes(new List<Mesh>() { lineMesh });
        }

        public override void Update(int deltaMs, Matrix4x4 matrixStack, MouseState mouseState, MonoEngineScene scene)
        {
            base.Update(deltaMs, Matrix4x4.CreateTranslation(matrixStack.Translation), mouseState, scene);
        }

        /*public override void UpdateWorldMatrix()
        {
            worldMatrix = Matrix4x4.CreateTranslation(GetParent().GetWorldMatrix().Translation);
        }*/
    }
}
