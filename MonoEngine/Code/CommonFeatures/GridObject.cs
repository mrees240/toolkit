﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Rendering;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WpfCore.Utility;
using WpfCore.ViewModel;

namespace MonoEngine.Code.CommonFeatures
{
    public class GridObject : GenericSceneObject
    {
        private Mesh gridMesh;
        private EngineSettings engineSettings;

        public GridObject(EngineSettings engineSettings)
        {
            this.engineSettings = engineSettings;
            drawState = new DrawState();
            drawState.PrimitiveType = PrimitiveType.LineList;
            drawState.IsSolidColor = true;
            drawState.Shadeless = true;
            drawState.ChangesFarDistance = true;
            drawState.FarClipDrawDistance = 99999999999.0f;
            gridMesh = CreateGridMesh(engineSettings.GridLength, engineSettings.GridRows);
            meshes.Add(gridMesh);
        }

        private Mesh CreateGridMesh(float cellLength, int rows)
        {
            var color = System.Drawing.Color.White;
            var textures = new List<Bitmap>();
            var gridMesh = new Mesh();
            var bmp = TextureFileUtility.CreateSolidColorBitmap(System.Drawing.Color.White);

            //int rows = 10;
            //float length = 10000;
            float completeLength = cellLength * (rows - 1);
            float completeLengthHalf = completeLength / 2.0f;
            //rows *= 2 - 1;

            for (int iRow = 0; iRow < rows * 2; iRow++)
            {
                float yPos = -completeLengthHalf + iRow * cellLength;
                float yPos2 = yPos + cellLength;

                var vert0 = new Vertex(-completeLengthHalf, yPos, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);
                var vert1 = new Vertex(completeLengthHalf, yPos, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);
                var vert2 = new Vertex(yPos, -completeLengthHalf, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);
                var vert3 = new Vertex(yPos, completeLengthHalf, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);

                gridMesh.Vertices.Add(vert0);
                gridMesh.Vertices.Add(vert1);
                gridMesh.Vertices.Add(vert2);
                gridMesh.Vertices.Add(vert3);
            }
            bool oddRowCount = rows % 2 != 0;
            int polyCount = gridMesh.Vertices.Count / 2;
            for (int iPoly = 0; iPoly < polyCount; iPoly++)
            {
                int i0 = iPoly * 2;
                int i1 = i0 + 1;

                var index0 = new PolygonIndex(i0, color, new TextureCoordinate());
                var index1 = new PolygonIndex(i1, color, new TextureCoordinate());
                var index2 = new PolygonIndex(i1, color, new TextureCoordinate());
                var index3 = new PolygonIndex(i1, color, new TextureCoordinate());

                var p0 = new Polygon(index0, index1, index2, index3);
                gridMesh.AddPolygon(p0);
            }

            return gridMesh;
        }

        public void UpdateDrawState(MonoEngineScene scene)
        {
            drawState.ActiveShader = scene.Shaders.NormalShader;
            drawState.IsVisible = engineSettings.ShowGrid;
        }

        /*Dictionary<Mesh, List<MeshBuffer>> IDrawableSceneObject.LoadMeshBuffers(GraphicsDeviceManager graphicsDeviceManager, ShaderContainer shaders)
        {
            var pairs = new Dictionary<Mesh, List<MeshBuffer>>();

            var meshBuffer = CreateGridBuffer(graphicsDeviceManager, shaders);

            pairs.Add(gridMesh, new List<MeshBuffer>() { meshBuffer });

            return pairs;
        }*/

        public void Update(int deltaMs, MouseState mouseState, MonoEngineScene scene)
        {
        }

        public void SetLocalMatrix(Matrix4x4 matrix)
        {

        }
    }
}
