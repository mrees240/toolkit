﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.CommonFeatures
{
    public class WorldTextObject : GenericSceneObject
    {
        private Matrix4x4 worldMatrix;

        public WorldTextObject()
        {
            worldMatrix = Matrix4x4.Identity;
            drawState = new DrawState();
            drawState.IsText = true;
        }

        public string Text { get => drawState.Text; set { drawState.Text = value; } }
    }
}
