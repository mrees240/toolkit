﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code
{
    public class EngineSettings
    {
        // If this value needs to be changed, it must be updated in the shaders as well.
        public readonly int ShaderClipPlaneLimit = 6;

        public static readonly string DebugDir = "DebugLogs";

        public EngineSettings()
        {
            debugShowBillboards = true;
            debugUpdateMouseRays = true;
        }

        private bool debugShowPickableMeshes;
        private bool debugShowBillboards;
        private bool debugShowMouseRay;
        private bool debugUpdateMouseRays;
        private bool debugHighlightMouseHoveredObjects;
        private bool debugShowBroadPhaseCollision;
        private bool showGrid;
        private float gridLength = 10000;
        private int gridRows = 10;

        public bool ShowGrid { get => showGrid; set => showGrid = value; }
        public float GridLength { get => gridLength; set => gridLength = value; }
        public int GridRows { get => gridRows; set => gridRows = value; }
        public bool DebugShowPickableMeshes { get => debugShowPickableMeshes; set => debugShowPickableMeshes = value; }
        public bool DebugShowMouseRay { get => debugShowMouseRay; set => debugShowMouseRay = value; }
        public bool DebugShowBillboards { get => debugShowBillboards; set => debugShowBillboards = value; }
        public bool DebugHighlightMouseHoveredObjects { get => debugHighlightMouseHoveredObjects; set => debugHighlightMouseHoveredObjects = value; }
        public bool DebugShowBroadPhaseCollision { get => debugShowBroadPhaseCollision; set => debugShowBroadPhaseCollision = value; }
        public bool DebugUpdateMouseRays { get => debugUpdateMouseRays; set => debugUpdateMouseRays = value; }
    }
}
