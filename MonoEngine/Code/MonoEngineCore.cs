﻿using MonoEngine.Code.CommonFeatures;
using MonoEngine.Code.Render;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoEngine.Input.MouseInputs;
using MonoEngine.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WpfCore.ViewModel;

namespace MonoEngine.Code
{
    /// <summary>
    /// The main instance of the mono engine.
    /// </summary>
    public class MonoEngineCore
    {
        private bool inputFocused;
        private string errorMessage;
        private BaseEngineGame game;
        private List<IMonoViewportViewModelWrapper> monoViewportVmWrappers;
        private MouseControllerBase mouseController;
        private KeyboardCommandController keyboardController;
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private Stopwatch mainStopWatch;
        private int deltaMs;
        private MonoEngineScene gameScene;
        private MouseState mouseState;
        private EngineSettings engineSettings;
        private bool settingsIntialized;
        private System.Drawing.Rectangle windowRect;
        private System.Windows.DpiScale dpi;
        private ResourcesLoader resources;
        //private int visibleViewportCount;

        public MonoEngineCore(
            List<IMonoViewportViewModelWrapper> monoViewportVms,
            MouseControllerBase mouseController,
            EngineSettings engineSettings)
        {
            dpi = VisualTreeHelper.GetDpi(App.Current.MainWindow);
            //visibleViewportCount = 1;
            this.engineSettings = engineSettings;
            keyboardController = new KeyboardCommandController(this);
            this.mouseState = mouseController.MouseState;
            this.mouseController = mouseController;
            mainStopWatch = new Stopwatch();
            InputFocused = true;
            this.monoViewportVmWrappers = monoViewportVms;
            resources = new ResourcesLoader();
            this.gameScene = new MonoEngineScene(monoViewportVmWrappers, resources, engineSettings, keyboardController);
            this.game = new BaseEngineGame(this, monoViewportVms, gameScene, engineSettings, this.mouseState);
            
            InitializeTimers();
        }

        public MonoEngineCore(
            List<IMonoViewportViewModelWrapper> monoViewportVms,
            MouseControllerBase mouseController) : this(monoViewportVms, mouseController, new EngineSettings())
        {
        }

        public void StartEngine()
        {
            mainStopWatch.Start();
            timer.Start();
        }

        private void InitializeEngineSettings()
        {
            if (!settingsIntialized)
            {
                settingsIntialized = true;
            }
        }

        private void InitializeTimers()
        {
            timer.Interval = 1;
            timer.Tick += Timer_Tick;
        }

        public void StopGame()
        {
            game.StopGame();
        }

        private void UpdateWindowRect()
        {
            windowRect = new System.Drawing.Rectangle(
                           (int)App.Current.MainWindow.Left,
                           (int)App.Current.MainWindow.Top,
                           (int)App.Current.MainWindow.ActualWidth,
                           (int)App.Current.MainWindow.ActualHeight);
        }

        private bool CheckWindowChanged()
        {
            if (App.Current.MainWindow != null)
            {
                if (windowRect.Left != (int)App.Current.MainWindow.Left)
                {
                    return true;
                }
                if (windowRect.Top != (int)App.Current.MainWindow.Top)
                {
                    return true;
                }
                if (windowRect.Width != (int)App.Current.MainWindow.ActualWidth)
                {
                    return true;
                }
                if (windowRect.Height != (int)App.Current.MainWindow.ActualHeight)
                {
                    return true;
                }
            }
            return false;
        }

        public void RunOneFrame()
        {
            if (windowRect == null || CheckWindowChanged())
            {
                UpdateWindowRect();
            }
            if (game.GameRunning)
            {
                deltaMs = (int)mainStopWatch.ElapsedMilliseconds;

                if (inputFocused)
                {
                    mouseController.UpdateInput(deltaMs,
                        gameScene,
                        monoViewportVmWrappers, windowRect, dpi.DpiScaleX, dpi.DpiScaleY);
                    gameScene.KeyboardController.CheckKeyboardInput();
                }
                mainStopWatch.Restart();

                var currentMousePosition = mouseController.GetCurrentMousePosition();

                int viewportCount = monoViewportVmWrappers.Count;

                for (int iViewport = 0; iViewport < viewportCount; iViewport++)
                {
                    var monoViewportVmWrapper = monoViewportVmWrappers[iViewport];
                    var baseViewportVm = monoViewportVmWrapper.GetBaseViewportVm();
                    baseViewportVm.Update(deltaMs, mouseController.MouseState, game.Shaders, gameScene);

                    if (monoViewportVmWrapper.GetBaseViewportVm().IsMouseOver)
                    {
                        gameScene.UpdateActiveViewModel(monoViewportVmWrapper.GetBaseViewportVm());
                    }
                }

                game.RunOneFrame();
            }
        }

        private void RunFrameDebugMode()
        {
            if (gameScene.GameRunning)
            {
                RunOneFrame();
            }
        }

        private void RunFrameReleaseMode()
        {
            if (gameScene.GameRunning)
            {
                RunOneFrame();
                /*try
                {
                }
                catch (Exception ex)
                {
                    errorMessage = $"An Error Occured\n" +
                        $"OS Version: {Environment.OSVersion.ToString()}\n" +
                        $".NET Version: {Environment.Version.ToString()}\n" +
                        $"Source: {ex.Source}\n{ex.Message}" +
                        $"\n\n{ex.StackTrace}";
                    StopGame();
                }*/
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            InitializeEngineSettings();
#if DEBUG
            RunFrameDebugMode();
#else
            RunFrameReleaseMode();
#endif
        }

        public void RestartEngine()
        {
            errorMessage = string.Empty;
            gameScene.GameRunning = true;
        }

        public BaseEngineGame Game { get => game; }
        public MonoEngineScene GameScene { get => gameScene; }
        public string ErrorMessage { get => errorMessage; }
        public EngineSettings EngineSettings { get => engineSettings; }
        public KeyboardCommandController KeyboardController { get => keyboardController; }
        public MouseControllerBase MouseController { get => mouseController; }
        public bool InputFocused { get => inputFocused; set => inputFocused = value; }
        public List<IMonoViewportViewModelWrapper> MonoViewportVmWrappers { get => monoViewportVmWrappers; }
    }
}
