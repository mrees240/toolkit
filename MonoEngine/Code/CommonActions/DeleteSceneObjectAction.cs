﻿using MonoEngine.Code.Render;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.Code.CommonActions
{
    public class DeleteSceneObjectAction : IUndoRedoAction
    {
        private IDeletableSceneObject sceneObject;

        public DeleteSceneObjectAction(IDeletableSceneObject sceneObject)
        {

        }

        public void Action()
        {
        }

        public string GetActionText()
        {
            return $"Delete {sceneObject.GetObjectName()}";
        }

        public void UndoAction()
        {
        }
    }
}
