﻿using MonoEngine.Code;
using MonoEngine.Input;
using MonoEngine.ViewModel;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfCore.ViewModel;

namespace MonoEngine.Views
{
    /// <summary>
    /// Interaction logic for MonoEngineViewport.xaml
    /// </summary>
    public partial class MonoEngineViewport : UserControl
    {
        private System.Windows.Forms.Timer timer;
        //private MapEditorViewportViewModel monoVm;

        private Stopwatch deltaWatch;
        private Stopwatch mouseReleaseWatch;

        private int id;
        public static int idCounter = 0;

        private IMouseButtonActions mouseButtonActions;
        protected MonoViewportViewModel monoViewportVm;
        private Point defaultPoint;
        private MousePoint viewportPos;
        private DpiScale dpi;

        public MonoEngineViewport()
        {
            InitializeComponent();
            InitializeTimers();
            defaultPoint = new Point();
            viewportPos = new MousePoint();
            dpi = VisualTreeHelper.GetDpi(App.Current.MainWindow);
        }

        private void InitializeTimers()
        {
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1;
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (monoViewportVm == null)
            {
                monoViewportVm = GetViewModel();
            }
            if (monoViewportVm != null)
            {
                monoViewportVm.IsMouseOver = MonoWrapper.IsMouseOver;
            }
        }

        private MousePoint GetViewportScreenPosition()
        {
            var vpPos = this.PointToScreen(defaultPoint);
            viewportPos.SetCoordinates(vpPos.X, vpPos.Y);

            return viewportPos;
        }

        private MonoViewportViewModel GetViewModel()
        {
            return ((MonoViewportViewModel)DataContext);
        }

        private Point3D ToPt(Vector3D vec)
        {
            return new Point3D(vec.X, vec.Y, vec.Z);
        }

        private void RenderImageControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (monoViewportVm != null)
            {
                if (e.Delta < 0)
                {
                    monoViewportVm.CameraController.MoveBackward(1000.0f, false);
                }
                else
                {
                    monoViewportVm.CameraController.MoveForward(1000.0f, false);
                }
            }
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Loaded += MonoEngineViewport_Loaded;
        }

        public void UpdateViewport()
        {
            if (monoViewportVm == null)
            {
                monoViewportVm = GetViewModel();
            }
            if (monoViewportVm != null && RenderSize.Width > 0 && RenderSize.Height > 0)
            {
                monoViewportVm.Dpi = this.dpi;

                var actualScreenPosition = GetViewportScreenPosition();

                if (monoViewportVm != null)
                {
                    if (monoViewportVm.WindowHandle != null)
                    {
                        monoViewportVm.WindowHandle = RenderPanel.Handle;
                    }
                }

                /*if (monoViewportVm.Viewport.X != (int)actualScreenPosition.X || monoViewportVm.Viewport.Y != (int)actualScreenPosition.Y)
                {
                    monoViewportVm.Viewport = new Microsoft.Xna.Framework.Graphics.Viewport((int)actualScreenPosition.X, (int)actualScreenPosition.Y,
                        (int)(ActualWidth), (int)(ActualHeight));
                }*/

                bool viewportNeedsUpdate = (int)monoViewportVm.ViewportWidth != (int)ActualWidth || (int)monoViewportVm.ViewportHeight != (int)ActualHeight;

                if (viewportNeedsUpdate)
                {
                    monoViewportVm.XPos = actualScreenPosition.X;
                    monoViewportVm.YPos = actualScreenPosition.Y;
                    monoViewportVm.ViewportWidth = ActualWidth;
                    monoViewportVm.ViewportHeight = ActualHeight;

                    monoViewportVm.MouseRay.Viewport = new Microsoft.Xna.Framework.Graphics.Viewport((int)(monoViewportVm.XPos / dpi.DpiScaleX), (int)(monoViewportVm.YPos / dpi.DpiScaleY),
                        (int)(ActualWidth), (int)(ActualHeight));
                }
            }
        }

        private void MonoEngineViewport_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateViewport();
        }

        private void MonoEngineViewport_Initialized(object sender, EventArgs e)
        {
        }

        private void UserControl_Initialized(object sender, EventArgs e)
        {
            UpdateViewport();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateViewport();
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            monoViewportVm.MouseController.LmbController.MouseButtonActions.DoubleClickAction(monoViewportVm.Scene);
        }
    }
}
