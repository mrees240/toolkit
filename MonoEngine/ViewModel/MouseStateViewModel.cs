﻿using GalaSoft.MvvmLight;
using MonoEngine.Input;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoEngine.ViewModel
{
    public class MouseStateViewModel : ViewModelBase
    {
        private MouseState mouseState;

        public MouseStateViewModel(MouseState mouseState)
        {
            this.mouseState = mouseState;
            mouseState.MouseStateChanged += MouseState_MouseStateChanged;
        }

        private void MouseState_MouseStateChanged(object sender, EventArgs e)
        {
            RaisePropertyChanged(nameof(MousePosition));
            RaisePropertyChanged(nameof(MouseMovementDelta));
        }

        public MousePoint MousePosition { get { return mouseState.MousePosition; } }
        public MousePoint MouseMovementDelta { get { return mouseState.MouseMovementDelta; } }
    }
}
