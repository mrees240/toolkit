﻿using GalaSoft.MvvmLight;
using Microsoft.Xna.Framework.Graphics;
using MonoEngine.Code;
using MonoEngine.Code.Render.Debug;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using MonoGameCore.Input;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfCore.Utility;

namespace MonoEngine.ViewModel
{
    public class MonoViewportViewModel : ViewModelBase, IDisposable
    {
        protected Viewport viewport;
        protected CameraMouseRay mouseRay;
        protected CameraController cameraController;
        protected System.Windows.Media.Color backgroundColor;
        protected Camera camera;
        protected double viewportWidth;
        protected double viewportHeight;
        private double xPos;
        private double yPos;
        public double ViewportWidth { get => viewportWidth; set { viewportWidth = value; RaisePropertyChanged(); } }
        public double ViewportHeight { get => viewportHeight; set { viewportHeight = value; RaisePropertyChanged(); } }
        protected bool isMouseOver;
        private int msSinceFpsUpdate;
        private int msSinceLastFrameDraw;
        private int deltaMs;
        private SwapChainRenderTarget swapChain;
        private bool visible;
        private SolidColorBrush backgroundBrush;
        private MouseControllerBase mouseController;

        public Camera Camera { get => camera; }
        private MonoEngineScene scene;
        private int framesPerSecond;

        private IntPtr windowHandle;

        private DpiScale dpi;

        public MonoViewportViewModel()
        {
            viewport = new Viewport();
            Visible = true;
            camera = new Camera(CameraType.Fly, ProjectionType.Perspective);
            cameraController = new CameraController(camera);
        }

        /*public MousePoint GetViewportRelativeMousePosition(MousePoint mousePosition)
        {
            double x = mousePosition.X - Viewport.X;
            double y = mousePosition.Y - Viewport.Y;
            viewportRelativeMousePosition.SetCoordinates(x, y);

            return viewportRelativeMousePosition;
        }*/

        public System.Drawing.Point GetViewportCenter()
        {
            return new System.Drawing.Point((int)(xPos + ViewportWidth / 2.0f), (int)(yPos + ViewportHeight / 2.0f));
        }

        private void UpdateFramesPerSecond(int deltaMs)
        {
            msSinceFpsUpdate += deltaMs;
            msSinceLastFrameDraw += deltaMs;

            if (msSinceFpsUpdate >= 100)
            {
                msSinceFpsUpdate = 0;
                framesPerSecond = (int)(1000.0f / deltaMs);
            }
        }


        private void UpdateMouseRay(int deltaMs, MouseState mouseState, ShaderContainer shaders)
        {
            if (shaders != null && visible)
            {
                cameraController.UpdateCamera(deltaMs);
            }
        }

        public void Update(int deltaMs, MouseState mouseState, ShaderContainer shaders, MonoEngineScene scene)
        {
            if (Visible)
            {
                if (swapChain == null && windowHandle != null && scene.GraphicsDevice != null && (int)viewportWidth > 0 && (int)viewportHeight > 0)
                {
                    swapChain = new SwapChainRenderTarget(scene.GraphicsDevice, windowHandle, (int)(viewportWidth * dpi.DpiScaleX), (int)(viewportHeight * dpi.DpiScaleY));
                }
                if (swapChain != null)
                {
                    if (swapChain.Width != (int)viewportWidth || swapChain.Height != (int)viewportHeight)
                    {
                        if (viewportWidth > 2 && viewportHeight > 2)
                        {
                            swapChain.Dispose();

                            swapChain = new SwapChainRenderTarget(scene.GraphicsDevice, windowHandle, (int)(viewportWidth * dpi.DpiScaleX), (int)(viewportHeight * dpi.DpiScaleY));
                        }
                    }
                }
                if ((int)viewportWidth > 0 && (int)viewportHeight > 0)
                {
                    this.deltaMs = deltaMs;
                    if (this.scene == null)
                    {
                        this.scene = scene;
                    }
                    UpdateMouseRay(deltaMs, mouseState, shaders);
                }
            }
            UpdateFramesPerSecond(deltaMs);
        }

        private void UpdateSize()
        {

        }

        public double GetAspectRatio()
        {
            if (ViewportHeight > 0)
            {
                return ViewportWidth / ViewportHeight;
            }
            return 0.0f;
        }

        public void InitializeMouseRay(MonoEngineScene scene, EngineSettings settings)
        {
            this.scene = scene;
            mouseRay = new CameraMouseRay(camera, viewport, settings);
        }

        public void Dispose()
        {
            if (swapChain != null)
            {
                swapChain.Dispose();
            }
        }

        public System.Windows.Media.Color BackgroundColor { get => backgroundColor; set { backgroundColor = value; RaisePropertyChanged(); BackgroundBrush = new SolidColorBrush(backgroundColor); } }
        public SolidColorBrush BackgroundBrush { get => backgroundBrush; set { backgroundBrush = value; RaisePropertyChanged(); } }
        public CameraController CameraController { get => cameraController; }
        public CameraMouseRay MouseRay { get => mouseRay; set => mouseRay = value; }
        public MonoEngineScene Scene { get => scene; }
        public bool IsMouseOver { get => isMouseOver; set => isMouseOver = value; }
        public int FramesPerSecond { get => framesPerSecond; }
        public int MsSinceLastFrameDraw { get => msSinceLastFrameDraw; set => msSinceLastFrameDraw = value; }
        public int DeltaMs { get => deltaMs; }
        public IntPtr WindowHandle { get => windowHandle; set => windowHandle = value; }
        public SwapChainRenderTarget SwapChain { get => swapChain; }
        public bool Visible { get => visible; set { visible = value; RaisePropertyChanged(); } }

        public double XPos { get => xPos; set => xPos = value; }
        public double YPos { get => yPos; set => yPos = value; }
        public DpiScale Dpi { get => dpi; set => dpi = value; }
        public MouseControllerBase MouseController { get => mouseController; set => mouseController = value; }
        public Viewport Viewport { get => viewport; set => viewport = value; }
        //public CameraMouseRay CameraMouseRay { get => cameraMouseRay; }
    }
}
