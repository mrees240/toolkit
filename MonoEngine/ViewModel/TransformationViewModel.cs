﻿using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MonoEngine.ViewModel
{
    public class TransformationViewModel : ViewModelBase
    {
        private Transformation transformation;

        private float xRotationDegreesWorld;
        private float yRotationDegreesWorld;
        private float zRotationDegreesWorld;
        private float xPositionWorld;
        private float yPositionWorld;
        private float zPositionWorld;

        public TransformationViewModel(Transformation transformation)
        {
            this.transformation = transformation;
            transformation.TransformationChanged += Transformation_TransformationChanged;
        }

        private void Transformation_TransformationChanged(object sender, EventArgs e)
        {
        }

        public float XRotationDegreesWorld
        {
            get
            {
                return xRotationDegreesWorld;
            }
            set
            {
                xRotationDegreesWorld = value;
                var xRads = MathUtility.ToRadians(xRotationDegreesWorld);
                transformation.SetLocalRadianRotationX((float)xRads);
                RaisePropertyChanged();
            }
        }

        public float YRotationDegreesWorld
        {
            get
            {
                return yRotationDegreesWorld;
            }
            set
            {
                yRotationDegreesWorld = value;
                var yRads = MathUtility.ToRadians(yRotationDegreesWorld);
                transformation.SetLocalRadianRotationY((float)yRads);
                RaisePropertyChanged();
            }
        }

        public float ZRotationDegreesWorld
        {
            get
            {
                return zRotationDegreesWorld;
            }
            set
            {
                zRotationDegreesWorld = value;
                var zRads = MathUtility.ToRadians(zRotationDegreesWorld);
                transformation.SetLocalRadianRotationZ((float)zRads);
                RaisePropertyChanged();
            }
        }

        public float XPositionWorld
        {
            get { return xPositionWorld; }
            set
            {
                xPositionWorld = value;
                transformation.SetLocalXPosition(xPositionWorld);
                RaisePropertyChanged();
            }
        }

        public float YPositionWorld
        {
            get { return yPositionWorld; }
            set
            {
                yPositionWorld = value;
                transformation.SetLocalYPosition(yPositionWorld);
                RaisePropertyChanged();
            }
        }

        public float ZPositionWorld
        {
            get { return zPositionWorld; }
            set
            {
                zPositionWorld = value;
                transformation.SetLocalZPosition(zPositionWorld);
                RaisePropertyChanged();
            }
        }

        public Transformation Transformation { get => transformation; }

        public void UpdateTransformationInViewModel()
        {
            xPositionWorld = transformation.PositionLocal.X;
            yPositionWorld = transformation.PositionLocal.Y;
            zPositionWorld = transformation.PositionLocal.Z;
            RaisePropertyChanged(nameof(XPositionWorld));
            RaisePropertyChanged(nameof(YPositionWorld));
            RaisePropertyChanged(nameof(ZPositionWorld));

            xRotationDegreesWorld = (float)MathUtility.ToDegrees(transformation.RotationRadiansLocal.X);
            yRotationDegreesWorld = (float)MathUtility.ToDegrees(transformation.RotationRadiansLocal.Y);
            zRotationDegreesWorld = (float)MathUtility.ToDegrees(transformation.RotationRadiansLocal.Z);

            RaisePropertyChanged(nameof(XRotationDegreesWorld));
            RaisePropertyChanged(nameof(YRotationDegreesWorld));
            RaisePropertyChanged(nameof(ZRotationDegreesWorld));
        }
    }
}
