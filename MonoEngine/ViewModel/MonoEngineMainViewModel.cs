using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MonoEngine.Code;
using MonoEngine.Code.Scenes;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Windows;
using WpfCore.ViewModel;
using System.Linq;
using MonoEngine.Input.MouseInputs;

namespace MonoEngine.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MonoEngineMainViewModel : ViewModelBase
    {
        private List<MonoViewportViewModel> baseViewportViewModels;
        private List<IMonoViewportViewModelWrapper> viewportViewModels;
        private MonoEngineCore engineCore;


        protected bool renderErrorOccured;
        protected string renderErrorMessage;
        protected bool errorHandled;

        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public MonoEngineMainViewModel(MouseControllerBase mouseController)
        {
            baseViewportViewModels = new List<MonoViewportViewModel>();
            viewportViewModels = new List<IMonoViewportViewModelWrapper>();
            engineCore = new MonoEngineCore(viewportViewModels, mouseController);
            InitializeTimers();
            InitializeCommands();
        }

        public void AddViewportViewModel(IMonoViewportViewModelWrapper viewportViewModel)
        {
            var baseVm = viewportViewModel.GetBaseViewportVm();
            baseViewportViewModels.Add(baseVm);
            baseVm.InitializeMouseRay(engineCore.GameScene, engineCore.EngineSettings);
            baseVm.MouseController = engineCore.MouseController;
            viewportViewModels.Add(viewportViewModel);
        }

        public void StartEngine()
        {
            engineCore.StartEngine();
            timer.Start();
        }

        private void InitializeTimers()
        {
            timer.Interval = 1;
            timer.Tick += Timer_Tick;
        }

        private void InitializeCommands()
        {
            RestartEngineCommand = new RelayCommand(() =>
            {
                RestartEngine();
            });
        }

        public void RemoveViewportViewModel(IMonoViewportViewModelWrapper viewportVm)
        {
            baseViewportViewModels.Remove(viewportVm.GetBaseViewportVm());
            viewportViewModels.Remove(viewportVm);
        }


        public void StopGame(MonoEngine.Code.MonoEngineCore engineCore)
        {
            engineCore.StopGame();
        }

        public void HandleError()
        {
            RenderErrorMessage = engineCore.ErrorMessage;
            RenderErrorOccured = true;
            errorHandled = true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (!engineCore.GameScene.GameRunning && !errorHandled)
            {
                HandleError();
            }
        }

        public void RestartEngine()
        {
            errorHandled = false;
            engineCore.RestartEngine();
            RenderErrorOccured = false;
        }

        public RelayCommand RestartEngineCommand
        {
            private set;
            get;
        }

        public bool RenderErrorOccured { get => renderErrorOccured; set { renderErrorOccured = value; RaisePropertyChanged(); } }

        public string RenderErrorMessage { get => renderErrorMessage; set { renderErrorMessage = value; RaisePropertyChanged(); } }

        public List<MonoViewportViewModel> BaseViewportViewModels { get => baseViewportViewModels; set => baseViewportViewModels = value; }
        public List<IMonoViewportViewModelWrapper> ViewportViewModels { get => viewportViewModels; set => viewportViewModels = value; }
        public MonoEngineCore EngineCore { get => engineCore; }
    }
}