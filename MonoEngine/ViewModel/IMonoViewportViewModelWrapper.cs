﻿using MonoEngine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfCore.ViewModel
{
    public interface IMonoViewportViewModelWrapper
    {
        MonoViewportViewModel GetBaseViewportVm();
    }
}
