﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MonoEngine.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using WpfCore.Settings;

namespace MonoEngine.ViewModel
{
    public class KeyBindingViewModel : ViewModelBase
    {
        private bool remappingMode;
        private MonoEngine.Input.KeyBinding keyBinding;
        private string displayName;
        private string actionDescription;
        private string bindingPrompt;
        private SolidColorBrush backgroundColor;
        private double actionDescriptionColumnWidth;
        protected FontSettings fontSettings;
        protected RelayCommand remapCommand;
        private double actionDescriptionFontSizeRatio;

        public KeyBindingViewModel(MonoEngine.Input.KeyBinding keyBinding, string actionDescription, FontSettings fontSettings, double actionDescriptionFontSizeRatio)
        {
            this.actionDescriptionFontSizeRatio = actionDescriptionFontSizeRatio;
            this.fontSettings = fontSettings;
            this.actionDescription = actionDescription;
            this.keyBinding = keyBinding;
            fontSettings.FontSizeChanged += FontSettings_FontSizeChanged;
            UpdateDisplayName();
            bindingPrompt = $"Press a key to bind to {actionDescription}\n\nPress ESC to cancel";
            keyBinding.KeyBindingChanged += KeyBinding_KeyBindingChanged;
            byte shade = 80;
            backgroundColor = new SolidColorBrush(Color.FromArgb(255, shade, shade, shade));
        }

        private void FontSettings_FontSizeChanged(object sender, EventArgs e)
        {
            UpdateDisplayName();
        }

        private void KeyBinding_KeyBindingChanged(object sender, EventArgs e)
        {
            UpdateDisplayName();
        }

        public void ChangeKey(System.Windows.Input.Key key)
        {
            keyBinding.CurrentKey = key;
            UpdateDisplayName();
        }

        public void ChangeRemappingMode()
        {
            if (!remappingMode)
            {
                StartRemappingMode();
            }
            else
            {
                EndRemappingMode();
            }
        }

        public void StartRemappingMode()
        {
            remappingMode = true;
            DisplayName = "(Press a key)";
        }

        public void EndRemappingMode()
        {
            remappingMode = false;
            UpdateDisplayName();
        }

        private void UpdateDisplayName()
        {
            DisplayName = keyBinding.Converter.ConvertToString(keyBinding.CurrentKey);
            ActionDescriptionColumnWidth = fontSettings.FontSize * actionDescriptionFontSizeRatio;
        }

        public RelayCommand RemapCommand
        {
            get { return remapCommand; }
            set { remapCommand = value; RaisePropertyChanged(); }
        }

        public string DisplayName { get => displayName; set { displayName = value; RaisePropertyChanged(); } }

        public Input.KeyBinding KeyBinding { get => keyBinding; }
        public string ActionDescription { get => actionDescription; }
        public string BindingPrompt { get => bindingPrompt; }
        public SolidColorBrush BackgroundColor { get => backgroundColor; }
        public double ActionDescriptionColumnWidth { get => actionDescriptionColumnWidth; private set { actionDescriptionColumnWidth = value; RaisePropertyChanged(); } }
    }
}
