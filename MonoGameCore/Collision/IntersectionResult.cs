﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Collision
{
    public class IntersectionResult
    {
        private bool intersects;
        private float distance;
        private Vector3 intersectionCoordinate;

        public IntersectionResult()
        {

        }

        public IntersectionResult(bool intersects, float distance, Vector3 intersectionCoordinate)
        {
            this.intersects = intersects;
            this.distance = distance;
            this.intersectionCoordinate = intersectionCoordinate;
        }

        public IntersectionResult(bool intersects, float distance)
        {
            this.intersects = intersects;
            this.distance = distance;
        }

        public void Reset()
        {
            intersects = false;
            distance = 0;
            intersectionCoordinate = new Vector3();
        }

        public void SetResult(float distance, Vector3 intersectionCoord)
        {
            intersects = true;
            this.distance = distance;
            this.intersectionCoordinate = intersectionCoord;
            this.intersects = true;
        }

        public void SetResult(bool intersects, float distance, Vector3 intersectionCoordinate)
        {
            this.intersects = intersects;
            this.distance = distance;
            this.intersectionCoordinate = intersectionCoordinate;
        }

        public bool Intersects { get => intersects; }
        public float Distance { get => distance; }
        public Vector3 IntersectionCoordinate { get => intersectionCoordinate; }
    }
}
