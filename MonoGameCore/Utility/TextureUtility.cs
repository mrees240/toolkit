﻿using FreeImageAPI;
using MathNet.Spatial.Euclidean;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Utility
{
    public class TextureUtility
    {
        public static FreeImageBitmap GetBitmapFromTexture(Texture2D texture, System.Drawing.Imaging.PixelFormat imageFormat)
        {
            var width = texture.Width;
            var height = texture.Height;

            Bitmap bitmap = new Bitmap(width, height, imageFormat);

            IntPtr safePtr;
            BitmapData bitmapData;
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, width, height);

            var textureData = new byte[width * height * 4];

            texture.GetData(textureData);
            bitmapData = bitmap.LockBits(rect, ImageLockMode.WriteOnly, imageFormat);
            safePtr = bitmapData.Scan0;
            Marshal.Copy(textureData, 0, safePtr, textureData.Length);
            bitmap.UnlockBits(bitmapData);
            textureData = null;

            return new FreeImageBitmap(bitmap);
        }
    }
}
