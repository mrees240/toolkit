﻿using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Utility
{
    public class RenderUtility
    {
        public static void BillboardLookAtPoint(Transformation transformation, Vector3 billboardPosition, BillboardInfo billboard, Vector3 ptToLookAt)
        {
            var billboardDir = Vector3.Normalize(ptToLookAt - billboardPosition);
            var atan = Math.Atan2(billboardDir.X, billboardDir.Y);
            
            transformation.SetLocalRadianRotation(new Vector3(0.0f, 0.0f, (float)-atan));
        }

        public static Matrix4x4 GetBillboardLookAtPointRotationMatrix(Vector3 billboardPosition, BillboardInfo billboard, Vector3 ptToLookAt)
        {
            var billboardDir = Vector3.Normalize(ptToLookAt - billboardPosition);
            var atan = Math.Atan2(billboardDir.X, billboardDir.Y);

            var mat = Matrix4x4.CreateRotationZ((float)-atan);

            return mat;
        }

        public static void ObjectLookAtPoint(Transformation transformation, Vector3 objectPosition, Vector3 ptToLookAt)
        {
            var objectUp = Vector3.UnitY;

            if (float.IsNaN(objectPosition.Length()))
            {
                objectPosition = ptToLookAt - Vector3.UnitY;
            }

            var z = Vector3.Normalize(ptToLookAt - objectPosition);
            var x = Vector3.Normalize(Vector3.Cross(objectUp, z));
            if (float.IsNaN(x.Length()))
            {
                x = new Vector3(0.0f, 0.0f, 1.0f);
            }
            var y = Vector3.Cross(z, x);

            var rotMat = Matrix4x4.Identity;

            rotMat.M11 = x.X;
            rotMat.M12 = x.Y;
            rotMat.M13 = x.Z;
            rotMat.M21 = y.X;
            rotMat.M22 = y.Y;
            rotMat.M23 = y.Z;
            rotMat.M31 = z.X;
            rotMat.M32 = z.Y;
            rotMat.M33 = z.Z;

            var scale = transformation.ScaleLocal;

            var localMat = Matrix4x4.Multiply(Matrix4x4.CreateScale(scale), rotMat);

            transformation.SetLocalMatrix(localMat);
        }
    }
}
