﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Utility
{
    public class MeshUtility
    {
        public static Mesh CreateLineMesh(System.Drawing.Color color, Vector3 start, Vector3 end)
        {
            var lineMesh = new Mesh();
            var vert0 = new Vertex(start.X, start.Y, start.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);
            var vert1 = new Vertex(end.X, end.Y, end.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, color);

            lineMesh.Vertices.Add(vert0);
            lineMesh.Vertices.Add(vert1);
            lineMesh.Vertices.Add(vert1);
            lineMesh.Vertices.Add(vert1);

            var texCoord = new TextureCoordinate();

            var index0 = new PolygonIndex(0, color, texCoord);
            var index1 = new PolygonIndex(1, color, texCoord);
            var index2 = new PolygonIndex(2, color, texCoord);
            var index3 = new PolygonIndex(3, color, texCoord);

            var p = new Polygon(index0, index1, index2, index3);
            lineMesh.AddPolygon(p);
            return lineMesh;
        }
    }
}
