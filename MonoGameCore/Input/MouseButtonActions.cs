﻿using MonoGameCore.Input.State;
using MonoGameCore.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonoGameCore.Input
{
    public abstract class MouseButtonActions
    {
        protected InputSettings inputSettings;
        protected InputMouseState parentInputState;
        protected double dpiX;
        protected double dpiY;
        protected InputMouseButtonState buttonState;
        protected System.Windows.Forms.MouseButtons mouseButton;
        protected Point mousePosition;
        protected Point mousePositionBeforeGrab;
        protected Point mouseMovementDelta;
        protected Point viewportCenter;
        protected bool isOverViewport;
        private bool clickHandled;

        protected float minMouseMovementThreshold = 1.0f;
        protected bool orbitStarted;
        protected bool panStarted;
        protected bool cursorGrabbed;

        protected abstract void InnerUpAction();
        protected abstract void InnerDownAction();
        protected abstract void InnerClickAction();
        protected abstract void InnerHoldAction();
        protected abstract void InnerReleaseAction();

        /// <summary>
        /// Will always execute regardless of mouse state.
        /// </summary>
        protected abstract void InnerDefaultAction();

        public MouseButtonActions(
            System.Windows.Forms.MouseButtons mouseButton,
            InputMouseButtonState buttonState,
            InputMouseState parentInputState)
        {
            this.parentInputState = parentInputState;
            this.mouseButton = mouseButton;
            this.buttonState = buttonState;
        }

        private void DefaultAction()
        {
            InnerDefaultAction();
        }

        private void UpAction()
        {
            InnerUpAction();
        }

        private void DownAction()
        {
            InnerDownAction();
        }

        private void ClickAction()
        {
            InnerClickAction();
            buttonState.ButtonClicked = true;
            clickHandled = true;
        }

        private void HoldAction()
        {
            InnerHoldAction();
            buttonState.ButtonClicked = false;
            buttonState.ButtonHold = true;
        }

        private void ReleaseAction()
        {
            InnerReleaseAction();
            buttonState.ButtonHold = false;
            buttonState.ButtonClicked = false;
            clickHandled = false;
        }

        public void UpdateInput(Point mousePosition, bool isOverViewport, Point viewportCenter, double dpiX, double dpiY)
        {
            this.viewportCenter = viewportCenter;
            this.dpiX = dpiX;
            this.dpiY = dpiY;
            this.isOverViewport = isOverViewport;
            mouseMovementDelta = new Point(mousePosition.X - this.mousePosition.X, mousePosition.Y - this.mousePosition.Y);
            this.mousePosition = mousePosition;

            DefaultAction();

            if (isOverViewport)
            {
                buttonState.ButtonDown = MouseButtonDown();

                if (buttonState.ButtonDown)
                {
                    DownAction();
                }
                else
                {
                    UpAction();
                }

                if (buttonState.ButtonDown && !clickHandled)
                {
                    ClickAction();
                }
                else if (buttonState.ButtonDown && clickHandled)
                {
                    HoldAction();
                }
                else if (!buttonState.ButtonDown && clickHandled)
                {
                    ReleaseAction();
                }
            }
            else
            {
                if (buttonState.ButtonDown)
                {
                    ReleaseAction();
                }
            }
        }
        
        private bool MouseButtonDown()
        {
            return (System.Windows.Forms.Control.MouseButtons & mouseButton) == mouseButton;
        }

        public void GrabCursor()
        {
            cursorGrabbed = true;
            HideCursor();
            SetCursorPos(viewportCenter.X, viewportCenter.Y);
        }

        public void ReleaseCursor()
        {
            SetCursorPos(mousePositionBeforeGrab.X, mousePositionBeforeGrab.Y);
            cursorGrabbed = false;
            ShowCursor();
        }

        protected void HideCursor()
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.None;
        }

        protected void ShowCursor()
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }
        protected void PanCameraClick()
        {
            panStarted = true;
            GrabCursor();
        }

        protected void PanCameraRelease()
        {
            panStarted = false;
            ReleaseCursor();
        }

        protected bool MeetsXThreshold()
        {
            return Math.Abs(mouseMovementDelta.X) >= minMouseMovementThreshold;
        }

        protected bool MeetsYThreshold()
        {
            return Math.Abs(mouseMovementDelta.Y) >= minMouseMovementThreshold;
        }

        protected void PanCameraDown(Camera camera, float cameraPanSpeedMultiplier)
        {
            if (panStarted)
            {
                SetCursorPos((int)viewportCenter.X, (int)viewportCenter.Y);

                if (MeetsYThreshold())
                {
                    camera.Pan(0.0f, -mouseMovementDelta.Y, cameraPanSpeedMultiplier);
                }

                if (MeetsXThreshold())
                {
                    camera.Pan(-mouseMovementDelta.X, 0.0f, cameraPanSpeedMultiplier);
                }
            }
        }

        protected void OrbitCameraRelease()
        {
            orbitStarted = false;
            ReleaseCursor();
        }

        protected void OrbitCameraClick()
        {
            orbitStarted = true;
            GrabCursor();
        }

        protected void OrbitCameraDown(Camera camera, float orbitSpeedMultiplier)
        {
            if (orbitStarted)
            {
                //if (monoVcamera.ProjectionType == ProjectionType.Perspective)
                {
                    SetCursorPos((int)viewportCenter.X, (int)viewportCenter.Y);

                    if (MeetsYThreshold())
                    {
                        camera.RotatePitch(mouseMovementDelta.Y, orbitSpeedMultiplier);
                    }

                    if (MeetsYThreshold())
                    {
                        camera.RotateYaw(mouseMovementDelta.X, orbitSpeedMultiplier);
                    }
                }
            }
        }


        public InputMouseButtonState MouseState { get => buttonState; }


        [DllImport("User32.dll")]
        static extern void ClipCursor(ref System.Drawing.Rectangle rect);

        [DllImport("User32.dll")]
        static extern void ClipCursor(IntPtr rect);

        [DllImport("User32.dll")]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("User32.dll")]
        private static extern bool GetCursorPos(out System.Drawing.Point point);

        [DllImport("User32.dll")]
        private static extern void SetCursor(System.Windows.Input.Cursor cursor);
    }
}
