﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Input
{
    public class MousePoint
    {
        private double x;
        private double y;

        public void SetCoordinates(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public void SetCoordinates(MousePoint newPoint)
        {
            this.X = newPoint.x;
            this.Y = newPoint.y;
        }

        public void ScalePoint(float scaleX, float scaleY)
        {
            this.x *= scaleX;
            this.y *= scaleY;
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
    }
}
