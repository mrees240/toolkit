﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using MathNet.Spatial.Euclidean;
using MonoGameCore.Collision;
using ColladaLibrary.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MonoGameCore.Render;
using System.Windows.Input;

namespace MonoGameCore.Input
{
    public class Ray
    {
        private Vector3 startPoint;
        private Vector3 endPoint;

        public Ray()
        {
            startPoint = new Vector3();
            endPoint = new Vector3();
        }

        public Ray(Vector3 nearPoint, Vector3 farPoint)
        {
            this.startPoint = nearPoint;
            this.endPoint = farPoint;
        }

        public void Update(Vector3 nearPoint, Vector3 farPoint)
        {
            this.startPoint = nearPoint;
            this.endPoint = farPoint;
        }

        public g3.Ray3f GetRayG3()
        {
            var direction = (endPoint - startPoint);

            var v0 = new g3.Vector3f(startPoint.X, startPoint.Y, startPoint.Z);
            var v1 = new g3.Vector3f(direction.X, direction.Y, direction.Z);

            var ray = new g3.Ray3f(v0, v1);
            return ray;
        }

        public Vector3 GetRayVector3()
        {
            var direction = (endPoint - startPoint);
            var v0 = new Vector3(startPoint.X, startPoint.Y, startPoint.Z);
            var v1 = new Vector3(direction.X, direction.Y, direction.Z);

            return v1 - v0;
        }

        public Ray3D GetRay()
        {
            var direction = Vector3.Normalize(endPoint - startPoint);

            var v0 = new Point3D(startPoint.X, startPoint.Y, startPoint.Z);
            var v1 = new Vector3D(direction.X, direction.Y, direction.Z);

            var ray = new Ray3D(v0, v1);
            return ray;
        }

        public void RayTriangleListIntersection(Mesh mesh, Matrix4x4 matrix, IntersectionResult finalResult)
        {
            bool intersects = false;
            float closestDistance = -1.0f;
            var intersectionCoordinate = new Vector3();
            IntersectionResult result = new IntersectionResult();

            int polyAmount = mesh.PolygonAmount;

            var currentTriangle = new Triangle();

            for (int iPoly = 0; iPoly < polyAmount; iPoly++)
            {
                var poly = mesh.GetPolygon(iPoly);
                var vertices = poly.GetVertices(mesh);

                currentTriangle.V0 = vertices[0].Vector;
                currentTriangle.V1 = vertices[1].Vector;
                currentTriangle.V2 = vertices[2].Vector;

                currentTriangle.V0 = Vector3.Transform(currentTriangle.V0, matrix);
                currentTriangle.V1 = Vector3.Transform(currentTriangle.V1, matrix);
                currentTriangle.V2 = Vector3.Transform(currentTriangle.V2, matrix);

                RayTriangleIntersection(currentTriangle, result);

                if (result.Intersects)
                {
                    intersects = true;

                    if (closestDistance < 0 || result.Distance < closestDistance)
                    {
                        closestDistance = result.Distance;
                        intersectionCoordinate = result.IntersectionCoordinate;

                        finalResult.SetResult(true, closestDistance, intersectionCoordinate);
                    }
                }
            }
        }

        public void RayTriangleIntersection(Triangle triangle, IntersectionResult finalResult)
        {
            var currentTestSphere = new BoundingSphere();

            // Broadphase first for each triangle
            var l0 = Vector3.Distance(triangle.V0, triangle.V1);
            var l1 = Vector3.Distance(triangle.V1, triangle.V2);
            var l2 = Vector3.Distance(triangle.V2, triangle.V0);

            var longest = l0;

            if (l1 > longest)
            {
                longest = l1;
            }
            if (l2 > longest)
            {
                longest = l2;
            }

            currentTestSphere.Diameter = longest * 2;
            currentTestSphere.Position = triangle.V0;

            finalResult.Reset();

            var ray = GetRayG3();

            Intersects(currentTestSphere, Matrix4x4.Identity, finalResult);

            // Narrow phase
            if (finalResult.Intersects)
            {
                var tri = new g3.Triangle3f(
                    new g3.Vector3f(triangle.V0.X, triangle.V0.Y, triangle.V0.Z),
                    new g3.Vector3f(triangle.V1.X, triangle.V1.Y, triangle.V1.Z),
                    new g3.Vector3f(triangle.V2.X, triangle.V2.Y, triangle.V2.Z)
                    );

                var intersection = new g3.IntrRay3Triangle3(ray, tri);
                var compute = intersection.Compute();
                var result = intersection.Result;

                var origin = new Vector3((float)intersection.Ray.Origin.x, (float)intersection.Ray.Origin.y, (float)intersection.Ray.Origin.z);
                var direction = new Vector3((float)intersection.Ray.Direction.x, (float)intersection.Ray.Direction.y, (float)intersection.Ray.Direction.z);
                float distance = (float)compute.RayParameter;

                var pickOriginalCoordinate = origin + direction * distance;

                if (result == g3.IntersectionResult.Intersects)
                {
                    finalResult.SetResult(true, (float)compute.RayParameter, pickOriginalCoordinate);
                }
                else
                {
                    finalResult.Reset();
                }
            }
        }

        private bool SphereContainsRayStart(BoundingSphere sphere)
        {
            if (Vector3.Distance(sphere.Position, startPoint) < (sphere.Diameter/2.0f))
            {
                return true;
            }

            return false;
        }

        public void Intersects(BoundingSphere sphere, Matrix4x4 matrix, IntersectionResult finalResult)
        {
            if (SphereContainsRayStart(sphere))
            {
                finalResult.SetResult(0.0f, startPoint);
            }
            else
            {
                var dir = Vector3.Normalize(endPoint - startPoint);
                var s = new Microsoft.Xna.Framework.BoundingSphere(sphere.Position.ToXnaVector3(), sphere.Diameter / 2.0f);
                var v = new Microsoft.Xna.Framework.Ray(startPoint.ToXnaVector3(), (dir.ToXnaVector3()));

                var distance = v.Intersects(s);

                if (distance != null && distance > 0)
                {
                    var intersection = startPoint + dir * (float)distance;
                    finalResult.SetResult((float)distance, intersection);
                }
                else
                {
                    finalResult.Reset();
                }
            }
        }

        public Point3D? RayPlaneIntersection(MathNet.Spatial.Euclidean.Plane plane)
        {
            var ray = GetRay();

            Point3D? intersection = ray.IntersectionWith(plane);

            return intersection;
        }

        public void SetBothPointsY(float y)
        {
            startPoint.Y = y;
            endPoint.Y = y;
        }

        public string GetInfoString()
        {
            return $"({startPoint.ToString()}) -> ({endPoint.ToString()})";
        }

        public void SetBothPointsZ(float z)
        {
            startPoint.Z = z;
            endPoint.Z = z;
        }

        public Vector3 StartPoint { get => startPoint; set => startPoint = value; }
        public Vector3 EndPoint { get => endPoint; set => endPoint = value; }
    }
}
