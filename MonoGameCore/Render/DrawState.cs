﻿using ColladaLibrary.Render;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render
{
    public class DrawState
    {
        private PrimitiveType primitiveType;
        private bool ignoresDepthBuffer;
        private Shader activeShader;
        private bool isVisible;
        private bool drawFirst;
        private bool useSizeRelativeToScreen;
        private bool useInternalLightList;
        private float screenRelativeSize;
        private bool twoSides;
        private bool meshBufferNeedsUpdated;
        private Vector3[] clipPlanes;
        private bool clippingEnabled;
        private bool isHeightmap;
        private float[] heightmapHeights;
        private bool shadeless;
        private bool isText;
        private string text;
        private bool isSolidColor;
        private Microsoft.Xna.Framework.Vector4 colorVec;
        private Microsoft.Xna.Framework.Vector3 transparencyColor;
        private Microsoft.Xna.Framework.Vector3 lineStart;
        private Microsoft.Xna.Framework.Vector3 lineEnd;
        private bool isLine;
        private Color color;
        private bool isBillboard;
        private BillboardInfo billboardInfo;
        private float minDrawDistance;
        private float maxDrawDistance;
        bool applyDrawDistance;
        private bool neverMoves;
        private bool changesNearDistance;
        private bool changesFarDistance;
        private float nearClipDrawDistance;
        private float farClipDrawDistance;

        private readonly int clipPlaneLimit = 6;

        public DrawState()
        {
            Color = System.Drawing.Color.White;
            transparencyColor = new Microsoft.Xna.Framework.Vector3(0.0f);
            clipPlanes = new Vector3[clipPlaneLimit];
            isVisible = true;
            meshBufferNeedsUpdated = true;
            text = string.Empty;
        }

        public bool IgnoresDepthBuffer { get => ignoresDepthBuffer; set => ignoresDepthBuffer = value; }
        public Shader ActiveShader { get => activeShader; set => activeShader = value; }
        public bool IsVisible { get => isVisible; set => isVisible = value; }
        public PrimitiveType PrimitiveType { get => primitiveType; set => primitiveType = value; }
        public bool UseSizeRelativeToScreen { get => useSizeRelativeToScreen; set => useSizeRelativeToScreen = value; }
        public bool UseInternalLightList { get => useInternalLightList; set => useInternalLightList = value; }
        public float ScreenRelativeSize { get => screenRelativeSize; set => screenRelativeSize = value; }
        public bool TwoSides { get => twoSides; set => twoSides = value; }
        public bool MeshBufferNeedsUpdated { get => meshBufferNeedsUpdated; set => meshBufferNeedsUpdated = value; }
        public bool ClippingEnabled { get => clippingEnabled; set => clippingEnabled = value; }

        public int ClipPlaneLimit => clipPlaneLimit;

        public void SetClipPlaneDirection(int index, Vector3 direction)
        {
            if (!float.IsNaN(direction.X))
            {
                clipPlanes[index] = direction;
            }
        }
        //public Vector3[] ClipPlanesDirections { get => clipPlaneDirections; }
        public bool IsHeightmap { get => isHeightmap; set => isHeightmap = value; }
        public float[] HeightmapHeights { get => heightmapHeights; set => heightmapHeights = value; }
        public bool Shadeless { get => shadeless; set => shadeless = value; }
        public Vector3[] ClipPlanes { get => clipPlanes; }
        public bool IsText { get => isText; set => isText = value; }
        public string Text { get => text; set => text = value; }
        public bool IsSolidColor { get => isSolidColor; set => isSolidColor = value; }
        public Microsoft.Xna.Framework.Vector4 ColorVec { get => colorVec; }
        public Microsoft.Xna.Framework.Vector3 TransparencyColor { get => transparencyColor; set => transparencyColor = value; }
        public Color Color { get => color; set { color = value;
                colorVec = new Microsoft.Xna.Framework.Vector4(color.R/(255.0f), color.G / (255.0f), color.B / (255.0f), color.A / (255.0f));
            } }

        public bool IsBillboard { get => isBillboard; set => isBillboard = value; }
        public bool IsLine { get => isLine; set => isLine = value; }
        public Microsoft.Xna.Framework.Vector3 LineStart { get => lineStart; set => lineStart = value; }
        public Microsoft.Xna.Framework.Vector3 LineEnd { get => lineEnd; set => lineEnd = value; }
        public BillboardInfo BillboardInfo { get => billboardInfo; set => billboardInfo = value; }
        public float MinDrawDistance { get => minDrawDistance; set => minDrawDistance = value; }
        public float MaxDrawDistance { get => maxDrawDistance; set => maxDrawDistance = value; }
        public bool ApplyDrawDistance { get => applyDrawDistance; set => applyDrawDistance = value; }
        public bool DrawFirst { get => drawFirst; set => drawFirst = value; }
        public bool NeverMoves { get => neverMoves; set => neverMoves = value; }
        public bool ChangesNearDistance { get => changesNearDistance; set => changesNearDistance = value; }
        public bool ChangesFarDistance { get => changesFarDistance; set => changesFarDistance = value; }
        public float NearClipDrawDistance { get => nearClipDrawDistance; set => nearClipDrawDistance = value; }
        public float FarClipDrawDistance { get => farClipDrawDistance; set => farClipDrawDistance = value; }
    }
}
