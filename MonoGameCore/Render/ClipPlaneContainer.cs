﻿using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render
{
    public class ClipPlaneContainer
    {
        //private System.Numerics.Plane[] planes;
        private Vector3[] planePositions;
        private Vector3[] planeDirections;
        private Vector4[] planesNormalForm;

        public ClipPlaneContainer(int clipPlaneLimit)
        {
            //planes = new System.Numerics.Plane[clipPlaneLimit];
            planesNormalForm = new Vector4[clipPlaneLimit];
            planePositions = new Vector3[clipPlaneLimit];
            planeDirections = new Vector3[clipPlaneLimit];
        }

        //public System.Numerics.Plane[] Planes { get => planes; }
        public Vector4[] PlanesNormalForm { get => planesNormalForm; }
        public Vector3[] PlaneDirections { get => planeDirections; }
        public Vector3[] PlanePositions { get => planePositions; }
    }
}
