﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MonoGameCore.Render
{
    public static class VectorExtensions
    {
        public static Vector4 ToXnaVector4(this System.Numerics.Vector4 vec)
        {
            return new Vector4(vec.X, vec.Y, vec.Z, vec.W);
        }

        public static Vector4 ToXnaVector4(this System.Numerics.Vector3 vec, float w)
        {
            return new Vector4(vec.X, vec.Y, vec.Z, w);
        }

        public static Vector3 ToXnaVector3(this System.Numerics.Vector3 vec)
        {
            return new Vector3(vec.X, vec.Y, vec.Z);
        }

        public static Vector2 ToXnaVector2(this System.Numerics.Vector2 vec)
        {
            return new Vector2(vec.X, vec.Y);
        }

        public static System.Numerics.Vector3 GetNormalizedVector3(this System.Numerics.Vector3 vec)
        {
            var len = vec.Length();

            return new System.Numerics.Vector3(vec.X / len, vec.Y / len, vec.Z / len);
        }
    }
}
