﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using MonoGameCore.Collision;
using MonoGameCore.Input;

namespace MonoGameCore.Render
{
    /*public class PickableMesh
    {
        private ColladaLibrary.Render.Mesh originalMesh;
        private ColladaLibrary.Render.Mesh currentMesh;
        // Will add bounding sphere
        private BoundingSphere sphere;
        private float currentRadius;
        private Vector3 meshCenterOffset;
        private IntersectionResult intersectionResult;
        private Transformation transformation;

        public PickableMesh(ColladaLibrary.Render.Mesh mesh, Transformation transformation)
        {
            this.transformation = transformation;
            this.originalMesh = mesh;
            this.currentMesh = new ColladaLibrary.Render.Mesh(this.originalMesh);
            meshCenterOffset = originalMesh.GetCenterPoint();
            intersectionResult = new IntersectionResult();
        }

        public void UpdatePickableMesh()
        {
            for (int iVert = 0; iVert < currentMesh.Vertices.Count; iVert++)
            {
                currentMesh.Vertices[iVert].Vector = Vector3.Transform(originalMesh.Vertices[iVert].Vector, transformation.WorldMatrix);
            }
            UpdateBoundingSphere();
        }

        public void UpdateBoundingSpherePosition()
        {
            if (sphere == null)
            {
                UpdateBoundingSphere();
            }
            else
            {
                sphere.Position = transformation.WorldMatrix.Translation + meshCenterOffset;
            }
        }

        public void UpdateBoundingSphere()
        {
            if (currentMesh != null && currentMesh.Vertices.Any())
            {
                var origin = currentMesh.GetCenterPoint();
                var radius = currentMesh.Vertices.Max(v => Vector3.Distance(v.Vector, origin));
                sphere = new BoundingSphere(origin, radius * 2);
            }
            else
            {
                sphere = new BoundingSphere();
            }
        }

        public bool RayIntersectsMeshBroadPhase(Ray ray)
        {
            intersectionResult.Reset();

            ray.Intersects(sphere, intersectionResult);

            return intersectionResult.Intersects;
        }

        public IntersectionResult RayIntersectsMeshNarrowPhase(Ray ray)
        {
            intersectionResult.Reset();

            ray.RayTriangleListIntersection(currentMesh.GetBasicTriangles(), intersectionResult);

            return intersectionResult;
        }

        public ColladaLibrary.Render.Mesh OriginalMesh { get => originalMesh; }
        public BoundingSphere Sphere { get => sphere; }
        public ColladaLibrary.Render.Mesh CurrentMesh { get => currentMesh; }
        public Transformation Transformation { get => transformation; }
    }*/
}
