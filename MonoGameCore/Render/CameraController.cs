﻿using ColladaLibrary.Utility;
using MonoGameCore.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render
{
    public class CameraController
    {
        private Camera camera;

        public CameraController(Camera camera)
        {
            this.camera = camera;
        }


        public void UpdateCamera(
            int deltaMs)
        {
            camera.Update(deltaMs);
        }


        public void Pan(MousePoint mouseMovementDelta, float panSpeedMultiplier)
        {
            var xDist = mouseMovementDelta.X;
            var yDist = mouseMovementDelta.Y;

            if (Math.Abs(xDist) > 0)
            {
                MoveRight((int)(xDist), panSpeedMultiplier);
            }
            if (Math.Abs(yDist) > 0)
            {
                if (camera.ProjectionType == ProjectionType.Perspective)
                {
                    yDist = -yDist;
                }
                MoveUp((int)(yDist), panSpeedMultiplier);
            }
        }

        public void RotateLeftRight(float mouseDiff, float mouseSensitivity)
        {
            camera.RotateLeftRight((float)(mouseDiff * mouseSensitivity));
        }
        public void RotateUpDown(float mouseDiff, float mouseSensitivity)
        {
            camera.RotateUpDown((float)(mouseDiff * mouseSensitivity));
        }

        public void MoveForward(float speedMultiplier, bool applyDelta)
        {
            float currentSpeed = camera.FindCurrentSpeed() * speedMultiplier;
            var forward = camera.CameraDirection;
            forward *= currentSpeed;

            camera.Translate(forward, applyDelta);
        }

        public void MoveForward()
        {
            float currentSpeed = camera.FindCurrentSpeed();
            var forward = camera.CameraDirection;
            forward *= currentSpeed;

            camera.Translate(forward, true);
        }

        public void MoveBackward(float speedMultiplier, bool applyDelta)
        {
            float currentSpeed = camera.FindCurrentSpeed() * speedMultiplier;
            var backward = -camera.CameraDirection;
            backward *= currentSpeed;

            camera.Translate(backward, applyDelta);
        }

        public void MoveBackward()
        {
            float currentSpeed = camera.FindCurrentSpeed();
            var backward = -camera.CameraDirection;
            backward *= currentSpeed;

            camera.Translate(backward, true);
        }

        public void MoveUp(int deltaMouseMovement)
        {
            MoveUp(deltaMouseMovement, 1.0f);
        }

        public void MoveUp(int deltaMouseMovement, float speedMultiplier)
        {
            float currentSpeed = camera.FindCurrentSpeed() * deltaMouseMovement;
            var up = camera.CameraUp;
            up *= (currentSpeed * speedMultiplier);

            camera.Translate(up, true);
        }

        public void MoveDown(int deltaMouseMovement)
        {
            float currentSpeed = camera.FindCurrentSpeed() * deltaMouseMovement;
            var down = -camera.CameraUp;
            down *= currentSpeed;

            camera.Translate(down, true);
        }

        public void MoveRight(int deltaMouseMovement)
        {
            MoveRight(deltaMouseMovement, 1.0f);
        }

        public void MoveRight(int deltaMouseMovement, float speedMultiplier)
        {
            float currentSpeed = camera.FindCurrentSpeed() * deltaMouseMovement;
            var right = camera.GetRightDirection();
            right *= (currentSpeed * speedMultiplier);

            camera.Translate(right, true);
        }

        public void MoveLeft(int deltaMouseMovement)
        {
            float currentSpeed = camera.FindCurrentSpeed() * deltaMouseMovement;
            var left = -camera.GetRightDirection();
            left *= currentSpeed;

            camera.Translate(left, true);
        }

        /*private void Move(float radianOffset, float deltaMs)
        {
            float currentSpeed = camera.FindCurrentSpeed();

            double zRot = Math.Cos(MathUtility.ConvertToRadians(camera.Direction.X) + radianOffset);
            double xRot = Math.Sin(MathUtility.ConvertToRadians(camera.Direction.Y) + radianOffset);
            double yRot = Math.Cos(MathUtility.ConvertToRadians(camera.Direction.Y) + radianOffset);

            float xTranslation = -(float)(xRot * currentSpeed * deltaMs * Math.Sin(MathUtility.ConvertToRadians(camera.Direction.X)));
            float yTranslation = -(float)(yRot * currentSpeed * deltaMs * Math.Sin(MathUtility.ConvertToRadians(camera.Direction.X)));
            float zTranslation = -((float)(zRot * currentSpeed * deltaMs));


            camera.Translate(xTranslation, yTranslation, zTranslation);
    }

        private void Move(float radianOffset)
        {
            Move(radianOffset, 1);
        }*/

        /*public void MoveUp(float deltaMs)
        {
            if (camera.ProjectionType == ProjectionType.Perspective)
            {
                camera.RotateDirectionX(90.0f);
                MoveForward((float)deltaMs);
                camera.RotateDirectionX(-90.0f);
            }
        }*/

        /*private void Strafe(float radianOffset, float delta)
        {
            float currentSpeed = camera.FindCurrentSpeed();

            float xTranlation = ((float)(Math.Sin(MathUtility.ConvertToRadians(camera.Direction.Y) + radianOffset)) * currentSpeed * delta);
            float yTranlation = ((float)(Math.Cos(MathUtility.ConvertToRadians(camera.Direction.Y) + radianOffset)) * currentSpeed * delta);

            camera.Translate(xTranlation, yTranlation, 0.0f);
        }

        public void MoveLeft(float deltaMs)
        {
            switch (camera.CameraType)
            {
                case CameraType.Fly:
                    Strafe(3.14f * 1.5f, deltaMs);
                    break;
            }
        }

        public void MoveRight(float deltaMs)
        {
            switch (camera.CameraType)
            {
                case CameraType.Fly:
                    Strafe(3.14f / 2.0f, deltaMs);
                    break;
            }
        }*/
    }
}
