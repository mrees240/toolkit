﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Shaders
{
    public class ShaderContainer
    {
        private Shader normalShader;
        /*private Shader normalShaderBumpMap;
        private Shader normalShaderInstanced;
        private Shader diffuseOnlyShader;
        private Shader depthShader;*/
        //private Shader instanceShader;
        //private Shader highlightSelectionShader;
        //private Shader diffuseOnlyShader;
        //private Shader farShader;

        public ShaderContainer()
        {
        }

        public Shader NormalShader { get => normalShader; set => normalShader = value; }
        /*public Shader NormalShaderInstanced { get => normalShaderInstanced; set => normalShaderInstanced = value; }
        public Shader DiffuseOnlyShader { get => diffuseOnlyShader; set => diffuseOnlyShader = value; }
        public Shader NormalShaderBumpMap { get => normalShaderBumpMap; set => normalShaderBumpMap = value; }
        public Shader DepthShader { get => depthShader; set => depthShader = value; }*/
        //public Shader InstanceShader { get => instanceShader; set => instanceShader = value; }
        //public Shader DiffuseOnlyShader { get => diffuseOnlyShader; set => diffuseOnlyShader = value; }
        //public Shader HighlightSelectionShader { get => highlightSelectionShader; set => highlightSelectionShader = value; }
        //public Shader FarShader { get => farShader; set => farShader = value; }
    }
}
