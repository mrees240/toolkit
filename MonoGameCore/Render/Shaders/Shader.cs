﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Shaders
{
    public class Shader
    {
        private Effect effect;
        private ShaderType shaderType;
        private bool hasLighting;

        public Shader(ContentManager content, string fileName, ShaderType shaderType)
        {
            effect = content.Load<Microsoft.Xna.Framework.Graphics.Effect>(fileName);
            this.shaderType = shaderType;
        }

        public Shader(Effect effect, ShaderType shaderType)
        {
            this.effect = effect;
            this.shaderType = shaderType;
        }

        public ShaderType ShaderType { get => shaderType; set => shaderType = value; }
        public Effect Effect { get => effect; set => effect = value; }
        public bool HasLighting { get => hasLighting; set => hasLighting = value; }
    }
}
