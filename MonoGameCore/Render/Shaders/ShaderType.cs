﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Shaders
{
    public enum ShaderType
    {
        Normal, NormalInstanced, NormalWithBumpMap, DiffuseColorOnly, HighlightSelection, DiffuseColorOnlyConstantSize, ColorOnly, Other,
        Depth
    }
}
