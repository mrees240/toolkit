﻿using System;
using Microsoft.Xna.Framework.Graphics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ColladaLibrary.Misc;
using FreeImageAPI;

namespace MonoGameCore.Render.Texture
{
    /// <summary>
    /// Provides a link between a Texture2D object and a bitmap so it can easily be updated at render-time.
    /// </summary>
    public class TextureBitmap
    {
        private Texture2D texture;
        private BitmapFilePair bitmap;
        private Bitmap queuedBitmap;
        private GraphicsDevice graphics;

        public TextureBitmap(BitmapFilePair bitmapFilePair)
        {
            this.bitmap = bitmapFilePair;
            if (bitmapFilePair != null)
            {
                bitmap.Updated += Bitmap_Updated;
                ChangeBitmap(bitmapFilePair.Bitmap);
            }
        }

        private void Bitmap_Updated(object sender, EventArgs e)
        {
            ChangeBitmap(bitmap.Bitmap);
        }

        public void ChangeBitmap(Bitmap bitmap)
        {
            this.queuedBitmap = bitmap;
            texture = null;
        }

        public void Update(GraphicsDevice graphics)
        {
            if (this.graphics == null)
            {
                this.graphics = graphics;
            }
            if (queuedBitmap != null)
            {
                if (texture != null)
                {
                    texture.Dispose();
                }

                queuedBitmap = null;

                using (var stream = new MemoryStream())
                {
                    var cloned = new Bitmap(bitmap.Bitmap);
                    cloned.Save(stream, ImageFormat.Bmp);
                    texture = Texture2D.FromStream(graphics, stream);
                    cloned.Dispose();
                }
            }
        }

        public Texture2D Texture
        {
            get
            {
                if (texture == null)
                {
                    Update(graphics);
                    //throw new Exception("Texture was not created. The update method must be called before accessing the texture.");
                }

                return texture;
            }
        }

        public Bitmap Bitmap { get => bitmap.Bitmap; }
        public bool TransparencyEnabled { get { return bitmap.TransparencyEnabled; } set { bitmap.TransparencyEnabled = value; } }
    }
}
