﻿using ColladaLibrary.Render;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Texture
{
    public class TextureMapSet
    {
        private TextureBitmap diffuse;
        private TextureBitmap normal;
        private bool needsUpdated;

        public TextureMapSet(BitmapSet bitmapSet)
        {
            needsUpdated = true;
            if (bitmapSet.Diffuse != null)
            {
                diffuse = new TextureBitmap(bitmapSet.Diffuse);
            }
            if (bitmapSet.Normal != null)
            {
                normal = new TextureBitmap(bitmapSet.Normal);
            }
        }

        public void Update(GraphicsDevice graphicsDevice)
        {
            if (needsUpdated)
            {
                if (Diffuse != null)
                {
                    diffuse.Update(graphicsDevice);
                }
                if (Normal != null)
                {
                    normal.Update(graphicsDevice);
                }
                needsUpdated = false;
            }
        }

        public List<TextureBitmap> GetAllTextures()
        {
            return new List<TextureBitmap>() { diffuse, normal };
        }

        public TextureBitmap Diffuse { get => diffuse; }
        public TextureBitmap Normal { get => normal; }

        public bool TransparencyEnabled
        {
            get
            {
                return diffuse.TransparencyEnabled;
            }
        }
    }
}
