﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Texture
{
    /// <summary>
    /// Stores info on where a texture map set is located in a Texture2D.
    /// </summary>
    public class BitmapSetInfo
    {
        private int x;
        private int y;

        public BitmapSetInfo()
        {

        }

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
    }
}
