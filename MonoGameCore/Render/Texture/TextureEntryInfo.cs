﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Texture
{
    public class TextureEntryInfo
    {
        private Bitmap bitmap;
        private float x;
        private float y;

        public TextureEntryInfo(Bitmap bitmap, float x, float y)
        {
            this.bitmap = bitmap;
            this.x = x;
            this.y = y;
        }

        public float X { get => x; set => x = value; }
        public float Y { get => y; set => y = value; }
    }
}
