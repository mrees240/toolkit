﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Render;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Render.Texture;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render
{
    /// <summary>
    /// Primary class for applying shader variables before being passed to the shader.
    /// </summary>
    public class ShaderManager
    {
        // Shader parameters
        private const string projectionMatrixProperty = "xProjectionMatrix";
        private const string viewMatrixProperty = "xViewMatrix";
        private const string worldMatrixProperty = "xWorldMatrix";
        private const string localPositionProperty = "xLocalPosition";
        private const string worldMatrixInverseProperty = "xWorldMatrixInverse";
        private const string hasDiffuseTextureProperty = "xHasDiffuseTexture";
        private const string diffuseTextureProperty = "xDiffuseTexture";
        private const string normalTextureProperty = "xNormalTexture";
        private const string hasNormalTextureProperty = "xHasNormalTexture";
        private const string lightDirectionsProperty = "xLightDirections";
        private const string lightColorsProperty = "xLightColors";
        private const string shadelessProperty = "xIsShadeless";
        private const string isSolidColorProperty = "xIsSolidColor";
        private const string colorProperty = "xSolidColor";
        private const string useLocalLightsProperty = "xUseLocalLights";

        private const string clippingEnabledProperty = "xClippingEnabled";
        private const string clipPlanesProperty = "xClippingPlanes";
        private const string activeClipPlanesProperty = "xActiveClippingPlanes";

        private const string worldInverseTransposeProperty = "xWorldInverseTranspose";

        private const string isHeightmapProperty = "xIsHeightmap";
        private const string heightmapHeightsProperty = "xHeightmapHeights";
        private const string heightmapMinProperty = "xHeightmapMin";
        private const string heightmapMaxProperty = "xHeightmapMax";

        private const string transparencyColorProperty = "xTransparencyColor";
        private const string transparencyProperty = "xTransparency";

        private const string isBillboardProperty = "xIsBillboard";
        private const string isLineProperty = "xIsLine";
        private const string lineStartProperty = "xLineStartPoint";
        private const string lineEndProperty = "xLineEndPoint";

        private const string neverMovesProperty = "xNeverMoves";

        /// <summary>
        /// If this limit needs to be changed. It must be updated in the shader as well.
        /// </summary>
        private const int lightLimit = 6;

        protected TextureBitmap defaultTexture;
        protected bool defaultTextureInitialized;

        private ShaderContainer shaders;

        private void CreateDefaultTexture()
        {
            var checker = ColladaLibrary.Utility.RenderUtility.GetCheckerBitmap();
            var filePair = new BitmapFilePair();
            filePair.Bitmap = checker;
            defaultTexture = new TextureBitmap(filePair);
        }

        public ShaderManager(ShaderContainer shaders)
        {
            this.shaders = shaders;
            CreateDefaultTexture();
        }

        private void UpdateLights(Shader shader, List<Light> lights)
        {
            var lightDirs = new Microsoft.Xna.Framework.Vector4[lightLimit];
            var lightColors = new Microsoft.Xna.Framework.Vector4[lightLimit];

            for (int iLight = 0; iLight < lightLimit; iLight++)
            {
                if (iLight < lights.Count)
                {
                    var light = lights[iLight];

                    if (light.LightType == LightType.Directional)
                    {
                        var dir = light.GetDirection();
                        lightDirs[iLight] = new Microsoft.Xna.Framework.Vector4(dir.X, dir.Y, dir.Z, 1.0f);
                    }
                    else
                    {
                        lightDirs[iLight] = new Microsoft.Xna.Framework.Vector4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                    lightColors[iLight] = RenderUtility.ColorToXnaVector4(light.Color, light.Intensity);
                }
                else
                {
                    lightDirs[iLight] = new Microsoft.Xna.Framework.Vector4(0.0f, 0.0f, 0.0f, -1.0f);
                    lightColors[iLight] = RenderUtility.ColorToXnaVector4(System.Drawing.Color.Black, 0.0f);
                }
            }

            shader.Effect.Parameters[lightDirectionsProperty].SetValue(lightDirs);
            shader.Effect.Parameters[lightColorsProperty].SetValue(lightColors);
        }

        private Transformation shaderTransformation = new Transformation();

        public void ApplyShaderVariables(
            GraphicsDevice graphics,
            Matrix4x4 projectionMatrix,
            Matrix4x4 viewMatrix,
            PrimitiveType primitiveType,
            Matrix4x4 currentMatrix,
            TextureMapSet textureMaps,
            List<Light> lights,
            DrawState drawState)
        {
            if (drawState.IsVisible)
            {
                var worldMat = MonoGameCore.Render.RenderUtility.ToXnaMatrix(currentMatrix);
                var viewMat = MonoGameCore.Render.RenderUtility.ToXnaMatrix(Matrix4x4.Multiply(currentMatrix, viewMatrix));
                var proj = MonoGameCore.Render.RenderUtility.ToXnaMatrix(projectionMatrix);
                var shader = shaders.NormalShader;
                var shaderType = shader.ShaderType;

                shader.Effect.Parameters[useLocalLightsProperty].SetValue(drawState.UseInternalLightList);
                shader.Effect.Parameters[isBillboardProperty].SetValue(drawState.IsBillboard);
                shader.Effect.Parameters[isLineProperty].SetValue(drawState.IsLine);
                shader.Effect.Parameters[lineStartProperty].SetValue(drawState.LineStart);
                shader.Effect.Parameters[lineEndProperty].SetValue(drawState.LineEnd);
                shader.Effect.Parameters[localPositionProperty].SetValue(new Microsoft.Xna.Framework.Vector3(currentMatrix.Translation.X, currentMatrix.Translation.Y, currentMatrix.Translation.Z));
                shader.Effect.Parameters[projectionMatrixProperty].SetValue(proj);
                shader.Effect.Parameters[viewMatrixProperty].SetValue(viewMat);
                shader.Effect.Parameters[worldMatrixProperty].SetValue(worldMat);
                shader.Effect.Parameters[neverMovesProperty].SetValue(drawState.NeverMoves);

                /*if (shaderType == ShaderType.Other || shaderType == ShaderType.Depth)
                {
                    return;
                }*/

                if (textureMaps != null)
                {
                    if (textureMaps.Diffuse.Bitmap != null && textureMaps.Diffuse.Texture == null)
                    {
                        textureMaps.Update(graphics);
                    }
                    shader.Effect.Parameters[diffuseTextureProperty].SetValue(textureMaps.Diffuse.Texture);
                }
                else
                {
                    if (!drawState.IsSolidColor)
                    {
                        if (!defaultTextureInitialized)
                        {
                            defaultTexture.Update(graphics);
                            defaultTextureInitialized = true;
                        }
                        shader.Effect.Parameters[diffuseTextureProperty].SetValue(defaultTexture.Texture);
                    }
                }

                switch (shaderType)
                {
                    case ShaderType.Normal:
                    case ShaderType.NormalInstanced:
                    case ShaderType.NormalWithBumpMap:
                        {
                            if (textureMaps != null)
                            {
                                shader.Effect.Parameters[transparencyProperty].SetValue(textureMaps.Diffuse.TransparencyEnabled);
                            }
                            shader.Effect.Parameters[transparencyColorProperty].SetValue(drawState.TransparencyColor);
                            shader.Effect.Parameters[shadelessProperty].SetValue(drawState.Shadeless);
                            shader.Effect.Parameters[isSolidColorProperty].SetValue(drawState.IsSolidColor);
                            shader.Effect.Parameters[colorProperty].SetValue(drawState.ColorVec);
                        }
                        break;
                }
                UpdateLights(shader, lights);
                if (textureMaps != null)
                {
                    switch (shaderType)
                    {
                        case ShaderType.Normal:
                        case ShaderType.NormalWithBumpMap:
                            {
                                shader.Effect.Parameters[hasDiffuseTextureProperty].SetValue(textureMaps.Diffuse.Texture != null);
                            }
                            break;
                    }
                }
            }
        }
    }
}
