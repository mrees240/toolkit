﻿using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Input;
using MonoGameCore.Render.Shaders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using System.Windows;

namespace MonoGameCore.Render
{
    public enum ProjectionType { Perspective, Ortho };

    public class Camera
    {
        private float rotationSpeedConstant;
        private ProjectionType projectionType;
        private CameraType cameraType;
        private float orthoZoom;
        private float cameraSpeed;
        private bool sprinting;
        private bool crawling;
        private bool movingForward;
        private bool movingBackward;
        private bool strafingLeft;
        private bool strafingRight;
        private float fovDegrees;
        private int deltaMs;
        private float near;
        private float far;

        private Matrix4x4 cameraRotation;

        private Vector3 coordinates;
        private float leftRightRotation;
        private float upDownRotation;

        private Matrix4x4 projectionMatrix;
        private Matrix4x4 viewMatrix;
        private Vector3 cameraDirection;
        private Vector3 cameraUp;
        private Vector3 cameraRight;


        private Vector3 cameraDefaultTarget;
        private Vector3 cameraRotTarget;
        private Vector3 cameraFinalTarget;
        private Vector3 cameraOriginalUpVector = new Vector3(0, 1, 0);
        private Vector3 cameraOriginalRightVector = new Vector3(1, 0, 0);

        private bool initialized;

        private double aspectRatio;
        private Matrix4x4 perspectiveMatrix;
        private Matrix4x4 perspectiveMatrixMouseUnprojection;
        private Matrix4x4 viewMatrixMouseUnprojection;
        private Matrix4x4 orthoMatrix;

        private Vector3 nearPoint;
        private Vector3 farPoint;

        public Camera(CameraType cameraType, ProjectionType projectionType)
        {
            near = 1.0f;
            far = 99999999.0f;
            rotationSpeedConstant = 0.0005f;
            fovDegrees = 90.0f;
            orthoZoom = 0.25f;
            this.projectionType = projectionType;
            projectionMatrix = new Matrix4x4();
            viewMatrix = new Matrix4x4();
            if (cameraType == CameraType.Orbit)
            {
                /*coordinates = new System.Numerics.Vector3(0, 0, 200);
                direction = new System.Numerics.Vector3(-65, 145, 0);*/
            }
            /*else if (cameraType == CameraType.Fly)
            {
                coordinates = new System.Numerics.Vector3(0, 0, 500);
                direction = new System.Numerics.Vector3(0, 0, -90);
            }*/
            cameraSpeed = 1.0f;
            this.cameraType = cameraType;
            coordinates = new Vector3();

            coordinates.Z = 3500;
            leftRightRotation = (float)Math.PI / 4.0f;
            upDownRotation = (float)Math.PI / 4.0f;
        }

        /*public System.Numerics.Vector3 GetForwardVector()
        {
            return MathUtility.EulerDegreesToNormal(new System.Numerics.Vector3(0.0f, 0.0f, -1.0f), direction.X, direction.Y, direction.Z);
        }*/

        private void SetPosition(System.Numerics.Vector3 pos)
        {
            this.coordinates.X = pos.X;
            this.coordinates.Y = pos.Y;
            this.coordinates.Z = pos.Z;
        }

        private void Move(float xDist, float yDist, float zDist)
        {
            float x = coordinates.X + xDist;
            float y = coordinates.Y + yDist;
            float z = coordinates.Z + zDist;

            coordinates.X = x;
            coordinates.Y = y;
            coordinates.Z = z;
        }

        /*

        public void DrawMouseLine(GraphicsDevice graphicsDevice, Shader shader)
        {
            if (projectionType == ProjectionType.Perspective)
            {
                //new MeshBufferController(graphicsDevice, shader).Draw(mouseLineBuffer, this, Matrix4x4.Identity);
            }
        }*/

        /// <summary>
        /// Creates a view matrix for the selected camera based on its position, rotation, and the dimensions of the viewport.
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="wrapperWidth"></param>
        /// <param name="wrapperHeight"></param>
        public void InitializeMatrix(Microsoft.Xna.Framework.Graphics.GraphicsDevice graphics, double wrapperWidth, double wrapperHeight,
            MousePoint mousePoint, Viewport viewport, DpiScale dpi)
        {
            double currentAspectRatio = 0.0;
            if (wrapperHeight > 0)
            {
                currentAspectRatio = wrapperWidth / wrapperHeight;
            }
            if (aspectRatio != currentAspectRatio)
            {
                initialized = false;
            }

            if (!initialized)
            {
                aspectRatio = currentAspectRatio;
                perspectiveMatrix =
                    System.Numerics.Matrix4x4.CreatePerspectiveFieldOfView(
                    Microsoft.Xna.Framework.MathHelper.ToRadians(fovDegrees),
                    (float)aspectRatio,
                    near, far);
                perspectiveMatrixMouseUnprojection =
                    System.Numerics.Matrix4x4.CreatePerspectiveFieldOfView(
                    Microsoft.Xna.Framework.MathHelper.ToRadians(fovDegrees),
                    (float)aspectRatio,
                    0.1f, 100000f);
                orthoMatrix =
                    System.Numerics.Matrix4x4.CreateOrthographic(
                    (float)(wrapperWidth),
                    (float)(wrapperHeight),
                    -far, far);

                if (projectionType == ProjectionType.Perspective)
                {
                    projectionMatrix = perspectiveMatrix;
                }
                else if (projectionType == ProjectionType.Ortho)
                {
                    projectionMatrix = orthoMatrix;
                }


                initialized = true;
            }

            UpdateViewMatrix();

            float mouseX = (float)(mousePoint.X);
            float mouseY = (float)(mousePoint.Y);

            if (viewport.Bounds.Contains(mouseX, mouseY))
            {
                var nearPointXna = viewport.
                    Unproject(new Microsoft.Xna.Framework.Vector3(mouseX,
                    mouseY, 0.0f),
                        MonoGameCore.Render.RenderUtility.ToXnaMatrix(perspectiveMatrixMouseUnprojection),
                        MonoGameCore.Render.RenderUtility.ToXnaMatrix(viewMatrix),
                        Microsoft.Xna.Framework.Matrix.Identity);

                var farPointXna = viewport.
                    Unproject(new Microsoft.Xna.Framework.Vector3(mouseX,
                    mouseY, 1.0f),
                        MonoGameCore.Render.RenderUtility.ToXnaMatrix(perspectiveMatrixMouseUnprojection),
                        MonoGameCore.Render.RenderUtility.ToXnaMatrix(viewMatrix),
                        Microsoft.Xna.Framework.Matrix.Identity);

                nearPoint = new Vector3(nearPointXna.X, nearPointXna.Y, nearPointXna.Z);
                farPoint = new Vector3(farPointXna.X, farPointXna.Y, farPointXna.Z);
            }
        }

        private void UpdateViewMatrix()
        {
            cameraRotation = Matrix4x4.CreateRotationX(upDownRotation) * Matrix4x4.CreateRotationZ(leftRightRotation);
            cameraDefaultTarget.X = 0;
            cameraDefaultTarget.Y = 0;
            cameraDefaultTarget.Z = -1;


            cameraRotTarget = Vector3.Transform(cameraDefaultTarget, cameraRotation);

            cameraFinalTarget = coordinates + cameraRotTarget;

            cameraUp = Vector3.Transform(cameraOriginalUpVector, cameraRotation);
            cameraRight = Vector3.Transform(cameraOriginalRightVector, cameraRotation);

            this.cameraDirection = Vector3.Normalize(cameraFinalTarget - coordinates);

            viewMatrix = Matrix4x4.CreateLookAt(coordinates, cameraFinalTarget, cameraUp);
            viewMatrixMouseUnprojection = Matrix4x4.CreateLookAt(coordinates, cameraFinalTarget, cameraUp);
        }

        public void Update(int deltaMs)
        {
            this.deltaMs = deltaMs;
        }

        /*private void AddToCameraPosition(Vector3 vec)
        {
            cameraRotation = Matrix4x4.CreateRotationX(upDownRotation) * Matrix4x4.CreateRotationY(leftRightRotation);
            var rotatedVector = Vector3.Transform(vec, cameraRotation);
            coordinates += rotatedVector * cameraSpeed;

            UpdateViewMatrix();
        }*/

            // Needs fixed
        public float GetConsistentScale(System.Numerics.Vector3 position, float consistentSizePercentage)
        {
            float fov = (float)MathUtility.ToRadians(fovDegrees);
            float cameraObjectDistance = Vector3.Distance(coordinates, position);
            float worldSize = (float)((2.0f * Math.Tan(fov / 2.0f)) * cameraObjectDistance);
            float size = 0.0001f * worldSize;

            return size;
        }

        public void SetXPosition(float xPosition)
        {
            coordinates.X = xPosition;
        }

        public void SetYPosition(float yPosition)
        {
            coordinates.Y = yPosition;
        }

        public void SetZPosition(float zPosition)
        {
            coordinates.Z = zPosition;
        }

        public void SetLeftRight(float radians)
        {
            leftRightRotation = radians;

            while (leftRightRotation < 0)
            {
                leftRightRotation += (float)(Math.PI * 2.0);
            }
            while (leftRightRotation > Math.PI * 2.0)
            {
                leftRightRotation -= (float)(Math.PI * 2.0);
            }
        }

        public void RotateLeftRight(float radians)
        {
            SetLeftRight(leftRightRotation - (radians * rotationSpeedConstant * deltaMs));
        }

        public void SetUpDown(float radians)
        {
            upDownRotation = radians;

            if (upDownRotation < 0)
            {
                upDownRotation = 0;
            }
            if (upDownRotation > Math.PI)
            {
                upDownRotation = (float)Math.PI;
            }
        }

        public void RotateUpDown(float radians)
        {
            SetUpDown(upDownRotation - (radians * rotationSpeedConstant * deltaMs));
        }

        public float FindCurrentSpeed()
        {
            if (sprinting)
            {
                return cameraSpeed * 4;
            }
            else if (crawling)
            {
                return cameraSpeed * 0.25f;
            }
            return cameraSpeed;
        }

        public Vector3 GetRightDirection()
        {
            return cameraRight;
        }

        public void Translate(System.Numerics.Vector3 translation, bool applyDelta)
        {
            if (applyDelta)
            {
                Translate(translation.X * deltaMs, translation.Y * deltaMs, translation.Z * deltaMs);
            }
            else
            {
                Translate(translation.X, translation.Y, translation.Z);
            }
        }

        public void Translate(float x, float y, float z)
        {
            coordinates.X += x;
            coordinates.Y += y;
            coordinates.Z += z;
        }

        public float DistanceFromPoint(Vector3 point)
        {
            return Vector3.Distance(coordinates, point);
        }

        public float DistanceFromPoint(float x, float y, float z)
        {
            return Vector3.Distance(coordinates, new Vector3(x, y, z));
            /*float xDistance = x - coordinates.X;
            float yDistance = y - coordinates.Y;
            float zDistance = z - coordinates.Z;

            return (float)Math.Sqrt(Math.Pow(xDistance, 2) + Math.Pow(yDistance, 2) + Math.Pow(zDistance, 2));*/
        }

        public System.Numerics.Vector3 Coordinates { get => coordinates; }
        public CameraType CameraType { get => cameraType; set => cameraType = value; }
        public bool Sprinting { get => sprinting; set => sprinting = value; }
        public bool Crawling { get => crawling; set => crawling = value; }
        public bool MovingForward { get => movingForward; set => movingForward = value; }
        public bool MovingBackward { get => movingBackward; set => movingBackward = value; }
        public bool StrafingLeft { get => strafingLeft; set => strafingLeft = value; }
        public bool StrafingRight { get => strafingRight; set => strafingRight = value; }
        public Matrix4x4 ProjectionMatrix { get => projectionMatrix; set => projectionMatrix = value; }
        public Matrix4x4 ViewMatrix { get => viewMatrix; set => viewMatrix = value; }
        public ProjectionType ProjectionType { get => projectionType; set => projectionType = value; }
        public float OrthoZoom { get => orthoZoom; set { orthoZoom = value; if (orthoZoom <= 0.1f) { orthoZoom = 0.1f; } } }
        public float FovDegrees { get => fovDegrees; set => fovDegrees = value; }
        public Vector3 CameraDirection { get => cameraDirection; set => cameraDirection = value; }
        public float LeftRightRotation { get => leftRightRotation; }
        public float UpDownRotation { get => upDownRotation; }
        public Vector3 CameraUp { get => cameraUp; }
        public Vector3 CameraRight { get => cameraRight; }
        public Vector3 NearPoint { get => nearPoint; }
        public Vector3 FarPoint { get => farPoint; }
        public float Near { get => near; }
        public float Far { get => far; }
    }
}