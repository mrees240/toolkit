﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Render.Texture;
using MonoGameCore.Render.VertexDefinitions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;

namespace MonoGameCore.Render
{
    // A class used to store a vertex buffer and vertices.
    public class BufferObject : IDisposable
    {
        private IndexBuffer indexBuffer;
        private VertexBuffer vertexBuffer;
        private MonoVertex[] vertices;
        private int currentVertexId;
        private int currentIndexId;
        private int[] indices;
        private bool buffersLoaded;
        private ShaderManager shaderManager;
        private GraphicsDevice graphics;
        private ShaderContainer shaders;

        public BufferObject(GraphicsDevice graphics, ShaderContainer shaders)
        {
            this.shaders = shaders;
            this.graphics = graphics;
            vertices = new MonoVertex[0];
            indices = new int[0];
            shaderManager = new ShaderManager(shaders);
            InitializeBuffer();
        }

        public void InitializeBuffer()
        {
            Reset();
            indices = new int[ushort.MaxValue];
            vertices = new MonoVertex[ushort.MaxValue];

            indexBuffer = new IndexBuffer(graphics, IndexElementSize.ThirtyTwoBits, indices.Length, BufferUsage.WriteOnly);
            vertexBuffer = new VertexBuffer(graphics, typeof(
                MonoVertex), vertices.Length, BufferUsage.
                WriteOnly);

            UpdateBufferData();
            graphics.SetVertexBuffer(vertexBuffer);
            graphics.Indices = indexBuffer;
            buffersLoaded = true;
        }

        public void UpdateBufferData()
        {
            vertexBuffer.SetData<MonoVertex>(vertices);
            indexBuffer.SetData<int>(indices);
        }

        /// <summary>
        /// Indexed buffer
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bufferRanges"></param>
        /*public BufferObject(MonoVertex[] vertices, int[] indices)
        {
        }*/
        

        public void Reset()
        {
            currentIndexId = 0;
            currentVertexId = 0;
            Dispose();
            buffersLoaded = false;
            indexBuffer = null;
            vertexBuffer = null;
        }

        public void Draw(
            Matrix4x4 matrix,
            Camera camera,
            List<Light> lights,
            DrawState drawState,
            TextureMapSet textureMap,
            int primitiveCount,
            int baseVertex,
            int startIndex)
        {
            var currentMatrix = matrix;
            if (camera.ProjectionType == ProjectionType.Ortho)
            {
                currentMatrix = Matrix4x4.Multiply(matrix, Matrix4x4.CreateScale(camera.OrthoZoom));
            }

            var viewMat0 = camera.ViewMatrix;
            var projMat0 = camera.ProjectionMatrix;

            shaderManager.ApplyShaderVariables(graphics, projMat0, viewMat0, drawState.PrimitiveType, currentMatrix, textureMap,
                lights, drawState);

            foreach (EffectPass pass in shaders.NormalShader.Effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                graphics.DrawIndexedPrimitives(drawState.PrimitiveType, baseVertex, startIndex, primitiveCount);
            }
        }

        public void AddVertices(List<Vertex> newVertices)
        {
            for (int iVert = 0; iVert < newVertices.Count; iVert++)
            {
                var vert = newVertices[iVert];
                var monoVert = new MonoVertex();
                monoVert.position = vert.Vector.ToXnaVector3();
                monoVert.textureCoordinate = new Microsoft.Xna.Framework.Vector2(vert.U, vert.V);
                monoVert.color = new Microsoft.Xna.Framework.Vector3(vert.DiffuseColor.R, vert.DiffuseColor.G, vert.DiffuseColor.B);
                monoVert.normal = vert.Normal.ToXnaVector3();

                vertices[currentVertexId++] = monoVert;
            }
        }

        public void AddIndices(List<int> newIndices)
        {
            if (currentIndexId + newIndices.Count >= indices.Length)
            {
                var newIndexList = new int[indices.Length + newIndices.Count];
                Array.Copy(indices, newIndexList, indices.Length);
                indices = newIndexList;
            }

            for (int iIndex = 0; iIndex < newIndices.Count; iIndex++)
            {
                indices[currentIndexId++] = newIndices[iIndex];
            }
        }

        public void Dispose()
        {
            if (indexBuffer != null)
            {
                indexBuffer.Dispose();
            }
            if (vertexBuffer != null)
            {
                vertexBuffer.Dispose();
            }
        }

        public VertexBuffer VertexBuffer { get => vertexBuffer; }
        public IndexBuffer IndexBuffer { get => indexBuffer; }
        public MonoVertex[] Vertices { get => vertices; }
        public int CurrentVertexId { get => currentVertexId; }
        public int CurrentIndexId { get => currentIndexId; }
    }
}
