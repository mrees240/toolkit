﻿using MonoGameCore.Render.Texture;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render
{
    public struct BufferRange
    {
        public int primitiveCount;
        public int startIndex;
        public int baseVertex;
        public TextureMapSet textureMaps;
        public Color color;
    }
}
