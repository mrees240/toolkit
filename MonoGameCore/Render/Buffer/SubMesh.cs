﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColladaLibrary.Render;
using ColladaLibrary.Rendering;

namespace MonoGameCore.Render.Buffer
{
    /// <summary>
    /// A mesh with a single texture.
    /// </summary>
    public class SubMesh
    {
        private ColladaLibrary.Render.Mesh owner;
        private List<Polygon> polygons;
        private BitmapSet bitmapSet;

        public SubMesh(ColladaLibrary.Render.Mesh owner, List<Polygon> polygons, BitmapSet bitmapSet)
        {
            this.owner = owner;
            this.polygons = polygons;
            this.bitmapSet = bitmapSet;
        }
    }
}
