﻿using ColladaLibrary.Render;
using ColladaLibrary.Render.Lighting;
using ColladaLibrary.Utility;
using Microsoft.Xna.Framework.Graphics;
using MonoGameCore.Render.Shaders;
using MonoGameCore.Render.Texture;
using MonoGameCore.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.Buffer
{
    public class BufferManager
    {
        private GraphicsDevice graphicsDevice;
        private BufferObject bufferTris;
        private Dictionary<Mesh, List<BufferRange>> bufferRanges;
        private Dictionary<BitmapSet, TextureMapSet> textures;
        private Dictionary<TextureMapSet, List<BufferRange>> bufferRangesByTexture;

        public BufferManager(GraphicsDevice graphicsDevice, ShaderContainer shaders)
        {
            this.graphicsDevice = graphicsDevice;
            bufferRanges = new Dictionary<Mesh, List<BufferRange>>();
            textures = new Dictionary<BitmapSet, TextureMapSet>();
            bufferRangesByTexture = new Dictionary<TextureMapSet, List<BufferRange>>();
            bufferTris = new BufferObject(graphicsDevice, shaders);
        }

        private List<BufferRange> CreateBufferRanges(Mesh mesh, BufferObject buffer, int indicesPerPolygon)
        {
            //var subsTextured = mesh.GetAllPolygons().Where(p => p.Textures != null && p.Textures.Diffuse.HasBitmap).GroupBy(p => p.Textures);
            var subsColored = mesh.GetAllPolygons().Where(p => !p.Textures.Diffuse.HasBitmap);
            //var colors = mesh.GetColors();

            var ranges = new List<BufferRange>();

            var verts = mesh.Vertices;

            var coloredIndices = subsColored.SelectMany(p => p.GetIndices());
            if (coloredIndices.Any())
            {
                BufferRange range = new BufferRange();
                range.primitiveCount = subsColored.Count();
                range.color = System.Drawing.Color.Yellow;

                var indexIds = new List<int>();//indices.Select(i => i.Id).ToList();
                int currentId = 0;
                //foreach (var poly in polys)
                {
                    //var currentPolyIndices = poly.GetIndices();

                    foreach (var index in coloredIndices)
                    {
                        indexIds.Add(index.Id);
                    }
                }

                /*foreach (var index in indices)
                {
                    var vert = mesh.Vertices.ElementAt(index.Id);
                    verts.Add(vert);
                }*/

                range.startIndex = buffer.CurrentIndexId;
                buffer.AddIndices(indexIds);
                range.baseVertex = buffer.CurrentVertexId;

                ranges.Add(range);
            }

            // COLORED POLYS
            //var coloredIndices = subsColored.SelectMany(p => p.GetIndices());
            /*foreach (var color in mesh.GetColors())
            {
                var polys = subsColored.Where(p => p.di)
            }*/

            // TEXTURED POLYS
            foreach (var tex in mesh.GetTextures())
            {
                //if (bitmap.Bitmap != null)
                {
                    if (!textures.ContainsKey(tex))
                    {
                        var texture = new TextureMapSet(tex);
                        texture.Update(graphicsDevice);
                        textures.Add(tex, texture);
                        bufferRangesByTexture.Add(texture, new List<BufferRange>());
                    }
                    var polys = mesh.GetPolygonsWithDiffuseTexture(tex.Diffuse.Bitmap);

                    foreach (var poly in polys)
                    {
                        poly.CrossProduct = ColladaLibrary.Utility.RenderUtility.FindCrossProduct(mesh, poly);
                        foreach (var vert in poly.GetVertices(mesh))
                        {
                            vert.Normal = poly.CrossProduct;
                        }
                    }

                    var indices = polys.SelectMany(p => p.GetIndices());
                    if (indices.Any())
                    {
                        var range = new BufferRange();
                        range.textureMaps = textures[tex];
                        range.primitiveCount = polys.Count;
                        range.color = System.Drawing.Color.White;

                        var indexIds = new List<int>();//indices.Select(i => i.Id).ToList();
                        int currentId = 0;
                        //foreach (var poly in polys)
                        {
                            //var currentPolyIndices = poly.GetIndices();

                            foreach (var index in indices)
                            {
                                indexIds.Add(index.Id);
                            }
                        }

                        /*foreach (var index in indices)
                        {
                            var vert = mesh.Vertices.ElementAt(index.Id);
                            verts.Add(vert);
                        }*/

                        range.startIndex = buffer.CurrentIndexId;
                        buffer.AddIndices(indexIds);
                        range.baseVertex = buffer.CurrentVertexId;

                        ranges.Add(range);
                        //buffer.AddVerticesIndices(verts, indexIds);
                    }
                }
            }
            buffer.AddVertices(verts);

            return ranges;
        }

        public void Draw(List<Mesh> meshes, GraphicsDevice graphics, Camera camera, Matrix4x4 matrix, DrawState drawState, List<Light> lights)
        {
            foreach (var mesh in meshes)
            {
                if (!bufferRanges.ContainsKey(mesh))
                {
                    AddMesh(mesh);
                }
                var buffers = bufferRanges[mesh];

                foreach (var range in buffers)
                {
                    if (range.textureMaps != null)
                    {
                        if (range.textureMaps.Diffuse.Bitmap != null)
                        {
                            range.textureMaps.Update(graphics);

                            bufferTris.Draw(matrix, camera, lights, drawState, range.textureMaps, range.primitiveCount, range.baseVertex, range.startIndex);
                        }
                    }
                    else
                    {
                        bufferTris.Draw(matrix, camera, lights, drawState, range.textureMaps, range.primitiveCount, range.baseVertex, range.startIndex);
                    }
                }
            }
        }

        public void AddMesh(Mesh mesh)
        {
            if (!bufferRanges.ContainsKey(mesh))
            {
                var meshBufferRangesTri = CreateBufferRanges(mesh, bufferTris, 3);

                bufferRanges.Add(mesh, meshBufferRangesTri);

                foreach (var bufferRange in meshBufferRangesTri)
                {
                    if (bufferRange.textureMaps != null)
                    {
                        bufferRangesByTexture[bufferRange.textureMaps].Add(bufferRange);
                    }
                }
                bufferTris.UpdateBufferData();
                //bufferTris.LoadBuffers(graphicsDevice, camera, Matrix4x4.Identity, new List<Light>());
            }
        }
    }
}
