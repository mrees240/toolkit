﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MonoGameCore.Render
{
    public class RenderUtility
    {
        public static Color ToXnaColor(System.Drawing.Color color)
        {
            return new Color(color.R, color.G, color.B, color.A);
        }

        public static System.Drawing.Color ToColor(Microsoft.Xna.Framework.Vector3 color)
        {
            return System.Drawing.Color.FromArgb((int)(color.X * 255.0f), (int)(color.Y * 255.0f), (int)(color.Z * 255.0f));
        }

        public static Microsoft.Xna.Framework.Vector3 ColorToXnaVector3(System.Drawing.Color color)
        {
            return new Microsoft.Xna.Framework.Vector3(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f);
        }

        public static Microsoft.Xna.Framework.Vector4 ColorToXnaVector4(System.Drawing.Color color, float intensity)
        {
            return new Microsoft.Xna.Framework.Vector4(color.R/255.0f, color.G / 255.0f, color.B / 255.0f, intensity);
        }

        public static Microsoft.Xna.Framework.Matrix ToXnaMatrix(Matrix4x4 numericMatrix)
        {
            return new Matrix(
                numericMatrix.M11,
                numericMatrix.M12,
                numericMatrix.M13,
                numericMatrix.M14,
                numericMatrix.M21,
                numericMatrix.M22,
                numericMatrix.M23,
                numericMatrix.M24,
                numericMatrix.M31,
                numericMatrix.M32,
                numericMatrix.M33,
                numericMatrix.M34,
                numericMatrix.M41,
                numericMatrix.M42,
                numericMatrix.M43,
                numericMatrix.M44
            );
        }
    }
}
