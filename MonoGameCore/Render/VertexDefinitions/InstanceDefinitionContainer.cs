﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ColladaLibrary.Render;

namespace MonoGameCore.Render.VertexDefinitions
{
    /// <summary>
    /// Is not passed directly to shader. Is used to store values for creating InstanceDefinition instances.
    /// </summary>
    public class InstanceDefinitionContainer
    {
        private Vector4 p0;

        public InstanceDefinitionContainer()
        {
        }

        public Vector4 P0 { get => p0; set => p0 = value; }
    }
}
