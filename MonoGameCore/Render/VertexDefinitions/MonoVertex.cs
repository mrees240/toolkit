﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.VertexDefinitions
{
    public struct MonoVertex : IVertexType
    {
        public Microsoft.Xna.Framework.Vector3 position;
        public Microsoft.Xna.Framework.Vector2 textureCoordinate;
        public Microsoft.Xna.Framework.Vector3 normal;
        public Microsoft.Xna.Framework.Vector3 color;

        public VertexDeclaration VertexDeclaration
        {
            get
            {
                VertexElement[] elements = new VertexElement[4];

                int currentOffset = 0;
                // Position
                elements[0] = new VertexElement(currentOffset, VertexElementFormat.Vector3,
                            VertexElementUsage.Position, 0);

                currentOffset += (sizeof(Single) * 3);

                // Texture coordinate
                elements[1] = new VertexElement(currentOffset, VertexElementFormat.Vector2,
                        VertexElementUsage.TextureCoordinate, 0);

                currentOffset += (sizeof(Single) * 2);

                elements[2] = new VertexElement(currentOffset, VertexElementFormat.Vector3,
                        VertexElementUsage.Normal, 0);

                currentOffset += (sizeof(Single) * 3);

                elements[3] = new VertexElement(currentOffset, VertexElementFormat.Vector3,
                        VertexElementUsage.Color, 0);

                return new VertexDeclaration(elements);
            }
        }
    }
}
