﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameCore.Render.VertexDefinitions
{
    /// <summary>
    /// Stores info for hardware instance rendering. Gets directly passed to shader.
    /// </summary>
    public struct InstanceDefinition : IVertexType
    {
        public Microsoft.Xna.Framework.Vector4 p0;
        public Microsoft.Xna.Framework.Vector4 p1;
        public Microsoft.Xna.Framework.Vector4 p2;
        public Microsoft.Xna.Framework.Vector4 p3;
        public Microsoft.Xna.Framework.Vector4 dimensions;

        public VertexDeclaration VertexDeclaration
        {
            get
            {
                int clipPlaneAmount = 2;
                VertexElement[] elements = new VertexElement[5];// + clipPlaneAmount];

                int currentOffset = 0;

                elements[0] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 1);

                currentOffset += (sizeof(Single) * 4);

                elements[1] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 2);

                currentOffset += (sizeof(Single) * 4);

                elements[2] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 3);

                currentOffset += (sizeof(Single) * 4);

                elements[3] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 4);

                currentOffset += (sizeof(Single) * 4);


                elements[4] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 5);

                currentOffset += (sizeof(Single) * 4);

                /*elements[5] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 6);

                currentOffset += (sizeof(Single) * 4);

                elements[6] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 7);

                currentOffset += (sizeof(Single) * 4);

                elements[7] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 8);

                currentOffset += (sizeof(Single) * 4);*/

                /*elements[5] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 6);

                currentOffset += (sizeof(Single) * 4);*/

                /*elements[5] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 6);

                currentOffset += (sizeof(Single) * 4);*/


                /*elements[6] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 7);

                currentOffset += (sizeof(Single) * 4);

                elements[7] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 8);

                currentOffset += (sizeof(Single) * 4);

                elements[8] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 9);

                currentOffset += (sizeof(Single) * 4);

                elements[9] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                            VertexElementUsage.TextureCoordinate, 10);

                currentOffset += (sizeof(Single) * 4);*/

                // Clip Planes
                /*for (int iElement = 0; iElement < clipPlaneAmount; iElement++)
                {
                    elements[iElement + 6] = new VertexElement(currentOffset, VertexElementFormat.Vector4,
                                VertexElementUsage.TextureCoordinate, iElement + 7);

                    currentOffset += (sizeof(Single) * 4);
                }*/

                return new VertexDeclaration(elements);
            }
        }
    }
}
