﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public class ProgressManager
    {
        // Ranges from 0.0 to 1.0
        private double fileWriteProgress;

        public double FileWriteProgress { get => fileWriteProgress; set { fileWriteProgress = value; OnFileWriteProgressChanged(); } }

        public event EventHandler FileWriteProgressChangedEvent;

        public virtual void OnFileWriteProgressChanged()
        {
            if (FileWriteProgressChangedEvent != null)
            {
                FileWriteProgressChangedEvent(this, EventArgs.Empty);
            }
        }
    }
}
