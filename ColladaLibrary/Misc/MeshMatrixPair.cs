﻿using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public class MeshMatrixPair
    {
        private List<string> names;
        private List<Mesh> meshes;
        private Matrix4x4 matrix;
        private List<MeshMatrixPair> children;

        public MeshMatrixPair(Matrix4x4 matrix)
        {
            this.matrix = matrix;
            this.names = new List<string>();
            this.meshes = new List<Mesh>();
            this.children = new List<MeshMatrixPair>();
        }

        public MeshMatrixPair(Mesh mesh, Matrix4x4 matrix) : this(matrix)
        {
            AddMesh(mesh);
        }

        public MeshMatrixPair(Mesh mesh, Matrix4x4 matrix, string name) : this(matrix)
        {
            AddMesh(mesh, name);
        }

        public MeshMatrixPair(List<Mesh> meshes, Matrix4x4 matrix, List<string> names) : this(matrix)
        {
            if (meshes.Count != names.Count)
            {
                throw new Exception("Mesh matrix pair must have an equal number of names and meshes.");
            }

            for (int iMesh = 0; iMesh < meshes.Count; iMesh++)
            {
                AddMesh(meshes[iMesh], names[iMesh]);
            }
        }


        public void AddMesh(Mesh mesh)
        {
            AddMesh(mesh, "");
        }

        public void AddMesh(Mesh mesh, string name)
        {
            meshes.Add(mesh);
            names.Add(name);
        }

        public Matrix4x4 Matrix { get => matrix; }
        public List<MeshMatrixPair> Children { get => children; }
        public List<Mesh> Meshes { get => meshes; }
        public List<string> Names { get => names; }
    }
}
