﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public enum TextureQuality
    {
        HighQuality, MediumQuality, LowQuality, VeryLowQuality
    }
}
