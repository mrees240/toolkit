﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public class BoundingSphere
    {
        private Vector3 position;
        private float diameter;

        public BoundingSphere()
        {

        }
        public BoundingSphere(Vector3 position, float diameter)
        {
            this.position = position;
            this.diameter = diameter;
        }

        public Vector3 Position { get => position; set => position = value; }
        public float Diameter { get => diameter; set => diameter = value; }

        public bool ContainsPoint(Vector3 pt)
        {
            return Vector3.Distance(position, pt) < diameter;
        }
    }
}
