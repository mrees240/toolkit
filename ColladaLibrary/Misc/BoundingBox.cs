﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public class BoundingBox
    {
        private Vector3 dimensions;
        private Vector3 position;

        public BoundingBox()
        {

        }

        public BoundingBox(Vector3 dimensions, Vector3 position)
        {
            this.dimensions = dimensions;
            this.position = position;
        }

        public bool ContainsPoint(Vector3 pt)
        {
            float minX = position.X - (dimensions.X / 2.0f);
            float minY = position.Y - (dimensions.Y / 2.0f);
            float minZ = position.Z - (dimensions.Z / 2.0f);


            float maxX = position.X + (dimensions.X / 2.0f);
            float maxY = position.Y + (dimensions.Y / 2.0f);
            float maxZ = position.Z + (dimensions.Z / 2.0f);

            if (pt.X >= minX && pt.X < maxX)
            {
                if (pt.Y >= minY && pt.Y < maxY)
                {
                    if (pt.Z >= minZ && pt.Z < maxZ)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
