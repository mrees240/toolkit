﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    /// <summary>
    /// Stores a bounding box using min, max values for each axis.
    /// </summary>
    public class BoundingBoxMinMax
    {
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
        private float minZ;
        private float maxZ;

        public float MinX { get => minX; set => minX = value; }
        public float MaxX { get => maxX; set => maxX = value; }
        public float MinY { get => minY; set => minY = value; }
        public float MaxY { get => maxY; set => maxY = value; }
        public float MinZ { get => minZ; set => minZ = value; }
        public float MaxZ { get => maxZ; set => maxZ = value; }

        public Vector3 GetCenter()
        {
            return new Vector3(
                minX + (GetDistanceX() / 2.0f),
                minY + (GetDistanceY() / 2.0f),
                minZ + (GetDistanceZ() / 2.0f)
                );
        }

        private float GetDistanceX()
        {
            return Math.Abs(maxX - minX);
        }

        private float GetDistanceY()
        {
            return Math.Abs(maxY - minY);
        }

        private float GetDistanceZ()
        {
            return Math.Abs(maxZ - minZ);
        }

        public float GetDiagonalDistance()
        {
            var start = new Vector3(minX, minY, minZ);
            var end = new Vector3(maxX, maxY, maxZ);

            return Vector3.Distance(start, end);
        }

        public float GetLongestAxis()
        {
            float longestAxis = -1.0f;
            float xDist = Math.Abs(maxX - minX);
            float yDist = Math.Abs(maxY - minY);
            float zDist = Math.Abs(maxZ - minZ);

            if (xDist > longestAxis)
            {
                longestAxis = xDist;
            }
            if (yDist > longestAxis)
            {
                longestAxis = yDist;
            }
            if (zDist > longestAxis)
            {
                longestAxis = zDist;
            }

            return longestAxis;
        }
    }
}
