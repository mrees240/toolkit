﻿using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Misc
{
    public class BitmapFilePair : IEquatable<BitmapFilePair>
    {
        private string fileName;
        private Bitmap bitmap;
        private bool transparencyEnabled;
        private bool hasBitmap;

        public BitmapFilePair()
        {
            fileName = string.Empty;
        }

        public BitmapFilePair(string fileName, Bitmap bitmap) : this()
        {
            this.fileName = fileName;
            this.bitmap = bitmap;
            hasBitmap = bitmap != null;
        }

        public BitmapFilePair(BitmapFilePair original) : this()
        {
            this.fileName = original.fileName;
            this.bitmap = original.bitmap;
            this.transparencyEnabled = original.transparencyEnabled;
            hasBitmap = bitmap != null;
        }

        public event EventHandler Updated;

        public virtual void OnUpdate()
        {
            if (Updated != null)
            {
                Updated(this, EventArgs.Empty);
            }
        }

        public string FileName { get => fileName; set => fileName = value; }
        public Bitmap Bitmap { get => bitmap; set { bitmap = value; hasBitmap = bitmap != null; if (bitmap != null) { OnUpdate(); } } }
        public bool TransparencyEnabled { get => transparencyEnabled; set { transparencyEnabled = value; } }

        public bool HasBitmap { get => hasBitmap; }

        public bool Equals(BitmapFilePair other)
        {
            return this.bitmap == other.bitmap;
        }
    }
}
