﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class PolygonVertexSetPair
    {
        private Polygon polygon;
        private List<Vertex> vertices;

        public PolygonVertexSetPair(Polygon polygon, List<Vertex> vertices)
        {
            this.polygon = polygon;
            this.vertices = vertices;
        }

        public Polygon Polygon { get => polygon; }
        public List<Vertex> Vertices { get => vertices; }
    }
}
