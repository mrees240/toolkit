﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    /// <summary>
    /// Represents a basic triangle represented with three vertices.
    /// </summary>
    public class Triangle
    {
        private Vector3 v0;
        private Vector3 v1;
        private Vector3 v2;

        public Triangle()
        {
        }

        public Triangle(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            this.v0 = v0;
            this.v1 = v1;
            this.v2 = v2;
        }

        public Vector3 V0 { get => v0; set => v0 = value; }
        public Vector3 V1 { get => v1; set => v1 = value; }
        public Vector3 V2 { get => v2; set => v2 = value; }
    }
}
