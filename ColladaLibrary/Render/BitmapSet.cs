﻿using ColladaLibrary.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class BitmapSet : IEquatable<BitmapSet>
    {
        private BitmapFilePair diffuse;
        private BitmapFilePair normal;

        /// <summary>
        /// Used for storing related bitmaps such as low resolution equivalents.
        /// </summary>
        private Dictionary<TextureQuality, BitmapSet> children;

        public BitmapSet()
        {
            diffuse = new BitmapFilePair();
            normal = new BitmapFilePair();
        }

        public BitmapFilePair Diffuse { get => diffuse; set { diffuse = value; } }
        public BitmapFilePair Normal { get => normal; set => normal = value; }
        public Dictionary<TextureQuality, BitmapSet> Children { get => children; }

        public void AddChild(TextureQuality key, BitmapSet bitmap)
        {
            if (children == null)
            {
                children = new Dictionary<TextureQuality, BitmapSet>();
            }
            children.Add(key, bitmap);
        }

        public void RemoveChild(TextureQuality key)
        {
            children.Remove(key);
        }

        public bool Equals(BitmapSet other)
        {
            return other.diffuse.Bitmap == diffuse.Bitmap;
        }
    }
}
