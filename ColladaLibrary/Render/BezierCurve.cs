﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class BezierCurve
    {
        public AnchorPoint p0;
        public AnchorPoint p1;
        public AnchorPoint p2;
        public AnchorPoint p3;

        public BezierCurve()
        {
            p0 = new AnchorPoint();
            p1 = new AnchorPoint();
            p2 = new AnchorPoint();
            p3 = new AnchorPoint();
        }

        public BezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            this.p0 = new AnchorPoint(p0);
            this.p1 = new AnchorPoint(p1);
            this.p2 = new AnchorPoint(p2);
            this.p3 = new AnchorPoint(p3);
            this.p0.Children.Add(this.p1);
            this.p3.Children.Add(this.p2);
        }

        public Mesh CreateMesh(int segmentsPerSection)
        {
            var mesh = new Mesh();


            for (int iSection = 0; iSection <= segmentsPerSection; iSection++)
            {
                float percentage = (float)iSection / segmentsPerSection;
                float percentage2 = (float)(iSection + 1) / segmentsPerSection;

                var pointa = CalculateBezierPoint(percentage);
                var pointb = CalculateBezierPoint(percentage2);

                var pointVec = pointb - pointa;
                var xDist = pointb.X - pointa.X;
                var yDist = pointb.Y - pointa.Y;
                var interceptSlope = -xDist / yDist;
                if (float.IsInfinity(interceptSlope))
                {
                    interceptSlope = 0.0f;
                }

                var va_0 = new Vertex(pointa);
                var va_1 = new Vertex(pointb);
                var vb_0 = new Vertex(pointb);

                int startIndex = mesh.Vertices.Count;

                mesh.Vertices.Add(va_0);
                mesh.Vertices.Add(va_1);

                var i0 = new PolygonIndex(startIndex + 0);
                var i1 = new PolygonIndex(startIndex + 1);
                var i2 = new PolygonIndex(startIndex + 1);

                var p0 = new Polygon(i0, i1, i2);
                var bmp = new Bitmap(2, 2);
                bmp.SetPixel(0, 0, System.Drawing.Color.White);
                bmp.SetPixel(0, 1, System.Drawing.Color.White);
                bmp.SetPixel(1, 0, System.Drawing.Color.White);
                bmp.SetPixel(1, 1, System.Drawing.Color.White);
                p0.Textures.Diffuse = new Misc.BitmapFilePair("", bmp);

                mesh.AddPolygon(p0);
            }

            return mesh;
        }

        public Vector3 CalculateBezierPoint(float t)
        {
            return CalculateBezierPoint(t, p0.Point, p1.Point, p2.Point, p3.Point);
        }

        public static Vector3 CalculateBezierPoint(float t,
            Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            float u = 1.0f - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 p = uuu * p0; //first term
            p += 3 * uu * t * p1; //second term
            p += 3 * u * tt * p2; //third term
            p += ttt * p3; //fourth term

            return p;
        }

        /*public static Matrix4x4 GetMatrix(float t,
            Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            var mat = Matrix4x4.Identity;


            return mat;
        }*/

    }
}
