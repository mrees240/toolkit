﻿using ColladaLibrary.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class BillboardInfo
    {
        private float width;
        private float height;
        private BitmapFilePair texture;
        private float xOffset;
        private float yOffset;
        private float zOffset;

        public event EventHandler BillboardChanged;

        public virtual void OnBillboardChanged()
        {
            if (BillboardChanged != null)
            {
                BillboardChanged(this, EventArgs.Empty);
            }
        }

        public BillboardInfo()
        {
            texture = new BitmapFilePair();
            width = 2000;
            height = 2000;
        }

        public BillboardInfo(float width, float height, float xOffset, float yOffset, float zOffset, string textureFileName) : this()
        {
            this.width = width;
            this.height = height;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.zOffset = zOffset;
            texture.FileName = textureFileName;
            texture.Bitmap = new System.Drawing.Bitmap(texture.FileName);
        }
        
        public Vector3 GetPositionVector()
        {
            return new Vector3(xOffset, yOffset, zOffset);
        }

        public float Width { get => width; set { width = value; OnBillboardChanged(); } }
        public float Height { get => height; set { height = value; OnBillboardChanged(); } }
        public BitmapFilePair Texture { get => texture; set => texture = value; }
        public float XOffset { get => xOffset; set { xOffset = value; OnBillboardChanged(); } }
        public float YOffset { get => yOffset; set { yOffset = value; OnBillboardChanged(); } }
        public float ZOffset { get => zOffset; set { zOffset = value; OnBillboardChanged(); } }
    }
}
