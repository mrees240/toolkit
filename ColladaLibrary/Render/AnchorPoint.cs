﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class AnchorPoint
    {
        private Vector3 point;
        private List<AnchorPoint> children;

        public AnchorPoint()
        {
            children = new List<AnchorPoint>();
            point = new Vector3();
        }

        public AnchorPoint(Vector3 point) : this()
        {
            this.point = point;
        }

        public Vector3 Point { get => point; set { var translation = value - point; point = value; foreach (var child in children) { child.point += translation; } } }
        public List<AnchorPoint> Children { get => children; }
    }
}
