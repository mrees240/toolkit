﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    /// <summary>
    /// Used for dithering algorithms.
    /// Credit: https://www.cyotek.com/blog/dithering-an-image-using-the-floyd-steinberg-algorithm-in-csharp
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct ArgbColor
    {
        /// <summary>
        /// Gets the blue component value of this <see cref="ArgbColor"/> structure.
        /// </summary>
        [FieldOffset(0)]
        public byte B;

        /// <summary>
        /// Gets the green component value of this <see cref="ArgbColor"/> structure.
        /// </summary>
        [FieldOffset(1)]
        public byte G;

        /// <summary>
        /// Gets the red component value of this <see cref="ArgbColor"/> structure.
        /// </summary>
        [FieldOffset(2)]
        public byte R;

        /// <summary>
        /// Gets the alpha component value of this <see cref="ArgbColor"/> structure.
        /// </summary>
        [FieldOffset(3)]
        public byte A;

        public ArgbColor(int alpha, int red, int green, int blue)
          : this()
        {
            A = (byte)alpha;
            R = (byte)red;
            G = (byte)green;
            B = (byte)blue;
        }
    }
}
