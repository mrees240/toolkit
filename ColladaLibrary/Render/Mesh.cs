﻿using ColladaLibrary.Misc;
using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;

namespace ColladaLibrary.Render
{
    public class Mesh
    {
        private List<Vector3> vertexPositions;
        private List<Vertex> vertices;
        private List<Polygon> polygons;
        private List<PolygonIndex> polygonIndices;
        private List<BitmapSet> textures;
        private string name;
        private Vector3 center;

        public Mesh()
        {
            this.vertexPositions = new List<Vector3>();
            textures = new List<BitmapSet>();
            polygonIndices = new List<PolygonIndex>();
            vertices = new List<Vertex>();
            polygons = new List<Polygon>();
            name = string.Empty;
        }

        public Mesh(string name) : this()
        {
            this.name = name;
        }

        public Mesh(Mesh mesh) : this()
        {
            name = mesh.name;

            var indices = new List<PolygonIndex>(mesh.GetPolygonIndices());

            vertexPositions.AddRange(mesh.vertexPositions);

            foreach (var vert in mesh.vertices)
            {
                var vert2 = new Vertex(vert);
                vertices.Add(vert2);
            }
            int maxIndices = indices.Max(i => i.Id);
            int maxIndices2 = mesh.GetPolygonIndices().Max(i => i.Id);
            if (maxIndices >= vertices.Count)
            {

            }
            foreach (var poly in mesh.polygons)
            {
                var poly2 = new Polygon(poly);
                AddPolygon(poly2);
            }
            UpdateCenter();
        }

        public Mesh(string name, List<Vertex> vertices, List<Polygon> polygons) : this(name)
        {
            this.vertices = vertices;

            foreach (var poly in polygons)
            {
                AddPolygon(poly);
            }
        }

        public void Transform(Matrix4x4 matrix)
        {
            for (int iVert = 0; iVert < Vertices.Count(); iVert++)
            {
                Vertices[iVert].Vector = Vector3.Transform(Vertices[iVert].Vector, matrix);
            }
            UpdateCenter();
        }

        public void ScaleAtCenter(float scalar)
        {
            ScaleAtCenter(new Vector3(scalar));
        }

        public void ScaleAtCenter(Vector3 scalar)
        {
            var originalPosition = GetCenterPoint();
            MoveToCenter();
            Transform(Matrix4x4.CreateScale(scalar));
            Transform(Matrix4x4.CreateTranslation(originalPosition));
        }

        public void MoveToCenter()
        {
            var center = GetCenterPoint();
            var negTranslation = new Vector3(-center.X, -center.Y, -center.Z);
            Transform(Matrix4x4.CreateTranslation(negTranslation));
        }

        public List<Color> GetColors()
        {
            return polygons.SelectMany(p => p.GetAllDiffuseColors()).Distinct().ToList();
        }

        private void UpdateCenter()
        {
            var validVerts = Vertices.Where(v => v != null);

            if (validVerts.Any())
            {
                var minX = validVerts.Min(v => v.Vector.X);
                var maxX = validVerts.Max(v => v.Vector.X);
                var minY = validVerts.Min(v => v.Vector.Y);
                var maxY = validVerts.Max(v => v.Vector.Y);
                var minZ = validVerts.Min(v => v.Vector.Z);
                var maxZ = validVerts.Max(v => v.Vector.Z);

                var xLength = Math.Abs(maxX - minX);
                var yLength = Math.Abs(maxY - minY);
                var zLength = Math.Abs(maxZ - minZ);

                this.center = new Vector3(minX + xLength / 2.0f, minY + yLength / 2.0f, minZ + zLength / 2.0f);
            }
        }

        public Vector3 GetCenterPoint()
        {
            return center;
        }

        public List<Bitmap> GetDiffuseList()
        {
            List<Bitmap> bitmaps = new List<Bitmap>(polygons.Where(p => p.Textures.Diffuse.HasBitmap).Select(p => p.Textures.Diffuse.Bitmap).Distinct());


            return bitmaps;
        }

        public void Subdivide(float maxArea)
        {
            foreach (var poly in polygons)
            {
                Subdivide(maxArea, poly, poly, 0);
            }
        }

        public void ReplaceDiffuseTexture(Bitmap prevBitmap, Bitmap newBitmap)
        {
            var bitmapPolys = polygons.Where(p => p.Textures.Diffuse.Bitmap == prevBitmap);

            foreach (var poly in bitmapPolys)
            {
                poly.Textures.Diffuse.Bitmap = newBitmap;
            }
        }


        public List<Vertex> GetVertices()
        {
            if (vertices.Count() == 0)
            {
                return vertices;
            }

            var verts = new List<Vertex>();

            foreach (var poly in polygons)
            {
                foreach (var index in poly.GetIndices())
                {
                    if (index.Id < vertices.Count)
                    {
                        verts.Add(vertices[index.Id]);
                    }
                }
            }

            return verts;
        }

        public void SubdivideWithTexCoordLimit(float maxTexCoordLimit)
        {
            foreach (var poly in polygons)
            {
                SubdivideWithTexCoordLimit(maxTexCoordLimit, poly, poly, 0);
            }
        }

        /// <summary>
        /// Keeps subdividing a polygon until all texture coordinates are under the maxTexCoordLimit.
        /// </summary>
        /// <param name="maxTexCoordLimit">The texture coordinate multiplied by the size of the polygon's texture.</param>
        /// <param name="poly"></param>
        /// <param name="root"></param>
        /// <param name="iteration"></param>
        private void SubdivideWithTexCoordLimit(float maxTexCoordLimit, Polygon poly, Polygon root, int iteration)
        {
            iteration++;

            if (poly.HasThreeIndices() && poly.Textures.Diffuse.HasBitmap)
            {
                var tex = poly.Textures.Diffuse.Bitmap;
                var maxTexCoordU = poly.GetAllTextureCoordinates().Max(t => t.U);
                var minTexCoordU = poly.GetAllTextureCoordinates().Min(t => t.U);
                var maxTexCoordV = poly.GetAllTextureCoordinates().Max(t => t.V);
                var minTexCoordV = poly.GetAllTextureCoordinates().Min(t => t.V);

                float uLen = Math.Abs(maxTexCoordU - minTexCoordU) * tex.Width;
                float vLen = Math.Abs(maxTexCoordV - minTexCoordV) * tex.Height;

                bool needsSubdivision = false;

                if (maxTexCoordU > maxTexCoordLimit || maxTexCoordV > maxTexCoordLimit)
                {
                    needsSubdivision = true;
                }

                var texCoordSet = new TextureCoordSet();
                foreach (var index in poly.GetIndices())
                {
                    texCoordSet.TextureCoordinates.Add(index.TexCoord);
                }
                texCoordSet.FixTexCoords();

                if (needsSubdivision)
                {
                    poly.Subdivide(this);

                    foreach (var subPoly in poly.SubPolygons)
                    {
                        SubdivideWithTexCoordLimit(maxTexCoordLimit, subPoly, root, iteration++);
                    }
                }
                else
                {
                }
            }
        }

        public void Subdivide(float maxArea, Polygon poly, Polygon root, int iteration)
        {
            iteration++;

            if (poly.HasThreeIndices())
            {
                var sa = poly.GetSurfaceArea(vertices);

                if (sa > maxArea)
                {
                    poly.Subdivide(this);

                    foreach (var subPoly in poly.SubPolygons)
                    {
                        Subdivide(maxArea, subPoly, root, iteration++);
                    }
                }
            }
        }

        public List<Polygon> GetFinalPolygons()
        {
            var finalPolys = new List<Polygon>();

            foreach (var poly in polygons)
            {
                poly.GetFinalPolygons(finalPolys);
            }

            return finalPolys;
        }

        public List<Triangle> GetBasicTriangles()
        {
            var transformedMesh = new Mesh(this);

            var tris = new List<Triangle>();

            foreach (var polygon in transformedMesh.polygons)
            {
                var v0 = transformedMesh.vertices[polygon.GetPolygonIndex(0).Id].Vector;
                var v1 = transformedMesh.vertices[polygon.GetPolygonIndex(1).Id].Vector;
                var v2 = transformedMesh.vertices[polygon.GetPolygonIndex(2).Id].Vector;

                var tri = new Triangle(v0, v1, v2);
                tris.Add(tri);
            }

            return tris;
        }

        public List<Triangle> GetBasicTriangles(Matrix4x4 matrix)
        {
            var transformedMesh = new Mesh(this);
            transformedMesh.Transform(matrix);

            var tris = new List<Triangle>();

            foreach (var polygon in transformedMesh.polygons)
            {
                var v0 = transformedMesh.vertices[polygon.GetPolygonIndex(0).Id].Vector;
                var v1 = transformedMesh.vertices[polygon.GetPolygonIndex(1).Id].Vector;
                var v2 = transformedMesh.vertices[polygon.GetPolygonIndex(2).Id].Vector;

                var tri = new Triangle(v0, v1, v2);
                tris.Add(tri);
            }

            return tris;
        }

        public List<Vertex> Vertices { get => vertices; }
        //public List<Polygon> Polygons { get => polygons; }

        public void AddPolygon(Polygon polygon)
        {
            AddPolygon(polygon, 0);
        }

        public void AddPolygon(Polygon polygon, int idOffset)
        {
            this.polygons.Add(polygon);
            var indices = polygon.GetIndices();
            for (int iIndex = 0; iIndex < indices.Length; iIndex++)
            {
                var index = indices[iIndex];
                var polygonIndex = new PolygonIndex(index, index.Id + idOffset);
                polygonIndices.Add(polygonIndex);
                polygon.SetIndex(iIndex, polygonIndex);
                if (index.Id >= vertices.Count)
                {

                }
            }
            if (!textures.Contains(polygon.Textures))
            {
                textures.Add(polygon.Textures);
            }
        }

        public void AddPolygonRange(List<Polygon> polygons)
        {
            AddPolygonRange(polygons, 0);
        }

        public void RemoveAllPolygons()
        {
            polygons.Clear();
            polygonIndices.Clear();
        }

        public void AddPolygonRange(List<Polygon> polygons, int idOffset)
        {
            foreach (var polygon in polygons)
            {
                var poly = new Polygon(polygon);
                AddPolygon(poly, idOffset);
            }
        }

        public void RemovePolygon(Polygon polygon)
        {
            var currentIndices = polygon.GetIndices();
            foreach (var index in currentIndices)
            {
                polygonIndices.Remove(index);
            }
        }

        public void RemovePolygonAt(int index)
        {
            var poly = polygons[index];
            RemovePolygon(poly);
        }

        public int PolygonAmount
        {
            get
            {
                return polygons.Count();
            }
        }

        public List<Vector3> VertexPositions { get => vertexPositions; set => vertexPositions = value; }
        public string Name { get => name; set => name = value; }

        public Polygon GetPolygon(int index)
        {
            return polygons[index];
        }

        public List<PolygonIndex> GetPolygonIndices()
        {
            return polygonIndices;
        }

        public List<Polygon> GetPolygonsWithDiffuseTexture(Bitmap bitmap)
        {
            return polygons.Where(p => p.Textures.Diffuse.Bitmap == bitmap).ToList();
        }

        private System.Drawing.Color SolidColor(System.Drawing.Color color)
        {
            return System.Drawing.Color.FromArgb((byte)255, color.R, color.G, color.B);
        }

        public List<Polygon> GetPolygonsWithColor(Color color)
        {
            var coloredPolygons = polygons.Where(p => p.Textures.Diffuse == null && p.GetAllDiffuseColors().Contains(color)).ToList();

            return coloredPolygons;
        }

        public List<Polygon> GetAllPolygons()
        {
            return polygons;
        }

        public List<BitmapSet> GetTextures()
        {
            return textures;
        }

        public float GetBoundingSphereDiameter()
        {
            return vertices.Max(v => v.Vector.Length()) + 5.0f;
        }

        public List<Mesh> DivideMeshByTexture()
        {
            var meshes = new List<Mesh>();

            var textures = GetTextures();

            foreach (var tex in textures)
            {
                var mesh = new Mesh();
                var polys = polygons.Where(p => p.Textures.Diffuse.Bitmap == tex.Diffuse.Bitmap);
                foreach (var poly in polys)
                {
                    var newPolygon = new Polygon(poly);
                    var finalVerts = new HashSet<Vertex>();
                    poly.GetFinalVertices(finalVerts, this);
                    var indices = newPolygon.GetIndices();

                    for (int iIndex = 0; iIndex < indices.Length; iIndex++)
                    {
                        var newIndex = new PolygonIndex(indices[iIndex]);
                        var vert = new Vertex(vertices[newIndex.Id]);
                        int newIndexId = mesh.vertices.IndexOf(vert);
                        if (newIndexId < 0)
                        {
                            mesh.vertices.Add(vert);
                            newIndexId = mesh.vertices.Count - 1;
                        }
                        newIndex.Id = newIndexId;
                        newPolygon.SetIndex(iIndex, newIndex);
                    }

                    mesh.AddPolygon(newPolygon);
                }
                if (mesh.PolygonAmount > 0)
                {
                    meshes.Add(mesh);
                }
            }

            return meshes;
        }

        public List<Mesh> DivideMesh(float columnWidth)
        {
            var minX = vertices.Min(v => v.Vector.X);
            var minY = vertices.Min(v => v.Vector.Y);
            var minZ = vertices.Min(v => v.Vector.Z);
            var maxX = vertices.Max(v => v.Vector.X);
            var maxY = vertices.Max(v => v.Vector.Y);
            var maxZ = vertices.Max(v => v.Vector.Z);

            var length = Math.Abs(maxX - minX);
            var depth = Math.Abs(maxY - minY);
            var height = Math.Abs(maxZ - minZ);

            if (length < 1.0f)
            {
                length = 1.0f;
            }
            if (depth < 1.0f)
            {
                depth = 1.0f;
            }
            if (height < 1.0f)
            {
                height = 1.0f;
            }

            var boxes = CreateBoundingBoxes(length * 2, depth * 2, height * 2, columnWidth);

            var meshes = new List<Mesh>();

            foreach (var box in boxes)
            {
                var mesh = new Mesh();
                var polys = polygons.Where(p => box.ContainsPoint(p.FindCenter(this)));
                foreach (var poly in polys)
                {
                    var newPolygon = new Polygon(poly);
                    var finalVerts = new HashSet<Vertex>();
                    poly.GetFinalVertices(finalVerts, this);
                    var indices = newPolygon.GetIndices();

                    for (int iIndex = 0; iIndex < indices.Length; iIndex++)
                    {
                        var newIndex = new PolygonIndex(indices[iIndex]);
                        var vert = new Vertex(vertices[newIndex.Id]);
                        int newIndexId = mesh.vertices.IndexOf(vert);
                        if (newIndexId < 0)
                        {
                            mesh.vertices.Add(vert);
                            newIndexId = mesh.vertices.Count - 1;
                        }
                        newIndex.Id = newIndexId;
                        newPolygon.SetIndex(iIndex, newIndex);
                    }

                    mesh.AddPolygon(newPolygon);
                }
                if (mesh.PolygonAmount > 0)
                {
                    meshes.Add(mesh);
                }
            }
            return meshes;
        }

        private List<BoundingBox> CreateBoundingBoxes(float length, float depth, float height, float columnWidth)
        {
            var boxes = new List<BoundingBox>();

            float boxLength = columnWidth;
            float boxDepth = columnWidth;
            float boxHeight = columnWidth;

            float xHalf = length / 2.0f;
            float yHalf = depth / 2.0f;
            float zHalf = height / 2.0f;

            int rowAmount = (int)(length / columnWidth) + 1;
            int columnAmount = (int)(depth / columnWidth) + 1;
            int heightAmount = (int)(height / columnWidth) + 1;

            for (int row = 0; row < rowAmount; row++)
            {
                for (int col = 0; col < columnAmount; col++)
                {
                    for (int vert = 0; vert < heightAmount; vert++)
                    {
                        var x = (boxLength / 2.0f + col * boxLength) - xHalf;
                        var y = (boxDepth / 2.0f + row * boxDepth) - yHalf;
                        var z = (boxHeight / 2.0f + vert * boxHeight) - zHalf;

                        var box = new BoundingBox(new Vector3(boxLength, boxDepth, boxHeight), new Vector3(x, y, z));
                        boxes.Add(box);
                    }
                }
            }

            return boxes;
        }
    }
}
