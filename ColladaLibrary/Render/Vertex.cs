﻿using ColladaLibrary.Rendering;
using System;
using System.Drawing;
using System.Numerics;

namespace ColladaLibrary.Render
{
    /// <summary>
    ///     Defines a vertex with a texture coordinate.
    /// </summary>
    public class Vertex : IEquatable<Vertex>
    {
        public static readonly int ValuesPerVertex = 9;

        public Vertex()
        {
            this.DiffuseColor = Color.White;
            Vector = new Vector3();
        }

        public Vertex(Vector3 position) : this()
        {
            this.Vector = position;
        }

        public Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, Color color) : this()
        {
            Vector = new Vector3(x, y, z);
            Normal = new Vector3(nx, ny, nz);
            this.DiffuseColor = color;
            U = u;
            V = v;
        }

        public Vertex(Vector3 vector, Vector3 normal, Vector2 texCoord, Color color) : this(vector.X, vector.Y, vector.Z, texCoord.X, texCoord.Y, normal.X, normal.Y, normal.Z, color)
        {
        }

        /*public Vertex(float x, float y, float z) : this()
        {
            Vector = new Vector3(x, y, z);
            U = 0.0f;
            V = 0.0f;
        }

        public Vertex(Vector3 vec) : this(vec.X, vec.Y, vec.Z)
        {
        }

        public Vertex(Vector3 vec, float u, float v) : this(vec.X, vec.Y, vec.Z)
        {
            this.U = u;
            this.V = v;
        }

        public Vertex(float x, float y, float z, float u, float v) : this(x, y, z)
        {
            U = u;
            V = v;
        }

        public Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz) : this(x, y, z, u, v)
        {
            Normal = new Vector3(nx, ny, nz);
        }

        public Vertex(float x, float y, float z, float u, float v, Color color) : this(x, y, z, u, v)
        {
            this.DiffuseColor = color;
        }*/

        public TextureCoordinate GetTexCoord()
        {
            return new TextureCoordinate(U, V);
        }

        public Vector2 TexCoordVector()
        {
            return new Vector2(U, V);
        }

        public Vertex(Vertex clone)
        {
            Vector = clone.Vector;
            U = clone.U;
            V = clone.V;
            Normal = clone.Normal;
            DiffuseColor = clone.DiffuseColor;
        }

        public float U { get; set; }

        public float V { get; set; }

        public Vector3 Vector { get; set; }

        public Vector3 Normal { get; set; }

        public Color DiffuseColor { get; set; }

        public bool Near(Vertex other, float maxDist)
        {
            return Vector3.Distance(Vector, other.Vector) <= maxDist;
        }

        public bool Equals(Vertex other)
        {
            return Vector == other.Vector && U == other.U && V == other.V;
        }
    }
}
