﻿using ColladaLibrary.Rendering;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using TMLibrary.Lib.Collada.Elements.Mesh;
using System.Linq;
using System;
using ColladaLibrary.Misc;
using MathNet.Spatial.Euclidean;
using ColladaLibrary.Utility;

namespace ColladaLibrary.Render
{
    public class Polygon
    {
        protected BitmapSet textures;
        protected PolygonIndex[] indices;
        protected Material material;
        protected Vector3 crossProduct;
        protected Vector3 center;
        protected int indexAmount;
        protected bool isBillboard;

        private List<Polygon> subPolygons;

        public Polygon()
        {
            textures = new BitmapSet();
            subPolygons = new List<Polygon>();
            indexAmount = 3;
            indices = new PolygonIndex[4];

            for (int i = 0; i < indices.Length; i++)
            {
                indices[i] = new PolygonIndex(-1);
            }
        }

        public Polygon(Polygon polygon) : this()
        {
            //this.bitmap = polygon.bitmap;
            this.textures = polygon.textures;
            this.material = polygon.material;
            this.crossProduct = polygon.crossProduct;
            this.center = polygon.center;
            this.isBillboard = polygon.isBillboard;
            this.indexAmount = polygon.indexAmount;
            var prevIndices = new List<PolygonIndex>(polygon.GetIndices()).ToArray();

            for (int iIndex = 0; iIndex < prevIndices.Length; iIndex++)
            {
                var prevIndex = prevIndices[iIndex];
                var index2 = new PolygonIndex(prevIndex);
                indices[iIndex] = index2;
            }

            for (int iSubPoly = 0; iSubPoly < subPolygons.Count; iSubPoly++)
            {
                var subPoly2 = new Polygon(subPolygons[iSubPoly]);
                this.subPolygons.Add(subPoly2);
            }
        }

        public void SetAsTriangle()
        {
            indexAmount = 3;
        }

        public void SetAsQuad()
        {
            indexAmount = 4;
        }

        public double GetSurfaceArea(List<Vertex> meshVertices)
        {
            if (!subPolygons.Any())
            {
                // Henron's formula
                double a = Vector3.Distance(meshVertices[indices[0].Id].Vector, meshVertices[indices[1].Id].Vector);
                double b = Vector3.Distance(meshVertices[indices[1].Id].Vector, meshVertices[indices[2].Id].Vector);
                double c = Vector3.Distance(meshVertices[indices[2].Id].Vector, meshVertices[indices[0].Id].Vector);

                double s = (a + b + c) / 2.0;

                double A = Math.Sqrt(s * (s - a) * (s - b) * (s - c));

                return A;
            }
            else
            {
                return subPolygons.Sum(p => p.GetSurfaceArea(meshVertices));
            }
        }

        public void GetFinalPolygons(List<Polygon> finalPolygons)
        {
            if (!subPolygons.Any())
            {
                finalPolygons.Add(this);
            }
            else
            {
                foreach (var subPoly in subPolygons)
                {
                    subPoly.GetFinalPolygons(finalPolygons);
                }
            }
        }

        public Polygon(int i0, int i1, int i2) : this()
        {
            this.indices[0] = new PolygonIndex(i0);
            this.indices[1] = new PolygonIndex(i1);
            this.indices[2] = new PolygonIndex(i2);
            this.indices[3] = null;

            SetAsTriangle();
        }

        public Polygon(int i0, int i1, int i2, int i3) : this()
        {
            this.indices[0] = new PolygonIndex(i0);
            this.indices[1] = new PolygonIndex(i1);
            this.indices[2] = new PolygonIndex(i2);
            this.indices[3] = new PolygonIndex(i3);

            SetAsQuad();
        }

        public Polygon(PolygonIndex i0, PolygonIndex i1, PolygonIndex i2) : this()
        {
            this.indices[0] = i0;
            this.indices[1] = i1;
            this.indices[2] = i2;
            this.indices[3] = null;

            SetAsTriangle();
        }

        public Polygon(PolygonIndex i0, PolygonIndex i1, PolygonIndex i2, PolygonIndex i3) : this()
        {
            this.indices[0] = i0;
            this.indices[1] = i1;
            this.indices[2] = i2;
            this.indices[3] = i3;

            SetAsQuad();
        }

        public List<Vertex> GetVertices(Mesh parent)
        {
            var verts = new List<Vertex>();

            foreach (var polyIndex in GetIndices())
            {
                verts.Add(parent.Vertices[polyIndex.Id]);
            }

            return verts;
        }

        public bool IsClockwise(Mesh mesh)
        {
            var verts = GetVertices(mesh);
            var e0 = (verts[0].Vector.X - verts[1].Vector.X)*(verts[0].Vector.Y - verts[1].Vector.Y);
            var e1 = (verts[1].Vector.X - verts[2].Vector.X) * (verts[1].Vector.Y - verts[2].Vector.Y);
            var e2 = (verts[2].Vector.X - verts[0].Vector.X) * (verts[2].Vector.Y - verts[0].Vector.Y);

            return (e0 + e1 + e2) >= 0;
        }

        private float FindY(float x, float slope, float yIntercept)
        {
            return slope * x + yIntercept;
        }

        public int GetHighestIndexId()
        {
            if (indices == null || indices.Length == 0)
            {
                return -1;
            }
            return indices.Where(i => i != null).Select(i => i.Id).Max();
        }

        public List<Polygon> GetTriangulatedPolygons()
        {

            if (HasThreeIndices())
            {
                return new List<Polygon>() { this };
            }

            var p0 = new Polygon(indices[0], indices[1], indices[2]);
            var p1 = new Polygon(indices[0], indices[2], indices[3]);

            p0.textures = textures;
            p1.textures = textures;

            return new List<Polygon> { p0, p1 };
        }

        public int GetIndexAmount()
        {
            return indexAmount;
        }

        public PolygonIndex GetPolygonIndex(int index)
        {
            return indices[index];
        }

        /*public void AddPolygonIndex(PolygonIndex index)
        {
            if (indices.Count < 4)
            {
                indices.Add(index);
            }
            else
            {
                throw new System.Exception("You cannot have more than four indices per polygon.");
            }
        }

        public void RemovePolygonIndex(int index)
        {
            indices.RemoveAt(index);
        }*/

        public IEnumerable<IGrouping<Color, PolygonIndex>> GetIndicesGroupedByDiffuseColors()
        {
            return indices.GroupBy(i => i.DiffuseColor);
        }

        public void ReverseIndices()
        {
            if (HasThreeIndices())
            {
                var revIndices = new PolygonIndex[4];
                revIndices[0] = indices[2];
                revIndices[1] = indices[1];
                revIndices[2] = indices[0];
                this.indices = revIndices;
            }
            else if (HasFourIndices())
            {
                var revIndices = new PolygonIndex[4];
                revIndices[0] = indices[3];
                revIndices[1] = indices[2];
                revIndices[2] = indices[1];
                revIndices[3] = indices[0];
                this.indices = revIndices;
            }
        }

        public float FindLongestLength(Mesh parent)
        {
            var verts = GetVertices(parent);

            var l0 = Vector3.Distance(verts[0].Vector, verts[1].Vector);
            var l1 = Vector3.Distance(verts[0].Vector, verts[2].Vector);
            var l2 = Vector3.Distance(verts[1].Vector, verts[2].Vector);

            return (float)(Math.Max(Math.Max(l0, l1), l2));
        }

        public Vector3 FindCenter(Mesh parent)
        {
            var verts = GetVertices(parent);

            return new Vector3(
                (verts[0].Vector.X + verts[1].Vector.X + verts[2].Vector.X) / 3.0f,
                (verts[0].Vector.Y + verts[1].Vector.Y + verts[2].Vector.Y) / 3.0f,
                (verts[0].Vector.Z + verts[1].Vector.Z + verts[2].Vector.Z) / 3.0f
                );
        }

        //public List<PolygonIndex> Indices { get => indices; }
        public Material Material { get => material; set => material = value; }
        //public PolygonIndex[] Indices { get => indices; }
        public Vector3 CrossProduct { get => crossProduct; set => crossProduct = value; }
        public Vector3 Center { get => center; set => center = value; }
        public int IndexAmount { get => indexAmount; set => indexAmount = value; }
        public List<Polygon> SubPolygons { get => subPolygons; }
        public bool IsBillboard { get => isBillboard; set => isBillboard = value; }
        public BitmapSet Textures { get => textures; set => textures = value; }

        public void SetIndex(int id, PolygonIndex polygonIndex)
        {
            indices[id] = polygonIndex;
        }

        public bool HasThreeIndices()
        {
            return indexAmount == 3;
        }

        public bool HasFourIndices()
        {
            return indexAmount == 4;
        }

        public void GetFinalVertices(HashSet<Vertex> vertices, Mesh parent)
        {
            if (!subPolygons.Any())
            {
                var indices = GetIndices();
                var verts = GetVertices(parent);

                for (int iIndex = 0; iIndex < indices.Length; iIndex++)
                {
                    vertices.Add(verts[iIndex]);
                }
            }
            else
            {
                foreach (var subpoly in subPolygons)
                {
                    subpoly.GetFinalVertices(vertices, parent);
                }
            }
        }

        public void Subdivide(Mesh parent)
        {
            var v0 = parent.Vertices[indices[0].Id];
            var v1 = parent.Vertices[indices[1].Id];
            var v2 = parent.Vertices[indices[2].Id];

            int indexV0 = indices[0].Id;
            int indexV1 = indices[1].Id;
            int indexV2 = indices[2].Id;
            int indexV3 = parent.Vertices.Count;
            int indexV4 = parent.Vertices.Count + 1;
            int indexV5 = parent.Vertices.Count + 2;

            var localTexCoords = new TextureCoordinate[]
            {
                indices[0].TexCoord,
                indices[1].TexCoord,
                indices[2].TexCoord,
                InterpolatedTextureCoordinate(indices[0].TexCoord, indices[1].TexCoord),
                InterpolatedTextureCoordinate(indices[1].TexCoord, indices[2].TexCoord),
                InterpolatedTextureCoordinate(indices[2].TexCoord, indices[0].TexCoord),
            };

            var p0 = new Polygon(indexV0, indexV3, indexV5);
            var p1 = new Polygon(indexV3, indexV1, indexV4);
            var p2 = new Polygon(indexV4, indexV5, indexV3);
            var p3 = new Polygon(indexV5, indexV4, indexV2);

            var subPolygons = new List<Polygon>();

            subPolygons.Add(p0);
            subPolygons.Add(p1);
            subPolygons.Add(p2);
            subPolygons.Add(p3);



            var texCoords = new List<TextureCoordinate>();
            texCoords.Add(indices[0].TexCoord);
            texCoords.Add(indices[1].TexCoord);
            texCoords.Add(indices[2].TexCoord);
            if (indices[3] != null)
            {
                texCoords.Add(indices[3].TexCoord);
            }
            else
            {
                texCoords.Add(new TextureCoordinate());
            }

            /*var u_0_0 = (texCoords[0].U + texCoords[0].U) / 2.0f;
            var v_0_0 = (texCoords[0].V + texCoords[0].V) / 2.0f;
            var u_1_1 = (texCoords[1].U + texCoords[1].U) / 2.0f;
            var v_1_1 = (texCoords[1].V + texCoords[1].V) / 2.0f;
            var u_2_2 = (texCoords[2].U + texCoords[2].U) / 2.0f;
            var v_2_2 = (texCoords[2].V + texCoords[2].V) / 2.0f;
            var u_0_1 = (texCoords[0].U + texCoords[1].U) / 2.0f;
            var v_0_1 = (texCoords[0].V + texCoords[1].V) / 2.0f;
            var u_1_2 = (texCoords[1].U + texCoords[1].U) / 2.0f;
            var v_1_2 = (texCoords[1].V + texCoords[2].V) / 2.0f;
            var s_2_0 = (texCoords[2].U + texCoords[0].U) / 2.0f;
            var t_2_0 = (texCoords[2].V + texCoords[0].V) / 2.0f;*/


            var v3 = new Vertex((v0.Vector + v1.Vector) / 2.0f);
            var v4 = new Vertex((v1.Vector + v2.Vector) / 2.0f);
            var v5 = new Vertex((v2.Vector + v0.Vector) / 2.0f);

            parent.Vertices.Add(v3);
            parent.Vertices.Add(v4);
            parent.Vertices.Add(v5);

            for (int iSubPoly = 0; iSubPoly < subPolygons.Count; iSubPoly++)
            {
                var subPoly = subPolygons[iSubPoly];
                subPoly.CrossProduct = CrossProduct;
                subPoly.Material = Material;
                subPoly.textures = textures;
                if (iSubPoly == 0)
                {
                    subPoly.indices[0].TexCoord = localTexCoords[0];
                    subPoly.indices[1].TexCoord = localTexCoords[3];
                    subPoly.indices[2].TexCoord = localTexCoords[5];
                }
                else if (iSubPoly == 1)
                {
                    subPoly.indices[0].TexCoord = localTexCoords[3];
                    subPoly.indices[1].TexCoord = localTexCoords[1];
                    subPoly.indices[2].TexCoord = localTexCoords[4];
                }
                else if (iSubPoly == 2)
                {
                    subPoly.indices[0].TexCoord = localTexCoords[4];
                    subPoly.indices[1].TexCoord = localTexCoords[5];
                    subPoly.indices[2].TexCoord = localTexCoords[3];
                }
                else if (iSubPoly == 3)
                {
                    subPoly.indices[0].TexCoord = localTexCoords[5];
                    subPoly.indices[1].TexCoord = localTexCoords[4];
                    subPoly.indices[2].TexCoord = localTexCoords[2];
                }
            }
            this.subPolygons.AddRange(subPolygons);
        }

        /// <summary>
        /// Returns all indices.
        /// </summary>
        /// <returns></returns>
        public PolygonIndex[] GetAllIndices()
        {
            return indices;
        }

        public List<Polygon> GetPolygons()
        {
            var polys = new List<Polygon>();

            if (!subPolygons.Any())
            {
                return polys;
            }
            else
            {
                foreach (var subPoly in subPolygons)
                {
                    polys.AddRange(subPoly.GetPolygons());
                }
            }

            return polys;
        }

        public List<TextureCoordinate> GetAllTextureCoordinates()
        {
            return GetIndices().Select(i => i.TexCoord).ToList();
        }

        public List<Color> GetAllDiffuseColors()
        {
            return GetIndices().Select(i => i.DiffuseColor).ToList();
        }

        public PolygonIndex[] GetIndices()
        {
            return indices.Take(indexAmount).ToArray();
        }

        private TextureCoordinate InterpolatedTextureCoordinate(TextureCoordinate t0, TextureCoordinate t1)
        {
            return new TextureCoordinate((t0.U + t1.U) / 2.0f, (t0.V + t1.V) / 2.0f);
        }
    }
}
