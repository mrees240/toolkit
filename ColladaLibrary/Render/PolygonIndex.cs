﻿using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class PolygonIndex
    {
        private int id;
        private Color diffuseColor;
        private TextureCoordinate texCoord;

        public PolygonIndex(int id)
        {
            this.id = id;
            diffuseColor = Color.Orange;
            texCoord = new TextureCoordinate();
            if (this.id == 19)
            {

            }
        }

        public PolygonIndex(int id, Color diffuseColor)
        {
            this.id = id;
            this.diffuseColor = diffuseColor;
            texCoord = new TextureCoordinate();
            if (this.id == 19)
            {

            }
        }

        public PolygonIndex(int id, Color diffuseColor, TextureCoordinate texCoord)
        {
            this.id = id;
            this.diffuseColor = diffuseColor;
            this.texCoord = texCoord;
            if (this.id == 19)
            {

            }
        }

        public PolygonIndex(int id, Color diffuseColor, float u, float v)
        {
            this.id = id;
            this.diffuseColor = diffuseColor;
            this.texCoord = new TextureCoordinate(u, v);
            if (this.id == 19)
            {

            }
        }

        public PolygonIndex(PolygonIndex polygonIndex) : this(polygonIndex.id)
        {
            this.diffuseColor = polygonIndex.diffuseColor;
            this.texCoord = new TextureCoordinate(polygonIndex.texCoord);
        }

        public PolygonIndex(PolygonIndex polygonIndex, int newId) : this(newId)
        {
            this.diffuseColor = polygonIndex.diffuseColor;
            this.texCoord = new TextureCoordinate(polygonIndex.texCoord);
        }

        public int Id { get => id; set => id = value; }
        public Color DiffuseColor { get => diffuseColor; set => diffuseColor = value; }
        public TextureCoordinate TexCoord { get => texCoord; set => texCoord = value; }
    }
}
