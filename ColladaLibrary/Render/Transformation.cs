﻿using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public class Transformation
    {
        private Vector3 positionLocal;
        private Vector3 scaleLocal;
        private Vector3 rotationRadiansLocal;

        private Matrix4x4 worldMatrix;
        /// <summary>
        /// If true then the transformation is not affected by parent scene object matrices.
        /// </summary>
        private bool isIndependent;

        public Transformation()
        {
            SetLocalScale(new Vector3(1.0f));
            worldMatrix = Matrix4x4.Identity;
        }

        public event EventHandler TransformationChanged;

        public virtual void OnTransformationChanged()
        {
            if (TransformationChanged != null)
            {
                TransformationChanged(this, EventArgs.Empty);
            }
        }

        public void SetLocalPosition(Vector3 position)
        {
            this.positionLocal = position;
            OnTransformationChanged();
        }

        public void SetLocalScale(float scale)
        {
            SetLocalScale(new Vector3(scale));
            OnTransformationChanged();
        }

        public void SetLocalScale(Vector3 scale)
        {
            this.scaleLocal = scale;
            OnTransformationChanged();
        }

        public void SetLocalRadianRotationX(float xRadians)
        {
            float radDiff = xRadians - rotationRadiansLocal.X;
            rotationRadiansLocal.X = xRadians;
            OnTransformationChanged();
        }

        public void SetLocalRadianRotationY(float yRadians)
        {
            float radDiff = yRadians - rotationRadiansLocal.Y;
            rotationRadiansLocal.Y = yRadians;
            OnTransformationChanged();
        }

        public void SetLocalRadianRotationZ(float zRadians)
        {
            float radDiff = zRadians - rotationRadiansLocal.Z;
            rotationRadiansLocal.Z = zRadians;
            OnTransformationChanged();
        }

        public void SetLocalRadianRotation(Vector3 zRadians)
        {
            SetLocalRadianRotationX(zRadians.X);
            SetLocalRadianRotationY(zRadians.Y);
            SetLocalRadianRotationZ(zRadians.Z);
            OnTransformationChanged();
        }

        public void SetLocalXPosition(float x)
        {
            positionLocal = new Vector3(x, this.positionLocal.Y, this.positionLocal.Z);
            OnTransformationChanged();
        }

        public void SetLocalYPosition(float y)
        {
            positionLocal = new Vector3(positionLocal.X, y, positionLocal.Z);
            OnTransformationChanged();
        }

        public void SetLocalZPosition(float z)
        {
            positionLocal = new Vector3(positionLocal.X, positionLocal.Y, z);
            OnTransformationChanged();
        }

        /*private void UpdateLocalMatrix()
        {
            var rotMatrix = Matrix4x4.CreateFromYawPitchRoll(rotationRadiansLocal.Y, rotationRadiansLocal.X, rotationRadiansLocal.Z);

            localMatrix = Matrix4x4.Multiply(Matrix4x4.CreateScale(scaleLocal), rotMatrix);
            localMatrix = Matrix4x4.Multiply(localMatrix, Matrix4x4.CreateTranslation(positionLocal));
        }*/

        public Matrix4x4 GetLocalMatrix()
        {
            var localMatrix = Matrix4x4.Identity;
            var rotMatrix = Matrix4x4.CreateFromYawPitchRoll(rotationRadiansLocal.Y, rotationRadiansLocal.X, rotationRadiansLocal.Z);

            localMatrix = Matrix4x4.Multiply(Matrix4x4.CreateScale(scaleLocal), rotMatrix);
            localMatrix = Matrix4x4.Multiply(localMatrix, Matrix4x4.CreateTranslation(positionLocal));

            return localMatrix;
        }

        public Vector3 GetLocalRotationDegrees()
        {
            return new Vector3((float)MathUtility.ToDegrees(rotationRadiansLocal.X), (float)MathUtility.ToDegrees(rotationRadiansLocal.Y), (float)MathUtility.ToDegrees(rotationRadiansLocal.Z));
        }

        public void SetLocalRotationDegrees(Vector3 rotationDegrees)
        {
            SetLocalRadianRotationX((float)MathUtility.ToRadians(rotationDegrees.X));
            SetLocalRadianRotationY((float)MathUtility.ToRadians(rotationDegrees.Y));
            SetLocalRadianRotationZ((float)MathUtility.ToRadians(rotationDegrees.Z));
        }

        public void SetLocalMatrix(Matrix4x4 localMatrix)
        {
            var trans = new Vector3();
            var scale = new Vector3();
            var rot = new Quaternion();
            Matrix4x4.Decompose(localMatrix, out scale, out rot, out trans);
            this.positionLocal = trans;
            this.scaleLocal = scale;
            var rads = MathUtility.ToEulerRadians(rot);
            this.rotationRadiansLocal = new Vector3((float)rads.xRadians, (float)rads.yRadians, (float)rads.zRadians);
        }

        public void SetWorldMatrix(Matrix4x4 worldMatrix)
        {
            this.worldMatrix = worldMatrix;
        }

        public void SetWorldPosition(Vector3 position)
        {
            var offset = position - worldMatrix.Translation;
            PositionLocal += offset;
            OnTransformationChanged();
        }

        public Vector3 PositionLocal { get => positionLocal; set { positionLocal = value; OnTransformationChanged(); } }
        public Vector3 ScaleLocal { get => scaleLocal; }
        public Vector3 RotationRadiansLocal { get => rotationRadiansLocal; }
        public Matrix4x4 WorldMatrix
        {
            get => worldMatrix;
        }

        public bool IsIndependent { get => isIndependent; set => isIndependent = value; }
    }
}
