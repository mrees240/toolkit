﻿using ColladaLibrary.Utility;
using ColladaLibrary.Xml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ColladaLibrary.Render.Lighting
{
    public class Light
    {
        private Vector3 direction;

        public Light()
        {
            LightType = LightType.Directional;
            Color = Color.White;
            Intensity = 1.0f;
        }

        public Light(LightType lightType, Color color, float intensity, float xRotDegrees, float yRotDegrees, float zRotDegrees)
        {
            LightType = lightType;
            this.Color = color;
            this.Intensity = intensity;
            XRotDegrees = xRotDegrees;
            YRotDegrees = yRotDegrees;
            ZRotDegrees = zRotDegrees;
            UpdateDirection();
        }

        public Light(LightType lightType, Color color, float intensity) : this(lightType, color, intensity, 0.0f, 0.0f, 0.0f)
        {
        }

        public void SetLight(Light newLight)
        {
            this.LightType = newLight.LightType;
            this.Color = newLight.Color;
            this.Intensity = newLight.Intensity;
            XRotDegrees = newLight.XRotDegrees;
            YRotDegrees = newLight.YRotDegrees;
            ZRotDegrees = newLight.ZRotDegrees;
            UpdateDirection();
        }

        public void UpdateDirection()
        {
            direction = new Vector3(0.0f, 0.0f, 1.0f);
            direction = Vector3.Transform(direction, Matrix4x4.CreateRotationX((float)MathUtility.ToRadians(XRotDegrees)));
            direction = Vector3.Transform(direction, Matrix4x4.CreateRotationY((float)MathUtility.ToRadians(YRotDegrees)));
            direction = Vector3.Transform(direction, Matrix4x4.CreateRotationZ((float)MathUtility.ToRadians(ZRotDegrees)));
            direction = Vector3.Normalize(direction);
        }

        public Vector3 GetDirection()
        {
            return direction;
        }

        [XmlElement("light_type")]
        public LightType LightType { get; set; }

        [XmlElement("color",Type =typeof(XmlColor))]
        public Color Color { get; set; }
        [XmlAttribute("light_intensity")]
        public float Intensity { get; set; }
        [XmlAttribute("x_rotation_degrees")]
        public float XRotDegrees { get; set; }
        [XmlAttribute("y_rotation_degrees")]
        public float YRotDegrees { get; set; }
        [XmlAttribute("z_rotation_degrees")]
        public float ZRotDegrees { get; set; }
    }
}
