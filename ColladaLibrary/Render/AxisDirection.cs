﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Render
{
    public enum AxisDirection
    {
        Positive_X, Positive_Y, Positive_Z,
        Negative_X, Negative_Y, Negative_Z
    }
}
