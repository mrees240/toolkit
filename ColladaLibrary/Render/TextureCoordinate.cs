﻿using System.Numerics;

namespace ColladaLibrary.Rendering
{
    /// <summary>
    ///     Defines one texture coordinate used by a polygon.
    /// </summary>
    public class TextureCoordinate
    {
        private float u, v;

        public TextureCoordinate()
        {
            U = 0;
            V = 0;
        }

        public TextureCoordinate(Vector2 vec) : this()
        {
            U = vec.X;
            V = vec.Y;
            ValidateCoordinates();
        }

        public TextureCoordinate(TextureCoordinate textureCoordinate) : this()
        {
            this.u = textureCoordinate.u;
            this.v = textureCoordinate.v;
            ValidateCoordinates();
        }

        public TextureCoordinate(float u, float v)
        {
            U = u;
            V = v;

            ValidateCoordinates();
        }

        public void Scale(float scalar)
        {
            U *= scalar;
            V *= scalar;

            ValidateCoordinates();
        }

        public void SetDefaultCoordinates()
        {
            U = 0.0f;
            V = 0.0f;
        }

        public void Translate(float uOffset, float vOffset)
        {
            u += uOffset;
            v += vOffset;

            ValidateCoordinates();
        }

        public Vector2 AsVector()
        {
            return new Vector2(u, v);
        }

        private void ValidateCoordinates()
        {
            if (float.IsNaN(U))
            {
                U = 0.0f;
            }
            if (float.IsNaN(V))
            {
                V = 0.0f;
            }
        }

        public float U { get => u;set => u = value; }
        public float V { get => v; set => v = value; }
    }
}