﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace ColladaLibrary.Rendering
{
    /// <summary>
    /// Contains a list of U, V coordinates used by textures.
    /// </summary>
    public class TextureCoordSet
    {
        private List<TextureCoordinate> textureCoordinates;

        public List<TextureCoordinate> TextureCoordinates { get => textureCoordinates; }

        public TextureCoordSet()
        {
            textureCoordinates = new List<TextureCoordinate>();
        }

        public TextureCoordSet(List<TextureCoordinate> coords)
        {
            this.textureCoordinates = coords;
        }

        public void SetDefaultTextureCoordinates()
        {
            foreach (var texCoord in textureCoordinates)
            {
                texCoord.SetDefaultCoordinates();
            }
        }

        public TextureCoordSet (TextureCoordSet original) : this()
        {
            textureCoordinates.Clear();
            for (int iTex = 0; iTex < original.TextureCoordinates.Count; iTex++)
            {
                var originalCoord = original.TextureCoordinates[iTex];
                textureCoordinates.Add(new TextureCoordinate(originalCoord.U, originalCoord.V));
            }
        }

        public void Translate(float offsetU, float offsetV)
        {
            foreach (var texCoord in textureCoordinates)
            {
                texCoord.U += offsetU;
                texCoord.V += offsetV;
            }
        }

        public void Scale(float scalarU, float scalarV)
        {
            foreach (var texCoord in textureCoordinates)
            {
                texCoord.U *= scalarU;
                texCoord.V *= scalarV;
            }
        }

        public void Scale(float scalar)
        {
            Scale(scalar, scalar);
        }

        public void MirrorVertical()
        {
            Scale(1.0f, -1.0f);
            FixTexCoords();
        }

        public void MirrorHorizontal()
        {
            Scale(-1.0f, 1.0f);
            FixTexCoords();
        }

        public void Mirror()
        {
            Scale(-1.0f, -1.0f);
            FixTexCoords();
        }

        private float FindWidth()
        {
            var minU = textureCoordinates.Min(t => t.U);
            var maxU = textureCoordinates.Max(t => t.U);
            return Math.Abs(maxU - minU);
        }

        private float FindHeight()
        {
            var minV = textureCoordinates.Min(t => t.V);
            var maxV = textureCoordinates.Max(t => t.V);
            return Math.Abs(maxV - minV);
        }

        public void MirrorAcrossCenter()
        {
            Vector2 center = FindCenter();

            Translate(-center.X, -center.Y);
            Mirror();
            Translate(center.X, center.Y);
        }

        public void MirrorAcrossCenterHorizontal()
        {
            Vector2 center = FindCenter();

            Translate(-center.X, -center.Y);
            MirrorHorizontal();
            Translate(center.X, center.Y);
        }

        public void MirrorAcrossCenterVertical()
        {
            Vector2 center = FindCenter();

            Translate(-center.X, -center.Y);
            MirrorVertical();
            Translate(center.X, center.Y);
        }

        public void ScaleFromCenter(float scalarU, float scalarV)
        {
            var center = FindCenter();
            Translate(-center.X, -center.Y);
            Scale(scalarU, scalarV);
            Translate(center.X, center.Y);
        }

        public Vector2 FindTopLeftOrigin()
        {
            var minU = textureCoordinates.Min(t => t.U);
            var minV = textureCoordinates.Min(t => t.V);

            return new Vector2(minU, minV);
        }

        /// <summary>
        /// Translation relative to texture map's width and height.
        /// </summary>
        /// <param name="uOffset"></param>
        /// <param name="vOffset"></param>
        public void TranslateRelativeToScale(float uOffset, float vOffset)
        {
            var width = FindWidth();
            var height = FindHeight();

            float uTranslation = width * uOffset;
            float vTranslation = height * vOffset;
            Translate(uTranslation, vTranslation);
        }

        public void SwapUV()
        {
            foreach (var texCoord in TextureCoordinates)
            {
                var v = texCoord.V;
                texCoord.V = texCoord.U;
                texCoord.U = v;
            }
        }

        public Vector2 FindCenter()
        {
            var width = FindWidth();
            var height = FindHeight();
            var minU = textureCoordinates.Min(t => t.U);
            var minV = textureCoordinates.Min(t => t.V);

            return new Vector2(minU + (width / 2.0f), minV + (height / 2.0f));
        }

        /// <summary>
        /// Prevents negative texture coordinates.
        /// </summary>
        public void FixTexCoords()
        {
            // PREVENT NEG
            var minU = textureCoordinates.Min(t => t.U);
            var minT = textureCoordinates.Min(t => t.V);
            float uRem = (float)Math.Abs(minU % 1.0);
            float vRemainder = (float)Math.Abs(minT % 1.0);
            float uIncrements = (int)Math.Abs(Math.Floor(minU));
            float vIncrements = (int)Math.Abs(Math.Floor(minT));
            if (minU < 0)
            {
                foreach (var texCoord in textureCoordinates)
                {
                    texCoord.U += uIncrements;
                }
            }
            if (minT < 0)
            {
                foreach (var texCoord in textureCoordinates)
                {
                    texCoord.V += vIncrements;
                }
            }
            minU = textureCoordinates.Min(t => t.U);
            minT = textureCoordinates.Min(t => t.V);
            uRem = (float)Math.Abs(minU % 1.0);
            vRemainder = (float)Math.Abs(minT % 1.0);
            foreach (var texCoord in textureCoordinates)
            {
                float distS = Math.Abs(texCoord.U) - Math.Abs(minU);
                float distT = Math.Abs(texCoord.V) - Math.Abs(minT);
                texCoord.U = uRem + distS;
                texCoord.V = vRemainder + distT;
            }
        }
    }
}
