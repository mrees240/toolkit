﻿using ColladaLibrary.Extensions;
using ColladaLibrary.Render;
using MathNet.Spatial.Euclidean;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System;
using ColladaLibrary.Misc;
using System.Drawing;

namespace ColladaLibrary.Utility
{
    public class RenderUtility
    {
        private static Bitmap checkerBitmap;

        public static Bitmap GetCheckerBitmap()
        {
            if (checkerBitmap == null)
            {
                checkerBitmap = new Bitmap(2, 2);
                var col0 = System.Drawing.Color.White;
                var col1 = System.Drawing.Color.Gray;
                checkerBitmap.SetPixel(0, 0, col0);
                checkerBitmap.SetPixel(1, 0, col1);
                checkerBitmap.SetPixel(0, 1, col1);
                checkerBitmap.SetPixel(1, 1, col0);
            }

            return checkerBitmap;
        }

        public static Vector3 FindCenter(Vector3[] verts)
        {
            if (verts.Count() == 3)
            {
                return new Vector3(
                    (verts[0].X + verts[1].X + verts[2].X) / 3.0f,
                    (verts[0].Y + verts[1].Y + verts[2].Y) / 3.0f,
                    (verts[0].Z + verts[1].Z + verts[2].Z) / 3.0f
                    );
            }
            else if (verts.Count() == 4)
            {
                return new Vector3(
                    (verts[0].X + verts[1].X + verts[2].X + verts[3].X) / 4.0f,
                    (verts[0].Y + verts[1].Y + verts[2].Y + verts[3].Y) / 4.0f,
                    (verts[0].Z + verts[1].Z + verts[2].Z + verts[3].Z) / 4.0f
                    );
            }
            throw new Exception("Find center method expects 3 or 4 vertices.");
        }

        public static Vector3 FindCenter(Vector3[] verts0, Vector3[] verts1)
        {
            var c0 = new Vector3(
                (verts0[0].X + verts0[1].X + verts0[2].X) / 3.0f,
                (verts0[0].Y + verts0[1].Y + verts0[2].Y) / 3.0f,
                (verts0[0].Z + verts0[1].Z + verts0[2].Z) / 3.0f
                );
            var c1 = new Vector3(
                (verts1[0].X + verts1[1].X + verts1[2].X) / 3.0f,
                (verts1[0].Y + verts1[1].Y + verts1[2].Y) / 3.0f,
                (verts1[0].Z + verts1[1].Z + verts1[2].Z) / 3.0f
                );
            return (c0 + c1) / 2.0f;
        }

        public static System.Numerics.Plane PtDirectionPlaneToNormalForm(Vector3 pt, Vector3 n)
        {
            float nx = n.X;
            float nx0 = nx * -pt.X;
            float ny = n.Y;
            float ny0 = ny * -pt.Y;
            float nz = n.Z;
            float nz0 = nz * -pt.Z;

            float d = -nx0 - ny0 - nz0;

            var plane = new System.Numerics.Plane();
            plane.D = d;
            plane.Normal = new Vector3(nx, ny, nz);

            return plane;
        }

        public static Mesh SliceMesh(Mesh mesh, Vector3 normalDirection, float offset)
        {
            var slicePlane = new MathNet.Spatial.Euclidean.Plane(
                UnitVector3D.Create(normalDirection.X, normalDirection.Y, normalDirection.Z),
                offset);
            var newMesh = new Mesh();

            var originalPolygons = mesh.GetAllPolygons();

            int iPoly = 0;
            foreach (var poly in originalPolygons)
            {
                var indices = poly.GetIndices();

                var id0 = indices[0].Id;
                var id1 = indices[1].Id;
                var id2 = indices[2].Id;
                var id3 = indices[0].Id;
                var v0 = mesh.Vertices[id0];
                var v1 = mesh.Vertices[id1];
                var v2 = mesh.Vertices[id2];
                var v3 = mesh.Vertices[id0];

                var p0 = ToPoint3D(v0.Vector);
                var p1 = ToPoint3D(v1.Vector);
                var p2 = ToPoint3D(v2.Vector);
                var p3 = ToPoint3D(v0.Vector);


                var l0 = new Line3D(p0, p1);
                var l1 = new Line3D(p1, p2);
                var l2 = new Line3D(p2, p0);
                var l3 = new Line3D(p0, p1);

                if (indices.Length == 4)
                {
                    id3 = indices[3].Id;
                    v3 = mesh.Vertices[id3];
                    p3 = ToPoint3D(v3.Vector);
                    l3 = new Line3D(p3, p0);
                }
                var newV0 = v0;
                var newV1 = v1;
                var newV2 = v2;
                var newV3 = v3;

                var intersection0 = new Point3D?(); 
                var intersection1 = new Point3D?();
                var intersection2 = new Point3D?();
                var intersection3 = new Point3D?();

                try
                {
                    intersection0 = slicePlane.IntersectionWith(l0);
                }
                catch (Exception ex)
                {
                }
                try
                {
                    intersection1 = slicePlane.IntersectionWith(l1);
                }
                catch (Exception ex)
                {
                }
                try
                {
                    intersection2 = slicePlane.IntersectionWith(l2);
                }
                catch (Exception ex)
                {
                }
                try
                {
                    intersection3 = slicePlane.IntersectionWith(l3);
                }
                catch (Exception ex)
                {
                }

                if (intersection0 != null)
                {
                    var intersection = ToVector3(intersection0.Value);
                    var newLength = Vector3.Distance(v0.Vector, intersection);
                    var dir = Vector3.Normalize(v1.Vector - v0.Vector);
                    newV0 = new Vertex(v0.Vector + dir * newLength, new Vector3(), v0.TexCoordVector(), System.Drawing.Color.White);
                }
                if (intersection1 != null)
                {
                    var intersection = ToVector3(intersection1.Value);
                    var newLength = Vector3.Distance(v1.Vector, intersection);
                    var dir = Vector3.Normalize(v2.Vector - v1.Vector);
                    newV1 = new Vertex(v1.Vector + dir * newLength, new Vector3(), v1.TexCoordVector(), System.Drawing.Color.White);
                }
                if (intersection2 != null)
                {
                    var intersection = ToVector3(intersection2.Value);
                    var newLength = Vector3.Distance(v2.Vector, intersection);
                    var dir = Vector3.Normalize(v2.Vector - v0.Vector);
                    newV2 = new Vertex(v2.Vector + dir * newLength, new Vector3(), v2.TexCoordVector(), System.Drawing.Color.White);
                }
                if (intersection3 != null)
                {
                    var intersection = ToVector3(intersection3.Value);
                    var newLength = Vector3.Distance(v3.Vector, intersection);
                    var dir = Vector3.Normalize(v0.Vector - v3.Vector);
                    newV3 = new Vertex(v3.Vector + dir * newLength, new Vector3(), v3.TexCoordVector(), System.Drawing.Color.White);
                }


                // TEXTURE COORDINATES NEED CHANGED
                int vertexStart = newMesh.Vertices.Count;
                newMesh.Vertices.Add(newV0);
                newMesh.Vertices.Add(newV1);
                newMesh.Vertices.Add(newV2);
                if (poly.IndexAmount == 4)
                {
                    newMesh.Vertices.Add(newV3);
                }
                var newI0 = new PolygonIndex(vertexStart, indices[0].DiffuseColor, indices[0].TexCoord);
                var newI1 = new PolygonIndex(vertexStart + 1, indices[1].DiffuseColor, indices[1].TexCoord);
                var newI2 = new PolygonIndex(vertexStart + 2, indices[2].DiffuseColor, indices[2].TexCoord);


                Polygon newPolygon = null;

                if (poly.IndexAmount == 3)
                {
                    newPolygon = new Polygon(newI0, newI1, newI2);
                }
                else if (poly.IndexAmount == 4)
                {
                    var newI3 = new PolygonIndex(vertexStart + 3, indices[3].DiffuseColor, indices[3].TexCoord);
                    newPolygon = new Polygon(newI0, newI1, newI2, newI3);
                }
                else
                {
                    throw new System.Exception($"Cannot slice polygon with {poly.IndexAmount} indices.");
                }

                newPolygon.Textures = poly.Textures;
                //if (iPoly % 2 == 0)
                {
                    newMesh.AddPolygon(newPolygon);
                }
                //newMesh.AddPolygon(newPolygon);
                iPoly++;
            }

            for (int iVert = 0; iVert < mesh.Vertices.Count; iVert++)
            {
                if (mesh.Vertices[iVert] != newMesh.Vertices[iVert])
                {

                }
            }

            return newMesh;
        }

        public static Vector3 ToVector3(Point3D pt)
        {
            return new Vector3((float)pt.X, (float)pt.Y, (float)pt.Z);
        }

        public static Point3D ToPoint3D(Vector3 vec)
        {
            return new Point3D(vec.X, vec.Y, vec.Z);
        }

        public static Vector3 FindCrossProduct(Mesh mesh, Polygon polygon)
        {
            try
            {
                var v1 = mesh.Vertices[polygon.GetPolygonIndex(0).Id];
                var v2 = mesh.Vertices[polygon.GetPolygonIndex(1).Id];
                var v3 = mesh.Vertices[polygon.GetPolygonIndex(2).Id];
                var diff1 = Vector3.Subtract(v3.Vector, v1.Vector);
                var diff2 = Vector3.Subtract(v2.Vector, v1.Vector);
                var crossProduct = Vector3.Cross(diff1, diff2);
                crossProduct = crossProduct.Normalize();
                return crossProduct;
            }
            catch (Exception ex)
            {
                return new Vector3();
            }
        }

        public static Vector3 FindCrossProduct(Vector3[] vertices)
        {
            return FindCrossProduct(vertices[0], vertices[1], vertices[2]);
        }

        public static Vector3 FindCrossProduct(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            var diff1 = Vector3.Subtract(v3, v1);
            var diff2 = Vector3.Subtract(v2, v1);
            var crossProduct = Vector3.Cross(diff1, diff2);

            if (crossProduct.Length() == 0)
            {
                crossProduct = diff1;
            }

            crossProduct = crossProduct.Normalize();
            return crossProduct;
        }

        public static Mesh MergeMeshes(List<Mesh> meshes)
        {
            var mesh = new Mesh();

            foreach (var innerMesh in meshes)
            {
                int offset = 0;
                if (mesh.PolygonAmount > 0)
                {
                    offset = mesh.Vertices.Count;
                }
                mesh.Vertices.AddRange(innerMesh.Vertices);
                var prevPolys = innerMesh.GetAllPolygons();
                mesh.AddPolygonRange(prevPolys, offset);
            }

            return mesh;
        }

        public static Mesh CreateBillboardMesh(BitmapFilePair texture)
        {
            return CreateBillboardMesh(texture, 1.0f, 1.0f);
        }

        public static Mesh CreateBillboardMesh(BitmapFilePair texture, float width, float height)
        {
            var mesh = new Mesh();

            var halfWidth = width / 2.0f;
            var halfHeight = height / 2.0f;

            var position = new Vector3();

            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(-halfWidth + position.X, position.Y,
                -halfHeight + position.Z, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(halfWidth + position.X, position.Y,
                -halfHeight + position.Z, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(halfWidth + position.X, position.Y,
                halfHeight + position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));
            mesh.Vertices.Add(new ColladaLibrary.Render.Vertex(-halfWidth + position.X, position.Y,
                halfHeight + position.Z, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, System.Drawing.Color.White));

            var i0 = new PolygonIndex(0, System.Drawing.Color.White, new Rendering.TextureCoordinate(0.0f, 0.0f));
            var i1 = new PolygonIndex(1, System.Drawing.Color.White, new Rendering.TextureCoordinate(0.0f, 0.0f));
            var i2 = new PolygonIndex(2, System.Drawing.Color.White, new Rendering.TextureCoordinate(0.0f, 0.0f));
            var i3 = new PolygonIndex(3, System.Drawing.Color.White, new Rendering.TextureCoordinate(0.0f, 0.0f));

            var poly = new Polygon(i0, i1, i2);
            var poly2 = new Polygon(i3, i2, i0);
            poly.Textures.Diffuse = texture;
            poly2.Textures.Diffuse = texture;

            mesh.AddPolygon(poly);
            mesh.AddPolygon(poly2);

            return mesh;
        }
    }
}
