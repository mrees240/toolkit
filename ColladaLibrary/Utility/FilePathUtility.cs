﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Utility
{
    public class FilePathUtility
    {
        public static string GetSafeFileName(string fileUrl)
        {
            fileUrl = fileUrl.Replace('/', '\\');

            return fileUrl.Split('\\').Last();
        }
    }
}
