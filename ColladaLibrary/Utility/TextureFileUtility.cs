﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ColladaLibrary.Utility
{
    public class TextureFileUtility
    {
        public static Bitmap BitmapFromSource(BitmapSource bitmapSource)
        {
            using (var stream = new MemoryStream())
            {
                return BitmapFromSource(stream, bitmapSource, bitmapSource.PixelWidth, bitmapSource.PixelHeight);
            }
        }

        public static Bitmap CreateSolidColorBitmap(Color color)
        {
            var bmp = new Bitmap(2, 2);

            bmp.SetPixel(0, 0, color);
            bmp.SetPixel(0, 1, color);
            bmp.SetPixel(1, 0, color);
            bmp.SetPixel(1, 1, color);

            return bmp;
        }

        public static Bitmap BitmapFromSource(Stream stream, BitmapSource bitmapSource, int width, int height)
        {
            BitmapEncoder enc = new BmpBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create(bitmapSource));
            enc.Save(stream);
            var bitmap = new Bitmap(stream).Clone(
                new Rectangle(0, 0, width, height),
                PixelFormat.Format8bppIndexed);
            return bitmap;
        }

        public static void WriteBitmapFile(string fileName, BitmapSource bitmapSource, int width, int height)
        {
            using (var outStream = new MemoryStream())
            {
                var bitmap = BitmapFromSource(outStream, bitmapSource, width, height);
                bitmap.Save(fileName, ImageFormat.Bmp);
                bitmap.Dispose();
            }
        }

        public static void WriteBitmapFile(string fileName, Bitmap bitmap)
        {
            using (var outStream = new MemoryStream())
            {
                var clone = new Bitmap(bitmap);
                clone.Save(fileName, ImageFormat.Bmp);
                clone.Dispose();
            }
        }
    }
}
