﻿using ColladaLibrary.Images.Dithering;
using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Utility
{
    /// <summary>
    /// Used for dithering algorithms.
    /// Credit: https://www.cyotek.com/blog/dithering-an-image-using-the-floyd-steinberg-algorithm-in-csharp
    /// </summary>
    public class ImageDitherUtility
    {
        public static void ApplyDitherToImage(Bitmap image)
        {
            ArgbColor[] originalData;
            Size size;
            IErrorDiffusion dither = new FloydSteinbergDithering();

            size = image.Size;

            originalData = image.GetPixelsFromBitArgbImage();

            var colors = originalData.Distinct();

            if (colors.Count() > 256)
            {
                for (int row = 0; row < size.Height; row++)
                {
                    for (int col = 0; col < size.Width; col++)
                    {
                        int index;
                        ArgbColor current;
                        ArgbColor transformed;

                        index = row * size.Width + col;

                        current = originalData[index];

                        // transform the pixel - normally this would be some form of color
                        // reduction. For this sample it's simple threshold based
                        // monochrome conversion
                        //transformed = this.TransformPixel(current);
                        transformed = originalData[index];

                        // apply a dither algorithm to this pixel
                        if (dither != null)
                        {
                            dither.Diffuse(originalData, current, transformed, col, row, size.Width, size.Height);
                        }
                    }
                }
            }
        }
    }
}
