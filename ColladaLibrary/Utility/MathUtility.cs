﻿using ColladaLibrary.MathHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Utility
{
    public class MathUtility
    {
        public static double ToRadians(double degrees)
        {
            return (Math.PI / 180.0) * degrees;
        }

        public static double ToDegrees(double radians)
        {
            return radians * 180.0 / Math.PI;
        }

        public static Matrix4x4 ScaleMatrixAtMatrixCenter(Matrix4x4 originalMat, float scale)
        {
            return ScaleMatrixAtMatrixCenter(originalMat, new System.Numerics.Vector3(scale));
        }

        public static Matrix4x4 ScaleMatrixAtMatrixCenter(Matrix4x4 originalMat, System.Numerics.Vector3 scale)
        {
            var pos = new System.Numerics.Vector3();
            var originalScale = new System.Numerics.Vector3();
            var rot = new System.Numerics.Quaternion();

            Matrix4x4.Decompose(originalMat, out originalScale, out rot, out pos);

            var scaleMat = Matrix4x4.CreateScale(originalScale * scale);
            var rotMat = Matrix4x4.CreateFromQuaternion(rot);
            var posMat = Matrix4x4.CreateTranslation(pos);

            var finalMat = scaleMat;
            finalMat = Matrix4x4.Multiply(finalMat, rotMat);
            finalMat = Matrix4x4.Multiply(finalMat, posMat);

            return finalMat;
        }

        public static float GetZRadianFromNormal(Vector3 normal)
        {
            return (float)Math.Atan2(normal.Y, normal.X);
        }

        public static float GetY(Vector2 point1, Vector2 point2, float x)
        {
            var dx = point2.X - point1.X;
            if (dx == 0)
            {
                return float.NaN;
            }
            var m = (point2.Y - point1.Y) / dx;
            var b = point1.Y - (m * point1.X);

            return m * x + b;
        }

        public static double Copysign(double val0, double val1)
        {
            if (val1 >= 0.0f)
            {
                return val0;
            }
            else
            {
                return -val0;
            }
        }

        /// <summary>
        /// Returns the rotation in radians for the x, y, z axis.
        /// </summary>
        /// <returns></returns>
        public static RadianRotation ToEulerRadians(Quaternion q)
        {
            var qx = q.X;
            var qy = q.Y;
            var qz = q.Z;
            var qw = q.W;

            var qx2 = qx * qx;
            var qy2 = qy * qy;
            var qz2 = qz * qz;

            var heading = Math.Atan2(2 * qy * qw - 2 * qx * qz, 1 - 2 * qy2 - 2 * qz2);
            var attitude = Math.Asin(2 * qx * qy + 2 * qz * qw);
            var bank = Math.Atan2(2 * qx * qw - 2 * qy * qz, 1 - 2 * qx2 - 2 * qz2);

            RadianRotation rotation;
            if (double.IsNaN(bank))
            {
                bank = 0;
            }
            if (double.IsNaN(heading))
            {
                heading = 0;
            }
            if (double.IsNaN(attitude))
            {
                attitude = 0;
            }
            rotation.xRadians = bank;
            rotation.yRadians = heading;
            rotation.zRadians = attitude;

            return rotation;
        }
    }
}
