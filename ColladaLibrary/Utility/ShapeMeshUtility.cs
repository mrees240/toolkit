﻿using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Utility
{
    /// <summary>
    /// Provides methods for easily creating shapes.
    /// </summary>
    public class ShapeMeshUtility
    {
        public static Mesh CreateCube(float length, float width, float height, Bitmap texture)
        {
            var cube = new Mesh();

            float halfLen = length / 2.0f;
            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;

            var v0 = new Vector3(-halfLen, halfWidth, halfHeight);
            var v1 = new Vector3(halfLen, halfWidth, halfHeight);
            var v2 = new Vector3(-halfLen, -halfWidth, halfHeight);
            var v3 = new Vector3(halfLen, -halfWidth, halfHeight);

            var t0 = new TextureCoordinate(0, 0);
            var t1 = new TextureCoordinate(1,0);
            var t2 = new TextureCoordinate(0, 1);
            var t3 = new TextureCoordinate(1, 1);

            cube.Vertices.Add(new Vertex(v0));
            cube.Vertices.Add(new Vertex(v1));
            cube.Vertices.Add(new Vertex(v2));
            cube.Vertices.Add(new Vertex(v3));

            var p0 = new Polygon(0, 1, 2);
            var p1 = new Polygon(2, 1, 3);

            var n0 = RenderUtility.FindCrossProduct(cube, p0);
            var n1 = RenderUtility.FindCrossProduct(cube, p1);
            p0.Textures.Diffuse.Bitmap = texture;
            p1.Textures.Diffuse.Bitmap = texture;

            cube.AddPolygon(p0);
            cube.AddPolygon(p1);

            cube.Vertices[0].U = t0.U;
            cube.Vertices[0].V = t0.V;
            cube.Vertices[1].U = t1.U;
            cube.Vertices[1].V = t1.V;
            cube.Vertices[2].U = t2.U;
            cube.Vertices[2].V = t2.V;
            cube.Vertices[3].U = t3.U;
            cube.Vertices[3].V = t3.V;

            return cube;
        }

        public static Mesh CreateCylinder(Vector3 start, Vector3 end, float startRadius, float endRadius, int segments, Bitmap tex)
        {
            var mesh = new Mesh();

            var direction = Vector3.Normalize(end - start);
            var length = Vector3.Distance(start, end);
            // xrot
            if (length == 0)
            {
                length = 1;
            }
            //var distZ = Math.Abs(start.Z - end.Z);
            //var xRot = Math.Asin(distZ / length);

            var right = new Vector3(1.0f, 0.0f, 0.0f);
            //var right = new Vector3(-direction.Y, direction.X, end.Z);
            //var up = Vector3.Cross(direction, right);

            var capVertices = new List<Vertex>();

            for (int iSegment = 0; iSegment <= segments; iSegment++)
            {
                float radians = (float)(((float)iSegment / (float)segments) * (Math.PI * 2));
                var v0 = Vector3.Transform(right, Matrix4x4.CreateFromAxisAngle(direction, radians)) * startRadius;
                var v1 = Vector3.Transform(right, Matrix4x4.CreateFromAxisAngle(direction, radians)) * endRadius;
                v1 = v1 + direction * length;
                capVertices.Add(new Vertex(v0));
                capVertices.Add(new Vertex(v1));
            }

            // 3 vert = 1 tri
            // 4 Vert = 2 tri
            // 5 vert = 3 tri

            /*int capTriangleCount = segments - 2;
            int[] capIndices0 = new int[capTriangleCount * 3];
            for (int iTri = 0; iTri < capTriangleCount; iTri++)
            {
                int i0 = iTri * 4;
                int i1 = i0 + 2;
                int i2 = i0 + 4;
                capIndices0[iTri * 3 + 0] = i0;
                capIndices0[iTri * 3 + 1] = i1;
                capIndices0[iTri * 3 + 2] = i2;
            }*/

            mesh.Vertices.AddRange(capVertices);

            int vertStride = mesh.Vertices.Count / 2;

            var diffuse = new BitmapFilePair("", tex);

            // Create polygons
            for (int iVert = 0; iVert < segments * 2; iVert += 2)
            {
                var i0 = new PolygonIndex(iVert, System.Drawing.Color.White);
                var i1 = new PolygonIndex(i0.Id + 1, System.Drawing.Color.White);
                var i2 = new PolygonIndex(i0.Id + 2, System.Drawing.Color.White);
                var i3 = new PolygonIndex(i0.Id + 3, System.Drawing.Color.White);
                var polygon = new Polygon(i0, i2, i1);
                mesh.AddPolygon(polygon);
                polygon.Textures.Diffuse = diffuse;


                var polygon2 = new Polygon(i2, i3, i1);
                polygon2.Textures.Diffuse = diffuse;
                mesh.AddPolygon(polygon2);
            }
            for (int iPoly = 0; iPoly < mesh.PolygonAmount; iPoly++)
            {
                var poly = mesh.GetPolygon(iPoly);
                poly.CrossProduct = RenderUtility.FindCrossProduct(mesh, poly);
            }
            // Create cap polygons
            //int indexOffset = mesh.GetPolygonIndices().Max(i => i.Id) + 1;
            /*var prevIndices = mesh.GetPolygonIndices();
            for (int iCapTri = 0; iCapTri < capTriangleCount; iCapTri++)
            {
                int index0 = capIndices0[iCapTri * 3 + 0];
                int index1 = capIndices0[iCapTri * 3 + 1];
                int index2 = capIndices0[iCapTri * 3 + 2];
                var i0 = prevIndices[index0];
                var i1 = prevIndices[index1];
                var i2 = prevIndices[index2];
                var polygon = new Polygon(i0, i1, i2);
                mesh.AddPolygon(polygon);
                polygon.Textures.Diffuse = diffuse;
                polygon.CrossProduct = RenderUtility.FindCrossProduct(mesh, polygon);
            }*/

            return mesh;
        }
    }
}
