﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.MathHelper
{
    public struct RadianRotation
    {
        public double xRadians;
        public double yRadians;
        public double zRadians;

        public Vector3 AsVector()
        {
            return new Vector3((float)xRadians, (float)yRadians, (float)zRadians);
        }
    }
}
