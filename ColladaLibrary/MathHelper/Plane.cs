﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.MathHelper
{
    public class Plane
    {
        private float x, y, z, d;

        public bool PointExistsWithinPlane(Vector3 pt)
        {
            return pt.X * x + pt.Y * y + pt.Z * z - d < 0;
        }

        public Plane(float x, float y, float z, float d)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.d = d;
        }

        public Plane(Vector3 pt, Vector3 n)
        {
            //nx(x - x0) + ny(y - y0) + nz(z - z0) = 0
            float nx = n.X;
            float nx0 = n.X * -pt.X;
            float ny = n.Y;
            float ny0 = n.Y * -pt.Y;
            float nz = n.Z;
            float nz0 = n.Z * -pt.Z;

            float d = -nx0 - ny0 - nz0;

            this.x = nx;
            this.y = ny;
            this.z = nz;
            this.d = d;
        }

        /*public static bool ExistsWithinTwoPlanes(Vector3 pt, NormalPlane p0, NormalPlane p1)
        {
            bool existsInPlaneOne = p0.PointExistsWithinPlane(pt);
            bool existsInPlaneTwo = p1.PointExistsWithinPlane(pt);

            return existsInPlaneOne && existsInPlaneTwo;
        }*/

        public float X { get => x; set => x = value; }
        public float Y { get => y; set => y = value; }
        public float Z { get => z; set => z = value; }
        public float D { get => d; set => d = value; }
    }
}
