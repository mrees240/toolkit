﻿using System.Numerics;

namespace ColladaLibrary.Extensions
{
    public static class MathExtensions
    {
        public static Vector3 GetScale(this Matrix4x4 mat)
        {
            return new Vector3(mat.M11, mat.M22, mat.M33);
        }

        public static Vector3 Normalize(this Vector3 vec)
        {
            var length = vec.Length();
            vec /= length;
            return vec;
        }
    }
}
