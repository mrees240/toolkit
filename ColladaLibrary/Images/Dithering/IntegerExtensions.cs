﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Images.Dithering
{
    /// <summary>
    /// Used for dithering algorithms.
    /// Credit: https://www.cyotek.com/blog/dithering-an-image-using-the-floyd-steinberg-algorithm-in-csharp
    /// </summary>
    internal static class IntegerExtensions
    {
        #region Static Methods

        internal static byte ToByte(this int value)
        {
            if (value < 0)
            {
                value = 0;
            }
            else if (value > 255)
            {
                value = 255;
            }

            return (byte)value;
        }

        #endregion
    }
}
