﻿using ColladaLibrary.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColladaLibrary.Images.Dithering
{
    /// <summary>
    /// Used for dithering algorithms.
    /// Credit: https://www.cyotek.com/blog/dithering-an-image-using-the-floyd-steinberg-algorithm-in-csharp
    /// </summary>
    public interface IErrorDiffusion
    {
        #region Methods

        void Diffuse(ArgbColor[] original, ArgbColor originalPixel, ArgbColor transformedPixel, int x, int y, int width, int height);

        #endregion
    }
}
