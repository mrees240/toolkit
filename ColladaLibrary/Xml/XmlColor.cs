﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ColladaLibrary.Xml
{
    public class XmlColor
    {
        private Color color = Color.Black;

        public XmlColor() { }
        public XmlColor(Color c) { color = c; }

        public Color ToColor()
        {
            return color;
        }

        public void FromColor(Color color)
        {
            this.color = color;
        }

        public static implicit operator Color(XmlColor x)
        {
            return x.ToColor();
        }

        public static implicit operator XmlColor(Color c)
        {
            return new XmlColor(c);
        }

        [XmlAttribute("red")]
        public byte Red
        {
            get { return color.R; }
            set
            {
                color = Color.FromArgb(color.A, value, color.G, color.B);
            }
        }

        [XmlAttribute("green")]
        public byte Green
        {
            get { return color.G; }
            set
            {
                color = Color.FromArgb(color.A, color.R, value, color.B);
            }
        }

        [XmlAttribute("blue")]
        public byte Blue
        {
            get { return color.B; }
            set
            {
                color = Color.FromArgb(color.A, color.R, color.G, value);
            }
        }
    }
}
