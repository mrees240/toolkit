﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ColladaLibrary.Xml
{
    public class XmlMatrix
    {
        private Matrix4x4 matrix;

        public XmlMatrix() { matrix = Matrix4x4.Identity; }
        public XmlMatrix(Matrix4x4 matrix) { this.matrix = matrix; }

        public Vector3 Translation
        {
            get
            {
                return matrix.Translation;
            }
        }

        public Matrix4x4 ToMatrix()
        {
            return matrix;
        }

        public void FromColor(Matrix4x4 matrix)
        {
            this.matrix = matrix;
        }

        public static implicit operator Matrix4x4(XmlMatrix m)
        {
            return m.ToMatrix();
        }

        public static implicit operator XmlMatrix(Matrix4x4 m)
        {
            return new XmlMatrix(m);
        }

        [XmlAttribute("value")]
        public string MatrixText
        {
            get { return MatrixToString(matrix); }
            set { this.matrix = TextToMatrix(value); }
        }

        private Matrix4x4 TextToMatrix(string text)
        {
            var values = text.Split(',');

            var matrix = Matrix4x4.Identity;

            matrix.M11 = float.Parse(values[0]);
            matrix.M12 = float.Parse(values[1]);
            matrix.M13 = float.Parse(values[2]);
            matrix.M14 = float.Parse(values[3]);

            matrix.M21 = float.Parse(values[4]);
            matrix.M22 = float.Parse(values[5]);
            matrix.M23 = float.Parse(values[6]);
            matrix.M24 = float.Parse(values[7]);

            matrix.M31 = float.Parse(values[8]);
            matrix.M32 = float.Parse(values[9]);
            matrix.M33 = float.Parse(values[10]);
            matrix.M34 = float.Parse(values[11]);

            matrix.M41 = float.Parse(values[12]);
            matrix.M42 = float.Parse(values[13]);
            matrix.M43 = float.Parse(values[14]);
            matrix.M44 = float.Parse(values[15]);

            return matrix;
        }

        private string MatrixToString(Matrix4x4 matrix)
        {
            return
                $"{matrix.M11},{matrix.M12},{matrix.M13},{matrix.M14}," +
                $"{matrix.M21},{matrix.M22},{matrix.M23},{matrix.M24}," +
                $"{matrix.M31},{matrix.M32},{matrix.M33},{matrix.M34}," +
                $"{matrix.M41},{matrix.M42},{matrix.M43},{matrix.M44}";
        }
    }
}
