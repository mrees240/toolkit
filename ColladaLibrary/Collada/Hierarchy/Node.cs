﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMLibrary.Lib.Collada.Elements.Lighting;
using TMLibrary.Lib.Collada.PseudoElements;
using System.Numerics;

namespace TMLibrary.Lib.Collada.Hierarchy
{
    public class Node : IComparable<Node>
    {
        private float boundingSphereRadius;
        private string name;
        private List<int> meshIdList;
        private List<Node> children;
        private Matrix4x4 matrix;
        private List<Light> lights;
        private List<MeshNode> meshes;
        private Node parent;

        public Node(String name, Matrix4x4 matrix, Node parent)
        {
            this.parent = parent;
            lights = new List<Light>();
            this.matrix = matrix;
            this.name = name;
            children = new List<Node>();
            meshIdList = new List<int>();
            meshes = new List<MeshNode>();
        }
        
        public Node Parent { get; set; }

        public Matrix4x4 GetStackedMatrix()
        {
            var currentParent = Parent;

            while (currentParent != null)
            {
                matrix = Matrix4x4.Multiply(matrix, currentParent.Matrix);
                currentParent = currentParent.Parent;
            }

            return matrix;
        }

        /// <summary>
        /// Ordered from biggest radius to smallest.
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        int IComparable<Node>.CompareTo(Node y)
        {
            float xRadius = BoundingSphereRadius;
            float yRadius = y.BoundingSphereRadius;
            if (xRadius > yRadius)
            {
                return -1;
            }
            else if (yRadius > xRadius)
            {
                return 1;
            }
            return 0;
        }

        public Vector3 FindCenterPoint()
        {
            float centerX = 0;
            float centerY = 0;
            float centerZ = 0;

            foreach (var mesh in meshes)
            {
                var center = mesh.Mesh.GetCenterPoint();
                centerX += center.X;
                centerY += center.Y;
                centerZ += center.Z;
            }
            if (meshes.Count() > 0)
            {
                centerX /= meshes.Count();
                centerY /= meshes.Count();
                centerZ /= meshes.Count();
            }

            return Vector3.Add(matrix.Translation, new Vector3(centerX, centerY, centerZ));
        }

        public List<Node> Children
        {
            get { return children; }
        }


        public string Name
        {
            get { return name; }
        }

        public void Scale(float scalarX, float scalarY, float scalarZ)
        {
            var pos = matrix.Translation;
            matrix = Matrix4x4.Multiply(matrix, Matrix4x4.CreateTranslation(-pos));
            matrix = Matrix4x4.Multiply(matrix, Matrix4x4.CreateScale(scalarX, scalarY, scalarZ));
            matrix = Matrix4x4.Multiply(matrix, Matrix4x4.CreateTranslation(pos));
        }

        //public InstructionMatrix Matrix { get => matrix; set => matrix = value; }
        public List<Light> Lights { get => lights; set => lights = value; }

        public List<int> MeshIdList { get => meshIdList; set => meshIdList = value; }
        public float BoundingSphereRadius { get => boundingSphereRadius; set => boundingSphereRadius = value; }
        public List<MeshNode> MeshNodes { get => meshes; }
        public Matrix4x4 Matrix { get => matrix; set => matrix = value; }
    }
}
