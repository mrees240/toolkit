﻿using System.Numerics;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class ColladaMatrix
    {
        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlText]
        public string Value { get; set; }

        public Matrix4x4 GetMatrix()
        {
            if (Value == null)
            {
                return Matrix4x4.Identity;
            }

            var values = Value.Split(' ');

            var matrix = Matrix4x4.Identity;

            matrix.M11 = float.Parse(values[0]);
            matrix.M21 = float.Parse(values[1]);
            matrix.M31 = float.Parse(values[2]);
            matrix.M41 = float.Parse(values[3]);

            matrix.M12 = float.Parse(values[4]);
            matrix.M22 = float.Parse(values[5]);
            matrix.M32 = float.Parse(values[6]);
            matrix.M42 = float.Parse(values[7]);

            matrix.M13 = float.Parse(values[8]);
            matrix.M23 = float.Parse(values[9]);
            matrix.M33 = float.Parse(values[10]);
            matrix.M43 = float.Parse(values[11]);

            matrix.M14 = float.Parse(values[12]);
            matrix.M24 = float.Parse(values[13]);
            matrix.M34 = float.Parse(values[14]);
            matrix.M44 = float.Parse(values[15]);

            return matrix;
        }
    }
}