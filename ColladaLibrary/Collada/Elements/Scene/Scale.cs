﻿using System;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class Scale
    {
        public Scale()
        {
            Sid = string.Empty;
            Value = string.Empty;
        }

        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlText]
        public string Value { get; set; }

        public double[] GetValues()
        {
            string[] splits = Value.Split(' ');
            double[] values = new double[splits.Length];

            for (int iVal = 0; iVal < splits.Length; iVal++)
            {
                try
                {
                    values[iVal] = double.Parse(splits[iVal]);
                }
                catch (Exception ex)
                {
                    values[iVal] = 1.0;
                }
            }

            return values;
        }
    }
}
