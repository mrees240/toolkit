﻿using ColladaLibrary.Collada.Elements.Scene;
using System;
using System.Xml;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class Asset
    {
        public Asset()
        {
            UpAxis = "Z_UP";
            Contributor = new Contributor();
            Unit = new Unit();
        }

        [XmlElement("contributor")]
        public Contributor Contributor { get; set; }

        [XmlElement("created")]
        public string Created { get; set; }

        [XmlElement("modified")]
        public string Modified { get; set; }

        [XmlElement("up_axis")]
        public string UpAxis { get; set; }

        [XmlElement("unit")]
        public Unit Unit { get; set; }
    }
}
