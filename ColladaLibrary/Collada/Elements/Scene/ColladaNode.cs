﻿using System.Collections.Generic;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Hierarchy;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class ColladaNode
    {
        public ColladaNode()
        {
            InstanceGeometries = new List<InstanceGeometry>();
            Rotations = new List<Rotate>();
        }

        [XmlIgnore]
        public Node InnerNode { get; set; }

        [XmlAttribute("id")]
        public string ID { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("matrix")]
        public ColladaMatrix Matrix { get; set; }

        [XmlElement("scale")]
        public Scale Scale { get; set; }

        [XmlElement("translate")]
        public Translate Translate { get; set; }

        [XmlElement("rotate")]
        public List<Rotate> Rotations { get; set; }

        [XmlElement("instance_geometry")]
        public List<InstanceGeometry> InstanceGeometries { get; set; }

        [XmlElement("node")]
        public List<ColladaNode> ChildNodes { get; set; }

        [XmlElement("instance_light")]
        public List<InstanceLight> Lights { get; set; }
    }
}