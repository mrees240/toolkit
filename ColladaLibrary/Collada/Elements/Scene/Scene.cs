﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class Scene
    {
        [XmlElement("instance_visual_scene")]
        public List<InstanceVisualScene> SceneInstances { get; set; }
    }
}
