﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class LibraryVisualScenes
    {
        [XmlElement("visual_scene")]
        public VisualScene[] ChildScenes { get; set; }
    }
}