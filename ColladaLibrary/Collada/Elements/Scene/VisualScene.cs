﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class VisualScene
    {
        [XmlAttribute("id")]
        public string ID { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("node")]
        public ColladaNode[] ChildNodes { get; set; }
    }
}