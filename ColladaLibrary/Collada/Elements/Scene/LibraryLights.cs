﻿using System.Collections.Generic;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Lighting;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class LibraryLights
    {
        [XmlElement("light")]
        public List<Light> Lights { get; set; }
    }
}
