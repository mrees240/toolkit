﻿using ColladaLibrary.Misc;
using System.Collections.Generic;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Mesh;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class InstanceGeometry
    {
        public InstanceGeometry()
        {
            BindMaterial = new BindMaterial();
        }

        [XmlIgnore]
        public bool IsColladaBillboard { get; set; }

        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("bind_material")]
        public BindMaterial BindMaterial { get; set; }

        [XmlIgnore]
        public int MeshId { get; set; }

        [XmlIgnore]
        public Geometry Geometry { get; set; }


        public List<BitmapFilePair> GetTextureFiles()
        {
            return BindMaterial.GetTextureFiles();
        }
    }
}