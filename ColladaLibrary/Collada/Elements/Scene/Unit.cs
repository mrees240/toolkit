﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ColladaLibrary.Collada.Elements.Scene
{
    public class Unit
    {
        public Unit()
        {
            Meter = 1.0f;
        }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("meter")]
        public float Meter { get; set; }
    }
}
