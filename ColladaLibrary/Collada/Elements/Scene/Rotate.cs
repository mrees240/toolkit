﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class Rotate
    {
        public Rotate()
        {
            Sid = string.Empty;
            Value = string.Empty;
        }

        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlText]
        public string Value { get; set; }

        public double[] GetValues()
        {
            string[] splits = Value.Split(' ');
            double[] values = new double[splits.Length];

            for (int iVal = 0; iVal < splits.Length; iVal++)
            {
                values[iVal] = double.Parse(splits[iVal]);
            }

            return values;
        }

        public double GetAxisRotation()
        {
            string[] splits = Value.Split(' ');
            return double.Parse(splits[splits.Length - 1]);
        }
    }
}
