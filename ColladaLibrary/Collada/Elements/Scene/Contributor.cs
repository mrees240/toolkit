﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ColladaLibrary.Collada.Elements.Scene
{
    public class Contributor
    {
        public Contributor()
        {
            AuthoringTool = "Collada Library";
        }

        [XmlElement("authoring_tool")]
        public string AuthoringTool { get; set; }
    }
}
