﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class InstanceVisualScene
    {
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}
