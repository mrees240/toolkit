﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Mesh;
using TMLibrary.Lib.Collada.Elements.Scene;

namespace TMLibrary.Lib.Collada.Elements
{
    [XmlRoot(ElementName = "COLLADA", Namespace = "http://www.collada.org/2005/11/COLLADASchema")]
    public class ColladaFile
    {
        public ColladaFile()
        {
            Asset = new Asset();
        }

        [XmlAttribute("version")]
        public string Version { get; set; }

        [XmlElement("asset")]
        public Asset Asset { get; set; }

        [XmlElement("library_images")]
        public LibraryImages LibraryImages { get; set; }

        [XmlElement("library_lights")]
        public LibraryLights LibraryLights { get; set; }

        [XmlElement("library_effects")]
        public LibraryEffects LibraryEffects { get; set; }

        [XmlElement("library_materials")]
        public LibraryMaterials LibraryMaterials { get; set; }

        [XmlElement("library_geometries")]
        public LibraryGeometries LibraryGeometries { get; set; }

        [XmlElement("library_visual_scenes")]
        public LibraryVisualScenes LibraryVisualScenes { get; set; }

        [XmlElement("scene")]
        public Scene.Scene Scene { get; set; }
    }
}