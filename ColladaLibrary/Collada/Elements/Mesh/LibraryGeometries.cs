﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class LibraryGeometries
    {
        [XmlElement("geometry")]
        public List<Geometry> Geometries { get; set; }
    }
}