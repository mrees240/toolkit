﻿using ColladaLibrary.Misc;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class InstanceMaterial
    {
        [XmlAttribute("symbol")]
        public string Symbol { get; set; }

        [XmlAttribute("target")]
        public string Target { get; set; }

        [XmlIgnore]
        public Material Material { get; set; }


        public BitmapFilePair GetTextureFile()
        {
            return Material.GetTextureFile();
        }
    }
}