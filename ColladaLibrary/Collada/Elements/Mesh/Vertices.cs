﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Vertices
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("input")]
        public Input[] Inputs { get; set; }
    }
}