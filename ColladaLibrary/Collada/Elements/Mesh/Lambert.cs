﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Lambert
    {
        [XmlElement("diffuse")]
        public Diffuse Diffuse { get; set; }
    }
}