﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Phong
    {
        [XmlElement("diffuse")]
        public Diffuse Diffuse { get; set; }
    }
}