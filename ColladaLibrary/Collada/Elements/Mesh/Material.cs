﻿using ColladaLibrary.Misc;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Material
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("instance_effect")]
        public InstanceEffect InstanceEffect { get; set; }

        [XmlIgnore]
        public string FileUrl { get; set; }

        [XmlIgnore]
        public Color DiffuseColor { get; set; }

        [XmlIgnore]
        public Effect Effect { get; set; }

        [XmlIgnore]
        public bool IsShadeless { get; set; }

        [XmlIgnore]
        public bool HasTransparency { get; set; }

        public BitmapFilePair GetTextureFile()
        {
            return Effect.GetTextureFile();
        }
    }
}