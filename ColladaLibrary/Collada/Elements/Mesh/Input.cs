﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Input
    {
        [XmlAttribute("semantic")]
        public string Semantic { get; set; }

        [XmlAttribute("source")]
        public string SourceId { get; set; }

        [XmlAttribute("offset")]
        public int Offset { get; set; }
        public bool ShouldSerializeOffset()
        {
            return Semantic.ToLower() != "position";
        }

        [XmlAttribute("set")]
        public string Set { get; set; }

        [XmlIgnore]
        public Source Source { get; set; }
    }
}