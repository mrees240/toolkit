﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Geometry
    {
        [XmlIgnore]
        public bool IsColladaBillboard { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("mesh")]
        public ColladaMesh Mesh { get; set; }
    }
}