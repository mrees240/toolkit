﻿using System.Linq;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class FloatArray
    {
        public FloatArray()
        {
            Value = string.Empty;
        }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("count")]
        public int Count { get; set; }

        [XmlText]
        public string Value { get; set; }

        public float[] GetValues()
        {
            var splitStrings = Value.Split(new char[] {' ', '\n'});
            splitStrings = splitStrings.Where(s => s.Length > 0).ToArray();
            var values = new float[splitStrings.Length];

            for (var iValue = 0; iValue < values.Length; iValue++)
            {
                values[iValue] = float.Parse(splitStrings[iValue]);
            }

            return values;
        }
    }
}