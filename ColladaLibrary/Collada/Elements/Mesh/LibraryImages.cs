﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class LibraryImages
    {
        [XmlElement("image")]
        public List<ColladaImage> Images { get; set; }
    }
}