﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Sampler2D
    {
        [XmlElement("source")]
        public SamplerSource Source { get; set; }
    }
}