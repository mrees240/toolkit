﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class LibraryMaterials
    {
        [XmlElement("material")]
        public List<Material> Materials { get; set; }
    }
}