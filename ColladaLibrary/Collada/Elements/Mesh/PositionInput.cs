﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class PositionInput : Input
    {
        [XmlIgnore]
        public Source SourceChild { get; set; }
    }
}