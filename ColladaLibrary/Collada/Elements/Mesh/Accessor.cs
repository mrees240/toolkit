﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Accessor
    {
        [XmlAttribute("source")]
        public string Source { get; set; }

        [XmlAttribute("stride")]
        public int Stride { get; set; }

        [XmlAttribute("count")]
        public int Count { get; set; }

        [XmlElement("param")]
        public Parameter[] Parameter { get; set; }
    }
}