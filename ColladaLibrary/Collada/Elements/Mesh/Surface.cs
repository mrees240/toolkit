﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Surface
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("init_from")]
        public string ImageId { get; set; }
    }
}