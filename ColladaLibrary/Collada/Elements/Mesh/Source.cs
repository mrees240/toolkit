﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Source
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("float_array")]
        public FloatArray FloatArray { get; set; }

        [XmlElement("technique_common")]
        public TechniqueCommon TechniqueCommon { get; set; }
    }
}