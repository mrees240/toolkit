﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class NewParam
    {
        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlElement("surface")]
        public Surface Surface { get; set; }

        [XmlElement("sampler2D")]
        public Sampler2D Sampler { get; set; }
    }
}