﻿using System.Linq;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Indices
    {
        public Indices()
        {
            Value = string.Empty;
        }

        [XmlText]
        public string Value { get; set; }

        public int[] GetValues()
        {
            var splitStrings = Value.Split(' ').Where(s => s.Length > 0).ToArray();
            var values = new int[splitStrings.Length];

            for (var iValue = 0; iValue < values.Length; iValue++)
                values[iValue] = int.Parse(splitStrings[iValue]);

            return values;
        }
    }
}