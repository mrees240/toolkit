﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    /// <summary>
    /// Used as a base class for Lines and Polylist elements
    /// </summary>
    public abstract class ElementsBase
    {
        [XmlElement("input")]
        public Input[] Inputs { get; set; }

        [XmlElement("vcount")]
        public VertexCount VertexCount { get; set; }
        public virtual bool ShouldSerializeVertexCount()
        {
            return false;
        }

        [XmlElement("p")]
        public Indices Indices { get; set; }

        /// <summary>
        /// Gets indices for a specified semantic.
        /// </summary>
        /// <param name="semantic"></param>
        /// <returns></returns>
        public List<int> GetIndices(string semantic)
        {
            List<int> semanticIndices = new List<int>();
            int totalInputs = Inputs.Max(i => i.Offset) + 1;
            var semanticInput = Inputs.Where(i => i.Semantic.ToUpper() == semantic).FirstOrDefault();

            if (semanticInput != null)
            {
                int[] allIndices = Indices.GetValues();
                int offset = semanticInput.Offset;

                for (int i = offset; i < allIndices.Length; i += totalInputs)
                {
                    semanticIndices.Add(allIndices[i]);
                }
            }

            return semanticIndices;
        }

        /// <summary>
        /// Gets indices based on a specific offset.
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<int> GetIndices(int offset)
        {
            List<int> semanticIndices = new List<int>();
            int totalInputs = Inputs.Length;
            var semanticInput = Inputs.Where(i => i.Offset == offset).FirstOrDefault();

            if (semanticInput != null)
            {
                int[] allIndices = Indices.GetValues();

                for (int i = offset; i < allIndices.Length; i += totalInputs)
                {
                    semanticIndices.Add(allIndices[i]);
                }
            }

            return semanticIndices;
        }
    }
}