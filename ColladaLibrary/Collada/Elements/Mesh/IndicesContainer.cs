﻿namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    /// <summary>
    ///     Used to store a list of indices. Does not exist in the Collada file itself.
    /// </summary>
    public class IndicesContainer
    {
        public IndicesContainer(int[] indices)
        {
            Indices = indices;
        }

        public int[] Indices { get; set; }
    }
}