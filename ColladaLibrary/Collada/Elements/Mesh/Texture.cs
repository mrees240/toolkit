﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Texture
    {
        [XmlAttribute("texture")]
        public string TextureValue { get; set; }

        [XmlAttribute("texcoord")]
        public string TexCoord { get; set; }
    }
}