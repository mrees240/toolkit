﻿using ColladaLibrary.Misc;
using System.Drawing;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class ColladaImage
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("init_from")]
        public string ImageUrl { get; set; }

        [XmlIgnore]
        public BitmapFilePair BitmapFile { get; set; }
    }
}