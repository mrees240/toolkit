﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class ColladaMesh
    {
        public ColladaMesh()
        {
            PolyLists = new List<PolyList>();
            Triangles = new List<PolyList>();
            Polygons = new List<PolyList>();
            Lines = new List<Mesh.Lines>();
        }

        [XmlElement("source")]
        public Source[] Sources { get; set; }

        [XmlElement("vertices")]
        public Vertices Vertices { get; set; }

        [XmlElement("polylist")]
        public List<PolyList> PolyLists { get; set; }

        [XmlElement("triangles")]
        public List<PolyList> Triangles { get; set; }

        [XmlElement("polygons")]
        public List<PolyList> Polygons { get; set; }

        [XmlElement("lines")]
        public List<Mesh.Lines> Lines { get; set; }
    }
}