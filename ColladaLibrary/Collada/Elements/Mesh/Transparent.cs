﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Transparent
    {
        [XmlElement("color")]
        public Color Color { get; set; }
    }
}
