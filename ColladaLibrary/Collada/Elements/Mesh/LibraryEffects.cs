﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class LibraryEffects
    {
        [XmlElement("effect")]
        public List<Effect> Effects { get; set; }
    }
}