﻿using ColladaLibrary.Misc;
using System.Collections.Generic;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class BindMaterial
    {
        public BindMaterial()
        {
            TechniqueCommon = new TechniqueCommon();
        }

        [XmlElement("technique_common")]
        public TechniqueCommon TechniqueCommon { get; set; }

        public List<BitmapFilePair> GetTextureFiles()
        {
            return TechniqueCommon.GetTextureFiles();
        }
    }
}