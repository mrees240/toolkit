﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class InstanceEffect
    {
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}