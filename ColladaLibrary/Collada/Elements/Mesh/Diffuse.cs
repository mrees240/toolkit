﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Diffuse
    {
        [XmlElement("texture")]
        public Texture Texture { get; set; }

        [XmlElement("color")]
        public Color Color { get; set; }
    }
}