﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class ProfileCommon
    {
        [XmlElement("newparam")]
        public NewParam[] NewParams { get; set; }

        [XmlElement("technique")]
        public Technique Technique { get; set; }
    }
}