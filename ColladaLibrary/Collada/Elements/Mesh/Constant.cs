﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Constant
    {
        [XmlElement("transparent")]
        public Transparent Transparent { get; set; }
    }
}