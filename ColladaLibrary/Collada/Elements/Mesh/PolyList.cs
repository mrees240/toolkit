﻿using ColladaLibrary.Misc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class PolyList : ElementsBase
    {

        [XmlAttribute("count")]
        public int Count { get; set; }

        [XmlAttribute("material")]
        public string MaterialId { get; set; }

        public override bool ShouldSerializeVertexCount()
        {
            return true;
        }

        [XmlIgnore]
        public Material Material { get; set; }
        
        public List<int> GetTexCoordIndices()
        {
            var indices = GetIndices("TEXCOORD");

            if (indices.Count() == 0)
            {
                indices = GetIndices(2);
            }

            return indices;
        }

        public List<int> GetVertexPositionIndices()
        {
            return GetIndices("VERTEX");
        }

        public BitmapFilePair GetTextureFile()
        {
            return Material.GetTextureFile();
        }
    }
}