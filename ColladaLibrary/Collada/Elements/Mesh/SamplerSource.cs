﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class SamplerSource
    {
        [XmlText]
        public string Value { get; set; }
    }
}