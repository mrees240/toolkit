﻿using ColladaLibrary.Misc;
using System;
using System.Drawing;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Mesh
{
    public class Effect
    {
        public Effect()
        {
            Id = string.Empty;
            Name = string.Empty;
            Color = Color.White;
        }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }
        public virtual bool ShouldSerializeName()
        {
            return Name.Length > 0;
        }

        [XmlElement("profile_COMMON")]
        public ProfileCommon ProfileCommon { get; set; }

        [XmlIgnore]
        public System.Drawing.Color Color { get; set; }

        [XmlIgnore]
        public ColladaImage Image { get; set; }

        public BitmapFilePair GetTextureFile()
        {
            return Image.BitmapFile;
        }
    }
}