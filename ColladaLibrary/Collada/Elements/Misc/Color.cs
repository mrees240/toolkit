﻿using System.Numerics;
using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Misc
{
    public class Color
    {
        private string value;

        public Color()
        {
            value = string.Empty;
            XnaColor = new Vector4(1.0f);
        }

        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlText]
        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
                this.value = value.Replace("  ", " ");
                string[] valueSplits = this.value.Split(' ');
                Values = new float[]
                {
                    float.Parse(valueSplits[2]),
                    float.Parse(valueSplits[1]),
                    float.Parse(valueSplits[0]),
                };
                XnaColor = new Vector4(
                    Values[0], Values[1], Values[2], 1.0f);
            }
        }

        [XmlIgnore]
        public Vector4 XnaColor { get; set; }

        public float[] Values { get; set; }
        public virtual bool ShouldSerializeValues()
        {
            return Value.Length == 0;
        }
    }
}