﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Lighting;
using TMLibrary.Lib.Collada.Elements.Mesh;

namespace TMLibrary.Lib.Collada.Elements.Misc
{
    public class Technique
    {
        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlElement("phong")]
        public Phong Phong { get; set; }

        [XmlElement("lambert")]
        public Lambert Lambert { get; set; }

        [XmlElement("constant")]
        public Constant Constant { get; set; }

        [XmlElement("intensity")]
        public Intensity Intensity { get; set; }
    }
}