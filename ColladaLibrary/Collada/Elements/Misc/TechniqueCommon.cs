﻿using ColladaLibrary.Misc;
using System.Collections.Generic;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Lighting;
using TMLibrary.Lib.Collada.Elements.Mesh;
using System.Linq;

namespace TMLibrary.Lib.Collada.Elements.Misc
{
    public class TechniqueCommon
    {
        public TechniqueCommon()
        {
            InstanceMaterials = new List<InstanceMaterial>();
        }

        [XmlElement("accessor")]
        public Accessor Accessor { get; set; }

        [XmlElement("instance_material")]
        public List<InstanceMaterial> InstanceMaterials { get; set; }

        [XmlElement("ambient")]
        public Ambient Ambient { get; set; }

        [XmlElement("directional")]
        public Directional Directional { get; set; }

        [XmlElement("spot")]
        public Spot Spot { get; set; }

        [XmlElement("point")]
        public Point Point { get; set; }


        public List<BitmapFilePair> GetTextureFiles()
        {
            return InstanceMaterials.Select(m => m.GetTextureFile()).ToList();
        }
    }
}