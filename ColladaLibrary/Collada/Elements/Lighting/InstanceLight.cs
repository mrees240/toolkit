﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Lighting;

namespace TMLibrary.Lib.Collada.Elements.Scene
{
    public class InstanceLight
    {
        private Light light;

        public InstanceLight()
        {
        }

        [XmlAttribute("url")]
        public string Url { get; set; }
        public Light Light { get => light; set => light = value; }
    }
}