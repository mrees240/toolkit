﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Lighting
{
    public class Extra
    {
        [XmlElement("technique")]
        public Technique Technique { get; set; }
    }
}
