﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Lighting
{
    public class Light
    {
        public Light()
        {
            Id = string.Empty;
            Name = string.Empty;
            TechniqueCommon = new TechniqueCommon();
            Extra = new Extra();
        }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("technique_common")]
        public TechniqueCommon TechniqueCommon { get; set; }

        [XmlElement("technique")]
        public Technique Technique { get; set; }

        [XmlElement("extra")]
        public Extra Extra { get; set; }
    }
}
