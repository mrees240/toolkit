﻿using System.Xml.Serialization;

namespace TMLibrary.Lib.Collada.Elements.Lighting
{
    public class Intensity
    {
        [XmlAttribute("sid")]
        public string Sid { get; set; }

        [XmlText]
        public double Value { get; set; }
    }
}