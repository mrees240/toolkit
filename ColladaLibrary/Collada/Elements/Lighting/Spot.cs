﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Lighting
{
    public class Spot
    {
        [XmlElement("color")]
        public Color Color { get; set; }

        [XmlElement("falloff_angle")]
        public double FalloffAngle { get; set; }
    }
}
