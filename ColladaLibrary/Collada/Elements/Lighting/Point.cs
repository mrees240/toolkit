﻿using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements.Misc;

namespace TMLibrary.Lib.Collada.Elements.Lighting
{
    public class Point
    {
        [XmlElement("color")]
        public Color Color { get; set; }
    }
}
