﻿using ColladaLibrary.Render;
using System.Numerics;

namespace TMLibrary.Lib.Collada.PseudoElements
{
    public class MeshNode
    {
        private Mesh mesh;
        private Vector3 translation;

        public MeshNode(Mesh mesh, Vector3 translation)
        {
            this.mesh = mesh;
            this.translation = translation;
        }

        public Mesh Mesh { get => mesh; set => mesh = value; }
        public Vector3 Translation { get => translation; }
    }
}