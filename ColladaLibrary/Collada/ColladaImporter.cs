﻿using System;
using System.Collections.Generic;
using System.IO;
using TMLibrary.Lib.Collada.Elements.Mesh;
using TMLibrary.Lib.Collada.Elements.Scene;
using System.Linq;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements;
using TMLibrary.Lib.Collada.Hierarchy;
using TMLibrary.Lib.Collada.Elements.Misc;
using TMLibrary.Lib.Collada.Elements.Lighting;
using System.Drawing;
using ColladaLibrary.Render;
using System.Numerics;
using ColladaLibrary.Extensions;
using ColladaLibrary.Utility;
using ColladaLibrary.Rendering;
using Color = TMLibrary.Lib.Collada.Elements.Misc.Color;
using ColladaLibrary.Misc;

namespace ColladaLibrary.Collada
{
    public class ColladaImporter
    {
        private List<BitmapFilePair> newTextures = new List<BitmapFilePair>();

        private bool roundedVertices;

        // XML
        private Asset asset = new Asset();
        private List<ColladaImage> allImages = new List<ColladaImage>();
        private List<Effect> allEffects = new List<Effect>();
        private List<Material> allMaterials = new List<Material>();
        private List<Geometry> allGeometries = new List<Geometry>();
        private List<InstanceGeometry> allInstanceGeometries = new List<InstanceGeometry>();
        private List<ColladaMesh> allColladaMeshes = new List<ColladaMesh>();
        private List<ElementsBase> allElements = new List<ElementsBase>();
        private List<Vertices> allVertexElements = new List<Vertices>();
        private List<FloatArray> allFloatArrays = new List<FloatArray>();
        private List<Source> allSources = new List<Source>();
        private List<Input> allInputs = new List<Input>();
        private List<InstanceMaterial> allInstanceMaterials = new List<InstanceMaterial>();
        private List<Light> allLights = new List<Light>();
        private List<ColladaNode> allLightNodes = new List<ColladaNode>();
        private LibraryVisualScenes libraryVisualScenes;
        private List<ColladaNode> allColladaNodes = new List<ColladaNode>();
        private Bitmap errorTexture;
        private InstanceMaterial defaultMaterial;

        private List<Mesh> allMeshes = new List<Mesh>();
        private List<string> allMeshNames = new List<string>();
        private List<Matrix4x4> allMeshGlobalMatrices = new List<Matrix4x4>();

        private string fileUrl;
        private string safeFileName;
        private string directory;

        private bool ignoreTextures;
        private bool defaultLighting;

        private bool fileLoaded = false;

        private Node rootNode;

        private Dictionary<string, Bitmap> loadedBitmaps = new Dictionary<string, Bitmap>();

        public Node RootNode
        {
            get { return rootNode; }
        }

        public List<BitmapFilePair> NewTextures
        {
            get { return newTextures; }
        }

        public List<Mesh> AllMeshes
        {
            get { return allMeshes; }
        }

        public string[] GetImageNames()
        {
            var names = new string[allImages.Count];

            for (int iImage = 0; iImage < names.Length; iImage++)
            {
                names[iImage] = allImages[iImage].Name;
            }

            return names;
        }

        public string[] GetMaterialIds()
        {
            var names = new string[allImages.Count];

            for (int iImage = 0; iImage < names.Length; iImage++)
            {
                names[iImage] = allImages[iImage].Id;
            }

            return names;
        }

        public string[] GetAllImagePaths()
        {
            var paths = new string[allImages.Count];

            for (int iImage = 0; iImage < paths.Length; iImage++)
            {
                paths[iImage] = allImages[iImage].ImageUrl;
            }

            return paths;
        }

        private Bitmap CreateErrorTexture()
        {
            var bmp = new Bitmap(2, 2);
            bmp.SetPixel(0, 0, System.Drawing.Color.DarkGray);
            bmp.SetPixel(0, 1, System.Drawing.Color.Gray);
            bmp.SetPixel(1, 1, System.Drawing.Color.DarkGray);
            bmp.SetPixel(1, 0, System.Drawing.Color.Gray);
            return bmp;
        }

        public bool DefaultLighting { get => defaultLighting; }
        public List<Geometry> AllGeometries { get => allGeometries; }
        public List<Effect> AllEffects { get => allEffects; }
        public List<ColladaImage> AllImages { get => allImages; set => allImages = value; }
        public List<ColladaMesh> AllColladaMeshes { get => allColladaMeshes; set => allColladaMeshes = value; }
        public List<Material> AllMaterials { get => allMaterials; set => allMaterials = value; }
        public List<string> AllMeshNames { get => allMeshNames; }

        private bool ignoreMeasurement;

        private ColladaImporter()
        {
            errorTexture = CreateErrorTexture();
            defaultMaterial = new InstanceMaterial();
            defaultMaterial.Material = new Material();
            defaultMaterial.Material.Name = "DefaultMaterial";
            defaultMaterial.Material.Effect = new Effect();
            defaultMaterial.Material.Effect.Image = new ColladaImage();
            defaultMaterial.Material.Effect.Image.BitmapFile = new BitmapFilePair();
            defaultMaterial.Material.Effect.Image.BitmapFile.Bitmap = errorTexture;
        }

        public ColladaImporter(bool ignoreTextures, bool defaultLighting, string fileUrl, bool ignoreMeasurement) : this()
        {
            this.ignoreMeasurement = ignoreMeasurement;
            this.ignoreTextures = ignoreTextures;
            this.fileUrl = fileUrl;
            this.safeFileName = GetSafeFileName(fileUrl);
            this.directory = GetDirectory(fileUrl);
            this.defaultLighting = defaultLighting;
        }

        public ColladaImporter(bool ignoreTextures, bool defaultLighting, string fileUrl) : this()
        {
            this.ignoreTextures = ignoreTextures;
            this.fileUrl = fileUrl;
            this.safeFileName = GetSafeFileName(fileUrl);
            this.directory = GetDirectory(fileUrl);
            this.defaultLighting = defaultLighting;
        }

        private string GetDirectory(string fileUrl)
        {
            string safeFileName = GetSafeFileName(fileUrl);

            return fileUrl.Replace(safeFileName, "");
        }

        private string GetSafeFileName(string fileUrl)
        {
            fileUrl = fileUrl.Replace('/', '\\');

            return fileUrl.Split('\\').Last();
        }

        public void Load()
        {
            roundedVertices = false;
            Initialize();
            fileLoaded = true;
        }

        public void Load(bool roundedVertices)
        {
            this.roundedVertices = roundedVertices;
            Initialize();
            fileLoaded = true;
        }

        public List<string> GetImageUrlList()
        {
            var urls = new List<string>();

            urls.AddRange(allImages.Select(i => i.ImageUrl));

            return urls;
        }

        public Mesh CreateSingleMesh()
        {
            if (fileLoaded)
            {
                var mesh = CreateSingleMeshFromNode(rootNode, Matrix4x4.Identity);

                return mesh;
            }
            else
            {
                throw new Exception("The file must be loaded before a mesh can be created.");
            }
        }

        public Mesh CreateSingleMeshFromNode(Node node, Matrix4x4 matrix)
        {
            var meshMatrix = matrix;
            var meshes = new List<Mesh>();

            CreateSingleMeshFromNode(node, matrix, meshes);

            var mesh = RenderUtility.MergeMeshes(meshes);

            return mesh;
        }

        public void CreateSingleMeshFromNode(Node node, Matrix4x4 stackedMatrix, List<Mesh> transformedMeshes)
        {
            stackedMatrix = Matrix4x4.Multiply(node.Matrix, stackedMatrix);

            foreach (var meshId in node.MeshIdList)
            {
                var mesh = new Mesh(allMeshes[meshId]);
                mesh.Transform(stackedMatrix);
                transformedMeshes.Add(mesh);
            }

            foreach (var child in node.Children)
            {
                CreateSingleMeshFromNode(child, stackedMatrix, transformedMeshes);
            }
        }

        public List<MeshMatrixPair> LoadMeshesWithMatrices()
        {
            var pairs = new List<MeshMatrixPair>();

            LoadMeshesWithMatrices(pairs, rootNode, Matrix4x4.Identity);

            return pairs;
        }

        private void LoadMeshesWithMatrices(List<MeshMatrixPair> pairs, Node node, Matrix4x4 globalMatrix)
        {
            globalMatrix = Matrix4x4.Multiply(node.Matrix, globalMatrix);

            foreach (var meshId in node.MeshIdList)
            {
                pairs.Add(new MeshMatrixPair(allMeshes[meshId], globalMatrix, node.Name));
            }

            foreach (var child in node.Children)
            {
                LoadMeshesWithMatrices(pairs, child, globalMatrix);
            }
        }

        private List<Vertex> LoadTransformedVertices(List<Vertex> vertices, Matrix4x4 matrix)
        {
            List<Vertex> transformedVertices = new List<Vertex>();

            foreach (var vertex in vertices)
            {
                var vec = vertex.Vector;
                vec = Vector3.Transform(vec, matrix);
                var transformedVertex = new Vertex(vec.X, vec.Y, vec.Z, vertex.U, vertex.V, vertex.Normal.X, vertex.Normal.Y, vertex.Normal.Z, vertex.DiffuseColor);

                transformedVertices.Add(transformedVertex);
            }
            return transformedVertices;
        }

        private void CreateNodeHierarchy()
        {
            rootNode = new Node("root", Matrix4x4.Identity, null);

            foreach (var scene in libraryVisualScenes.ChildScenes)
            {
                foreach (var child in scene.ChildNodes)
                {
                    CreateNode(child, RootNode);
                }
            }
        }

        private void CreateNode(ColladaNode colladaNode, Node parent)
        {
            List<int> meshIdList = new List<int>();

            foreach (var instanceGeometry in colladaNode.InstanceGeometries)
            {
                allInstanceGeometries.Add(instanceGeometry);

                if (instanceGeometry.Url != null)
                {
                    var geometryId = RemovePoundSign(instanceGeometry.Url);
                    var geometry = allGeometries.Where(g => g.Id == geometryId).FirstOrDefault();
                    if (geometry != null)
                    {
                        var mesh = geometry.Mesh;
                        int meshId = allColladaMeshes.IndexOf(mesh);
                        instanceGeometry.MeshId = meshId;
                        instanceGeometry.Geometry = geometry;

                        meshIdList.Add(meshId);
                    }
                }
            }

            Node node = null;

            if (colladaNode.Matrix != null)
            {
                node = new Node(colladaNode.Name, colladaNode.Matrix.GetMatrix(), parent);
            }
            else
            {
                /*var rotVec = new Vector3();
                var rotMatrix = Matrix4x4.Identity;

                if (colladaNode.Rotations.Count() > 0)
                {
                    // 3DS Max Compatibility
                    var rotations = colladaNode.Rotations;
                    var rotationX = rotations.Where(r => r.Sid.ToUpper() == "ROTATEX").FirstOrDefault();
                    var rotationY = rotations.Where(r => r.Sid.ToUpper() == "ROTATEY").FirstOrDefault();
                    var rotationZ = rotations.Where(r => r.Sid.ToUpper() == "ROTATEZ").FirstOrDefault();
                    Translate translate = colladaNode.Translate;

                    double yRad = 0.0;
                    double xRad = 0.0;
                    double zRad = 0.0;

                    if (rotationX != null)
                    {
                        xRad = rotationX.GetAxisRotation() * Math.PI / 180.0;
                    }
                    if (rotationY != null)
                    {
                        yRad = rotationY.GetAxisRotation() * Math.PI / 180.0;
                    }
                    if (rotationZ != null)
                    {
                        zRad = rotationZ.GetAxisRotation() * Math.PI / 180.0;
                    }
                    var xRotMatrix = Matrix4x4.CreateFromAxisAngle(new Vector3(1.0f, 0.0f, 0.0f), (float)xRad);
                    var yRotMatrix = Matrix4x4.CreateFromAxisAngle(new Vector3(0.0f, 1.0f, 0.0f), (float)yRad);
                    var zRotMatrix = Matrix4x4.CreateFromAxisAngle(new Vector3(0.0f, 0.0f, 1.0f), (float)zRad);
                    rotVec = new Vector3((float)xRad, (float)yRad, (float)zRad);
                    rotMatrix = Matrix4x4.Multiply(xRotMatrix, yRotMatrix);
                    rotMatrix = Matrix4x4.Multiply(rotMatrix, zRotMatrix);
                }
                else if (colladaNode.Matrix != null)
                {
                    var mat = colladaNode.Matrix.GetMatrix();
                    var quat = Quaternion.CreateFromRotationMatrix(mat);
                    rotMatrix = Matrix4x4.CreateFromQuaternion(quat);
                }

                var translationMatrix = Matrix4x4.Identity;
                var scaleMatrix = Matrix4x4.Identity;
                if (colladaNode.Scale != null)
                {
                    double[] scale = colladaNode.Scale.GetValues();
                    scaleMatrix = Matrix4x4.CreateScale(new Vector3((float)scale[0], (float)scale[1], (float)scale[2]));
                }
                if (colladaNode.Translate != null)
                {
                    double[] translation = colladaNode.Translate.GetValues();
                    translationMatrix = Matrix4x4.CreateTranslation((float)translation[0], (float)translation[1], (float)translation[2]);
                }
                else if (colladaNode.Matrix != null)
                {
                    translationMatrix = Matrix4x4.CreateTranslation(colladaNode.Matrix.GetMatrix().Translation);
                }

                if (scaleMatrix != Matrix4x4.Identity)
                {
                    for (int iMeshId = 0; iMeshId < meshIdList.Count(); iMeshId++)
                    {
                        int meshId = meshIdList[iMeshId];
                        var mesh = new Mesh(allMeshes[meshId]);

                        mesh.ScaleAtCenter(scaleMatrix.GetScale());

                        allMeshes.Add(mesh);
                        allMeshNames.Add(colladaNode.Name);
                        meshIdList[iMeshId] = allMeshes.Count() - 1;
                    }
                }
                */

                //var finalMatrix = Matrix4x4.Multiply(rotMatrix, translationMatrix);
                //node = new Node(colladaNode.Name, finalMatrix);
                node = new Node(colladaNode.Name, Matrix4x4.Identity, parent);
            }
            foreach (var meshId in meshIdList)
            {
                node.MeshIdList.Add(meshId);
            }
;
            colladaNode.InnerNode = node;
            node.Parent = parent;
            parent.Children.Add(node);

            foreach (var child in colladaNode.ChildNodes)
            {
                CreateNode(child, node);
            }
        }

        private void CreateLights()
        {
            foreach (var node in allColladaNodes)
            {
                CreateLights(node);
            }
        }

        // DOESNT WORK WITH TEXTURES AT THE MOMENT
        /*public List<Mesh> LoadMeshesWithTransformations()
        {
            var transformedMeshes = new List<Mesh>();

            LoadMeshesWithTransformations(rootNode, transformedMeshes, rootNode.Matrix);

            return transformedMeshes;
        }

        private void LoadMeshesWithTransformations(Node currentNode, List<Mesh> transformedMeshes, Matrix4x4 matrixStack)
        {
            var currentMatrix = Matrix4x4.Multiply(currentNode.Matrix, matrixStack);

            foreach (var meshId in currentNode.MeshIdList)
            {
                var transformedMesh = new Mesh(allMeshes[meshId]);
                transformedMesh.Transform(currentMatrix);
                transformedMeshes.Add(transformedMesh);
            }
            foreach (var child in currentNode.Children)
            {
                LoadMeshesWithTransformations(child, transformedMeshes, currentMatrix);
            }
        }*/

        private void CreateLights(ColladaNode node)
        {
            //foreach (var lightNode in lightNodes)
            if (node.Lights.Count() > 0)
            {
                foreach (var lightInstance in node.Lights)
                {
                    string url = lightInstance.Url;
                    var light = allLights.Where(l => l.Id == RemovePoundSign(url)).First();
                    lightInstance.Light = light;
                }
                allLightNodes.Add(node);
            }

            foreach (var child in node.ChildNodes)
            {
                CreateLights(child);
            }
        }

        private void Initialize()
        {
            DeserializeColladaFile();
            CreateNodeHierarchy();
            //AssignBillboards();
            CreateLights();
            CreateAllInstanceMaterials();
            AssignEffects();
            AssignEffectsToMaterials();
            //AssignInstanceMaterialsToPolyLists();
            AssignMaterialsToPolyLists();
            AssignInputSources();
            LoadAllMeshes();
            ApplyLighting();
        }

        public void ApplyLighting()
        {
            if (!defaultLighting)
            {
                ApplyLighting(rootNode);
            }
        }

        public void ApplyLighting(Mesh mesh)
        {
            for (int iPoly = 0; iPoly < mesh.PolygonAmount; iPoly++)
            {
                var polygon = mesh.GetPolygon(iPoly);
                int polyRed = 0;
                int polyGreen = 0;
                int polyBlue = 0;

                foreach (var lightNode in allLightNodes)
                {
                    foreach (var light in lightNode.Lights)
                    {
                        bool isDirectionalLight = light.Light.TechniqueCommon.Directional != null;
                        bool isSpotLight = light.Light.TechniqueCommon.Spot != null;

                        float intensity = 1.0f;

                        System.Drawing.Color finalColor = System.Drawing.Color.Black;

                        if (!defaultLighting)
                        {
                            if (light.Light.Technique != null)
                            {
                                intensity = (float)light.Light.Technique.Intensity.Value;
                            }
                            var cross = RenderUtility.FindCrossProduct(mesh, polygon);

                            if (isDirectionalLight)
                            {
                                var lightRotVec = GetLightNormal(lightNode);
                                intensity = Vector3.Dot(lightRotVec, cross) * intensity;
                            }
                            else if (isSpotLight)
                            {
                                var lightRotVec = GetLightNormal(lightNode);
                                intensity = Vector3.Dot(lightRotVec, cross) * intensity;
                            }
                            float intensityMinimum = 0.0f;
                            if (intensity > 1.0f)
                            {
                                intensity = 1.0f;
                            }
                            else if (intensity < intensityMinimum)
                            {
                                intensity = 0.0f;
                            }
                        }
                        else
                        {
                            finalColor = polygon.GetPolygonIndex(0).DiffuseColor;
                            continue;
                        }


                        var originalColor = System.Drawing.Color.White;
                        originalColor = polygon.GetPolygonIndex(0).DiffuseColor;
                        finalColor = originalColor;
                        var ambient = light.Light.TechniqueCommon.Ambient;
                        var directional = light.Light.TechniqueCommon.Directional;
                        var point = light.Light.TechniqueCommon.Point;
                        var spot = light.Light.TechniqueCommon.Spot;

                        int r = 0;
                        int g = 0;
                        int b = 0;
                        if (ambient != null)
                        {
                            r = (int)(ambient.Color.Values[0] * byte.MaxValue);
                            g = (int)(ambient.Color.Values[1] * byte.MaxValue);
                            b = (int)(ambient.Color.Values[2] * byte.MaxValue);
                        }
                        else if (spot != null)
                        {
                            r = (int)(spot.Color.Values[0] * (intensity) * byte.MaxValue);
                            g = (int)(spot.Color.Values[1] * (intensity) * byte.MaxValue);
                            b = (int)(spot.Color.Values[2] * (intensity) * byte.MaxValue);
                        }
                        else if (directional != null)
                        {
                            r = (int)(directional.Color.Values[0] * (intensity) * byte.MaxValue);
                            g = (int)(directional.Color.Values[1] * (intensity) * byte.MaxValue);
                            b = (int)(directional.Color.Values[2] * (intensity) * byte.MaxValue);
                        }
                        else if (point != null)
                        {
                            r = (int)(point.Color.Values[0] * (intensity) * byte.MaxValue);
                            g = (int)(point.Color.Values[1] * (intensity) * byte.MaxValue);
                            b = (int)(point.Color.Values[2] * (intensity) * byte.MaxValue);
                        }
                        if (polyRed + finalColor.R <= byte.MaxValue)
                        {
                            polyRed += finalColor.R;
                        }
                        else
                        {
                            polyRed = byte.MaxValue;
                        }
                        if (polyGreen + finalColor.G <= byte.MaxValue)
                        {
                            polyGreen += finalColor.G;
                        }
                        else
                        {
                            polyGreen = byte.MaxValue;
                        }
                        if (polyBlue + finalColor.B <= byte.MaxValue)
                        {
                            polyBlue += finalColor.B;
                        }
                        else
                        {
                            polyRed = byte.MaxValue;
                        }
                        finalColor = System.Drawing.Color.FromArgb(byte.MaxValue, polyRed, polyGreen, polyBlue);
                    }

                    for (int iDiffuse = 0; iDiffuse < polygon.GetIndexAmount(); iDiffuse++)
                    {
                        polygon.GetPolygonIndex(iDiffuse).DiffuseColor = System.Drawing.Color.FromArgb(byte.MaxValue, polyRed, polyGreen, polyBlue);
                    }
                }

            }
        }

        private void ApplyLighting(Node node)
        {
            foreach (var meshId in node.MeshIdList)
            //for (int iMesh = 0; iMesh < node.GetMeshIdListLength(); iMesh++)
            {
                //int meshId = node.GetMeshId(iMesh);
                var mesh = allMeshes[meshId];
                ApplyLighting(mesh);
            }

            foreach (var child in node.Children)
            {
                ApplyLighting(child);
            }
        }

        /// <summary>
        /// Finds the normal of a light's rotation. It uses the cross product of a polygon to determine the normal value.
        /// </summary>
        /// <param name="lightNode"></param>
        /// <returns></returns>
        private Vector3 GetLightNormal(ColladaNode lightNode)
        {
            var lightRotVec = new Vector3(0.0f, 0.0f, -1.0f);

            // Create a temporary polygon to represent the cross product of the light.
            var verts = new List<Vertex>()
            {
                new Vertex(new Vector3(0.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(10.0f, 0.0f, 0.0f)),
                new Vertex(new Vector3(0.0f, -10.0f, 0.0f)),
            };
            var pIndex0 = new PolygonIndex(0);
            var pIndex1 = new PolygonIndex(1);
            var pIndex2 = new PolygonIndex(2);

            var lightPolygon = new Polygon(pIndex0, pIndex1, pIndex2);
            var lightMesh = new Mesh("", verts, new List<Polygon>() { lightPolygon });

            if (lightNode.Matrix != null)
            {
                var lightMat = lightNode.Matrix.GetMatrix();
                //lightMat = Matrix.Multiply(lightMat, Matrix.CreateRotationZ(3.14f / 2.0f));
                var lightRot = Matrix4x4.CreateFromQuaternion(Quaternion.CreateFromRotationMatrix(lightMat));
                lightMesh.Transform(lightRot);
                //lightRotVec= new Vector3(lightRot.X, lightRot.Y * lightRot.W, lightRot.Z);
            }
            lightRotVec = RenderUtility.FindCrossProduct(lightMesh, lightPolygon);
            if (lightRotVec.Length() > 0)
            {
                lightRotVec = lightRotVec.Normalize();
            }
            else
            {
                lightRotVec = new Vector3(0.0f, 0.0f, -1.0f);
            }
            if (float.IsNaN(lightRotVec.X) || float.IsNaN(lightRotVec.Y) || float.IsNaN(lightRotVec.Z))
            {
                throw new Exception("Light rotation is not a number.");
            }

            return lightRotVec;
        }

        private int[] CreateTriangleVertexCountList(int amount)
        {
            int[] vCounts = new int[amount];

            for (int iCount = 0; iCount < amount; iCount++)
            {
                vCounts[iCount] = 3;
            }

            return vCounts;
        }

        private void CreateNewIndexList(PolyList polyList, List<Vertex> vertices, List<int> vertexCountList, List<int> indexList)
        {
            int[] originalVertexCount = null;
            if (polyList.VertexCount != null)
            {
                originalVertexCount = polyList.VertexCount.GetValues();
            }
            else
            {
                originalVertexCount = CreateTriangleVertexCountList(polyList.Count);
            }
            var originalIndices = polyList.GetVertexPositionIndices();

            for (int iVCount = 0; iVCount < polyList.Count; iVCount++)
            {
                int vCount = originalVertexCount[iVCount];
                vertexCountList.Add(vCount);
            }
            indexList.AddRange(originalIndices);

        }

        public Mesh CreateMeshFromGeometry(InstanceGeometry instanceGeometry)
        {
            var geometry = instanceGeometry.Geometry;
            var mesh = new Mesh();

            var polyLists = new List<PolyList>();
            polyLists.AddRange(geometry.Mesh.Triangles);
            polyLists.AddRange(geometry.Mesh.Polygons);
            polyLists.AddRange(geometry.Mesh.PolyLists);

            for (int iPolyList = 0; iPolyList < polyLists.Count; iPolyList++)
            {
                var polyList = polyLists[iPolyList];
                var instanceMat = defaultMaterial; //
                //if (instanceMat == null)
                {
                    if (iPolyList < instanceGeometry.BindMaterial.TechniqueCommon.InstanceMaterials.Count)
                    {
                        instanceMat = instanceGeometry.BindMaterial.TechniqueCommon.InstanceMaterials[iPolyList];
                    }
                    //throw new Exception($"Unable to find material for {instanceGeometry.Name} in {safeFileName}. Assign {instanceGeometry.Name} a material.");
                }
                polyList.Material = instanceMat.Material;
                CreatePolygonsWithUniqueVertices(polyList, mesh, geometry.Mesh.Vertices);
            }

            foreach (var lines in geometry.Mesh.Lines)
            {
                CreatePolygonsFromLines(lines, mesh);
            }

            return mesh;
        }

        private void LoadAllMeshes()
        {
            var rootNode = allColladaNodes.FirstOrDefault();
            if (rootNode == null)
            {
                throw new Exception($"Root node could not be found in {safeFileName}.");
            }
            LoadAllMeshes(rootNode);
        }

        /*private void LoadAllInstanceGeometries(ColladaNode node)
        {
            var allMeshIdList = new List<int>();

            foreach (var instance in allInstanceGeometries)
            {
                allMeshes.Add(CreateMeshFromGeometry(instance));
            }

            foreach (var child in node.ChildNodes)
            {
                LoadAllMeshes(child);

                foreach (var instanceGeometry in child.InstanceGeometries)
                {
                    instanceGeometry.Geometry = allGeometries.Where(g => g.Id == RemovePoundSign(instanceGeometry.Url)).FirstOrDefault();
                    allInstanceGeometries.Add(instanceGeometry);
                }
            }
        }*/

        private void LoadAllMeshes(ColladaNode node)
        {
            var allMeshIdList = new List<int>();

            foreach (var instance in allInstanceGeometries)
            {
                var mesh = CreateMeshFromGeometry(instance);
                if (!ignoreMeasurement)
                {
                    mesh.Transform(Matrix4x4.CreateScale(asset.Unit.Meter));
                }
                allMeshes.Add(mesh);
            }

            foreach (var child in node.ChildNodes)
            {
                LoadAllMeshes(child);
            }
        }

        private void CreatePolygonsWithUniqueVertices(PolyList polyList, Mesh mesh, Vertices verticesElement)
        {
            List<int> vertexCountList = new List<int>();
            List<int> indexList = new List<int>();
            List<Polygon> polygons = new List<Polygon>();
            List<Vertex> vertices = new List<Vertex>();

            CreateVertices(vertices, polyList);
            //mesh.Vertices.AddRange(vertices);
            CreateNewIndexList(polyList, vertices, vertexCountList, indexList);
            CreatePolygons(polyList, vertices, polygons, vertexCountList, indexList, mesh, verticesElement);

            mesh.AddPolygonRange(polygons);
        }

        private void CreatePolygonsFromLines(Lines lines, Mesh mesh)
        {
            List<Polygon> polygons = new List<Polygon>();
            var vertices = new List<Vertex>();
            CreateVertices(vertices, lines);
            var indices = lines.GetIndices("VERTEX");
            for (int i = 0; i < indices.Count(); i += 2)
            {
                var pIndex0 = new PolygonIndex(indices[i]);
                var pIndex1 = new PolygonIndex(indices[i + 1]);
                var pIndex2 = new PolygonIndex(indices[i + 1]);

                var poly = new Polygon(pIndex0, pIndex1, pIndex2);
                polygons.Add(poly);
            }
            mesh.Vertices.AddRange(vertices);
            mesh.AddPolygonRange(polygons);
        }

        private void CreatePolygons(PolyList polyList, List<Vertex> vertices, List<Polygon> polygons,
            List<int> vertexCountList, List<int> indexList, Mesh mesh, Vertices verticesElement)
        {
            var allIndices = polyList.Indices.GetValues();
            var texCoordIndices = polyList.GetTexCoordIndices();
            var texCoordInput = polyList.Inputs.Where(i => i.Semantic.ToUpper() == "TEXCOORD").FirstOrDefault();
            var allTexCoords = new Vector2[0];
            int inputAmount = polyList.Inputs.Count();
            string currentDirectory = Directory.GetCurrentDirectory();

            bool useVerticesElement = texCoordIndices.Count() == 0;

            if (texCoordInput != null && !useVerticesElement)
            {
                allIndices = polyList.Indices.GetValues();
                texCoordIndices = new List<int>();
                var texCoordVals = texCoordInput.Source.FloatArray.GetValues();
                allTexCoords = CreateTextureCoordinatesFromValues(texCoordVals);

                if (!texCoordVals.Any())
                {

                }

                int offset = texCoordInput.Offset;
                int totalInputs = polyList.Inputs.Count();

                for (int i = offset; i < allIndices.Length; i += totalInputs)
                {
                    texCoordIndices.Add(allIndices.ElementAt(i));
                }
            }
            else // Maya compatibility
            {
                texCoordInput = verticesElement.Inputs.Where(i => i.Semantic.ToUpper() == "TEXCOORD").FirstOrDefault();

                if (texCoordInput == null)
                {
                    texCoordInput = verticesElement.Inputs.Where(i => i.Offset == 2).FirstOrDefault();
                }

                if (texCoordInput != null)
                {
                    texCoordIndices.Clear();
                    //texCoordIndices = new List<int>();
                    allTexCoords = CreateTextureCoordinatesFromValues(texCoordInput.Source.FloatArray.GetValues());
                    texCoordIndices = allIndices.ToList();
                }

            }
            int currentIndex = 0;

            for (int iPolygon = 0; iPolygon < vertexCountList.Count; iPolygon++)
            {
                int vertexCount = vertexCountList[iPolygon];

                //if (vertexCount == 3 || vertexCount == 4)
                {
                    var indices = indexList.Skip(currentIndex).Take(vertexCount).ToArray(); ;

                    // Fix quad order
                    if (indices.Count() == 4)
                    {
                        indices = new int[] { indices[3], indices[2], indices[1], indices[0] };
                    }

                    var currentTexCoordIndices = new int[vertexCount];

                    if (texCoordInput != null || texCoordIndices.Count() > 0)
                    {
                        currentTexCoordIndices = texCoordIndices.Skip(currentIndex).Take(vertexCount).ToArray();
                        // fix order
                        if (currentTexCoordIndices.Count() == 3)
                        {
                            //currentTexCoordIndices = new int[] { currentTexCoordIndices[polyOrderTest[0]], currentTexCoordIndices[polyOrderTest[1]], currentTexCoordIndices[polyOrderTest[2]], };
                        }
                        else if (currentTexCoordIndices.Count() == 4)
                        {
                            currentTexCoordIndices = new int[] { currentTexCoordIndices[3], currentTexCoordIndices[2], currentTexCoordIndices[1], currentTexCoordIndices[0] };
                        }
                    }
                    //bool colorOnly = texCoordInput == null || polyList.Material == null;

                    Polygon polygon = new Polygon(indices[0], indices[1], indices[2]);
                    if (indices.Length == 4)
                    {
                        polygon = new Polygon(indices[0], indices[1], indices[2], indices[3]);
                    }

                    /*if (polyList.Material != null)
                    {
                        //CreatePolygonWithDefinition(indices.Count(), mesh, colorOnly, ref polygon);
                        if (polyList.Material != null && polyList.Material.Effect != null)
                        {
                            var col = polyList.Material.Effect.Color;
                            var indicesPolyList = polygon.GetIndices();

                            for (int iIndex = 0; iIndex < polygon.GetIndexAmount(); iIndex++)
                            {
                                var index = indicesPolyList[iIndex];
                                index.DiffuseColor = System.Drawing.Color.FromArgb(col.A, col.B, col.G, col.R);
                            }
                        }
                        polygon.TextureFile = polyList.Material.Effect.GetTextureFile();
                        polygon.Bitmap = polygon.TextureFile.Bitmap;
                    }*/
                    bool colorOnly = polyList.Material == null || polyList.Material.Effect.Image == null;
                    //else if (polyList.Material != null)
                    {
                        SetMaterial(polygon, polyList.Material);

                        int iIndex = 0;

                        if (colorOnly)
                        {
                            //foreach (var poly in polygons)
                            {
                                //if (polygon.Textures.Diffuse == null)
                                {
                                    foreach (var index in polygon.GetIndices())
                                    {
                                        var id = index.Id;
                                        var vert = vertices[id];
                                        index.Id = mesh.Vertices.Count;
                                        mesh.Vertices.Add(vert);
                                    }
                                }
                            }
                        }
                        // Set texCoords
                        if (!colorOnly)
                        {
                            for (int iTexCoord = 0; iTexCoord < vertexCount; iTexCoord++)
                            {
                                if (iTexCoord < currentTexCoordIndices.Length)
                                {
                                    int texCoordIndex = currentTexCoordIndices.ElementAt(iTexCoord);

                                    if (texCoordIndex < allTexCoords.Length)
                                    {
                                        var index = polygon.GetPolygonIndex(iTexCoord);
                                        int id = index.Id;
                                        var texCoord = index.TexCoord;
                                        //polygon.GetPolygonIndex(iTexCoord).TexCoord = new TextureCoordinate(,
                                        //-allTexCoords[texCoordIndex].Y);

                                        index.TexCoord.U = allTexCoords[texCoordIndex].X;
                                        index.TexCoord.V = allTexCoords[texCoordIndex].Y;

                                        var vert = new Vertex(vertices[id]);
                                        vert.U = index.TexCoord.U;
                                        vert.V = index.TexCoord.V;


                                        int vertId = mesh.Vertices.IndexOf(vert);
                                        bool vertAlreadyExists = vertId >= 0;
                                        //vertices.Add(vert);
                                        if (vertAlreadyExists)
                                        {
                                            index.Id = vertId;
                                        }
                                        else
                                        {
                                            index.Id = mesh.Vertices.Count;
                                            mesh.Vertices.Add(vert);
                                        }
                                    }
                                    else
                                    {
                                        //throw new Exception($"Error loading texture coordinates from {safeFileName}.");
                                        polygon.GetPolygonIndex(iTexCoord).TexCoord = new TextureCoordinate();
                                    }

                                    iIndex++;
                                }
                                else
                                {
                                    //throw new Exception($"Error loading texture coordinates from {safeFileName}.");
                                }
                            }
                            if (polyList.Material.Effect.Image != null)
                            {
                                polygon.Textures.Diffuse = polyList.Material.Effect.GetTextureFile();

                                // Autodetect normal-map
                                string ext = "." + polygon.Textures.Diffuse.FileName.Split('.').Last();
                                var normalMapFileName = polygon.Textures.Diffuse.FileName.Replace(ext, $"_n{ext}");
                                //normalMapFileName = $"{currentDirectory}\\{normalMapFileName}";
                                if (File.Exists(normalMapFileName))
                                {
                                    if (!loadedBitmaps.ContainsKey(normalMapFileName))
                                    {
                                        var bmp = new Bitmap(normalMapFileName);
                                        loadedBitmaps.Add(normalMapFileName, bmp);
                                    }
                                    polygon.Textures.Normal = new BitmapFilePair(normalMapFileName, loadedBitmaps[normalMapFileName]);
                                }
                            }
                        }
                        //else
                        {
                            var col = polyList.Material.Effect.Color;
                            var indicesPolyList = polygon.GetIndices();

                            for (int iIndex2 = 0; iIndex2 < polygon.GetIndexAmount(); iIndex2++)
                            {
                                var index = indicesPolyList[iIndex2];
                                index.DiffuseColor = System.Drawing.Color.FromArgb(col.A, col.B, col.G, col.R);
                            }
                        }

                        //polygon.TextureMap.FixTexCoords();
                    }
                    /*else if (polyList.InstanceMaterial != null)
                    {
                        polygon.TextureFile = polyList.InstanceMaterial.Material.Effect.GetTextureFile();
                        polygon.Bitmap = polygon.TextureFile.Bitmap;
                    }*/

                    polygons.Add(polygon);
                }

                currentIndex += vertexCount;
            }
        }

        // May move inside polygon class
        public class TextureCoordinateGroup
        {
            private Vector2[] texCoords;
            private Polygon polygon;

            public TextureCoordinateGroup(Vector2[] texCoords, Polygon polygon)
            {
                this.texCoords = texCoords;
                this.polygon = polygon;
            }

            /// <summary>
            /// Removes negative texture coordinates
            /// </summary>
            /// <returns></returns>
            public void Validate()
            {
                float tLen = GetULength();
                float sLen = GetVLength();

                float minS = texCoords.Min(t => t.X);
                float minT = texCoords.Min(t => t.Y);

                if (minS < 0)
                {
                    Translate(Math.Abs(minS) * 2.0f, 0.0f);
                }
                if (minT < 0)
                {
                    Translate(0.0f, Math.Abs(minT) * 2.0f);
                }
                minS = texCoords.Min(t => t.X);
                minT = texCoords.Min(t => t.Y);

                if (sLen > 0)
                {
                    float incrementsS = minS / sLen;
                    float translationS = incrementsS * sLen;

                    Translate(-translationS, 0.0f);
                }
                if (tLen > 0)
                {
                    float incrementsT = minT / tLen;
                    float translationT = incrementsT * tLen;

                    Translate(0.0f, -translationT);
                }
            }

            public float GetULength()
            {
                var minU = texCoords.Min(t => t.X);
                var maxU = texCoords.Max(t => t.X);
                return Math.Abs(maxU - minU);
            }

            public float GetVLength()
            {
                var minV = texCoords.Min(t => t.Y);
                var maxV = texCoords.Max(t => t.Y);
                return Math.Abs(maxV - minV);
            }

            public Vector2 GetCenterOrigin()
            {
                var minU = texCoords.Min(t => t.X);
                var maxU = texCoords.Max(t => t.X);
                var minV = texCoords.Min(t => t.Y);
                var maxV = texCoords.Max(t => t.Y);

                float middleU = (minU + maxU) / 2.0f;
                float middleV = (minV + maxV) / 2.0f;

                return new Vector2(middleU, middleV);
            }

            public void Translate(float u, float v)
            {
                for (int iTexCoord = 0; iTexCoord < texCoords.Length; iTexCoord++)
                {
                    var texCoord = texCoords[iTexCoord];
                    texCoord.X += u;
                    texCoord.Y += v;
                }
            }
        }

        private void DeserializeColladaFile()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ColladaFile));

            if (!File.Exists(fileUrl))
            {
                fileUrl = Directory.GetCurrentDirectory() + fileUrl;
            }

            using (var stream = new FileStream(fileUrl, FileMode.Open))
            {
                DeserializeCollada(xmlSerializer, stream);
            }
        }

        private void SetMaterial(Polygon polygon, Material material)
        {
            if (material != null)
            {
                polygon.Material = material;
            }
            else
            {
                var diffuse = material.Effect.Color;
                for (int iColor = 0; iColor < polygon.GetIndexAmount(); iColor++)
                {
                    polygon.GetPolygonIndex(iColor).DiffuseColor = System.Drawing.Color.FromArgb(byte.MaxValue, diffuse.R, diffuse.G, diffuse.B);
                }
            }
            /*else
            {
                for (int iColor = 0; iColor < polygon.GetIndexAmount(); iColor++)
                {
                    polygon.GetPolygonIndex(iColor).DiffuseColor = System.Drawing.Color.White;
                }
            }*/
        }

        private void CreateVertices(List<Vertex> vertices, ElementsBase elements)
        {
            double[] vertexValues = CreatePositionValues(elements);
            int valuesPerVertex = Vertex.ValuesPerVertex;
            int vertexAmount = vertexValues.Length / valuesPerVertex;
            //float multiplier = asset

            // Create vertices
            for (int iIndex = 0; iIndex < vertexAmount; iIndex++)
            {
                var vertex = new Vertex(new Vector3(
                    (float)vertexValues[iIndex * valuesPerVertex],
                    (float)vertexValues[iIndex * valuesPerVertex + 1],
                    (float)vertexValues[iIndex * valuesPerVertex + 2]));

                vertices.Add(vertex);
            }
        }

        private int[] ExtractIndices(int[] original, int offset, int stride)
        {
            int amount = original.Length;
            amount /= stride;

            int[] newIndices = new int[amount];

            int iIndex = 0;
            for (int i = offset; i < original.Length; i += stride)
            {
                newIndices[iIndex] = original[i];
                iIndex++;
            }

            return newIndices;
        }

        public static Random rng = new Random();

        /// <summary>
        /// Creates the vertex values from an PolyList or Lines element.
        /// verticesPerShape = 3 if it's a PolyList
        /// verticesPerShape = 2 if it's a Lines element
        /// </summary>
        /// <param name="elements"></param>
        /// <param name="verticesPerShape"></param>
        /// <returns></returns>
        private double[] CreatePositionValues(ElementsBase elements)
        {
            int verticesPerElement = 3;
            bool isLinesElement = elements is Lines;
            var vertexInput = elements.Inputs.Where(i => i.Semantic.ToUpper() == "VERTEX").FirstOrDefault();
            var vertexPositionValues = vertexInput.Source.FloatArray.GetValues();
            int vertexCount = vertexPositionValues.Length / 3;

            int offset = 0;
            for (int iVertPosValue = 0; iVertPosValue < vertexPositionValues.Length; iVertPosValue++)
            {
                double value = vertexPositionValues[iVertPosValue];
                //value *= asset.Unit.Meter;

                if (value < 0)
                {
                    vertexPositionValues[iVertPosValue] -= offset;
                }
                else if (value > 0)
                {
                    vertexPositionValues[iVertPosValue] += offset;
                }
            }

            int stride = Vertex.ValuesPerVertex;

            int valueAmount = vertexCount * stride;

            double[] vertexValues = new double[valueAmount];

            for (int iValue = 0; iValue < vertexValues.Length; iValue++)
            {
                vertexValues[iValue] = (float)(rng.NextDouble());
            }

            int[] indices = elements.Indices.GetValues();
            int inputAmount = elements.Inputs.Length;

            if (vertexInput != null)
            {
                for (int iVertex = 0; iVertex < vertexCount; iVertex++)
                {
                    vertexValues[iVertex * stride + 0] = vertexPositionValues[iVertex * verticesPerElement + 0];
                    vertexValues[iVertex * stride + 1] = vertexPositionValues[iVertex * verticesPerElement + 1];

                    if (isLinesElement)
                    {
                        vertexValues[iVertex * stride + 1] = vertexPositionValues[iVertex * verticesPerElement + 1];
                    }
                    else
                    {
                        vertexValues[iVertex * stride + 2] = vertexPositionValues[iVertex * verticesPerElement + 2];
                    }
                }
            }

            return vertexValues;
        }

        private Vector2[] CreateTextureCoordinatesFromValues(float[] values)
        {
            int amount = values.Length / 2;
            var texCoords = new Vector2[amount];

            for (int iTexCoord = 0; iTexCoord < amount; iTexCoord++)
            {
                float s = values[iTexCoord * 2];
                float t = values[iTexCoord * 2 + 1];
                texCoords[iTexCoord] = new Vector2(s, -t);
            }

            return texCoords;
        }

        private void AssignInputSources()
        {
            foreach (var input in allInputs)
            {
                string sourceId = "";

                if (input.Semantic.ToUpper() == "VERTEX")
                {
                    string vertexSource = RemovePoundSign(input.SourceId);
                    var vertex = allVertexElements.Where(v => v.Id == vertexSource).FirstOrDefault();
                    if (vertex != null)
                    {
                        var posInput = vertex.Inputs.Where(i => i.Semantic == "POSITION").FirstOrDefault();
                        sourceId = RemovePoundSign(posInput.SourceId);

                        /*if (posInput != null)
                        {
                            sourceId = RemovePoundSign(posInput.Source);
                        }*/
                    }
                }
                else if (input.Semantic.ToUpper() == "TEXCOORD")
                {
                    sourceId = RemovePoundSign(input.SourceId);
                }

                if (sourceId.Length > 0)
                {
                    var source = allSources.Where(s => s.Id == sourceId).FirstOrDefault();
                    input.Source = source;
                }
            }
        }

        private void DeserializeCollada(XmlSerializer xmlSerializer, FileStream stream)
        {
            var collada = (ColladaFile)xmlSerializer.Deserialize(stream);

            asset = collada.Asset;

            if (collada.LibraryImages != null)
            {
                allImages = collada.LibraryImages.Images;
            }
            if (collada.LibraryEffects != null)
            {
                allEffects = collada.LibraryEffects.Effects;
            }
            if (collada.LibraryMaterials != null)
            {
                allMaterials = collada.LibraryMaterials.Materials;

                foreach (var mat in allMaterials)
                {
                    var matName = mat.Name.ToLower();
                    if (matName.Contains("_shadeless"))
                    {
                        mat.IsShadeless = true;
                    }
                    if (matName.Contains("_transparent"))
                    {
                        mat.HasTransparency = true;
                    }
                }
            }
            if (collada.LibraryLights != null)
            {
                allLights = collada.LibraryLights.Lights;
            }
            allColladaNodes = collada.LibraryVisualScenes.ChildScenes.SelectMany(s => s.ChildNodes).ToList();
            allGeometries = collada.LibraryGeometries.Geometries;
            allColladaMeshes = allGeometries.Select(g => g.Mesh).ToList();
            var polyLists = allColladaMeshes.Where(m => m.PolyLists != null).SelectMany(m => m.PolyLists).ToList();
            var lines = allColladaMeshes.Where(m => m.Lines != null).SelectMany(m => m.Lines).ToList();
            polyLists.AddRange(allColladaMeshes.Where(m => m.Triangles != null).SelectMany(m => m.Triangles));
            allElements.AddRange(polyLists);
            allElements.AddRange(lines);
            allVertexElements = allColladaMeshes.Select(m => m.Vertices).ToList();
            allSources = allGeometries.SelectMany(g => g.Mesh.Sources).ToList();
            allFloatArrays = (allColladaMeshes.SelectMany(m => m.Sources)).Select(s => s.FloatArray).ToList();
            allInputs = allElements.SelectMany(p => p.Inputs).ToList();

            foreach (var vertElement in allVertexElements)
            {
                for (int i = 0; i < vertElement.Inputs.Count(); i++)
                {
                    var currentInput = vertElement.Inputs[i];
                    currentInput.Offset = i;
                    allInputs.Add(currentInput);
                }
            }
            foreach (var currentInput in allInputs)
            {
                if (currentInput.Semantic.ToLower() == "texcoord")
                {
                    currentInput.Source = allSources.Where(i => i.Id == RemovePoundSign(currentInput.SourceId)).First();
                }

            }
            libraryVisualScenes = collada.LibraryVisualScenes;
        }

        private void CreateAllInstanceMaterials()
        {
            var instanceMats = new List<InstanceMaterial>();

            var childScenes = libraryVisualScenes.ChildScenes;

            List<ColladaNode> colladaNodes = childScenes.SelectMany(c => c.ChildNodes).ToList();
            foreach (var node in colladaNodes)
            {
                CreateAllInstanceMaterials(node, instanceMats);
            }
            

            this.allInstanceMaterials.AddRange(instanceMats);
        }

        private void CreateAllInstanceMaterials(ColladaNode node, List<InstanceMaterial> instanceMaterials)
        {
            foreach (var child in node.ChildNodes)
            {
                CreateAllInstanceMaterials(child, instanceMaterials);
            }

            /*List<ColladaNode> colladaNodes = node.ChildNodes.SelectMany(c => c.ChildNodes).ToList();*/
            List<TechniqueCommon> allTechniqueCommons = node.InstanceGeometries.Select(g => g.BindMaterial.TechniqueCommon).ToList();
            instanceMaterials.AddRange(allTechniqueCommons.SelectMany(t => t.InstanceMaterials).ToList());
        }

        private void AssignEffects()
        {
            // Create new textures
            int texIdOffset = 0;
            if (allImages != null)
            {
                if (!ignoreTextures)
                {
                    foreach (var image in allImages)
                    {
                        int textureId = texIdOffset++;
                        Bitmap bitmap = null;

                        if (!ignoreTextures)
                        {
                            var currentDir = Directory.GetCurrentDirectory();
                            var url = ValidateImageUrl(directory + image.ImageUrl);
                            var name = image.Name;

                            if (name == null)
                            {
                                name = image.ImageUrl;
                            }

                            bool bitmapFound = true;
                            /*try
                            {
                                bitmap = new Bitmap(url);
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    url = directory + image.ImageUrl;
                                    url = ValidateImageUrl(url);
                                    //image.ImageUrl = url;
                                    bitmap = new Bitmap(url);
                                }
                                catch (Exception ex2)
                                {
                                    url = ValidateImageUrl(image.ImageUrl);
                                    string url2 = url.Replace(" ", "_");
                                    string url3 = url.Replace(" ", "-");
                                    if (!File.Exists(url))
                                    {
                                        url = url2;
                                    }
                                    if (!File.Exists(url))
                                    {
                                        url = url3;
                                    }
                                    string path = fileUrl;
                                    var lastSection = path.Split('\\').Last();
                                    path = path.Replace(lastSection, url);

                                    try
                                    {
                                        bitmap = new Bitmap(path);
                                    }
                                    catch (Exception ex4)
                                    {
                                        bitmapFound = false;
                                    }

                                    /*try
                                    {
                                        bitmap = new FreeImageBitmap(path);
                                    }
                                    catch (Exception ex3)
                                    {
                                        try
                                        {
                                            var freeImgBitmap = new FreeImageAPI.FreeImageBitmap(path);

                                            bitmap = freeImgBitmap.ToBitmap();
                                        }
                                        catch (Exception ex4)
                                        {
                                            try
                                            {
                                                var freeImgBitmap = new FreeImageAPI.FreeImageBitmap(url);

                                                bitmap = freeImgBitmap.ToBitmap();
                                            }
                                            catch (Exception ex5)
                                            {
                                                bitmapFound = false;
                                            }
                                        }
                                    }*/
                            /*}
                        }*/
                            bitmapFound = File.Exists(url);

                            if (bitmapFound)
                            {
                                using (var stream = new FileStream(url, FileMode.Open))
                                {
                                    bitmap = new Bitmap(stream);
                                }
                                var bitmapFile = new BitmapFilePair(url, bitmap);
                                image.BitmapFile = bitmapFile;
                                NewTextures.Add(bitmapFile);
                            }
                            else
                            {
                                image.BitmapFile = new BitmapFilePair("internal", errorTexture);
                                NewTextures.Add(image.BitmapFile);
                            }
                        }
                    }

                    foreach (var effect in allEffects)
                    {
                        bool textureFound = false;

                        var newParams = effect.ProfileCommon.NewParams;
                        if (newParams != null && !ignoreTextures)
                        {
                            var surfaces = newParams.Select(p => p.Surface);

                            var surface2D = surfaces.Where(s => s.Type.ToUpper() == "2D").FirstOrDefault();

                            if (surface2D != null)
                            {
                                // Find matching Image
                                string imageId = RemovePoundSign(surface2D.ImageId);

                                var matchingImage = allImages.Where(i => i.Id == imageId).FirstOrDefault();
                                int index = allImages.IndexOf(matchingImage);

                                if (matchingImage != null)
                                {
                                    effect.Image = matchingImage;
                                    textureFound = true;
                                }
                            }
                        }
                        // 3DS MAX Compatibility
                        else if (newParams == null)
                        {
                            Texture tex = null;

                            if (effect.ProfileCommon.Technique.Phong != null)
                            {
                                tex = effect.ProfileCommon.Technique.Phong.Diffuse.Texture;
                            }
                            else if (effect.ProfileCommon.Technique.Lambert != null)
                            {
                                tex = effect.ProfileCommon.Technique.Lambert.Diffuse.Texture;
                            }

                            if (tex != null)
                            {
                                var matchingImage = allImages.Where(i => i.Id == tex.TextureValue).FirstOrDefault();
                                if (matchingImage != null)
                                {
                                    if (!ignoreTextures)
                                    {
                                        int index = allImages.IndexOf(matchingImage);

                                        effect.Image = matchingImage;
                                        textureFound = true;
                                    }
                                }
                            }
                        }
                        if (!textureFound)
                        {
                            try
                            {
                                var technique = effect.ProfileCommon.Technique;
                                if (technique.Phong != null)
                                {
                                    var phong = technique.Phong;
                                    var diffuse = phong.Diffuse;
                                    effect.Color = DiffuseToColor(diffuse);
                                }
                                else if (technique.Lambert != null)
                                {
                                    var lambert = technique.Lambert;
                                    var diffuse = lambert.Diffuse;
                                    effect.Color = DiffuseToColor(diffuse);
                                }
                                else if (technique.Constant != null)
                                {
                                    var constant = technique.Constant;
                                    effect.Color = TransparentToColor(constant.Transparent);
                                }
                                else
                                {
                                    effect.Color = System.Drawing.Color.White;
                                }
                            }
                            catch (NullReferenceException ex)
                            {
                                effect.Color = System.Drawing.Color.White;
                            }
                        }
                    }
                }
            }
        }

        private System.Drawing.Color DiffuseToColor(Diffuse diffuse)
        {
            var diffuseColor = diffuse.Color;
            string[] diffuseSplits = diffuseColor.Value.Split(' ').Where(s => s.Length > 0).ToArray();
            int r = (int)(float.Parse(diffuseSplits[0]) * 255.0f);
            int g = (int)(float.Parse(diffuseSplits[1]) * 255.0f);
            int b = (int)(float.Parse(diffuseSplits[2]) * 255.0f);
            return System.Drawing.Color.FromArgb(byte.MaxValue, b, g, r);
        }

        private System.Drawing.Color TransparentToColor(Transparent transparent)
        {
            var diffuseColor = transparent.Color;
            string[] transparentSplit = diffuseColor.Value.Split(' ').Where(s => s.Length > 0).ToArray();
            int r = (int)(float.Parse(transparentSplit[0]) * 255.0f);
            int g = (int)(float.Parse(transparentSplit[1]) * 255.0f);
            int b = (int)(float.Parse(transparentSplit[2]) * 255.0f);
            return System.Drawing.Color.FromArgb(byte.MaxValue, b, g, r);
        }

        private void AssignEffectsToMaterials()
        {
            foreach (var material in allMaterials)
            {
                if (material.InstanceEffect != null)
                {
                    string materialId = RemovePoundSign(material.InstanceEffect.Url);

                    var effect = allEffects.Where(e => e.Id == materialId).FirstOrDefault();

                    if (effect != null)
                    {
                        material.Effect = effect;
                        material.DiffuseColor = new Color();
                        material.DiffuseColor.Values = new float[] { material.Effect.Color.R, material.Effect.Color.G, material.Effect.Color.B, material.Effect.Color.A };
                    }
                }
            }
        }

        private void AssignMaterialsToPolyLists()
        {
            foreach (var instanceMaterial in allInstanceMaterials)
            {
                var mat = allMaterials.Where(m => m.Id == RemovePoundSign(instanceMaterial.Target)).First();
                instanceMaterial.Material = mat;
                //instanceMaterial.Material.Id = instanceMaterial.Symbol;
            }

            /*foreach (var polyList in allElements.OfType<PolyList>())
            {
                var instanceMat = allInstanceMaterials.Where(m => RemovePoundSign(m.Target) == polyList.MaterialId).FirstOrDefault();
                polyList.Material = instanceMat.Material;
            }*/

                                    /*foreach (var mat in allMaterials)
                                    {
                                        string instanceMaterialId = RemovePoundSign(mat.Id);
                                        var instanceMaterial = allInstanceMaterials.Where(m => m.Target == mat.Id).First();


                                        var polyLists = allElements.OfType<PolyList>().Where(p => p.MaterialId == instanceMaterialId);

                                        foreach (var polyList in polyLists)
                                        {
                                            polyList.Material = mat;
                                        }
                                    }*/
                                }

        /*private void AssignInstanceMaterialsToPolyLists()
        {
            foreach (var instanceMat in allInstanceMaterials)
            {
                var polyLists = allElements.OfType<PolyList>().Where(p => p.MaterialId == instanceMat.Symbol);

                foreach (var polyList in polyLists)
                {
                    var target = RemovePoundSign(instanceMat.Target);
                    var mat = allMaterials.Where(m => m.Id == target).First();
                    polyList.InstanceMaterial = instanceMat;
                    polyList.Material = mat;

                    instanceMat.Material = mat;
                }
            }
        }*/

        /*private void AssignMaterialsToInstanceMaterials()
        {
            foreach (var mat in allMaterials)
            {
                string instanceMaterialId = RemovePoundSign(mat.Id);

                var instanceMaterials = allInstanceMaterials.Where(i => RemovePoundSign(i.Target) == instanceMaterialId);

                foreach (var instanceMaterial in instanceMaterials)
                {
                    instanceMaterial.Material = mat;
                }
            }
        }*/

        private string ValidateImageUrl(string url)
        {
            // 3DS MAX Compatibility
            url = url.Replace("file://", "");

            if (url[0] == '/' || url[0] == '\\')
            {
                url = url.Remove(0, 1);
            }
            url = url.Replace("/", "\\");
            //url = url.Replace("%20", " ");
            //url = url.Replace(" ", "_");
            url = url.Replace("%3a", ":");

            return url;
        }

        private string RemovePoundSign(string text)
        {
            return text.Replace("#", "");
        }
    }
}
