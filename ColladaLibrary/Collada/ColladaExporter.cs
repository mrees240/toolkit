﻿//using Microsoft.Xna.Framework;
using ColladaLibrary.Misc;
using ColladaLibrary.Render;
using ColladaLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml.Serialization;
using TMLibrary.Lib.Collada.Elements;
using TMLibrary.Lib.Collada.Elements.Mesh;
using TMLibrary.Lib.Collada.Elements.Misc;
using TMLibrary.Lib.Collada.Elements.Scene;
/*using TMLibrary.Lib.Definitions;
using TMLibrary.Rendering;
using TMLibrary.Textures;*/

namespace TMLibrary.Lib.Collada
{
    /// <summary>
    /// Provides a class for exporting an entity or mesh to a Collada file.
    /// </summary>
    public class ColladaExporter
    {
        private string safeFileName;
        private string meshName;
        private string directory;
        private string textureDirectory;
        private ColladaFile colladaFile;
        //private ProjectDefinition project;
        //private Entity entity;
        private int currentNode;
        private bool exportingMesh;

        private List<System.Drawing.Color> colors;
        private List<Bitmap> finalTextures;
        private List<Mesh> meshes;
        private MeshMatrixPair root;
        private bool reverseFaceDirection;

        // CreateGeometry(Mesh mesh) NEEDS UPDATED SINCE COLLADA CHANGES

        public void Export(string fileName, MeshMatrixPair root, bool reverseFaceDirection)//ProjectDefinition project, Entity entity, string fileName, bool exportingMesh)
        {
            this.reverseFaceDirection = reverseFaceDirection;
            colors = new List<System.Drawing.Color>();
            meshes = new List<Mesh>();
            // CREATE MESHES
            AddMeshesToList(root);

            this.root = root;
            finalTextures = meshes.SelectMany(m => m.GetDiffuseList()).Distinct().ToList();
            colladaFile = new ColladaFile();
            colladaFile.Version = "1.4.1";
            colladaFile.Asset.Created = DateTime.Now.ToString("o");
            colladaFile.Asset.Modified = colladaFile.Asset.Created;

            var fileNameSplits = fileName.Split('\\');
            safeFileName = fileNameSplits[fileNameSplits.Length - 1];
            meshName = safeFileName.Replace(".dae", "");
            directory = fileName.Replace(safeFileName, "");
            textureDirectory = (directory + "textures").Replace("\\", "/");

            AddImageUrlList();
            CreateColors(root);
            CreateEffects();
            CreateMaterials();
            CreateGeometries();
            CreateScene();
            SerializeColladaFile(colladaFile, fileName);
            CreateTextureFiles();
        }

        private void AddMeshesToList(MeshMatrixPair meshPair)
        {
            foreach (var mesh in meshPair.Meshes)
            {
                meshes.Add(mesh);
            }

            foreach (var child in meshPair.Children)
            {
                AddMeshesToList(child);
            }
        }

        private void CreateScene()
        {
            colladaFile.Scene = new Scene();
            colladaFile.Scene.SceneInstances = new List<InstanceVisualScene>();
            var sceneInstance = new InstanceVisualScene();
            sceneInstance.Url = "#" + colladaFile.LibraryVisualScenes.ChildScenes[0].ID;
            colladaFile.Scene.SceneInstances.Add(sceneInstance);
        }

        private void CreateTextureFiles()
        {
            Directory.CreateDirectory(textureDirectory);

            foreach (var texture in finalTextures)
            {
                string url = GetTexturePath(finalTextures.IndexOf(texture));
                TextureFileUtility.WriteBitmapFile(url, texture);
            }
        }

        private string GetTexturePath(int textureId)
        {
            return textureDirectory + $"/{meshName}_{textureId}.bmp";
        }

        private void CreateEffects()
        {
            // Create effects
            colladaFile.LibraryEffects = new LibraryEffects();
            colladaFile.LibraryEffects.Effects = new List<Effect>();

            foreach (var tex in finalTextures)
            {
                int texId = finalTextures.IndexOf(tex);
                colladaFile.LibraryEffects.Effects.Add(new Effect());
                colladaFile.LibraryEffects.Effects[texId].Id = $"material_effect_img_{texId}";
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon = new ProfileCommon();

                // Setup new params
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams = new NewParam[2];
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0] = new NewParam();
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0].Sid = $"{GetImageId(texId, meshName)}_surface";
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0].Surface = new Surface();
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0].Surface.Type = "2D";
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0].Surface.ImageId = GetImageId(texId, meshName);

                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1] = new NewParam();
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1].Sid = $"{GetImageId(texId, meshName)}_sampler";
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1].Sampler = new Sampler2D();
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1].Sampler.Source = new SamplerSource();
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1].Sampler.Source.Value = colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[0].Sid;

                var technique = new Technique();
                technique.Phong = new Phong();
                technique.Sid = "common";
                technique.Phong.Diffuse = new Diffuse();
                technique.Phong.Diffuse.Texture = new Texture();
                technique.Phong.Diffuse.Texture.TexCoord = "CHANNEL0";
                technique.Phong.Diffuse.Texture.TextureValue = colladaFile.LibraryEffects.Effects[texId].ProfileCommon.NewParams[1].Sid;
                colladaFile.LibraryEffects.Effects[texId].ProfileCommon.Technique = technique;
            }

            int effectStart = finalTextures.Count;

            // Create color effects
            int colorId = 0;
            foreach (var color in colors)
            {
                var colorEffect = new Effect();
                colorEffect.Id = $"material_effect_color_{colorId}";
                colorEffect.ProfileCommon = new ProfileCommon();

                var technique = new Technique();
                technique.Phong = new Phong();
                technique.Sid = "common";
                technique.Phong.Diffuse = new Diffuse();
                technique.Phong.Diffuse.Color = new Elements.Misc.Color();
                technique.Phong.Diffuse.Color.Value = $"{color.B / 255.0f} {color.G / 255.0f} {color.R / 255.0f} {1.0f}";
                technique.Phong.Diffuse.Color.Sid = "diffuse";
                colorEffect.ProfileCommon.Technique = technique;

                colladaFile.LibraryEffects.Effects.Add(colorEffect);

                colorId++;
            }
        }

        private void CreateColors(MeshMatrixPair meshPair)
        {
            // Create colors
            //foreach (var meshPair in meshMatrixPairs)
            {
                foreach (var mesh in meshPair.Meshes)
                {
                    var meshColors = mesh.GetColors();

                    foreach (var color in meshColors)
                    {
                        if (!colors.Contains(color))
                        {
                            colors.Add(color);
                        }
                    }
                }
                foreach (var child in meshPair.Children)
                {
                    CreateColors(child);
                }
            }
        }

        private void CreateMaterials()
        {
            colladaFile.LibraryMaterials = new LibraryMaterials();
            colladaFile.LibraryMaterials.Materials = new List<Material>();

            foreach (var texFile in finalTextures)
            {
                int idNumber = finalTextures.IndexOf(texFile);
                var texMat = new Material();
                texMat.Effect = colladaFile.LibraryEffects.Effects[idNumber];
                texMat.InstanceEffect = new InstanceEffect();
                texMat.Id = $"material_img_{idNumber}";
                texMat.InstanceEffect.Url = $"#material_effect_img_{idNumber}";
                texMat.Name = texMat.Id;
                colladaFile.LibraryMaterials.Materials.Add(texMat);
            }
            int colorId = 0;
            int materialIndexStart = finalTextures.Count;

            foreach (var color in colors)
            {
                var colorMat = new Material();
                colorMat.Effect = colladaFile.LibraryEffects.Effects[materialIndexStart + colorId];
                colorMat.InstanceEffect = new InstanceEffect();
                colorMat.Id = $"material_color_{colorId}";
                colorMat.InstanceEffect.Url = $"#material_effect_color_{colorId}";
                colorMat.Name = colorMat.Id;
                colladaFile.LibraryMaterials.Materials.Add(colorMat);

                ++colorId;
            }
        }

        private List<PolygonList> CreatePolygonLists(Mesh mesh)
        {
            var polyList = new List<PolygonList>();

            var textures = mesh.GetDiffuseList();
            var colors = mesh.GetColors();

            foreach (var tex in textures)
            {
                var list = new PolygonList();
                list.Texture = tex;
                polyList.Add(list);
            }
            foreach (var color in colors)
            {
                var list = new PolygonList();
                list.Color = color;
                polyList.Add(list);
            }

            return polyList;
        }

        private class PolygonList
        {
            public System.Drawing.Color Color;
            public Bitmap Texture;
        }

        private void CreateGeometry(Mesh mesh)
        {
            //foreach (var mesh in meshPair.Meshes)
            {
                int meshId = meshes.IndexOf(mesh);
                var polyLists = CreatePolygonLists(mesh);

                var meshVertices = mesh.Vertices.Distinct().ToList();

                var geometry = new Geometry();
                geometry.Id = $"mesh_{meshId}";
                geometry.Name = $"mesh_{meshId}";
                geometry.Mesh = new ColladaMesh();
                geometry.Mesh.PolyLists = new List<PolyList>();
                var positionSource = new Source();
                var texCoordSource = new Source();

                // Create vertex position source
                positionSource.Id = $"{geometry.Id}_positions";
                positionSource.FloatArray = new FloatArray();
                positionSource.FloatArray.Id = $"{positionSource.Id}_array";
                positionSource.FloatArray.Count = meshVertices.Count * 3;
                positionSource.FloatArray.Value = string.Empty;
                positionSource.TechniqueCommon = new TechniqueCommon();

                // Create position accesses
                positionSource.TechniqueCommon.Accessor = new Accessor();
                positionSource.TechniqueCommon.Accessor.Source = $"#{positionSource.FloatArray.Id}";
                positionSource.TechniqueCommon.Accessor.Stride = 3;
                positionSource.TechniqueCommon.Accessor.Count = meshVertices.Count();
                positionSource.TechniqueCommon.Accessor.Parameter = CreatePositionParameterList();

                // Create tex coord source
                texCoordSource.Id = $"{geometry.Id}_texcoords";
                texCoordSource.FloatArray = new FloatArray();
                texCoordSource.FloatArray.Id = $"{texCoordSource.Id}_array";
                texCoordSource.FloatArray.Count = meshVertices.Count() * 2;
                texCoordSource.TechniqueCommon = new TechniqueCommon();

                // Create tex coord accesses
                texCoordSource.TechniqueCommon.Accessor = new Accessor();
                texCoordSource.TechniqueCommon.Accessor.Source = $"#{texCoordSource.FloatArray.Id}";
                texCoordSource.TechniqueCommon.Accessor.Stride = 2;
                texCoordSource.TechniqueCommon.Accessor.Parameter = CreateTexCoordParameterList();

                // Create vertices element
                geometry.Mesh.Vertices = new Vertices();
                geometry.Mesh.Vertices.Id = $"{geometry.Id}_vertices";
                geometry.Mesh.Vertices.Inputs = new Input[1];
                geometry.Mesh.Vertices.Inputs[0] = new Input();
                geometry.Mesh.Vertices.Inputs[0].Semantic = "POSITION";
                geometry.Mesh.Vertices.Inputs[0].Source = positionSource;
                geometry.Mesh.Vertices.Inputs[0].SourceId = $"#{geometry.Id}_positions";
                //geometry.Mesh.Vertices.Inputs[0].Offset = null;

                int texCoordIndex = 0;

                var vertSet = new List<Vertex>();
                vertSet.AddRange(mesh.Vertices);

                foreach (var polyList in polyLists)
                {
                    var polygons = new List<Polygon>();
                    var polyListElement = new PolyList();

                    if (polyList.Texture != null)
                    {
                        polygons = mesh.GetPolygonsWithDiffuseTexture(polyList.Texture);
                        polyListElement.Inputs = new Input[2];
                        polyListElement.Inputs[1] = new Input();
                        polyListElement.Inputs[1].Offset = 1;
                        polyListElement.Inputs[1].Semantic = "TEXCOORD";
                        polyListElement.Inputs[1].SourceId = $"#{texCoordSource.Id}";
                        polyListElement.Inputs[1].Source = texCoordSource;
                        polyListElement.Inputs[1].Set = "0";
                    }
                    else
                    {
                        polygons = mesh.GetPolygonsWithColor(polyList.Color);
                        polyListElement.Inputs = new Input[1];
                    }

                    if (polygons.Count == 0)
                    {
                        continue;
                    }

                    var indicesList = new StringBuilder();
                    polyListElement.Count = polygons.Count;
                    polyListElement.Inputs[0] = new Input();
                    polyListElement.Inputs[0].Offset = 0;
                    polyListElement.Inputs[0].Semantic = "VERTEX";
                    polyListElement.Inputs[0].SourceId = $"#{geometry.Mesh.Vertices.Id}";
                    polyListElement.Inputs[0].Source = positionSource;

                    // Create polygon count
                    var polyCountList = new StringBuilder();

                    foreach (var vert in mesh.Vertices)
                    {
                        positionSource.FloatArray.Value +=
                            vert.Vector.X + " " + vert.Vector.Y + " " + vert.Vector.Z + " ";
                    }

                    foreach (var polygon in polygons)
                    {
                        if (polygon.GetIndexAmount() == 3)
                        {
                            polyCountList.Append($"3 ");

                            var vertex0 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(0).Id);
                            var vertex1 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(1).Id);
                            var vertex2 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(2).Id);

                            if (polyList.Texture != null)
                            {
                                var texCoords = polygon.GetAllTextureCoordinates();

                                texCoordSource.FloatArray.Value += $"{texCoords[0].U} {-texCoords[0].V} ";
                                texCoordSource.FloatArray.Value += $"{texCoords[1].U} {-texCoords[1].V} ";
                                texCoordSource.FloatArray.Value += $"{texCoords[2].U} {-texCoords[2].V} ";
                            }
                            var indices = polygon.GetIndices();
                            int index0 = indices[0].Id;
                            int index1 = indices[1].Id;
                            int index2 = indices[2].Id;

                            if (polyList.Texture != null)
                            {
                                indicesList.Append($"{index0} {texCoordIndex++} ");
                                indicesList.Append($"{index1} {texCoordIndex++} ");
                                indicesList.Append($"{index2} {texCoordIndex++} ");
                            }
                            else
                            {
                                indicesList.Append($"{index0} ");
                                indicesList.Append($"{index1} ");
                                indicesList.Append($"{index2} ");
                            }
                        }
                        else if (polygon.GetIndexAmount() == 4)
                        {
                            polyCountList.Append($"4 ");

                            var vertex0 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(0).Id);
                            var vertex1 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(1).Id);
                            var vertex2 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(2).Id);
                            var vertex3 = mesh.Vertices.ElementAt(polygon.GetPolygonIndex(3).Id);

                            int index0 = vertSet.IndexOf(vertex0);
                            int index1 = vertSet.IndexOf(vertex1);
                            int index2 = vertSet.IndexOf(vertex2);
                            int index3 = vertSet.IndexOf(vertex2);

                            if (!vertSet.Contains(vertex0))
                            {
                                vertSet.Add(vertex0);
                                positionSource.FloatArray.Value +=
                                    vertex0.Vector.X + " " + vertex0.Vector.Y + " " + vertex0.Vector.Z + " ";
                            }
                            if (!vertSet.Contains(vertex1))
                            {
                                vertSet.Add(vertex1);
                                positionSource.FloatArray.Value +=
                                    vertex1.Vector.X + " " + vertex1.Vector.Y + " " + vertex1.Vector.Z + " ";
                            }
                            if (!vertSet.Contains(vertex2))
                            {
                                vertSet.Add(vertex2);
                                positionSource.FloatArray.Value +=
                                    vertex2.Vector.X + " " + vertex2.Vector.Y + " " + vertex2.Vector.Z + " ";
                            }
                            if (!vertSet.Contains(vertex3))
                            {
                                vertSet.Add(vertex3);
                                positionSource.FloatArray.Value +=
                                    vertex3.Vector.X + " " + vertex3.Vector.Y + " " + vertex3.Vector.Z + " ";
                            }

                            if (polyList.Texture != null)
                            {
                                var texCoords = polygon.GetAllTextureCoordinates();

                                texCoordSource.FloatArray.Value += $"{texCoords[0].U} {-texCoords[0].V} ";
                                texCoordSource.FloatArray.Value += $"{texCoords[1].U} {-texCoords[1].V} ";
                                texCoordSource.FloatArray.Value += $"{texCoords[2].U} {-texCoords[2].V} ";
                                texCoordSource.FloatArray.Value += $"{texCoords[3].U} {-texCoords[3].V} ";
                            }
                            if (polyList.Texture != null)
                            {
                                indicesList.Append($"{index0} {texCoordIndex++} ");
                                indicesList.Append($"{index1} {texCoordIndex++} ");
                                indicesList.Append($"{index2} {texCoordIndex++} ");
                                indicesList.Append($"{index3} {texCoordIndex++} ");
                            }
                            else
                            {
                                indicesList.Append($"{index0} ");
                                indicesList.Append($"{index1} ");
                                indicesList.Append($"{index2} ");
                                indicesList.Append($"{index3} ");
                            }
                        }
                    }
                    polyListElement.VertexCount = new VertexCount();
                    polyListElement.VertexCount.Value = polyCountList.ToString();

                    polyListElement.Indices = new Indices();

                    // Remove extra space
                    if (indicesList.Length > 0)
                    {
                        indicesList.Remove(indicesList.Length - 1, 1);
                    }
                    polyListElement.Indices.Value = indicesList.ToString();
                    if (polyList.Texture != null)
                    {
                        int texId = finalTextures.IndexOf(polyList.Texture);
                        polyListElement.MaterialId = $"material_img_{texId}";
                    }
                    else
                    {
                        int colorId = colors.IndexOf(polyList.Color);
                        polyListElement.MaterialId = $"material_color_{colorId}";
                    }
                    if (polyListElement.VertexCount.Value.Length > 0)
                    {
                        polyListElement.VertexCount.Value = polyListElement.VertexCount.Value.Remove(polyListElement.VertexCount.Value.Length - 1, 1);
                    }
                    geometry.Mesh.PolyLists.Add(polyListElement);
                }
                texCoordSource.FloatArray.Count = texCoordIndex * 2;
                texCoordSource.TechniqueCommon.Accessor.Count = texCoordIndex;
                // Remove extra space
                if (positionSource.FloatArray.Value.Length > 0)
                {
                    positionSource.FloatArray.Value = positionSource.FloatArray.Value.Remove(positionSource.FloatArray.Value.Length - 1, 1);
                }
                if (texCoordSource.FloatArray.Value.Length > 0)
                {
                    texCoordSource.FloatArray.Value = texCoordSource.FloatArray.Value.Remove(texCoordSource.FloatArray.Value.Length - 1, 1);
                }
                geometry.Mesh.Sources = new Source[2];
                geometry.Mesh.Sources[0] = positionSource;
                geometry.Mesh.Sources[1] = texCoordSource;

                if (geometry.Mesh.PolyLists.Any())
                {
                    colladaFile.LibraryGeometries.Geometries.Add(geometry);
                }
            }
        }

        private void CreateGeometries()
        {
            colladaFile.LibraryGeometries = new LibraryGeometries();

            colladaFile.LibraryGeometries.Geometries = new List<Geometry>();

            // Every mesh has a geometry

            foreach (var mesh in meshes)
            {
                CreateGeometry(mesh);
            }

            // Create entity nodes
            colladaFile.LibraryVisualScenes = new LibraryVisualScenes();

            var rootNode = new ColladaNode();
            //rootNode.InstanceGeometry = new InstanceGeometry();
            colladaFile.LibraryVisualScenes.ChildScenes = new VisualScene[1];
            colladaFile.LibraryVisualScenes.ChildScenes[0] = new VisualScene();
            colladaFile.LibraryVisualScenes.ChildScenes[0].ID = "main_scene";
            colladaFile.LibraryVisualScenes.ChildScenes[0].Name = "main_scene";
            colladaFile.LibraryVisualScenes.ChildScenes[0].ChildNodes = new ColladaNode[] { rootNode };

            //rootNode.ChildNodes[0] = new Node[1];
            rootNode.ChildNodes = new List<ColladaNode>();
            var node2 = new ColladaNode();
            node2.Type = "NODE";
            node2.ID = "root";
            node2.Name = "root";
            node2.InstanceGeometries = new List<InstanceGeometry>();
            var instanceGeometry = new InstanceGeometry();
            instanceGeometry.Name = "root_geometry";
            node2.InstanceGeometries.Add(instanceGeometry);
            rootNode.ChildNodes.Add(node2);

            node2 = CreateRecursiveNode(rootNode, root);

        }

        private ColladaNode CreateRecursiveNode(ColladaNode parent, MeshMatrixPair meshMatrixPair)
        {
            int totalChildren = meshMatrixPair.Children.Count;
            foreach (var meshPair in meshMatrixPair.Children)
            {
                foreach (var mesh in meshPair.Meshes)
                {
                    int totalPolyList = mesh.GetDiffuseList().Count() + mesh.GetColors().Count();
                    totalChildren += totalPolyList;
                }
            }

            parent.Name = "node_" + currentNode++;
            parent.Type = "NODE";
            parent.ChildNodes = new List<ColladaNode>();
            parent.Matrix = new ColladaMatrix();
            parent.Matrix.Value = ConvertMatrixToText(meshMatrixPair.Matrix);
            parent.Matrix.Sid = "transform";

            for (int iChild = 0; iChild < meshMatrixPair.Children.Count; iChild++)
            {
                var childMatrixPair = meshMatrixPair.Children[iChild];

                var child = new ColladaNode();
                parent.ChildNodes.Add(CreateRecursiveNode(child, childMatrixPair));
            }

            for (int iMesh = 0; iMesh < meshMatrixPair.Meshes.Count; iMesh++)
            {
                var mesh = meshMatrixPair.Meshes.ElementAt(iMesh);
                int meshId = meshes.IndexOf(mesh);

                parent.ChildNodes.Add(CreateMeshNode(meshId, mesh, meshMatrixPair.Matrix));
            }

            return parent;
        }

        private ColladaNode CreateMeshNode(int meshId, Mesh mesh, Matrix4x4 matrix)
        {
            var textures = mesh.GetDiffuseList();
            var meshColors = mesh.GetColors();

            var node = new ColladaNode();
            node.Type = "NODE";
            node.Name = "node_" + currentNode;
            var instanceGeometry = new InstanceGeometry();
            instanceGeometry.BindMaterial = new BindMaterial();
            instanceGeometry.BindMaterial.TechniqueCommon = new TechniqueCommon();

            node.InstanceGeometries.Add(instanceGeometry);

            foreach (var texture in textures)
            {
                int texId = finalTextures.IndexOf(texture);
                var instanceMaterial = new InstanceMaterial();
                instanceMaterial.Target = $"#material_img_{texId}";
                instanceMaterial.Symbol = $"material_img_{texId}";
                instanceGeometry.BindMaterial.TechniqueCommon.InstanceMaterials.Add(instanceMaterial);
            }
            foreach (var color in meshColors)
            {
                var instanceMaterial = new InstanceMaterial();
                int colorId = colors.IndexOf(color);
                instanceMaterial.Target = $"#material_color_{colorId}";
                instanceMaterial.Symbol = $"material_color_{colorId}";
                instanceGeometry.BindMaterial.TechniqueCommon.InstanceMaterials.Add(instanceMaterial);
            }

            instanceGeometry.Url = $"#mesh_{meshId}";
            instanceGeometry.Name = "mesh_geometry_" + currentNode++;

            return node;
        }

        private string ConvertMatrixToText(Matrix4x4 matrix)
        {
            matrix = Matrix4x4.Transpose(matrix);

            string text =
                $"{matrix.M11} {matrix.M12} {matrix.M13} {matrix.M14} " +
                $"{matrix.M21} {matrix.M22} {matrix.M23} {matrix.M24} " +
                $"{matrix.M31} {matrix.M32} {matrix.M33} {matrix.M34} " +
                $"{matrix.M41} {matrix.M42} {matrix.M43} {matrix.M44}";
            return text;
        }

        private Parameter[] CreatePositionParameterList()
        {
            var parameterX = new Parameter();
            parameterX.Name = "X";
            parameterX.Type = "float";

            var parameterY = new Parameter();
            parameterY.Name = "Y";
            parameterY.Type = "float";

            var parameterZ = new Parameter();
            parameterZ.Name = "Z";
            parameterZ.Type = "float";

            return new Parameter[]
            {
                parameterX,
                parameterY,
                parameterZ,
            };
        }

        private Parameter[] CreateTexCoordParameterList()
        {
            var parameterX = new Parameter();
            parameterX.Name = "S";
            parameterX.Type = "float";

            var parameterY = new Parameter();
            parameterY.Name = "T";
            parameterY.Type = "float";

            return new Parameter[]
            {
                parameterX,
                parameterY
            };
        }

        private string FloatArrayPositionToText(List<Vertex> vertices)
        {
            var sb = new StringBuilder();
            foreach (var vertex in vertices)
            {
                sb.Append(vertex.Vector.X);
                sb.Append(" ");
                sb.Append(vertex.Vector.Y);
                sb.Append(" ");
                sb.Append(vertex.Vector.Z);

                if (vertices.Last() != vertex)
                {
                    sb.Append(" ");
                }
            }

            return sb.ToString();
        }

        private string FloatArrayTexCoordToText(List<Vertex> vertices)
        {
            var sb = new StringBuilder();
            foreach (var vertex in vertices)
            {
                sb.Append(vertex.U);
                sb.Append(" ");
                sb.Append(vertex.V);
                sb.Append(" ");
                if (vertices.Last() != vertex)
                {
                    sb.Append(" ");
                }
            }

            return sb.ToString();
        }

        private void AddImageUrlList()
        {
            foreach (var mesh in meshes)
            {
                var textures = mesh.GetDiffuseList();

                foreach (var tex in textures)
                {
                    if (!finalTextures.Contains(tex))
                    {
                        finalTextures.Add(tex);
                    }
                }
            }

            colladaFile.LibraryImages = new LibraryImages();
            colladaFile.LibraryImages.Images = new List<ColladaImage>();

            for (int iTex = 0; iTex < finalTextures.Count(); iTex++)
            {
                string extension = ".bmp";
                var textureFile = finalTextures[iTex];
                string imageId = GetImageId(iTex, meshName);
                var colladaImage = new ColladaImage();
                colladaImage.Id = imageId;
                colladaImage.Name = imageId;
                colladaImage.ImageUrl = $"textures/{meshName}_{iTex}{extension}";
                colladaImage.ImageUrl = colladaImage.ImageUrl.Replace(" ", "%20");
                colladaFile.LibraryImages.Images.Add(colladaImage);
            }
        }

        private string GetImageId(int textureId, string meshName)
        {
            return $"{meshName}_{textureId}_bmp";
        }

        private void SerializeColladaFile(ColladaFile colladaFile, string fileUrl)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ColladaFile));

            using (var stream = new FileStream(fileUrl, FileMode.Create))
            {
                xmlSerializer.Serialize(stream, colladaFile);
            }
        }
    }
}
